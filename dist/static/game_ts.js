/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./client_src/Main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./client_src/Controller.ts":
/*!**********************************!*\
  !*** ./client_src/Controller.ts ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const GUI_1 = __webpack_require__(/*! ./gui/GUI */ "./client_src/gui/GUI.ts");
const Client_1 = __webpack_require__(/*! ./networking/Client */ "./client_src/networking/Client.ts");
const HitMap_1 = __webpack_require__(/*! ./game/HitMap */ "./client_src/game/HitMap.ts");
const GamePhase_1 = __webpack_require__(/*! ./game/GamePhase */ "./client_src/game/GamePhase.ts");
const Item_1 = __webpack_require__(/*! ./game/Item */ "./client_src/game/Item.ts");
const Inventory_1 = __webpack_require__(/*! ./game/Inventory */ "./client_src/game/Inventory.ts");
const ItemMemento_1 = __webpack_require__(/*! ./game/ItemMemento */ "./client_src/game/ItemMemento.ts");
const Action_1 = __webpack_require__(/*! ./game/Action */ "./client_src/game/Action.ts");
class Controller {
    constructor() {
        // True iff the GUI is animating changes
        this._animating = false;
        // Storm variables
        this._stormRadius = -1;
        this._stormDmgPerTurn = 0;
        this._stormSickDmgPerTurn = 0;
        // Game details obtained from server
        // The current phase of the game (defaults to LOBBY i.e. not started)
        this._gamePhase = GamePhase_1.GamePhase.LOBBY;
        // Time taken for each phase (in seconds)
        this._dropPhaseTime = 10;
        this._movePhaseTime = 10;
        this._actionPhaseTime = 30;
        // AP value for my player (as a percentage)
        this._myPlayerAP = 100;
        // The player ID of the the player using this client
        this._myID = -1;
        // If true, _myID gives the ID of spectated player and no actions can be made
        this._spectating = false;
        this._gui = GUI_1.GUI.new(this);
        this._client = new Client_1.Client(this);
        this._playerStubs = {};
        this._playerDetails = {};
        this._playerHPs = {};
        this._myPlayerModifiers = {
            viewRange: 0,
            hitChance: 0,
            totalAP: 0,
            apMoveCost: 0,
            dmgPerMove: 0,
            sickDmgPerTurn: 0,
            viewRangeDesc: null,
            hitChanceDesc: null,
            totalAPDesc: null,
            apMoveCostDesc: null,
            dmgPerMoveDesc: null,
            sickDmgPerTurnDesc: null
        };
        this._hitMap = new HitMap_1.HitMap();
        this._items = new Array();
        this._itemMementos = new Array();
        this._myPlayerActions = new Array();
        this._myPlayerInventory = new Inventory_1.Inventory(this);
    }
    // Start a timer
    startTimer(time, intervalCallback, endedCallback, setTimeoutID = false, interval = 200) {
        const startTime = Date.now();
        const $this = this;
        function timerLoop() {
            const timeElapsed = Date.now() - startTime;
            const timeLeft = time * 1000 - timeElapsed;
            if (timeLeft > 0) {
                // check again in 200 ms
                const id = setTimeout(timerLoop, interval);
                if (setTimeoutID) {
                    $this._timeoutID = id;
                }
                if (intervalCallback) {
                    intervalCallback(timeLeft, id);
                }
            }
            else if (endedCallback) {
                // Reached end of time, therefore, call callback function
                endedCallback();
            }
        }
        // start the first execution of timer loop immediately
        timerLoop();
    }
    // Send a message to the other players in the lobby
    sendMsg(msg) {
        this._client.sendSendMsgMsg(msg);
    }
    // Toggle the ready state of the player
    // Sends ready state toggle command to server
    toggleReadyState() {
        this._client.sendToggleReadyStateMsg();
    }
    // Set the ready state of player to true
    setReadyStateToTrue() {
        this._client.sendSetReadyStateToTrueMsg();
    }
    // Notify the server of the player's move
    setMoveTurn(x, y) {
        this._client.sendSetMoveTurnMsg(x, y);
    }
    // Notify the server that the player wishes to search a tile
    searchTile(x, y) {
        this._client.sendSearchTileMsg(x, y);
    }
    // Notify the server that the player has taken an item
    takeItem(entityID, backpackX, backpackY) {
        this._client.sendTakeItemMsg(entityID, backpackX, backpackY);
    }
    // Notify the server that the player wishes to equip an item from their backpack
    equipItem(entityID) {
        this._client.sendEquipItemMsg(entityID);
    }
    // Notify the server that the player wishes to drop an item from their backpack
    dropItem(entityID) {
        this._client.sendDropItemMsg(entityID);
    }
    // Notify the server that the player wishes to use a bandage from their backpack
    useBandage(entityID, limb) {
        this._client.sendUseBandageMsg(entityID, limb);
        this._gui.requestCloseFullOverlay();
    }
    // Notify the server that the player wishses to attack another player
    attackPlayer(playerID, limb) {
        this._client.sendAttackPlayerMsg(playerID, limb);
    }
    // Notify the server that the player has finished their turn
    endTurn() {
        this._client.sendEndTurnMsg();
    }
    // Request the map to be redrawn
    requestDrawMap() {
        if (this._gui && !this._animating) {
            this._gui.requestDrawMap();
        }
    }
    // Request that the movement of players be animated on the map
    requestAnimateMovement(oldPlayerDetails, callback) {
        if (!this._animating) {
            this._animating = true;
            this._gui.requestAnimateMovement(oldPlayerDetails, () => {
                this._animating = false;
                callback();
            });
        }
    }
    // Returns true iff there is a player on the specified tile
    // IMPORTANT - Excludes my player
    isPlayerOnTile(x, y) {
        for (let playerID in this._playerDetails) {
            const player = this._playerDetails[playerID];
            if (player.x == x && player.y == y && player != this.myPlayerDetails) {
                return true;
            }
        }
        return false;
    }
    // Returns list of player ids on tile
    // IMPORTANT - Excludes my player
    getPlayersOnTile(x, y) {
        let ls = new Array();
        for (let playerID in this._playerDetails) {
            const player = this._playerDetails[playerID];
            if (player.x == x && player.y == y && player != this.myPlayerDetails) {
                ls.push(playerID);
            }
        }
        return ls;
    }
    get myPlayerStub() {
        return this._playerStubs[this._myID];
    }
    get myPlayerDetails() {
        return this._playerDetails[this._myID];
    }
    get myPlayerModifiers() {
        return this._myPlayerModifiers;
    }
    set myPlayerModifiers(p) {
        if (p) {
            this._myPlayerModifiers = p;
            this._gui.onMyPlayerModifiersUpdated();
        }
    }
    get myPlayerHP() {
        return this._playerHPs[this._myID];
    }
    get playerHPs() {
        return this._playerHPs;
    }
    set playerHPs(hp) {
        if (hp) {
            if (hp[this._myID]) {
                const totalHPChangedBy = this.myPlayerHP ? hp[this._myID].total - this.myPlayerHP.total : 0;
                const headInjured = this.myPlayerHP ? this.myPlayerHP.head > hp[this._myID].head : false;
                const chestInjured = this.myPlayerHP ? this.myPlayerHP.chest > hp[this._myID].chest : false;
                const rarmInjured = this.myPlayerHP ? this.myPlayerHP.rarm > hp[this._myID].rarm : false;
                const larmInjured = this.myPlayerHP ? this.myPlayerHP.larm > hp[this._myID].larm : false;
                const rlegInjured = this.myPlayerHP ? this.myPlayerHP.rleg > hp[this._myID].rleg : false;
                const llegInjured = this.myPlayerHP ? this.myPlayerHP.lleg > hp[this._myID].lleg : false;
                this._playerHPs = hp;
                this._gui.onMyPlayerHPUpdated(totalHPChangedBy, headInjured, chestInjured, rarmInjured, larmInjured, rlegInjured, llegInjured);
            }
            else {
                this._playerHPs = hp;
            }
        }
    }
    get myPlayerAP() {
        return this._myPlayerAP;
    }
    /*set myPlayerAP(ap: number) {
        this._myPlayerAP = ap;
    }*/
    get myPlayerInventory() {
        return this._myPlayerInventory;
    }
    get myPlayerEquippedItem() {
        //console.log("Equipped", this._myPlayerEffectivelyEquippedItem, this._myPlayerEquippedItem)
        if (this._myPlayerEffectivelyEquippedItem != null) {
            return this._myPlayerEffectivelyEquippedItem;
        }
        return this._myPlayerEquippedItem;
    }
    set myPlayerEquippedItem(entity) {
        if (entity) {
            this._myPlayerEquippedItem = entity;
        }
    }
    get spectating() {
        return this._spectating;
    }
    get gamePhase() {
        return this._gamePhase;
    }
    get globalMouseX() {
        return this._globalMouseX;
    }
    get globalMouseY() {
        return this._globalMouseY;
    }
    set globalMouseX(x) {
        this._globalMouseX = x;
    }
    set globalMouseY(y) {
        this._globalMouseY = y;
    }
    // Get a reference to the loaded map
    get map() {
        return this._map;
    }
    // Set the map being played
    set map(m) {
        if (m) {
            this._map = m;
        }
    }
    get hitMap() {
        return this._hitMap;
    }
    // Get a reference to the list of all available items
    get items() {
        return this._items;
    }
    get stormRadius() {
        return this._stormRadius;
    }
    get stormDmgPerTurn() {
        return this._stormDmgPerTurn;
    }
    get stormSickDmgPerTurn() {
        return this._stormSickDmgPerTurn;
    }
    // Get all player stubs
    get playerStubs() {
        return this._playerStubs;
    }
    // Get all player details
    get playerDetails() {
        return this._playerDetails;
    }
    // Set all the player stubs
    set playerStubs(stubs) {
        if (stubs) {
            this._playerStubs = stubs;
            this._gui.onPlayerStubsUpdated(); // notify GUI of player stubs update
        }
    }
    // Set all the player details
    set playerDetails(details) {
        if (details) {
            // Keep track of previous details and animate between them
            clearTimeout(this._timeoutID);
            this._timeoutID = null;
            const oldDetails = this._playerDetails;
            this._playerDetails = details;
            console.log("got details", this.gamePhase);
            // If in the move phase, animate the movement of players
            if (this.gamePhase == GamePhase_1.GamePhase.MOVE_PHASE) {
                console.log("animating");
                this.requestAnimateMovement(oldDetails, () => {
                    this.setReadyStateToTrue();
                    this._gui.onWaitingForServer();
                    console.log("done animating");
                    this.requestDrawMap();
                });
            }
            else {
                this.setReadyStateToTrue();
                this._gui.onWaitingForServer();
            }
            if (this._playerDetails[this._myID]) {
                this.onMyPlayerDetailsUpdated();
            }
        }
    }
    // Set a single player stub
    setPlayerStub(id, stub) {
        if (stub) {
            this._playerStubs[id] = stub;
            this._gui.onPlayerStubsUpdated(); // notify GUI of player stubs update
        }
    }
    // Set a single player's details
    setPlayerDetails(id, details) {
        if (details) {
            this._playerDetails[id] = details;
            if (id == "" + this._myID) {
                this.onMyPlayerDetailsUpdated();
            }
        }
    }
    // Called whenever myPlayerDetails are updated by the server
    onMyPlayerDetailsUpdated() {
        // Notify GUI of my player details update
        this._gui.onMyPlayerDetailsUpdated();
    }
    // Get a reference to the list of item mementos
    get itemMementos() {
        return this._itemMementos;
    }
    // Add a new item memento to the list of item mementos
    addItemMemento(i) {
        if (i) {
            let exists = false;
            for (const m of this._itemMementos) {
                if (m.mapX === i.mapX && m.mapY === i.mapY) {
                    // Reset this memento rather than adding a new one
                    m.items = i.items;
                    m.reset();
                    exists = true;
                    break;
                }
            }
            // Only add new memento if there is no momento on same tile
            if (!exists) {
                i.colorKey = this._hitMap.addItemMemento(i);
                this._itemMementos.push(i);
            }
        }
    }
    // Set the current item found
    // TODO - add support for multiple items found on one tile
    setItemsFound(items) {
        // Add a new memento for the current tile
        let m = new ItemMemento_1.ItemMemento(this.myPlayerDetails.x, this.myPlayerDetails.y);
        for (const obj of items) {
            const itemID = parseInt(obj.itemID, 10);
            const item = itemID >= 0 && itemID < this._items.length
                ? this._items[itemID] : null;
            if (item) {
                m.items.push(item);
            }
        }
        this.addItemMemento(m);
        this._gui.setItemsFound(items);
    }
    // Get the time given for the drop phase (in seconds)
    get dropPhaseTime() {
        return this._dropPhaseTime;
    }
    // Get the time given for the move phase (in seconds)
    get movePhaseTime() {
        return this._movePhaseTime;
    }
    // Get the time given for the action phase (in seconds)
    get actionPhaseTime() {
        return this._actionPhaseTime;
    }
    // Called when the server initiates the drop phase
    onDropPhaseStart() {
        this._gamePhase = GamePhase_1.GamePhase.DROP_PHASE;
        this._gui.onGameStarted();
        this._gui.onDropPhaseStart();
        this.requestDrawMap();
        this.startTimer(this._dropPhaseTime, (timeLeft) => {
            this._gui.updateTimerLabel("Drop Phase", Math.floor(timeLeft / 1000));
        }, () => {
            if (this._gamePhase === GamePhase_1.GamePhase.DROP_PHASE) {
                this._gamePhase = GamePhase_1.GamePhase.WAITING_FOR_SERVER;
                // Send ready message
                this.setReadyStateToTrue();
                this._gui.onWaitingForServer();
            }
        }, true);
    }
    // Called when the server initiates a move phase
    onMovePhaseStart() {
        // Full overlay should only ever be open during action phase
        this._gui.requestCloseFullOverlay();
        // Determine the current effectively equipped item
        this._myPlayerEquippedItem = this._myPlayerEffectivelyEquippedItem ?
            this._myPlayerEffectivelyEquippedItem : this._myPlayerEquippedItem;
        this._myPlayerEffectivelyEquippedItem = null;
        // Increment all item momentos
        for (const m of this._itemMementos) {
            m.incSince();
        }
        // Remove any expired item mementos
        this._itemMementos = this._itemMementos.filter((e) => {
            return e.since < 6;
        });
        // Reset turn stats
        this._myPlayerAP = 100 +
            (this.myPlayerModifiers.totalAP != null ? this.myPlayerModifiers.totalAP : 0);
        this._gamePhase = GamePhase_1.GamePhase.MOVE_PHASE;
        this._gui.onMovePhaseStart();
        this.startTimer(this._movePhaseTime, (timeLeft) => {
            this._gui.updateTimerLabel("Move Phase", Math.floor(timeLeft / 1000));
        }, () => {
            if (this._gamePhase === GamePhase_1.GamePhase.MOVE_PHASE) {
                // Send ready message
                this.waitForPlayerDetailsUpdate(5);
                //    this._gui.onWaitingForServer();
            }
        }, true);
    }
    // Called when the server initiates an action phase
    onActionPhaseStart() {
        this._myPlayerActions.length = 0;
        this._gamePhase = GamePhase_1.GamePhase.ACTION_PHASE;
        this._gui.onActionPhaseStart();
        this.startTimer(this.actionPhaseTime, (timeLeft) => {
            this._gui.updateTimerLabel("Action Phase", Math.floor(timeLeft / 1000));
        }, () => {
            if (this._gamePhase === GamePhase_1.GamePhase.ACTION_PHASE) {
                this._gamePhase = GamePhase_1.GamePhase.WAITING_FOR_SERVER;
                // Send ready message
                this.setReadyStateToTrue();
                this._gui.onWaitingForServer();
            }
        }, true);
    }
    // Wait for timeout seconds for the client to receive a player stubs update
    // Allows client to animate player movement & gunfire
    waitForPlayerDetailsUpdate(timeout) {
        this.startTimer(timeout, (timeLeft, timeoutID) => {
            /*if(!this._animating) {
                clearTimeout(timeoutID);
                timeoutID = null;
                this.toggleReadyState();
                this._gui.onWaitingForServer();
            }*/
        }, () => {
            this._gamePhase = GamePhase_1.GamePhase.WAITING_FOR_SERVER;
            this.requestDrawMap();
            this.setReadyStateToTrue();
            this._gui.onWaitingForServer();
        });
    }
    // Called when the phase time settings have been received from the server
    onPhaseTimesUpdate(dropTime, moveTime, actionTime) {
        this._dropPhaseTime = parseInt(dropTime, 10) || this._dropPhaseTime;
        this._movePhaseTime = parseInt(moveTime, 10) || this._movePhaseTime;
        this._actionPhaseTime = parseInt(actionTime, 10) || this._actionPhaseTime;
    }
    // Called when a move has been deemed valid
    onMoveValid(newX, newY, crouch) {
        if (this.gamePhase == GamePhase_1.GamePhase.MOVE_PHASE) {
            if (crouch) {
                // NOTE - Reset AP rather than subtract since can clicking resets move turn
                this._myPlayerAP = 100 + this._myPlayerModifiers.totalAP - Action_1.ActionApCosts.CROUCH;
            }
            else {
                // NOTE - Reset AP rather than subtract since can clicking resets move turn
                this._myPlayerAP = 100 + this._myPlayerModifiers.totalAP - Action_1.ActionApCosts.MOVE -
                    (this.myPlayerModifiers.apMoveCost != null ? this.myPlayerModifiers.apMoveCost : 0);
            }
            this._gui.onMoveValid(newX, newY, crouch);
        }
    }
    // Called when an attack has been deemed valid
    onAttackValid() {
        if (this.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE) {
            this._myPlayerAP -= Action_1.ActionApCosts.ATTACK;
            this._gui.onAttackValid({ name: "Attack", apCost: Action_1.ActionApCosts.ATTACK, playerID: -1 });
        }
    }
    // Called when a search has been deemed valid
    onSearchValid() {
        if (this.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE) {
            this._myPlayerAP -= Action_1.ActionApCosts.SEARCH;
            this._gui.onSearchValid({ name: "Search Tile", apCost: Action_1.ActionApCosts.SEARCH });
        }
    }
    // Called when the server has validified a take item request
    onTakeItemValid(entityID, itemID, firstSlotX, firstSlotY) {
        this._myPlayerInventory.addItemEntity(new Item_1.ItemEntity(entityID, itemID), firstSlotX, firstSlotY);
        this._gui.onTakeItemValid(entityID, itemID, firstSlotX, firstSlotY);
    }
    // Called when the server has validified an equip item request
    onEquipItemValid(entityID, itemID) {
        if (itemID < 0 || itemID >= this._items.length ||
            this._myPlayerActions.length >= 5 ||
            this._myPlayerAP - Action_1.ActionApCosts.EQUIP_WEAPON + this._myPlayerModifiers.totalAP < 0) {
            return;
        }
        const action = {
            name: "Equip " + this._items[itemID].name,
            apCost: Action_1.ActionApCosts.EQUIP_WEAPON,
            entityID: entityID,
            itemID: itemID
        };
        this._myPlayerEffectivelyEquippedItem = new Item_1.ItemEntity(entityID, itemID);
        this._myPlayerAP -= Action_1.ActionApCosts.EQUIP_WEAPON;
        this._myPlayerActions.push(action);
        this._gui.onEquipItemValid(action);
    }
    // Called when the server has validifed a drop item request
    onDropItemValid(entityID, itemID) {
        if (itemID < 0 || itemID >= this._items.length) {
            return;
        }
        this._gui.onDropItemValid(entityID, itemID);
    }
    // Called when the server has validified a use bandage request
    onUseBandageValid(entityID, itemID) {
        if (itemID < 0 || itemID >= this._items.length) {
            return;
        }
        this._gui.onUseBandageValid(entityID, itemID);
    }
    // Called when the player initiates an attack on another player
    onInitiateAttackPlayer(playerID) {
        this._gui.onInitiateAttackPlayer(playerID);
    }
    // Called when the player wants to attack a player on a tile
    onAttackPlayerOnTile(x, y, tileX, tileY) {
        this._gui.onAttackPlayerOnTile(x, y, tileX, tileY);
    }
    // Called at end of turn if players have died
    onPlayerDead(playerID) {
        const playerName = this._playerStubs[playerID] ? this._playerStubs[playerID].Name : "Player" + playerID;
        this._gui.onPlayerDead(playerName);
        // If my player died, display You Died message
        if (playerID == this._myID) {
            this._gui.onMyPlayerDead();
            // Spectate the player who killed you
            //    this._myID = playerID;
            this._spectating = true;
        }
    }
    // Called at the end of the game
    onGameOver(winnerIDs) {
        if (winnerIDs.length === 1) {
            if (winnerIDs[0] === this._myID) {
                // My player is the sole winner
                this._gui.onGameOver(winnerIDs, true, false);
            }
            else {
                // My player lost
                this._gui.onGameOver(winnerIDs, false, false);
            }
        }
        else {
            let didMyPlayerDraw = false;
            for (const winnerID of winnerIDs) {
                if (winnerID === this._myID) {
                    didMyPlayerDraw = true;
                }
            }
            if (didMyPlayerDraw) {
                // My player tied with other players
                this._gui.onGameOver(winnerIDs, true, true);
            }
            else {
                // My player lost
                this._gui.onGameOver(winnerIDs, false, false);
            }
        }
    }
    // Called when a storm approach update is received from server
    onStormApproachUpdate() {
        this._gui.onStormApproachUpdate();
    }
    // Called when a storm radius update is received from server
    onStormRadiusUpdate(radius) {
        this._stormRadius = radius;
        this._gui.onStormRadiusUpdate();
    }
    // Called when a storm dmg update is received from server
    onStormDmgUpdate(dmgPerTurn, sickDmgPerTurn) {
        this._stormDmgPerTurn = dmgPerTurn;
        this._stormSickDmgPerTurn = sickDmgPerTurn;
        this._gui.onStormDmgUpdate();
    }
    // Called when the map is finished loading and being initialised
    onMapLoaded() {
        this._gui.onMapLoaded();
    }
    // Called when the item list is received from the server
    onItemsLoaded(items) {
        if (items) {
            this._items = items;
        }
    }
    // Called when a players ready state is changed
    onReadyStateUpdate(playerID, readyState) {
        if (this._playerStubs[playerID]) {
            this._playerStubs[playerID].Ready = readyState;
        }
        this._gui.onReadyStateUpdate(playerID, readyState);
    }
    // Called when this player's ID is received from the server
    onMyIDUpdate(myID) {
        this._myID = !isNaN(parseInt(myID, 10)) ? parseInt(myID, 10) : this._myID;
    }
    // Called when a player disconnects from the game
    onPlayerDisconnect(playerID) {
        const playerName = this.playerStubs[playerID] ?
            this.playerStubs[playerID].Name : `Player ${playerID}`;
        if (this._playerStubs[playerID]) {
            delete this._playerStubs[playerID];
        }
        if (this._playerDetails[playerID]) {
            delete this._playerDetails[playerID];
        }
        this._gui.onPlayerDisconnect(playerName);
    }
    // Called upon receiving a msg from another player
    onMsgReceived(playerID, msg) {
        const playerName = this.playerStubs[playerID] ?
            this.playerStubs[playerID].Name : `Player ${playerID}`;
        this._gui.onMsgReceived(playerName, msg);
    }
    // Called when the player starts dragging an item in their inventory
    onItemDragStart() {
        this._gui.onItemDragStart();
    }
    // Called when the player stops dragging an item in their inventory
    onItemDragEnd() {
        this._gui.onItemDragEnd();
    }
    // Called when the player left or right clicks a bandage in their inventory
    onBandageClick(x, y, entityID) {
        this._gui.onBandageClick(x, y, entityID);
    }
    // Called when the player clicks a limb button
    onHealLimbClick(limb) {
        this._gui.onHealLimbClick(limb);
    }
    // Called when the player clicks the full screen overlay
    onFullOverlayClick() {
        this._gui.onFullOverlayClick();
    }
    // Called when the player begins hovering over a new tile during the drop phase
    onTileHoverDropPhase(i, j, x, y) {
        this._gui.onTileHoverDropPhase(i, j, x, y);
    }
    // Called when the player begins hovering over a new tile during the move phase
    onTileHoverMovePhase(i, j, x, y) {
        this._gui.onTileHoverMovePhase(i, j, x, y);
    }
    // Called when the player begins hovering over a new tile during the action phase
    onTileHoverActionPhase(name, desc, apCost, x, y) {
        this._gui.onTileHoverActionPhase(name, desc, apCost, x, y);
    }
    // Called when the player clicks a tile during the action phase
    onTileRightClickedActionPhase(x, y, tileX, tileY, canSearch, canAttack) {
        this._gui.onTileClickedActionPhase(x, y, tileX, tileY, canSearch, canAttack);
    }
    // Called when the player begins hovering over an item
    onItemHover(entity, x, y) {
        this._gui.onItemHover(entity, x, y);
    }
    // Called when the player hovers an item memento
    onItemMementoHover(mem, x, y) {
        this._gui.onItemMementoHover(mem, x, y);
    }
    // Resize parts of the GUI when the window resizes
    onWindowResize() {
        this._gui.onWindowResize();
    }
}
// Uses touchstart on mobile devices and click on desktop
Controller.CLICK_EVENT = "ontouchend" in window ? "touchend" : "click";
exports.Controller = Controller;


/***/ }),

/***/ "./client_src/Main.ts":
/*!****************************!*\
  !*** ./client_src/Main.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
Licensing Info

Copyright (c) James Sugden 2018



ws, httphead licensed under
================================================================================
The MIT License (MIT)
================================================================================

Copyright (c) 2017 Sergey Kamardin <gobwas@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



js-cookie licensed under
================================================================================
MIT License
================================================================================

Copyright (c) 2018 Copyright 2018 Klaus Hartl, Fagner Brack, GitHub Contributors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/
Object.defineProperty(exports, "__esModule", { value: true });
const Controller_1 = __webpack_require__(/*! ./Controller */ "./client_src/Controller.ts");
let controller;
function main() {
    controller = new Controller_1.Controller();
}
// When the window resizes, update the size of the canvas
window.addEventListener("resize", () => {
    controller.onWindowResize();
});
// Wait until the page is fully loaded before doing anything
window.addEventListener("load", () => {
    main();
});


/***/ }),

/***/ "./client_src/game/Action.ts":
/*!***********************************!*\
  !*** ./client_src/game/Action.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.ActionApCosts = {
    CROUCH: 0,
    MOVE: 40,
    ATTACK: 60,
    EQUIP_WEAPON: 5,
    BANDAGE: 30,
    SEARCH: 20
};


/***/ }),

/***/ "./client_src/game/GamePhase.ts":
/*!**************************************!*\
  !*** ./client_src/game/GamePhase.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var GamePhase;
(function (GamePhase) {
    GamePhase[GamePhase["LOBBY"] = 0] = "LOBBY";
    GamePhase[GamePhase["DROP_PHASE"] = 1] = "DROP_PHASE";
    GamePhase[GamePhase["MOVE_PHASE"] = 2] = "MOVE_PHASE";
    GamePhase[GamePhase["ACTION_PHASE"] = 3] = "ACTION_PHASE";
    GamePhase[GamePhase["WAITING_FOR_SERVER"] = 4] = "WAITING_FOR_SERVER";
})(GamePhase = exports.GamePhase || (exports.GamePhase = {}));


/***/ }),

/***/ "./client_src/game/HitMap.ts":
/*!***********************************!*\
  !*** ./client_src/game/HitMap.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class HitMap {
    constructor() {
        // NOTE - use 2 separate maps for faster memento lookup
        // Map from color to tile (x, y)
        this._tileColorMap = {};
        // Map from color to item memento (mem)
        this._mementoColorMap = {};
    }
    // add an item memento to the color map
    // returns the color key as a string
    addItemMemento(m) {
        if (m) {
            const colorKey = this.genUniqueRandomColor();
            this._mementoColorMap[colorKey] = { mem: m };
            return colorKey;
        }
        return null;
    }
    // remove an item memento from the color map
    removeItemMemento(m) {
        for (const color in this._mementoColorMap) {
            const mem = this._mementoColorMap[color].mem;
            if (mem === m) {
                delete this._mementoColorMap[color];
                break;
            }
        }
    }
    // add a tile to the color map
    // returns the color key as a string
    addTile(x, y) {
        const colorKey = this.genUniqueRandomColor();
        this._tileColorMap[colorKey] = {
            x: x,
            y: y
        };
        return colorKey;
    }
    // get the tile or item memento associated with the given color
    get(colorKey) {
        if (this._tileColorMap[colorKey]) {
            return this._tileColorMap[colorKey];
        }
        return this._mementoColorMap[colorKey];
    }
    // generate a unique random color
    // sourced from https://blog.lavrton.com/hit-region-detection-for-html5-canvas-and-how-to-listen-to-click-events-on-canvas-shapes-815034d7e9f8
    genUniqueRandomColor() {
        // keep generating random colors until unique color is found
        var colorKey;
        do {
            const r = Math.round(Math.random() * 255);
            const g = Math.round(Math.random() * 255);
            const b = Math.round(Math.random() * 255);
            colorKey = `rgb(${r},${g},${b})`;
        } while (this._tileColorMap[colorKey] || this._mementoColorMap[colorKey]);
        return colorKey;
    }
}
exports.HitMap = HitMap;


/***/ }),

/***/ "./client_src/game/Inventory.ts":
/*!**************************************!*\
  !*** ./client_src/game/Inventory.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Inventory {
    constructor(controller) {
        this.controller = controller;
        // Initialise item slots as a grid full of null item entities
        this._itemSlots = new Array();
        for (let j = 0; j < Inventory.COL_HEIGHT; j++) {
            this._itemSlots.push(new Array());
            for (let i = 0; i < Inventory.ROW_LENGTH; i++) {
                this._itemSlots[j].push(null);
            }
        }
    }
    get itemSlots() {
        return this._itemSlots;
    }
    // Add an item entity to the players inventory
    // Returns true iff the item is added successfully
    addItemEntity(itemEntity, firstSlotX, firstSlotY) {
        if (!itemEntity) {
            return false;
        }
        const item = this.controller.items[itemEntity.itemID];
        // Check item exists
        if (!item) {
            return false;
        }
        // Bounds check
        if (firstSlotX < 0 || firstSlotX + item.gridWidth > Inventory.ROW_LENGTH ||
            firstSlotY < 0 || firstSlotY + item.gridHeight > Inventory.COL_HEIGHT) {
            return false;
        }
        // If all slots required by the item are free, add the entity to the inventory
        // TODO - add support for rotated item entities
        for (let slotX = firstSlotX; slotX < firstSlotX + item.gridWidth; slotX++) {
            for (let slotY = firstSlotY; slotY < firstSlotY + item.gridHeight; slotY++) {
                this._itemSlots[slotY][slotX] = itemEntity;
            }
        }
        // Return true as item entity successfully added
        return true;
    }
    removeItemEntity(itemEntity) {
    }
}
Inventory.ROW_LENGTH = 3;
Inventory.COL_HEIGHT = 4;
exports.Inventory = Inventory;


/***/ }),

/***/ "./client_src/game/Item.ts":
/*!*********************************!*\
  !*** ./client_src/game/Item.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function isGun(item) {
    return "shotsPerAction" in item;
}
exports.isGun = isGun;
function isProjectile(item) {
    return "timeToThrow" in item;
}
exports.isProjectile = isProjectile;
function isBandage(item) {
    return "healAmount" in item;
}
exports.isBandage = isBandage;
class ItemEntity {
    constructor(entityID, itemID) {
        this.entityID = entityID;
        this.itemID = itemID;
    }
}
exports.ItemEntity = ItemEntity;


/***/ }),

/***/ "./client_src/game/ItemMemento.ts":
/*!****************************************!*\
  !*** ./client_src/game/ItemMemento.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class ItemMemento {
    constructor(_mapX, _mapY) {
        this._mapX = _mapX;
        this._mapY = _mapY;
        this._items = new Array();
        this._since = 0;
    }
    get mapX() {
        return this._mapX;
    }
    get mapY() {
        return this._mapY;
    }
    get since() {
        return this._since;
    }
    incSince() {
        this._since++;
    }
    reset() {
        this._since = 0;
    }
    get items() {
        return this._items;
    }
    set items(i) {
        if (i) {
            this._items = i;
        }
    }
    get colorKey() {
        return this._colorKey;
    }
    set colorKey(s) {
        if (s) {
            this._colorKey = s;
        }
    }
}
exports.ItemMemento = ItemMemento;


/***/ }),

/***/ "./client_src/game/Map.ts":
/*!********************************!*\
  !*** ./client_src/game/Map.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class Map {
    // map from color to tile (x, y)
    //private _tileColorMap = {};
    constructor(controller, text) {
        this.controller = controller;
        this.initMap(text);
    }
    initMap(text) {
        // initialise the map array
        this._hexGrid = [];
        // split into lines (rows)
        const lines = text.split("\n");
        let y = 0;
        for (const line of lines) {
            this._hexGrid.push([]);
            // split into tiles
            const tiles = line.split(" ");
            let x = 0;
            for (const tile of tiles) {
                /*var colorKey = this.genRandomColor();
                // key generating random colors until unique color is found
                while(this._tileColorMap[colorKey]) {
                    colorKey = this.genRandomColor();
                }*/
                // set the tile object & add tile to hit map
                this._hexGrid[this._hexGrid.length - 1].push({
                    type: parseInt(tile) || 0,
                    colorKey: this.controller.hitMap.addTile(x, y)
                });
                // add tile to color map
                /*this._tileColorMap[colorKey] = {
                    x: x,
                    y: y
                };*/
                x++;
            }
            y++;
        }
    }
    // generate a random color
    // sourced from https://blog.lavrton.com/hit-region-detection-for-html5-canvas-and-how-to-listen-to-click-events-on-canvas-shapes-815034d7e9f8
    /*private genRandomColor(): string {
        const r = Math.round(Math.random() * 255);
        const g = Math.round(Math.random() * 255);
        const b = Math.round(Math.random() * 255);
        return `rgb(${r},${g},${b})`;
    }*/
    get hexGrid() {
        return this._hexGrid;
    }
}
exports.Map = Map;


/***/ }),

/***/ "./client_src/game/TileType.ts":
/*!*************************************!*\
  !*** ./client_src/game/TileType.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class TileType {
    constructor(_name, _color, _desc, _img = null, _impassable = false, _undroppable = false, _blocksVision = false) {
        this._name = _name;
        this._color = _color;
        this._desc = _desc;
        this._img = _img;
        this._impassable = _impassable;
        this._undroppable = _undroppable;
        this._blocksVision = _blocksVision;
    }
    // TODO - make tileset map specific using map.meta file
    static init() {
        this._types = [
            new TileType("Water", "#40a4df", ["Requires boat to travel"], null, true, true),
            new TileType("Bog", "#395D33", ["Vehicles get stuck"]),
            new TileType("Grassland", "#308014", ["May contain mines"]),
            new TileType("Forest", "#308014", ["Provides cover from enemies", "Greater chance to find items"], new Image(), false, false, true),
            new TileType("Hills", "#308014", ["Up High (+1 View Range)"], new Image()),
            new TileType("Moutains", "#808080", ["Will probably block vision when I figure out how to get that to work"], new Image(), true, true, true),
            new TileType("House", "#7f0000", ["Provides cover from enemies", "Greater chance to find items"], new Image(), false, true),
            new TileType("City", "#7f0000", ["Provides cover from enemies", "Greater chance to find items", "Up High (+1 View Range)"], new Image(), false, true, true),
            new TileType("Bridge", "#40a4df", ["Allows land-based travel over water"], new Image())
        ];
        // yuck
        this._types[3].img.src = "assets/Tree (512x512).png";
        this._types[4].img.src = "assets/Hill (512x512).png";
        this._types[5].img.src = "assets/Mountain (512x512).png";
        this._types[6].img.src = "assets/House (512x512).png";
        this._types[7].img.src = "assets/Skyscraper (512x512).png";
        this._types[8].img.src = "assets/Bridge (512x512).png";
    }
    get name() {
        return this._name;
    }
    get color() {
        return this._color;
    }
    get desc() {
        return this._desc;
    }
    get img() {
        return this._img;
    }
    get impassable() {
        return this._impassable;
    }
    get undroppable() {
        return this._undroppable;
    }
    get blocksVision() {
        return this._blocksVision;
    }
    static get types() {
        return TileType._types;
    }
}
exports.TileType = TileType;
TileType.init();


/***/ }),

/***/ "./client_src/gui/ActionList.ts":
/*!**************************************!*\
  !*** ./client_src/gui/ActionList.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class ActionList {
    constructor(controller) {
        this.controller = controller;
        this.actionListContainer = document.getElementById("action-list-container");
        this.actionList = document.getElementById("action-list");
    }
    appendAction(action, mousex, mousey) {
        let titleLbl = document.createElement("label");
        titleLbl.innerHTML = action.name;
        titleLbl.style.margin = "5px";
        titleLbl.style.fontSize = "2vw";
        let apLbl = document.createElement("label");
        apLbl.innerHTML = `${action.apCost}AP`;
        apLbl.style.color = "#0000b2";
        apLbl.style.margin = "5px";
        apLbl.style.fontSize = "2vw";
        const totalAPLbl = document.getElementById("action-list-ap-lbl");
        totalAPLbl.innerHTML = `${this.controller.myPlayerAP}AP`;
        this.actionList.appendChild(document.createElement("br"));
        this.actionList.appendChild(titleLbl);
        this.actionList.appendChild(apLbl);
    }
    resetActionList() {
        // Remove all elements from the div
        while (this.actionList.firstChild) {
            this.actionList.removeChild(this.actionList.firstChild);
        }
        // Re-add the title and AP label
        /*let titleLbl = document.createElement("label");
        titleLbl.innerHTML = "Actions";
        titleLbl.style.margin = "0.25em";

        let apLbl = document.createElement("label");
        apLbl.id = "total-ap-lbl";
        apLbl.innerHTML = `${this.controller.myPlayerAP}AP`;
        apLbl.style.color = "#0000b2";
        apLbl.style.margin = "0.25em";

        this.actionList.appendChild(titleLbl);
        this.actionList.appendChild(apLbl);
        this.actionList.style.visibility = "visible";
        this.actionList.style.display = "block";*/
        this.actionListContainer.style.visibility = "visible";
    }
    hide() {
        this.actionListContainer.style.visibility = "hidden";
    }
}
exports.ActionList = ActionList;


/***/ }),

/***/ "./client_src/gui/AttackCharDiv.ts":
/*!*****************************************!*\
  !*** ./client_src/gui/AttackCharDiv.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class AttackCharDiv {
    constructor(controller) {
        this.controller = controller;
        this.onCancelBtnClick = (e) => {
            this.hide();
        };
        this.onAttackHeadBtnClick = (e) => {
            this.controller.attackPlayer(this.playerID, "head");
            this.hide();
        };
        this.onAttackChestBtnClick = (e) => {
            this.controller.attackPlayer(this.playerID, "chest");
            this.hide();
        };
        this.onAttackRArmBtnClick = (e) => {
            this.controller.attackPlayer(this.playerID, "rarm");
            this.hide();
        };
        this.onAttackLArmBtnClick = (e) => {
            this.controller.attackPlayer(this.playerID, "larm");
            this.hide();
        };
        this.onAttackRLegBtnClick = (e) => {
            this.controller.attackPlayer(this.playerID, "rleg");
            this.hide();
        };
        this.onAttackLLegBtnClick = (e) => {
            this.controller.attackPlayer(this.playerID, "lleg");
            this.hide();
        };
        this.attackCharDiv = document.getElementById("attack-char-div");
        this.attackCharLbl = document.getElementById("attack-char-lbl");
        this.attackCharCancelBtn = document.getElementById("attack-char-cancel-btn");
        this.attackHeadBtn = document.getElementById("attack-head-btn");
        this.attackChestBtn = document.getElementById("attack-chest-btn");
        this.attackRArmBtn = document.getElementById("attack-rarm-btn");
        this.attackLArmBtn = document.getElementById("attack-larm-btn");
        this.attackRLegBtn = document.getElementById("attack-rleg-btn");
        this.attackLLegBtn = document.getElementById("attack-lleg-btn");
        this.attackHeadHPBar = document.getElementById("attack-head-hp-bar");
        this.attackChestHPBar = document.getElementById("attack-chest-hp-bar");
        this.attackRArmHPBar = document.getElementById("attack-rarm-hp-bar");
        this.attackLArmHPBar = document.getElementById("attack-larm-hp-bar");
        this.attackRLegHPBar = document.getElementById("attack-rleg-hp-bar");
        this.attackLLegHPBar = document.getElementById("attack-lleg-hp-bar");
        this.attackHeadHPLbl = document.getElementById("attack-head-hp-lbl");
        this.attackChestHPLbl = document.getElementById("attack-chest-hp-lbl");
        this.attackRArmHPLbl = document.getElementById("attack-rarm-hp-lbl");
        this.attackLArmHPLbl = document.getElementById("attack-larm-hp-lbl");
        this.attackRLegHPLbl = document.getElementById("attack-rleg-hp-lbl");
        this.attackLLegHPLbl = document.getElementById("attack-lleg-hp-lbl");
        this.attackHeadHitChanceLbl = document.getElementById("attack-head-hit-chance-lbl");
        this.attackChestHitChanceLbl = document.getElementById("attack-chest-hit-chance-lbl");
        this.attackRArmHitChanceLbl = document.getElementById("attack-rarm-hit-chance-lbl");
        this.attackLArmHitChanceLbl = document.getElementById("attack-larm-hit-chance-lbl");
        this.attackRLegHitChanceLbl = document.getElementById("attack-rleg-hit-chance-lbl");
        this.attackLLegHitChanceLbl = document.getElementById("attack-lleg-hit-chance-lbl");
        this.attackCharCancelBtn.addEventListener("click", this.onCancelBtnClick);
        this.attackHeadBtn.addEventListener("click", this.onAttackHeadBtnClick);
        this.attackChestBtn.addEventListener("click", this.onAttackChestBtnClick);
        this.attackRArmBtn.addEventListener("click", this.onAttackRArmBtnClick);
        this.attackLArmBtn.addEventListener("click", this.onAttackLArmBtnClick);
        this.attackRLegBtn.addEventListener("click", this.onAttackRLegBtnClick);
        this.attackLLegBtn.addEventListener("click", this.onAttackLLegBtnClick);
    }
    show() {
        this.attackCharDiv.style.visibility = "visible";
    }
    hide() {
        this.attackCharDiv.style.visibility = "hidden";
    }
    refreshDisplay(playerID) {
        this.playerID = playerID;
        this.calcHitChances();
        this.show();
    }
    calcHitChances() {
        const p = this.controller.myPlayerDetails;
        const target = this.controller.playerDetails[this.playerID];
        // TODO - this range doesn't really work with the hexgrid system
        const rangeX = Math.abs(p.x - target.x);
        const rangeY = Math.abs(p.y - target.y);
        const rangeD = Math.sqrt(rangeX * rangeX + rangeY * rangeY) + 1;
        const e = this.controller.myPlayerEquippedItem;
        let dmgPerHit;
        let shotsPerAction;
        let accuracy;
        let weaponEquipped = false;
        if (e != null) {
            const gun = this.controller.items[e.itemID];
            if (gun) {
                // If a weapon is equipped, use weapon stats
                dmgPerHit = gun.dmgPerHit;
                shotsPerAction = gun.shotsPerAction;
                accuracy = gun.accuracy;
                weaponEquipped = true;
            }
        }
        if (!weaponEquipped) {
            // If no weapon equipped, simulate punching
            dmgPerHit = 5;
            shotsPerAction = 1;
            accuracy = 0.6;
        }
        // Calculate the hit chance as a whole numbered percentage
        const hitChance = Math.floor(100 / rangeD * accuracy * (1 + this.controller.myPlayerModifiers.hitChance / 100) + 0.5);
        // Display the hit chance below the health bars
        this.attackHeadHitChanceLbl.innerHTML = "" + hitChance + "% To Hit";
        this.attackChestHitChanceLbl.innerHTML = "" + hitChance + "% To Hit";
        this.attackRArmHitChanceLbl.innerHTML = "" + hitChance + "% To Hit";
        this.attackLArmHitChanceLbl.innerHTML = "" + hitChance + "% To Hit";
        this.attackRLegHitChanceLbl.innerHTML = "" + hitChance + "% To Hit";
        this.attackLLegHitChanceLbl.innerHTML = "" + hitChance + "% To Hit";
        // Display the player's name in the title label
        this.attackCharLbl.innerHTML = "Attack " + (this.controller.playerStubs[this.playerID] ?
            this.controller.playerStubs[this.playerID].Name : "Player" + this.playerID);
        // Set value of HP as percentage width of span
        const hp = this.controller.playerHPs[this.playerID];
        if (hp) {
            this.attackHeadHPBar.style.width = `${hp.head < 0 ? 0 : hp.head}%`;
            this.attackChestHPBar.style.width = `${hp.chest < 0 ? 0 : hp.chest}%`;
            this.attackRArmHPBar.style.width = `${hp.rarm < 0 ? 0 : hp.rarm}%`;
            this.attackLArmHPBar.style.width = `${hp.larm < 0 ? 0 : hp.larm}%`;
            this.attackRLegHPBar.style.width = `${hp.rleg < 0 ? 0 : hp.rleg}%`;
            this.attackLLegHPBar.style.width = `${hp.lleg < 0 ? 0 : hp.lleg}%`;
            // Set label of hp bar to be the value of hp
            this.attackHeadHPLbl.innerHTML = "" + (hp.head < 0 ? 0 : hp.head);
            this.attackChestHPLbl.innerHTML = "" + (hp.chest < 0 ? 0 : hp.chest);
            this.attackRArmHPLbl.innerHTML = "" + (hp.rarm < 0 ? 0 : hp.rarm);
            this.attackLArmHPLbl.innerHTML = "" + (hp.larm < 0 ? 0 : hp.larm);
            this.attackRLegHPLbl.innerHTML = "" + (hp.rleg < 0 ? 0 : hp.rleg);
            this.attackLLegHPLbl.innerHTML = "" + (hp.lleg < 0 ? 0 : hp.lleg);
            if (hp.head <= 30) {
                this.attackHeadHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.attackHeadHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (hp.chest <= 30) {
                this.attackChestHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.attackChestHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (hp.rarm <= 30) {
                this.attackRArmHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.attackRArmHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (hp.larm <= 30) {
                this.attackLArmHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.attackLArmHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (hp.rleg <= 30) {
                this.attackRLegHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.attackRLegHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (hp.lleg <= 30) {
                this.attackLLegHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.attackLLegHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
        }
    }
}
exports.AttackCharDiv = AttackCharDiv;


/***/ }),

/***/ "./client_src/gui/CenterLbl.ts":
/*!*************************************!*\
  !*** ./client_src/gui/CenterLbl.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class CenterLbl {
    constructor(controller) {
        this.controller = controller;
        this.centerLbl = document.getElementById("center-lbl");
    }
    show(msg, fadeInAndOut = false) {
        this.centerLbl.innerHTML = msg;
        if (fadeInAndOut) {
            this.centerLbl.style.visibility = "hidden";
            this.centerLbl.classList.remove("fade-in");
            this.centerLbl.classList.add("fade-in-out");
        }
        else {
            this.centerLbl.style.visibility = "visible";
            this.centerLbl.classList.remove("fade-in-out");
            this.centerLbl.classList.add("fade-in");
        }
    }
    hide() {
        this.centerLbl.style.visibility = "hidden";
    }
}
exports.CenterLbl = CenterLbl;


/***/ }),

/***/ "./client_src/gui/CharacterPanel.ts":
/*!******************************************!*\
  !*** ./client_src/gui/CharacterPanel.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Controller_1 = __webpack_require__(/*! ../Controller */ "./client_src/Controller.ts");
const GamePhase_1 = __webpack_require__(/*! ../game/GamePhase */ "./client_src/game/GamePhase.ts");
class CharacterPanel {
    constructor(controller) {
        this.controller = controller;
        this.bloodSplatImgs = ["assets/Blood-Splat-1.png", "assets/Blood-Splat-2.png", "assets/Blood-Splat-3.png"];
        this.onHeadBtnClick = (e) => {
            if (this.controller.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.head < 100 && this.controller.myPlayerHP.head > 0) {
                this.controller.onHealLimbClick("head");
            }
        };
        this.onChestBtnClick = (e) => {
            if (this.controller.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.chest < 100 && this.controller.myPlayerHP.chest > 0) {
                this.controller.onHealLimbClick("chest");
            }
        };
        this.onRArmBtnClick = (e) => {
            if (this.controller.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.rarm < 100 && this.controller.myPlayerHP.rarm > 0) {
                this.controller.onHealLimbClick("rarm");
            }
        };
        this.onLArmBtnClick = (e) => {
            if (this.controller.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.larm < 100 && this.controller.myPlayerHP.larm > 0) {
                this.controller.onHealLimbClick("larm");
            }
        };
        this.onRLegBtnClick = (e) => {
            if (this.controller.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.rleg < 100 && this.controller.myPlayerHP.rleg > 0) {
                this.controller.onHealLimbClick("rleg");
            }
        };
        this.onLLegBtnClick = (e) => {
            if (this.controller.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.lleg < 100 && this.controller.myPlayerHP.lleg > 0) {
                this.controller.onHealLimbClick("lleg");
            }
        };
        this.charDiv = document.getElementById("char-div");
        this.charLbl = document.getElementById("char-lbl");
        this.headBtn = document.getElementById("head-btn");
        this.chestBtn = document.getElementById("chest-btn");
        this.rArmBtn = document.getElementById("rarm-btn");
        this.lArmBtn = document.getElementById("larm-btn");
        this.rLegBtn = document.getElementById("rleg-btn");
        this.lLegBtn = document.getElementById("lleg-btn");
        this.totalHPBar = document.getElementById("total-hp-bar");
        this.headHPBar = document.getElementById("head-hp-bar");
        this.chestHPBar = document.getElementById("chest-hp-bar");
        this.rarmHPBar = document.getElementById("rarm-hp-bar");
        this.larmHPBar = document.getElementById("larm-hp-bar");
        this.rlegHPBar = document.getElementById("rleg-hp-bar");
        this.llegHPBar = document.getElementById("lleg-hp-bar");
        this.headHPLbl = document.getElementById("head-hp-lbl");
        this.chestHPLbl = document.getElementById("chest-hp-lbl");
        this.rarmHPLbl = document.getElementById("rarm-hp-lbl");
        this.larmHPLbl = document.getElementById("larm-hp-lbl");
        this.rlegHPLbl = document.getElementById("rleg-hp-lbl");
        this.llegHPLbl = document.getElementById("lleg-hp-lbl");
        this.headBloodSplatImg = document.getElementById("head-blood-splat-img");
        this.chestBloodSplatImg = document.getElementById("chest-blood-splat-img");
        this.rarmBloodSplatImg = document.getElementById("rarm-blood-splat-img");
        this.larmBloodSplatImg = document.getElementById("larm-blood-splat-img");
        this.rlegBloodSplatImg = document.getElementById("rleg-blood-splat-img");
        this.llegBloodSplatImg = document.getElementById("lleg-blood-splat-img");
        this.headBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onHeadBtnClick);
        this.chestBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onChestBtnClick);
        this.rArmBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onRArmBtnClick);
        this.lArmBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onLArmBtnClick);
        this.rLegBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onRLegBtnClick);
        this.lLegBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onLLegBtnClick);
    }
    refreshDisplay() {
        const myPlayerHP = this.controller.myPlayerHP;
        if (myPlayerHP) {
            this.charLbl.innerHTML = `Total HP <span style="color: ${myPlayerHP.total <= 30 ? "#0040ff" : "#0040ff"}">
                    ${myPlayerHP.total < 0 ? 0 : myPlayerHP.total}</span>`;
            // Set value of total HP as percentage of height
            this.totalHPBar.style.height = `${myPlayerHP.total < 0 ? 0 : myPlayerHP.total}%`;
            // Set value of HP as percentage width of span
            this.headHPBar.style.width = `${myPlayerHP.head < 0 ? 0 : myPlayerHP.head}%`;
            this.chestHPBar.style.width = `${myPlayerHP.chest < 0 ? 0 : myPlayerHP.chest}%`;
            this.rarmHPBar.style.width = `${myPlayerHP.rarm < 0 ? 0 : myPlayerHP.rarm}%`;
            this.larmHPBar.style.width = `${myPlayerHP.larm < 0 ? 0 : myPlayerHP.larm}%`;
            this.rlegHPBar.style.width = `${myPlayerHP.rleg < 0 ? 0 : myPlayerHP.rleg}%`;
            this.llegHPBar.style.width = `${myPlayerHP.lleg < 0 ? 0 : myPlayerHP.lleg}%`;
            // Set label of hp bar to be the value of hp
            this.headHPLbl.innerHTML = "" + (myPlayerHP.head < 0 ? 0 : myPlayerHP.head);
            this.chestHPLbl.innerHTML = "" + (myPlayerHP.chest < 0 ? 0 : myPlayerHP.chest);
            this.rarmHPLbl.innerHTML = "" + (myPlayerHP.rarm < 0 ? 0 : myPlayerHP.rarm);
            this.larmHPLbl.innerHTML = "" + (myPlayerHP.larm < 0 ? 0 : myPlayerHP.larm);
            this.rlegHPLbl.innerHTML = "" + (myPlayerHP.rleg < 0 ? 0 : myPlayerHP.rleg);
            this.llegHPLbl.innerHTML = "" + (myPlayerHP.lleg < 0 ? 0 : myPlayerHP.lleg);
            // Change color of hp bars to red if below 30% hp
            if (myPlayerHP.total <= 30) {
                this.totalHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.totalHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (myPlayerHP.head <= 30) {
                this.headHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.headHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (myPlayerHP.chest <= 30) {
                this.chestHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.chestHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (myPlayerHP.rarm <= 30) {
                this.rarmHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.rarmHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (myPlayerHP.larm <= 30) {
                this.larmHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.larmHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (myPlayerHP.rleg <= 30) {
                this.rlegHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.rlegHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
            if (myPlayerHP.lleg <= 30) {
                this.llegHPBar.classList.add("limb-btn-hp-bar-injured");
            }
            else {
                this.llegHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
        }
    }
    showBloodSplatHead() {
        const splat = this.getRandomSplat();
        this.headBloodSplatImg.src = splat;
        this.headBloodSplatImg.classList.remove("fade-out");
        this.headBloodSplatImg.classList.add("fade-out");
    }
    showBloodSplatChest() {
        const splat = this.getRandomSplat();
        this.chestBloodSplatImg.src = splat;
        this.chestBloodSplatImg.classList.remove("fade-out");
        this.chestBloodSplatImg.classList.add("fade-out");
    }
    showBloodSplatRArm() {
        const splat = this.getRandomSplat();
        this.rarmBloodSplatImg.src = splat;
        this.rarmBloodSplatImg.classList.remove("fade-out");
        this.rarmBloodSplatImg.classList.add("fade-out");
    }
    showBloodSplatLArm() {
        const splat = this.getRandomSplat();
        this.larmBloodSplatImg.src = splat;
        this.larmBloodSplatImg.classList.remove("fade-out");
        this.larmBloodSplatImg.classList.add("fade-out");
    }
    showBloodSplatRLeg() {
        const splat = this.getRandomSplat();
        this.rlegBloodSplatImg.src = splat;
        this.rlegBloodSplatImg.classList.remove("fade-out");
        this.rlegBloodSplatImg.classList.add("fade-out");
    }
    showBloodSplatLLeg() {
        const splat = this.getRandomSplat();
        this.llegBloodSplatImg.src = splat;
        this.llegBloodSplatImg.classList.remove("fade-out");
        this.llegBloodSplatImg.classList.add("fade-out");
    }
    getRandomSplat() {
        return this.bloodSplatImgs[Math.floor(Math.random() * this.bloodSplatImgs.length)];
    }
}
exports.CharacterPanel = CharacterPanel;


/***/ }),

/***/ "./client_src/gui/ChatWindow.ts":
/*!**************************************!*\
  !*** ./client_src/gui/ChatWindow.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class ChatWindow {
    constructor(controller) {
        this.controller = controller;
        this.onSendBtnClick = (e) => {
            this.sendMsg(this.chatInput.value);
        };
        this.onInputEntered = (e) => {
            console.log("input entered");
            if (e.keyCode == 13) {
                console.log("enter entered");
                e.preventDefault();
                this.sendMsg(this.chatInput.value);
            }
        };
        this.chatWindow = document.getElementById("chat-window");
        this.chatLog = document.getElementById("chat-log");
        this.chatInput = document.getElementById("chat-input");
        this.chatSendBtn = document.getElementById("chat-send-btn");
        //this.chatSendBtn.addEventListener(Controller.CLICK_EVENT, this.onSendBtnClick);
        this.chatInput.addEventListener("keyup", this.onInputEntered);
    }
    appendMsg(msg) {
        let lbl = document.createElement("p");
        lbl.innerHTML = msg;
        lbl.classList.add("chat-window-item");
        this.chatLog.appendChild(lbl);
        //this.chatWindow.insertBefore(lbl, this.chatWindow.lastChild);
        //this.chatWindow.appendChild(lbl);
    }
    sendMsg(msg) {
        if (msg) {
            this.controller.sendMsg(msg);
            this.chatInput.value = "";
        }
    }
}
exports.ChatWindow = ChatWindow;


/***/ }),

/***/ "./client_src/gui/DropdownDiv.ts":
/*!***************************************!*\
  !*** ./client_src/gui/DropdownDiv.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Controller_1 = __webpack_require__(/*! ../Controller */ "./client_src/Controller.ts");
class DropdownDiv {
    constructor(controller) {
        this.controller = controller;
        this.dropdownDiv = document.getElementById("dropdown-div");
    }
    hide() {
        this.dropdownDiv.style.visibility = "hidden";
    }
    show(x, y) {
        // set the position of the tooltip
        this.dropdownDiv.style.left = `calc(${x}px - ${this.dropdownDiv.clientWidth / 2}px)`;
        this.dropdownDiv.style.top = `${y}px`;
        // make sure the tooltip is visible
        this.dropdownDiv.style.visibility = "visible";
    }
    clear() {
        // clear contents of div
        while (this.dropdownDiv.firstChild) {
            this.dropdownDiv.removeChild(this.dropdownDiv.firstChild);
        }
    }
    showUseBandageList(x, y, entityID) {
        const hp = this.controller.myPlayerHP;
        if (x >= 0 && y >= 0 && hp) {
            this.clear();
            const $this = this;
            function onClick(limb) {
                $this.controller.useBandage(entityID, limb);
            }
            // set the contents of the dropdown
            if (hp.head > 0 && hp.head < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Head";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller_1.Controller.CLICK_EVENT, (e) => { onClick("head"); });
            }
            if (hp.chest > 0 && hp.chest < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Chest";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller_1.Controller.CLICK_EVENT, (e) => { onClick("chest"); });
            }
            if (hp.rarm > 0 && hp.rarm < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Right Arm";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller_1.Controller.CLICK_EVENT, (e) => { onClick("rarm"); });
            }
            if (hp.larm > 0 && hp.larm < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Left Arm";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller_1.Controller.CLICK_EVENT, (e) => { onClick("larm"); });
            }
            if (hp.rleg > 0 && hp.rleg < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Right Leg";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller_1.Controller.CLICK_EVENT, (e) => { onClick("rleg"); });
            }
            if (hp.lleg > 0 && hp.lleg < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Left Leg";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller_1.Controller.CLICK_EVENT, (e) => { onClick("lleg"); });
            }
            this.show(x, y);
        }
        else {
            this.hide();
        }
    }
    showTileActionList(x, y, tileX, tileY, canSearch = false, canAttack = false, canThrowGrenade = false) {
        if (x >= 0 && y >= 0) {
            this.clear();
            const $this = this;
            function onAttackClick() {
                const playerIDs = $this.controller.getPlayersOnTile(tileX, tileY);
                if (playerIDs.length === 1) {
                    // If there is only one player on the tile, then attack the player
                    $this.controller.onInitiateAttackPlayer(playerIDs[0]);
                }
                else if (playerIDs.length > 1) {
                    // If there are more than players on the tile, allow my player to choose
                    $this.showAttackPlayerList(x, y, tileX, tileY);
                }
            }
            // set the contents of the dropdown
            if (canAttack) {
                let btn = document.createElement("button");
                let img = document.createElement("img");
                let span = document.createElement("span");
                img.src = "assets/target_icon.png";
                img.style.display = "inline-block";
                span.innerHTML = "Attack";
                span.style.display = "table-cell";
                btn.appendChild(img);
                btn.appendChild(span);
                this.dropdownDiv.appendChild(btn);
            }
            // set the contents of the dropdown
            if (canSearch) {
                let btn = document.createElement("button");
                let img = document.createElement("img");
                let span = document.createElement("span");
                img.src = "assets/search_icon.png";
                img.style.display = "inline-block";
                img.style.height = "100%";
                span.innerHTML = "Search";
                span.style.display = "inline-block";
                span.style.height = "100%";
                span.style.verticalAlign = "middle";
                btn.appendChild(img);
                btn.appendChild(span);
                this.dropdownDiv.appendChild(btn);
            }
            // set the contents of the tooltip
            /*if(canThrowGrenade) {
                let lbl = document.createElement("button");
                lbl.innerHTML = "Grenade"
                this.tileActionList.appendChild(lbl);
            }*/
            this.show(x, y);
        }
        else {
            this.hide();
        }
    }
    showAttackPlayerList(x, y, tileX, tileY) {
        if (x >= 0 && y >= 0) {
            this.clear();
            const $this = this;
            function onClick(playerID) {
                $this.controller.onInitiateAttackPlayer(playerID);
            }
            const players = this.controller.getPlayersOnTile(tileX, tileY);
            for (const playerID of players) {
                let btn = document.createElement("button");
                const stub = this.controller.playerStubs[playerID];
                if (stub) {
                    btn.innerHTML = stub.Name;
                }
                else {
                    btn.innerHTML = "Player" + playerID;
                }
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller_1.Controller.CLICK_EVENT, (e) => { onClick(playerID); });
            }
            this.show(x, y);
        }
        else {
            this.hide();
        }
    }
}
exports.DropdownDiv = DropdownDiv;


/***/ }),

/***/ "./client_src/gui/EndTurnBtn.ts":
/*!**************************************!*\
  !*** ./client_src/gui/EndTurnBtn.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Controller_1 = __webpack_require__(/*! ../Controller */ "./client_src/Controller.ts");
class EndTurnBtn {
    constructor(controller) {
        this.controller = controller;
        this.onClick = (e) => {
            this.controller.endTurn();
            this.endTurnBtn.disabled = true;
        };
        this.endTurnBtn = document.getElementById("end-turn-btn");
        this.endTurnBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onClick);
    }
    enable() {
        this.endTurnBtn.disabled = false;
    }
}
exports.EndTurnBtn = EndTurnBtn;


/***/ }),

/***/ "./client_src/gui/FullOverlay.ts":
/*!***************************************!*\
  !*** ./client_src/gui/FullOverlay.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Controller_1 = __webpack_require__(/*! ../Controller */ "./client_src/Controller.ts");
class FullOverlay {
    constructor(controller) {
        this.controller = controller;
        this.onClick = (e) => {
            this.hide();
            this.controller.onFullOverlayClick();
        };
        this.fullOverlay = document.getElementById("full-overlay");
        this.fullOverlay.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onClick);
    }
    hide() {
        this.fullOverlay.style.visibility = "hidden";
    }
    show() {
        this.fullOverlay.style.visibility = "visible";
    }
}
exports.FullOverlay = FullOverlay;


/***/ }),

/***/ "./client_src/gui/GUI.ts":
/*!*******************************!*\
  !*** ./client_src/gui/GUI.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Action_1 = __webpack_require__(/*! ../game/Action */ "./client_src/game/Action.ts");
const MapCanvas_1 = __webpack_require__(/*! ./MapCanvas */ "./client_src/gui/MapCanvas.ts");
const PlayerList_1 = __webpack_require__(/*! ./PlayerList */ "./client_src/gui/PlayerList.ts");
const ToggleReadyBtn_1 = __webpack_require__(/*! ./ToggleReadyBtn */ "./client_src/gui/ToggleReadyBtn.ts");
const TurnLbl_1 = __webpack_require__(/*! ./TurnLbl */ "./client_src/gui/TurnLbl.ts");
const ItemsFoundDiv_1 = __webpack_require__(/*! ./ItemsFoundDiv */ "./client_src/gui/ItemsFoundDiv.ts");
const InventoryPanel_1 = __webpack_require__(/*! ./InventoryPanel */ "./client_src/gui/InventoryPanel.ts");
const ModifiersList_1 = __webpack_require__(/*! ./ModifiersList */ "./client_src/gui/ModifiersList.ts");
const Tooltip_1 = __webpack_require__(/*! ./Tooltip */ "./client_src/gui/Tooltip.ts");
const ActionList_1 = __webpack_require__(/*! ./ActionList */ "./client_src/gui/ActionList.ts");
const ChatWindow_1 = __webpack_require__(/*! ./ChatWindow */ "./client_src/gui/ChatWindow.ts");
const ItemActionList_1 = __webpack_require__(/*! ./ItemActionList */ "./client_src/gui/ItemActionList.ts");
const AttackCharDiv_1 = __webpack_require__(/*! ./AttackCharDiv */ "./client_src/gui/AttackCharDiv.ts");
const CharacterPanel_1 = __webpack_require__(/*! ./CharacterPanel */ "./client_src/gui/CharacterPanel.ts");
const CenterLbl_1 = __webpack_require__(/*! ./CenterLbl */ "./client_src/gui/CenterLbl.ts");
const ViewportDamageIndicator_1 = __webpack_require__(/*! ./ViewportDamageIndicator */ "./client_src/gui/ViewportDamageIndicator.ts");
const EndTurnBtn_1 = __webpack_require__(/*! ./EndTurnBtn */ "./client_src/gui/EndTurnBtn.ts");
const DropdownDiv_1 = __webpack_require__(/*! ./DropdownDiv */ "./client_src/gui/DropdownDiv.ts");
const FullOverlay_1 = __webpack_require__(/*! ./FullOverlay */ "./client_src/gui/FullOverlay.ts");
const RightPanelMobileDiv_1 = __webpack_require__(/*! ./RightPanelMobileDiv */ "./client_src/gui/RightPanelMobileDiv.ts");
class GUI {
    constructor(controller) {
        this.controller = controller;
        this._tooltip = new Tooltip_1.Tooltip(controller);
        this._mapCanvas = new MapCanvas_1.MapCanvas(controller);
        this._playerList = new PlayerList_1.PlayerList(controller);
        this._toggleReadyBtn = new ToggleReadyBtn_1.ToggleReadyBtn(controller);
        this._turnLbl = new TurnLbl_1.TurnLbl(controller);
        this._itemsFoundDiv = new ItemsFoundDiv_1.ItemsFoundDiv(controller);
        this._inventPanel = new InventoryPanel_1.InventoryPanel(controller);
        this._modifiersList = new ModifiersList_1.ModifiersList(controller);
        this._actionList = new ActionList_1.ActionList(controller);
        this._chatWindow = new ChatWindow_1.ChatWindow(controller);
        this._itemActionList = new ItemActionList_1.ItemActionList(controller);
        this._attackCharDiv = new AttackCharDiv_1.AttackCharDiv(controller);
        this._characterPanel = new CharacterPanel_1.CharacterPanel(controller);
        this._centerLbl = new CenterLbl_1.CenterLbl(controller);
        this._viewportDamageIndicator = new ViewportDamageIndicator_1.ViewportDamageIndicator(controller);
        this._endTurnBtn = new EndTurnBtn_1.EndTurnBtn(controller);
        this._dropdownDiv = new DropdownDiv_1.DropdownDiv(controller);
        this._fullOverlay = new FullOverlay_1.FullOverlay(controller);
        this._rightPanelMobileDiv = new RightPanelMobileDiv_1.RightPanelMobileDiv(controller);
    }
    static new(controller) {
        if (controller) {
            return new GUI(controller);
        }
        return null;
    }
    // Request that the map be redrawn
    requestDrawMap() {
        this._mapCanvas.drawMap();
    }
    // Request that the movement of players be animated on the map
    requestAnimateMovement(oldPlayerDetails, callback) {
        this._mapCanvas.animateMovement(oldPlayerDetails, callback);
    }
    // Request that the full overlay be closed
    requestCloseFullOverlay() {
        this._fullOverlay.hide();
    }
    // Update timer label
    updateTimerLabel(text, timeLeft) {
        const txt = text + (timeLeft > 0 ? " " + timeLeft : "");
        this._turnLbl.setTitleText(txt);
    }
    // Called when the drop phase has begun
    onDropPhaseStart() {
        this._endTurnBtn.enable();
        this._centerLbl.show("Drop Phase", true);
        this._turnLbl.setInstructionsText("Choose a Drop Location");
    }
    // Called when a move phase has begun
    onMovePhaseStart() {
        this._endTurnBtn.enable();
        this._centerLbl.show("Move Phase", true);
        this._actionList.resetActionList();
        this._attackCharDiv.hide();
        this._actionList.appendAction({ name: "Crouch", apCost: Action_1.ActionApCosts.CROUCH }, null, null);
        this._turnLbl.setInstructionsText(this.controller.spectating ?
            "Spectating " + this.controller.myPlayerStub.Name : "Move or Crouch");
        this._dropdownDiv.hide();
        this._mapCanvas.onMovePhaseStart();
    }
    // Called when an action phase has begun
    onActionPhaseStart() {
        this._endTurnBtn.enable();
        this._centerLbl.show("Action Phase", true);
        this._actionList.resetActionList();
        this._turnLbl.setInstructionsText(this.controller.spectating ?
            "Spectating " + this.controller.myPlayerStub.Name : "Perform up to 5 Actions");
        this._tooltip.hideTooltip();
        this._mapCanvas.onActionPhaseStart();
    }
    // Set the current item found
    setItemsFound(items) {
        this._itemsFoundDiv.setItemsFound(items);
    }
    // Called when the game is paused whilst waiting for server
    onWaitingForServer() {
        this._centerLbl.show("Waiting for Server <div class='spinner'></div>");
        this._mapCanvas.onWaitingForServer();
    }
    // Called when the map is fully loaded and initialised
    onMapLoaded() {
        this._mapCanvas.onMapLoaded();
        this._toggleReadyBtn.enable();
    }
    // Called when the player stubs map is updated
    onPlayerStubsUpdated() {
        this._playerList.refreshDisplay();
        //this._toggleReadyBtn.update();
    }
    // Called when the player details map is updated
    onMyPlayerDetailsUpdated() {
        this._mapCanvas.onMyPlayerDetailsUpdated();
    }
    // On player modifiers update
    onMyPlayerModifiersUpdated() {
        this._modifiersList.refreshDisplay();
    }
    // On player hp update
    onMyPlayerHPUpdated(totalHPChangedBy, headInjured, chestInjured, rarmInjured, larmInjured, rlegInjured, llegInjured) {
        this._characterPanel.refreshDisplay();
        if (totalHPChangedBy < 0) {
            this._viewportDamageIndicator.refreshDisplay(totalHPChangedBy);
        }
        if (headInjured) {
            this._characterPanel.showBloodSplatHead();
        }
        if (chestInjured) {
            this._characterPanel.showBloodSplatChest();
        }
        if (rarmInjured) {
            this._characterPanel.showBloodSplatRArm();
        }
        if (larmInjured) {
            this._characterPanel.showBloodSplatLArm();
        }
        if (rlegInjured) {
            this._characterPanel.showBloodSplatRLeg();
        }
        if (llegInjured) {
            this._characterPanel.showBloodSplatLLeg();
        }
    }
    // Called when a move has been deemed valid
    onMoveValid(newX, newY, crouch) {
        this._actionList.resetActionList();
        if (crouch) {
            this._actionList.appendAction({ name: "Crouch", apCost: Action_1.ActionApCosts.CROUCH }, this.controller.globalMouseX, this.controller.globalMouseY);
        }
        else {
            this._actionList.appendAction({ name: "Move", apCost: (Action_1.ActionApCosts.MOVE +
                    (this.controller.myPlayerModifiers.apMoveCost != null ? this.controller.myPlayerModifiers.apMoveCost : 0)) }, null, null);
        }
        this._mapCanvas.onMoveValid(newX, newY);
    }
    // Called when an attack has been deemed valid
    onAttackValid(action) {
        this._actionList.appendAction(action, null, null);
        this._mapCanvas.onAttackValid(action.playerID);
    }
    // Called when a search has been deemed valid
    onSearchValid(action) {
        this._actionList.appendAction(action, null, null);
    }
    // Called when the server has validified a take item request
    onTakeItemValid(entityID, itemID, firstSlotX, firstSlotY) {
        this._inventPanel.onItemTaken(entityID, itemID, firstSlotX, firstSlotY);
        this._itemsFoundDiv.removeItem(entityID, itemID);
    }
    // Called when the server has validified an equip item request
    onEquipItemValid(action) {
        this._actionList.appendAction(action, null, null);
        this._inventPanel.resetSlotStyle();
    }
    // Called when the server has validifed a drop item request
    onDropItemValid(entityID, itemID) {
        this._itemsFoundDiv.addItem(entityID, itemID);
        this._inventPanel.removeFromBackpack(entityID, itemID);
    }
    // Called when the server has validified a use bandage request
    onUseBandageValid(entityID, itemID) {
        this._inventPanel.removeFromBackpack(entityID, itemID);
        this._actionList.appendAction({ name: "Bandage", apCost: Action_1.ActionApCosts.BANDAGE }, null, null);
    }
    // Called when a players ready state is changed
    onReadyStateUpdate(playerID, readyState) {
        this._playerList.updateListItem(playerID, readyState);
        //this._toggleReadyBtn.update();
    }
    // Called when the game leaves the lobby phase and enters the drop phase
    onGameStarted() {
        this._toggleReadyBtn.disable();
        this._toggleReadyBtn.hide();
    }
    // Called at end of turn if players have died
    onPlayerDead(playerName) {
        this._chatWindow.appendMsg(`<b><span style="color: #0000e5;">${playerName}</span></b> has died`);
    }
    // Called along with onPlayerDead if the player is myPlayer
    onMyPlayerDead() {
        // Show you died message and hide all gameplay only content
        this._centerLbl.show("You Died");
        this._rightPanelMobileDiv.hide();
        this._mapCanvas.disable();
        this._actionList.hide();
    }
    // Called when the game is over
    onGameOver(winnerIDs, didMyPlayerWin, didMyPlayerDraw) {
        // Display msg specific to my player
        if (didMyPlayerDraw) {
            this._centerLbl.show("Draw");
        }
        else if (didMyPlayerWin) {
            this._centerLbl.show("Victory");
        }
        else {
            this._centerLbl.show("Defeat");
        }
        // Display who won in the kill feed
        let winMsg = "";
        for (const winnerID of winnerIDs) {
            const playerName = this.controller.playerStubs[winnerID] ?
                this.controller.playerStubs[winnerID].Name : "Player" + winnerID;
            winMsg += `<b><span style="color: #0000e5;">${playerName}</span></b>, `;
        }
        winMsg = winMsg.slice(0, -2);
        winMsg += (winnerIDs.length === 1 ? " is" : " are") + " victorious";
        this._chatWindow.appendMsg(winMsg);
    }
    // Called when a storm approach update is received from server
    onStormApproachUpdate() {
        this._centerLbl.show("Nuclear Meltdown Imminent");
    }
    // Called when a storm radius update is received from server
    onStormRadiusUpdate() {
        this.controller.requestDrawMap();
    }
    // Called when a storm dmg update is received from server
    onStormDmgUpdate() {
    }
    // Called when the player starts dragging an item in their inventory
    onItemDragStart() {
        this._itemsFoundDiv.enableDropMode();
    }
    // Called when the player stops dragging an item in their inventory
    onItemDragEnd() {
        this._itemsFoundDiv.disableDropMode();
        this._inventPanel.onItemDragEnd();
    }
    // Called when the player begins hovering over a new tile during the drop phase
    onTileHoverDropPhase(i, j, x, y) {
        this._tooltip.showDropTileTooltip(i, j, x, y);
    }
    // Called when the player begins hovering over a new tile during the move phase
    onTileHoverMovePhase(i, j, x, y) {
        this._tooltip.showTileTooltip(i, j, x, y);
    }
    // Called when the player begins hovering over a new tile during the action phase
    onTileHoverActionPhase(name, desc, apCost, x, y) {
        this._tooltip.showActionTooltip(name, desc, apCost, x, y);
    }
    // Called when the player clicks a tile during the action phase
    onTileClickedActionPhase(x, y, tileX, tileY, canSearch, canAttack) {
        this._dropdownDiv.showTileActionList(x, y, tileX, tileY, canSearch, canAttack, true);
        this._tooltip.hideTooltip();
    }
    // Called when the player hovers an item memento
    onItemMementoHover(mem, x, y) {
        this._tooltip.showItemMementoTooltip(mem, x, y);
    }
    // Called when the player wants to attack a player on a tile
    onAttackPlayerOnTile(x, y, tileX, tileY) {
        this._dropdownDiv.showAttackPlayerList(x, y, tileX, tileY);
    }
    // Called when the player begins hovering over an item
    onItemHover(entity, x, y) {
        this._tooltip.showItemTooltip(entity, x, y);
    }
    // Called when the player left or right clicks a bandage in their inventory
    onBandageClick(x, y, entityID) {
        this._dropdownDiv.showUseBandageList(x, y, entityID);
    }
    // Called when the play clicks a limb button
    onHealLimbClick(limb) {
        // Ensure player has bandage in their inventory
        if (this._inventPanel.containsBandage()) {
            this._fullOverlay.show();
            this._inventPanel.highlightBandages(limb);
        }
    }
    // Called when the player clicks the full screen overlay
    onFullOverlayClick() {
        this._inventPanel.unHighlightItems();
    }
    // Called when the player initiates an attack on another player
    onInitiateAttackPlayer(playerID) {
        this._attackCharDiv.refreshDisplay(playerID);
    }
    // Called when a player disconnects from the game
    onPlayerDisconnect(playerName) {
        this._playerList.refreshDisplay();
        this._chatWindow.appendMsg(`<b><span style="color: #0000e5;">${playerName}</span></b> has disconnected`);
    }
    // Called upon receiving a msg from another player
    onMsgReceived(playerName, msg) {
        this._chatWindow.appendMsg(`<b><span style="color: #0000e5;">${playerName}:</span></b> ${msg}`);
    }
    onWindowResize() {
        this._mapCanvas.resize();
    }
}
exports.GUI = GUI;


/***/ }),

/***/ "./client_src/gui/InventoryPanel.ts":
/*!******************************************!*\
  !*** ./client_src/gui/InventoryPanel.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Controller_1 = __webpack_require__(/*! ../Controller */ "./client_src/Controller.ts");
const GamePhase_1 = __webpack_require__(/*! ../game/GamePhase */ "./client_src/game/GamePhase.ts");
const Item_1 = __webpack_require__(/*! ../game/Item */ "./client_src/game/Item.ts");
class InventoryPanel {
    constructor(controller) {
        this.controller = controller;
        this.rowLength = 3;
        this.colHeight = 4;
        this.onDragEnter = (e) => {
            if (!e.target) {
                return;
            }
            e.preventDefault();
            const slotIndex = parseInt(e.target.id.split("invent-slot-")[1], 10);
            const splits = e.dataTransfer.getData("text").split("_");
            let itemIndex, entityID;
            if (splits.length == 3) {
                // Dropped item from ground
                itemIndex = parseInt(splits[1], 10);
                entityID = parseInt(splits[2], 10);
            }
            else if (splits.length == 6) {
                // Dropped item already in inventory
                itemIndex = parseInt(splits[2], 10);
                entityID = parseInt(splits[3], 10);
            }
            if (isNaN(slotIndex) || isNaN(itemIndex) || isNaN(entityID)) {
                return;
            }
            // Clear all drag hover effects
            for (const row of this.itemSlots) {
                for (const itemSlot of row) {
                    itemSlot.style.cssText = "";
                }
            }
            // Determine whether a drop is allowed based on slot emptiness
            const item = this.controller.items[itemIndex];
            this.updateSlot(item, slotIndex, entityID);
        };
        this.onDragLeave = (e) => {
            e.preventDefault();
        };
        this.onDragOver = (e) => {
            e.preventDefault();
        };
        this.onDrop = (e) => {
            if (!e.target) {
                return;
            }
            e.preventDefault();
            const slotIndex = parseInt(e.target.id.split("invent-slot-")[1], 10);
            const splits = e.dataTransfer.getData("text").split("_");
            let itemIndex, entityID;
            if (splits.length == 3) {
                // Dropped item from ground
                itemIndex = splits[1];
                entityID = splits[2];
            }
            else if (splits.length == 6) {
                // Dropped item already in inventory
                itemIndex = splits[2];
                entityID = splits[3];
            }
            if (isNaN(slotIndex) || isNaN(itemIndex) || isNaN(entityID)) {
                return;
            }
            // Check the wether the slot is avialable to be dropped in
            const item = this.controller.items[itemIndex];
            this.updateSlot(item, slotIndex, entityID, false, true);
        };
        this.onDragStart = (e) => {
            const itemObj = this.controller.items[parseInt(e.target.id.split("_")[2], 10)];
            const entityID = parseInt(e.target.id.split("_")[3], 10);
            // set the entity to be hidden so it looks like it is being picked up
            for (const row of this.itemSlots) {
                for (const slot of row) {
                    if (slot.children.length > 0) {
                        const elem = document.getElementById(slot.children[0].id);
                        if (elem) {
                            const eID = parseInt(elem.id.split("_")[3], 10);
                            if (eID == entityID) {
                                elem.style.visibility = "hidden";
                            }
                        }
                    }
                }
            }
            // set the drag-img to the item img, with size relative to item slot size
            // method for setting drag-img size sourced from https://stackoverflow.com/questions/31994572/change-drag-ghost-image-size
            let dragIcon = document.createElement("img");
            dragIcon.src = e.target.src;
            dragIcon.style.width = `${this.itemSlots[0][0].clientWidth * itemObj.gridWidth}px`;
            dragIcon.style.height = `${this.itemSlots[0][0].clientHeight * itemObj.gridHeight}px`;
            let div = document.createElement("div");
            div.id = `drag_${e.target.id}`;
            div.appendChild(dragIcon);
            div.style.position = "absolute";
            div.style.top = "0px";
            div.style.left = `${-this.itemSlots[0][0].clientWidth * itemObj.gridWidth}px`;
            document.querySelector("body").appendChild(div);
            e.dataTransfer.setData("text/plain", e.target.id);
            e.dataTransfer.setDragImage(div, this.itemSlots[0][0].clientWidth / 2, this.itemSlots[0][0].clientHeight / 2);
            this.controller.onItemDragStart();
        };
        this.onDragEnd = (e) => {
            // when the item has finished being dragged, remove the image in the dom
            document.getElementById(`drag_${e.target.id}`).remove();
            this.controller.onItemDragEnd();
            const entityID = e.target.id.split("_")[3];
            // set the entity in back to being visible
            this.resetSlotStyle();
        };
        this.onClick = (e) => {
            e.preventDefault();
            if (this.controller.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE) {
                const entityID = e.target.id.split("_")[3];
                const itemID = e.target.id.split("_")[2];
                const item = this.controller.items[itemID];
                if (Item_1.isBandage(item)) {
                    if (this.limbSelected) {
                        this.controller.useBandage(entityID, this.limbSelected);
                        this.limbSelected = null;
                    }
                    else {
                        this.controller.onBandageClick(e.clientX, e.clientY, entityID);
                    }
                }
                else if (Item_1.isGun(item)) {
                    this.controller.equipItem(entityID);
                }
            }
        };
        this.onHover = (e) => {
            e.preventDefault();
            const itemID = parseInt(e.target.id.split("_")[2], 10);
            const entityID = parseInt(e.target.id.split("_")[3], 10);
            this.controller.onItemHover(new Item_1.ItemEntity(entityID, itemID), e.clientX, e.clientY);
        };
        this.itemSlots = new Array();
        for (let j = 0; j < this.colHeight; j++) {
            this.itemSlots.push(new Array());
            for (let i = 0; i < this.rowLength; i++) {
                const itemSlot = document.getElementById(`invent-slot-${i + (j * 3)}`);
                itemSlot.addEventListener("dragenter", this.onDragEnter);
                itemSlot.addEventListener("dragleave", this.onDragLeave);
                itemSlot.addEventListener("dragover", this.onDragOver);
                itemSlot.addEventListener("drop", this.onDrop);
                this.itemSlots[this.itemSlots.length - 1].push(itemSlot);
            }
        }
    }
    // Returns true iff the player's inventory contains at least one bandage item
    containsBandage() {
        for (const group of this.itemSlots) {
            for (const slot of group) {
                if (slot.firstChild) {
                    const splits = slot.children[0].id.split("_");
                    if (splits.length >= 3) {
                        const itemIndex = splits[2];
                        const item = this.controller.items[itemIndex];
                        if (Item_1.isBandage(item)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
    // Highlight bandages so player can easily pick which to use to heal limb
    highlightBandages(limb) {
        this.limbSelected = limb;
        for (const group of this.itemSlots) {
            for (const slot of group) {
                if (slot.firstChild) {
                    const splits = slot.children[0].id.split("_");
                    if (splits.length >= 3) {
                        const itemIndex = splits[2];
                        const item = this.controller.items[itemIndex];
                        if (Item_1.isBandage(item)) {
                            slot.style.zIndex = "75";
                        }
                    }
                }
            }
        }
    }
    unHighlightItems() {
        this.limbSelected = null;
        for (const group of this.itemSlots) {
            for (const slot of group) {
                slot.style.zIndex = "0";
            }
        }
    }
    onItemDragEnd() {
        this.resetSlotStyle();
    }
    // Assumes slot has already been validified
    fillSlot(entityID, firstSlotX, firstSlotY) {
        this.controller.takeItem(entityID, firstSlotX, firstSlotY);
    }
    updateSlot(item, slotIndex, entityID, color = true, drop = false) {
        const firstSlotX = (slotIndex % this.rowLength);
        const firstSlotY = Math.floor(slotIndex / this.rowLength);
        const slotY = firstSlotY;
        // check if the item fits on this row
        if (firstSlotX + item.gridWidth > this.rowLength) {
            if (color) {
                for (let x = firstSlotX; x < this.rowLength; x++) {
                    this.itemSlots[slotY][x].style.backgroundColor = "#f00";
                }
            }
            return;
        }
        // check if the slots are taken
        for (let x = firstSlotX; x < firstSlotX + item.gridWidth; x++) {
            if (this.itemSlots[slotY][x].innerHTML != "") {
                if (color) {
                    for (let x = firstSlotX; x < firstSlotX + item.gridWidth; x++) {
                        this.itemSlots[slotY][x].style.backgroundColor = "#f00";
                    }
                }
                return;
            }
        }
        // else, the slots must be free
        for (let x = firstSlotX; x < firstSlotX + item.gridWidth; x++) {
            if (color) {
                this.itemSlots[slotY][x].style.backgroundColor = "#0f0";
            }
        }
        if (drop) {
            // only need to tell server about first slot indexes
            this.fillSlot(entityID, firstSlotX, firstSlotY);
        }
    }
    removeFromBackpack(entityID, itemID) {
        for (let slotY = 0; slotY < this.colHeight; slotY++) {
            for (let slotX = 0; slotX < this.rowLength; slotX++) {
                const entity = document.getElementById(`backpack_item_${itemID}_${entityID}_${slotX}_${slotY}`);
                if (entity) {
                    entity.remove();
                    this.itemSlots[slotY][slotX].classList.remove("invent-slot-joined-right");
                    this.itemSlots[slotY][slotX].classList.remove("invent-slot-joined-left");
                }
            }
        }
    }
    // Called from controller once server has validified item taken
    onItemTaken(entityID, itemID, firstSlotX, firstSlotY) {
        if (itemID < 0 || itemID >= this.controller.items.length) {
            return;
        }
        const item = this.controller.items[itemID];
        // first, ensure the item fits into the space
        if (item.gridWidth > firstSlotX + item.gridWidth) {
            return;
        }
        // remove the item from the current spot in backpack if it is in backpack
        this.removeFromBackpack(entityID, itemID);
        const slotY = firstSlotY; // TODO - change when item rotation implemented
        let i = 0;
        for (let slotX = firstSlotX; slotX < firstSlotX + item.gridWidth; slotX++) {
            const slot = this.itemSlots[slotY][slotX];
            // set the drag-img to the item img, with size relative to item slot size
            // method for setting drag-img size sourced from https://stackoverflow.com/questions/31994572/change-drag-ghost-image-size
            let img = document.createElement("img");
            img.id = `backpack_item_${itemID}_${entityID}_${slotX}_${slotY}`;
            img.src = item.image;
            img.style.width = `${(slot.clientWidth - 0) * item.gridWidth}px`;
            img.style.height = `${(slot.clientHeight - 0) * item.gridHeight}px`;
            img.classList.add("invent-item-img");
            img.style.left = `${-slot.clientWidth * i}px`;
            img.addEventListener("dragstart", this.onDragStart);
            img.addEventListener("dragend", this.onDragEnd);
            img.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onClick);
            img.addEventListener("mousemove", this.onHover);
            slot.appendChild(img);
            i++;
        }
        // remove right borders from all but last slot
        for (let slotX = firstSlotX; slotX < firstSlotX + item.gridWidth - 1; slotX++) {
            ///console.log("removing right border");
            const slot = this.itemSlots[slotY][slotX];
            slot.style.borderRight = "none";
            slot.classList.add("invent-slot-joined-right");
        }
        // remove left borders from all but first slot
        for (let slotX = firstSlotX + 1; slotX < firstSlotX + item.gridWidth; slotX++) {
            ///console.log("removing left border");
            const slot = this.itemSlots[slotY][slotX];
            slot.classList.add("invent-slot-joined-left");
        }
        this.resetSlotStyle();
    }
    // Called when the action phase is performed and a new weapon is equipped
    onItemEquipped() {
        this.resetSlotStyle();
    }
    resetSlotStyle() {
        for (const row of this.itemSlots) {
            for (const slot of row) {
                let equipped = false;
                // set the entity in back to being visible
                if (slot.children.length > 0) {
                    const elem = document.getElementById(slot.children[0].id);
                    if (elem) {
                        elem.style.visibility = "visible";
                        const eID = parseInt(elem.id.split("_")[3], 10);
                        if (this.controller.myPlayerEquippedItem &&
                            eID == this.controller.myPlayerEquippedItem.entityID) {
                            slot.style.backgroundColor = "#0EBFE9";
                            equipped = true;
                        }
                    }
                }
                // set any slots not containing equipped weapons to default
                if (!equipped) {
                    slot.style.cssText = "";
                }
            }
        }
    }
}
exports.InventoryPanel = InventoryPanel;


/***/ }),

/***/ "./client_src/gui/ItemActionList.ts":
/*!******************************************!*\
  !*** ./client_src/gui/ItemActionList.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class ItemActionList {
    constructor(controller) {
        this.controller = controller;
        this.itemActionList = document.getElementById("item-action-list");
    }
    showActionList(x, y, canEquip = false) {
        if (x >= 0 && y >= 0) {
            // clear contents of div
            while (this.itemActionList.firstChild) {
                this.itemActionList.removeChild(this.itemActionList.firstChild);
            }
            // set the contents of the tooltip
            if (canEquip) {
                let btn = document.createElement("button");
                let img = document.createElement("img");
                let span = document.createElement("span");
                img.src = "assets/use_icon.png";
                img.style.display = "inline-block";
                span.innerHTML = "Equip";
                span.style.display = "table-cell";
                btn.appendChild(img);
                btn.appendChild(span);
                this.itemActionList.appendChild(btn);
            }
            // set the position of the tooltip
            this.itemActionList.style.left = `calc(${x}px - ${this.itemActionList.clientWidth / 2}px)`;
            this.itemActionList.style.top = `${y}px`;
            // make sure the tooltip is visible
            this.itemActionList.style.visibility = "visible";
        }
        else {
            this.itemActionList.style.visibility = "hidden";
        }
    }
}
exports.ItemActionList = ItemActionList;


/***/ }),

/***/ "./client_src/gui/ItemsFoundDiv.ts":
/*!*****************************************!*\
  !*** ./client_src/gui/ItemsFoundDiv.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class ItemsFoundDiv {
    constructor(controller) {
        this.controller = controller;
        this.onDragStart = (e) => {
            const itemObj = this.controller.items[parseInt(e.target.id.split("_")[1], 10)];
            // set the drag-img to the item img, with size relative to item slot size
            // method for setting drag-img size sourced from https://stackoverflow.com/questions/31994572/change-drag-ghost-image-size
            let dragIcon = document.createElement("img");
            dragIcon.src = e.target.src;
            dragIcon.style.width = `${this.itemSlot1.clientWidth * itemObj.gridWidth}px`;
            dragIcon.style.height = `${this.itemSlot1.clientHeight * itemObj.gridHeight}px`;
            let div = document.createElement("div");
            div.id = `drag_${e.target.id}`;
            div.appendChild(dragIcon);
            div.style.position = "absolute";
            div.style.top = "0px";
            div.style.left = `${-this.itemSlot1.clientWidth * itemObj.gridWidth}px`;
            document.querySelector("body").appendChild(div);
            e.dataTransfer.setData("text/plain", e.target.id);
            e.dataTransfer.setDragImage(div, this.itemSlot1.clientWidth / 2, this.itemSlot1.clientHeight / 2);
        };
        this.onDragEnd = (e) => {
            // when the item has finished being dragged, remove the image in the dom
            document.getElementById(`drag_${e.target.id}`).remove();
            this.controller.onItemDragEnd();
        };
        this.onDragEnter = (e) => {
            e.preventDefault();
        };
        this.onDragOver = (e) => {
            e.preventDefault();
        };
        this.onDrop = (e) => {
            e.preventDefault();
            const entityID = parseInt(e.dataTransfer.getData("text/html").split("_")[3], 10);
            this.controller.dropItem(entityID);
        };
        this.itemsFoundDiv = document.getElementById("items-found-div");
        this.itemsFoundList = document.getElementById("items-found-list");
        this.itemSlot1 = document.getElementById("invent-slot-1");
        this.dropitemDiv = document.getElementById("drop-item-div");
        this.dropitemDiv.addEventListener("dragenter", this.onDragEnter);
        this.dropitemDiv.addEventListener("dragover", this.onDragOver);
        this.dropitemDiv.addEventListener("drop", this.onDrop);
    }
    clear() {
        while (this.itemsFoundList.firstChild) {
            this.itemsFoundList.removeChild(this.itemsFoundList.firstChild);
        }
    }
    setItemsFound(items) {
        this.clear();
        for (const item of items) {
            const itemObj = this.controller.items[item.itemID];
            let img = document.createElement("img");
            img.id = `item_${item.itemID}_${item.entityID}`;
            img.classList.add("item-found-img");
            img.src = itemObj.image;
            img.title = itemObj.name;
            img.style.width = `${this.itemSlot1.clientWidth * itemObj.gridWidth}px`;
            img.style.height = `${this.itemSlot1.clientHeight * itemObj.gridHeight}px`;
            this.itemsFoundList.appendChild(img);
        }
        this.itemsFoundDiv.style.visibility = items.length > 0 ? "visible" : "hidden";
        for (const item of items) {
            document.getElementById(`item_${item.itemID}_${item.entityID}`).addEventListener("dragstart", this.onDragStart);
            document.getElementById(`item_${item.itemID}_${item.entityID}`).addEventListener("dragend", this.onDragEnd);
        }
    }
    addItem(entityID, itemID) {
        const itemObj = this.controller.items[itemID];
        const img = document.createElement("img");
        img.id = `item_${itemID}_${entityID}`;
        img.classList.add("item-found-img");
        img.src = itemObj.image;
        img.title = itemObj.name;
        img.style.width = `${this.itemSlot1.clientWidth * itemObj.gridWidth}px`;
        img.style.height = `${this.itemSlot1.clientHeight * itemObj.gridHeight}px`;
        img.addEventListener("dragstart", this.onDragStart);
        img.addEventListener("dragend", this.onDragEnd);
        this.itemsFoundDiv.style.visibility = "visible";
        this.itemsFoundList.appendChild(img);
    }
    removeItem(entityID, itemID) {
        const entity = document.getElementById(`item_${itemID}_${entityID}`);
        if (entity) {
            entity.remove();
            if (this.itemsFoundList.children.length <= 1) {
                this.itemsFoundDiv.style.visibility = "hidden";
            }
        }
    }
    enableDropMode() {
        this.dropitemDiv.style.visibility = "visible";
        this.itemsFoundDiv.style.visibility = "hidden";
    }
    disableDropMode() {
        this.dropitemDiv.style.visibility = "hidden";
        if (this.itemsFoundList.children.length > 1) {
            this.itemsFoundDiv.style.visibility = "visible";
        }
    }
}
exports.ItemsFoundDiv = ItemsFoundDiv;


/***/ }),

/***/ "./client_src/gui/MapCanvas.ts":
/*!*************************************!*\
  !*** ./client_src/gui/MapCanvas.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Controller_1 = __webpack_require__(/*! ../Controller */ "./client_src/Controller.ts");
const GamePhase_1 = __webpack_require__(/*! ../game/GamePhase */ "./client_src/game/GamePhase.ts");
const TileType_1 = __webpack_require__(/*! ../game/TileType */ "./client_src/game/TileType.ts");
const Action_1 = __webpack_require__(/*! ../game/Action */ "./client_src/game/Action.ts");
const MapCanvasArrow_1 = __webpack_require__(/*! ./MapCanvasArrow */ "./client_src/gui/MapCanvasArrow.ts");
class MapCanvas {
    // Initialise the MapCanvas object
    constructor(controller) {
        this.controller = controller;
        this.zoomLevel = 1.0;
        // All information needed to draw a hexagon
        // Sourced from https://gist.github.com/cDecker32/2500868
        this.hexagonAngle = 0.523598776; // 30 degrees in radians
        this.sideLength = 72;
        this.hexHeight = Math.sin(this.hexagonAngle) * this.sideLength;
        this.hexRadius = Math.cos(this.hexagonAngle) * this.sideLength;
        this.hexRectangleHeight = this.sideLength + 2 * this.hexHeight;
        this.hexRectangleWidth = 2 * this.hexRadius;
        // The player image to be drawn wherever there's a player
        // TODO - include a player image in the player's stub
        this.playerImage = new Image();
        this.onClick = (e) => {
            e.preventDefault();
            const myDetails = this.controller.myPlayerDetails;
            const objUnderMouse = this.getObjectUnderMouse(e);
            const hexGrid = this.controller.map.hexGrid;
            if (hexGrid && objUnderMouse && ("x" in objUnderMouse)) {
                const hexagon = objUnderMouse;
                // get the tile type id and tile type
                const tileTypeID = hexGrid[hexagon.y][hexagon.x].type;
                const tileType = TileType_1.TileType.types[tileTypeID];
                if (this.controller.gamePhase == GamePhase_1.GamePhase.DROP_PHASE) {
                    // Ensure the tile can be landed on
                    if (!tileType.impassable && !tileType.undroppable) {
                        this.hexSelectedX = hexagon.x;
                        this.hexSelectedY = hexagon.y;
                        this.controller.setMoveTurn(this.hexSelectedX, this.hexSelectedY);
                    }
                }
                else if (this.controller.gamePhase == GamePhase_1.GamePhase.MOVE_PHASE) {
                    if (myDetails) {
                        const dx = myDetails.x - hexagon.x;
                        const dy = myDetails.y - hexagon.y;
                        if ((((dy == -1 || dy == 1) && (myDetails.y % 2 == 1) && dx >= -1 && dx <= 0) ||
                            ((dy == -1 || dy == 1) && (myDetails.y % 2 == 0) && dx >= 0 && dx <= 1) ||
                            (dy == 0 && dx >= -1 && dx <= 1)) && !tileType.impassable) {
                            this.hexSelectedX = hexagon.x;
                            this.hexSelectedY = hexagon.y;
                            this.controller.setMoveTurn(this.hexSelectedX, this.hexSelectedY);
                            this.controller.onMoveValid(this.hexSelectedX, this.hexSelectedY, dx == dy && dx == 0);
                        }
                    }
                }
                else if (this.controller.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE) {
                    if (myDetails) {
                        const item = this.controller.myPlayerEquippedItem ?
                            this.controller.items[this.controller.myPlayerEquippedItem.itemID] : null;
                        const searchRange = Math.max(1, 0);
                        const attackRange = Math.max(item ? item.maxRange : 0, 0);
                        const canSearch = this.isPointWithinHexagon(hexagon.x, hexagon.y, searchRange, myDetails.x, myDetails.y);
                        const canAttack = this.controller.isPlayerOnTile(hexagon.x, hexagon.y) &&
                            this.isPointWithinHexagon(hexagon.x, hexagon.y, attackRange, myDetails.x, myDetails.y) &&
                            this.controller.myPlayerAP >= Action_1.ActionApCosts.ATTACK;
                        if (e.button == 2) {
                            // if right click, show the custom context menu
                            this.controller.onTileRightClickedActionPhase(this.lastGlobalMouseX, this.lastGlobalMouseY, hexagon.x, hexagon.y, canSearch, canAttack);
                        }
                        else if (e.button == 0) {
                            // if left click and can attack, determine which player is on tile
                            const playerIDs = this.controller.getPlayersOnTile(hexagon.x, hexagon.y);
                            if (canAttack && playerIDs.length === 1) {
                                this.controller.onInitiateAttackPlayer(playerIDs[0]);
                            }
                            else if (canAttack && playerIDs.length > 1) {
                                this.controller.onAttackPlayerOnTile(this.lastGlobalMouseX, this.lastGlobalMouseY, hexagon.x, hexagon.y);
                            }
                            else if (canSearch) {
                                this.controller.searchTile(hexagon.x, hexagon.y);
                            }
                        }
                    }
                }
                // request the map be redrawn with new selection info
                this.controller.requestDrawMap();
            }
            return false;
        };
        this.onMouseMove = (e) => {
            const myDetails = this.controller.myPlayerDetails;
            const objUnderMouse = this.getObjectUnderMouse(e);
            const hexGrid = this.controller.map.hexGrid;
            this.controller.globalMouseX = this.lastGlobalMouseX = e.clientX;
            this.controller.globalMouseY = this.lastGlobalMouseY = e.clientY;
            this.lastMouseX = this.lastGlobalMouseX - this.canvas.getBoundingClientRect().left;
            this.lastMouseY = this.lastGlobalMouseY - this.canvas.getBoundingClientRect().top;
            if (hexGrid && objUnderMouse && ("x" in objUnderMouse)) {
                const hexagon = objUnderMouse;
                // get the tile type id and tile type
                const tileTypeID = hexGrid[hexagon.y][hexagon.x].type;
                const tileType = TileType_1.TileType.types[tileTypeID];
                this.hexHoverX = hexagon.x;
                this.hexHoverY = hexagon.y;
                if (this.controller.gamePhase == GamePhase_1.GamePhase.DROP_PHASE) {
                    // notify the controller about tile hover so a tooltip can be created
                    this.controller.onTileHoverDropPhase(hexagon.x, hexagon.y, this.lastGlobalMouseX, this.lastGlobalMouseY);
                    // Ensure the tile can be landed on
                    if (!tileType.impassable && !tileType.undroppable) {
                        this.hexHoverValid = true;
                        this.canvas.style.cursor = "url('assets/Boots_Icon.png') 16 16, auto";
                    }
                    else {
                        this.hexHoverValid = false;
                        this.canvas.style.cursor = "url('assets/Cross_Icon.png') 16 16, auto";
                    }
                }
                else if (this.controller.gamePhase == GamePhase_1.GamePhase.MOVE_PHASE) {
                    // notify the controller about the tile hover so a tooltip can be created
                    this.controller.onTileHoverMovePhase(hexagon.x, hexagon.y, this.lastGlobalMouseX, this.lastGlobalMouseY);
                    if (myDetails) {
                        const dx = myDetails.x - hexagon.x;
                        const dy = myDetails.y - hexagon.y;
                        if ((((dy == -1 || dy == 1) && (myDetails.y % 2 == 1) && dx >= -1 && dx <= 0) ||
                            ((dy == -1 || dy == 1) && (myDetails.y % 2 == 0) && dx >= 0 && dx <= 1) ||
                            (dy == 0 && dx >= -1 && dx <= 1)) && !tileType.impassable) {
                            this.hexHoverValid = true;
                            this.canvas.style.cursor = "url('assets/Boots_Icon.png') 16 16, auto";
                        }
                        else {
                            this.hexHoverValid = false;
                            this.canvas.style.cursor = "url('assets/Cross_Icon.png') 16 16, auto";
                        }
                    }
                }
                else if (this.controller.gamePhase == GamePhase_1.GamePhase.ACTION_PHASE) {
                    this.hexHoverValid = false;
                    if (myDetails) {
                        const item = this.controller.myPlayerEquippedItem ?
                            this.controller.items[this.controller.myPlayerEquippedItem.itemID] : null;
                        const searchRange = Math.max(1, 0);
                        const attackRange = Math.max(item ? item.maxRange : 0, 0);
                        const canSearch = this.isPointWithinHexagon(hexagon.x, hexagon.y, searchRange, myDetails.x, myDetails.y);
                        const canAttack = this.controller.isPlayerOnTile(hexagon.x, hexagon.y) &&
                            this.isPointWithinHexagon(hexagon.x, hexagon.y, attackRange, myDetails.x, myDetails.y) &&
                            this.controller.myPlayerAP >= Action_1.ActionApCosts.ATTACK;
                        // set the mouse cursor icon based on what is being hovered
                        //    console.log("Can Attack/Search", canAttack, canSearch, this.controller.myPlayerAP);
                        if (canSearch && !canAttack) {
                            this.canvas.style.cursor = "url('assets/search_icon.png') 16 16, auto";
                            this.hexHoverValid = true;
                            // TODO - get proper ap costs from somewhere
                            this.controller.onTileHoverActionPhase("Search", "Search current/adjacent tile for mines", 10, this.lastGlobalMouseX, this.lastGlobalMouseY);
                        }
                        else if (canAttack) {
                            this.canvas.style.cursor = "url('assets/target_icon.png') 20 20, auto";
                            this.hexHoverValid = true;
                            // TODO - get proper ap costs from somewhere
                            this.controller.onTileHoverActionPhase("Attack", "Attack player with currently equipped weapon", 60, this.lastGlobalMouseX, this.lastGlobalMouseY);
                        }
                        else {
                            this.canvas.style.cursor = "url('assets/Cross_Icon.png') 16 16, auto";
                            this.controller.onTileHoverActionPhase(null, null, 0, 0, 0);
                        }
                    }
                }
                // request the map be redrawn with new hover info
                this.controller.requestDrawMap();
            }
            else if (objUnderMouse && ("mem" in objUnderMouse)) {
                const memento = objUnderMouse.mem;
                this.controller.onItemMementoHover(memento, this.lastGlobalMouseX, this.lastGlobalMouseY);
            }
        };
        this.onMouseWheel = (e) => {
            // Calculate the min. zoom level s.t. the entire map is visible
            const wheelDelta = e.wheelDelta ? e.wheelDelta : e.deltaY ? -e.deltaY : 0;
            const canvasToMapWidthRatio = this.canvas.width / this.totalMapWidth;
            const canvasToMapHeightRatio = this.canvas.height / this.totalMapHeight;
            let minZoomLevel = canvasToMapWidthRatio < canvasToMapHeightRatio ? canvasToMapWidthRatio : canvasToMapHeightRatio;
            // Calculate the new zoom level based on the direction the wheel was spun
            const wheelDirection = wheelDelta < 0 ? -1 : wheelDelta > 0 ? 1 : 0;
            this.zoomLevel += wheelDirection * 0.05;
            this.zoomLevel = this.zoomLevel < minZoomLevel ? minZoomLevel : this.zoomLevel > 1.0 ? 1.0 : this.zoomLevel;
            // Determine the zoom target based on the mouse location
            // NOTE - when zooming out, the target should be centered on map center
            if (!this.controller.myPlayerDetails) {
                if (wheelDirection < 0) {
                    // zooming out
                    this.zoomTargetX = this.canvas.width / 2 - this.totalMapWidth / 2 * this.zoomLevel;
                    this.zoomTargetY = this.canvas.height / 2 - this.totalMapHeight / 2 * this.zoomLevel;
                }
                else {
                    // zooming in
                    this.zoomTargetX = this.canvas.width / 2 - this.totalMapWidth / 2 * this.zoomLevel;
                    this.zoomTargetY = this.canvas.height / 2 - this.totalMapHeight / 2 * this.zoomLevel;
                }
            }
            else {
                // center the player
                const i = this.controller.myPlayerDetails.x;
                const j = this.controller.myPlayerDetails.y;
                var x = i * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * (j % 2)) + this.hexRectangleWidth / 2;
                var y = j * this.hexRectangleHeight - (this.sideLength / 2 * j) + this.hexRectangleHeight / 2;
                this.zoomTargetX = this.canvas.width / 2 - x * this.zoomLevel;
                this.zoomTargetY = this.canvas.height / 2 - y * this.zoomLevel;
            }
            // Redraw the map at the new zoom level
            e.preventDefault();
            this.controller.requestDrawMap();
            return false;
        };
        this.canvas = document.getElementById("map-canvas");
        this.hitCanvas = document.getElementById("hit-canvas");
        this.ctx = this.canvas.getContext("2d");
        this.hitCtx = this.hitCanvas.getContext("2d");
        this.resize();
        // Load the player image
        this.playerImage.src = "assets/Stickman 1.png";
        // Initialise the list of arrows
        this.displayArrows = new Array();
    }
    // Disable after my player dies to disallow mouse input
    disable() {
        this.canvas.style.pointerEvents = "none";
    }
    animateMovement(oldPlayerDetails, callback) {
        const newPlayerDetails = this.controller.playerDetails;
        let interpolatedDetails = {};
        for (const playerID in newPlayerDetails) {
            interpolatedDetails[playerID] = Object.assign({}, newPlayerDetails[playerID]);
        }
        const animDuration = 1400;
        this.controller.startTimer(animDuration / 1000, (timeLeft, timeoutID) => {
            // Every interval seconds, redraw map with interpolated player co-ordinates
            for (const playerID in newPlayerDetails) {
                const newDetails = newPlayerDetails[playerID];
                let slerpX = newDetails.x;
                let slerpY = newDetails.y;
                if (oldPlayerDetails[playerID]) {
                    const slerpFactor = 1 - (timeLeft / animDuration);
                    const oldDetails = oldPlayerDetails[playerID];
                    const dy = oldDetails.y - newDetails.y;
                    let oldX = oldDetails.x;
                    let newX = newDetails.x;
                    if (oldDetails.y % 2 === 0 && dy != 0) {
                        oldX -= 1;
                    }
                    if (dy === -1) {
                        if (oldDetails.y % 2 === 0) {
                            oldX += 1;
                        }
                        else {
                            newX -= 1;
                        }
                    }
                    slerpX = (oldX) +
                        (slerpFactor * (newX - oldX));
                    slerpY = (oldDetails.y) +
                        (slerpFactor * (newDetails.y - oldDetails.y));
                }
                interpolatedDetails[playerID].x = slerpX;
                interpolatedDetails[playerID].y = slerpY;
            }
            this.drawMap(interpolatedDetails);
        }, callback, false, 40);
    }
    drawMap(playerDetails = this.controller.playerDetails) {
        if (!this.controller.map) {
            return;
        }
        const hexGrid = this.controller.map.hexGrid;
        if (!hexGrid) {
            return;
        }
        this.ctx.fillStyle = TileType_1.TileType.types[0].color;
        //ctx.clearRect(0, 0, canvas.width, canvas.height);
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.hitCtx.clearRect(0, 0, this.hitCanvas.width, this.hitCanvas.height);
        this.ctx.save();
        this.ctx.translate(this.zoomTargetX, this.zoomTargetY);
        this.ctx.scale(this.zoomLevel, this.zoomLevel);
        this.hitCtx.save();
        this.hitCtx.translate(this.zoomTargetX, this.zoomTargetY);
        this.hitCtx.scale(this.zoomLevel, this.zoomLevel);
        // get a reference to my player
        const myDetails = this.controller.myPlayerDetails;
        // draw the hex grid
        for (var j = 0; j < hexGrid.length; j++) {
            for (var i = 0; i < hexGrid[j].length; i++) {
                this.drawTile(i, j, hexGrid, true);
            }
        }
        if (myDetails) {
            // draw a shadow over the entire map
            this.ctx.globalAlpha = 0.6;
            this.ctx.fillStyle = "#000";
            this.ctx.fillRect(-this.zoomTargetX / this.zoomLevel, -this.zoomTargetY / this.zoomLevel, this.canvas.width / this.zoomLevel, this.canvas.height / this.zoomLevel);
            this.ctx.globalAlpha = 1.0;
            // determine the which tiles to draw the outline around
            let range = 1; // move range
            if (this.controller.gamePhase === GamePhase_1.GamePhase.ACTION_PHASE) {
                const item = this.controller.myPlayerEquippedItem ?
                    this.controller.items[this.controller.myPlayerEquippedItem.itemID] : null;
                const searchRange = 1;
                const attackRange = item ? item.maxRange : 0;
                range = Math.max(searchRange, attackRange);
            }
            // draw an outline around the movable tiles
            const viewRange = Math.max(2 + this.controller.myPlayerModifiers.viewRange, 0);
            // actionable range must be <= view range
            range = Math.min(range, viewRange);
            const gap = viewRange - range;
            this.drawOuterTilesInViewRange(myDetails, hexGrid, gap);
            this.drawTileOutlines(myDetails, range, hexGrid);
            // redraw the parts of the map in the players vision
            this.drawInnerTilesInViewRange(myDetails, hexGrid, gap);
        }
        // draw the tile selected circle
        if (this.hexSelectedX >= 0 && this.hexSelectedY >= 0) {
            var x = this.hexSelectedX * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * (this.hexSelectedY % 2)) + this.hexRectangleWidth / 2;
            var y = this.hexSelectedY * this.hexRectangleHeight - (this.sideLength / 2 * this.hexSelectedY) + this.hexRectangleHeight / 2;
            this.ctx.save();
            this.ctx.strokeStyle = "#ADFF2F";
            this.ctx.shadowColor = "black";
            this.ctx.shadowBlur = 15;
            this.ctx.shadowOffsetX = 0;
            this.ctx.shadowOffsetY = 0;
            this.ctx.lineWidth = 2 / this.zoomLevel;
            this.drawCircle(this.ctx, x, y, this.hexRadius);
            this.ctx.stroke();
            this.ctx.restore();
        }
        // draw the mouse hover circle & the hover tile tooltip
        if (this.hexHoverX >= 0 && this.hexHoverY >= 0) {
            var x = this.hexHoverX * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * (this.hexHoverY % 2)) + this.hexRectangleWidth / 2;
            var y = this.hexHoverY * this.hexRectangleHeight - (this.sideLength / 2 * this.hexHoverY) + this.hexRectangleHeight / 2;
            this.ctx.save();
            this.ctx.strokeStyle = this.hexHoverValid ? "#3498db" : "#e74c3c";
            this.ctx.shadowColor = "black";
            this.ctx.shadowBlur = 15;
            this.ctx.shadowOffsetX = 0;
            this.ctx.shadowOffsetY = 0;
            this.ctx.lineWidth = 2 / this.zoomLevel;
            this.drawCircle(this.ctx, x, y, this.hexRadius);
            this.ctx.stroke();
            this.ctx.restore();
        }
        // draw all the players my player knows the location of
        for (const playerID in playerDetails) {
            const details = playerDetails[playerID];
            if (details) {
                const moduloMultiplier = details.y % 2;
                const x = details.x * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * moduloMultiplier) + this.hexRectangleWidth / 2;
                const y = details.y * this.hexRectangleHeight - (this.sideLength / 2 * details.y) + this.hexRectangleHeight / 2;
                const imgWidth = this.hexRectangleHeight * 0.75 * 1080 / 1920;
                const imgHeight = this.hexRectangleHeight * 0.75;
                this.ctx.drawImage(this.playerImage, x - imgWidth / 2, y - imgHeight / 2, imgWidth, imgHeight);
            }
        }
        // draw all the display arrows
        for (const arrow of this.displayArrows) {
            const x1 = arrow.fromX * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * (arrow.fromY % 2)) + this.hexRectangleWidth / 2;
            const y1 = arrow.fromY * this.hexRectangleHeight - (this.sideLength / 2 * arrow.fromY) + this.hexRectangleHeight / 2;
            const x4 = arrow.toX * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * (arrow.toY % 2)) + this.hexRectangleWidth / 2;
            const y4 = arrow.toY * this.hexRectangleHeight - (this.sideLength / 2 * arrow.toY) + this.hexRectangleHeight / 2;
            this.ctx.strokeStyle = "#000";
            this.ctx.beginPath();
            this.ctx.moveTo(x1, y1);
            this.ctx.lineTo(x4, y4);
            this.ctx.stroke();
        }
        if (myDetails) {
            // draw all item memento bubbles
            for (const m of this.controller.itemMementos) {
                if (!(m.mapX === myDetails.x && m.mapY === myDetails.y)) {
                    // draw the item info bubble
                    this.drawItemBubble(m);
                }
            }
        }
        this.ctx.restore();
        this.hitCtx.restore();
    }
    drawTile(i, j, hexGrid, drawToHit = false) {
        if (j < 0 || j >= hexGrid.length || i < 0 || i >= hexGrid[j].length) {
            return;
        }
        const tileType = TileType_1.TileType.types[hexGrid[j][i].type];
        var x = i * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * (j % 2));
        var y = j * this.hexRectangleHeight - (this.sideLength / 2 * j);
        // draw visible hexagon
        this.ctx.fillStyle = tileType.color;
        this.ctx.strokeStyle = "#999";
        this.drawHexagon(this.ctx, x, y);
        this.ctx.fill();
        this.ctx.stroke();
        // draw tile image over top of hexagon
        if (tileType.img) {
            var imgWidth = this.hexRectangleHeight;
            var imgHeight = this.hexRectangleHeight;
            if (imgWidth > this.hexRectangleWidth) {
                imgWidth = this.hexRectangleWidth;
                imgHeight = this.hexRectangleWidth;
            }
            this.ctx.drawImage(tileType.img, x, y, imgWidth, imgHeight);
        }
        // draw storm over tile if tile is in the storm
        const stormRadius = this.controller.stormRadius;
        if (stormRadius >= 0) {
            const stormX = Math.floor(hexGrid.length / 2);
            const stormY = Math.floor(hexGrid[0].length / 2);
            //    const iOffset = (stormX+stormY%2) - (i+j%2);
            //    const jOffset = stormY - j;
            if (!this.isPointWithinHexagon(i, j, stormRadius, stormX, stormY)) {
                //if(iOffset*iOffset + jOffset*jOffset > stormRadius*stormRadius) {
                this.ctx.fillStyle = "#ffa500";
                this.ctx.globalAlpha = 0.6;
                this.drawHexagon(this.ctx, x, y);
                this.ctx.fill();
                this.ctx.globalAlpha = 1.0;
            }
        }
        if (drawToHit) {
            // draw hitbox hexagon
            this.hitCtx.fillStyle = hexGrid[j][i].colorKey;
            this.drawHexagon(this.hitCtx, x, y);
            this.hitCtx.fill();
        }
    }
    drawHexagon(context, x, y) {
        context.beginPath();
        context.moveTo(x + this.hexRadius, y);
        context.lineTo(x + this.hexRectangleWidth, y + this.hexHeight);
        context.lineTo(x + this.hexRectangleWidth, y + this.hexHeight + this.sideLength);
        context.lineTo(x + this.hexRadius, y + this.hexRectangleHeight);
        context.lineTo(x, y + this.sideLength + this.hexHeight);
        context.lineTo(x, y + this.hexHeight);
        context.closePath();
    }
    drawCircle(context, x, y, r) {
        context.beginPath();
        context.arc(x, y, r, 0, 2 * Math.PI);
        context.closePath();
    }
    drawItemBubble(m) {
        const i = m.mapX;
        const j = m.mapY;
        const x = i * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * (j % 2)) + this.hexRectangleWidth / 2;
        const y = j * this.hexRectangleHeight - (this.sideLength / 2 * j) + (this.hexRadius / 4);
        const botY = y + this.hexRadius;
        const radius = this.hexRadius / 2;
        // maths class paid off
        const yOffset = radius * Math.sin(Math.PI / 4);
        const xOffset = Math.sqrt(radius * radius - yOffset * yOffset);
        // draw the icon
        this.ctx.save();
        this.ctx.fillStyle = "#555";
        this.ctx.strokeStyle = "#fff";
        this.ctx.globalAlpha = 0.9;
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius, Math.PI / 4, -1.25 * Math.PI, true);
        this.ctx.bezierCurveTo(x - xOffset / 2, (y + botY) / 2, x, botY, x, botY);
        this.ctx.bezierCurveTo(x, botY, x + xOffset / 2, (y + botY) / 2, x + xOffset, y + yOffset);
        this.ctx.fill();
        this.ctx.globalAlpha = 1.0;
        this.ctx.stroke();
        this.ctx.restore();
        // draw the text
        this.ctx.fillStyle = "#00ff00";
        this.ctx.textAlign = "center";
        this.ctx.textBaseline = "middle";
        this.ctx.font = `${radius * 1.5}px Calibri`;
        this.ctx.fillText("" + m.since, x, y);
        // draw a circle to the hit context
        this.hitCtx.fillStyle = m.colorKey;
        this.drawCircle(this.hitCtx, x, y, radius);
        this.hitCtx.fill();
    }
    drawOuterTilesInViewRange(myDetails, hexGrid, gap) {
        const viewRange = Math.max(2 + this.controller.myPlayerModifiers.viewRange, 0);
        const diameter = viewRange * 2 + 1;
        const xOffset = myDetails.y % 2;
        const startY = myDetails.y - viewRange;
        const endY = myDetails.y + viewRange;
        for (let j = startY; j <= endY; j++) {
            // calculate the length of this row based on the vertical distance from the center tile
            const vDistToCenter = Math.abs(j - myDetails.y);
            const rowLength = diameter - vDistToCenter;
            const rowXOffset = (vDistToCenter % 2) * xOffset;
            const startX = myDetails.x - Math.floor(rowLength / 2);
            // if the row is in the outer range, draw whole row, else, draw left and right side only
            if (j < startY + gap || j > endY - gap) {
                // draw whole row
                for (let i = startX; i < startX + rowLength; i++) {
                    this.drawTile(i + rowXOffset, j, hexGrid);
                }
            }
            else {
                // draw the left side of the current row
                for (let i = startX; i < startX + gap; i++) {
                    this.drawTile(i + rowXOffset, j, hexGrid);
                }
                // draw the right side of the current row
                for (let i = startX + rowLength - gap; i < startX + rowLength; i++) {
                    this.drawTile(i + rowXOffset, j, hexGrid);
                }
            }
        }
    }
    drawInnerTilesInViewRange(myDetails, hexGrid, gap) {
        // draw the tiles
        // wow, i can't believe this worked first try...
        // ...this may be the greatest achievement in the history of mathematics
        const viewRange = Math.max(2 + this.controller.myPlayerModifiers.viewRange, 0);
        const diameter = viewRange * 2 + 1;
        const xOffset = myDetails.y % 2;
        for (let j = myDetails.y - viewRange + gap; j <= myDetails.y + viewRange - gap; j++) {
            // calculate the length of this row based on the vertical distance from the center tile
            const vDistToCenter = Math.abs(j - myDetails.y);
            const rowLength = diameter - vDistToCenter;
            const rowXOffset = (vDistToCenter % 2) * xOffset;
            const startX = myDetails.x - Math.floor(rowLength / 2);
            // draw the current row
            for (let i = startX + gap; i < startX + rowLength - gap; i++) {
                this.drawTile(i + rowXOffset, j, hexGrid);
            }
        }
    }
    drawTileOutlines(myDetails, range, hexGrid) {
        const centerX = myDetails.x;
        const centerY = myDetails.y;
        const diameter = range * 2 + 1;
        const xOffset = centerY % 2;
        this.ctx.strokeStyle = "#fff";
        this.ctx.globalAlpha = 1;
        this.ctx.lineWidth = 7.5;
        for (let j = centerY - range; j <= centerY + range; j++) {
            // calculate the length of this row based on the vertical distance from the center tile
            const vDistToCenter = Math.abs(j - centerY);
            const rowLength = diameter - vDistToCenter;
            const rowXOffset = (vDistToCenter % 2) * xOffset;
            const startX = centerX - Math.floor(rowLength / 2);
            for (let i = startX; i < startX + rowLength; i++) {
                var x = (i + rowXOffset) * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * (j % 2));
                var y = j * this.hexRectangleHeight - (this.sideLength / 2 * j);
                this.drawHexagon(this.ctx, x, y);
                this.ctx.stroke();
            }
        }
        this.ctx.lineWidth = 1;
    }
    isPointWithinHexagon(pointX, pointY, outerRadius, circleX, circleY) {
        const diameter = outerRadius * 2 + 1;
        const xOffset = circleY % 2;
        for (let j = circleY - outerRadius; j <= circleY + outerRadius; j++) {
            // calculate the length of this row based on the vertical distance from the center tile
            const vDistToCenter = Math.abs(j - circleY);
            const rowLength = diameter - vDistToCenter;
            const rowXOffset = (vDistToCenter % 2) * xOffset;
            const startX = circleX - Math.floor(rowLength / 2);
            // check whether hexagon x & y are within search range
            for (let i = startX; i < startX + rowLength; i++) {
                if (pointX === i + rowXOffset && pointY === j) {
                    return true;
                }
            }
        }
        return false;
    }
    onMyPlayerDetailsUpdated() {
        // center the player
        const i = this.controller.myPlayerDetails.x;
        const j = this.controller.myPlayerDetails.y;
        var x = i * this.hexRectangleWidth + (this.hexRectangleWidth / 2 * (j % 2)) + this.hexRectangleWidth / 2;
        var y = j * this.hexRectangleHeight - (this.sideLength / 2 * j) + this.hexRectangleHeight / 2;
        this.zoomTargetX = this.canvas.width / 2 - x * this.zoomLevel;
        this.zoomTargetY = this.canvas.height / 2 - y * this.zoomLevel;
    }
    onMoveValid(newX, newY) {
        const myDetails = this.controller.myPlayerDetails;
        if (myDetails) {
            this.displayArrows.length = 0;
            if (myDetails.x !== newX && myDetails.y !== newY) {
                this.displayArrows.push(new MapCanvasArrow_1.MapCanvasArrow(myDetails.x, myDetails.y, newX, newY, "MOVE"));
            }
        }
    }
    onAttackValid(targetID) {
        const myDetails = this.controller.myPlayerDetails;
        const targetDetails = this.controller.playerDetails[targetID];
        // Add an "Attack" arrow from my player to the target player
        if (myDetails && targetDetails) {
            this.displayArrows.push(new MapCanvasArrow_1.MapCanvasArrow(myDetails.x, myDetails.y, targetDetails.x, targetDetails.y, "ATTACK"));
        }
    }
    resize() {
        // Set the internal width & height of the canvases equal to the canvas element's width & height
        this.canvas.setAttribute("width", String(this.canvas.clientWidth));
        this.canvas.setAttribute("height", String(this.canvas.clientHeight));
        this.hitCanvas.setAttribute("width", String(this.hitCanvas.clientWidth));
        this.hitCanvas.setAttribute("height", String(this.hitCanvas.clientHeight));
        this.controller.requestDrawMap();
    }
    onMapLoaded() {
        this.totalMapWidth = this.hexRectangleWidth * (this.controller.map.hexGrid.length + 0.5);
        this.totalMapHeight = this.hexRectangleHeight * (this.controller.map.hexGrid[0].length + 0.5) * 3 / 4;
        this.canvas.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onClick);
        this.canvas.addEventListener("contextmenu", this.onClick);
        this.canvas.addEventListener("mousemove", this.onMouseMove);
        this.canvas.addEventListener("onwheel" in window ? "wheel" : "DOMMouseScroll", this.onMouseWheel, true);
        // set default zoom level to be fully zoomed out
        const canvasToMapWidthRatio = this.canvas.width / this.totalMapWidth;
        const canvasToMapHeightRatio = this.canvas.height / this.totalMapHeight;
        let minZoomLevel = canvasToMapWidthRatio < canvasToMapHeightRatio ? canvasToMapWidthRatio : canvasToMapHeightRatio;
        this.zoomLevel = minZoomLevel;
        // set default zoom to provide complete overview of map
        this.zoomTargetX = this.canvas.width / 2 - this.totalMapWidth / 2 * this.zoomLevel;
        this.zoomTargetY = this.canvas.height / 2 - this.totalMapHeight / 2 * this.zoomLevel;
    }
    // Called when a move phase has begun
    onMovePhaseStart() {
        this.displayArrows.length = 0;
        if (this.controller.myPlayerDetails) {
            this.hexSelectedX = this.controller.myPlayerDetails.x;
            this.hexSelectedY = this.controller.myPlayerDetails.y;
        }
        // Trigger false onMouseMove event to reset mouse cursor icon
        this.onMouseMove({
            clientX: this.lastMouseX + this.canvas.getBoundingClientRect().left,
            clientY: this.lastMouseY + this.canvas.getBoundingClientRect().top
        });
    }
    // Called when an action phase has begun
    onActionPhaseStart() {
        this.displayArrows.length = 0;
        if (this.controller.myPlayerDetails) {
            this.hexSelectedX = this.controller.myPlayerDetails.x;
            this.hexSelectedY = this.controller.myPlayerDetails.y;
        }
        // Trigger false onMouseMove event to reset mouse cursor icon
        this.onMouseMove({
            clientX: this.lastMouseX + this.canvas.getBoundingClientRect().left,
            clientY: this.lastMouseY + this.canvas.getBoundingClientRect().top
        });
    }
    // Called when a phase has finished and the game is now waiting for the server
    onWaitingForServer() {
        this.canvas.style.cursor = "url('assets/Cross_Icon.png') 16 16, auto";
    }
    // Get the hexagon of the hit canvas the mouse is positioned over
    getObjectUnderMouse(e) {
        var mousex = e.clientX - this.hitCanvas.getBoundingClientRect().left;
        var mousey = e.clientY - this.hitCanvas.getBoundingClientRect().top;
        var pixel = this.hitCtx.getImageData(mousex, mousey, 1, 1).data;
        var color = `rgb(${pixel[0]},${pixel[1]},${pixel[2]})`;
        return this.controller.hitMap.get(color);
    }
}
exports.MapCanvas = MapCanvas;


/***/ }),

/***/ "./client_src/gui/MapCanvasArrow.ts":
/*!******************************************!*\
  !*** ./client_src/gui/MapCanvasArrow.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class MapCanvasArrow {
    constructor(fromX, fromY, toX, toY, text) {
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.text = text;
    }
}
exports.MapCanvasArrow = MapCanvasArrow;


/***/ }),

/***/ "./client_src/gui/ModifiersList.ts":
/*!*****************************************!*\
  !*** ./client_src/gui/ModifiersList.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class ModifiersList {
    constructor(controller) {
        this.controller = controller;
        this.onMouseEnter = (e) => {
            e.preventDefault();
            if (e.target.id == "view-range-modifier-div") {
                for (const desc of this.controller.myPlayerModifiers.viewRangeDesc) {
                    let div = document.createElement("div");
                    div.id = "view-range-modifier-desc";
                    div.classList.add("modifiers-list-item");
                    let lbl = document.createElement("label");
                    lbl.classList.add("modifiers-desc-lbl");
                    lbl.innerHTML = desc;
                    div.appendChild(lbl);
                    e.target.appendChild(div);
                }
            }
            else if (e.target.id == "hit-chance-modifier-div") {
                for (const desc of this.controller.myPlayerModifiers.hitChanceDesc) {
                    let div = document.createElement("div");
                    div.id = "hit-chance-modifier-desc";
                    div.classList.add("modifiers-list-item");
                    let lbl = document.createElement("label");
                    lbl.classList.add("modifiers-desc-lbl");
                    lbl.innerHTML = desc;
                    div.appendChild(lbl);
                    e.target.appendChild(div);
                }
            }
            else if (e.target.id == "total-ap-modifier-div") {
                for (const desc of this.controller.myPlayerModifiers.totalAPDesc) {
                    let div = document.createElement("div");
                    div.id = "total-ap-modifier-desc";
                    div.classList.add("modifiers-list-item");
                    let lbl = document.createElement("label");
                    lbl.classList.add("modifiers-desc-lbl");
                    lbl.innerHTML = desc;
                    div.appendChild(lbl);
                    e.target.appendChild(div);
                }
            }
            else if (e.target.id == "ap-move-cost-modifier-div") {
                for (const desc of this.controller.myPlayerModifiers.apMoveCostDesc) {
                    let div = document.createElement("div");
                    div.id = "ap-move-cost-modifier-desc";
                    div.classList.add("modifiers-list-item");
                    let lbl = document.createElement("label");
                    lbl.classList.add("modifiers-desc-lbl");
                    lbl.innerHTML = desc;
                    div.appendChild(lbl);
                    e.target.appendChild(div);
                }
            }
            else if (e.target.id == "dmg-per-move-modifier-div") {
                for (const desc of this.controller.myPlayerModifiers.dmgPerMoveDesc) {
                    let div = document.createElement("div");
                    div.id = "dmg-per-move-modifier-desc";
                    div.classList.add("modifiers-list-item");
                    let lbl = document.createElement("label");
                    lbl.classList.add("modifiers-desc-lbl");
                    lbl.innerHTML = desc;
                    div.appendChild(lbl);
                    e.target.appendChild(div);
                }
            }
            else if (e.target.id == "sick-dmg-per-turn-modifier-div") {
                for (const desc of this.controller.myPlayerModifiers.sickDmgPerTurnDesc) {
                    let div = document.createElement("div");
                    div.id = "sick-dmg-per-turn-modifier-desc";
                    div.classList.add("modifiers-list-item");
                    let lbl = document.createElement("label");
                    lbl.classList.add("modifiers-desc-lbl");
                    lbl.innerHTML = desc;
                    div.appendChild(lbl);
                    e.target.appendChild(div);
                }
            }
        };
        this.onMouseLeave = (e) => {
            e.preventDefault();
            this.refreshDisplay();
        };
        this.modifiersList = document.getElementById("modifiers-list");
    }
    refreshDisplay() {
        let html = "";
        const modifiers = this.controller.myPlayerModifiers;
        if (modifiers.viewRange != 0 || (modifiers.viewRangeDesc && modifiers.viewRangeDesc.length > 0)) {
            const sign = modifiers.viewRange > 0 ? "+" : "";
            const color = sign == "+" ? "#0000b2" : "#b20000";
            html += `
                <div id="view-range-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">View Range</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.viewRange}</label>
                </div>`;
        }
        if (modifiers.hitChance != 0 || (modifiers.hitChanceDesc && modifiers.hitChanceDesc.length > 0)) {
            const sign = modifiers.hitChance > 0 ? "+" : "";
            const color = sign == "+" ? "#0000b2" : "#b20000";
            html += `
                <div id="hit-chance-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Hit Chance</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.hitChance}%</label>
                </div>`;
        }
        if (modifiers.totalAP != 0 || (modifiers.totalAPDesc && modifiers.totalAPDesc.length > 0)) {
            const sign = modifiers.totalAP > 0 ? "+" : "";
            const color = sign == "+" ? "#0000b2" : "#b20000";
            html += `
                <div id="total-ap-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Total AP</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.totalAP}</label>
                </div>`;
        }
        if (modifiers.apMoveCost != 0 || (modifiers.apMoveCostDesc && modifiers.apMoveCostDesc.length > 0)) {
            const sign = modifiers.apMoveCost > 0 ? "+" : "";
            const color = sign == "" ? "#0000b2" : "#b20000";
            html += `
                <div id="ap-move-cost-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Move AP Cost</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.apMoveCost}</label>
                </div>`;
        }
        if (modifiers.dmgPerMove != 0 || (modifiers.dmgPerMoveDesc && modifiers.dmgPerMoveDesc.length > 0)) {
            const sign = modifiers.dmgPerMove > 0 ? "+" : "";
            const color = sign == "" ? "#0000b2" : "#b20000";
            html += `
                <div id="dmg-per-move-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Dmg Per Move</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.dmgPerMove}</label>
                </div>`;
        }
        if (modifiers.sickDmgPerTurn != 0 || (modifiers.sickDmgPerTurnDesc && modifiers.sickDmgPerTurnDesc.length > 0)) {
            const sign = modifiers.sickDmgPerTurn > 0 ? "+" : "";
            const color = sign == "" ? "#0000b2" : "#b20000";
            html += `
                <div id="sick-dmg-per-turn-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Dmg Per Turn</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.sickDmgPerTurn}</label>
                </div>`;
        }
        this.modifiersList.innerHTML = html;
        const viewRangeModifierDiv = document.getElementById("view-range-modifier-div");
        if (viewRangeModifierDiv) {
            viewRangeModifierDiv.addEventListener("mouseleave", this.onMouseLeave);
            viewRangeModifierDiv.addEventListener("mouseenter", this.onMouseEnter);
        }
        const hitChanceModifierDiv = document.getElementById("hit-chance-modifier-div");
        if (hitChanceModifierDiv) {
            hitChanceModifierDiv.addEventListener("mouseleave", this.onMouseLeave);
            hitChanceModifierDiv.addEventListener("mouseenter", this.onMouseEnter);
        }
        const totalAPModifierDiv = document.getElementById("total-ap-modifier-div");
        if (totalAPModifierDiv) {
            totalAPModifierDiv.addEventListener("mouseleave", this.onMouseLeave);
            totalAPModifierDiv.addEventListener("mouseenter", this.onMouseEnter);
        }
        const apMoveCostModifiersDiv = document.getElementById("ap-move-cost-modifier-div");
        if (apMoveCostModifiersDiv) {
            apMoveCostModifiersDiv.addEventListener("mouseleave", this.onMouseLeave);
            apMoveCostModifiersDiv.addEventListener("mouseenter", this.onMouseEnter);
        }
        const dmgPerMoveModifiersDiv = document.getElementById("dmg-per-move-modifier-div");
        if (dmgPerMoveModifiersDiv) {
            dmgPerMoveModifiersDiv.addEventListener("mouseleave", this.onMouseLeave);
            dmgPerMoveModifiersDiv.addEventListener("mouseenter", this.onMouseEnter);
        }
        const sickDmgPerTurnModifiersDiv = document.getElementById("sick-dmg-per-turn-modifier-div");
        if (sickDmgPerTurnModifiersDiv) {
            sickDmgPerTurnModifiersDiv.addEventListener("mouseleave", this.onMouseLeave);
            sickDmgPerTurnModifiersDiv.addEventListener("mouseenter", this.onMouseEnter);
        }
    }
}
exports.ModifiersList = ModifiersList;


/***/ }),

/***/ "./client_src/gui/PlayerList.ts":
/*!**************************************!*\
  !*** ./client_src/gui/PlayerList.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class PlayerList {
    constructor(controller) {
        this.controller = controller;
        this.playerList = document.getElementById("player-list");
    }
    // Update the HTML to reflect changes to player list
    refreshDisplay() {
        let html = "";
        for (const id in this.controller.playerStubs) {
            const player = this.controller.playerStubs[id];
            html += `<button id="player-list-item-${id}"
                        class="player-list-item"
                        style="border-color:${player.Ready ? '#55cc55' : '#cc5555'};">
                        ${player.Name}</button>`;
        }
        this.playerList.innerHTML = html;
    }
    updateListItem(playerID, readyState) {
        console.log("update player " + playerID + ", " + readyState);
        const listItem = document.getElementById(`player-list-item-${playerID}`);
        if (listItem) {
            // update the list item to reflect the update
            if (readyState) {
                listItem.style.borderColor = "#55cc55";
            }
            else {
                listItem.style.borderColor = "#cc5555";
            }
        }
    }
}
exports.PlayerList = PlayerList;


/***/ }),

/***/ "./client_src/gui/RightPanelMobileDiv.ts":
/*!***********************************************!*\
  !*** ./client_src/gui/RightPanelMobileDiv.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Controller_1 = __webpack_require__(/*! ../Controller */ "./client_src/Controller.ts");
class RightPanelMobileDiv {
    constructor(controller) {
        this.controller = controller;
        this.onShowCharBtnClick = (e) => {
            this.showInventBtn.style.display = "block";
            this.showCharBtn.style.display = "none";
            this.inventDiv.style.display = "none";
            this.charDiv.style.display = "block";
        };
        this.onShowInventBtnClick = (e) => {
            this.showInventBtn.style.display = "none";
            this.showCharBtn.style.display = "block";
            this.inventDiv.style.display = "block";
            this.charDiv.style.display = "none";
        };
        this.showCharBtn = document.getElementById("show-char-btn");
        this.showInventBtn = document.getElementById("show-invent-btn");
        this.charDiv = document.getElementById("char-div");
        this.inventDiv = document.getElementById("invent-div");
        this.rightPanel = document.getElementById("right-panel");
        this.showCharBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onShowCharBtnClick);
        this.showInventBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onShowInventBtnClick);
    }
    hide() {
        this.rightPanel.style.visibility = "hidden";
    }
}
exports.RightPanelMobileDiv = RightPanelMobileDiv;


/***/ }),

/***/ "./client_src/gui/ToggleReadyBtn.ts":
/*!******************************************!*\
  !*** ./client_src/gui/ToggleReadyBtn.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const Controller_1 = __webpack_require__(/*! ../Controller */ "./client_src/Controller.ts");
class ToggleReadyBtn {
    constructor(controller) {
        this.controller = controller;
        this.onClick = (e) => {
            this.controller.toggleReadyState();
        };
        this.toggleReadyBtn = document.getElementById("toggle-ready-btn");
        this.toggleReadyBtn.addEventListener(Controller_1.Controller.CLICK_EVENT, this.onClick);
    }
    update() {
        console.log("update toggle ready btn", this.controller.myPlayerStub);
        if (!this.controller.myPlayerStub.Ready) {
            this.show();
            this.enable();
        }
        else {
            this.disable();
            this.hide();
        }
    }
    hide() {
        this.toggleReadyBtn.style.visibility = "hidden";
    }
    show() {
        this.toggleReadyBtn.style.visibility = "visible";
    }
    disable() {
        this.toggleReadyBtn.disabled = true;
    }
    enable() {
        this.toggleReadyBtn.disabled = false;
    }
    isVisible() {
        return this.toggleReadyBtn.style.visibility == "visible";
    }
}
exports.ToggleReadyBtn = ToggleReadyBtn;


/***/ }),

/***/ "./client_src/gui/Tooltip.ts":
/*!***********************************!*\
  !*** ./client_src/gui/Tooltip.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const TileType_1 = __webpack_require__(/*! ../game/TileType */ "./client_src/game/TileType.ts");
const GamePhase_1 = __webpack_require__(/*! ../game/GamePhase */ "./client_src/game/GamePhase.ts");
const Action_1 = __webpack_require__(/*! ../game/Action */ "./client_src/game/Action.ts");
const Item_1 = __webpack_require__(/*! ../game/Item */ "./client_src/game/Item.ts");
class Tooltip {
    constructor(controller) {
        this.controller = controller;
        this.tooltipDiv = document.getElementById("tooltip-div");
    }
    hideTooltip() {
        this.tooltipDiv.style.visibility = "hidden";
    }
    show(x, y) {
        // set the position of the tooltip
        this.tooltipDiv.style.left = `calc(${x}px - ${this.tooltipDiv.clientWidth / 2}px)`;
        this.tooltipDiv.style.top = `${y + 25}px`;
        // make sure the tooltip is visible
        this.tooltipDiv.style.visibility = "visible";
    }
    clear() {
        // clear contents of div
        while (this.tooltipDiv.firstChild) {
            this.tooltipDiv.removeChild(this.tooltipDiv.firstChild);
        }
    }
    showDropTileTooltip(i, j, x, y) {
        if (i >= 0 && j >= 0) {
            // get the tile type of the tile hovered
            const tileType = TileType_1.TileType.types[this.controller.map.hexGrid[j][i].type];
            this.clear();
            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = tileType.name;
            this.tooltipDiv.appendChild(nameLbl);
            if (tileType.impassable || tileType.undroppable) {
                let lbl = document.createElement("label");
                lbl.innerHTML = "You can't land here";
                this.tooltipDiv.appendChild(lbl);
            }
            else {
                let lbl = document.createElement("label");
                lbl.innerHTML = "Click to set Drop Location";
                this.tooltipDiv.appendChild(lbl);
            }
            this.show(x, y);
        }
        else {
            this.hideTooltip();
        }
    }
    showTileTooltip(i, j, x, y) {
        if (i >= 0 && j >= 0) {
            // get the tile type of the tile hovered
            const tileType = TileType_1.TileType.types[this.controller.map.hexGrid[j][i].type];
            this.clear();
            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = tileType.name;
            this.tooltipDiv.appendChild(nameLbl);
            for (const desc of tileType.desc) {
                let descLbl = document.createElement("label");
                descLbl.innerHTML = desc;
                this.tooltipDiv.appendChild(descLbl);
            }
            this.show(x, y);
        }
        else {
            this.hideTooltip();
        }
    }
    showActionTooltip(name, desc, apCost, x, y) {
        if (name != null && desc != null) {
            // clear contents of div
            this.clear();
            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = name;
            nameLbl.style.display = "inline";
            this.tooltipDiv.appendChild(nameLbl);
            let apLbl = document.createElement("label");
            apLbl.innerHTML = `${apCost}AP`;
            apLbl.style.display = "inline";
            apLbl.style.color = "#0000e5";
            this.tooltipDiv.appendChild(apLbl);
            let descLbl = document.createElement("label");
            descLbl.innerHTML = desc;
            this.tooltipDiv.appendChild(descLbl);
            this.show(x, y);
        }
        else {
            this.hideTooltip();
        }
    }
    showItemTooltip(entity, x, y) {
        if (!entity) {
            return;
        }
        const item = this.controller.items[entity.itemID];
        if (item) {
            // clear contents of div
            this.clear();
            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = item.name;
            nameLbl.style.display = "inline";
            this.tooltipDiv.appendChild(nameLbl);
            // if the item is a gun, include gun specific info
            if (Item_1.isGun(item)) {
                const gun = item;
                if ((this.controller.myPlayerEquippedItem &&
                    entity.entityID === this.controller.myPlayerEquippedItem.entityID) ||
                    this.controller.gamePhase === GamePhase_1.GamePhase.ACTION_PHASE) {
                    let equipLbl = document.createElement("label");
                    equipLbl.innerHTML = this.controller.myPlayerEquippedItem &&
                        entity.entityID === this.controller.myPlayerEquippedItem.entityID ?
                        "Equipped" : "Click To Equip " + Action_1.ActionApCosts.EQUIP_WEAPON + "AP";
                    equipLbl.style.display = "inline";
                    equipLbl.style.color = "#0000e5";
                    this.tooltipDiv.appendChild(equipLbl);
                }
                let maxRangeLbl = document.createElement("label");
                maxRangeLbl.innerHTML = "Max Range <span style='color:#0000e5'>" + gun.maxRange + "</span>";
                maxRangeLbl.style.display = "block";
                this.tooltipDiv.appendChild(maxRangeLbl);
                let accuracyLbl = document.createElement("label");
                accuracyLbl.innerHTML = "Accuracy <span style='color:#0000e5'>" + Math.floor(gun.accuracy * 100 + 0.5) + "%</span>";
                accuracyLbl.style.display = "block";
                this.tooltipDiv.appendChild(accuracyLbl);
            }
            else if (Item_1.isBandage(item)) {
                const bandage = item;
                let healAmountLbl = document.createElement("label");
                healAmountLbl.innerHTML = "Heal Amount <span style='color:#0000e5'>" + bandage.healAmount + "</span>";
                healAmountLbl.style.display = "block";
                this.tooltipDiv.appendChild(healAmountLbl);
            }
            this.show(x, y);
        }
        else {
            this.hideTooltip();
        }
    }
    showItemMementoTooltip(mem, x, y) {
        if (mem) {
            this.clear();
            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = "Last Visited " + mem.since + " Round(s) Ago";
            nameLbl.style.display = "inline";
            this.tooltipDiv.appendChild(nameLbl);
            // add an icon for each item
            for (const item of mem.items) {
                let img = document.createElement("img");
                img.classList.add("item-found-img");
                img.src = item.image;
                img.title = item.name;
                img.style.width = `${this.tooltipDiv.clientWidth / 3 * item.gridWidth}px`;
                img.style.height = `${this.tooltipDiv.clientWidth / 3 * item.gridHeight}px`;
                this.tooltipDiv.appendChild(img);
            }
            this.show(x, y);
        }
        else {
            this.hideTooltip();
        }
    }
}
exports.Tooltip = Tooltip;


/***/ }),

/***/ "./client_src/gui/TurnLbl.ts":
/*!***********************************!*\
  !*** ./client_src/gui/TurnLbl.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class TurnLbl {
    constructor(controller) {
        this.controller = controller;
        this.titleLbl = document.getElementById("title-lbl");
        this.instructionsLbl = document.getElementById("instructions-lbl");
    }
    setTitleText(text) {
        if (text != null) {
            this.titleLbl.innerHTML = text;
        }
    }
    setInstructionsText(text) {
        if (text != null) {
            this.instructionsLbl.innerHTML = text;
        }
    }
}
exports.TurnLbl = TurnLbl;


/***/ }),

/***/ "./client_src/gui/ViewportDamageIndicator.ts":
/*!***************************************************!*\
  !*** ./client_src/gui/ViewportDamageIndicator.ts ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class ViewportDamageIndicator {
    constructor(controller) {
        this.controller = controller;
        this.bloodTriangleTopLeft = document.getElementById("blood-triangle-top-left");
        this.bloodTriangleTopRight = document.getElementById("blood-triangle-top-right");
        this.bloodTriangleBottomLeft = document.getElementById("blood-triangle-bottom-left");
        this.bloodTriangleBottomRight = document.getElementById("blood-triangle-bottom-right");
    }
    refreshDisplay(totalHPChangedBy) {
        const percentage = -totalHPChangedBy * 1.5 > 50 ? 50 : -totalHPChangedBy * 1.5;
        this.bloodTriangleTopLeft.style.visibility = "hidden";
        this.bloodTriangleTopLeft.style.width = `${percentage}%`;
        this.bloodTriangleTopLeft.style.height = `${percentage}%`;
        this.bloodTriangleTopLeft.classList.remove("blood-splat-dissappear");
        void this.bloodTriangleTopLeft.offsetWidth;
        this.bloodTriangleTopLeft.classList.add("blood-splat-dissappear");
        this.bloodTriangleTopRight.style.visibility = "hidden";
        this.bloodTriangleTopRight.style.width = `${percentage}%`;
        this.bloodTriangleTopRight.style.height = `${percentage}%`;
        this.bloodTriangleTopRight.classList.remove("blood-splat-dissappear");
        void this.bloodTriangleTopRight.offsetWidth;
        this.bloodTriangleTopRight.classList.add("blood-splat-dissappear");
        this.bloodTriangleBottomLeft.style.visibility = "hidden";
        this.bloodTriangleBottomLeft.style.width = `${percentage}%`;
        this.bloodTriangleBottomLeft.style.height = `${percentage}%`;
        this.bloodTriangleBottomLeft.classList.remove("blood-splat-dissappear");
        void this.bloodTriangleBottomLeft.offsetWidth;
        this.bloodTriangleBottomLeft.classList.add("blood-splat-dissappear");
        this.bloodTriangleBottomRight.style.visibility = "hidden";
        this.bloodTriangleBottomRight.style.width = `${percentage}%`;
        this.bloodTriangleBottomRight.style.height = `${percentage}%`;
        this.bloodTriangleBottomRight.classList.remove("blood-splat-dissappear");
        void this.bloodTriangleBottomRight.offsetWidth;
        this.bloodTriangleBottomRight.classList.add("blood-splat-dissappear");
    }
}
exports.ViewportDamageIndicator = ViewportDamageIndicator;


/***/ }),

/***/ "./client_src/networking/Client.ts":
/*!*****************************************!*\
  !*** ./client_src/networking/Client.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const HTTPHandler_1 = __webpack_require__(/*! ./HTTPHandler */ "./client_src/networking/HTTPHandler.ts");
const Map_1 = __webpack_require__(/*! ../game/Map */ "./client_src/game/Map.ts");
class Client {
    constructor(controller) {
        this.controller = controller;
        this.connectToWebSocketServer();
    }
    connectToWebSocketServer() {
        this.socket = new WebSocket(`ws://${location.hostname}/open_ws_connection`);
        this.socket.onopen = (e) => {
            this.socket.send(`Aname:${Cookies.get("player_name")};token:${Cookies.get("player_token")};lobby:${Cookies.get("lobby_name")}`);
        };
        this.socket.onmessage = (e) => {
            this.onMessage(e.data);
        };
    }
    sendToggleReadyStateMsg() {
        this.socket.send(`B`);
    }
    sendSetReadyStateToTrueMsg() {
        this.socket.send(`C`);
    }
    sendSetMoveTurnMsg(x, y) {
        this.socket.send(`Gcrouch:false;x:${x};y:${y}`);
    }
    sendSearchTileMsg(x, y) {
        this.socket.send(`Mx:${x};y:${y}`);
    }
    sendTakeItemMsg(entityID, backpackX, backpackY) {
        this.socket.send(`HentityID:${entityID};backpackX:${backpackX};backpackY:${backpackY}`);
    }
    sendEquipItemMsg(entityID) {
        this.socket.send(`IentityID:${entityID}`);
    }
    sendDropItemMsg(entityID) {
        this.socket.send(`KentityID:${entityID}`);
    }
    sendUseBandageMsg(entityID, limb) {
        this.socket.send(`LentityID:${entityID};limb:${limb}`);
    }
    sendAttackPlayerMsg(playerID, limb) {
        this.socket.send(`JplayerID:${playerID};limb:${limb}`);
    }
    sendEndTurnMsg() {
        this.socket.send(`D`);
    }
    sendSendMsgMsg(msg) {
        this.socket.send(`Emsg:${msg}`);
    }
    // Called upon receiving a message from the server
    onMessage(msg) {
        console.log("received: " + msg);
        if (msg == "update:players") {
            this.processPlayersUpdate();
        }
        else if (msg.startsWith("update:ready;")) {
            this.processReadyStateUpdate(msg);
        }
        else if (msg.startsWith("update:game;")) {
            this.processGameUpdate(msg);
        }
        else if (msg.startsWith("update:dead;")) {
            this.processDeadPlayersUpdate(msg);
        }
        else if (msg.startsWith("update:winners;")) {
            this.processWinnersUpdate(msg);
        }
        else if (msg.startsWith("update:storm;")) {
            this.processStormUpdate(msg);
        }
        else if (msg.startsWith("yourid;")) {
            this.processMyIDUpdate(msg);
        }
        else if (msg.startsWith("phasetime;")) {
            this.processPhaseTimeUpdate(msg);
        }
        else if (msg.startsWith("map;")) {
            this.processMapUpdate(msg);
        }
        else if (msg.startsWith("items;")) {
            this.processItemsList(msg);
        }
        else if (msg == "request:drop") {
            this.processDropPhase();
        }
        else if (msg == "request:move") {
            this.processMovePhase();
        }
        else if (msg == "request:action") {
            this.processActionPhase();
        }
        else if (msg.startsWith("takeitem:valid;")) {
            this.processTakeItemValid(msg);
        }
        else if (msg == "takeitem:invalid;") {
            console.log("take item invalid");
        }
        else if (msg.startsWith("equipitem:valid;")) {
            this.processEquipItemValid(msg);
        }
        else if (msg == "equipitem:invalid;") {
            console.log("equip item invalid");
        }
        else if (msg.startsWith("dropitem:valid;")) {
            this.processDropItemValid(msg);
        }
        else if (msg == "dropitem:invalid;") {
            console.log("drop item invalid");
        }
        else if (msg == "move:valid") {
            console.log("move valid");
        }
        else if (msg == "move:invalid") {
            console.log("move invalid");
        }
        else if (msg == "attack:valid") {
            this.controller.onAttackValid();
        }
        else if (msg == "attack:invalid") {
            console.log("attack invalid");
        }
        else if (msg.startsWith("usebandage:valid;")) {
            this.processUseBandageValid(msg);
        }
        else if (msg == "usebandage:invalid;") {
            console.log("use bandage invalid");
        }
        else if (msg == "search:valid;") {
            this.controller.onSearchValid();
        }
        else if (msg == "search:invalid;") {
            console.log("search invalid");
        }
        else if (msg.startsWith("disconnect;")) {
            this.processDisconnect(msg);
        }
        else if (msg.startsWith("message;")) {
            this.processPlayerMsg(msg);
        }
    }
    processDropPhase() {
        this.controller.onDropPhaseStart();
    }
    processMovePhase() {
        this.controller.onMovePhaseStart();
    }
    processActionPhase() {
        this.controller.onActionPhaseStart();
    }
    processGameUpdate(msg) {
        var data = msg.split("update:game;").length > 1 ? msg.split("update:game;")[1] : null;
        if (data) {
            const splits = data.split(";");
            let items = new Array();
            let newPlayerDetails = {};
            let newPlayerHP = {};
            // extract relevant information from data string
            for (const elem of splits) {
                if (elem.startsWith("details:player,")) {
                    const details = elem.split("details:player,");
                    if (details.length > 1) {
                        const p = this.processPlayerDetailsUpdate(details[1]);
                        if (p && p.id && p.details) {
                            newPlayerDetails[p.id] = p.details;
                        }
                    }
                }
                else if (elem.startsWith("details:item,")) {
                    const details = elem.split("details:item,");
                    if (details.length > 1) {
                        const item = this.processItemsFound(details[1]);
                        if (item) {
                            items.push(item);
                        }
                    }
                }
                else if (elem.startsWith("details:modifiers,")) {
                    const details = elem.split("details:modifiers,");
                    if (details.length > 1) {
                        this.processMyPlayerModifiersUpdate(details[1]);
                    }
                }
                else if (elem.startsWith("details:hp,")) {
                    const details = elem.split("details:hp,");
                    if (details.length > 1) {
                        const p = this.processPlayerHPUpdate(details[1]);
                        if (p && p.id && p.hp) {
                            newPlayerHP[p.id] = p.hp;
                        }
                    }
                }
            }
            this.controller.playerDetails = newPlayerDetails;
            this.controller.playerHPs = newPlayerHP;
            this.controller.setItemsFound(items);
            this.controller.requestDrawMap();
        }
    }
    // Process list of dead players
    processDeadPlayersUpdate(msg) {
        const data = msg.split(";");
        for (let index = 1; index < data.length; index++) {
            const playerID = parseInt(data[index], 10);
            this.controller.onPlayerDead(playerID);
        }
    }
    // Process list of winners
    processWinnersUpdate(msg) {
        const data = msg.split(";");
        let winnerIDs = new Array();
        for (let index = 1; index < data.length; index++) {
            const playerID = parseInt(data[index], 10);
            winnerIDs.push(playerID);
        }
        this.controller.onGameOver(winnerIDs);
    }
    // Process update to the nuclear storm
    processStormUpdate(msg) {
        const data = this.dissectMultiPartMsg(msg, "update:storm;");
        if (data) {
            if (data.approach) {
                this.controller.onStormApproachUpdate();
            }
            if (data.radius) {
                const radius = parseInt(data.radius, 10);
                if (radius != null) {
                    this.controller.onStormRadiusUpdate(radius);
                }
            }
            if (data.dmg && data.sickDmg) {
                const dmgPerTurn = parseInt(data.dmg, 10);
                const sickDmgPerTurn = parseInt(data.sickdmg, 10);
                if (dmgPerTurn != null && sickDmgPerTurn != null) {
                    this.controller.onStormDmgUpdate(dmgPerTurn, sickDmgPerTurn);
                }
            }
        }
    }
    // Process an individual player's details update
    processPlayerDetailsUpdate(elem) {
        const data = this.dissectMultiPartMsg(elem, null, ",");
        if (data) {
            if (data.id && data.x && data.y && data.c) {
                const x = parseInt(data.x, 10);
                const y = parseInt(data.y, 10);
                const crouch = data.crouch == "true";
                return { id: data.id, details: { x: x, y: y, crouch: crouch } };
            }
        }
        return null;
    }
    // Process an update to the items on the current tile
    processItemsFound(elem) {
        const data = this.dissectMultiPartMsg(elem, null, ",");
        if (data) {
            if (data.entityID && data.itemID) {
                console.log("found item", data.entityID, data.itemID);
                return { entityID: data.entityID, itemID: data.itemID };
            }
        }
        return null;
    }
    // Process an update to my player's modifiers list
    processMyPlayerModifiersUpdate(elem) {
        const data = this.dissectMultiPartMsg(elem, null, ",");
        if (data) {
            if (data.viewRange && data.hitChance && data.viewRangeDesc != null && data.hitChanceDesc != null) {
                const viewRange = parseInt(data.viewRange, 10);
                const hitChance = parseInt(data.hitChance, 10);
                const totalAP = parseInt(data.totalAP, 10);
                const apMoveCost = parseInt(data.apMoveCost, 10);
                const dmgPerMove = parseInt(data.dmgPerMove, 10);
                const sickDmgPerTurn = parseInt(data.sickDmgPerTurn, 10);
                let viewRangeDesc = new Array();
                let hitChanceDesc = new Array();
                let totalAPDesc = new Array();
                let apMoveCostDesc = new Array();
                let dmgPerMoveDesc = new Array();
                let sickDmgPerTurnDesc = new Array();
                // get the reasons for the view range modifier
                for (let desc of data.viewRangeDesc.split("~")) {
                    if (desc)
                        viewRangeDesc.push(desc);
                }
                // get the reasons for the hit chance modifier
                for (let desc of data.hitChanceDesc.split("~")) {
                    if (desc)
                        hitChanceDesc.push(desc);
                }
                // get the reasons for the total ap modifier
                for (let desc of data.totalAPDesc.split("~")) {
                    if (desc)
                        totalAPDesc.push(desc);
                }
                // get the reasons for the ap move cost modifier
                for (let desc of data.apMoveCostDesc.split("~")) {
                    if (desc)
                        apMoveCostDesc.push(desc);
                }
                // get the reasons for the dmg per move modifier
                for (let desc of data.dmgPerMoveDesc.split("~")) {
                    if (desc)
                        dmgPerMoveDesc.push(desc);
                }
                // get the reasons for the sickness dmg per turn modifier
                for (let desc of data.sickDmgPerTurnDesc.split("~")) {
                    if (desc)
                        sickDmgPerTurnDesc.push(desc);
                }
                // update my players modifiers with the new information
                this.controller.myPlayerModifiers = {
                    viewRange: viewRange,
                    hitChance: hitChance,
                    totalAP: totalAP,
                    apMoveCost: apMoveCost,
                    dmgPerMove: dmgPerMove,
                    sickDmgPerTurn: sickDmgPerTurn,
                    viewRangeDesc: viewRangeDesc,
                    hitChanceDesc: hitChanceDesc,
                    totalAPDesc: totalAPDesc,
                    apMoveCostDesc: apMoveCostDesc,
                    dmgPerMoveDesc: dmgPerMoveDesc,
                    sickDmgPerTurnDesc: sickDmgPerTurnDesc
                };
            }
        }
    }
    // Process an update to my player's health
    processPlayerHPUpdate(elem) {
        const data = this.dissectMultiPartMsg(elem, null, ",");
        if (data) {
            if (data.id && data.total && data.head && data.chest && data.rarm && data.larm && data.rleg && data.lleg) {
                const playerID = data.id;
                const total = parseInt(data.total, 10);
                const head = parseInt(data.head, 10);
                const chest = parseInt(data.chest, 10);
                const rarm = parseInt(data.rarm, 10);
                const larm = parseInt(data.larm, 10);
                const rleg = parseInt(data.rleg, 10);
                const lleg = parseInt(data.lleg, 10);
                // update the players hp with the new information
                return {
                    id: playerID,
                    hp: {
                        total: total,
                        head: head,
                        chest: chest,
                        rarm: rarm,
                        larm: larm,
                        rleg: rleg,
                        lleg: lleg
                    }
                };
            }
        }
        return null;
    }
    // Process an update to the player list
    processPlayersUpdate() {
        HTTPHandler_1.fetchPlayerList(Cookies.get("lobby_name"), (xmlHttp) => {
            // on success
            const p = JSON.parse(xmlHttp.responseText);
            if (p) {
                this.controller.playerStubs = p;
            }
        }, (xmlHttp) => {
            // on error
            console.log(xmlHttp.responseText);
        });
    }
    // Process a valid take item request
    processTakeItemValid(msg) {
        const data = this.dissectMultiPartMsg(msg, "takeitem:valid;");
        if (data) {
            // message must contain entity id and item id to be usable ...
            if (data.entityID && data.itemID && data.firstSlotX && data.firstSlotY) {
                const entityID = parseInt(data.entityID, 10);
                const itemID = parseInt(data.itemID, 10);
                const firstSlotX = parseInt(data.firstSlotX, 10);
                const firstSlotY = parseInt(data.firstSlotY, 10);
                this.controller.onTakeItemValid(entityID, itemID, firstSlotX, firstSlotY);
            }
        }
    }
    // Process a valid equip item request
    processEquipItemValid(msg) {
        const data = this.dissectMultiPartMsg(msg, "equipitem:valid;");
        if (data) {
            // message must contain entity id and item id to be usable
            if (data.entityID && data.itemID) {
                const entityID = parseInt(data.entityID, 10);
                const itemID = parseInt(data.itemID, 10);
                this.controller.onEquipItemValid(entityID, itemID);
            }
        }
    }
    // Process a valid drop item request
    processDropItemValid(msg) {
        const data = this.dissectMultiPartMsg(msg, "dropitem:valid;");
        if (data) {
            // message must contain entity id and item id to be usable
            if (data.entityID && data.itemID) {
                const entityID = parseInt(data.entityID, 10);
                const itemID = parseInt(data.itemID, 10);
                this.controller.onDropItemValid(entityID, itemID);
            }
        }
    }
    // Process a valid use bandage request
    processUseBandageValid(msg) {
        const data = this.dissectMultiPartMsg(msg, "usebandage:valid;");
        if (data) {
            if (data.entityID && data.itemID) {
                const entityID = parseInt(data.entityID, 10);
                const itemID = parseInt(data.itemID, 10);
                this.controller.onUseBandageValid(entityID, itemID);
            }
        }
    }
    // Dissect a ready state update message and make relevant GUI changes
    processReadyStateUpdate(msg) {
        const data = this.dissectMultiPartMsg(msg, "update:ready;");
        if (data) {
            // message must contain player and state to be usable
            if (data.player && data.ready) {
                this.controller.onReadyStateUpdate(data.player, data.ready == "true" ? true : false);
            }
        }
    }
    // Called when a phase time update is received from the server
    processPhaseTimeUpdate(msg) {
        const data = this.dissectMultiPartMsg(msg, "phasetime;");
        if (data) {
            // message must contain droptime, movetime and actiontime to be usable
            if (data.droptime && data.movetime && data.actiontime) {
                this.controller.onPhaseTimesUpdate(data.droptime, data.movetime, data.actiontime);
            }
        }
    }
    // Called when the name of the next map to be played is received from the server
    processMapUpdate(msg) {
        const data = this.dissectMultiPartMsg(msg, "map;");
        // message must contain mapName to be usable
        if (data.name) {
            // fetch the map from the server
            HTTPHandler_1.fetchMap(data.name, (xmlHttp) => {
                // on success
                const m = new Map_1.Map(this.controller, xmlHttp.responseText);
                this.controller.map = m;
                this.controller.onMapLoaded();
            }, (xmlHttp) => {
                // on error
                alert("Fatal Error: Failed to load map " + data.mapName);
            });
        }
    }
    // Called when the item list for this game is received from the server
    processItemsList(msg) {
        const data = msg.split("items;").length > 1 ? msg.split("items;")[1] : null;
        if (data) {
            const splits = data.split(";");
            let items = new Array();
            // extract relevant information from data string
            for (const elem of splits) {
                const itemObj = this.dissectMultiPartMsg(elem, null, ",");
                let item;
                if (itemObj.shotsPerAction) {
                    item = {
                        name: itemObj.name,
                        image: itemObj.image,
                        gridWidth: parseInt(itemObj.gridw, 10) || 1,
                        gridHeight: parseInt(itemObj.gridh, 10) || 1,
                        shotsPerAction: parseFloat(itemObj.shotsPerAction) || 0,
                        dmgPerHit: parseFloat(itemObj.shotsPerAction) || 0,
                        maxRange: parseFloat(itemObj.maxRange) || 0,
                        accuracy: parseFloat(itemObj.accuracy) || 0
                    };
                }
                else if (itemObj.timeToThrow) {
                    item = {
                        name: itemObj.name,
                        image: itemObj.image,
                        gridWidth: parseInt(itemObj.gridw, 10) || 1,
                        gridHeight: parseInt(itemObj.gridh, 10) || 1,
                        timeToThrow: parseFloat(itemObj.timeToThrow) || 0,
                        dmgPerHit: parseFloat(itemObj.dmgPerHit) || 0
                    };
                }
                else if (itemObj.healAmount) {
                    item = {
                        name: itemObj.name,
                        image: itemObj.image,
                        gridWidth: parseInt(itemObj.gridw, 10) || 1,
                        gridHeight: parseInt(itemObj.gridh, 10) || 1,
                        healAmount: parseInt(itemObj.healAmount, 10) || 0
                    };
                }
                items.push(item);
            }
            this.controller.onItemsLoaded(items);
        }
    }
    // Called when the ID of the player using this client is received from the server
    processMyIDUpdate(msg) {
        const data = this.dissectMultiPartMsg(msg, "yourid;");
        if (data) {
            if (data.id) {
                this.controller.onMyIDUpdate(data.id);
            }
        }
    }
    // Called when a player disconnects from the game
    processDisconnect(msg) {
        const data = this.dissectMultiPartMsg(msg, "disconnect;");
        if (data && data.id) {
            this.controller.onPlayerDisconnect(data.id);
        }
    }
    // Called upon receiving a msg from another player
    processPlayerMsg(msg) {
        const data = this.dissectMultiPartMsg(msg, "message;");
        if (data && data.id && data.msg) {
            this.controller.onMsgReceived(data.id, data.msg);
        }
    }
    dissectMultiPartMsg(msg, cutAt, sep = ";") {
        const data = msg.split(cutAt).length > 1 ? msg.split(cutAt)[1] : msg;
        let map = {};
        if (data) {
            const splits = data.split(sep);
            // extract relevant information from data string
            for (const elem of splits) {
                const parts = elem.split(":");
                if (parts.length > 1) {
                    const key = parts[0];
                    const value = parts[1];
                    map[key] = value;
                }
            }
        }
        return map;
    }
}
exports.Client = Client;


/***/ }),

/***/ "./client_src/networking/HTTPHandler.ts":
/*!**********************************************!*\
  !*** ./client_src/networking/HTTPHandler.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
function sendGetRequest(URL, success, error) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4) {
            if (xmlHttp.status >= 200 && xmlHttp.status < 300) {
                if (typeof (success) === "function") {
                    success(xmlHttp);
                }
            }
            else {
                if (typeof (error) === "function") {
                    error(xmlHttp);
                }
            }
        }
    };
    // send the ajax request
    xmlHttp.open("GET", URL, true); // true for asynchronous
    xmlHttp.send(null);
}
function fetchMap(mapName, success, error) {
    sendGetRequest(`/maps/${mapName}.txt`, (xmlHttp) => {
        // on success
        if (success) {
            success(xmlHttp);
        }
    }, (xmlHttp) => {
        // on error
        if (error) {
            error(xmlHttp);
        }
    });
}
exports.fetchMap = fetchMap;
function fetchPlayerList(lobbyName, success, error) {
    sendGetRequest(`/players?lobby_name=${lobbyName}`, (xmlHttp) => {
        // on success
        if (success) {
            success(xmlHttp);
        }
    }, (xmlHttp) => {
        // on error
        if (error) {
            error(xmlHttp);
        }
    });
}
exports.fetchPlayerList = fetchPlayerList;


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9Db250cm9sbGVyLnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvTWFpbi50cyIsIndlYnBhY2s6Ly8vLi9jbGllbnRfc3JjL2dhbWUvQWN0aW9uLnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvZ2FtZS9HYW1lUGhhc2UudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9nYW1lL0hpdE1hcC50cyIsIndlYnBhY2s6Ly8vLi9jbGllbnRfc3JjL2dhbWUvSW52ZW50b3J5LnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvZ2FtZS9JdGVtLnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvZ2FtZS9JdGVtTWVtZW50by50cyIsIndlYnBhY2s6Ly8vLi9jbGllbnRfc3JjL2dhbWUvTWFwLnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvZ2FtZS9UaWxlVHlwZS50cyIsIndlYnBhY2s6Ly8vLi9jbGllbnRfc3JjL2d1aS9BY3Rpb25MaXN0LnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvZ3VpL0F0dGFja0NoYXJEaXYudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvQ2VudGVyTGJsLnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvZ3VpL0NoYXJhY3RlclBhbmVsLnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvZ3VpL0NoYXRXaW5kb3cudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvRHJvcGRvd25EaXYudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvRW5kVHVybkJ0bi50cyIsIndlYnBhY2s6Ly8vLi9jbGllbnRfc3JjL2d1aS9GdWxsT3ZlcmxheS50cyIsIndlYnBhY2s6Ly8vLi9jbGllbnRfc3JjL2d1aS9HVUkudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvSW52ZW50b3J5UGFuZWwudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvSXRlbUFjdGlvbkxpc3QudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvSXRlbXNGb3VuZERpdi50cyIsIndlYnBhY2s6Ly8vLi9jbGllbnRfc3JjL2d1aS9NYXBDYW52YXMudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvTWFwQ2FudmFzQXJyb3cudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvTW9kaWZpZXJzTGlzdC50cyIsIndlYnBhY2s6Ly8vLi9jbGllbnRfc3JjL2d1aS9QbGF5ZXJMaXN0LnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvZ3VpL1JpZ2h0UGFuZWxNb2JpbGVEaXYudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvVG9nZ2xlUmVhZHlCdG4udHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9ndWkvVG9vbHRpcC50cyIsIndlYnBhY2s6Ly8vLi9jbGllbnRfc3JjL2d1aS9UdXJuTGJsLnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvZ3VpL1ZpZXdwb3J0RGFtYWdlSW5kaWNhdG9yLnRzIiwid2VicGFjazovLy8uL2NsaWVudF9zcmMvbmV0d29ya2luZy9DbGllbnQudHMiLCJ3ZWJwYWNrOi8vLy4vY2xpZW50X3NyYy9uZXR3b3JraW5nL0hUVFBIYW5kbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHlEQUFpRCxjQUFjO0FBQy9EOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOzs7QUFHQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7QUNuRUEsOEVBQStCO0FBQy9CLHFHQUE0QztBQU01Qyx5RkFBc0M7QUFDdEMsa0dBQTRDO0FBQzVDLG1GQUE4QztBQUM5QyxrR0FBNEM7QUFDNUMsd0dBQWdEO0FBQ2hELHlGQUF3RTtBQUV4RTtJQWtFSTtRQXhDQSx3Q0FBd0M7UUFDaEMsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUtwQyxrQkFBa0I7UUFDVixpQkFBWSxHQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzFCLHFCQUFnQixHQUFXLENBQUMsQ0FBQztRQUM3Qix5QkFBb0IsR0FBVyxDQUFDLENBQUM7UUFFekMsb0NBQW9DO1FBQ3BDLHFFQUFxRTtRQUM3RCxlQUFVLEdBQWMscUJBQVMsQ0FBQyxLQUFLLENBQUM7UUFDaEQseUNBQXlDO1FBQ2pDLG1CQUFjLEdBQVcsRUFBRSxDQUFDO1FBQzVCLG1CQUFjLEdBQVcsRUFBRSxDQUFDO1FBQzVCLHFCQUFnQixHQUFXLEVBQUUsQ0FBQztRQVF0QywyQ0FBMkM7UUFDbkMsZ0JBQVcsR0FBVyxHQUFHLENBQUM7UUFTbEMsb0RBQW9EO1FBQzVDLFVBQUssR0FBVyxDQUFDLENBQUMsQ0FBQztRQUMzQiw2RUFBNkU7UUFDckUsZ0JBQVcsR0FBWSxLQUFLLENBQUM7UUFHakMsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxlQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLGtCQUFrQixHQUFHO1lBQ3RCLFNBQVMsRUFBRSxDQUFDO1lBQ1osU0FBUyxFQUFFLENBQUM7WUFDWixPQUFPLEVBQUUsQ0FBQztZQUNWLFVBQVUsRUFBRSxDQUFDO1lBQ2IsVUFBVSxFQUFFLENBQUM7WUFDYixjQUFjLEVBQUUsQ0FBQztZQUNqQixhQUFhLEVBQUUsSUFBSTtZQUNuQixhQUFhLEVBQUUsSUFBSTtZQUNuQixXQUFXLEVBQUUsSUFBSTtZQUNqQixjQUFjLEVBQUUsSUFBSTtZQUNwQixjQUFjLEVBQUUsSUFBSTtZQUNwQixrQkFBa0IsRUFBRSxJQUFJO1NBQzNCLENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksZUFBTSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEtBQUssRUFBUSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxLQUFLLEVBQWUsQ0FBQztRQUM5QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxLQUFLLEVBQVUsQ0FBQztRQUM1QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxxQkFBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRCxnQkFBZ0I7SUFDVCxVQUFVLENBQUMsSUFBWSxFQUFFLGdCQUFpRSxFQUN6RixhQUEyQixFQUFFLGVBQXNCLEtBQUssRUFBRSxXQUFpQixHQUFHO1FBQ2xGLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUM3QixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUM7UUFFbkI7WUFDSSxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsU0FBUyxDQUFDO1lBQzNDLE1BQU0sUUFBUSxHQUFHLElBQUksR0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDO1lBRXpDLEVBQUUsRUFBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDZCx3QkFBd0I7Z0JBQ3hCLE1BQU0sRUFBRSxHQUFHLFVBQVUsQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQzNDLEVBQUUsRUFBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUNkLEtBQUssQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2dCQUMxQixDQUFDO2dCQUVELEVBQUUsRUFBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7b0JBQ2xCLGdCQUFnQixDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDbkMsQ0FBQztZQUNMLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLHlEQUF5RDtnQkFDekQsYUFBYSxFQUFFLENBQUM7WUFDcEIsQ0FBQztRQUNMLENBQUM7UUFFRCxzREFBc0Q7UUFDdEQsU0FBUyxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUVELG1EQUFtRDtJQUM1QyxPQUFPLENBQUMsR0FBVztRQUN0QixJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsdUNBQXVDO0lBQ3ZDLDZDQUE2QztJQUN0QyxnQkFBZ0I7UUFDbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO0lBQzNDLENBQUM7SUFFRCx3Q0FBd0M7SUFDakMsbUJBQW1CO1FBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztJQUM5QyxDQUFDO0lBRUQseUNBQXlDO0lBQ2xDLFdBQVcsQ0FBQyxDQUFTLEVBQUUsQ0FBUztRQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQsNERBQTREO0lBQ3JELFVBQVUsQ0FBQyxDQUFTLEVBQUUsQ0FBUztRQUNsQyxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsc0RBQXNEO0lBQy9DLFFBQVEsQ0FBQyxRQUFnQixFQUFFLFNBQWlCLEVBQUUsU0FBaUI7UUFDbEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsZ0ZBQWdGO0lBQ3pFLFNBQVMsQ0FBQyxRQUFnQjtRQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCwrRUFBK0U7SUFDeEUsUUFBUSxDQUFDLFFBQWdCO1FBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCxnRkFBZ0Y7SUFDekUsVUFBVSxDQUFDLFFBQWdCLEVBQUUsSUFBWTtRQUM1QyxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7SUFDeEMsQ0FBQztJQUVELHFFQUFxRTtJQUM5RCxZQUFZLENBQUMsUUFBZ0IsRUFBRSxJQUFZO1FBQzlDLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFRCw0REFBNEQ7SUFDckQsT0FBTztRQUNWLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDbEMsQ0FBQztJQUVELGdDQUFnQztJQUN6QixjQUFjO1FBQ2pCLEVBQUUsRUFBQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUMvQixDQUFDO0lBQ0wsQ0FBQztJQUVELDhEQUE4RDtJQUN2RCxzQkFBc0IsQ0FBQyxnQkFBaUQsRUFBRSxRQUFzQjtRQUNuRyxFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGdCQUFnQixFQUFFLEdBQUcsRUFBRTtnQkFDcEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7Z0JBQ3hCLFFBQVEsRUFBRSxDQUFDO1lBQ2YsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO0lBQ0wsQ0FBQztJQUVELDJEQUEyRDtJQUMzRCxpQ0FBaUM7SUFDMUIsY0FBYyxDQUFDLENBQVMsRUFBRSxDQUFTO1FBQ3RDLEdBQUcsRUFBQyxJQUFJLFFBQVEsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztZQUN0QyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdDLEVBQUUsRUFBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xFLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDaEIsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxxQ0FBcUM7SUFDckMsaUNBQWlDO0lBQzFCLGdCQUFnQixDQUFDLENBQVMsRUFBRSxDQUFTO1FBQ3hDLElBQUksRUFBRSxHQUFHLElBQUksS0FBSyxFQUFVLENBQUM7UUFDN0IsR0FBRyxFQUFDLElBQUksUUFBUSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDN0MsRUFBRSxFQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDbEUsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN0QixDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBRUQsSUFBSSxZQUFZO1FBQ1osTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFRCxJQUFJLGVBQWU7UUFDZixNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVELElBQUksaUJBQWlCO1FBQ2pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDbkMsQ0FBQztJQUVELElBQUksaUJBQWlCLENBQUMsQ0FBa0I7UUFDcEMsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztRQUMzQyxDQUFDO0lBQ0wsQ0FBQztJQUVELElBQUksVUFBVTtRQUNWLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsSUFBSSxTQUFTO1FBQ1QsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztJQUVELElBQUksU0FBUyxDQUFDLEVBQWtDO1FBQzVDLEVBQUUsRUFBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ0osRUFBRSxFQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoQixNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzVGLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pGLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQzVGLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pGLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pGLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pGLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pGLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2dCQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDbkksQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1lBQ3pCLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELElBQUksVUFBVTtRQUNWLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRDs7T0FFRztJQUVILElBQUksaUJBQWlCO1FBQ2pCLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDbkMsQ0FBQztJQUVELElBQUksb0JBQW9CO1FBQ3BCLDRGQUE0RjtRQUM1RixFQUFFLEVBQUMsSUFBSSxDQUFDLGdDQUFnQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQztRQUNqRCxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztJQUN0QyxDQUFDO0lBRUQsSUFBSSxvQkFBb0IsQ0FBQyxNQUFrQjtRQUN2QyxFQUFFLEVBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNSLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxNQUFNLENBQUM7UUFDeEMsQ0FBQztJQUNMLENBQUM7SUFFRCxJQUFJLFVBQVU7UUFDVixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDO0lBRUQsSUFBSSxTQUFTO1FBQ1QsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztJQUVELElBQUksWUFBWTtRQUNaLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzlCLENBQUM7SUFFRCxJQUFJLFlBQVk7UUFDWixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQsSUFBSSxZQUFZLENBQUMsQ0FBUztRQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsSUFBSSxZQUFZLENBQUMsQ0FBUztRQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsb0NBQW9DO0lBQ3BDLElBQUksR0FBRztRQUNILE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBQ3JCLENBQUM7SUFFRCwyQkFBMkI7SUFDM0IsSUFBSSxHQUFHLENBQUMsQ0FBTTtRQUNWLEVBQUUsRUFBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7UUFDbEIsQ0FBQztJQUNMLENBQUM7SUFFRCxJQUFJLE1BQU07UUFDTixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDO0lBRUQscURBQXFEO0lBQ3JELElBQUksS0FBSztRQUNMLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxJQUFJLFdBQVc7UUFDWCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDO0lBRUQsSUFBSSxlQUFlO1FBQ2YsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUNqQyxDQUFDO0lBRUQsSUFBSSxtQkFBbUI7UUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQztJQUNyQyxDQUFDO0lBRUQsdUJBQXVCO0lBQ3ZCLElBQUksV0FBVztRQUNYLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7SUFFRCx5QkFBeUI7SUFDekIsSUFBSSxhQUFhO1FBQ2IsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0IsQ0FBQztJQUVELDJCQUEyQjtJQUMzQixJQUFJLFdBQVcsQ0FBQyxLQUFtQztRQUMvQyxFQUFFLEVBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNQLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxDQUFHLG9DQUFvQztRQUM1RSxDQUFDO0lBQ0wsQ0FBQztJQUVELDZCQUE2QjtJQUM3QixJQUFJLGFBQWEsQ0FBQyxPQUF3QztRQUN0RCxFQUFFLEVBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNULDBEQUEwRDtZQUMxRCxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7WUFDdkMsSUFBSSxDQUFDLGNBQWMsR0FBRyxPQUFPLENBQUM7WUFFOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUUxQyx3REFBd0Q7WUFDeEQsRUFBRSxFQUFDLElBQUksQ0FBQyxTQUFTLElBQUkscUJBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN6QixJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxFQUFFLEdBQUcsRUFBRTtvQkFDekMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7b0JBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztvQkFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO29CQUM5QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQzFCLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2dCQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7WUFDbkMsQ0FBQztZQUVELEVBQUUsRUFBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1lBQ3BDLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELDJCQUEyQjtJQUNwQixhQUFhLENBQUMsRUFBVSxFQUFFLElBQWdCO1FBQzdDLEVBQUUsRUFBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUcsb0NBQW9DO1FBQzVFLENBQUM7SUFDTCxDQUFDO0lBRUQsZ0NBQWdDO0lBQ3pCLGdCQUFnQixDQUFDLEVBQVUsRUFBRSxPQUFzQjtRQUN0RCxFQUFFLEVBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNULElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDO1lBQ2xDLEVBQUUsRUFBQyxFQUFFLElBQUksRUFBRSxHQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztZQUNwQyxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCw0REFBNEQ7SUFDcEQsd0JBQXdCO1FBQzVCLHlDQUF5QztRQUN6QyxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVELCtDQUErQztJQUMvQyxJQUFJLFlBQVk7UUFDWixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM5QixDQUFDO0lBRUQsc0RBQXNEO0lBQy9DLGNBQWMsQ0FBQyxDQUFjO1FBQ2hDLEVBQUUsRUFBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ25CLEdBQUcsRUFBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztnQkFDaEMsRUFBRSxFQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUN4QyxrREFBa0Q7b0JBQ2xELENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDbEIsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO29CQUNWLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBQ2QsS0FBSyxDQUFDO2dCQUNWLENBQUM7WUFDTCxDQUFDO1lBQ0QsMkRBQTJEO1lBQzNELEVBQUUsRUFBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsQ0FBQyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0IsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQsNkJBQTZCO0lBQzdCLDBEQUEwRDtJQUNuRCxhQUFhLENBQUMsS0FBa0Q7UUFDbkUseUNBQXlDO1FBQ3pDLElBQUksQ0FBQyxHQUFHLElBQUkseUJBQVcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLEdBQUcsRUFBQyxNQUFNLEdBQUcsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sSUFBSSxHQUFHLE1BQU0sSUFBSSxDQUFDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTTtnQkFDM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUN6QyxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDTixDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2QixDQUFDO1FBQ0wsQ0FBQztRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELHFEQUFxRDtJQUNyRCxJQUFJLGFBQWE7UUFDYixNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQixDQUFDO0lBRUQscURBQXFEO0lBQ3JELElBQUksYUFBYTtRQUNiLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQy9CLENBQUM7SUFFRCx1REFBdUQ7SUFDdkQsSUFBSSxlQUFlO1FBQ2YsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUNqQyxDQUFDO0lBRUQsa0RBQWtEO0lBQzNDLGdCQUFnQjtRQUNuQixJQUFJLENBQUMsVUFBVSxHQUFHLHFCQUFTLENBQUMsVUFBVSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUM5QyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLENBQUMsRUFBRSxHQUFHLEVBQUU7WUFDSixFQUFFLEVBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxxQkFBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLElBQUksQ0FBQyxVQUFVLEdBQUcscUJBQVMsQ0FBQyxrQkFBa0IsQ0FBQztnQkFDL0MscUJBQXFCO2dCQUNyQixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQ25DLENBQUM7UUFDTCxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDYixDQUFDO0lBRUQsZ0RBQWdEO0lBQ3pDLGdCQUFnQjtRQUNuQiw0REFBNEQ7UUFDNUQsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1FBRXBDLGtEQUFrRDtRQUNsRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGdDQUFnQyxDQUFDLENBQUM7WUFDaEUsSUFBSSxDQUFDLGdDQUFnQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUM7UUFDdkUsSUFBSSxDQUFDLGdDQUFnQyxHQUFHLElBQUksQ0FBQztRQUU3Qyw4QkFBOEI7UUFDOUIsR0FBRyxFQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNqQixDQUFDO1FBRUQsbUNBQW1DO1FBQ25DLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNqRCxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDdkIsQ0FBQyxDQUFDLENBQUM7UUFFSCxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHO1lBQ2xCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRWxGLElBQUksQ0FBQyxVQUFVLEdBQUcscUJBQVMsQ0FBQyxVQUFVLENBQUM7UUFDdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQzlDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDeEUsQ0FBQyxFQUFFLEdBQUcsRUFBRTtZQUNKLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxLQUFLLHFCQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDMUMscUJBQXFCO2dCQUNyQixJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZDLHFDQUFxQztZQUNyQyxDQUFDO1FBQ0wsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVELG1EQUFtRDtJQUM1QyxrQkFBa0I7UUFDckIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLFVBQVUsR0FBRyxxQkFBUyxDQUFDLFlBQVksQ0FBQztRQUN6QyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUU7WUFDL0MsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUMxRSxDQUFDLEVBQUUsR0FBRyxFQUFFO1lBQ0osRUFBRSxFQUFDLElBQUksQ0FBQyxVQUFVLEtBQUsscUJBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUM1QyxJQUFJLENBQUMsVUFBVSxHQUFHLHFCQUFTLENBQUMsa0JBQWtCLENBQUM7Z0JBRS9DLHFCQUFxQjtnQkFDckIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUNuQyxDQUFDO1FBQ0wsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVELDJFQUEyRTtJQUMzRSxxREFBcUQ7SUFDN0MsMEJBQTBCLENBQUMsT0FBZTtRQUM5QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsRUFBRTtZQUM3Qzs7Ozs7ZUFLRztRQUNQLENBQUMsRUFBRSxHQUFHLEVBQUU7WUFDSixJQUFJLENBQUMsVUFBVSxHQUFHLHFCQUFTLENBQUMsa0JBQWtCLENBQUM7WUFDL0MsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1lBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx5RUFBeUU7SUFDbEUsa0JBQWtCLENBQUMsUUFBZ0IsRUFBRSxRQUFnQixFQUFFLFVBQWtCO1FBQzVFLElBQUksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxRQUFRLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztJQUM5RSxDQUFDO0lBRUQsMkNBQTJDO0lBQ3BDLFdBQVcsQ0FBQyxJQUFZLEVBQUUsSUFBWSxFQUFFLE1BQWU7UUFDMUQsRUFBRSxFQUFDLElBQUksQ0FBQyxTQUFTLElBQUkscUJBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLEVBQUUsRUFBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUNSLDJFQUEyRTtnQkFDM0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sR0FBRyxzQkFBYSxDQUFDLE1BQU0sQ0FBQztZQUNwRixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osMkVBQTJFO2dCQUMzRSxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxHQUFHLHNCQUFhLENBQUMsSUFBSTtvQkFDekUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUYsQ0FBQztZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDOUMsQ0FBQztJQUNMLENBQUM7SUFFRCw4Q0FBOEM7SUFDdkMsYUFBYTtRQUNoQixFQUFFLEVBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxzQkFBYSxDQUFDLE1BQU0sQ0FBQztZQUN6QyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLHNCQUFhLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDNUYsQ0FBQztJQUNMLENBQUM7SUFFRCw2Q0FBNkM7SUFDdEMsYUFBYTtRQUNoQixFQUFFLEVBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDMUMsSUFBSSxDQUFDLFdBQVcsSUFBSSxzQkFBYSxDQUFDLE1BQU0sQ0FBQztZQUN6QyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLHNCQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUNuRixDQUFDO0lBQ0wsQ0FBQztJQUVELDREQUE0RDtJQUNyRCxlQUFlLENBQUMsUUFBZ0IsRUFBRSxNQUFjLEVBQUUsVUFBa0IsRUFBRSxVQUFrQjtRQUMzRixJQUFJLENBQUMsa0JBQWtCLENBQUMsYUFBYSxDQUFDLElBQUksaUJBQVUsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ2hHLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFFRCw4REFBOEQ7SUFDdkQsZ0JBQWdCLENBQUMsUUFBZ0IsRUFBRSxNQUFjO1FBQ3BELEVBQUUsRUFBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU07WUFDekMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sSUFBSSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsc0JBQWEsQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RGLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxNQUFNLE1BQU0sR0FBRztZQUNYLElBQUksRUFBRSxRQUFRLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJO1lBQ3ZDLE1BQU0sRUFBRSxzQkFBYSxDQUFDLFlBQVk7WUFDbEMsUUFBUSxFQUFFLFFBQVE7WUFDbEIsTUFBTSxFQUFFLE1BQU07U0FDSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQ0FBZ0MsR0FBRyxJQUFJLGlCQUFVLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxXQUFXLElBQUksc0JBQWEsQ0FBQyxZQUFZLENBQUM7UUFDL0MsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRCwyREFBMkQ7SUFDcEQsZUFBZSxDQUFDLFFBQWdCLEVBQUUsTUFBYztRQUNuRCxFQUFFLEVBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzVDLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELDhEQUE4RDtJQUN2RCxpQkFBaUIsQ0FBQyxRQUFnQixFQUFFLE1BQWM7UUFDckQsRUFBRSxFQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUM1QyxNQUFNLENBQUM7UUFDWCxDQUFDO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVELCtEQUErRDtJQUN4RCxzQkFBc0IsQ0FBQyxRQUFnQjtRQUMxQyxJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRCw0REFBNEQ7SUFDckQsb0JBQW9CLENBQUMsQ0FBUyxFQUFFLENBQVMsRUFBRSxLQUFhLEVBQUUsS0FBYTtRQUMxRSxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCw2Q0FBNkM7SUFDdEMsWUFBWSxDQUFDLFFBQWdCO1FBQ2hDLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUMsUUFBUSxDQUFDO1FBQ3RHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ25DLDhDQUE4QztRQUM5QyxFQUFFLEVBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDM0IscUNBQXFDO1lBQ3pDLDRCQUE0QjtZQUN4QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUM1QixDQUFDO0lBQ0wsQ0FBQztJQUVELGdDQUFnQztJQUN6QixVQUFVLENBQUMsU0FBd0I7UUFDdEMsRUFBRSxFQUFDLFNBQVMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QixFQUFFLEVBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUM3QiwrQkFBK0I7Z0JBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDakQsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLGlCQUFpQjtnQkFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsRCxDQUFDO1FBQ0wsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxlQUFlLEdBQUcsS0FBSyxDQUFDO1lBQzVCLEdBQUcsRUFBQyxNQUFNLFFBQVEsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixFQUFFLEVBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUN6QixlQUFlLEdBQUcsSUFBSSxDQUFDO2dCQUMzQixDQUFDO1lBQ0wsQ0FBQztZQUNELEVBQUUsRUFBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO2dCQUNqQixvQ0FBb0M7Z0JBQ3BDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDaEQsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLGlCQUFpQjtnQkFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNsRCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCw4REFBOEQ7SUFDdkQscUJBQXFCO1FBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztJQUN0QyxDQUFDO0lBRUQsNERBQTREO0lBQ3JELG1CQUFtQixDQUFDLE1BQWM7UUFDckMsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUM7UUFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQ3BDLENBQUM7SUFFRCx5REFBeUQ7SUFDbEQsZ0JBQWdCLENBQUMsVUFBa0IsRUFBRSxjQUFzQjtRQUM5RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDO1FBQ25DLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxjQUFjLENBQUM7UUFDM0MsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFFRCxnRUFBZ0U7SUFDekQsV0FBVztRQUNkLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELHdEQUF3RDtJQUNqRCxhQUFhLENBQUMsS0FBa0I7UUFDbkMsRUFBRSxFQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDUCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUN4QixDQUFDO0lBQ0wsQ0FBQztJQUVELCtDQUErQztJQUN4QyxrQkFBa0IsQ0FBQyxRQUFnQixFQUFFLFVBQW1CO1FBQzNELEVBQUUsRUFBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUM7UUFDbkQsQ0FBQztRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRCwyREFBMkQ7SUFDcEQsWUFBWSxDQUFDLElBQVk7UUFDNUIsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDOUUsQ0FBQztJQUVELGlEQUFpRDtJQUMxQyxrQkFBa0IsQ0FBQyxRQUFnQjtRQUN0QyxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFVBQVUsUUFBUSxFQUFFLENBQUM7UUFDL0QsRUFBRSxFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBQ0QsRUFBRSxFQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9CLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxDQUFDO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsa0RBQWtEO0lBQzNDLGFBQWEsQ0FBQyxRQUFnQixFQUFFLEdBQVc7UUFDOUMsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxVQUFVLFFBQVEsRUFBRSxDQUFDO1FBQy9ELElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsb0VBQW9FO0lBQzdELGVBQWU7UUFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRUQsbUVBQW1FO0lBQzVELGFBQWE7UUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUM5QixDQUFDO0lBRUQsMkVBQTJFO0lBQ3BFLGNBQWMsQ0FBQyxDQUFTLEVBQUUsQ0FBUyxFQUFFLFFBQWdCO1FBQ3hELElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVELDhDQUE4QztJQUN2QyxlQUFlLENBQUMsSUFBWTtRQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsd0RBQXdEO0lBQ2pELGtCQUFrQjtRQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVELCtFQUErRTtJQUN4RSxvQkFBb0IsQ0FBQyxDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVMsRUFBRSxDQUFTO1FBQ2xFLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELCtFQUErRTtJQUN4RSxvQkFBb0IsQ0FBQyxDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVMsRUFBRSxDQUFTO1FBQ2xFLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVELGlGQUFpRjtJQUMxRSxzQkFBc0IsQ0FBQyxJQUFZLEVBQUUsSUFBWSxFQUFFLE1BQWMsRUFBRSxDQUFTLEVBQUUsQ0FBUztRQUMxRixJQUFJLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQsK0RBQStEO0lBQ3hELDZCQUE2QixDQUFDLENBQVMsRUFBRSxDQUFTLEVBQUUsS0FBYSxFQUNoRSxLQUFhLEVBQUUsU0FBa0IsRUFBRSxTQUFrQjtRQUN6RCxJQUFJLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVELHNEQUFzRDtJQUMvQyxXQUFXLENBQUMsTUFBa0IsRUFBRSxDQUFTLEVBQUUsQ0FBUztRQUN2RCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFRCxnREFBZ0Q7SUFDekMsa0JBQWtCLENBQUMsR0FBZ0IsRUFBRSxDQUFTLEVBQUUsQ0FBUztRQUM1RCxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUVELGtEQUFrRDtJQUMzQyxjQUFjO1FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDL0IsQ0FBQzs7QUF2ekJELHlEQUF5RDtBQUNsQyxzQkFBVyxHQUFXLFlBQVksSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO0FBRi9GLGdDQXl6QkM7Ozs7Ozs7Ozs7Ozs7O0FDdjBCRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUEyREU7O0FBRUYsMkZBQXlDO0FBR3pDLElBQUksVUFBc0IsQ0FBQztBQUUzQjtJQUNJLFVBQVUsR0FBRyxJQUFJLHVCQUFVLEVBQUUsQ0FBQztBQUNsQyxDQUFDO0FBRUQseURBQXlEO0FBQ3pELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFO0lBQ25DLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztBQUNoQyxDQUFDLENBQUMsQ0FBQztBQUVILDREQUE0RDtBQUM1RCxNQUFNLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRTtJQUNqQyxJQUFJLEVBQUUsQ0FBQztBQUNYLENBQUMsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUM5RVUscUJBQWEsR0FBRztJQUN6QixNQUFNLEVBQUUsQ0FBQztJQUNULElBQUksRUFBRSxFQUFFO0lBQ1IsTUFBTSxFQUFFLEVBQUU7SUFDVixZQUFZLEVBQUUsQ0FBQztJQUNmLE9BQU8sRUFBRSxFQUFFO0lBQ1gsTUFBTSxFQUFFLEVBQUU7Q0FDYixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUNQRixJQUFZLFNBTVg7QUFORCxXQUFZLFNBQVM7SUFDakIsMkNBQUs7SUFDTCxxREFBVTtJQUNWLHFEQUFVO0lBQ1YseURBQVk7SUFDWixxRUFBa0I7QUFDdEIsQ0FBQyxFQU5XLFNBQVMsR0FBVCxpQkFBUyxLQUFULGlCQUFTLFFBTXBCOzs7Ozs7Ozs7Ozs7Ozs7QUNKRDtJQUFBO1FBQ0ksdURBQXVEO1FBQ3ZELGdDQUFnQztRQUN4QixrQkFBYSxHQUFHLEVBQUUsQ0FBQztRQUMzQix1Q0FBdUM7UUFDL0IscUJBQWdCLEdBQUcsRUFBRSxDQUFDO0lBeURsQyxDQUFDO0lBdkRHLHVDQUF1QztJQUN2QyxvQ0FBb0M7SUFDN0IsY0FBYyxDQUFDLENBQUM7UUFDbkIsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDSCxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUM3QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDN0MsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNwQixDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsNENBQTRDO0lBQ3JDLGlCQUFpQixDQUFDLENBQUM7UUFDdEIsR0FBRyxFQUFDLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDdkMsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQztZQUM3QyxFQUFFLEVBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1gsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3BDLEtBQUssQ0FBQztZQUNWLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELDhCQUE4QjtJQUM5QixvQ0FBb0M7SUFDN0IsT0FBTyxDQUFDLENBQVMsRUFBRSxDQUFTO1FBQy9CLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBQzdDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEdBQUc7WUFDM0IsQ0FBQyxFQUFFLENBQUM7WUFDSixDQUFDLEVBQUUsQ0FBQztTQUNQLENBQUM7UUFDRixNQUFNLENBQUMsUUFBUSxDQUFDO0lBQ3BCLENBQUM7SUFFRCwrREFBK0Q7SUFDeEQsR0FBRyxDQUFDLFFBQWdCO1FBQ3ZCLEVBQUUsRUFBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN4QyxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsaUNBQWlDO0lBQ2pDLDhJQUE4STtJQUN0SSxvQkFBb0I7UUFDeEIsNERBQTREO1FBQzVELElBQUksUUFBUSxDQUFDO1FBQ2IsR0FBRyxDQUFDO1lBQ0EsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDMUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDMUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsR0FBRyxDQUFDLENBQUM7WUFDMUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztRQUNyQyxDQUFDLFFBQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEVBQUU7UUFFekUsTUFBTSxDQUFDLFFBQVEsQ0FBQztJQUNwQixDQUFDO0NBQ0o7QUE5REQsd0JBOERDOzs7Ozs7Ozs7Ozs7Ozs7QUM3REQ7SUFPSSxZQUEyQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQzdDLDZEQUE2RDtRQUM3RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksS0FBSyxFQUFxQixDQUFDO1FBQ2pELEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUMzQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssRUFBYyxDQUFDLENBQUM7WUFDOUMsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsU0FBUyxDQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUMzQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQyxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCxJQUFXLFNBQVM7UUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDM0IsQ0FBQztJQUVELDhDQUE4QztJQUM5QyxrREFBa0Q7SUFDM0MsYUFBYSxDQUFDLFVBQXNCLEVBQUUsVUFBa0IsRUFBRSxVQUFrQjtRQUMvRSxFQUFFLEVBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ2IsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUQsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXRELG9CQUFvQjtRQUNwQixFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1AsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUQsZUFBZTtRQUNmLEVBQUUsRUFBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLFVBQVUsR0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxVQUFVO1lBQ2pFLFVBQVUsR0FBRyxDQUFDLElBQUksVUFBVSxHQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDdEUsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBRUQsOEVBQThFO1FBQzlFLCtDQUErQztRQUMvQyxHQUFHLEVBQUMsSUFBSSxLQUFLLEdBQUcsVUFBVSxFQUFFLEtBQUssR0FBRyxVQUFVLEdBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDO1lBQ3JFLEdBQUcsRUFBQyxJQUFJLEtBQUssR0FBRyxVQUFVLEVBQUUsS0FBSyxHQUFHLFVBQVUsR0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUM7Z0JBQ3RFLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsVUFBVSxDQUFDO1lBQy9DLENBQUM7UUFDTCxDQUFDO1FBRUQsZ0RBQWdEO1FBQ2hELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVNLGdCQUFnQixDQUFDLFVBQXNCO0lBRTlDLENBQUM7O0FBdkRzQixvQkFBVSxHQUFHLENBQUMsQ0FBQztBQUNmLG9CQUFVLEdBQUcsQ0FBQyxDQUFDO0FBRjFDLDhCQXlEQzs7Ozs7Ozs7Ozs7Ozs7O0FDckNELGVBQXNCLElBQVU7SUFDNUIsTUFBTSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQztBQUNwQyxDQUFDO0FBRkQsc0JBRUM7QUFFRCxzQkFBNkIsSUFBVTtJQUNuQyxNQUFNLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQztBQUNqQyxDQUFDO0FBRkQsb0NBRUM7QUFFRCxtQkFBMEIsSUFBVTtJQUNoQyxNQUFNLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQztBQUNoQyxDQUFDO0FBRkQsOEJBRUM7QUFFRDtJQUNJLFlBQTBCLFFBQWdCLEVBQVMsTUFBYztRQUF2QyxhQUFRLEdBQVIsUUFBUSxDQUFRO1FBQVMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUFHLENBQUM7Q0FDeEU7QUFGRCxnQ0FFQzs7Ozs7Ozs7Ozs7Ozs7O0FDbkNEO0lBS0ksWUFBMkIsS0FBYSxFQUFVLEtBQWE7UUFBcEMsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUFVLFVBQUssR0FBTCxLQUFLLENBQVE7UUFDM0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEtBQUssRUFBUSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3BCLENBQUM7SUFFRCxJQUFJLElBQUk7UUFDSixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUs7SUFDckIsQ0FBQztJQUVELElBQUksSUFBSTtRQUNKLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7SUFFRCxJQUFJLEtBQUs7UUFDTCxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRU0sUUFBUTtRQUNYLElBQUksQ0FBQyxNQUFNLEVBQUcsQ0FBQztJQUNuQixDQUFDO0lBRU0sS0FBSztRQUNSLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3BCLENBQUM7SUFFRCxJQUFJLEtBQUs7UUFDTCxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0lBRUQsSUFBSSxLQUFLLENBQUMsQ0FBYztRQUNwQixFQUFFLEVBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ3BCLENBQUM7SUFDTCxDQUFDO0lBRUQsSUFBSSxRQUFRO1FBQ1IsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDMUIsQ0FBQztJQUVELElBQUksUUFBUSxDQUFDLENBQVM7UUFDbEIsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztRQUN2QixDQUFDO0lBQ0wsQ0FBQztDQUNKO0FBakRELGtDQWlEQzs7Ozs7Ozs7Ozs7Ozs7O0FDakREO0lBSUksZ0NBQWdDO0lBQ2hDLDZCQUE2QjtJQUU3QixZQUEyQixVQUFzQixFQUFFLElBQVk7UUFBcEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUM3QyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFTyxPQUFPLENBQUMsSUFBWTtRQUN4QiwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsMEJBQTBCO1FBQzFCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ1YsR0FBRyxFQUFDLE1BQU0sSUFBSSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdkIsbUJBQW1CO1lBQ25CLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ1YsR0FBRyxFQUFDLE1BQU0sSUFBSSxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCOzs7O21CQUlHO2dCQUNILDRDQUE0QztnQkFDNUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQ3ZDLElBQUksRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFDekIsUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUNqRCxDQUFDLENBQUM7Z0JBQ0gsd0JBQXdCO2dCQUN4Qjs7O29CQUdJO2dCQUNKLENBQUMsRUFBRSxDQUFDO1lBQ1IsQ0FBQztZQUNELENBQUMsRUFBRSxDQUFDO1FBQ1IsQ0FBQztJQUNMLENBQUM7SUFFRCwwQkFBMEI7SUFDMUIsOElBQThJO0lBQzlJOzs7OztPQUtHO0lBRUgsSUFBSSxPQUFPO1FBQ1AsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQztDQUtKO0FBNURELGtCQTREQzs7Ozs7Ozs7Ozs7Ozs7O0FDOUREO0lBR0ksWUFBNEIsS0FBYSxFQUFVLE1BQWMsRUFBVSxLQUFvQixFQUFVLE9BQXVCLElBQUksRUFDNUcsY0FBcUIsS0FBSyxFQUFVLGVBQXNCLEtBQUssRUFDL0QsZ0JBQXVCLEtBQUs7UUFGeEIsVUFBSyxHQUFMLEtBQUssQ0FBUTtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxVQUFLLEdBQUwsS0FBSyxDQUFlO1FBQVUsU0FBSSxHQUFKLElBQUksQ0FBdUI7UUFDNUcsZ0JBQVcsR0FBWCxXQUFXLENBQWU7UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBZTtRQUMvRCxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtJQUFHLENBQUM7SUFFeEQsdURBQXVEO0lBQ3ZELE1BQU0sQ0FBQyxJQUFJO1FBQ1AsSUFBSSxDQUFDLE1BQU0sR0FBRztZQUNWLElBQUksUUFBUSxDQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsQ0FBQyx5QkFBeUIsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO1lBQy9FLElBQUksUUFBUSxDQUFDLEtBQUssRUFBRSxTQUFTLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQ3RELElBQUksUUFBUSxDQUFDLFdBQVcsRUFBRSxTQUFTLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQzNELElBQUksUUFBUSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQyw2QkFBNkIsRUFBRSw4QkFBOEIsQ0FBQyxFQUFFLElBQUksS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUM7WUFDbkksSUFBSSxRQUFRLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxDQUFDLHlCQUF5QixDQUFDLEVBQUUsSUFBSSxLQUFLLEVBQUUsQ0FBQztZQUMxRSxJQUFJLFFBQVEsQ0FBQyxVQUFVLEVBQUUsU0FBUyxFQUFFLENBQUMsc0VBQXNFLENBQUMsRUFBRSxJQUFJLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO1lBQzVJLElBQUksUUFBUSxDQUFDLE9BQU8sRUFBRSxTQUFTLEVBQUUsQ0FBQyw2QkFBNkIsRUFBRSw4QkFBOEIsQ0FBQyxFQUFFLElBQUksS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQztZQUMzSCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLENBQUMsNkJBQTZCLEVBQUUsOEJBQThCLEVBQUUseUJBQXlCLENBQUMsRUFBRSxJQUFJLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDO1lBQzNKLElBQUksUUFBUSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxxQ0FBcUMsQ0FBQyxFQUFFLElBQUksS0FBSyxFQUFFLENBQUM7U0FDMUYsQ0FBQztRQUVGLE9BQU87UUFDUCxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsMkJBQTJCLENBQUM7UUFDckQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLDJCQUEyQixDQUFDO1FBQ3JELElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRywrQkFBK0IsQ0FBQztRQUN6RCxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsNEJBQTRCLENBQUM7UUFDdEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFHLGlDQUFpQyxDQUFDO1FBQzNELElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyw2QkFBNkIsQ0FBQztJQUMzRCxDQUFDO0lBRUQsSUFBSSxJQUFJO1FBQ0osTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVELElBQUksS0FBSztRQUNMLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxJQUFJLElBQUk7UUFDSixNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQsSUFBSSxHQUFHO1FBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDckIsQ0FBQztJQUVELElBQUksVUFBVTtRQUNWLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRCxJQUFJLFdBQVc7UUFDWCxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUM3QixDQUFDO0lBRUQsSUFBSSxZQUFZO1FBQ1osTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDOUIsQ0FBQztJQUVELE1BQU0sS0FBSyxLQUFLO1FBQ1osTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7SUFDM0IsQ0FBQztDQUNKO0FBN0RELDRCQTZEQztBQUVELFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7O0FDNURoQjtJQUlJLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDN0MsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQW1CLENBQUM7UUFDOUYsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBbUIsQ0FBQztJQUMvRSxDQUFDO0lBRU0sWUFBWSxDQUFDLE1BQWMsRUFBRSxNQUFjLEVBQUUsTUFBYztRQUM5RCxJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9DLFFBQVEsQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNqQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDOUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBRWhDLElBQUksS0FBSyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDNUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQztRQUN2QyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7UUFDOUIsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQzNCLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUU3QixNQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDakUsVUFBVSxDQUFDLFNBQVMsR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxJQUFJLENBQUM7UUFFekQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFTSxlQUFlO1FBQ2xCLG1DQUFtQztRQUNuQyxPQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM1RCxDQUFDO1FBRUQsZ0NBQWdDO1FBQ2hDOzs7Ozs7Ozs7Ozs7O2tEQWEwQztRQUMxQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7SUFDMUQsQ0FBQztJQUVNLElBQUk7UUFDUCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7SUFDekQsQ0FBQztDQUNKO0FBeERELGdDQXdEQzs7Ozs7Ozs7Ozs7Ozs7O0FDeEREO0lBa0NJLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFnSzFDLHFCQUFnQixHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDNUIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFFTSx5QkFBb0IsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFFTSwwQkFBcUIsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFFTSx5QkFBb0IsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFFTSx5QkFBb0IsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFFTSx5QkFBb0IsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUFFTSx5QkFBb0IsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hCLENBQUM7UUEvTEcsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFtQixDQUFDO1FBQ2xGLElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBcUIsQ0FBQztRQUNwRixJQUFJLENBQUMsbUJBQW1CLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyx3QkFBd0IsQ0FBc0IsQ0FBQztRQUVsRyxJQUFJLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQXNCLENBQUM7UUFDckYsSUFBSSxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFzQixDQUFDO1FBQ3ZGLElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBc0IsQ0FBQztRQUNyRixJQUFJLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQXNCLENBQUM7UUFDckYsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFzQixDQUFDO1FBQ3JGLElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBc0IsQ0FBQztRQUVyRixJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQW9CLENBQUM7UUFDeEYsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQW9CLENBQUM7UUFDMUYsSUFBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFvQixDQUFDO1FBQ3hGLElBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBb0IsQ0FBQztRQUN4RixJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQW9CLENBQUM7UUFDeEYsSUFBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFvQixDQUFDO1FBRXhGLElBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBcUIsQ0FBQztRQUN6RixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBcUIsQ0FBQztRQUMzRixJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQXFCLENBQUM7UUFDekYsSUFBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFxQixDQUFDO1FBQ3pGLElBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBcUIsQ0FBQztRQUN6RixJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQXFCLENBQUM7UUFFekYsSUFBSSxDQUFDLHNCQUFzQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsNEJBQTRCLENBQXFCLENBQUM7UUFDeEcsSUFBSSxDQUFDLHVCQUF1QixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsNkJBQTZCLENBQXFCLENBQUM7UUFDMUcsSUFBSSxDQUFDLHNCQUFzQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsNEJBQTRCLENBQXFCLENBQUM7UUFDeEcsSUFBSSxDQUFDLHNCQUFzQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsNEJBQTRCLENBQXFCLENBQUM7UUFDeEcsSUFBSSxDQUFDLHNCQUFzQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsNEJBQTRCLENBQXFCLENBQUM7UUFDeEcsSUFBSSxDQUFDLHNCQUFzQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsNEJBQTRCLENBQXFCLENBQUM7UUFFeEcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUUxRSxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBRU8sSUFBSTtRQUNSLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7SUFDcEQsQ0FBQztJQUVNLElBQUk7UUFDUCxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO0lBQ25ELENBQUM7SUFFTSxjQUFjLENBQUMsUUFBZ0I7UUFDbEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoQixDQUFDO0lBRU8sY0FBYztRQUNsQixNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQztRQUMxQyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUQsZ0VBQWdFO1FBQ2hFLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN4QyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBQyxNQUFNLEdBQUcsTUFBTSxHQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUU1RCxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDO1FBQy9DLElBQUksU0FBUyxDQUFDO1FBQ2QsSUFBSSxjQUFjLENBQUM7UUFDbkIsSUFBSSxRQUFRLENBQUM7UUFFYixJQUFJLGNBQWMsR0FBRyxLQUFLLENBQUM7UUFDM0IsRUFBRSxFQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1gsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBUSxDQUFDO1lBQ25ELEVBQUUsRUFBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNMLDRDQUE0QztnQkFDNUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUM7Z0JBQzFCLGNBQWMsR0FBRyxHQUFHLENBQUMsY0FBYyxDQUFDO2dCQUNwQyxRQUFRLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQztnQkFDeEIsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMxQixDQUFDO1FBQ0wsQ0FBQztRQUVELEVBQUUsRUFBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7WUFDakIsMkNBQTJDO1lBQzNDLFNBQVMsR0FBRyxDQUFDLENBQUM7WUFDZCxjQUFjLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLFFBQVEsR0FBRyxHQUFHLENBQUM7UUFDbkIsQ0FBQztRQUVELDBEQUEwRDtRQUMxRCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxNQUFNLEdBQUcsUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsU0FBUyxHQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBRXBILCtDQUErQztRQUMvQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxHQUFHLEVBQUUsR0FBQyxTQUFTLEdBQUMsVUFBVSxDQUFDO1FBQ2hFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLEdBQUcsRUFBRSxHQUFDLFNBQVMsR0FBQyxVQUFVLENBQUM7UUFDakUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsR0FBRyxFQUFFLEdBQUMsU0FBUyxHQUFDLFVBQVUsQ0FBQztRQUNoRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxHQUFHLEVBQUUsR0FBQyxTQUFTLEdBQUMsVUFBVSxDQUFDO1FBQ2hFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEdBQUcsRUFBRSxHQUFDLFNBQVMsR0FBQyxVQUFVLENBQUM7UUFDaEUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsR0FBRyxFQUFFLEdBQUMsU0FBUyxHQUFDLFVBQVUsQ0FBQztRQUVoRSwrQ0FBK0M7UUFDL0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsU0FBUyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDNUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUV4Riw4Q0FBOEM7UUFDOUMsTUFBTSxFQUFFLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNuRCxFQUFFLEVBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQztZQUNuRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQztZQUN0RSxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxFQUFFLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFHLENBQUM7WUFDbkUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsRUFBRSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksR0FBRyxDQUFDO1lBQ25FLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQztZQUNuRSxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxFQUFFLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFHLENBQUM7WUFFbkUsNENBQTRDO1lBQzVDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyRSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xFLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsRSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFbEUsRUFBRSxFQUFDLEVBQUUsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUNsRSxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDckUsQ0FBQztZQUVELEVBQUUsRUFBQyxFQUFFLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDbkUsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDdEUsQ0FBQztZQUVELEVBQUUsRUFBQyxFQUFFLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDbEUsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3JFLENBQUM7WUFFRCxFQUFFLEVBQUMsRUFBRSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNmLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ2xFLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUNyRSxDQUFDO1lBRUQsRUFBRSxFQUFDLEVBQUUsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDZixJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUNsRSxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDckUsQ0FBQztZQUVELEVBQUUsRUFBQyxFQUFFLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDbEUsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQ3JFLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztDQW1DSjtBQW5PRCxzQ0FtT0M7Ozs7Ozs7Ozs7Ozs7OztBQ3BPRDtJQUdJLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDN0MsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBcUIsQ0FBQztJQUMvRSxDQUFDO0lBRU0sSUFBSSxDQUFDLEdBQVcsRUFBRSxlQUFzQixLQUFLO1FBQ2hELElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQztRQUUvQixFQUFFLEVBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7WUFDM0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNoRCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1lBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUMvQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDNUMsQ0FBQztJQUNMLENBQUM7SUFFTSxJQUFJO1FBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztJQUMvQyxDQUFDO0NBQ0o7QUF4QkQsOEJBd0JDOzs7Ozs7Ozs7Ozs7Ozs7QUMxQkQsNEZBQTBDO0FBQzFDLG1HQUE2QztBQUU3QztJQW1DSSxZQUEyQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBRnpDLG1CQUFjLEdBQWtCLENBQUMsMEJBQTBCLEVBQUUsMEJBQTBCLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztRQStKckgsbUJBQWMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQzNCLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFlBQVk7Z0JBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25GLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVDLENBQUM7UUFDTCxDQUFDO1FBRU8sb0JBQWUsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQzVCLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFlBQVk7Z0JBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JGLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzdDLENBQUM7UUFDTCxDQUFDO1FBRU8sbUJBQWMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQzNCLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFlBQVk7Z0JBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25GLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVDLENBQUM7UUFDTCxDQUFDO1FBRU8sbUJBQWMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQzNCLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFlBQVk7Z0JBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25GLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVDLENBQUM7UUFDTCxDQUFDO1FBRU8sbUJBQWMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQzNCLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFlBQVk7Z0JBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25GLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVDLENBQUM7UUFDTCxDQUFDO1FBRU8sbUJBQWMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQzNCLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFlBQVk7Z0JBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25GLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzVDLENBQUM7UUFDTCxDQUFDO1FBcE1HLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQW1CLENBQUM7UUFDckUsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBcUIsQ0FBQztRQUV2RSxJQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFzQixDQUFDO1FBQ3hFLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQXNCLENBQUM7UUFDMUUsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBc0IsQ0FBQztRQUN4RSxJQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFzQixDQUFDO1FBQ3hFLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQXNCLENBQUM7UUFDeEUsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBc0IsQ0FBQztRQUV4RSxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFvQixDQUFDO1FBQzdFLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQW9CLENBQUM7UUFDM0UsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBb0IsQ0FBQztRQUM3RSxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFvQixDQUFDO1FBQzNFLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQW9CLENBQUM7UUFDM0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBb0IsQ0FBQztRQUMzRSxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFvQixDQUFDO1FBRTNFLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQXFCLENBQUM7UUFDNUUsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBcUIsQ0FBQztRQUM5RSxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFxQixDQUFDO1FBQzVFLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQXFCLENBQUM7UUFDNUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBcUIsQ0FBQztRQUM1RSxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFxQixDQUFDO1FBRTVFLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFxQixDQUFDO1FBQzdGLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFxQixDQUFDO1FBQy9GLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFxQixDQUFDO1FBQzdGLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFxQixDQUFDO1FBQzdGLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFxQixDQUFDO1FBQzdGLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLHNCQUFzQixDQUFxQixDQUFDO1FBRTdGLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQzdFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFTSxjQUFjO1FBQ2pCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDO1FBQzlDLEVBQUUsRUFBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ1osSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsZ0NBQWdDLFVBQVUsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVM7c0JBQzdGLFVBQVUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLFNBQVMsQ0FBQztZQUUvRCxnREFBZ0Q7WUFDaEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDO1lBRWpGLDhDQUE4QztZQUM5QyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUM7WUFDN0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDO1lBQ2hGLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLFVBQVUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsQ0FBQztZQUM3RSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUM7WUFDN0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsVUFBVSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxDQUFDO1lBQzdFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLFVBQVUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsQ0FBQztZQUU3RSw0Q0FBNEM7WUFDNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsRUFBRSxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVFLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLEVBQUUsR0FBRyxDQUFDLFVBQVUsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMvRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsRUFBRSxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLEVBQUUsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM1RSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFNUUsaURBQWlEO1lBQ2pELEVBQUUsRUFBQyxVQUFVLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQzdELENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUNoRSxDQUFDO1lBRUQsRUFBRSxFQUFDLFVBQVUsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDNUQsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQy9ELENBQUM7WUFFRCxFQUFFLEVBQUMsVUFBVSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN4QixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUM3RCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDaEUsQ0FBQztZQUVELEVBQUUsRUFBQyxVQUFVLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQzVELENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUMvRCxDQUFDO1lBRUQsRUFBRSxFQUFDLFVBQVUsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDNUQsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQy9ELENBQUM7WUFFRCxFQUFFLEVBQUMsVUFBVSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUM1RCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUM7WUFDL0QsQ0FBQztZQUVELEVBQUUsRUFBQyxVQUFVLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1lBQzVELENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUMsQ0FBQztZQUMvRCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFTSxrQkFBa0I7UUFDckIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFTSxtQkFBbUI7UUFDdEIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFTSxrQkFBa0I7UUFDckIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFTSxrQkFBa0I7UUFDckIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFTSxrQkFBa0I7UUFDckIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFTSxrQkFBa0I7UUFDckIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1FBQ25DLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFFTyxjQUFjO1FBQ2xCLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUN2RixDQUFDO0NBMkNKO0FBek9ELHdDQXlPQzs7Ozs7Ozs7Ozs7Ozs7O0FDMU9EO0lBTUksWUFBMkIsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQXlCekMsbUJBQWMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQzNCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBRU8sbUJBQWMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDO1lBQzVCLEVBQUUsRUFBQyxDQUFDLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDO2dCQUM1QixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ25CLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN2QyxDQUFDO1FBQ0wsQ0FBQztRQW5DRyxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFtQixDQUFDO1FBQzNFLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQW1CLENBQUM7UUFDckUsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBcUIsQ0FBQztRQUMzRSxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFzQixDQUFDO1FBQ2pGLGlGQUFpRjtRQUNqRixJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVNLFNBQVMsQ0FBQyxHQUFXO1FBQ3hCLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEMsR0FBRyxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7UUFDcEIsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM5QiwrREFBK0Q7UUFDL0QsbUNBQW1DO0lBQ3ZDLENBQUM7SUFFTyxPQUFPLENBQUMsR0FBVztRQUN2QixFQUFFLEVBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUM5QixDQUFDO0lBQ0wsQ0FBQztDQWNKO0FBM0NELGdDQTJDQzs7Ozs7Ozs7Ozs7Ozs7O0FDN0NELDRGQUEwQztBQUUxQztJQUdJLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDN0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBbUIsQ0FBQztJQUNqRixDQUFDO0lBRU0sSUFBSTtRQUNQLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7SUFDakQsQ0FBQztJQUVPLElBQUksQ0FBQyxDQUFTLEVBQUUsQ0FBUztRQUM3QixrQ0FBa0M7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQyxRQUFRLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFDLENBQUMsS0FBSyxDQUFDO1FBQ25GLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO1FBRXRDLG1DQUFtQztRQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO0lBQ2xELENBQUM7SUFFTyxLQUFLO1FBQ1Qsd0JBQXdCO1FBQ3hCLE9BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNoQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzlELENBQUM7SUFDTCxDQUFDO0lBRU0sa0JBQWtCLENBQUMsQ0FBUyxFQUFFLENBQVMsRUFBRSxRQUFnQjtRQUM1RCxNQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQztRQUN0QyxFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDeEIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBRWIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ25CLGlCQUFpQixJQUFZO2dCQUN6QixLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDaEQsQ0FBQztZQUVELG1DQUFtQztZQUNuQyxFQUFFLEVBQUMsRUFBRSxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMzQyxHQUFHLENBQUMsU0FBUyxHQUFHLGNBQWMsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2xDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUUsQ0FBQztZQUNELEVBQUUsRUFBQyxFQUFFLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hDLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzNDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDO2dCQUNoQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDbEMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLHVCQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvRSxDQUFDO1lBQ0QsRUFBRSxFQUFDLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDOUIsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDM0MsR0FBRyxDQUFDLFNBQVMsR0FBRyxtQkFBbUIsQ0FBQztnQkFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2xDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUUsQ0FBQztZQUNELEVBQUUsRUFBQyxFQUFFLENBQUMsSUFBSSxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzNDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsa0JBQWtCLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNsQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzlFLENBQUM7WUFDRCxFQUFFLEVBQUMsRUFBRSxDQUFDLElBQUksR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMzQyxHQUFHLENBQUMsU0FBUyxHQUFHLG1CQUFtQixDQUFDO2dCQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDbEMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLHVCQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM5RSxDQUFDO1lBQ0QsRUFBRSxFQUFDLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDOUIsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDM0MsR0FBRyxDQUFDLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ2xDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDOUUsQ0FBQztZQUVELElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNoQixDQUFDO0lBQ0wsQ0FBQztJQUVNLGtCQUFrQixDQUFDLENBQVMsRUFBRSxDQUFTLEVBQUUsS0FBYSxFQUFFLEtBQWEsRUFDcEUsWUFBbUIsS0FBSyxFQUFFLFlBQW1CLEtBQUssRUFBRSxrQkFBeUIsS0FBSztRQUN0RixFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7WUFFYixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUM7WUFDbkI7Z0JBQ0ksTUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ2xFLEVBQUUsRUFBQyxTQUFTLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3hCLGtFQUFrRTtvQkFDbEUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDMUQsQ0FBQztnQkFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDN0Isd0VBQXdFO29CQUN4RSxLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ25ELENBQUM7WUFDTCxDQUFDO1lBRUQsbUNBQW1DO1lBQ25DLEVBQUUsRUFBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNYLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzNDLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3hDLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsd0JBQXdCLENBQUM7Z0JBQ25DLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLGNBQWMsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQztnQkFDbEMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDckIsR0FBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdEMsQ0FBQztZQUVELG1DQUFtQztZQUNuQyxFQUFFLEVBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDWCxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMzQyxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN4QyxJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMxQyxHQUFHLENBQUMsR0FBRyxHQUFHLHdCQUF3QixDQUFDO2dCQUNuQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxjQUFjLENBQUM7Z0JBQ25DLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLGNBQWMsQ0FBQztnQkFDcEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO2dCQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUM7Z0JBQ3BDLEdBQUcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3JCLEdBQUcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RDLENBQUM7WUFFRCxrQ0FBa0M7WUFDbEM7Ozs7ZUFJRztZQUVILElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNoQixDQUFDO0lBQ0wsQ0FBQztJQUVNLG9CQUFvQixDQUFDLENBQVMsRUFBRSxDQUFTLEVBQUUsS0FBYSxFQUFFLEtBQWE7UUFDMUUsRUFBRSxFQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBRWIsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ25CLGlCQUFpQixRQUFnQjtnQkFDN0IsS0FBSyxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN0RCxDQUFDO1lBRUQsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDL0QsR0FBRyxFQUFDLE1BQU0sUUFBUSxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzNDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUVuRCxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDTixHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQzlCLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ0osR0FBRyxDQUFDLFNBQVMsR0FBRyxRQUFRLEdBQUcsUUFBUSxDQUFDO2dCQUN4QyxDQUFDO2dCQUVELElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNsQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hGLENBQUM7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEIsQ0FBQztJQUNMLENBQUM7Q0FDSjtBQTNLRCxrQ0EyS0M7Ozs7Ozs7Ozs7Ozs7OztBQzdLRCw0RkFBMEM7QUFFMUM7SUFHSSxZQUEyQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBU3pDLFlBQU8sR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3BDLENBQUM7UUFYRyxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFzQixDQUFDO1FBQy9FLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFFTSxNQUFNO1FBQ1QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQ3JDLENBQUM7Q0FNSjtBQWhCRCxnQ0FnQkM7Ozs7Ozs7Ozs7Ozs7OztBQ2xCRCw0RkFBMEM7QUFFMUM7SUFHSSxZQUEyQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBYXpDLFlBQU8sR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNaLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUN6QyxDQUFDO1FBZkcsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBbUIsQ0FBQztRQUM3RSxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLHVCQUFVLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM1RSxDQUFDO0lBRU0sSUFBSTtRQUNQLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7SUFDakQsQ0FBQztJQUVNLElBQUk7UUFDUCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO0lBQ2xELENBQUM7Q0FNSjtBQXBCRCxrQ0FvQkM7Ozs7Ozs7Ozs7Ozs7OztBQ25CRCwwRkFBdUY7QUFHdkYsNEZBQXVDO0FBQ3ZDLCtGQUF5QztBQUN6QywyR0FBaUQ7QUFDakQsc0ZBQW1DO0FBQ25DLHdHQUErQztBQUMvQywyR0FBaUQ7QUFDakQsd0dBQStDO0FBQy9DLHNGQUFtQztBQUNuQywrRkFBeUM7QUFDekMsK0ZBQXlDO0FBQ3pDLDJHQUFpRDtBQUNqRCx3R0FBK0M7QUFDL0MsMkdBQWlEO0FBQ2pELDRGQUF1QztBQUN2QyxzSUFBbUU7QUFDbkUsK0ZBQXlDO0FBQ3pDLGtHQUEwQztBQUMxQyxrR0FBMkM7QUFDM0MsMEhBQTJEO0FBRTNEO0lBcUJJLFlBQTRCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDOUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLGlCQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLHFCQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLHVCQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLCtCQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLGlCQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLDZCQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLCtCQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLDZCQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLHVCQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLHVCQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLCtCQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLDZCQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLCtCQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLHFCQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksaURBQXVCLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLHVCQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLHlCQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLHlCQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUkseUNBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVNLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBc0I7UUFDcEMsRUFBRSxFQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDWixNQUFNLENBQUMsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVELGtDQUFrQztJQUMzQixjQUFjO1FBQ2pCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUVELDhEQUE4RDtJQUN2RCxzQkFBc0IsQ0FBQyxnQkFBaUQsRUFBRSxRQUFzQjtRQUNuRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRUQsMENBQTBDO0lBQ25DLHVCQUF1QjtRQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCxxQkFBcUI7SUFDZCxnQkFBZ0IsQ0FBQyxJQUFZLEVBQUUsUUFBZ0I7UUFDbEQsTUFBTSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELHVDQUF1QztJQUNoQyxnQkFBZ0I7UUFDbkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDekMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFRCxxQ0FBcUM7SUFDOUIsZ0JBQWdCO1FBQ25CLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLHNCQUFhLENBQUMsTUFBTSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVGLElBQUksQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMxRCxhQUFhLEdBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ3ZDLENBQUM7SUFFRCx3Q0FBd0M7SUFDakMsa0JBQWtCO1FBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDbkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzFELGFBQWEsR0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDakYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVELDZCQUE2QjtJQUN0QixhQUFhLENBQUMsS0FBa0Q7UUFDbkUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQUVELDJEQUEyRDtJQUNwRCxrQkFBa0I7UUFDckIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsZ0RBQWdELENBQUMsQ0FBQztRQUN2RSxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVELHNEQUFzRDtJQUMvQyxXQUFXO1FBQ2QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2xDLENBQUM7SUFFRCw4Q0FBOEM7SUFDdkMsb0JBQW9CO1FBQ3ZCLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDbEMsZ0NBQWdDO0lBQ3BDLENBQUM7SUFFRCxnREFBZ0Q7SUFDekMsd0JBQXdCO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztJQUMvQyxDQUFDO0lBRUQsNkJBQTZCO0lBQ3RCLDBCQUEwQjtRQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFRCxzQkFBc0I7SUFDZixtQkFBbUIsQ0FBQyxnQkFBd0IsRUFBRSxXQUFvQixFQUFFLFlBQXFCLEVBQ3hGLFdBQW9CLEVBQUUsV0FBb0IsRUFBRSxXQUFvQixFQUNoRSxXQUFvQjtRQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RDLEVBQUUsRUFBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNuRSxDQUFDO1FBQ0QsRUFBRSxFQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDYixJQUFJLENBQUMsZUFBZSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDOUMsQ0FBQztRQUNELEVBQUUsRUFBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQy9DLENBQUM7UUFDRCxFQUFFLEVBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNiLElBQUksQ0FBQyxlQUFlLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM5QyxDQUFDO1FBQ0QsRUFBRSxFQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDYixJQUFJLENBQUMsZUFBZSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDOUMsQ0FBQztRQUNELEVBQUUsRUFBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ2IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzlDLENBQUM7UUFDRCxFQUFFLEVBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUNiLElBQUksQ0FBQyxlQUFlLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM5QyxDQUFDO0lBQ0wsQ0FBQztJQUVELDJDQUEyQztJQUNwQyxXQUFXLENBQUMsSUFBWSxFQUFFLElBQVksRUFBRSxNQUFlO1FBQzFELElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDbkMsRUFBRSxFQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDUixJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLHNCQUFhLENBQUMsTUFBTSxFQUFFLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNoSixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUMsc0JBQWEsQ0FBQyxJQUFJO29CQUNyRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbEksQ0FBQztRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsOENBQThDO0lBQ3ZDLGFBQWEsQ0FBQyxNQUFvQjtRQUNyQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQsNkNBQTZDO0lBQ3RDLGFBQWEsQ0FBQyxNQUFjO1FBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELDREQUE0RDtJQUNyRCxlQUFlLENBQUMsUUFBZ0IsRUFBRSxNQUFjLEVBQUUsVUFBa0IsRUFBRSxVQUFrQjtRQUMzRixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELDhEQUE4RDtJQUN2RCxnQkFBZ0IsQ0FBQyxNQUF5QjtRQUM3QyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVELDJEQUEyRDtJQUNwRCxlQUFlLENBQUMsUUFBZ0IsRUFBRSxNQUFjO1FBQ25ELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsWUFBWSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRUQsOERBQThEO0lBQ3ZELGlCQUFpQixDQUFDLFFBQWdCLEVBQUUsTUFBYztRQUNyRCxJQUFJLENBQUMsWUFBWSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLHNCQUFhLENBQUMsT0FBTyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2xHLENBQUM7SUFFRCwrQ0FBK0M7SUFDeEMsa0JBQWtCLENBQUMsUUFBZ0IsRUFBRSxVQUFtQjtRQUMzRCxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDdEQsZ0NBQWdDO0lBQ3BDLENBQUM7SUFFRCx3RUFBd0U7SUFDakUsYUFBYTtRQUNoQixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELDZDQUE2QztJQUN0QyxZQUFZLENBQUMsVUFBa0I7UUFDbEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsb0NBQW9DLFVBQVUsc0JBQXNCLENBQUMsQ0FBQztJQUNyRyxDQUFDO0lBRUQsMkRBQTJEO0lBQ3BELGNBQWM7UUFDakIsMkRBQTJEO1FBQzNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVELCtCQUErQjtJQUN4QixVQUFVLENBQUMsU0FBd0IsRUFBRSxjQUF1QixFQUFFLGVBQXdCO1FBQ3pGLG9DQUFvQztRQUNwQyxFQUFFLEVBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUNqQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNqQyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ25DLENBQUM7UUFFRCxtQ0FBbUM7UUFDbkMsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLEdBQUcsRUFBQyxNQUFNLFFBQVEsSUFBSSxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzlCLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RELElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztZQUNyRSxNQUFNLElBQUksb0NBQW9DLFVBQVUsZUFBZSxDQUFDO1FBQzVFLENBQUM7UUFDRCxNQUFNLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM3QixNQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxhQUFhLENBQUM7UUFDcEUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELDhEQUE4RDtJQUN2RCxxQkFBcUI7UUFDeEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQsNERBQTREO0lBQ3JELG1CQUFtQjtRQUN0QixJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3JDLENBQUM7SUFFRCx5REFBeUQ7SUFDbEQsZ0JBQWdCO0lBRXZCLENBQUM7SUFFRCxvRUFBb0U7SUFDN0QsZUFBZTtRQUNsQixJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFRCxtRUFBbUU7SUFDNUQsYUFBYTtRQUNoQixJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVELCtFQUErRTtJQUN4RSxvQkFBb0IsQ0FBQyxDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVMsRUFBRSxDQUFTO1FBQ2xFLElBQUksQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVELCtFQUErRTtJQUN4RSxvQkFBb0IsQ0FBQyxDQUFTLEVBQUUsQ0FBUyxFQUFFLENBQVMsRUFBRSxDQUFTO1FBQ2xFLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxpRkFBaUY7SUFDMUUsc0JBQXNCLENBQUMsSUFBWSxFQUFFLElBQVksRUFBRSxNQUFjLEVBQUUsQ0FBUyxFQUFFLENBQVM7UUFDMUYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELCtEQUErRDtJQUN4RCx3QkFBd0IsQ0FBQyxDQUFTLEVBQUUsQ0FBUyxFQUFFLEtBQWEsRUFDM0QsS0FBYSxFQUFFLFNBQWtCLEVBQUUsU0FBa0I7UUFDekQsSUFBSSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNyRixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxnREFBZ0Q7SUFDekMsa0JBQWtCLENBQUMsR0FBZ0IsRUFBRSxDQUFTLEVBQUUsQ0FBUztRQUM1RCxJQUFJLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELDREQUE0RDtJQUNyRCxvQkFBb0IsQ0FBQyxDQUFTLEVBQUUsQ0FBUyxFQUFFLEtBQWEsRUFBRSxLQUFhO1FBQzFFLElBQUksQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELHNEQUFzRDtJQUMvQyxXQUFXLENBQUMsTUFBa0IsRUFBRSxDQUFTLEVBQUUsQ0FBUztRQUN2RCxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCwyRUFBMkU7SUFDcEUsY0FBYyxDQUFDLENBQVMsRUFBRSxDQUFTLEVBQUUsUUFBZ0I7UUFDeEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRCw0Q0FBNEM7SUFDckMsZUFBZSxDQUFDLElBQVk7UUFDL0IsK0NBQStDO1FBQy9DLEVBQUUsRUFBQyxJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUMsQ0FBQztJQUNMLENBQUM7SUFFRCx3REFBd0Q7SUFDakQsa0JBQWtCO1FBQ3JCLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRUQsK0RBQStEO0lBQ3hELHNCQUFzQixDQUFDLFFBQWdCO1FBQzFDLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxpREFBaUQ7SUFDMUMsa0JBQWtCLENBQUMsVUFBa0I7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxvQ0FBb0MsVUFBVSw4QkFBOEIsQ0FBQyxDQUFDO0lBQzdHLENBQUM7SUFFRCxrREFBa0Q7SUFDM0MsYUFBYSxDQUFDLFVBQWtCLEVBQUUsR0FBVztRQUNoRCxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxvQ0FBb0MsVUFBVSxnQkFBZ0IsR0FBRyxFQUFFLENBQUMsQ0FBQztJQUNwRyxDQUFDO0lBRU0sY0FBYztRQUNqQixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQzdCLENBQUM7Q0FDSjtBQXhXRCxrQkF3V0M7Ozs7Ozs7Ozs7Ozs7OztBQ2xZRCw0RkFBMEM7QUFDMUMsbUdBQTZDO0FBQzdDLG9GQUFpRTtBQUVqRTtJQU9JLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFOaEMsY0FBUyxHQUFHLENBQUMsQ0FBQztRQUNkLGNBQVMsR0FBRyxDQUFDLENBQUM7UUFtRXZCLGdCQUFXLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUN4QixFQUFFLEVBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFBQyxNQUFNLENBQUM7WUFBQyxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNuQixNQUFNLFNBQVMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3JFLE1BQU0sTUFBTSxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN6RCxJQUFJLFNBQVMsRUFBRSxRQUFRLENBQUM7WUFFeEIsRUFBRSxFQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEIsMkJBQTJCO2dCQUMzQixTQUFTLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDcEMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDdkMsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMzQixvQ0FBb0M7Z0JBQ3BDLFNBQVMsR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNwQyxRQUFRLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN2QyxDQUFDO1lBRUQsRUFBRSxFQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekQsTUFBTSxDQUFDO1lBQ1gsQ0FBQztZQUVELCtCQUErQjtZQUMvQixHQUFHLEVBQUMsTUFBTSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLEdBQUcsRUFBQyxNQUFNLFFBQVEsSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUN4QixRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7Z0JBQ2hDLENBQUM7WUFDTCxDQUFDO1lBRUQsOERBQThEO1lBQzlELE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUMvQyxDQUFDO1FBRU8sZ0JBQVcsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3hCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixDQUFDO1FBRU8sZUFBVSxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDdkIsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLENBQUM7UUFFTyxXQUFNLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNuQixFQUFFLEVBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFBQyxNQUFNLENBQUM7WUFBQyxDQUFDO1lBQ3pCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNuQixNQUFNLFNBQVMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3JFLE1BQU0sTUFBTSxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN6RCxJQUFJLFNBQVMsRUFBRSxRQUFRLENBQUM7WUFFeEIsRUFBRSxFQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEIsMkJBQTJCO2dCQUMzQixTQUFTLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixRQUFRLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0Isb0NBQW9DO2dCQUNwQyxTQUFTLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixRQUFRLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLENBQUM7WUFFRCxFQUFFLEVBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN6RCxNQUFNLENBQUM7WUFDWCxDQUFDO1lBRUQsMERBQTBEO1lBQzFELE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVELENBQUM7UUFzSE8sZ0JBQVcsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3hCLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMvRSxNQUFNLFFBQVEsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBRXpELHFFQUFxRTtZQUNyRSxHQUFHLEVBQUMsTUFBTSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLEdBQUcsRUFBQyxNQUFNLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUNwQixFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDMUIsTUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO3dCQUMxRCxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs0QkFDTixNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7NEJBQ2hELEVBQUUsRUFBQyxHQUFHLElBQUksUUFBUSxDQUFDLENBQUMsQ0FBQztnQ0FDakIsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDOzRCQUNyQyxDQUFDO3dCQUNMLENBQUM7b0JBQ0wsQ0FBQztnQkFDTCxDQUFDO1lBQ0wsQ0FBQztZQUVELHlFQUF5RTtZQUN6RSwwSEFBMEg7WUFDMUgsSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3QyxRQUFRLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO1lBQzVCLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUMsT0FBTyxDQUFDLFNBQVMsSUFBSSxDQUFDO1lBQ2pGLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLEdBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxDQUFDO1lBRXBGLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEMsR0FBRyxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDL0IsR0FBRyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxQixHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7WUFDaEMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBQyxPQUFPLENBQUMsU0FBUyxJQUFJLENBQUM7WUFDM0UsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFaEQsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDbEQsQ0FBQyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBQyxDQUFDLEVBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxHQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXpDLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEMsQ0FBQztRQUVPLGNBQVMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3RCLHdFQUF3RTtZQUN4RSxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3hELElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDaEMsTUFBTSxRQUFRLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRTNDLDBDQUEwQztZQUMxQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDMUIsQ0FBQztRQUVPLFlBQU8sR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3BCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNuQixFQUFFLEVBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLElBQUkscUJBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUNyRCxNQUFNLFFBQVEsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLE1BQU0sTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzNDLEVBQUUsRUFBQyxnQkFBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDakIsRUFBRSxFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO3dCQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO3dCQUN4RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztvQkFDN0IsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDSixJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUM7b0JBQ25FLENBQUM7Z0JBQ0wsQ0FBQztnQkFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLFlBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QyxDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7UUFFTyxZQUFPLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNwQixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDbkIsTUFBTSxNQUFNLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN2RCxNQUFNLFFBQVEsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ3pELElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksaUJBQVUsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDeEYsQ0FBQztRQWhVRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksS0FBSyxFQUF5QixDQUFDO1FBQ3BELEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUNyQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssRUFBa0IsQ0FBQyxDQUFDO1lBQ2pELEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDckMsTUFBTSxRQUFRLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsR0FBQyxDQUFDLENBQUMsR0FBQyxDQUFDLENBQUMsRUFBRSxDQUFtQixDQUFDO2dCQUNyRixRQUFRLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDekQsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3pELFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN2RCxRQUFRLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDM0QsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQsNkVBQTZFO0lBQ3RFLGVBQWU7UUFDbEIsR0FBRyxFQUFDLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLEdBQUcsRUFBQyxNQUFNLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixFQUFFLEVBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7b0JBQ2pCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDOUMsRUFBRSxFQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDcEIsTUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUM1QixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDOUMsRUFBRSxFQUFDLGdCQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNqQixNQUFNLENBQUMsSUFBSSxDQUFDO3dCQUNoQixDQUFDO29CQUNMLENBQUM7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQseUVBQXlFO0lBQ2xFLGlCQUFpQixDQUFDLElBQVk7UUFDakMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsR0FBRyxFQUFDLE1BQU0sS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2hDLEdBQUcsRUFBQyxNQUFNLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixFQUFFLEVBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7b0JBQ2pCLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDOUMsRUFBRSxFQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDcEIsTUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUM1QixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDOUMsRUFBRSxFQUFDLGdCQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQzdCLENBQUM7b0JBQ0wsQ0FBQztnQkFDTCxDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRU0sZ0JBQWdCO1FBQ25CLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLEdBQUcsRUFBQyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNoQyxHQUFHLEVBQUMsTUFBTSxJQUFJLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1lBQzVCLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQXFFTSxhQUFhO1FBQ2hCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRUQsMkNBQTJDO0lBQ25DLFFBQVEsQ0FBQyxRQUFnQixFQUFFLFVBQWtCLEVBQUUsVUFBa0I7UUFDckUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRU8sVUFBVSxDQUFDLElBQVUsRUFBRSxTQUFpQixFQUFFLFFBQWdCLEVBQUUsS0FBSyxHQUFHLElBQUksRUFBRSxJQUFJLEdBQUcsS0FBSztRQUMxRixNQUFNLFVBQVUsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEQsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzFELE1BQU0sS0FBSyxHQUFHLFVBQVUsQ0FBQztRQUV6QixxQ0FBcUM7UUFDckMsRUFBRSxFQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzlDLEVBQUUsRUFBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNQLEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxVQUFVLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztvQkFDOUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQztnQkFDNUQsQ0FBQztZQUNMLENBQUM7WUFDRCxNQUFNLENBQUM7UUFDWCxDQUFDO1FBRUQsK0JBQStCO1FBQy9CLEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxVQUFVLEVBQUUsQ0FBQyxHQUFHLFVBQVUsR0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDekQsRUFBRSxFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLEVBQUUsRUFBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUNQLEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxVQUFVLEVBQUUsQ0FBQyxHQUFHLFVBQVUsR0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7d0JBQ3pELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUM7b0JBQzVELENBQUM7Z0JBQ0wsQ0FBQztnQkFDRCxNQUFNLENBQUM7WUFDWCxDQUFDO1FBQ0wsQ0FBQztRQUVELCtCQUErQjtRQUMvQixHQUFHLEVBQUMsSUFBSSxDQUFDLEdBQUcsVUFBVSxFQUFFLENBQUMsR0FBRyxVQUFVLEdBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3pELEVBQUUsRUFBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNQLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxNQUFNLENBQUM7WUFDNUQsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNOLG9EQUFvRDtZQUNwRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDcEQsQ0FBQztJQUNMLENBQUM7SUFFTSxrQkFBa0IsQ0FBQyxRQUFnQixFQUFFLE1BQWM7UUFDdEQsR0FBRyxFQUFDLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDO1lBQ2pELEdBQUcsRUFBQyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQztnQkFDakQsTUFBTSxNQUFNLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsTUFBTSxJQUFJLFFBQVEsSUFBSSxLQUFLLElBQUksS0FBSyxFQUFFLENBQUMsQ0FBQztnQkFDaEcsRUFBRSxFQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ1IsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsMEJBQTBCLENBQUMsQ0FBQztvQkFDMUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLENBQUM7Z0JBQzdFLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCwrREFBK0Q7SUFDeEQsV0FBVyxDQUFDLFFBQWdCLEVBQUUsTUFBYyxFQUFFLFVBQWtCLEVBQUUsVUFBa0I7UUFDdkYsRUFBRSxFQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksTUFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDdEQsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUNELE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRTNDLDZDQUE2QztRQUM3QyxFQUFFLEVBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDOUMsTUFBTSxDQUFDO1FBQ1gsQ0FBQztRQUVELHlFQUF5RTtRQUN6RSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBRTFDLE1BQU0sS0FBSyxHQUFHLFVBQVUsQ0FBQyxDQUFHLCtDQUErQztRQUMzRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDVixHQUFHLEVBQUMsSUFBSSxLQUFLLEdBQUcsVUFBVSxFQUFFLEtBQUssR0FBRyxVQUFVLEdBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDO1lBQ3JFLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUMseUVBQXlFO1lBQ3pFLDBIQUEwSDtZQUMxSCxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsaUJBQWlCLE1BQU0sSUFBSSxRQUFRLElBQUksS0FBSyxJQUFJLEtBQUssRUFBRSxDQUFDO1lBQ2pFLEdBQUcsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUNyQixHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsR0FBQyxDQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUM7WUFDN0QsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUMsQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxDQUFDO1lBQ2hFLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7WUFDckMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEdBQUMsQ0FBQyxJQUFJLENBQUM7WUFDNUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDcEQsR0FBRyxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDaEQsR0FBRyxDQUFDLGdCQUFnQixDQUFDLHVCQUFVLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMzRCxHQUFHLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3RCLENBQUMsRUFBRSxDQUFDO1FBQ1IsQ0FBQztRQUVELDhDQUE4QztRQUM5QyxHQUFHLEVBQUMsSUFBSSxLQUFLLEdBQUcsVUFBVSxFQUFFLEtBQUssR0FBRyxVQUFVLEdBQUMsSUFBSSxDQUFDLFNBQVMsR0FBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQztZQUN2RSx3Q0FBd0M7WUFDeEMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7WUFDaEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQUNuRCxDQUFDO1FBRUQsOENBQThDO1FBQzlDLEdBQUcsRUFBQyxJQUFJLEtBQUssR0FBRyxVQUFVLEdBQUMsQ0FBQyxFQUFFLEtBQUssR0FBRyxVQUFVLEdBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsRUFBRSxDQUFDO1lBQ3ZFLHVDQUF1QztZQUN2QyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDbEQsQ0FBQztRQUVELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBZ0ZELHlFQUF5RTtJQUNsRSxjQUFjO1FBQ2pCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRU0sY0FBYztRQUNqQixHQUFHLEVBQUMsTUFBTSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDOUIsR0FBRyxFQUFDLE1BQU0sSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BCLElBQUksUUFBUSxHQUFHLEtBQUssQ0FBQztnQkFDckIsMENBQTBDO2dCQUMxQyxFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDMUIsTUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUMxRCxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDTixJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7d0JBQ2xDLE1BQU0sR0FBRyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQzt3QkFDaEQsRUFBRSxFQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9COzRCQUMvQixHQUFHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDOzRCQUMzRCxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUM7NEJBQ3ZDLFFBQVEsR0FBRyxJQUFJLENBQUM7d0JBQ3BCLENBQUM7b0JBQ0wsQ0FBQztnQkFDTCxDQUFDO2dCQUNELDJEQUEyRDtnQkFDM0QsRUFBRSxFQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDWCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7Z0JBQzVCLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7Q0FDSjtBQXZXRCx3Q0F1V0M7Ozs7Ozs7Ozs7Ozs7OztBQ3pXRDtJQUdJLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDN0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFtQixDQUFDO0lBQ3hGLENBQUM7SUFFTSxjQUFjLENBQUMsQ0FBUyxFQUFFLENBQVMsRUFBRSxRQUFRLEdBQUMsS0FBSztRQUN0RCxFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQix3QkFBd0I7WUFDeEIsT0FBTSxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNuQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BFLENBQUM7WUFFRCxrQ0FBa0M7WUFDbEMsRUFBRSxFQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ1YsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDM0MsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDeEMsSUFBSSxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDMUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxxQkFBcUIsQ0FBQztnQkFDaEMsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsY0FBYyxDQUFDO2dCQUNuQyxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQztnQkFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDO2dCQUNsQyxHQUFHLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyQixHQUFHLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN0QixJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN6QyxDQUFDO1lBRUQsa0NBQWtDO1lBQ2xDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsUUFBUSxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsR0FBQyxDQUFDLEtBQUssQ0FBQztZQUN6RixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztZQUV6QyxtQ0FBbUM7WUFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztRQUNyRCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO1FBQ3BELENBQUM7SUFDTCxDQUFDO0NBQ0o7QUF0Q0Qsd0NBc0NDOzs7Ozs7Ozs7Ozs7Ozs7QUN0Q0Q7SUFNSSxZQUEyQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBK0R6QyxnQkFBVyxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDeEIsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBRS9FLHlFQUF5RTtZQUN6RSwwSEFBMEg7WUFDMUgsSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3QyxRQUFRLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO1lBQzVCLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUMsT0FBTyxDQUFDLFNBQVMsSUFBSSxDQUFDO1lBQzNFLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEdBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxDQUFDO1lBRTlFLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEMsR0FBRyxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDL0IsR0FBRyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxQixHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7WUFDaEMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBQyxPQUFPLENBQUMsU0FBUyxJQUFJLENBQUM7WUFDckUsUUFBUSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFaEQsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDbEQsQ0FBQyxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBQyxDQUFDLEVBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxHQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZDLENBQUM7UUFFTyxjQUFTLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUN0Qix3RUFBd0U7WUFDeEUsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUN4RCxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3BDLENBQUM7UUFjTyxnQkFBVyxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDeEIsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLENBQUM7UUFFTyxlQUFVLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUN2QixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsQ0FBQztRQUVPLFdBQU0sR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ25CLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNuQixNQUFNLFFBQVEsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ2pGLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZDLENBQUM7UUFwSEcsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFtQixDQUFDO1FBQ2xGLElBQUksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBbUIsQ0FBQztRQUNwRixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFtQixDQUFDO1FBQzVFLElBQUksQ0FBQyxXQUFXLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQW1CLENBQUM7UUFDOUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVPLEtBQUs7UUFDVCxPQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNwRSxDQUFDO0lBQ0wsQ0FBQztJQUVNLGFBQWEsQ0FBQyxLQUFrRDtRQUNuRSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDYixHQUFHLEVBQUMsTUFBTSxJQUFJLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQztZQUN0QixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkQsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QyxHQUFHLENBQUMsRUFBRSxHQUFHLFFBQVEsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEQsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNwQyxHQUFHLENBQUMsR0FBRyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7WUFDeEIsR0FBRyxDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ3pCLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUMsT0FBTyxDQUFDLFNBQVMsSUFBSSxDQUFDO1lBQ3RFLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEdBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSSxDQUFDO1lBQ3pFLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pDLENBQUM7UUFFRCxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBRTlFLEdBQUcsRUFBQyxNQUFNLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDaEgsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNoSCxDQUFDO0lBQ0wsQ0FBQztJQUVNLE9BQU8sQ0FBQyxRQUFnQixFQUFFLE1BQWM7UUFDM0MsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUMsTUFBTSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQyxHQUFHLENBQUMsRUFBRSxHQUFHLFFBQVEsTUFBTSxJQUFJLFFBQVEsRUFBRSxDQUFDO1FBQ3RDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDcEMsR0FBRyxDQUFDLEdBQUcsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO1FBQ3hCLEdBQUcsQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztRQUN6QixHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUksQ0FBQztRQUN0RSxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxHQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksQ0FBQztRQUN6RSxHQUFHLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNwRCxHQUFHLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO1FBQ2hELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7SUFFTSxVQUFVLENBQUMsUUFBZ0IsRUFBRSxNQUFjO1FBQzlDLE1BQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxNQUFNLElBQUksUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNyRSxFQUFFLEVBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNSLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNoQixFQUFFLEVBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7WUFDbkQsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBZ0NNLGNBQWM7UUFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztRQUM5QyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO0lBQ25ELENBQUM7SUFFTSxlQUFlO1FBQ2xCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7UUFDN0MsRUFBRSxFQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7UUFDcEQsQ0FBQztJQUNMLENBQUM7Q0FlSjtBQTVIRCxzQ0E0SEM7Ozs7Ozs7Ozs7Ozs7OztBQzlIRCw0RkFBMEM7QUFDMUMsbUdBQTZDO0FBQzdDLGdHQUEyQztBQUczQywwRkFBOEM7QUFFOUMsMkdBQWlEO0FBRWpEO0lBZ0RJLGtDQUFrQztJQUNsQyxZQUEyQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBdkN6QyxjQUFTLEdBQVcsR0FBRyxDQUFDO1FBc0JoQywyQ0FBMkM7UUFDM0MseURBQXlEO1FBQ2pELGlCQUFZLEdBQUcsV0FBVyxDQUFDLENBQUMsd0JBQXdCO1FBQ3BELGVBQVUsR0FBRyxFQUFFLENBQUM7UUFDaEIsY0FBUyxHQUFXLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDbEUsY0FBUyxHQUFXLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDbEUsdUJBQWtCLEdBQVksSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUNuRSxzQkFBaUIsR0FBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUV4RCx5REFBeUQ7UUFDekQscURBQXFEO1FBQzdDLGdCQUFXLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQTBaMUIsWUFBTyxHQUFHLENBQUMsQ0FBYSxFQUFFLEVBQUU7WUFDaEMsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ25CLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDO1lBQ2xELE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUM7WUFFNUMsRUFBRSxFQUFDLE9BQU8sSUFBSSxhQUFhLElBQUksQ0FBQyxHQUFHLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwRCxNQUFNLE9BQU8sR0FBRyxhQUF5QyxDQUFDO2dCQUMxRCxxQ0FBcUM7Z0JBQ3JDLE1BQU0sVUFBVSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDdEQsTUFBTSxRQUFRLEdBQUcsbUJBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBRTVDLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7b0JBQ25ELG1DQUFtQztvQkFDbkMsRUFBRSxFQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO3dCQUMvQyxJQUFJLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQzlCLElBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQzt3QkFDOUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3RFLENBQUM7Z0JBQ0wsQ0FBQztnQkFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxJQUFJLHFCQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztvQkFDMUQsRUFBRSxFQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQ1gsTUFBTSxFQUFFLEdBQUcsU0FBUyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUNuQyxNQUFNLEVBQUUsR0FBRyxTQUFTLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQ25DLEVBQUUsRUFBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ3pFLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDOzRCQUN2RSxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7NEJBQzNELElBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDOzRCQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzs0QkFDbEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUMzRixDQUFDO29CQUNMLENBQUM7Z0JBQ0wsQ0FBQztnQkFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxJQUFJLHFCQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztvQkFDNUQsRUFBRSxFQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQ1gsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDOzRCQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQzlFLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUNuQyxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUUsSUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO3dCQUNuRSxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDekcsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDOzRCQUM5RCxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxFQUFFLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUM7NEJBQ3RGLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxJQUFJLHNCQUFhLENBQUMsTUFBTSxDQUFDO3dCQUUzRCxFQUFFLEVBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNmLCtDQUErQzs0QkFDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUN0RixPQUFPLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO3dCQUNwRCxDQUFDO3dCQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUN0QixrRUFBa0U7NEJBQ2xFLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3pFLEVBQUUsRUFBQyxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUN6RCxDQUFDOzRCQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQ0FDMUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLE9BQU8sQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUM3RyxDQUFDOzRCQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQ0FDbEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3JELENBQUM7d0JBQ0wsQ0FBQztvQkFDTCxDQUFDO2dCQUNMLENBQUM7Z0JBQ0QscURBQXFEO2dCQUNyRCxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3JDLENBQUM7WUFDRCxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFFTyxnQkFBVyxHQUFHLENBQUMsQ0FBYSxFQUFFLEVBQUU7WUFDcEMsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUM7WUFDbEQsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2xELE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQztZQUU1QyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUNqRSxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUNqRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFLENBQUMsSUFBSSxDQUFDO1lBQ25GLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxHQUFHLENBQUM7WUFFbEYsRUFBRSxFQUFDLE9BQU8sSUFBSSxhQUFhLElBQUksQ0FBQyxHQUFHLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwRCxNQUFNLE9BQU8sR0FBRyxhQUF5QyxDQUFDO2dCQUMxRCxxQ0FBcUM7Z0JBQ3JDLE1BQU0sVUFBVSxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDdEQsTUFBTSxRQUFRLEdBQUcsbUJBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBRTVDLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUUzQixFQUFFLEVBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLElBQUkscUJBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUNuRCxxRUFBcUU7b0JBQ3JFLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztvQkFDekcsbUNBQW1DO29CQUNuQyxFQUFFLEVBQUMsQ0FBQyxRQUFRLENBQUMsVUFBVSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7d0JBQy9DLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO3dCQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsMENBQTBDLENBQUM7b0JBQzFFLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7d0JBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRywwQ0FBMEMsQ0FBQztvQkFDMUUsQ0FBQztnQkFDTCxDQUFDO2dCQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLElBQUkscUJBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUMxRCx5RUFBeUU7b0JBQ3pFLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztvQkFDekcsRUFBRSxFQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQ1gsTUFBTSxFQUFFLEdBQUcsU0FBUyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUNuQyxNQUFNLEVBQUUsR0FBRyxTQUFTLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQ25DLEVBQUUsRUFBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ3pFLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDOzRCQUN2RSxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7NEJBQzNELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDOzRCQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsMENBQTBDLENBQUM7d0JBQzFFLENBQUM7d0JBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ0osSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7NEJBQzNCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRywwQ0FBMEMsQ0FBQzt3QkFDMUUsQ0FBQztvQkFDTCxDQUFDO2dCQUNMLENBQUM7Z0JBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsSUFBSSxxQkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7b0JBQzVELElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO29CQUMzQixFQUFFLEVBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzt3QkFDWCxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLENBQUM7NEJBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDOUUsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQ25DLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBRSxJQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQ25FLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLEVBQUUsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUN6RyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7NEJBQzlELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDLEVBQUUsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQzs0QkFDdEYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLElBQUksc0JBQWEsQ0FBQyxNQUFNLENBQUM7d0JBRTNELDJEQUEyRDt3QkFDL0QseUZBQXlGO3dCQUNyRixFQUFFLEVBQUMsU0FBUyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzs0QkFDekIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLDJDQUEyQyxDQUFDOzRCQUN2RSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQzs0QkFDMUIsNENBQTRDOzRCQUM1QyxJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsRUFBRSx3Q0FBd0MsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO3dCQUNqSixDQUFDO3dCQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzs0QkFDbEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLDJDQUEyQyxDQUFDOzRCQUN2RSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQzs0QkFDMUIsNENBQTRDOzRCQUM1QyxJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsRUFBRSw4Q0FBOEMsRUFBRSxFQUFFLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO3dCQUN2SixDQUFDO3dCQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNKLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRywwQ0FBMEMsQ0FBQzs0QkFDdEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQ2hFLENBQUM7b0JBQ0wsQ0FBQztnQkFDTCxDQUFDO2dCQUNELGlEQUFpRDtnQkFDakQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUNyQyxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxhQUFhLElBQUksQ0FBQyxLQUFLLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsRCxNQUFNLE9BQU8sR0FBSSxhQUFzQyxDQUFDLEdBQUcsQ0FBQztnQkFDNUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQzlGLENBQUM7UUFDTCxDQUFDO1FBRU8saUJBQVksR0FBRyxDQUFDLENBQWEsRUFBRSxFQUFFO1lBQ3JDLCtEQUErRDtZQUMvRCxNQUFNLFVBQVUsR0FBRyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxRSxNQUFNLHFCQUFxQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7WUFDckUsTUFBTSxzQkFBc0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1lBQ3hFLElBQUksWUFBWSxHQUFHLHFCQUFxQixHQUFHLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsc0JBQXNCLENBQUM7WUFFbkgseUVBQXlFO1lBQ3pFLE1BQU0sY0FBYyxHQUFHLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwRSxJQUFJLENBQUMsU0FBUyxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDeEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBRTVHLHdEQUF3RDtZQUN4RCx1RUFBdUU7WUFDdkUsRUFBRSxFQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO2dCQUNsQyxFQUFFLEVBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3BCLGNBQWM7b0JBQ2QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDN0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsR0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFDbkYsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDSixhQUFhO29CQUNiLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQzdFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLEdBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQ25GLENBQUM7WUFDTCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osb0JBQW9CO2dCQUNwQixNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7Z0JBQzVDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUMsQ0FBQyxDQUFDO2dCQUNyRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFDLENBQUMsQ0FBQztnQkFDMUYsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBQyxDQUFDLEdBQUcsQ0FBQyxHQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQzFELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxHQUFHLENBQUMsR0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQy9ELENBQUM7WUFFRCx1Q0FBdUM7WUFDdkMsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDakMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBL2tCRyxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFzQixDQUFDO1FBQ3pFLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQXNCLENBQUM7UUFDNUUsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUVkLHdCQUF3QjtRQUN4QixJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsR0FBRyx1QkFBdUIsQ0FBQztRQUUvQyxnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLEtBQUssRUFBa0IsQ0FBQztJQUNyRCxDQUFDO0lBRUQsdURBQXVEO0lBQ2hELE9BQU87UUFDVixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDO0lBQzdDLENBQUM7SUFFTSxlQUFlLENBQUMsZ0JBQWlELEVBQUUsUUFBc0I7UUFDNUYsTUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQztRQUN2RCxJQUFJLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM3QixHQUFHLEVBQUMsTUFBTSxRQUFRLElBQUksZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDbEYsQ0FBQztRQUNELE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQztRQUUxQixJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEdBQUMsSUFBSSxFQUFFLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxFQUFFO1lBQ2xFLDJFQUEyRTtZQUMzRSxHQUFHLEVBQUMsTUFBTSxRQUFRLElBQUksZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUNyQyxNQUFNLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDOUMsSUFBSSxNQUFNLEdBQVcsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDbEMsSUFBSSxNQUFNLEdBQVcsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDbEMsRUFBRSxFQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDNUIsTUFBTSxXQUFXLEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxHQUFDLFlBQVksQ0FBQyxDQUFDO29CQUNoRCxNQUFNLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDOUMsTUFBTSxFQUFFLEdBQUcsVUFBVSxDQUFDLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUN2QyxJQUFJLElBQUksR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUN4QixJQUFJLElBQUksR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUV4QixFQUFFLEVBQUMsVUFBVSxDQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNuQyxJQUFJLElBQUksQ0FBQyxDQUFDO29CQUNkLENBQUM7b0JBRUQsRUFBRSxFQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ1gsRUFBRSxFQUFDLFVBQVUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3hCLElBQUksSUFBSSxDQUFDLENBQUM7d0JBQ2QsQ0FBQzt3QkFBQyxJQUFJLENBQUMsQ0FBQzs0QkFDSixJQUFJLElBQUksQ0FBQyxDQUFDO3dCQUNkLENBQUM7b0JBQ0wsQ0FBQztvQkFFRCxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUM7d0JBQ1gsQ0FBQyxXQUFXLEdBQUMsQ0FBQyxJQUFJLEdBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDOUIsTUFBTSxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsQ0FBQyxXQUFXLEdBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxHQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsRCxDQUFDO2dCQUNELG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUM7Z0JBQ3pDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUM7WUFDN0MsQ0FBQztZQUNELElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUN0QyxDQUFDLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRU0sT0FBTyxDQUFDLGdCQUErQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWE7UUFDdkYsRUFBRSxFQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFDNUMsRUFBRSxFQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNWLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxtQkFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDN0MsbURBQW1EO1FBQ25ELElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFekUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBRWxELCtCQUErQjtRQUMvQixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQztRQUVsRCxvQkFBb0I7UUFDcEIsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3JDLEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztZQUN2QyxDQUFDO1FBQ0wsQ0FBQztRQUVELEVBQUUsRUFBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ1gsb0NBQW9DO1lBQ3BDLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztZQUMzQixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUM7WUFDNUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxHQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDM0osSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO1lBQzNCLHVEQUF1RDtZQUN2RCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBRSxhQUFhO1lBQzdCLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsS0FBSyxxQkFBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQ3RELE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsQ0FBQztvQkFDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUM5RSxNQUFNLFdBQVcsR0FBRyxDQUFDLENBQUM7Z0JBQ3RCLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUUsSUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0RCxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDL0MsQ0FBQztZQUNELDJDQUEyQztZQUMzQyxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUMvRSx5Q0FBeUM7WUFDekMsS0FBSyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBQ25DLE1BQU0sR0FBRyxHQUFHLFNBQVMsR0FBQyxLQUFLLENBQUM7WUFDNUIsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDeEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDakQsb0RBQW9EO1lBQ3BELElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzVELENBQUM7UUFFRCxnQ0FBZ0M7UUFDaEMsRUFBRSxFQUFDLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFDLENBQUMsQ0FBQztZQUNySSxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUMsQ0FBQyxDQUFDO1lBQzFILElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQztZQUMvQixJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFDekIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQztZQUMzQixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDaEQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNsQixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3ZCLENBQUM7UUFFRCx1REFBdUQ7UUFDdkQsRUFBRSxFQUFDLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFDLENBQUMsQ0FBQztZQUMvSCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUMsQ0FBQyxDQUFDO1lBQ3BILElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFDbEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDO1lBQy9CLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUM7WUFDM0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDdkIsQ0FBQztRQUVELHVEQUF1RDtRQUN2RCxHQUFHLEVBQUMsTUFBTSxRQUFRLElBQUksYUFBYSxDQUFDLENBQUMsQ0FBQztZQUNsQyxNQUFNLE9BQU8sR0FBRyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDeEMsRUFBRSxFQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsTUFBTSxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDdkMsTUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEdBQUMsQ0FBQyxHQUFHLGdCQUFnQixDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFDLENBQUMsQ0FBQztnQkFDeEgsTUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFDLENBQUMsQ0FBQztnQkFDNUcsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUM5RCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO2dCQUNqRCxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBQyxRQUFRLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxTQUFTLEdBQUMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUMzRixDQUFDO1FBQ0wsQ0FBQztRQUVELDhCQUE4QjtRQUM5QixHQUFHLEVBQUMsTUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDcEMsTUFBTSxFQUFFLEdBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEdBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBQyxDQUFDLENBQUM7WUFDNUgsTUFBTSxFQUFFLEdBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFDLENBQUMsQ0FBQztZQUNqSCxNQUFNLEVBQUUsR0FBRyxLQUFLLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFDLENBQUMsQ0FBQztZQUN4SCxNQUFNLEVBQUUsR0FBRyxLQUFLLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUMsQ0FBQyxDQUFDO1lBRTdHLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztZQUM5QixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN4QixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDeEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUN0QixDQUFDO1FBRUQsRUFBRSxFQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDWCxnQ0FBZ0M7WUFDaEMsR0FBRyxFQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDMUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNyRCw0QkFBNEI7b0JBQzVCLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNCLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUVELElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDbkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRU8sUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLFNBQVMsR0FBQyxLQUFLO1FBQzNDLEVBQUUsRUFBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxPQUFPLENBQUMsTUFBTSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2pFLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxNQUFNLFFBQVEsR0FBRyxtQkFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFcEQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxRSxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFFOUQsdUJBQXVCO1FBQ3ZCLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDcEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBRWxCLHNDQUFzQztRQUN0QyxFQUFFLEVBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDZCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7WUFDdkMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDO1lBQ3hDLEVBQUUsRUFBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztnQkFDbkMsUUFBUSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztnQkFDbEMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztZQUN2QyxDQUFDO1lBQ0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztRQUNoRSxDQUFDO1FBRUQsK0NBQStDO1FBQy9DLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO1FBQ2hELEVBQUUsRUFBQyxXQUFXLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ25ELGtEQUFrRDtZQUNsRCxpQ0FBaUM7WUFDN0IsRUFBRSxFQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25FLG1FQUFtRTtnQkFDL0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO2dCQUMvQixJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztZQUMvQixDQUFDO1FBQ0wsQ0FBQztRQUVELEVBQUUsRUFBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ1gsc0JBQXNCO1lBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUM7WUFDL0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3ZCLENBQUM7SUFDTCxDQUFDO0lBRU8sV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQztRQUM3QixPQUFPLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDcEIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN0QyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMvRCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pGLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2hFLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN4RCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3RDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRU8sVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUM7UUFDL0IsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDbkMsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFTyxjQUFjLENBQUMsQ0FBYztRQUNqQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ2pCLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDakIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUMsQ0FBQyxDQUFDO1FBQ3JHLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUMsQ0FBQyxDQUFDLENBQUM7UUFDckYsTUFBTSxJQUFJLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDaEMsTUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBQyxDQUFDLENBQUM7UUFDaEMsdUJBQXVCO1FBQ3ZCLE1BQU0sT0FBTyxHQUFHLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUM7UUFDN0MsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUMsTUFBTSxHQUFHLE9BQU8sR0FBQyxPQUFPLENBQUMsQ0FBQztRQUUzRCxnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUM7UUFDNUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO1FBQzlCLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztRQUMzQixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxFQUFFLEdBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxHQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFDLE9BQU8sR0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxHQUFDLE9BQU8sR0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBQyxPQUFPLEVBQUUsQ0FBQyxHQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9FLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO1FBQzNCLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDbEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUVuQixnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQy9CLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztRQUM5QixJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUM7UUFDakMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsR0FBRyxNQUFNLEdBQUMsR0FBRyxZQUFZLENBQUM7UUFDMUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsRUFBRSxHQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRXBDLG1DQUFtQztRQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVPLHlCQUF5QixDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsR0FBVztRQUM3RCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUMvRSxNQUFNLFFBQVEsR0FBRyxTQUFTLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuQyxNQUFNLE9BQU8sR0FBRyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoQyxNQUFNLE1BQU0sR0FBRyxTQUFTLENBQUMsQ0FBQyxHQUFDLFNBQVMsQ0FBQztRQUNyQyxNQUFNLElBQUksR0FBRyxTQUFTLENBQUMsQ0FBQyxHQUFDLFNBQVMsQ0FBQztRQUNuQyxHQUFHLEVBQUMsSUFBSSxDQUFDLEdBQUcsTUFBTSxFQUFFLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUNqQyx1RkFBdUY7WUFDdkYsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hELE1BQU0sU0FBUyxHQUFHLFFBQVEsR0FBRyxhQUFhLENBQUM7WUFDM0MsTUFBTSxVQUFVLEdBQUcsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFDO1lBQ2pELE1BQU0sTUFBTSxHQUFHLFNBQVMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckQsd0ZBQXdGO1lBQ3hGLEVBQUUsRUFBQyxDQUFDLEdBQUcsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsSUFBSSxHQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLGlCQUFpQjtnQkFDakIsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLEdBQUcsTUFBTSxHQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO29CQUM1QyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBQyxVQUFVLEVBQUUsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUM1QyxDQUFDO1lBQ0wsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLHdDQUF3QztnQkFDeEMsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLEdBQUcsTUFBTSxHQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO29CQUN0QyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBQyxVQUFVLEVBQUUsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUM1QyxDQUFDO2dCQUNELHlDQUF5QztnQkFDekMsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sR0FBQyxTQUFTLEdBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxNQUFNLEdBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7b0JBQzFELElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQzVDLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFTyx5QkFBeUIsQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLEdBQVc7UUFDN0QsaUJBQWlCO1FBQ2pCLGdEQUFnRDtRQUNoRCx3RUFBd0U7UUFDeEUsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDL0UsTUFBTSxRQUFRLEdBQUcsU0FBUyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkMsTUFBTSxPQUFPLEdBQUcsU0FBUyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEMsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLEdBQUMsU0FBUyxHQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksU0FBUyxDQUFDLENBQUMsR0FBQyxTQUFTLEdBQUMsR0FBRyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDekUsdUZBQXVGO1lBQ3ZGLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoRCxNQUFNLFNBQVMsR0FBRyxRQUFRLEdBQUcsYUFBYSxDQUFDO1lBQzNDLE1BQU0sVUFBVSxHQUFHLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQztZQUNqRCxNQUFNLE1BQU0sR0FBRyxTQUFTLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXJELHVCQUF1QjtZQUN2QixHQUFHLEVBQUMsSUFBSSxDQUFDLEdBQUcsTUFBTSxHQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsTUFBTSxHQUFDLFNBQVMsR0FBQyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDcEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUMsVUFBVSxFQUFFLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUM1QyxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFTyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsS0FBSyxFQUFFLE9BQU87UUFDOUMsTUFBTSxPQUFPLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUM1QixNQUFNLE9BQU8sR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQzVCLE1BQU0sUUFBUSxHQUFHLEtBQUssR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLE1BQU0sT0FBTyxHQUFHLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFFNUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDO1FBQzlCLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztRQUN6QixJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUM7UUFFekIsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFHLE9BQU8sR0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLE9BQU8sR0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUNqRCx1RkFBdUY7WUFDdkYsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUM7WUFDNUMsTUFBTSxTQUFTLEdBQUcsUUFBUSxHQUFHLGFBQWEsQ0FBQztZQUMzQyxNQUFNLFVBQVUsR0FBRyxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUM7WUFDakQsTUFBTSxNQUFNLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFDLENBQUMsQ0FBQyxDQUFDO1lBRWpELEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxNQUFNLEVBQUUsQ0FBQyxHQUFHLE1BQU0sR0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixHQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN2RixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQzlELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDdEIsQ0FBQztRQUNMLENBQUM7UUFFRCxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVPLG9CQUFvQixDQUFDLE1BQWMsRUFBRSxNQUFjLEVBQUUsV0FBbUIsRUFBRSxPQUFlLEVBQUUsT0FBZTtRQUM5RyxNQUFNLFFBQVEsR0FBRyxXQUFXLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxNQUFNLE9BQU8sR0FBRyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBRTVCLEdBQUcsRUFBQyxJQUFJLENBQUMsR0FBRyxPQUFPLEdBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxPQUFPLEdBQUMsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7WUFDN0QsdUZBQXVGO1lBQ3ZGLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDO1lBQzVDLE1BQU0sU0FBUyxHQUFHLFFBQVEsR0FBRyxhQUFhLENBQUM7WUFDM0MsTUFBTSxVQUFVLEdBQUcsQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFDO1lBQ2pELE1BQU0sTUFBTSxHQUFHLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBQyxDQUFDLENBQUMsQ0FBQztZQUVqRCxzREFBc0Q7WUFDdEQsR0FBRyxFQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLEdBQUcsTUFBTSxHQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO2dCQUM1QyxFQUFFLEVBQUMsTUFBTSxLQUFLLENBQUMsR0FBQyxVQUFVLElBQUksTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pDLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUVELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQWdNTSx3QkFBd0I7UUFDM0Isb0JBQW9CO1FBQ3BCLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztRQUM1QyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLEdBQUMsQ0FBQyxDQUFDO1FBQ3JHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEdBQUMsQ0FBQyxDQUFDO1FBQzFGLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUMsQ0FBQyxHQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3hELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxHQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzdELENBQUM7SUFFTSxXQUFXLENBQUMsSUFBWSxFQUFFLElBQVk7UUFDekMsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUM7UUFDbEQsRUFBRSxFQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDWCxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFDOUIsRUFBRSxFQUFDLFNBQVMsQ0FBQyxDQUFDLEtBQUssSUFBSSxJQUFJLFNBQVMsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDOUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDOUYsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRU0sYUFBYSxDQUFDLFFBQWdCO1FBQ2pDLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDO1FBQ2xELE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRTlELDREQUE0RDtRQUM1RCxFQUFFLEVBQUMsU0FBUyxJQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSwrQkFBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxFQUFFLGFBQWEsQ0FBQyxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUN0SCxDQUFDO0lBQ0wsQ0FBQztJQUVNLE1BQU07UUFDVCwrRkFBK0Y7UUFDL0YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7UUFDM0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRU0sV0FBVztRQUNkLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQztRQUN6RixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFDLENBQUMsQ0FBQztRQUVwRyxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLHVCQUFVLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRXhHLGdEQUFnRDtRQUNoRCxNQUFNLHFCQUFxQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDckUsTUFBTSxzQkFBc0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQ3hFLElBQUksWUFBWSxHQUFHLHFCQUFxQixHQUFHLHNCQUFzQixDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsc0JBQXNCLENBQUM7UUFDbkgsSUFBSSxDQUFDLFNBQVMsR0FBRyxZQUFZLENBQUM7UUFFOUIsdURBQXVEO1FBQ3ZELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEdBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDN0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsR0FBQyxDQUFDLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUNuRixDQUFDO0lBRUQscUNBQXFDO0lBQzlCLGdCQUFnQjtRQUNuQixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFFOUIsRUFBRSxFQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUNqQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztRQUMxRCxDQUFDO1FBQ0QsNkRBQTZEO1FBQzdELElBQUksQ0FBQyxXQUFXLENBQUM7WUFDYixPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFLENBQUMsSUFBSTtZQUNuRSxPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFLENBQUMsR0FBRztTQUN2RCxDQUFDLENBQUM7SUFDckIsQ0FBQztJQUVELHdDQUF3QztJQUNqQyxrQkFBa0I7UUFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBRTlCLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDMUQsQ0FBQztRQUNELDZEQUE2RDtRQUM3RCxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ2IsT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLElBQUk7WUFDbkUsT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUc7U0FDdkQsQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFFRCw4RUFBOEU7SUFDdkUsa0JBQWtCO1FBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRywwQ0FBMEMsQ0FBQztJQUMxRSxDQUFDO0lBRUQsaUVBQWlFO0lBQ3pELG1CQUFtQixDQUFDLENBQUM7UUFDekIsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixFQUFFLENBQUMsSUFBSSxDQUFDO1FBQ3JFLElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDLEdBQUcsQ0FBQztRQUVwRSxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDaEUsSUFBSSxLQUFLLEdBQUcsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1FBQ3ZELE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0MsQ0FBQztDQUNKO0FBMXVCRCw4QkEwdUJDOzs7Ozs7Ozs7Ozs7Ozs7QUNudkJEO0lBT0ksWUFBbUIsS0FBSyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUk7UUFDM0MsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDbkIsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNmLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO0lBQ3JCLENBQUM7Q0FDSjtBQWRELHdDQWNDOzs7Ozs7Ozs7Ozs7Ozs7QUNaRDtJQUdJLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFpSHpDLGlCQUFZLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUN6QixDQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDbkIsRUFBRSxFQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLHlCQUF5QixDQUFDLENBQUMsQ0FBQztnQkFDMUMsR0FBRyxFQUFDLE1BQU0sSUFBSSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztvQkFDaEUsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEMsR0FBRyxDQUFDLEVBQUUsR0FBRywwQkFBMEIsQ0FBQztvQkFDcEMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFDekMsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDMUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztvQkFDeEMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3JCLEdBQUcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3JCLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM5QixDQUFDO1lBQ0wsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLElBQUkseUJBQXlCLENBQUMsQ0FBQyxDQUFDO2dCQUNqRCxHQUFHLEVBQUMsTUFBTSxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO29CQUNoRSxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN4QyxHQUFHLENBQUMsRUFBRSxHQUFHLDBCQUEwQixDQUFDO29CQUNwQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO29CQUN6QyxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUMxQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO29CQUN4QyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDckIsR0FBRyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDckIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzlCLENBQUM7WUFDTCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7Z0JBQy9DLEdBQUcsRUFBQyxNQUFNLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQzlELElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3hDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsd0JBQXdCLENBQUM7b0JBQ2xDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7b0JBQ3pDLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQzFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUM7b0JBQ3hDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO29CQUNyQixHQUFHLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNyQixDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDOUIsQ0FBQztZQUNMLENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLDJCQUEyQixDQUFDLENBQUMsQ0FBQztnQkFDbkQsR0FBRyxFQUFDLE1BQU0sSUFBSSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztvQkFDakUsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEMsR0FBRyxDQUFDLEVBQUUsR0FBRyw0QkFBNEIsQ0FBQztvQkFDdEMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFDekMsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDMUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztvQkFDeEMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3JCLEdBQUcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3JCLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM5QixDQUFDO1lBQ0wsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLElBQUksMkJBQTJCLENBQUMsQ0FBQyxDQUFDO2dCQUNuRCxHQUFHLEVBQUMsTUFBTSxJQUFJLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO29CQUNqRSxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN4QyxHQUFHLENBQUMsRUFBRSxHQUFHLDRCQUE0QixDQUFDO29CQUN0QyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO29CQUN6QyxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUMxQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO29CQUN4QyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDckIsR0FBRyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDckIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzlCLENBQUM7WUFDTCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hELEdBQUcsRUFBQyxNQUFNLElBQUksSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztvQkFDckUsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEMsR0FBRyxDQUFDLEVBQUUsR0FBRyxpQ0FBaUMsQ0FBQztvQkFDM0MsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztvQkFDekMsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDMUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztvQkFDeEMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3JCLEdBQUcsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3JCLENBQUMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM5QixDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7UUFFTyxpQkFBWSxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDekIsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ25CLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUMxQixDQUFDO1FBMUxHLElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBbUIsQ0FBQztJQUNyRixDQUFDO0lBRU0sY0FBYztRQUNqQixJQUFJLElBQUksR0FBRyxFQUFFLENBQUM7UUFDZCxNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDO1FBRXBELEVBQUUsRUFBQyxTQUFTLENBQUMsU0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLElBQUksU0FBUyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdGLE1BQU0sSUFBSSxHQUFHLFNBQVMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUNoRCxNQUFNLEtBQUssR0FBRyxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBRTtZQUVuRCxJQUFJLElBQUk7OztzRUFHa0QsS0FBSyxLQUFLLElBQUksR0FBRyxTQUFTLENBQUMsU0FBUzt1QkFDbkYsQ0FBQztRQUNoQixDQUFDO1FBRUQsRUFBRSxFQUFDLFNBQVMsQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsSUFBSSxTQUFTLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0YsTUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ2hELE1BQU0sS0FBSyxHQUFHLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFFO1lBRW5ELElBQUksSUFBSTs7O3NFQUdrRCxLQUFLLEtBQUssSUFBSSxHQUFHLFNBQVMsQ0FBQyxTQUFTO3VCQUNuRixDQUFDO1FBQ2hCLENBQUM7UUFFRCxFQUFFLEVBQUMsU0FBUyxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxJQUFJLFNBQVMsQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN2RixNQUFNLElBQUksR0FBRyxTQUFTLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDOUMsTUFBTSxLQUFLLEdBQUcsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUU7WUFFbkQsSUFBSSxJQUFJOzs7c0VBR2tELEtBQUssS0FBSyxJQUFJLEdBQUcsU0FBUyxDQUFDLE9BQU87dUJBQ2pGLENBQUM7UUFDaEIsQ0FBQztRQUVELEVBQUUsRUFBQyxTQUFTLENBQUMsVUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLElBQUksU0FBUyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2hHLE1BQU0sSUFBSSxHQUFHLFNBQVMsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUNqRCxNQUFNLEtBQUssR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBRTtZQUVsRCxJQUFJLElBQUk7OztzRUFHa0QsS0FBSyxLQUFLLElBQUksR0FBRyxTQUFTLENBQUMsVUFBVTt1QkFDcEYsQ0FBQztRQUNoQixDQUFDO1FBRUQsRUFBRSxFQUFDLFNBQVMsQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsSUFBSSxTQUFTLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEcsTUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ2pELE1BQU0sS0FBSyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFFO1lBRWxELElBQUksSUFBSTs7O3NFQUdrRCxLQUFLLEtBQUssSUFBSSxHQUFHLFNBQVMsQ0FBQyxVQUFVO3VCQUNwRixDQUFDO1FBQ2hCLENBQUM7UUFFRCxFQUFFLEVBQUMsU0FBUyxDQUFDLGNBQWMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsa0JBQWtCLElBQUksU0FBUyxDQUFDLGtCQUFrQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUcsTUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3JELE1BQU0sS0FBSyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFFO1lBRWxELElBQUksSUFBSTs7O3NFQUdrRCxLQUFLLEtBQUssSUFBSSxHQUFHLFNBQVMsQ0FBQyxjQUFjO3VCQUN4RixDQUFDO1FBQ2hCLENBQUM7UUFFRCxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFFcEMsTUFBTSxvQkFBb0IsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDaEYsRUFBRSxFQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUN0QixvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3ZFLG9CQUFvQixDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDM0UsQ0FBQztRQUVELE1BQU0sb0JBQW9CLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDO1FBQ2hGLEVBQUUsRUFBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7WUFDdEIsb0JBQW9CLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN2RSxvQkFBb0IsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzNFLENBQUM7UUFFRCxNQUFNLGtCQUFrQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUM1RSxFQUFFLEVBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLGtCQUFrQixDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDckUsa0JBQWtCLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN6RSxDQUFDO1FBRUQsTUFBTSxzQkFBc0IsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLDJCQUEyQixDQUFDLENBQUM7UUFDcEYsRUFBRSxFQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztZQUN4QixzQkFBc0IsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3pFLHNCQUFzQixDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDN0UsQ0FBQztRQUVELE1BQU0sc0JBQXNCLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1FBQ3BGLEVBQUUsRUFBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7WUFDeEIsc0JBQXNCLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN6RSxzQkFBc0IsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzdFLENBQUM7UUFFRCxNQUFNLDBCQUEwQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztRQUM3RixFQUFFLEVBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDO1lBQzVCLDBCQUEwQixDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDN0UsMEJBQTBCLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNqRixDQUFDO0lBQ0wsQ0FBQztDQTZFSjtBQS9MRCxzQ0ErTEM7Ozs7Ozs7Ozs7Ozs7OztBQy9MRDtJQUdJLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDN0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBbUIsQ0FBQztJQUMvRSxDQUFDO0lBRUQsb0RBQW9EO0lBQzdDLGNBQWM7UUFDakIsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2QsR0FBRyxFQUFDLE1BQU0sRUFBRSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztZQUMxQyxNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMvQyxJQUFJLElBQUksZ0NBQWdDLEVBQUU7OzhDQUVSLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUzswQkFDeEQsTUFBTSxDQUFDLElBQUksV0FBVyxDQUFDO1FBQ3pDLENBQUM7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7SUFDckMsQ0FBQztJQUVNLGNBQWMsQ0FBQyxRQUFnQixFQUFFLFVBQW1CO1FBQ3ZELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxHQUFHLElBQUksR0FBRyxVQUFVLENBQUMsQ0FBQztRQUM3RCxNQUFNLFFBQVEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLG9CQUFvQixRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3pFLEVBQUUsRUFBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ1YsNkNBQTZDO1lBQzdDLEVBQUUsRUFBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNaLFFBQVEsQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQztZQUMzQyxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDO1lBQzNDLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztDQUNKO0FBaENELGdDQWdDQzs7Ozs7Ozs7Ozs7Ozs7O0FDbENELDRGQUEwQztBQUUxQztJQU9JLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFjekMsdUJBQWtCLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1lBQzNDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7WUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQ3pDLENBQUM7UUFFTyx5QkFBb0IsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ2pDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7WUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztZQUN6QyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDeEMsQ0FBQztRQXpCRyxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFzQixDQUFDO1FBQ2pGLElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBc0IsQ0FBQztRQUNyRixJQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFtQixDQUFDO1FBQ3JFLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQW1CLENBQUM7UUFDekUsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBbUIsQ0FBQztRQUMzRSxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLHVCQUFVLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ25GLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDM0YsQ0FBQztJQUVNLElBQUk7UUFDUCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO0lBQ2hELENBQUM7Q0FlSjtBQWxDRCxrREFrQ0M7Ozs7Ozs7Ozs7Ozs7OztBQ3BDRCw0RkFBMEM7QUFFMUM7SUFHSSxZQUEyQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBb0MxQyxZQUFPLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDdkMsQ0FBQyxDQUFDO1FBckNFLElBQUksQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2xFLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsdUJBQVUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFTSxNQUFNO1FBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztRQUNwRSxFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNaLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUNsQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDZixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEIsQ0FBQztJQUNMLENBQUM7SUFFTSxJQUFJO1FBQ1AsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztJQUNwRCxDQUFDO0lBRU0sSUFBSTtRQUNQLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7SUFDckQsQ0FBQztJQUVNLE9BQU87UUFDVixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDeEMsQ0FBQztJQUVNLE1BQU07UUFDVCxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDekMsQ0FBQztJQUVNLFNBQVM7UUFDWixNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLFNBQVMsQ0FBQztJQUM3RCxDQUFDO0NBS0o7QUExQ0Qsd0NBMENDOzs7Ozs7Ozs7Ozs7Ozs7QUMzQ0QsZ0dBQTJDO0FBQzNDLG1HQUE2QztBQUM3QywwRkFBOEM7QUFFOUMsb0ZBQStFO0FBRS9FO0lBR0ksWUFBMkIsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUM3QyxJQUFJLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFtQixDQUFDO0lBQy9FLENBQUM7SUFFTSxXQUFXO1FBQ2QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztJQUNoRCxDQUFDO0lBRU8sSUFBSSxDQUFDLENBQVMsRUFBRSxDQUFTO1FBQzdCLGtDQUFrQztRQUNsQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLFFBQVEsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEdBQUMsQ0FBQyxLQUFLLENBQUM7UUFDakYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFDLEVBQUUsSUFBSSxDQUFDO1FBRXhDLG1DQUFtQztRQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO0lBQ2pELENBQUM7SUFFTyxLQUFLO1FBQ1Qsd0JBQXdCO1FBQ3hCLE9BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzVELENBQUM7SUFDTCxDQUFDO0lBRU0sbUJBQW1CLENBQUMsQ0FBUyxFQUFFLENBQVMsRUFBRSxDQUFTLEVBQUUsQ0FBUztRQUNqRSxFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQix3Q0FBd0M7WUFDeEMsTUFBTSxRQUFRLEdBQUcsbUJBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUViLGtDQUFrQztZQUNsQyxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUNsQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVyQyxFQUFFLEVBQUMsUUFBUSxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDMUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxxQkFBcUIsQ0FBQztnQkFDdEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDckMsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNKLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzFDLEdBQUcsQ0FBQyxTQUFTLEdBQUcsNEJBQTRCLENBQUM7Z0JBQzdDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3JDLENBQUM7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdkIsQ0FBQztJQUNMLENBQUM7SUFFTSxlQUFlLENBQUMsQ0FBUyxFQUFFLENBQVMsRUFBRSxDQUFTLEVBQUUsQ0FBUztRQUM3RCxFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsQix3Q0FBd0M7WUFDeEMsTUFBTSxRQUFRLEdBQUcsbUJBQVEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUViLGtDQUFrQztZQUNsQyxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUNsQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVyQyxHQUFHLEVBQUMsTUFBTSxJQUFJLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzlDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN6QyxDQUFDO1lBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLENBQUM7SUFDTCxDQUFDO0lBRU0saUJBQWlCLENBQUMsSUFBWSxFQUFFLElBQVksRUFBRSxNQUFjLEVBQUUsQ0FBUyxFQUFFLENBQVM7UUFDckYsRUFBRSxFQUFDLElBQUksSUFBSSxJQUFJLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDOUIsd0JBQXdCO1lBQ3hCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUViLGtDQUFrQztZQUNsQyxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQztZQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVyQyxJQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVDLEtBQUssQ0FBQyxTQUFTLEdBQUcsR0FBRyxNQUFNLElBQUksQ0FBQztZQUNoQyxLQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUM7WUFDL0IsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsU0FBUyxDQUFDO1lBQzlCLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRW5DLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7WUFFckMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDcEIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLENBQUM7SUFDTCxDQUFDO0lBRU0sZUFBZSxDQUFDLE1BQWtCLEVBQUUsQ0FBUyxFQUFFLENBQVM7UUFDM0QsRUFBRSxFQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNULE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbEQsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDTix3QkFBd0I7WUFDeEIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBRWIsa0NBQWtDO1lBQ2xDLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzlCLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQztZQUNqQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUVyQyxrREFBa0Q7WUFDbEQsRUFBRSxFQUFDLFlBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsTUFBTSxHQUFHLEdBQUcsSUFBVyxDQUFDO2dCQUV4QixFQUFFLEVBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQjtvQkFDaEMsTUFBTSxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQztvQkFDbEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEtBQUsscUJBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUMzRCxJQUFJLFFBQVEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUMvQyxRQUFRLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CO3dCQUNqRCxNQUFNLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ25FLFVBQVUsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLEdBQUMsc0JBQWEsQ0FBQyxZQUFZLEdBQUMsSUFBSSxDQUFDO29CQUN2RSxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUM7b0JBQ2xDLFFBQVEsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFNBQVMsQ0FBQztvQkFDakMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzFDLENBQUM7Z0JBRUQsSUFBSSxXQUFXLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDbEQsV0FBVyxDQUFDLFNBQVMsR0FBRyx3Q0FBd0MsR0FBQyxHQUFHLENBQUMsUUFBUSxHQUFDLFNBQVMsQ0FBQztnQkFDeEYsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO2dCQUNwQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFFekMsSUFBSSxXQUFXLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDbEQsV0FBVyxDQUFDLFNBQVMsR0FBRyx1Q0FBdUMsR0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUMsR0FBRyxHQUFDLEdBQUcsQ0FBQyxHQUFDLFVBQVUsQ0FBQztnQkFDNUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO2dCQUNwQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUM3QyxDQUFDO1lBRUQsSUFBSSxDQUFDLEVBQUUsRUFBQyxnQkFBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdEIsTUFBTSxPQUFPLEdBQUcsSUFBZSxDQUFDO2dCQUVoQyxJQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNwRCxhQUFhLENBQUMsU0FBUyxHQUFHLDBDQUEwQyxHQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUMsU0FBUyxDQUFDO2dCQUNsRyxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQy9DLENBQUM7WUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdkIsQ0FBQztJQUNMLENBQUM7SUFFTSxzQkFBc0IsQ0FBQyxHQUFnQixFQUFFLENBQVMsRUFBRSxDQUFTO1FBQ2hFLEVBQUUsRUFBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ0wsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBRWIsa0NBQWtDO1lBQ2xDLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxlQUFlLEdBQUcsR0FBRyxDQUFDLEtBQUssR0FBRyxlQUFlLENBQUM7WUFDbEUsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRXJDLDRCQUE0QjtZQUM1QixHQUFHLEVBQUMsTUFBTSxJQUFJLElBQUksR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3hDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3BDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDckIsR0FBRyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUN0QixHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxHQUFDLENBQUMsR0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUM7Z0JBQ3RFLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEdBQUMsQ0FBQyxHQUFDLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQztnQkFDeEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDckMsQ0FBQztZQUVELElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN2QixDQUFDO0lBQ0wsQ0FBQztDQUNKO0FBM0xELDBCQTJMQzs7Ozs7Ozs7Ozs7Ozs7O0FDaE1EO0lBSUksWUFBMkIsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUM3QyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFxQixDQUFDO1FBQ3pFLElBQUksQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBcUIsQ0FBQztJQUMzRixDQUFDO0lBRU0sWUFBWSxDQUFDLElBQVk7UUFDNUIsRUFBRSxFQUFDLElBQUksSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ25DLENBQUM7SUFDTCxDQUFDO0lBRU0sbUJBQW1CLENBQUMsSUFBWTtRQUNuQyxFQUFFLEVBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDMUMsQ0FBQztJQUNMLENBQUM7Q0FDSjtBQXBCRCwwQkFvQkM7Ozs7Ozs7Ozs7Ozs7OztBQ3BCRDtJQU1JLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDN0MsSUFBSSxDQUFDLG9CQUFvQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMseUJBQXlCLENBQW1CLENBQUM7UUFDakcsSUFBSSxDQUFDLHFCQUFxQixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsMEJBQTBCLENBQW1CLENBQUM7UUFDbkcsSUFBSSxDQUFDLHVCQUF1QixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsNEJBQTRCLENBQW1CLENBQUM7UUFDdkcsSUFBSSxDQUFDLHdCQUF3QixHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsNkJBQTZCLENBQW1CLENBQUM7SUFDN0csQ0FBQztJQUVNLGNBQWMsQ0FBQyxnQkFBd0I7UUFDMUMsTUFBTSxVQUFVLEdBQUcsQ0FBQyxnQkFBZ0IsR0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEdBQUMsR0FBRyxDQUFDO1FBRTNFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztRQUN0RCxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLFVBQVUsR0FBRyxDQUFDO1FBQ3pELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsVUFBVSxHQUFHLENBQUM7UUFDMUQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUNyRSxLQUFLLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUM7UUFDM0MsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUVsRSxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7UUFDdkQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxVQUFVLEdBQUcsQ0FBQztRQUMxRCxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxHQUFHLFVBQVUsR0FBRyxDQUFDO1FBQzNELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDdEUsS0FBSyxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxDQUFDO1FBQzVDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFFbkUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO1FBQ3pELElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsVUFBVSxHQUFHLENBQUM7UUFDNUQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsR0FBRyxVQUFVLEdBQUcsQ0FBQztRQUM3RCxJQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3hFLEtBQUssSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsQ0FBQztRQUM5QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBRXJFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztRQUMxRCxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxHQUFHLFVBQVUsR0FBRyxDQUFDO1FBQzdELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEdBQUcsVUFBVSxHQUFHLENBQUM7UUFDOUQsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUN6RSxLQUFLLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxXQUFXLENBQUM7UUFDL0MsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztJQUMxRSxDQUFDO0NBQ0o7QUE1Q0QsMERBNENDOzs7Ozs7Ozs7Ozs7Ozs7QUM3Q0QseUdBQXlEO0FBQ3pELGlGQUFpQztBQVFqQztJQUdJLFlBQTJCLFVBQXNCO1FBQXRCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDN0MsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUVPLHdCQUF3QjtRQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksU0FBUyxDQUFDLFFBQVEsUUFBUSxDQUFDLFFBQVEscUJBQXFCLENBQUMsQ0FBQztRQUU1RSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsVUFBVSxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxVQUFVLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3BJLENBQUMsQ0FBQztRQUVGLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUU7WUFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsQ0FBQyxDQUFDO0lBQ04sQ0FBQztJQUVNLHVCQUF1QjtRQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRU0sMEJBQTBCO1FBQzdCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFTSxrQkFBa0IsQ0FBQyxDQUFTLEVBQUUsQ0FBUztRQUMxQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVNLGlCQUFpQixDQUFDLENBQVMsRUFBRSxDQUFTO1FBQ3pDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVNLGVBQWUsQ0FBQyxRQUFnQixFQUFFLFNBQWlCLEVBQUUsU0FBaUI7UUFDekUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsYUFBYSxRQUFRLGNBQWMsU0FBUyxjQUFjLFNBQVMsRUFBRSxDQUFDLENBQUM7SUFDNUYsQ0FBQztJQUVNLGdCQUFnQixDQUFDLFFBQWdCO1FBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRU0sZUFBZSxDQUFDLFFBQWdCO1FBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRU0saUJBQWlCLENBQUMsUUFBZ0IsRUFBRSxJQUFZO1FBQ25ELElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsUUFBUSxTQUFTLElBQUksRUFBRSxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVNLG1CQUFtQixDQUFDLFFBQWdCLEVBQUUsSUFBWTtRQUNyRCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLFFBQVEsU0FBUyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFFTSxjQUFjO1FBQ2pCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFTSxjQUFjLENBQUMsR0FBVztRQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVELGtEQUFrRDtJQUMxQyxTQUFTLENBQUMsR0FBVztRQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUMsQ0FBQztRQUNoQyxFQUFFLEVBQUMsR0FBRyxJQUFJLGdCQUFnQixDQUFDLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUNoQyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNuQyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2hDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0IsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLEdBQUcsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVCLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLEdBQUcsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQzVCLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDOUIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxJQUFJLG1CQUFtQixDQUFDLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDckMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDcEMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxJQUFJLG9CQUFvQixDQUFDLENBQUMsQ0FBQztZQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDdEMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxJQUFJLG1CQUFtQixDQUFDLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDckMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxJQUFJLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM5QixDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLElBQUksY0FBYyxDQUFDLENBQUMsQ0FBQztZQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxFQUFDLEdBQUcsSUFBSSxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDcEMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxJQUFJLGdCQUFnQixDQUFDLENBQUMsQ0FBQztZQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDbEMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsc0JBQXNCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDckMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxJQUFJLHFCQUFxQixDQUFDLENBQUMsQ0FBQztZQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDdkMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNwQyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLElBQUksaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNsQyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxHQUFHLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDaEMsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLENBQUM7SUFDTCxDQUFDO0lBRU8sZ0JBQWdCO1FBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRU8sZ0JBQWdCO1FBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRU8sa0JBQWtCO1FBQ3RCLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRU8saUJBQWlCLENBQUMsR0FBVztRQUNqQyxJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN0RixFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNOLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDL0IsSUFBSSxLQUFLLEdBQUcsSUFBSSxLQUFLLEVBQXdDLENBQUM7WUFDOUQsSUFBSSxnQkFBZ0IsR0FBb0MsRUFBRSxDQUFDO1lBQzNELElBQUksV0FBVyxHQUFtQyxFQUFFLENBQUM7WUFFckQsZ0RBQWdEO1lBQ2hELEdBQUcsRUFBQyxNQUFNLElBQUksSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixFQUFFLEVBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDcEMsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO29CQUM5QyxFQUFFLEVBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNwQixNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3RELEVBQUUsRUFBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzs0QkFDeEIsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUM7d0JBQ3ZDLENBQUM7b0JBQ0wsQ0FBQztnQkFDTCxDQUFDO2dCQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3pDLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQzVDLEVBQUUsRUFBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3BCLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEQsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7NEJBQ04sS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDckIsQ0FBQztvQkFDTCxDQUFDO2dCQUNMLENBQUM7Z0JBQUMsSUFBSSxDQUFDLEVBQUUsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM5QyxNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUM7b0JBQ2pELEVBQUUsRUFBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3BCLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDcEQsQ0FBQztnQkFDTCxDQUFDO2dCQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZDLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQzFDLEVBQUUsRUFBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3BCLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDakQsRUFBRSxFQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDOzRCQUNuQixXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQzdCLENBQUM7b0JBQ0wsQ0FBQztnQkFDTCxDQUFDO1lBQ0wsQ0FBQztZQUVELElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxHQUFHLGdCQUFnQixDQUFDO1lBQ2pELElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztZQUN4QyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3JDLENBQUM7SUFDTCxDQUFDO0lBRUQsK0JBQStCO0lBQ3ZCLHdCQUF3QixDQUFDLEdBQVc7UUFDeEMsTUFBTSxJQUFJLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QixHQUFHLEVBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUM7WUFDOUMsTUFBTSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDBCQUEwQjtJQUNsQixvQkFBb0IsQ0FBQyxHQUFXO1FBQ3BDLE1BQU0sSUFBSSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDNUIsSUFBSSxTQUFTLEdBQUcsSUFBSSxLQUFLLEVBQVUsQ0FBQztRQUNwQyxHQUFHLEVBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUM7WUFDOUMsTUFBTSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUMzQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM1QixDQUFDO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVELHNDQUFzQztJQUM5QixrQkFBa0IsQ0FBQyxHQUFXO1FBQ2xDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDNUQsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDTixFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1lBQzVDLENBQUM7WUFDRCxFQUFFLEVBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsTUFBTSxNQUFNLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLEVBQUUsRUFBQyxNQUFNLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDaEQsQ0FBQztZQUNMLENBQUM7WUFDRCxFQUFFLEVBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDMUIsTUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzFDLE1BQU0sY0FBYyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNsRCxFQUFFLEVBQUMsVUFBVSxJQUFJLElBQUksSUFBSSxjQUFjLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDOUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUUsY0FBYyxDQUFDLENBQUM7Z0JBQ2pFLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCxnREFBZ0Q7SUFDeEMsMEJBQTBCLENBQUMsSUFBWTtRQUMzQyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN2RCxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNOLEVBQUUsRUFBQyxJQUFJLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDdkMsTUFBTSxDQUFDLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQy9CLE1BQU0sQ0FBQyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUMvQixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQztnQkFDckMsTUFBTSxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsT0FBTyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDO1lBQ3BFLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQscURBQXFEO0lBQzdDLGlCQUFpQixDQUFDLElBQVk7UUFDbEMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDdkQsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDTixFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3RELE1BQU0sQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDNUQsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxrREFBa0Q7SUFDMUMsOEJBQThCLENBQUMsSUFBWTtRQUMvQyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN2RCxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNOLEVBQUUsRUFBQyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBRSxJQUFJLElBQUksSUFBSSxDQUFDLGFBQWEsSUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dCQUMxRixNQUFNLFNBQVMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDL0MsTUFBTSxTQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQy9DLE1BQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUMzQyxNQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDakQsTUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ2pELE1BQU0sY0FBYyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUN6RCxJQUFJLGFBQWEsR0FBRyxJQUFJLEtBQUssRUFBVSxDQUFDO2dCQUN4QyxJQUFJLGFBQWEsR0FBRyxJQUFJLEtBQUssRUFBVSxDQUFDO2dCQUN4QyxJQUFJLFdBQVcsR0FBRyxJQUFJLEtBQUssRUFBVSxDQUFDO2dCQUN0QyxJQUFJLGNBQWMsR0FBRyxJQUFJLEtBQUssRUFBVSxDQUFDO2dCQUN6QyxJQUFJLGNBQWMsR0FBRyxJQUFJLEtBQUssRUFBVSxDQUFDO2dCQUN6QyxJQUFJLGtCQUFrQixHQUFHLElBQUksS0FBSyxFQUFVLENBQUM7Z0JBQzdDLDhDQUE4QztnQkFDOUMsR0FBRyxFQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDNUMsRUFBRSxFQUFDLElBQUksQ0FBQzt3QkFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6QyxDQUFDO2dCQUNELDhDQUE4QztnQkFDOUMsR0FBRyxFQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDNUMsRUFBRSxFQUFDLElBQUksQ0FBQzt3QkFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN6QyxDQUFDO2dCQUNELDRDQUE0QztnQkFDNUMsR0FBRyxFQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDMUMsRUFBRSxFQUFDLElBQUksQ0FBQzt3QkFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUN2QyxDQUFDO2dCQUNELGdEQUFnRDtnQkFDaEQsR0FBRyxFQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDN0MsRUFBRSxFQUFDLElBQUksQ0FBQzt3QkFBSSxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMxQyxDQUFDO2dCQUNELGdEQUFnRDtnQkFDaEQsR0FBRyxFQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDN0MsRUFBRSxFQUFDLElBQUksQ0FBQzt3QkFBSSxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMxQyxDQUFDO2dCQUNELHlEQUF5RDtnQkFDekQsR0FBRyxFQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNqRCxFQUFFLEVBQUMsSUFBSSxDQUFDO3dCQUFJLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDOUMsQ0FBQztnQkFDRCx1REFBdUQ7Z0JBQ3ZELElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEdBQUc7b0JBQ2hDLFNBQVMsRUFBRSxTQUFTO29CQUNwQixTQUFTLEVBQUUsU0FBUztvQkFDcEIsT0FBTyxFQUFFLE9BQU87b0JBQ2hCLFVBQVUsRUFBRSxVQUFVO29CQUN0QixVQUFVLEVBQUUsVUFBVTtvQkFDdEIsY0FBYyxFQUFFLGNBQWM7b0JBQzlCLGFBQWEsRUFBRSxhQUFhO29CQUM1QixhQUFhLEVBQUUsYUFBYTtvQkFDNUIsV0FBVyxFQUFFLFdBQVc7b0JBQ3hCLGNBQWMsRUFBRSxjQUFjO29CQUM5QixjQUFjLEVBQUUsY0FBYztvQkFDOUIsa0JBQWtCLEVBQUUsa0JBQWtCO2lCQUN6QyxDQUFDO1lBQ04sQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQsMENBQTBDO0lBQ2xDLHFCQUFxQixDQUFDLElBQVk7UUFDdEMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDdkQsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDTixFQUFFLEVBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDdEcsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztnQkFDekIsTUFBTSxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3ZDLE1BQU0sSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNyQyxNQUFNLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDdkMsTUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3JDLE1BQU0sSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNyQyxNQUFNLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDckMsTUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3JDLGlEQUFpRDtnQkFDakQsTUFBTSxDQUFDO29CQUNILEVBQUUsRUFBRSxRQUFRO29CQUNaLEVBQUUsRUFBRTt3QkFDQSxLQUFLLEVBQUUsS0FBSzt3QkFDWixJQUFJLEVBQUUsSUFBSTt3QkFDVixLQUFLLEVBQUUsS0FBSzt3QkFDWixJQUFJLEVBQUUsSUFBSTt3QkFDVixJQUFJLEVBQUUsSUFBSTt3QkFDVixJQUFJLEVBQUUsSUFBSTt3QkFDVixJQUFJLEVBQUUsSUFBSTtxQkFDRztpQkFDcEIsQ0FBQztZQUNOLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsdUNBQXVDO0lBQy9CLG9CQUFvQjtRQUN4Qiw2QkFBZSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUNuRCxhQUFhO1lBQ2IsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDM0MsRUFBRSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO1lBQ3BDLENBQUM7UUFDTCxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUNYLFdBQVc7WUFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN0QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxvQ0FBb0M7SUFDNUIsb0JBQW9CLENBQUMsR0FBVztRQUNwQyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDOUQsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDTiw4REFBOEQ7WUFDOUQsRUFBRSxFQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUNwRSxNQUFNLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDN0MsTUFBTSxNQUFNLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLE1BQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUNqRCxNQUFNLFVBQVUsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDakQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDOUUsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQscUNBQXFDO0lBQzdCLHFCQUFxQixDQUFDLEdBQVc7UUFDckMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQy9ELEVBQUUsRUFBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ04sMERBQTBEO1lBQzFELEVBQUUsRUFBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixNQUFNLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDN0MsTUFBTSxNQUFNLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ3ZELENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELG9DQUFvQztJQUM1QixvQkFBb0IsQ0FBQyxHQUFXO1FBQ3BDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUM5RCxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNOLDBEQUEwRDtZQUMxRCxFQUFFLEVBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDOUIsTUFBTSxRQUFRLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzdDLE1BQU0sTUFBTSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDdEQsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQsc0NBQXNDO0lBQzlCLHNCQUFzQixDQUFDLEdBQVc7UUFDdEMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO1FBQ2hFLEVBQUUsRUFBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ04sRUFBRSxFQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQzlCLE1BQU0sUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUM3QyxNQUFNLE1BQU0sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDeEQsQ0FBQztRQUNMLENBQUM7SUFDTCxDQUFDO0lBRUQscUVBQXFFO0lBQzdELHVCQUF1QixDQUFDLEdBQVc7UUFDdkMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxlQUFlLENBQUMsQ0FBQztRQUM1RCxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNOLHFEQUFxRDtZQUNyRCxFQUFFLEVBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pGLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELDhEQUE4RDtJQUN0RCxzQkFBc0IsQ0FBQyxHQUFXO1FBQ3RDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDekQsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDTixzRUFBc0U7WUFDdEUsRUFBRSxFQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3RGLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELGdGQUFnRjtJQUN4RSxnQkFBZ0IsQ0FBQyxHQUFXO1FBQ2hDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFFbkQsNENBQTRDO1FBQzVDLEVBQUUsRUFBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNYLGdDQUFnQztZQUNoQyxzQkFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDNUIsYUFBYTtnQkFDYixNQUFNLENBQUMsR0FBRyxJQUFJLFNBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztnQkFDekQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO2dCQUN4QixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ2xDLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNYLFdBQVc7Z0JBQ1gsS0FBSyxDQUFDLGtDQUFrQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM3RCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7SUFDTCxDQUFDO0lBRUQsc0VBQXNFO0lBQzlELGdCQUFnQixDQUFDLEdBQVc7UUFDaEMsTUFBTSxJQUFJLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDNUUsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDTixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9CLElBQUksS0FBSyxHQUFHLElBQUksS0FBSyxFQUFRLENBQUM7WUFFOUIsZ0RBQWdEO1lBQ2hELEdBQUcsRUFBQyxNQUFNLElBQUksSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxJQUFJLENBQUM7Z0JBQ1QsRUFBRSxFQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO29CQUN4QixJQUFJLEdBQUc7d0JBQ0gsSUFBSSxFQUFFLE9BQU8sQ0FBQyxJQUFJO3dCQUNsQixLQUFLLEVBQUUsT0FBTyxDQUFDLEtBQUs7d0JBQ3BCLFNBQVMsRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDO3dCQUMzQyxVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQzt3QkFDNUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQzt3QkFDdkQsU0FBUyxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQzt3QkFDbEQsUUFBUSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQzt3QkFDM0MsUUFBUSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztxQkFDdkMsQ0FBQztnQkFDYixDQUFDO2dCQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7b0JBQzVCLElBQUksR0FBRzt3QkFDSCxJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUk7d0JBQ2xCLEtBQUssRUFBRSxPQUFPLENBQUMsS0FBSzt3QkFDcEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0JBQzNDLFVBQVUsRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDO3dCQUM1QyxXQUFXLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO3dCQUNqRCxTQUFTLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO3FCQUNsQyxDQUFDO2dCQUNwQixDQUFDO2dCQUFDLElBQUksQ0FBQyxFQUFFLEVBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7b0JBQzNCLElBQUksR0FBRzt3QkFDSCxJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUk7d0JBQ2xCLEtBQUssRUFBRSxPQUFPLENBQUMsS0FBSzt3QkFDcEIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUM7d0JBQzNDLFVBQVUsRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDO3dCQUM1QyxVQUFVLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQztxQkFDekMsQ0FBQztnQkFDakIsQ0FBQztnQkFDRCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JCLENBQUM7WUFFRCxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlGQUFpRjtJQUN6RSxpQkFBaUIsQ0FBQyxHQUFXO1FBQ2pDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDdEQsRUFBRSxFQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDTixFQUFFLEVBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ1QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzFDLENBQUM7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVELGlEQUFpRDtJQUN6QyxpQkFBaUIsQ0FBQyxHQUFXO1FBQ2pDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDMUQsRUFBRSxFQUFDLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNqQixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUNoRCxDQUFDO0lBQ0wsQ0FBQztJQUVELGtEQUFrRDtJQUMxQyxnQkFBZ0IsQ0FBQyxHQUFXO1FBQ2hDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDdkQsRUFBRSxFQUFDLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JELENBQUM7SUFDTCxDQUFDO0lBRU8sbUJBQW1CLENBQUMsR0FBVyxFQUFFLEtBQWEsRUFBRSxNQUFZLEdBQUc7UUFDbkUsTUFBTSxJQUFJLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7UUFDckUsSUFBSSxHQUFHLEdBQThCLEVBQUUsQ0FBQztRQUV4QyxFQUFFLEVBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNOLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFL0IsZ0RBQWdEO1lBQ2hELEdBQUcsRUFBQyxNQUFNLElBQUksSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM5QixFQUFFLEVBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNsQixNQUFNLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3JCLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDdkIsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQztnQkFDckIsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO1FBRUQsTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUNmLENBQUM7Q0FFSjtBQXRpQkQsd0JBc2lCQzs7Ozs7Ozs7Ozs7Ozs7O0FDaGpCRCx3QkFBd0IsR0FBRyxFQUFFLE9BQU8sRUFBRSxLQUFLO0lBQ3ZDLElBQUksT0FBTyxHQUFHLElBQUksY0FBYyxFQUFFLENBQUM7SUFDbkMsT0FBTyxDQUFDLGtCQUFrQixHQUFHO1FBQ3pCLEVBQUUsRUFBQyxPQUFPLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekIsRUFBRSxFQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksR0FBRyxJQUFJLE9BQU8sQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDL0MsRUFBRSxFQUFDLE9BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUNoQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3JCLENBQUM7WUFDTCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osRUFBRSxFQUFDLE9BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUM5QixLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ25CLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCx3QkFBd0I7SUFDeEIsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUcsd0JBQXdCO0lBQzFELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDdkIsQ0FBQztBQUVELGtCQUF5QixPQUFlLEVBQUUsT0FBc0MsRUFBRSxLQUFvQztJQUNsSCxjQUFjLENBQUMsU0FBUyxPQUFPLE1BQU0sRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFFO1FBQy9DLGFBQWE7UUFDYixFQUFFLEVBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztZQUNULE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyQixDQUFDO0lBQ0wsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUU7UUFDWCxXQUFXO1FBQ1gsRUFBRSxFQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDUCxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbkIsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQVpELDRCQVlDO0FBRUQseUJBQWdDLFNBQWlCLEVBQUUsT0FBc0MsRUFBRSxLQUFvQztJQUMzSCxjQUFjLENBQUMsdUJBQXVCLFNBQVMsRUFBRSxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUU7UUFDM0QsYUFBYTtRQUNiLEVBQUUsRUFBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ1QsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JCLENBQUM7SUFDTCxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtRQUNYLFdBQVc7UUFDWCxFQUFFLEVBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNQLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuQixDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDO0FBWkQsMENBWUMiLCJmaWxlIjoic3RhdGljL2dhbWVfdHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9jbGllbnRfc3JjL01haW4udHNcIik7XG4iLCJpbXBvcnQgeyBHVUkgfSBmcm9tIFwiLi9ndWkvR1VJXCJcclxuaW1wb3J0IHsgQ2xpZW50IH0gZnJvbSBcIi4vbmV0d29ya2luZy9DbGllbnRcIlxyXG5pbXBvcnQgeyBQbGF5ZXJTdHViIH0gZnJvbSBcIi4vZ2FtZS9QbGF5ZXJTdHViXCJcclxuaW1wb3J0IHsgUGxheWVyRGV0YWlscyB9IGZyb20gXCIuL2dhbWUvUGxheWVyRGV0YWlsc1wiXHJcbmltcG9ydCB7IFBsYXllck1vZGlmaWVycyB9IGZyb20gXCIuL2dhbWUvUGxheWVyTW9kaWZpZXJzXCJcclxuaW1wb3J0IHsgUGxheWVySGVhbHRoIH0gZnJvbSBcIi4vZ2FtZS9QbGF5ZXJIZWFsdGhcIlxyXG5pbXBvcnQgeyBNYXAgfSBmcm9tIFwiLi9nYW1lL01hcFwiXHJcbmltcG9ydCB7IEhpdE1hcCB9IGZyb20gXCIuL2dhbWUvSGl0TWFwXCJcclxuaW1wb3J0IHsgR2FtZVBoYXNlIH0gZnJvbSBcIi4vZ2FtZS9HYW1lUGhhc2VcIlxyXG5pbXBvcnQgeyBJdGVtLCBJdGVtRW50aXR5IH0gZnJvbSBcIi4vZ2FtZS9JdGVtXCJcclxuaW1wb3J0IHsgSW52ZW50b3J5IH0gZnJvbSBcIi4vZ2FtZS9JbnZlbnRvcnlcIlxyXG5pbXBvcnQgeyBJdGVtTWVtZW50byB9IGZyb20gXCIuL2dhbWUvSXRlbU1lbWVudG9cIlxyXG5pbXBvcnQgeyBBY3Rpb24sIEFjdGlvbkFwQ29zdHMsIEFjdGlvbkVxdWlwV2VhcG9uIH0gZnJvbSBcIi4vZ2FtZS9BY3Rpb25cIlxyXG5cclxuZXhwb3J0IGNsYXNzIENvbnRyb2xsZXIge1xyXG4gICAgLy8gVXNlcyB0b3VjaHN0YXJ0IG9uIG1vYmlsZSBkZXZpY2VzIGFuZCBjbGljayBvbiBkZXNrdG9wXHJcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IENMSUNLX0VWRU5UOiBzdHJpbmcgPSBcIm9udG91Y2hlbmRcIiBpbiB3aW5kb3cgPyBcInRvdWNoZW5kXCIgOiBcImNsaWNrXCI7XHJcblxyXG4gICAgLy8gUmVmZXJlbmNlIHRvIHRoZSBHVUlcclxuICAgIHByaXZhdGUgX2d1aTogR1VJO1xyXG5cclxuICAgIC8vIE5ldHdvcmsgV2ViU29ja2V0IGNsaWVudFxyXG4gICAgcHJpdmF0ZSBfY2xpZW50OiBDbGllbnQ7XHJcblxyXG4gICAgLy8gR2xvYmFsIG1vdXNleCBhbmQgbW91c2V5IGZyb20gdGhlIGxhc3QgbW91c2UgZXZlbnRcclxuICAgIHByaXZhdGUgX2dsb2JhbE1vdXNlWDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBfZ2xvYmFsTW91c2VZOiBudW1iZXI7XHJcblxyXG4gICAgLy8gTWFwIG9iamVjdCBsb2FkZWQgZnJvbSB0aGUgc2VydmVyXHJcbiAgICBwcml2YXRlIF9tYXA6IE1hcDtcclxuXHJcbiAgICAvLyBIaXRNYXAgb2JqZWN0LCBwcm92aWRlIG1ldGhvZCBvZiBtb3VzZSAtPiBtYXAgaGl0IGRldGVjdGlvblxyXG4gICAgcHJpdmF0ZSBfaGl0TWFwOiBIaXRNYXA7XHJcblxyXG4gICAgLy8gTGlzdCBvZiBhdmFpbGFibGUgaXRlbXNcclxuICAgIHByaXZhdGUgX2l0ZW1zOiBBcnJheTxJdGVtPlxyXG5cclxuICAgIC8vIElEIG9mIGN1cnJlbnQgVGltZW91dCB1c2VkIGJ5IHRoaXMgb2JqZWN0XHJcbiAgICBwcml2YXRlIF90aW1lb3V0SUQ6IG51bWJlcjtcclxuXHJcbiAgICAvLyBUcnVlIGlmZiB0aGUgR1VJIGlzIGFuaW1hdGluZyBjaGFuZ2VzXHJcbiAgICBwcml2YXRlIF9hbmltYXRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICAvLyBMaXN0IG9mIGl0ZW0gbWVtZW50b3MsIGVhY2ggZGlzc2FwZWFycyBhZnRlciA1IHR1cm5zXHJcbiAgICBwcml2YXRlIF9pdGVtTWVtZW50b3M6IEFycmF5PEl0ZW1NZW1lbnRvPjtcclxuXHJcbiAgICAvLyBTdG9ybSB2YXJpYWJsZXNcclxuICAgIHByaXZhdGUgX3N0b3JtUmFkaXVzOiBudW1iZXIgPSAtMTtcclxuICAgIHByaXZhdGUgX3N0b3JtRG1nUGVyVHVybjogbnVtYmVyID0gMDtcclxuICAgIHByaXZhdGUgX3N0b3JtU2lja0RtZ1BlclR1cm46IG51bWJlciA9IDA7XHJcblxyXG4gICAgLy8gR2FtZSBkZXRhaWxzIG9idGFpbmVkIGZyb20gc2VydmVyXHJcbiAgICAvLyBUaGUgY3VycmVudCBwaGFzZSBvZiB0aGUgZ2FtZSAoZGVmYXVsdHMgdG8gTE9CQlkgaS5lLiBub3Qgc3RhcnRlZClcclxuICAgIHByaXZhdGUgX2dhbWVQaGFzZTogR2FtZVBoYXNlID0gR2FtZVBoYXNlLkxPQkJZO1xyXG4gICAgLy8gVGltZSB0YWtlbiBmb3IgZWFjaCBwaGFzZSAoaW4gc2Vjb25kcylcclxuICAgIHByaXZhdGUgX2Ryb3BQaGFzZVRpbWU6IG51bWJlciA9IDEwO1xyXG4gICAgcHJpdmF0ZSBfbW92ZVBoYXNlVGltZTogbnVtYmVyID0gMTA7XHJcbiAgICBwcml2YXRlIF9hY3Rpb25QaGFzZVRpbWU6IG51bWJlciA9IDMwO1xyXG4gICAgLy8gTWFwIGZyb20gcGxheWVyIElEIHRvIHN0dWIgYW5kIGRldGFpbHMgb2JqZWN0c1xyXG4gICAgcHJpdmF0ZSBfcGxheWVyU3R1YnM6IHsgW2lkOiBudW1iZXJdOiBQbGF5ZXJTdHViIH07XHJcbiAgICBwcml2YXRlIF9wbGF5ZXJEZXRhaWxzOiB7IFtpZDogbnVtYmVyXTogUGxheWVyRGV0YWlscyB9O1xyXG4gICAgLy8gSGVhbHRoIHZhbHVlcyBmb3IgYWxsIHZpc2libGUgcGxheWVyc1xyXG4gICAgcHJpdmF0ZSBfcGxheWVySFBzOiB7IFtpZDogbnVtYmVyXTogUGxheWVySGVhbHRoIH07XHJcbiAgICAvLyBNb2RpZmllciB2YWx1ZXMgZm9yIG15IHBsYXllclxyXG4gICAgcHJpdmF0ZSBfbXlQbGF5ZXJNb2RpZmllcnM6IFBsYXllck1vZGlmaWVycztcclxuICAgIC8vIEFQIHZhbHVlIGZvciBteSBwbGF5ZXIgKGFzIGEgcGVyY2VudGFnZSlcclxuICAgIHByaXZhdGUgX215UGxheWVyQVA6IG51bWJlciA9IDEwMDtcclxuICAgIC8vIExpc3Qgb2YgYWN0aW9ucyB0byBiZSBwZXJmb3JtZWQgYnkgbXkgcGxheWVyIGF0IGVuZCBvZiBhY3Rpb24gcGhhc2VcclxuICAgIHByaXZhdGUgX215UGxheWVyQWN0aW9uczogQXJyYXk8QWN0aW9uPjtcclxuICAgIC8vIFRoZSBpbnZlbnRvcnkgb2YgbXkgcGxheWVyXHJcbiAgICBwcml2YXRlIF9teVBsYXllckludmVudG9yeTogSW52ZW50b3J5O1xyXG4gICAgLy8gVGhlIGl0ZW0gY3VycmVudGx5IGVxdWlwcGVkIGJ5IG15IHBsYXllclxyXG4gICAgcHJpdmF0ZSBfbXlQbGF5ZXJFcXVpcHBlZEl0ZW06IEl0ZW1FbnRpdHk7XHJcbiAgICAvLyBUaGUgaXRlbSBlZmZlY3RpdmVseSBlcXVpcHBlZCBmb3IgZnV0dXJlIGFjdGlvbnNcclxuICAgIHByaXZhdGUgX215UGxheWVyRWZmZWN0aXZlbHlFcXVpcHBlZEl0ZW06IEl0ZW1FbnRpdHk7XHJcbiAgICAvLyBUaGUgcGxheWVyIElEIG9mIHRoZSB0aGUgcGxheWVyIHVzaW5nIHRoaXMgY2xpZW50XHJcbiAgICBwcml2YXRlIF9teUlEOiBudW1iZXIgPSAtMTtcclxuICAgIC8vIElmIHRydWUsIF9teUlEIGdpdmVzIHRoZSBJRCBvZiBzcGVjdGF0ZWQgcGxheWVyIGFuZCBubyBhY3Rpb25zIGNhbiBiZSBtYWRlXHJcbiAgICBwcml2YXRlIF9zcGVjdGF0aW5nOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuX2d1aSA9IEdVSS5uZXcodGhpcyk7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50ID0gbmV3IENsaWVudCh0aGlzKTtcclxuICAgICAgICB0aGlzLl9wbGF5ZXJTdHVicyA9IHt9O1xyXG4gICAgICAgIHRoaXMuX3BsYXllckRldGFpbHMgPSB7fTtcclxuICAgICAgICB0aGlzLl9wbGF5ZXJIUHMgPSB7fTtcclxuICAgICAgICB0aGlzLl9teVBsYXllck1vZGlmaWVycyA9IHtcclxuICAgICAgICAgICAgdmlld1JhbmdlOiAwLFxyXG4gICAgICAgICAgICBoaXRDaGFuY2U6IDAsXHJcbiAgICAgICAgICAgIHRvdGFsQVA6IDAsXHJcbiAgICAgICAgICAgIGFwTW92ZUNvc3Q6IDAsXHJcbiAgICAgICAgICAgIGRtZ1Blck1vdmU6IDAsXHJcbiAgICAgICAgICAgIHNpY2tEbWdQZXJUdXJuOiAwLFxyXG4gICAgICAgICAgICB2aWV3UmFuZ2VEZXNjOiBudWxsLFxyXG4gICAgICAgICAgICBoaXRDaGFuY2VEZXNjOiBudWxsLFxyXG4gICAgICAgICAgICB0b3RhbEFQRGVzYzogbnVsbCxcclxuICAgICAgICAgICAgYXBNb3ZlQ29zdERlc2M6IG51bGwsXHJcbiAgICAgICAgICAgIGRtZ1Blck1vdmVEZXNjOiBudWxsLFxyXG4gICAgICAgICAgICBzaWNrRG1nUGVyVHVybkRlc2M6IG51bGxcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuX2hpdE1hcCA9IG5ldyBIaXRNYXAoKTtcclxuICAgICAgICB0aGlzLl9pdGVtcyA9IG5ldyBBcnJheTxJdGVtPigpO1xyXG4gICAgICAgIHRoaXMuX2l0ZW1NZW1lbnRvcyA9IG5ldyBBcnJheTxJdGVtTWVtZW50bz4oKTtcclxuICAgICAgICB0aGlzLl9teVBsYXllckFjdGlvbnMgPSBuZXcgQXJyYXk8QWN0aW9uPigpO1xyXG4gICAgICAgIHRoaXMuX215UGxheWVySW52ZW50b3J5ID0gbmV3IEludmVudG9yeSh0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBTdGFydCBhIHRpbWVyXHJcbiAgICBwdWJsaWMgc3RhcnRUaW1lcih0aW1lOiBudW1iZXIsIGludGVydmFsQ2FsbGJhY2s6ICgodGltZUxlZnQ6IG51bWJlciwgdGltZW91dElEOiBudW1iZXIpID0+IHZvaWQpLFxyXG4gICAgICAgICAgICBlbmRlZENhbGxiYWNrOiAoKCkgPT4gdm9pZCksIHNldFRpbWVvdXRJRDogYm9vbGVhbj1mYWxzZSwgaW50ZXJ2YWw6IG51bWJlcj0yMDApIHtcclxuICAgICAgICBjb25zdCBzdGFydFRpbWUgPSBEYXRlLm5vdygpO1xyXG4gICAgICAgIGNvbnN0ICR0aGlzID0gdGhpcztcclxuXHJcbiAgICAgICAgZnVuY3Rpb24gdGltZXJMb29wKCkge1xyXG4gICAgICAgICAgICBjb25zdCB0aW1lRWxhcHNlZCA9IERhdGUubm93KCkgLSBzdGFydFRpbWU7XHJcbiAgICAgICAgICAgIGNvbnN0IHRpbWVMZWZ0ID0gdGltZSoxMDAwIC0gdGltZUVsYXBzZWQ7XHJcblxyXG4gICAgICAgICAgICBpZih0aW1lTGVmdCA+IDApIHtcclxuICAgICAgICAgICAgICAgIC8vIGNoZWNrIGFnYWluIGluIDIwMCBtc1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaWQgPSBzZXRUaW1lb3V0KHRpbWVyTG9vcCwgaW50ZXJ2YWwpO1xyXG4gICAgICAgICAgICAgICAgaWYoc2V0VGltZW91dElEKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHRoaXMuX3RpbWVvdXRJRCA9IGlkO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmKGludGVydmFsQ2FsbGJhY2spIHtcclxuICAgICAgICAgICAgICAgICAgICBpbnRlcnZhbENhbGxiYWNrKHRpbWVMZWZ0LCBpZCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZihlbmRlZENhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBSZWFjaGVkIGVuZCBvZiB0aW1lLCB0aGVyZWZvcmUsIGNhbGwgY2FsbGJhY2sgZnVuY3Rpb25cclxuICAgICAgICAgICAgICAgIGVuZGVkQ2FsbGJhY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gc3RhcnQgdGhlIGZpcnN0IGV4ZWN1dGlvbiBvZiB0aW1lciBsb29wIGltbWVkaWF0ZWx5XHJcbiAgICAgICAgdGltZXJMb29wKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU2VuZCBhIG1lc3NhZ2UgdG8gdGhlIG90aGVyIHBsYXllcnMgaW4gdGhlIGxvYmJ5XHJcbiAgICBwdWJsaWMgc2VuZE1zZyhtc2c6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX2NsaWVudC5zZW5kU2VuZE1zZ01zZyhtc2cpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRvZ2dsZSB0aGUgcmVhZHkgc3RhdGUgb2YgdGhlIHBsYXllclxyXG4gICAgLy8gU2VuZHMgcmVhZHkgc3RhdGUgdG9nZ2xlIGNvbW1hbmQgdG8gc2VydmVyXHJcbiAgICBwdWJsaWMgdG9nZ2xlUmVhZHlTdGF0ZSgpIHtcclxuICAgICAgICB0aGlzLl9jbGllbnQuc2VuZFRvZ2dsZVJlYWR5U3RhdGVNc2coKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBTZXQgdGhlIHJlYWR5IHN0YXRlIG9mIHBsYXllciB0byB0cnVlXHJcbiAgICBwdWJsaWMgc2V0UmVhZHlTdGF0ZVRvVHJ1ZSgpIHtcclxuICAgICAgICB0aGlzLl9jbGllbnQuc2VuZFNldFJlYWR5U3RhdGVUb1RydWVNc2coKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBOb3RpZnkgdGhlIHNlcnZlciBvZiB0aGUgcGxheWVyJ3MgbW92ZVxyXG4gICAgcHVibGljIHNldE1vdmVUdXJuKHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50LnNlbmRTZXRNb3ZlVHVybk1zZyh4LCB5KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBOb3RpZnkgdGhlIHNlcnZlciB0aGF0IHRoZSBwbGF5ZXIgd2lzaGVzIHRvIHNlYXJjaCBhIHRpbGVcclxuICAgIHB1YmxpYyBzZWFyY2hUaWxlKHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50LnNlbmRTZWFyY2hUaWxlTXNnKHgsIHkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE5vdGlmeSB0aGUgc2VydmVyIHRoYXQgdGhlIHBsYXllciBoYXMgdGFrZW4gYW4gaXRlbVxyXG4gICAgcHVibGljIHRha2VJdGVtKGVudGl0eUlEOiBudW1iZXIsIGJhY2twYWNrWDogbnVtYmVyLCBiYWNrcGFja1k6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2NsaWVudC5zZW5kVGFrZUl0ZW1Nc2coZW50aXR5SUQsIGJhY2twYWNrWCwgYmFja3BhY2tZKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBOb3RpZnkgdGhlIHNlcnZlciB0aGF0IHRoZSBwbGF5ZXIgd2lzaGVzIHRvIGVxdWlwIGFuIGl0ZW0gZnJvbSB0aGVpciBiYWNrcGFja1xyXG4gICAgcHVibGljIGVxdWlwSXRlbShlbnRpdHlJRDogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50LnNlbmRFcXVpcEl0ZW1Nc2coZW50aXR5SUQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE5vdGlmeSB0aGUgc2VydmVyIHRoYXQgdGhlIHBsYXllciB3aXNoZXMgdG8gZHJvcCBhbiBpdGVtIGZyb20gdGhlaXIgYmFja3BhY2tcclxuICAgIHB1YmxpYyBkcm9wSXRlbShlbnRpdHlJRDogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fY2xpZW50LnNlbmREcm9wSXRlbU1zZyhlbnRpdHlJRCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gTm90aWZ5IHRoZSBzZXJ2ZXIgdGhhdCB0aGUgcGxheWVyIHdpc2hlcyB0byB1c2UgYSBiYW5kYWdlIGZyb20gdGhlaXIgYmFja3BhY2tcclxuICAgIHB1YmxpYyB1c2VCYW5kYWdlKGVudGl0eUlEOiBudW1iZXIsIGxpbWI6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX2NsaWVudC5zZW5kVXNlQmFuZGFnZU1zZyhlbnRpdHlJRCwgbGltYik7XHJcbiAgICAgICAgdGhpcy5fZ3VpLnJlcXVlc3RDbG9zZUZ1bGxPdmVybGF5KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gTm90aWZ5IHRoZSBzZXJ2ZXIgdGhhdCB0aGUgcGxheWVyIHdpc2hzZXMgdG8gYXR0YWNrIGFub3RoZXIgcGxheWVyXHJcbiAgICBwdWJsaWMgYXR0YWNrUGxheWVyKHBsYXllcklEOiBzdHJpbmcsIGxpbWI6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX2NsaWVudC5zZW5kQXR0YWNrUGxheWVyTXNnKHBsYXllcklELCBsaW1iKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBOb3RpZnkgdGhlIHNlcnZlciB0aGF0IHRoZSBwbGF5ZXIgaGFzIGZpbmlzaGVkIHRoZWlyIHR1cm5cclxuICAgIHB1YmxpYyBlbmRUdXJuKCkge1xyXG4gICAgICAgIHRoaXMuX2NsaWVudC5zZW5kRW5kVHVybk1zZygpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJlcXVlc3QgdGhlIG1hcCB0byBiZSByZWRyYXduXHJcbiAgICBwdWJsaWMgcmVxdWVzdERyYXdNYXAoKSB7XHJcbiAgICAgICAgaWYodGhpcy5fZ3VpICYmICF0aGlzLl9hbmltYXRpbmcpIHtcclxuICAgICAgICAgICAgdGhpcy5fZ3VpLnJlcXVlc3REcmF3TWFwKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIFJlcXVlc3QgdGhhdCB0aGUgbW92ZW1lbnQgb2YgcGxheWVycyBiZSBhbmltYXRlZCBvbiB0aGUgbWFwXHJcbiAgICBwdWJsaWMgcmVxdWVzdEFuaW1hdGVNb3ZlbWVudChvbGRQbGF5ZXJEZXRhaWxzOiB7IFtpZDogbnVtYmVyXTogUGxheWVyRGV0YWlscyB9LCBjYWxsYmFjazogKCgpID0+IHZvaWQpKSB7XHJcbiAgICAgICAgaWYoIXRoaXMuX2FuaW1hdGluZykge1xyXG4gICAgICAgICAgICB0aGlzLl9hbmltYXRpbmcgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLl9ndWkucmVxdWVzdEFuaW1hdGVNb3ZlbWVudChvbGRQbGF5ZXJEZXRhaWxzLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9hbmltYXRpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNhbGxiYWNrKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBSZXR1cm5zIHRydWUgaWZmIHRoZXJlIGlzIGEgcGxheWVyIG9uIHRoZSBzcGVjaWZpZWQgdGlsZVxyXG4gICAgLy8gSU1QT1JUQU5UIC0gRXhjbHVkZXMgbXkgcGxheWVyXHJcbiAgICBwdWJsaWMgaXNQbGF5ZXJPblRpbGUoeDogbnVtYmVyLCB5OiBudW1iZXIpIHtcclxuICAgICAgICBmb3IobGV0IHBsYXllcklEIGluIHRoaXMuX3BsYXllckRldGFpbHMpIHtcclxuICAgICAgICAgICAgY29uc3QgcGxheWVyID0gdGhpcy5fcGxheWVyRGV0YWlsc1twbGF5ZXJJRF07XHJcbiAgICAgICAgICAgIGlmKHBsYXllci54ID09IHggJiYgcGxheWVyLnkgPT0geSAmJiBwbGF5ZXIgIT0gdGhpcy5teVBsYXllckRldGFpbHMpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBSZXR1cm5zIGxpc3Qgb2YgcGxheWVyIGlkcyBvbiB0aWxlXHJcbiAgICAvLyBJTVBPUlRBTlQgLSBFeGNsdWRlcyBteSBwbGF5ZXJcclxuICAgIHB1YmxpYyBnZXRQbGF5ZXJzT25UaWxlKHg6IG51bWJlciwgeTogbnVtYmVyKTogQXJyYXk8c3RyaW5nPiB7XHJcbiAgICAgICAgbGV0IGxzID0gbmV3IEFycmF5PHN0cmluZz4oKTtcclxuICAgICAgICBmb3IobGV0IHBsYXllcklEIGluIHRoaXMuX3BsYXllckRldGFpbHMpIHtcclxuICAgICAgICAgICAgY29uc3QgcGxheWVyID0gdGhpcy5fcGxheWVyRGV0YWlsc1twbGF5ZXJJRF07XHJcbiAgICAgICAgICAgIGlmKHBsYXllci54ID09IHggJiYgcGxheWVyLnkgPT0geSAmJiBwbGF5ZXIgIT0gdGhpcy5teVBsYXllckRldGFpbHMpIHtcclxuICAgICAgICAgICAgICAgIGxzLnB1c2gocGxheWVySUQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBscztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbXlQbGF5ZXJTdHViKCk6IFBsYXllclN0dWIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wbGF5ZXJTdHVic1t0aGlzLl9teUlEXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbXlQbGF5ZXJEZXRhaWxzKCk6IFBsYXllckRldGFpbHMge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wbGF5ZXJEZXRhaWxzW3RoaXMuX215SURdO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBteVBsYXllck1vZGlmaWVycygpOiBQbGF5ZXJNb2RpZmllcnMge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9teVBsYXllck1vZGlmaWVycztcclxuICAgIH1cclxuXHJcbiAgICBzZXQgbXlQbGF5ZXJNb2RpZmllcnMocDogUGxheWVyTW9kaWZpZXJzKSB7XHJcbiAgICAgICAgaWYocCkge1xyXG4gICAgICAgICAgICB0aGlzLl9teVBsYXllck1vZGlmaWVycyA9IHA7XHJcbiAgICAgICAgICAgIHRoaXMuX2d1aS5vbk15UGxheWVyTW9kaWZpZXJzVXBkYXRlZCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXQgbXlQbGF5ZXJIUCgpOiBQbGF5ZXJIZWFsdGgge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wbGF5ZXJIUHNbdGhpcy5fbXlJRF07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHBsYXllckhQcygpOiB7IFtpZDogbnVtYmVyXTogUGxheWVySGVhbHRoIH0ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wbGF5ZXJIUHM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IHBsYXllckhQcyhocDogeyBbaWQ6IG51bWJlcl06IFBsYXllckhlYWx0aCB9KSB7XHJcbiAgICAgICAgaWYoaHApIHtcclxuICAgICAgICAgICAgaWYoaHBbdGhpcy5fbXlJRF0pIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRvdGFsSFBDaGFuZ2VkQnkgPSB0aGlzLm15UGxheWVySFAgPyBocFt0aGlzLl9teUlEXS50b3RhbCAtIHRoaXMubXlQbGF5ZXJIUC50b3RhbCA6IDA7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBoZWFkSW5qdXJlZCA9IHRoaXMubXlQbGF5ZXJIUCA/IHRoaXMubXlQbGF5ZXJIUC5oZWFkID4gaHBbdGhpcy5fbXlJRF0uaGVhZCA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY2hlc3RJbmp1cmVkID0gdGhpcy5teVBsYXllckhQID8gdGhpcy5teVBsYXllckhQLmNoZXN0ID4gaHBbdGhpcy5fbXlJRF0uY2hlc3QgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJhcm1Jbmp1cmVkID0gdGhpcy5teVBsYXllckhQID8gdGhpcy5teVBsYXllckhQLnJhcm0gPiBocFt0aGlzLl9teUlEXS5yYXJtIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBsYXJtSW5qdXJlZCA9IHRoaXMubXlQbGF5ZXJIUCA/IHRoaXMubXlQbGF5ZXJIUC5sYXJtID4gaHBbdGhpcy5fbXlJRF0ubGFybSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcmxlZ0luanVyZWQgPSB0aGlzLm15UGxheWVySFAgPyB0aGlzLm15UGxheWVySFAucmxlZyA+IGhwW3RoaXMuX215SURdLnJsZWcgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGxsZWdJbmp1cmVkID0gdGhpcy5teVBsYXllckhQID8gdGhpcy5teVBsYXllckhQLmxsZWcgPiBocFt0aGlzLl9teUlEXS5sbGVnIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9wbGF5ZXJIUHMgPSBocDtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2d1aS5vbk15UGxheWVySFBVcGRhdGVkKHRvdGFsSFBDaGFuZ2VkQnksIGhlYWRJbmp1cmVkLCBjaGVzdEluanVyZWQsIHJhcm1Jbmp1cmVkLCBsYXJtSW5qdXJlZCwgcmxlZ0luanVyZWQsIGxsZWdJbmp1cmVkKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3BsYXllckhQcyA9IGhwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBteVBsYXllckFQKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX215UGxheWVyQVA7XHJcbiAgICB9XHJcblxyXG4gICAgLypzZXQgbXlQbGF5ZXJBUChhcDogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fbXlQbGF5ZXJBUCA9IGFwO1xyXG4gICAgfSovXHJcblxyXG4gICAgZ2V0IG15UGxheWVySW52ZW50b3J5KCk6IEludmVudG9yeSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX215UGxheWVySW52ZW50b3J5O1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBteVBsYXllckVxdWlwcGVkSXRlbSgpOiBJdGVtRW50aXR5IHtcclxuICAgICAgICAvL2NvbnNvbGUubG9nKFwiRXF1aXBwZWRcIiwgdGhpcy5fbXlQbGF5ZXJFZmZlY3RpdmVseUVxdWlwcGVkSXRlbSwgdGhpcy5fbXlQbGF5ZXJFcXVpcHBlZEl0ZW0pXHJcbiAgICAgICAgaWYodGhpcy5fbXlQbGF5ZXJFZmZlY3RpdmVseUVxdWlwcGVkSXRlbSAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9teVBsYXllckVmZmVjdGl2ZWx5RXF1aXBwZWRJdGVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fbXlQbGF5ZXJFcXVpcHBlZEl0ZW07XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IG15UGxheWVyRXF1aXBwZWRJdGVtKGVudGl0eTogSXRlbUVudGl0eSkge1xyXG4gICAgICAgIGlmKGVudGl0eSkge1xyXG4gICAgICAgICAgICB0aGlzLl9teVBsYXllckVxdWlwcGVkSXRlbSA9IGVudGl0eTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHNwZWN0YXRpbmcoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3NwZWN0YXRpbmc7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGdhbWVQaGFzZSgpOiBHYW1lUGhhc2Uge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9nYW1lUGhhc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGdsb2JhbE1vdXNlWCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9nbG9iYWxNb3VzZVg7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGdsb2JhbE1vdXNlWSgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9nbG9iYWxNb3VzZVk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IGdsb2JhbE1vdXNlWCh4OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9nbG9iYWxNb3VzZVggPSB4O1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBnbG9iYWxNb3VzZVkoeTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fZ2xvYmFsTW91c2VZID0geTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBHZXQgYSByZWZlcmVuY2UgdG8gdGhlIGxvYWRlZCBtYXBcclxuICAgIGdldCBtYXAoKTogTWFwIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbWFwO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFNldCB0aGUgbWFwIGJlaW5nIHBsYXllZFxyXG4gICAgc2V0IG1hcChtOiBNYXApIHtcclxuICAgICAgICBpZihtKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX21hcCA9IG07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBoaXRNYXAoKTogSGl0TWFwIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faGl0TWFwO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEdldCBhIHJlZmVyZW5jZSB0byB0aGUgbGlzdCBvZiBhbGwgYXZhaWxhYmxlIGl0ZW1zXHJcbiAgICBnZXQgaXRlbXMoKTogQXJyYXk8SXRlbT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pdGVtcztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgc3Rvcm1SYWRpdXMoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3Rvcm1SYWRpdXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHN0b3JtRG1nUGVyVHVybigpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zdG9ybURtZ1BlclR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHN0b3JtU2lja0RtZ1BlclR1cm4oKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3Rvcm1TaWNrRG1nUGVyVHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBHZXQgYWxsIHBsYXllciBzdHVic1xyXG4gICAgZ2V0IHBsYXllclN0dWJzKCk6IHsgW2lkOiBudW1iZXJdOiBQbGF5ZXJTdHViIH0ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wbGF5ZXJTdHVicztcclxuICAgIH1cclxuXHJcbiAgICAvLyBHZXQgYWxsIHBsYXllciBkZXRhaWxzXHJcbiAgICBnZXQgcGxheWVyRGV0YWlscygpOiB7IFtpZDogbnVtYmVyXTogUGxheWVyRGV0YWlscyB9IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcGxheWVyRGV0YWlscztcclxuICAgIH1cclxuXHJcbiAgICAvLyBTZXQgYWxsIHRoZSBwbGF5ZXIgc3R1YnNcclxuICAgIHNldCBwbGF5ZXJTdHVicyhzdHViczogeyBbaWQ6IG51bWJlcl06IFBsYXllclN0dWIgfSkge1xyXG4gICAgICAgIGlmKHN0dWJzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3BsYXllclN0dWJzID0gc3R1YnM7XHJcbiAgICAgICAgICAgIHRoaXMuX2d1aS5vblBsYXllclN0dWJzVXBkYXRlZCgpOyAgIC8vIG5vdGlmeSBHVUkgb2YgcGxheWVyIHN0dWJzIHVwZGF0ZVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBTZXQgYWxsIHRoZSBwbGF5ZXIgZGV0YWlsc1xyXG4gICAgc2V0IHBsYXllckRldGFpbHMoZGV0YWlsczogeyBbaWQ6IG51bWJlcl06IFBsYXllckRldGFpbHMgfSkge1xyXG4gICAgICAgIGlmKGRldGFpbHMpIHtcclxuICAgICAgICAgICAgLy8gS2VlcCB0cmFjayBvZiBwcmV2aW91cyBkZXRhaWxzIGFuZCBhbmltYXRlIGJldHdlZW4gdGhlbVxyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5fdGltZW91dElEKTtcclxuICAgICAgICAgICAgdGhpcy5fdGltZW91dElEID0gbnVsbDtcclxuICAgICAgICAgICAgY29uc3Qgb2xkRGV0YWlscyA9IHRoaXMuX3BsYXllckRldGFpbHM7XHJcbiAgICAgICAgICAgIHRoaXMuX3BsYXllckRldGFpbHMgPSBkZXRhaWxzO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJnb3QgZGV0YWlsc1wiLCB0aGlzLmdhbWVQaGFzZSlcclxuXHJcbiAgICAgICAgICAgIC8vIElmIGluIHRoZSBtb3ZlIHBoYXNlLCBhbmltYXRlIHRoZSBtb3ZlbWVudCBvZiBwbGF5ZXJzXHJcbiAgICAgICAgICAgIGlmKHRoaXMuZ2FtZVBoYXNlID09IEdhbWVQaGFzZS5NT1ZFX1BIQVNFKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImFuaW1hdGluZ1wiKTtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVxdWVzdEFuaW1hdGVNb3ZlbWVudChvbGREZXRhaWxzLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRSZWFkeVN0YXRlVG9UcnVlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fZ3VpLm9uV2FpdGluZ0ZvclNlcnZlcigpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZG9uZSBhbmltYXRpbmdcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXF1ZXN0RHJhd01hcCgpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFJlYWR5U3RhdGVUb1RydWUoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2d1aS5vbldhaXRpbmdGb3JTZXJ2ZXIoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYodGhpcy5fcGxheWVyRGV0YWlsc1t0aGlzLl9teUlEXSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vbk15UGxheWVyRGV0YWlsc1VwZGF0ZWQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBTZXQgYSBzaW5nbGUgcGxheWVyIHN0dWJcclxuICAgIHB1YmxpYyBzZXRQbGF5ZXJTdHViKGlkOiBzdHJpbmcsIHN0dWI6IFBsYXllclN0dWIpIHtcclxuICAgICAgICBpZihzdHViKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3BsYXllclN0dWJzW2lkXSA9IHN0dWI7XHJcbiAgICAgICAgICAgIHRoaXMuX2d1aS5vblBsYXllclN0dWJzVXBkYXRlZCgpOyAgIC8vIG5vdGlmeSBHVUkgb2YgcGxheWVyIHN0dWJzIHVwZGF0ZVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBTZXQgYSBzaW5nbGUgcGxheWVyJ3MgZGV0YWlsc1xyXG4gICAgcHVibGljIHNldFBsYXllckRldGFpbHMoaWQ6IHN0cmluZywgZGV0YWlsczogUGxheWVyRGV0YWlscykge1xyXG4gICAgICAgIGlmKGRldGFpbHMpIHtcclxuICAgICAgICAgICAgdGhpcy5fcGxheWVyRGV0YWlsc1tpZF0gPSBkZXRhaWxzO1xyXG4gICAgICAgICAgICBpZihpZCA9PSBcIlwiK3RoaXMuX215SUQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMub25NeVBsYXllckRldGFpbHNVcGRhdGVkKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW5ldmVyIG15UGxheWVyRGV0YWlscyBhcmUgdXBkYXRlZCBieSB0aGUgc2VydmVyXHJcbiAgICBwcml2YXRlIG9uTXlQbGF5ZXJEZXRhaWxzVXBkYXRlZCgpIHtcclxuICAgICAgICAvLyBOb3RpZnkgR1VJIG9mIG15IHBsYXllciBkZXRhaWxzIHVwZGF0ZVxyXG4gICAgICAgIHRoaXMuX2d1aS5vbk15UGxheWVyRGV0YWlsc1VwZGF0ZWQoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBHZXQgYSByZWZlcmVuY2UgdG8gdGhlIGxpc3Qgb2YgaXRlbSBtZW1lbnRvc1xyXG4gICAgZ2V0IGl0ZW1NZW1lbnRvcygpOiBBcnJheTxJdGVtTWVtZW50bz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pdGVtTWVtZW50b3M7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQWRkIGEgbmV3IGl0ZW0gbWVtZW50byB0byB0aGUgbGlzdCBvZiBpdGVtIG1lbWVudG9zXHJcbiAgICBwdWJsaWMgYWRkSXRlbU1lbWVudG8oaTogSXRlbU1lbWVudG8pIHtcclxuICAgICAgICBpZihpKSB7XHJcbiAgICAgICAgICAgIGxldCBleGlzdHMgPSBmYWxzZTtcclxuICAgICAgICAgICAgZm9yKGNvbnN0IG0gb2YgdGhpcy5faXRlbU1lbWVudG9zKSB7XHJcbiAgICAgICAgICAgICAgICBpZihtLm1hcFggPT09IGkubWFwWCAmJiBtLm1hcFkgPT09IGkubWFwWSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIFJlc2V0IHRoaXMgbWVtZW50byByYXRoZXIgdGhhbiBhZGRpbmcgYSBuZXcgb25lXHJcbiAgICAgICAgICAgICAgICAgICAgbS5pdGVtcyA9IGkuaXRlbXM7XHJcbiAgICAgICAgICAgICAgICAgICAgbS5yZXNldCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGV4aXN0cyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gT25seSBhZGQgbmV3IG1lbWVudG8gaWYgdGhlcmUgaXMgbm8gbW9tZW50byBvbiBzYW1lIHRpbGVcclxuICAgICAgICAgICAgaWYoIWV4aXN0cykge1xyXG4gICAgICAgICAgICAgICAgaS5jb2xvcktleSA9IHRoaXMuX2hpdE1hcC5hZGRJdGVtTWVtZW50byhpKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2l0ZW1NZW1lbnRvcy5wdXNoKGkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIFNldCB0aGUgY3VycmVudCBpdGVtIGZvdW5kXHJcbiAgICAvLyBUT0RPIC0gYWRkIHN1cHBvcnQgZm9yIG11bHRpcGxlIGl0ZW1zIGZvdW5kIG9uIG9uZSB0aWxlXHJcbiAgICBwdWJsaWMgc2V0SXRlbXNGb3VuZChpdGVtczogQXJyYXk8eyBlbnRpdHlJRDogc3RyaW5nLCBpdGVtSUQ6IHN0cmluZyB9Pikge1xyXG4gICAgICAgIC8vIEFkZCBhIG5ldyBtZW1lbnRvIGZvciB0aGUgY3VycmVudCB0aWxlXHJcbiAgICAgICAgbGV0IG0gPSBuZXcgSXRlbU1lbWVudG8odGhpcy5teVBsYXllckRldGFpbHMueCwgdGhpcy5teVBsYXllckRldGFpbHMueSk7XHJcbiAgICAgICAgZm9yKGNvbnN0IG9iaiBvZiBpdGVtcykge1xyXG4gICAgICAgICAgICBjb25zdCBpdGVtSUQgPSBwYXJzZUludChvYmouaXRlbUlELCAxMCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGl0ZW0gPSBpdGVtSUQgPj0gMCAmJiBpdGVtSUQgPCB0aGlzLl9pdGVtcy5sZW5ndGhcclxuICAgICAgICAgICAgICAgICAgICAgICAgPyB0aGlzLl9pdGVtc1tpdGVtSURdIDogbnVsbDtcclxuICAgICAgICAgICAgaWYoaXRlbSkge1xyXG4gICAgICAgICAgICAgICAgbS5pdGVtcy5wdXNoKGl0ZW0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuYWRkSXRlbU1lbWVudG8obSk7XHJcbiAgICAgICAgdGhpcy5fZ3VpLnNldEl0ZW1zRm91bmQoaXRlbXMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEdldCB0aGUgdGltZSBnaXZlbiBmb3IgdGhlIGRyb3AgcGhhc2UgKGluIHNlY29uZHMpXHJcbiAgICBnZXQgZHJvcFBoYXNlVGltZSgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kcm9wUGhhc2VUaW1lO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEdldCB0aGUgdGltZSBnaXZlbiBmb3IgdGhlIG1vdmUgcGhhc2UgKGluIHNlY29uZHMpXHJcbiAgICBnZXQgbW92ZVBoYXNlVGltZSgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9tb3ZlUGhhc2VUaW1lO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEdldCB0aGUgdGltZSBnaXZlbiBmb3IgdGhlIGFjdGlvbiBwaGFzZSAoaW4gc2Vjb25kcylcclxuICAgIGdldCBhY3Rpb25QaGFzZVRpbWUoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYWN0aW9uUGhhc2VUaW1lO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBzZXJ2ZXIgaW5pdGlhdGVzIHRoZSBkcm9wIHBoYXNlXHJcbiAgICBwdWJsaWMgb25Ecm9wUGhhc2VTdGFydCgpIHtcclxuICAgICAgICB0aGlzLl9nYW1lUGhhc2UgPSBHYW1lUGhhc2UuRFJPUF9QSEFTRTtcclxuICAgICAgICB0aGlzLl9ndWkub25HYW1lU3RhcnRlZCgpO1xyXG4gICAgICAgIHRoaXMuX2d1aS5vbkRyb3BQaGFzZVN0YXJ0KCk7XHJcbiAgICAgICAgdGhpcy5yZXF1ZXN0RHJhd01hcCgpO1xyXG4gICAgICAgIHRoaXMuc3RhcnRUaW1lcih0aGlzLl9kcm9wUGhhc2VUaW1lLCAodGltZUxlZnQpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fZ3VpLnVwZGF0ZVRpbWVyTGFiZWwoXCJEcm9wIFBoYXNlXCIsIE1hdGguZmxvb3IodGltZUxlZnQvMTAwMCkpO1xyXG4gICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgaWYodGhpcy5fZ2FtZVBoYXNlID09PSBHYW1lUGhhc2UuRFJPUF9QSEFTRSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZ2FtZVBoYXNlID0gR2FtZVBoYXNlLldBSVRJTkdfRk9SX1NFUlZFUjtcclxuICAgICAgICAgICAgICAgIC8vIFNlbmQgcmVhZHkgbWVzc2FnZVxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRSZWFkeVN0YXRlVG9UcnVlKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9ndWkub25XYWl0aW5nRm9yU2VydmVyKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCB0cnVlKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgc2VydmVyIGluaXRpYXRlcyBhIG1vdmUgcGhhc2VcclxuICAgIHB1YmxpYyBvbk1vdmVQaGFzZVN0YXJ0KCkge1xyXG4gICAgICAgIC8vIEZ1bGwgb3ZlcmxheSBzaG91bGQgb25seSBldmVyIGJlIG9wZW4gZHVyaW5nIGFjdGlvbiBwaGFzZVxyXG4gICAgICAgIHRoaXMuX2d1aS5yZXF1ZXN0Q2xvc2VGdWxsT3ZlcmxheSgpO1xyXG5cclxuICAgICAgICAvLyBEZXRlcm1pbmUgdGhlIGN1cnJlbnQgZWZmZWN0aXZlbHkgZXF1aXBwZWQgaXRlbVxyXG4gICAgICAgIHRoaXMuX215UGxheWVyRXF1aXBwZWRJdGVtID0gdGhpcy5fbXlQbGF5ZXJFZmZlY3RpdmVseUVxdWlwcGVkSXRlbSA/XHJcbiAgICAgICAgICAgIHRoaXMuX215UGxheWVyRWZmZWN0aXZlbHlFcXVpcHBlZEl0ZW0gOiB0aGlzLl9teVBsYXllckVxdWlwcGVkSXRlbTtcclxuICAgICAgICB0aGlzLl9teVBsYXllckVmZmVjdGl2ZWx5RXF1aXBwZWRJdGVtID0gbnVsbDtcclxuXHJcbiAgICAgICAgLy8gSW5jcmVtZW50IGFsbCBpdGVtIG1vbWVudG9zXHJcbiAgICAgICAgZm9yKGNvbnN0IG0gb2YgdGhpcy5faXRlbU1lbWVudG9zKSB7XHJcbiAgICAgICAgICAgIG0uaW5jU2luY2UoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFJlbW92ZSBhbnkgZXhwaXJlZCBpdGVtIG1lbWVudG9zXHJcbiAgICAgICAgdGhpcy5faXRlbU1lbWVudG9zID0gdGhpcy5faXRlbU1lbWVudG9zLmZpbHRlcigoZSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gZS5zaW5jZSA8IDY7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIC8vIFJlc2V0IHR1cm4gc3RhdHNcclxuICAgICAgICB0aGlzLl9teVBsYXllckFQID0gMTAwICtcclxuICAgICAgICAgICAgKHRoaXMubXlQbGF5ZXJNb2RpZmllcnMudG90YWxBUCAhPSBudWxsID8gdGhpcy5teVBsYXllck1vZGlmaWVycy50b3RhbEFQIDogMCk7XHJcblxyXG4gICAgICAgIHRoaXMuX2dhbWVQaGFzZSA9IEdhbWVQaGFzZS5NT1ZFX1BIQVNFO1xyXG4gICAgICAgIHRoaXMuX2d1aS5vbk1vdmVQaGFzZVN0YXJ0KCk7XHJcbiAgICAgICAgdGhpcy5zdGFydFRpbWVyKHRoaXMuX21vdmVQaGFzZVRpbWUsICh0aW1lTGVmdCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9ndWkudXBkYXRlVGltZXJMYWJlbChcIk1vdmUgUGhhc2VcIiwgTWF0aC5mbG9vcih0aW1lTGVmdC8xMDAwKSk7XHJcbiAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICBpZih0aGlzLl9nYW1lUGhhc2UgPT09IEdhbWVQaGFzZS5NT1ZFX1BIQVNFKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBTZW5kIHJlYWR5IG1lc3NhZ2VcclxuICAgICAgICAgICAgICAgIHRoaXMud2FpdEZvclBsYXllckRldGFpbHNVcGRhdGUoNSk7XHJcbiAgICAgICAgICAgIC8vICAgIHRoaXMuX2d1aS5vbldhaXRpbmdGb3JTZXJ2ZXIoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBzZXJ2ZXIgaW5pdGlhdGVzIGFuIGFjdGlvbiBwaGFzZVxyXG4gICAgcHVibGljIG9uQWN0aW9uUGhhc2VTdGFydCgpIHtcclxuICAgICAgICB0aGlzLl9teVBsYXllckFjdGlvbnMubGVuZ3RoID0gMDtcclxuICAgICAgICB0aGlzLl9nYW1lUGhhc2UgPSBHYW1lUGhhc2UuQUNUSU9OX1BIQVNFO1xyXG4gICAgICAgIHRoaXMuX2d1aS5vbkFjdGlvblBoYXNlU3RhcnQoKTtcclxuICAgICAgICB0aGlzLnN0YXJ0VGltZXIodGhpcy5hY3Rpb25QaGFzZVRpbWUsICh0aW1lTGVmdCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9ndWkudXBkYXRlVGltZXJMYWJlbChcIkFjdGlvbiBQaGFzZVwiLCBNYXRoLmZsb29yKHRpbWVMZWZ0LzEwMDApKTtcclxuICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmKHRoaXMuX2dhbWVQaGFzZSA9PT0gR2FtZVBoYXNlLkFDVElPTl9QSEFTRSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZ2FtZVBoYXNlID0gR2FtZVBoYXNlLldBSVRJTkdfRk9SX1NFUlZFUjtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBTZW5kIHJlYWR5IG1lc3NhZ2VcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0UmVhZHlTdGF0ZVRvVHJ1ZSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZ3VpLm9uV2FpdGluZ0ZvclNlcnZlcigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgdHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gV2FpdCBmb3IgdGltZW91dCBzZWNvbmRzIGZvciB0aGUgY2xpZW50IHRvIHJlY2VpdmUgYSBwbGF5ZXIgc3R1YnMgdXBkYXRlXHJcbiAgICAvLyBBbGxvd3MgY2xpZW50IHRvIGFuaW1hdGUgcGxheWVyIG1vdmVtZW50ICYgZ3VuZmlyZVxyXG4gICAgcHJpdmF0ZSB3YWl0Rm9yUGxheWVyRGV0YWlsc1VwZGF0ZSh0aW1lb3V0OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnN0YXJ0VGltZXIodGltZW91dCwgKHRpbWVMZWZ0LCB0aW1lb3V0SUQpID0+IHtcclxuICAgICAgICAgICAgLyppZighdGhpcy5fYW5pbWF0aW5nKSB7XHJcbiAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodGltZW91dElEKTtcclxuICAgICAgICAgICAgICAgIHRpbWVvdXRJRCA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRvZ2dsZVJlYWR5U3RhdGUoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2d1aS5vbldhaXRpbmdGb3JTZXJ2ZXIoKTtcclxuICAgICAgICAgICAgfSovXHJcbiAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9nYW1lUGhhc2UgPSBHYW1lUGhhc2UuV0FJVElOR19GT1JfU0VSVkVSO1xyXG4gICAgICAgICAgICB0aGlzLnJlcXVlc3REcmF3TWFwKCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0UmVhZHlTdGF0ZVRvVHJ1ZSgpO1xyXG4gICAgICAgICAgICB0aGlzLl9ndWkub25XYWl0aW5nRm9yU2VydmVyKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBoYXNlIHRpbWUgc2V0dGluZ3MgaGF2ZSBiZWVuIHJlY2VpdmVkIGZyb20gdGhlIHNlcnZlclxyXG4gICAgcHVibGljIG9uUGhhc2VUaW1lc1VwZGF0ZShkcm9wVGltZTogc3RyaW5nLCBtb3ZlVGltZTogc3RyaW5nLCBhY3Rpb25UaW1lOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLl9kcm9wUGhhc2VUaW1lID0gcGFyc2VJbnQoZHJvcFRpbWUsIDEwKSB8fCB0aGlzLl9kcm9wUGhhc2VUaW1lO1xyXG4gICAgICAgIHRoaXMuX21vdmVQaGFzZVRpbWUgPSBwYXJzZUludChtb3ZlVGltZSwgMTApIHx8IHRoaXMuX21vdmVQaGFzZVRpbWU7XHJcbiAgICAgICAgdGhpcy5fYWN0aW9uUGhhc2VUaW1lID0gcGFyc2VJbnQoYWN0aW9uVGltZSwgMTApIHx8IHRoaXMuX2FjdGlvblBoYXNlVGltZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiBhIG1vdmUgaGFzIGJlZW4gZGVlbWVkIHZhbGlkXHJcbiAgICBwdWJsaWMgb25Nb3ZlVmFsaWQobmV3WDogbnVtYmVyLCBuZXdZOiBudW1iZXIsIGNyb3VjaDogYm9vbGVhbikge1xyXG4gICAgICAgIGlmKHRoaXMuZ2FtZVBoYXNlID09IEdhbWVQaGFzZS5NT1ZFX1BIQVNFKSB7XHJcbiAgICAgICAgICAgIGlmKGNyb3VjaCkge1xyXG4gICAgICAgICAgICAgICAgLy8gTk9URSAtIFJlc2V0IEFQIHJhdGhlciB0aGFuIHN1YnRyYWN0IHNpbmNlIGNhbiBjbGlja2luZyByZXNldHMgbW92ZSB0dXJuXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9teVBsYXllckFQID0gMTAwICsgdGhpcy5fbXlQbGF5ZXJNb2RpZmllcnMudG90YWxBUCAtIEFjdGlvbkFwQ29zdHMuQ1JPVUNIO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gTk9URSAtIFJlc2V0IEFQIHJhdGhlciB0aGFuIHN1YnRyYWN0IHNpbmNlIGNhbiBjbGlja2luZyByZXNldHMgbW92ZSB0dXJuXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9teVBsYXllckFQID0gMTAwICsgdGhpcy5fbXlQbGF5ZXJNb2RpZmllcnMudG90YWxBUCAtIEFjdGlvbkFwQ29zdHMuTU9WRSAtXHJcbiAgICAgICAgICAgICAgICAgICAgKHRoaXMubXlQbGF5ZXJNb2RpZmllcnMuYXBNb3ZlQ29zdCAhPSBudWxsID8gdGhpcy5teVBsYXllck1vZGlmaWVycy5hcE1vdmVDb3N0IDogMCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5fZ3VpLm9uTW92ZVZhbGlkKG5ld1gsIG5ld1ksIGNyb3VjaCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGFuIGF0dGFjayBoYXMgYmVlbiBkZWVtZWQgdmFsaWRcclxuICAgIHB1YmxpYyBvbkF0dGFja1ZhbGlkKCkge1xyXG4gICAgICAgIGlmKHRoaXMuZ2FtZVBoYXNlID09IEdhbWVQaGFzZS5BQ1RJT05fUEhBU0UpIHtcclxuICAgICAgICAgICAgdGhpcy5fbXlQbGF5ZXJBUCAtPSBBY3Rpb25BcENvc3RzLkFUVEFDSztcclxuICAgICAgICAgICAgdGhpcy5fZ3VpLm9uQXR0YWNrVmFsaWQoeyBuYW1lOiBcIkF0dGFja1wiLCBhcENvc3Q6IEFjdGlvbkFwQ29zdHMuQVRUQUNLLCBwbGF5ZXJJRDogLTEgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGEgc2VhcmNoIGhhcyBiZWVuIGRlZW1lZCB2YWxpZFxyXG4gICAgcHVibGljIG9uU2VhcmNoVmFsaWQoKSB7XHJcbiAgICAgICAgaWYodGhpcy5nYW1lUGhhc2UgPT0gR2FtZVBoYXNlLkFDVElPTl9QSEFTRSkge1xyXG4gICAgICAgICAgICB0aGlzLl9teVBsYXllckFQIC09IEFjdGlvbkFwQ29zdHMuU0VBUkNIO1xyXG4gICAgICAgICAgICB0aGlzLl9ndWkub25TZWFyY2hWYWxpZCh7IG5hbWU6IFwiU2VhcmNoIFRpbGVcIiwgYXBDb3N0OiBBY3Rpb25BcENvc3RzLlNFQVJDSCB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHNlcnZlciBoYXMgdmFsaWRpZmllZCBhIHRha2UgaXRlbSByZXF1ZXN0XHJcbiAgICBwdWJsaWMgb25UYWtlSXRlbVZhbGlkKGVudGl0eUlEOiBudW1iZXIsIGl0ZW1JRDogbnVtYmVyLCBmaXJzdFNsb3RYOiBudW1iZXIsIGZpcnN0U2xvdFk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX215UGxheWVySW52ZW50b3J5LmFkZEl0ZW1FbnRpdHkobmV3IEl0ZW1FbnRpdHkoZW50aXR5SUQsIGl0ZW1JRCksIGZpcnN0U2xvdFgsIGZpcnN0U2xvdFkpO1xyXG4gICAgICAgIHRoaXMuX2d1aS5vblRha2VJdGVtVmFsaWQoZW50aXR5SUQsIGl0ZW1JRCwgZmlyc3RTbG90WCwgZmlyc3RTbG90WSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHNlcnZlciBoYXMgdmFsaWRpZmllZCBhbiBlcXVpcCBpdGVtIHJlcXVlc3RcclxuICAgIHB1YmxpYyBvbkVxdWlwSXRlbVZhbGlkKGVudGl0eUlEOiBudW1iZXIsIGl0ZW1JRDogbnVtYmVyKSB7XHJcbiAgICAgICAgaWYoaXRlbUlEIDwgMCB8fCBpdGVtSUQgPj0gdGhpcy5faXRlbXMubGVuZ3RoIHx8XHJcbiAgICAgICAgICAgIHRoaXMuX215UGxheWVyQWN0aW9ucy5sZW5ndGggPj0gNSB8fFxyXG4gICAgICAgICAgICB0aGlzLl9teVBsYXllckFQIC0gQWN0aW9uQXBDb3N0cy5FUVVJUF9XRUFQT04gKyB0aGlzLl9teVBsYXllck1vZGlmaWVycy50b3RhbEFQIDwgMCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGFjdGlvbiA9IHtcclxuICAgICAgICAgICAgbmFtZTogXCJFcXVpcCBcIit0aGlzLl9pdGVtc1tpdGVtSURdLm5hbWUsXHJcbiAgICAgICAgICAgIGFwQ29zdDogQWN0aW9uQXBDb3N0cy5FUVVJUF9XRUFQT04sXHJcbiAgICAgICAgICAgIGVudGl0eUlEOiBlbnRpdHlJRCxcclxuICAgICAgICAgICAgaXRlbUlEOiBpdGVtSURcclxuICAgICAgICB9IGFzIEFjdGlvbkVxdWlwV2VhcG9uO1xyXG4gICAgICAgIHRoaXMuX215UGxheWVyRWZmZWN0aXZlbHlFcXVpcHBlZEl0ZW0gPSBuZXcgSXRlbUVudGl0eShlbnRpdHlJRCwgaXRlbUlEKTtcclxuICAgICAgICB0aGlzLl9teVBsYXllckFQIC09IEFjdGlvbkFwQ29zdHMuRVFVSVBfV0VBUE9OO1xyXG4gICAgICAgIHRoaXMuX215UGxheWVyQWN0aW9ucy5wdXNoKGFjdGlvbik7XHJcbiAgICAgICAgdGhpcy5fZ3VpLm9uRXF1aXBJdGVtVmFsaWQoYWN0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgc2VydmVyIGhhcyB2YWxpZGlmZWQgYSBkcm9wIGl0ZW0gcmVxdWVzdFxyXG4gICAgcHVibGljIG9uRHJvcEl0ZW1WYWxpZChlbnRpdHlJRDogbnVtYmVyLCBpdGVtSUQ6IG51bWJlcikge1xyXG4gICAgICAgIGlmKGl0ZW1JRCA8IDAgfHwgaXRlbUlEID49IHRoaXMuX2l0ZW1zLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX2d1aS5vbkRyb3BJdGVtVmFsaWQoZW50aXR5SUQsIGl0ZW1JRCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHNlcnZlciBoYXMgdmFsaWRpZmllZCBhIHVzZSBiYW5kYWdlIHJlcXVlc3RcclxuICAgIHB1YmxpYyBvblVzZUJhbmRhZ2VWYWxpZChlbnRpdHlJRDogbnVtYmVyLCBpdGVtSUQ6IG51bWJlcikge1xyXG4gICAgICAgIGlmKGl0ZW1JRCA8IDAgfHwgaXRlbUlEID49IHRoaXMuX2l0ZW1zLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX2d1aS5vblVzZUJhbmRhZ2VWYWxpZChlbnRpdHlJRCwgaXRlbUlEKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIGluaXRpYXRlcyBhbiBhdHRhY2sgb24gYW5vdGhlciBwbGF5ZXJcclxuICAgIHB1YmxpYyBvbkluaXRpYXRlQXR0YWNrUGxheWVyKHBsYXllcklEOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLl9ndWkub25Jbml0aWF0ZUF0dGFja1BsYXllcihwbGF5ZXJJRCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBsYXllciB3YW50cyB0byBhdHRhY2sgYSBwbGF5ZXIgb24gYSB0aWxlXHJcbiAgICBwdWJsaWMgb25BdHRhY2tQbGF5ZXJPblRpbGUoeDogbnVtYmVyLCB5OiBudW1iZXIsIHRpbGVYOiBudW1iZXIsIHRpbGVZOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9ndWkub25BdHRhY2tQbGF5ZXJPblRpbGUoeCwgeSwgdGlsZVgsIHRpbGVZKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgYXQgZW5kIG9mIHR1cm4gaWYgcGxheWVycyBoYXZlIGRpZWRcclxuICAgIHB1YmxpYyBvblBsYXllckRlYWQocGxheWVySUQ6IG51bWJlcikge1xyXG4gICAgICAgIGNvbnN0IHBsYXllck5hbWUgPSB0aGlzLl9wbGF5ZXJTdHVic1twbGF5ZXJJRF0gPyB0aGlzLl9wbGF5ZXJTdHVic1twbGF5ZXJJRF0uTmFtZSA6IFwiUGxheWVyXCIrcGxheWVySUQ7XHJcbiAgICAgICAgdGhpcy5fZ3VpLm9uUGxheWVyRGVhZChwbGF5ZXJOYW1lKTtcclxuICAgICAgICAvLyBJZiBteSBwbGF5ZXIgZGllZCwgZGlzcGxheSBZb3UgRGllZCBtZXNzYWdlXHJcbiAgICAgICAgaWYocGxheWVySUQgPT0gdGhpcy5fbXlJRCkge1xyXG4gICAgICAgICAgICB0aGlzLl9ndWkub25NeVBsYXllckRlYWQoKTtcclxuICAgICAgICAgICAgLy8gU3BlY3RhdGUgdGhlIHBsYXllciB3aG8ga2lsbGVkIHlvdVxyXG4gICAgICAgIC8vICAgIHRoaXMuX215SUQgPSBwbGF5ZXJJRDtcclxuICAgICAgICAgICAgdGhpcy5fc3BlY3RhdGluZyA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCBhdCB0aGUgZW5kIG9mIHRoZSBnYW1lXHJcbiAgICBwdWJsaWMgb25HYW1lT3Zlcih3aW5uZXJJRHM6IEFycmF5PG51bWJlcj4pIHtcclxuICAgICAgICBpZih3aW5uZXJJRHMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgIGlmKHdpbm5lcklEc1swXSA9PT0gdGhpcy5fbXlJRCkge1xyXG4gICAgICAgICAgICAgICAgLy8gTXkgcGxheWVyIGlzIHRoZSBzb2xlIHdpbm5lclxyXG4gICAgICAgICAgICAgICAgdGhpcy5fZ3VpLm9uR2FtZU92ZXIod2lubmVySURzLCB0cnVlLCBmYWxzZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBNeSBwbGF5ZXIgbG9zdFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fZ3VpLm9uR2FtZU92ZXIod2lubmVySURzLCBmYWxzZSwgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGV0IGRpZE15UGxheWVyRHJhdyA9IGZhbHNlO1xyXG4gICAgICAgICAgICBmb3IoY29uc3Qgd2lubmVySUQgb2Ygd2lubmVySURzKSB7XHJcbiAgICAgICAgICAgICAgICBpZih3aW5uZXJJRCA9PT0gdGhpcy5fbXlJRCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpZE15UGxheWVyRHJhdyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYoZGlkTXlQbGF5ZXJEcmF3KSB7XHJcbiAgICAgICAgICAgICAgICAvLyBNeSBwbGF5ZXIgdGllZCB3aXRoIG90aGVyIHBsYXllcnNcclxuICAgICAgICAgICAgICAgIHRoaXMuX2d1aS5vbkdhbWVPdmVyKHdpbm5lcklEcywgdHJ1ZSwgdHJ1ZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBNeSBwbGF5ZXIgbG9zdFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fZ3VpLm9uR2FtZU92ZXIod2lubmVySURzLCBmYWxzZSwgZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGEgc3Rvcm0gYXBwcm9hY2ggdXBkYXRlIGlzIHJlY2VpdmVkIGZyb20gc2VydmVyXHJcbiAgICBwdWJsaWMgb25TdG9ybUFwcHJvYWNoVXBkYXRlKCkge1xyXG4gICAgICAgIHRoaXMuX2d1aS5vblN0b3JtQXBwcm9hY2hVcGRhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiBhIHN0b3JtIHJhZGl1cyB1cGRhdGUgaXMgcmVjZWl2ZWQgZnJvbSBzZXJ2ZXJcclxuICAgIHB1YmxpYyBvblN0b3JtUmFkaXVzVXBkYXRlKHJhZGl1czogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fc3Rvcm1SYWRpdXMgPSByYWRpdXM7XHJcbiAgICAgICAgdGhpcy5fZ3VpLm9uU3Rvcm1SYWRpdXNVcGRhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiBhIHN0b3JtIGRtZyB1cGRhdGUgaXMgcmVjZWl2ZWQgZnJvbSBzZXJ2ZXJcclxuICAgIHB1YmxpYyBvblN0b3JtRG1nVXBkYXRlKGRtZ1BlclR1cm46IG51bWJlciwgc2lja0RtZ1BlclR1cm46IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX3N0b3JtRG1nUGVyVHVybiA9IGRtZ1BlclR1cm47XHJcbiAgICAgICAgdGhpcy5fc3Rvcm1TaWNrRG1nUGVyVHVybiA9IHNpY2tEbWdQZXJUdXJuO1xyXG4gICAgICAgIHRoaXMuX2d1aS5vblN0b3JtRG1nVXBkYXRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIG1hcCBpcyBmaW5pc2hlZCBsb2FkaW5nIGFuZCBiZWluZyBpbml0aWFsaXNlZFxyXG4gICAgcHVibGljIG9uTWFwTG9hZGVkKCkge1xyXG4gICAgICAgIHRoaXMuX2d1aS5vbk1hcExvYWRlZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBpdGVtIGxpc3QgaXMgcmVjZWl2ZWQgZnJvbSB0aGUgc2VydmVyXHJcbiAgICBwdWJsaWMgb25JdGVtc0xvYWRlZChpdGVtczogQXJyYXk8SXRlbT4pIHtcclxuICAgICAgICBpZihpdGVtcykge1xyXG4gICAgICAgICAgICB0aGlzLl9pdGVtcyA9IGl0ZW1zO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiBhIHBsYXllcnMgcmVhZHkgc3RhdGUgaXMgY2hhbmdlZFxyXG4gICAgcHVibGljIG9uUmVhZHlTdGF0ZVVwZGF0ZShwbGF5ZXJJRDogc3RyaW5nLCByZWFkeVN0YXRlOiBib29sZWFuKSB7XHJcbiAgICAgICAgaWYodGhpcy5fcGxheWVyU3R1YnNbcGxheWVySURdKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3BsYXllclN0dWJzW3BsYXllcklEXS5SZWFkeSA9IHJlYWR5U3RhdGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX2d1aS5vblJlYWR5U3RhdGVVcGRhdGUocGxheWVySUQsIHJlYWR5U3RhdGUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoaXMgcGxheWVyJ3MgSUQgaXMgcmVjZWl2ZWQgZnJvbSB0aGUgc2VydmVyXHJcbiAgICBwdWJsaWMgb25NeUlEVXBkYXRlKG15SUQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX215SUQgPSAhaXNOYU4ocGFyc2VJbnQobXlJRCwgMTApKSA/IHBhcnNlSW50KG15SUQsIDEwKSA6IHRoaXMuX215SUQ7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gYSBwbGF5ZXIgZGlzY29ubmVjdHMgZnJvbSB0aGUgZ2FtZVxyXG4gICAgcHVibGljIG9uUGxheWVyRGlzY29ubmVjdChwbGF5ZXJJRDogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgcGxheWVyTmFtZSA9IHRoaXMucGxheWVyU3R1YnNbcGxheWVySURdID9cclxuICAgICAgICAgICAgICAgIHRoaXMucGxheWVyU3R1YnNbcGxheWVySURdLk5hbWUgOiBgUGxheWVyICR7cGxheWVySUR9YDtcclxuICAgICAgICBpZih0aGlzLl9wbGF5ZXJTdHVic1twbGF5ZXJJRF0pIHtcclxuICAgICAgICAgICAgZGVsZXRlIHRoaXMuX3BsYXllclN0dWJzW3BsYXllcklEXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYodGhpcy5fcGxheWVyRGV0YWlsc1twbGF5ZXJJRF0pIHtcclxuICAgICAgICAgICAgZGVsZXRlIHRoaXMuX3BsYXllckRldGFpbHNbcGxheWVySURdO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9ndWkub25QbGF5ZXJEaXNjb25uZWN0KHBsYXllck5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB1cG9uIHJlY2VpdmluZyBhIG1zZyBmcm9tIGFub3RoZXIgcGxheWVyXHJcbiAgICBwdWJsaWMgb25Nc2dSZWNlaXZlZChwbGF5ZXJJRDogc3RyaW5nLCBtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IHBsYXllck5hbWUgPSB0aGlzLnBsYXllclN0dWJzW3BsYXllcklEXSA/XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBsYXllclN0dWJzW3BsYXllcklEXS5OYW1lIDogYFBsYXllciAke3BsYXllcklEfWA7XHJcbiAgICAgICAgdGhpcy5fZ3VpLm9uTXNnUmVjZWl2ZWQocGxheWVyTmFtZSwgbXNnKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIHN0YXJ0cyBkcmFnZ2luZyBhbiBpdGVtIGluIHRoZWlyIGludmVudG9yeVxyXG4gICAgcHVibGljIG9uSXRlbURyYWdTdGFydCgpIHtcclxuICAgICAgICB0aGlzLl9ndWkub25JdGVtRHJhZ1N0YXJ0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBsYXllciBzdG9wcyBkcmFnZ2luZyBhbiBpdGVtIGluIHRoZWlyIGludmVudG9yeVxyXG4gICAgcHVibGljIG9uSXRlbURyYWdFbmQoKSB7XHJcbiAgICAgICAgdGhpcy5fZ3VpLm9uSXRlbURyYWdFbmQoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIGxlZnQgb3IgcmlnaHQgY2xpY2tzIGEgYmFuZGFnZSBpbiB0aGVpciBpbnZlbnRvcnlcclxuICAgIHB1YmxpYyBvbkJhbmRhZ2VDbGljayh4OiBudW1iZXIsIHk6IG51bWJlciwgZW50aXR5SUQ6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2d1aS5vbkJhbmRhZ2VDbGljayh4LCB5LCBlbnRpdHlJRCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBsYXllciBjbGlja3MgYSBsaW1iIGJ1dHRvblxyXG4gICAgcHVibGljIG9uSGVhbExpbWJDbGljayhsaW1iOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLl9ndWkub25IZWFsTGltYkNsaWNrKGxpbWIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBwbGF5ZXIgY2xpY2tzIHRoZSBmdWxsIHNjcmVlbiBvdmVybGF5XHJcbiAgICBwdWJsaWMgb25GdWxsT3ZlcmxheUNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMuX2d1aS5vbkZ1bGxPdmVybGF5Q2xpY2soKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIGJlZ2lucyBob3ZlcmluZyBvdmVyIGEgbmV3IHRpbGUgZHVyaW5nIHRoZSBkcm9wIHBoYXNlXHJcbiAgICBwdWJsaWMgb25UaWxlSG92ZXJEcm9wUGhhc2UoaTogbnVtYmVyLCBqOiBudW1iZXIsIHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fZ3VpLm9uVGlsZUhvdmVyRHJvcFBoYXNlKGksIGosIHgsIHkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBwbGF5ZXIgYmVnaW5zIGhvdmVyaW5nIG92ZXIgYSBuZXcgdGlsZSBkdXJpbmcgdGhlIG1vdmUgcGhhc2VcclxuICAgIHB1YmxpYyBvblRpbGVIb3Zlck1vdmVQaGFzZShpOiBudW1iZXIsIGo6IG51bWJlciwgeDogbnVtYmVyLCB5OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9ndWkub25UaWxlSG92ZXJNb3ZlUGhhc2UoaSwgaiwgeCwgeSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBsYXllciBiZWdpbnMgaG92ZXJpbmcgb3ZlciBhIG5ldyB0aWxlIGR1cmluZyB0aGUgYWN0aW9uIHBoYXNlXHJcbiAgICBwdWJsaWMgb25UaWxlSG92ZXJBY3Rpb25QaGFzZShuYW1lOiBzdHJpbmcsIGRlc2M6IHN0cmluZywgYXBDb3N0OiBudW1iZXIsIHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5fZ3VpLm9uVGlsZUhvdmVyQWN0aW9uUGhhc2UobmFtZSwgZGVzYywgYXBDb3N0LCB4LCB5KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIGNsaWNrcyBhIHRpbGUgZHVyaW5nIHRoZSBhY3Rpb24gcGhhc2VcclxuICAgIHB1YmxpYyBvblRpbGVSaWdodENsaWNrZWRBY3Rpb25QaGFzZSh4OiBudW1iZXIsIHk6IG51bWJlciwgdGlsZVg6IG51bWJlcixcclxuICAgICAgICAgICAgdGlsZVk6IG51bWJlciwgY2FuU2VhcmNoOiBib29sZWFuLCBjYW5BdHRhY2s6IGJvb2xlYW4pIHtcclxuICAgICAgICB0aGlzLl9ndWkub25UaWxlQ2xpY2tlZEFjdGlvblBoYXNlKHgsIHksIHRpbGVYLCB0aWxlWSwgY2FuU2VhcmNoLCBjYW5BdHRhY2spO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBwbGF5ZXIgYmVnaW5zIGhvdmVyaW5nIG92ZXIgYW4gaXRlbVxyXG4gICAgcHVibGljIG9uSXRlbUhvdmVyKGVudGl0eTogSXRlbUVudGl0eSwgeDogbnVtYmVyLCB5OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9ndWkub25JdGVtSG92ZXIoZW50aXR5LCB4LCB5KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIGhvdmVycyBhbiBpdGVtIG1lbWVudG9cclxuICAgIHB1YmxpYyBvbkl0ZW1NZW1lbnRvSG92ZXIobWVtOiBJdGVtTWVtZW50bywgeDogbnVtYmVyLCB5OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9ndWkub25JdGVtTWVtZW50b0hvdmVyKG1lbSwgeCwgeSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUmVzaXplIHBhcnRzIG9mIHRoZSBHVUkgd2hlbiB0aGUgd2luZG93IHJlc2l6ZXNcclxuICAgIHB1YmxpYyBvbldpbmRvd1Jlc2l6ZSgpIHtcclxuICAgICAgICB0aGlzLl9ndWkub25XaW5kb3dSZXNpemUoKTtcclxuICAgIH1cclxufVxyXG4iLCIvKlxyXG5MaWNlbnNpbmcgSW5mb1xyXG5cclxuQ29weXJpZ2h0IChjKSBKYW1lcyBTdWdkZW4gMjAxOFxyXG5cclxuXHJcblxyXG53cywgaHR0cGhlYWQgbGljZW5zZWQgdW5kZXJcclxuPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuVGhlIE1JVCBMaWNlbnNlIChNSVQpXHJcbj09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcblxyXG5Db3B5cmlnaHQgKGMpIDIwMTcgU2VyZ2V5IEthbWFyZGluIDxnb2J3YXNAZ21haWwuY29tPlxyXG5cclxuUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGEgY29weVxyXG5vZiB0aGlzIHNvZnR3YXJlIGFuZCBhc3NvY2lhdGVkIGRvY3VtZW50YXRpb24gZmlsZXMgKHRoZSBcIlNvZnR3YXJlXCIpLCB0byBkZWFsXHJcbmluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmcgd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHNcclxudG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLCBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbFxyXG5jb3BpZXMgb2YgdGhlIFNvZnR3YXJlLCBhbmQgdG8gcGVybWl0IHBlcnNvbnMgdG8gd2hvbSB0aGUgU29mdHdhcmUgaXNcclxuZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZSBmb2xsb3dpbmcgY29uZGl0aW9uczpcclxuXHJcblRoZSBhYm92ZSBjb3B5cmlnaHQgbm90aWNlIGFuZCB0aGlzIHBlcm1pc3Npb24gbm90aWNlIHNoYWxsIGJlIGluY2x1ZGVkIGluIGFsbFxyXG5jb3BpZXMgb3Igc3Vic3RhbnRpYWwgcG9ydGlvbnMgb2YgdGhlIFNvZnR3YXJlLlxyXG5cclxuVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTUyBPUlxyXG5JTVBMSUVELCBJTkNMVURJTkcgQlVUIE5PVCBMSU1JVEVEIFRPIFRIRSBXQVJSQU5USUVTIE9GIE1FUkNIQU5UQUJJTElUWSxcclxuRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJTkdFTUVOVC4gSU4gTk8gRVZFTlQgU0hBTEwgVEhFXHJcbkFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sIERBTUFHRVMgT1IgT1RIRVJcclxuTElBQklMSVRZLCBXSEVUSEVSIElOIEFOIEFDVElPTiBPRiBDT05UUkFDVCwgVE9SVCBPUiBPVEhFUldJU0UsIEFSSVNJTkcgRlJPTSxcclxuT1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhFIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEVcclxuU09GVFdBUkUuXHJcblxyXG5cclxuXHJcbmpzLWNvb2tpZSBsaWNlbnNlZCB1bmRlclxyXG49PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5NSVQgTGljZW5zZVxyXG49PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PVxyXG5cclxuQ29weXJpZ2h0IChjKSAyMDE4IENvcHlyaWdodCAyMDE4IEtsYXVzIEhhcnRsLCBGYWduZXIgQnJhY2ssIEdpdEh1YiBDb250cmlidXRvcnNcclxuXHJcblBlcm1pc3Npb24gaXMgaGVyZWJ5IGdyYW50ZWQsIGZyZWUgb2YgY2hhcmdlLCB0byBhbnkgcGVyc29uIG9idGFpbmluZyBhIGNvcHlcclxub2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGUgXCJTb2Z0d2FyZVwiKSwgdG8gZGVhbFxyXG5pbiB0aGUgU29mdHdhcmUgd2l0aG91dCByZXN0cmljdGlvbiwgaW5jbHVkaW5nIHdpdGhvdXQgbGltaXRhdGlvbiB0aGUgcmlnaHRzXHJcbnRvIHVzZSwgY29weSwgbW9kaWZ5LCBtZXJnZSwgcHVibGlzaCwgZGlzdHJpYnV0ZSwgc3VibGljZW5zZSwgYW5kL29yIHNlbGxcclxuY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdCBwZXJzb25zIHRvIHdob20gdGhlIFNvZnR3YXJlIGlzXHJcbmZ1cm5pc2hlZCB0byBkbyBzbywgc3ViamVjdCB0byB0aGUgZm9sbG93aW5nIGNvbmRpdGlvbnM6XHJcblxyXG5UaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZCBpbiBhbGxcclxuY29waWVzIG9yIHN1YnN0YW50aWFsIHBvcnRpb25zIG9mIHRoZSBTb2Z0d2FyZS5cclxuXHJcblRIRSBTT0ZUV0FSRSBJUyBQUk9WSURFRCBcIkFTIElTXCIsIFdJVEhPVVQgV0FSUkFOVFkgT0YgQU5ZIEtJTkQsIEVYUFJFU1MgT1JcclxuSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFksXHJcbkZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFIEFORCBOT05JTkZSSU5HRU1FTlQuIElOIE5PIEVWRU5UIFNIQUxMIFRIRVxyXG5BVVRIT1JTIE9SIENPUFlSSUdIVCBIT0xERVJTIEJFIExJQUJMRSBGT1IgQU5ZIENMQUlNLCBEQU1BR0VTIE9SIE9USEVSXHJcbkxJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1IgT1RIRVJXSVNFLCBBUklTSU5HIEZST00sXHJcbk9VVCBPRiBPUiBJTiBDT05ORUNUSU9OIFdJVEggVEhFIFNPRlRXQVJFIE9SIFRIRSBVU0UgT1IgT1RIRVIgREVBTElOR1MgSU4gVEhFXHJcblNPRlRXQVJFLlxyXG5cclxuKi9cclxuXHJcbmltcG9ydCB7IENvbnRyb2xsZXIgfSBmcm9tIFwiLi9Db250cm9sbGVyXCJcclxuaW1wb3J0IHsgR1VJIH0gZnJvbSBcIi4vZ3VpL0dVSVwiXHJcblxyXG5sZXQgY29udHJvbGxlcjogQ29udHJvbGxlcjtcclxuXHJcbmZ1bmN0aW9uIG1haW4oKSB7XHJcbiAgICBjb250cm9sbGVyID0gbmV3IENvbnRyb2xsZXIoKTtcclxufVxyXG5cclxuLy8gV2hlbiB0aGUgd2luZG93IHJlc2l6ZXMsIHVwZGF0ZSB0aGUgc2l6ZSBvZiB0aGUgY2FudmFzXHJcbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsICgpID0+IHtcclxuICAgIGNvbnRyb2xsZXIub25XaW5kb3dSZXNpemUoKTtcclxufSk7XHJcblxyXG4vLyBXYWl0IHVudGlsIHRoZSBwYWdlIGlzIGZ1bGx5IGxvYWRlZCBiZWZvcmUgZG9pbmcgYW55dGhpbmdcclxud2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJsb2FkXCIsICgpID0+IHtcclxuICAgIG1haW4oKTtcclxufSk7XHJcbiIsImV4cG9ydCBjb25zdCBBY3Rpb25BcENvc3RzID0ge1xyXG4gICAgQ1JPVUNIOiAwLFxyXG4gICAgTU9WRTogNDAsXHJcbiAgICBBVFRBQ0s6IDYwLFxyXG4gICAgRVFVSVBfV0VBUE9OOiA1LFxyXG4gICAgQkFOREFHRTogMzAsXHJcbiAgICBTRUFSQ0g6IDIwXHJcbn07XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEFjdGlvbiB7XHJcbiAgICBuYW1lOiBzdHJpbmcsXHJcbiAgICBhcENvc3Q6IG51bWJlclxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEFjdGlvbkF0dGFjayBleHRlbmRzIEFjdGlvbiB7XHJcbiAgICBwbGF5ZXJJRDogbnVtYmVyXHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgQWN0aW9uRXF1aXBXZWFwb24gZXh0ZW5kcyBBY3Rpb24ge1xyXG4gICAgZW50aXR5SUQ6IG51bWJlcixcclxuICAgIGl0ZW1JRDogbnVtYmVyXHJcbn1cclxuIiwiZXhwb3J0IGVudW0gR2FtZVBoYXNlIHtcclxuICAgIExPQkJZLFxyXG4gICAgRFJPUF9QSEFTRSxcclxuICAgIE1PVkVfUEhBU0UsXHJcbiAgICBBQ1RJT05fUEhBU0UsXHJcbiAgICBXQUlUSU5HX0ZPUl9TRVJWRVJcclxufVxyXG4iLCJpbXBvcnQgeyBJdGVtTWVtZW50byB9IGZyb20gXCIuL0l0ZW1NZW1lbnRvXCJcclxuXHJcbmV4cG9ydCBjbGFzcyBIaXRNYXAge1xyXG4gICAgLy8gTk9URSAtIHVzZSAyIHNlcGFyYXRlIG1hcHMgZm9yIGZhc3RlciBtZW1lbnRvIGxvb2t1cFxyXG4gICAgLy8gTWFwIGZyb20gY29sb3IgdG8gdGlsZSAoeCwgeSlcclxuICAgIHByaXZhdGUgX3RpbGVDb2xvck1hcCA9IHt9O1xyXG4gICAgLy8gTWFwIGZyb20gY29sb3IgdG8gaXRlbSBtZW1lbnRvIChtZW0pXHJcbiAgICBwcml2YXRlIF9tZW1lbnRvQ29sb3JNYXAgPSB7fTtcclxuXHJcbiAgICAvLyBhZGQgYW4gaXRlbSBtZW1lbnRvIHRvIHRoZSBjb2xvciBtYXBcclxuICAgIC8vIHJldHVybnMgdGhlIGNvbG9yIGtleSBhcyBhIHN0cmluZ1xyXG4gICAgcHVibGljIGFkZEl0ZW1NZW1lbnRvKG0pOiBzdHJpbmcge1xyXG4gICAgICAgIGlmKG0pIHtcclxuICAgICAgICAgICAgY29uc3QgY29sb3JLZXkgPSB0aGlzLmdlblVuaXF1ZVJhbmRvbUNvbG9yKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX21lbWVudG9Db2xvck1hcFtjb2xvcktleV0gPSB7IG1lbTogbSB9O1xyXG4gICAgICAgICAgICByZXR1cm4gY29sb3JLZXk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHJlbW92ZSBhbiBpdGVtIG1lbWVudG8gZnJvbSB0aGUgY29sb3IgbWFwXHJcbiAgICBwdWJsaWMgcmVtb3ZlSXRlbU1lbWVudG8obSkge1xyXG4gICAgICAgIGZvcihjb25zdCBjb2xvciBpbiB0aGlzLl9tZW1lbnRvQ29sb3JNYXApIHtcclxuICAgICAgICAgICAgY29uc3QgbWVtID0gdGhpcy5fbWVtZW50b0NvbG9yTWFwW2NvbG9yXS5tZW07XHJcbiAgICAgICAgICAgIGlmKG1lbSA9PT0gbSkge1xyXG4gICAgICAgICAgICAgICAgZGVsZXRlIHRoaXMuX21lbWVudG9Db2xvck1hcFtjb2xvcl07XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBhZGQgYSB0aWxlIHRvIHRoZSBjb2xvciBtYXBcclxuICAgIC8vIHJldHVybnMgdGhlIGNvbG9yIGtleSBhcyBhIHN0cmluZ1xyXG4gICAgcHVibGljIGFkZFRpbGUoeDogbnVtYmVyLCB5OiBudW1iZXIpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IGNvbG9yS2V5ID0gdGhpcy5nZW5VbmlxdWVSYW5kb21Db2xvcigpO1xyXG4gICAgICAgIHRoaXMuX3RpbGVDb2xvck1hcFtjb2xvcktleV0gPSB7XHJcbiAgICAgICAgICAgIHg6IHgsXHJcbiAgICAgICAgICAgIHk6IHlcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiBjb2xvcktleTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBnZXQgdGhlIHRpbGUgb3IgaXRlbSBtZW1lbnRvIGFzc29jaWF0ZWQgd2l0aCB0aGUgZ2l2ZW4gY29sb3JcclxuICAgIHB1YmxpYyBnZXQoY29sb3JLZXk6IHN0cmluZyk6IHsgeDogbnVtYmVyLCB5OiBudW1iZXIgfSB8IHsgbWVtOiBJdGVtTWVtZW50byB9IHtcclxuICAgICAgICBpZih0aGlzLl90aWxlQ29sb3JNYXBbY29sb3JLZXldKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl90aWxlQ29sb3JNYXBbY29sb3JLZXldO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fbWVtZW50b0NvbG9yTWFwW2NvbG9yS2V5XTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBnZW5lcmF0ZSBhIHVuaXF1ZSByYW5kb20gY29sb3JcclxuICAgIC8vIHNvdXJjZWQgZnJvbSBodHRwczovL2Jsb2cubGF2cnRvbi5jb20vaGl0LXJlZ2lvbi1kZXRlY3Rpb24tZm9yLWh0bWw1LWNhbnZhcy1hbmQtaG93LXRvLWxpc3Rlbi10by1jbGljay1ldmVudHMtb24tY2FudmFzLXNoYXBlcy04MTUwMzRkN2U5ZjhcclxuICAgIHByaXZhdGUgZ2VuVW5pcXVlUmFuZG9tQ29sb3IoKTogc3RyaW5nIHtcclxuICAgICAgICAvLyBrZWVwIGdlbmVyYXRpbmcgcmFuZG9tIGNvbG9ycyB1bnRpbCB1bmlxdWUgY29sb3IgaXMgZm91bmRcclxuICAgICAgICB2YXIgY29sb3JLZXk7XHJcbiAgICAgICAgZG8ge1xyXG4gICAgICAgICAgICBjb25zdCByID0gTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMjU1KTtcclxuICAgICAgICAgICAgY29uc3QgZyA9IE1hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqIDI1NSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGIgPSBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAyNTUpO1xyXG4gICAgICAgICAgICBjb2xvcktleSA9IGByZ2IoJHtyfSwke2d9LCR7Yn0pYDtcclxuICAgICAgICB9IHdoaWxlKHRoaXMuX3RpbGVDb2xvck1hcFtjb2xvcktleV0gfHwgdGhpcy5fbWVtZW50b0NvbG9yTWFwW2NvbG9yS2V5XSk7XHJcblxyXG4gICAgICAgIHJldHVybiBjb2xvcktleTtcclxuICAgIH1cclxufVxyXG4iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcIi4uL0NvbnRyb2xsZXJcIlxyXG5pbXBvcnQgeyBJdGVtRW50aXR5IH0gZnJvbSBcIi4vSXRlbVwiXHJcblxyXG5leHBvcnQgY2xhc3MgSW52ZW50b3J5IHtcclxuICAgIHB1YmxpYyBzdGF0aWMgcmVhZG9ubHkgUk9XX0xFTkdUSCA9IDM7XHJcbiAgICBwdWJsaWMgc3RhdGljIHJlYWRvbmx5IENPTF9IRUlHSFQgPSA0O1xyXG5cclxuICAgIC8vIEdyaWQgb2YgaXRlbSBlbnRpdGllc1xyXG4gICAgcHJpdmF0ZSBfaXRlbVNsb3RzOiBBcnJheTxBcnJheTxJdGVtRW50aXR5Pj47XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIC8vIEluaXRpYWxpc2UgaXRlbSBzbG90cyBhcyBhIGdyaWQgZnVsbCBvZiBudWxsIGl0ZW0gZW50aXRpZXNcclxuICAgICAgICB0aGlzLl9pdGVtU2xvdHMgPSBuZXcgQXJyYXk8QXJyYXk8SXRlbUVudGl0eT4+KCk7XHJcbiAgICAgICAgZm9yKGxldCBqID0gMDsgaiA8IEludmVudG9yeS5DT0xfSEVJR0hUOyBqKyspIHtcclxuICAgICAgICAgICAgdGhpcy5faXRlbVNsb3RzLnB1c2gobmV3IEFycmF5PEl0ZW1FbnRpdHk+KCkpO1xyXG4gICAgICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgSW52ZW50b3J5LlJPV19MRU5HVEg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5faXRlbVNsb3RzW2pdLnB1c2gobnVsbCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldCBpdGVtU2xvdHMoKTogQXJyYXk8QXJyYXk8SXRlbUVudGl0eT4+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faXRlbVNsb3RzO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFkZCBhbiBpdGVtIGVudGl0eSB0byB0aGUgcGxheWVycyBpbnZlbnRvcnlcclxuICAgIC8vIFJldHVybnMgdHJ1ZSBpZmYgdGhlIGl0ZW0gaXMgYWRkZWQgc3VjY2Vzc2Z1bGx5XHJcbiAgICBwdWJsaWMgYWRkSXRlbUVudGl0eShpdGVtRW50aXR5OiBJdGVtRW50aXR5LCBmaXJzdFNsb3RYOiBudW1iZXIsIGZpcnN0U2xvdFk6IG51bWJlcik6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmKCFpdGVtRW50aXR5KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGl0ZW0gPSB0aGlzLmNvbnRyb2xsZXIuaXRlbXNbaXRlbUVudGl0eS5pdGVtSURdO1xyXG5cclxuICAgICAgICAvLyBDaGVjayBpdGVtIGV4aXN0c1xyXG4gICAgICAgIGlmKCFpdGVtKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIEJvdW5kcyBjaGVja1xyXG4gICAgICAgIGlmKGZpcnN0U2xvdFggPCAwIHx8IGZpcnN0U2xvdFgraXRlbS5ncmlkV2lkdGggPiBJbnZlbnRvcnkuUk9XX0xFTkdUSCB8fFxyXG4gICAgICAgICAgICBmaXJzdFNsb3RZIDwgMCB8fCBmaXJzdFNsb3RZK2l0ZW0uZ3JpZEhlaWdodCA+IEludmVudG9yeS5DT0xfSEVJR0hUKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIElmIGFsbCBzbG90cyByZXF1aXJlZCBieSB0aGUgaXRlbSBhcmUgZnJlZSwgYWRkIHRoZSBlbnRpdHkgdG8gdGhlIGludmVudG9yeVxyXG4gICAgICAgIC8vIFRPRE8gLSBhZGQgc3VwcG9ydCBmb3Igcm90YXRlZCBpdGVtIGVudGl0aWVzXHJcbiAgICAgICAgZm9yKGxldCBzbG90WCA9IGZpcnN0U2xvdFg7IHNsb3RYIDwgZmlyc3RTbG90WCtpdGVtLmdyaWRXaWR0aDsgc2xvdFgrKykge1xyXG4gICAgICAgICAgICBmb3IobGV0IHNsb3RZID0gZmlyc3RTbG90WTsgc2xvdFkgPCBmaXJzdFNsb3RZK2l0ZW0uZ3JpZEhlaWdodDsgc2xvdFkrKykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5faXRlbVNsb3RzW3Nsb3RZXVtzbG90WF0gPSBpdGVtRW50aXR5O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBSZXR1cm4gdHJ1ZSBhcyBpdGVtIGVudGl0eSBzdWNjZXNzZnVsbHkgYWRkZWRcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcmVtb3ZlSXRlbUVudGl0eShpdGVtRW50aXR5OiBJdGVtRW50aXR5KSB7XHJcblxyXG4gICAgfVxyXG59XHJcbiIsImV4cG9ydCBpbnRlcmZhY2UgSXRlbSB7XHJcbiAgICBuYW1lOiBzdHJpbmcsXHJcbiAgICBpbWFnZTogc3RyaW5nLFxyXG4gICAgZ3JpZFdpZHRoOiBudW1iZXIsXHJcbiAgICBncmlkSGVpZ2h0OiBudW1iZXJcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBHdW4gZXh0ZW5kcyBJdGVtIHtcclxuICAgIHNob3RzUGVyQWN0aW9uOiBudW1iZXIsXHJcbiAgICBkbWdQZXJIaXQ6IG51bWJlcixcclxuICAgIG1heFJhbmdlOiBudW1iZXIsXHJcbiAgICBhY2N1cmFjeTogbnVtYmVyLFxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFByb2plY3RpbGUgZXh0ZW5kcyBJdGVtIHtcclxuICAgIHRpbWVUb1Rocm93OiBudW1iZXIsXHJcbiAgICBkbWdQZXJIaXQ6IG51bWJlclxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEJhbmRhZ2UgZXh0ZW5kcyBJdGVtIHtcclxuICAgIGhlYWxBbW91bnQ6IG51bWJlclxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gaXNHdW4oaXRlbTogSXRlbSk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIFwic2hvdHNQZXJBY3Rpb25cIiBpbiBpdGVtO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gaXNQcm9qZWN0aWxlKGl0ZW06IEl0ZW0pOiBib29sZWFuIHtcclxuICAgIHJldHVybiBcInRpbWVUb1Rocm93XCIgaW4gaXRlbTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGlzQmFuZGFnZShpdGVtOiBJdGVtKTogYm9vbGVhbiB7XHJcbiAgICByZXR1cm4gXCJoZWFsQW1vdW50XCIgaW4gaXRlbTtcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEl0ZW1FbnRpdHkge1xyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHB1YmxpYyBlbnRpdHlJRDogbnVtYmVyLCBwdWJsaWMgaXRlbUlEOiBudW1iZXIpIHt9XHJcbn1cclxuIiwiaW1wb3J0IHsgSXRlbSB9IGZyb20gXCIuL0l0ZW1cIlxyXG5cclxuZXhwb3J0IGNsYXNzIEl0ZW1NZW1lbnRvIHtcclxuICAgIHByaXZhdGUgX3NpbmNlOiBudW1iZXI7ICAvLyBjb3VudHMgdXAgZnJvbSAwLCBvbmNlIDUsIG1lbWVudG8gd2lsbCBiZSByZW1vdmVkXHJcbiAgICBwcml2YXRlIF9pdGVtczogQXJyYXk8SXRlbT47XHJcbiAgICBwcml2YXRlIF9jb2xvcktleTogc3RyaW5nO1xyXG5cclxuICAgIHB1YmxpYyBjb25zdHJ1Y3Rvcihwcml2YXRlIF9tYXBYOiBudW1iZXIsIHByaXZhdGUgX21hcFk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2l0ZW1zID0gbmV3IEFycmF5PEl0ZW0+KCk7XHJcbiAgICAgICAgdGhpcy5fc2luY2UgPSAwO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBtYXBYKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX21hcFhcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbWFwWSgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9tYXBZO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBzaW5jZSgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zaW5jZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaW5jU2luY2UoKSB7XHJcbiAgICAgICAgdGhpcy5fc2luY2UgKys7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlc2V0KCkge1xyXG4gICAgICAgIHRoaXMuX3NpbmNlID0gMDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaXRlbXMoKTogQXJyYXk8SXRlbT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pdGVtcztcclxuICAgIH1cclxuXHJcbiAgICBzZXQgaXRlbXMoaTogQXJyYXk8SXRlbT4pIHtcclxuICAgICAgICBpZihpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2l0ZW1zID0gaTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGNvbG9yS2V5KCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbG9yS2V5O1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBjb2xvcktleShzOiBzdHJpbmcpIHtcclxuICAgICAgICBpZihzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NvbG9yS2V5ID0gcztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuXHJcbmV4cG9ydCBjbGFzcyBNYXAge1xyXG4gICAgLy8gbXVsdGktZGltZW5zaW9uYWwgYXJyYXkgZ2l2aW5nIHRoZSBtYXAgdGlsZXNcclxuICAgIHByaXZhdGUgX2hleEdyaWQ6IGFueTtcclxuXHJcbiAgICAvLyBtYXAgZnJvbSBjb2xvciB0byB0aWxlICh4LCB5KVxyXG4gICAgLy9wcml2YXRlIF90aWxlQ29sb3JNYXAgPSB7fTtcclxuXHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IocHJpdmF0ZSBjb250cm9sbGVyOiBDb250cm9sbGVyLCB0ZXh0OiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLmluaXRNYXAodGV4dCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpbml0TWFwKHRleHQ6IHN0cmluZykge1xyXG4gICAgICAgIC8vIGluaXRpYWxpc2UgdGhlIG1hcCBhcnJheVxyXG4gICAgICAgIHRoaXMuX2hleEdyaWQgPSBbXTtcclxuICAgICAgICAvLyBzcGxpdCBpbnRvIGxpbmVzIChyb3dzKVxyXG4gICAgICAgIGNvbnN0IGxpbmVzID0gdGV4dC5zcGxpdChcIlxcblwiKTtcclxuICAgICAgICBsZXQgeSA9IDA7XHJcbiAgICAgICAgZm9yKGNvbnN0IGxpbmUgb2YgbGluZXMpIHtcclxuICAgICAgICAgICAgdGhpcy5faGV4R3JpZC5wdXNoKFtdKTtcclxuICAgICAgICAgICAgLy8gc3BsaXQgaW50byB0aWxlc1xyXG4gICAgICAgICAgICBjb25zdCB0aWxlcyA9IGxpbmUuc3BsaXQoXCIgXCIpO1xyXG4gICAgICAgICAgICBsZXQgeCA9IDA7XHJcbiAgICAgICAgICAgIGZvcihjb25zdCB0aWxlIG9mIHRpbGVzKSB7XHJcbiAgICAgICAgICAgICAgICAvKnZhciBjb2xvcktleSA9IHRoaXMuZ2VuUmFuZG9tQ29sb3IoKTtcclxuICAgICAgICAgICAgICAgIC8vIGtleSBnZW5lcmF0aW5nIHJhbmRvbSBjb2xvcnMgdW50aWwgdW5pcXVlIGNvbG9yIGlzIGZvdW5kXHJcbiAgICAgICAgICAgICAgICB3aGlsZSh0aGlzLl90aWxlQ29sb3JNYXBbY29sb3JLZXldKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3JLZXkgPSB0aGlzLmdlblJhbmRvbUNvbG9yKCk7XHJcbiAgICAgICAgICAgICAgICB9Ki9cclxuICAgICAgICAgICAgICAgIC8vIHNldCB0aGUgdGlsZSBvYmplY3QgJiBhZGQgdGlsZSB0byBoaXQgbWFwXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9oZXhHcmlkW3RoaXMuX2hleEdyaWQubGVuZ3RoLTFdLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6IHBhcnNlSW50KHRpbGUpIHx8IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3JLZXk6IHRoaXMuY29udHJvbGxlci5oaXRNYXAuYWRkVGlsZSh4LCB5KVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAvLyBhZGQgdGlsZSB0byBjb2xvciBtYXBcclxuICAgICAgICAgICAgICAgIC8qdGhpcy5fdGlsZUNvbG9yTWFwW2NvbG9yS2V5XSA9IHtcclxuICAgICAgICAgICAgICAgICAgICB4OiB4LFxyXG4gICAgICAgICAgICAgICAgICAgIHk6IHlcclxuICAgICAgICAgICAgICAgIH07Ki9cclxuICAgICAgICAgICAgICAgIHgrKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB5Kys7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIGdlbmVyYXRlIGEgcmFuZG9tIGNvbG9yXHJcbiAgICAvLyBzb3VyY2VkIGZyb20gaHR0cHM6Ly9ibG9nLmxhdnJ0b24uY29tL2hpdC1yZWdpb24tZGV0ZWN0aW9uLWZvci1odG1sNS1jYW52YXMtYW5kLWhvdy10by1saXN0ZW4tdG8tY2xpY2stZXZlbnRzLW9uLWNhbnZhcy1zaGFwZXMtODE1MDM0ZDdlOWY4XHJcbiAgICAvKnByaXZhdGUgZ2VuUmFuZG9tQ29sb3IoKTogc3RyaW5nIHtcclxuICAgICAgICBjb25zdCByID0gTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMjU1KTtcclxuICAgICAgICBjb25zdCBnID0gTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMjU1KTtcclxuICAgICAgICBjb25zdCBiID0gTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMjU1KTtcclxuICAgICAgICByZXR1cm4gYHJnYigke3J9LCR7Z30sJHtifSlgO1xyXG4gICAgfSovXHJcblxyXG4gICAgZ2V0IGhleEdyaWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2hleEdyaWQ7XHJcbiAgICB9XHJcblxyXG4vKiAgICBnZXQgdGlsZUNvbG9yTWFwKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl90aWxlQ29sb3JNYXA7XHJcbiAgICB9Ki9cclxufVxyXG4iLCJleHBvcnQgY2xhc3MgVGlsZVR5cGUge1xyXG4gICAgcHJpdmF0ZSBzdGF0aWMgX3R5cGVzOiBBcnJheTxUaWxlVHlwZT47XHJcblxyXG4gICAgcHJpdmF0ZSBjb25zdHJ1Y3Rvcihwcml2YXRlIF9uYW1lOiBzdHJpbmcsIHByaXZhdGUgX2NvbG9yOiBzdHJpbmcsIHByaXZhdGUgX2Rlc2M6IEFycmF5PHN0cmluZz4sIHByaXZhdGUgX2ltZzogSFRNTEltYWdlRWxlbWVudD1udWxsLFxyXG4gICAgICAgICAgICAgICAgICAgIHByaXZhdGUgX2ltcGFzc2FibGU6IGJvb2xlYW49ZmFsc2UsIHByaXZhdGUgX3VuZHJvcHBhYmxlOiBib29sZWFuPWZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHByaXZhdGUgX2Jsb2Nrc1Zpc2lvbjogYm9vbGVhbj1mYWxzZSkge31cclxuXHJcbiAgICAvLyBUT0RPIC0gbWFrZSB0aWxlc2V0IG1hcCBzcGVjaWZpYyB1c2luZyBtYXAubWV0YSBmaWxlXHJcbiAgICBzdGF0aWMgaW5pdCgpIHtcclxuICAgICAgICB0aGlzLl90eXBlcyA9IFtcclxuICAgICAgICAgICAgbmV3IFRpbGVUeXBlKFwiV2F0ZXJcIiwgXCIjNDBhNGRmXCIsIFtcIlJlcXVpcmVzIGJvYXQgdG8gdHJhdmVsXCJdLCBudWxsLCB0cnVlLCB0cnVlKSxcclxuICAgICAgICAgICAgbmV3IFRpbGVUeXBlKFwiQm9nXCIsIFwiIzM5NUQzM1wiLCBbXCJWZWhpY2xlcyBnZXQgc3R1Y2tcIl0pLFxyXG4gICAgICAgICAgICBuZXcgVGlsZVR5cGUoXCJHcmFzc2xhbmRcIiwgXCIjMzA4MDE0XCIsIFtcIk1heSBjb250YWluIG1pbmVzXCJdKSxcclxuICAgICAgICAgICAgbmV3IFRpbGVUeXBlKFwiRm9yZXN0XCIsIFwiIzMwODAxNFwiLCBbXCJQcm92aWRlcyBjb3ZlciBmcm9tIGVuZW1pZXNcIiwgXCJHcmVhdGVyIGNoYW5jZSB0byBmaW5kIGl0ZW1zXCJdLCBuZXcgSW1hZ2UoKSwgZmFsc2UsIGZhbHNlLCB0cnVlKSxcclxuICAgICAgICAgICAgbmV3IFRpbGVUeXBlKFwiSGlsbHNcIiwgXCIjMzA4MDE0XCIsIFtcIlVwIEhpZ2ggKCsxIFZpZXcgUmFuZ2UpXCJdLCBuZXcgSW1hZ2UoKSksXHJcbiAgICAgICAgICAgIG5ldyBUaWxlVHlwZShcIk1vdXRhaW5zXCIsIFwiIzgwODA4MFwiLCBbXCJXaWxsIHByb2JhYmx5IGJsb2NrIHZpc2lvbiB3aGVuIEkgZmlndXJlIG91dCBob3cgdG8gZ2V0IHRoYXQgdG8gd29ya1wiXSwgbmV3IEltYWdlKCksIHRydWUsIHRydWUsIHRydWUpLFxyXG4gICAgICAgICAgICBuZXcgVGlsZVR5cGUoXCJIb3VzZVwiLCBcIiM3ZjAwMDBcIiwgW1wiUHJvdmlkZXMgY292ZXIgZnJvbSBlbmVtaWVzXCIsIFwiR3JlYXRlciBjaGFuY2UgdG8gZmluZCBpdGVtc1wiXSwgbmV3IEltYWdlKCksIGZhbHNlLCB0cnVlKSxcclxuICAgICAgICAgICAgbmV3IFRpbGVUeXBlKFwiQ2l0eVwiLCBcIiM3ZjAwMDBcIiwgW1wiUHJvdmlkZXMgY292ZXIgZnJvbSBlbmVtaWVzXCIsIFwiR3JlYXRlciBjaGFuY2UgdG8gZmluZCBpdGVtc1wiLCBcIlVwIEhpZ2ggKCsxIFZpZXcgUmFuZ2UpXCJdLCBuZXcgSW1hZ2UoKSwgZmFsc2UsIHRydWUsIHRydWUpLFxyXG4gICAgICAgICAgICBuZXcgVGlsZVR5cGUoXCJCcmlkZ2VcIiwgXCIjNDBhNGRmXCIsIFtcIkFsbG93cyBsYW5kLWJhc2VkIHRyYXZlbCBvdmVyIHdhdGVyXCJdLCBuZXcgSW1hZ2UoKSlcclxuICAgICAgICBdO1xyXG5cclxuICAgICAgICAvLyB5dWNrXHJcbiAgICAgICAgdGhpcy5fdHlwZXNbM10uaW1nLnNyYyA9IFwiYXNzZXRzL1RyZWUgKDUxMng1MTIpLnBuZ1wiO1xyXG4gICAgICAgIHRoaXMuX3R5cGVzWzRdLmltZy5zcmMgPSBcImFzc2V0cy9IaWxsICg1MTJ4NTEyKS5wbmdcIjtcclxuICAgICAgICB0aGlzLl90eXBlc1s1XS5pbWcuc3JjID0gXCJhc3NldHMvTW91bnRhaW4gKDUxMng1MTIpLnBuZ1wiO1xyXG4gICAgICAgIHRoaXMuX3R5cGVzWzZdLmltZy5zcmMgPSBcImFzc2V0cy9Ib3VzZSAoNTEyeDUxMikucG5nXCI7XHJcbiAgICAgICAgdGhpcy5fdHlwZXNbN10uaW1nLnNyYyA9IFwiYXNzZXRzL1NreXNjcmFwZXIgKDUxMng1MTIpLnBuZ1wiO1xyXG4gICAgICAgIHRoaXMuX3R5cGVzWzhdLmltZy5zcmMgPSBcImFzc2V0cy9CcmlkZ2UgKDUxMng1MTIpLnBuZ1wiO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBuYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX25hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGNvbG9yKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbG9yO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBkZXNjKCk6IEFycmF5PHN0cmluZz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kZXNjO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpbWcoKTogSFRNTEltYWdlRWxlbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2ltZztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaW1wYXNzYWJsZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faW1wYXNzYWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgdW5kcm9wcGFibGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3VuZHJvcHBhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBibG9ja3NWaXNpb24oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2Jsb2Nrc1Zpc2lvbjtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgZ2V0IHR5cGVzKCk6IEFycmF5PFRpbGVUeXBlPiB7XHJcbiAgICAgICAgcmV0dXJuIFRpbGVUeXBlLl90eXBlcztcclxuICAgIH1cclxufVxyXG5cclxuVGlsZVR5cGUuaW5pdCgpO1xyXG4iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcIi4uL0NvbnRyb2xsZXJcIlxyXG5pbXBvcnQgeyBBY3Rpb24gfSBmcm9tIFwiLi4vZ2FtZS9BY3Rpb25cIlxyXG5cclxuZXhwb3J0IGNsYXNzIEFjdGlvbkxpc3Qge1xyXG4gICAgcHJpdmF0ZSBhY3Rpb25MaXN0Q29udGFpbmVyOiBIVE1MRGl2RWxlbWVudDtcclxuICAgIHByaXZhdGUgYWN0aW9uTGlzdDogSFRNTERpdkVsZW1lbnQ7XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIHRoaXMuYWN0aW9uTGlzdENvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYWN0aW9uLWxpc3QtY29udGFpbmVyXCIpIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYWN0aW9uTGlzdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYWN0aW9uLWxpc3RcIikgYXMgSFRNTERpdkVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFwcGVuZEFjdGlvbihhY3Rpb246IEFjdGlvbiwgbW91c2V4OiBudW1iZXIsIG1vdXNleTogbnVtYmVyKSB7XHJcbiAgICAgICAgbGV0IHRpdGxlTGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgIHRpdGxlTGJsLmlubmVySFRNTCA9IGFjdGlvbi5uYW1lO1xyXG4gICAgICAgIHRpdGxlTGJsLnN0eWxlLm1hcmdpbiA9IFwiNXB4XCI7XHJcbiAgICAgICAgdGl0bGVMYmwuc3R5bGUuZm9udFNpemUgPSBcIjJ2d1wiO1xyXG5cclxuICAgICAgICBsZXQgYXBMYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIik7XHJcbiAgICAgICAgYXBMYmwuaW5uZXJIVE1MID0gYCR7YWN0aW9uLmFwQ29zdH1BUGA7XHJcbiAgICAgICAgYXBMYmwuc3R5bGUuY29sb3IgPSBcIiMwMDAwYjJcIjtcclxuICAgICAgICBhcExibC5zdHlsZS5tYXJnaW4gPSBcIjVweFwiO1xyXG4gICAgICAgIGFwTGJsLnN0eWxlLmZvbnRTaXplID0gXCIydndcIjtcclxuXHJcbiAgICAgICAgY29uc3QgdG90YWxBUExibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYWN0aW9uLWxpc3QtYXAtbGJsXCIpO1xyXG4gICAgICAgIHRvdGFsQVBMYmwuaW5uZXJIVE1MID0gYCR7dGhpcy5jb250cm9sbGVyLm15UGxheWVyQVB9QVBgO1xyXG5cclxuICAgICAgICB0aGlzLmFjdGlvbkxpc3QuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImJyXCIpKTtcclxuICAgICAgICB0aGlzLmFjdGlvbkxpc3QuYXBwZW5kQ2hpbGQodGl0bGVMYmwpO1xyXG4gICAgICAgIHRoaXMuYWN0aW9uTGlzdC5hcHBlbmRDaGlsZChhcExibCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlc2V0QWN0aW9uTGlzdCgpIHtcclxuICAgICAgICAvLyBSZW1vdmUgYWxsIGVsZW1lbnRzIGZyb20gdGhlIGRpdlxyXG4gICAgICAgIHdoaWxlKHRoaXMuYWN0aW9uTGlzdC5maXJzdENoaWxkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWN0aW9uTGlzdC5yZW1vdmVDaGlsZCh0aGlzLmFjdGlvbkxpc3QuZmlyc3RDaGlsZCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBSZS1hZGQgdGhlIHRpdGxlIGFuZCBBUCBsYWJlbFxyXG4gICAgICAgIC8qbGV0IHRpdGxlTGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgIHRpdGxlTGJsLmlubmVySFRNTCA9IFwiQWN0aW9uc1wiO1xyXG4gICAgICAgIHRpdGxlTGJsLnN0eWxlLm1hcmdpbiA9IFwiMC4yNWVtXCI7XHJcblxyXG4gICAgICAgIGxldCBhcExibCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsYWJlbFwiKTtcclxuICAgICAgICBhcExibC5pZCA9IFwidG90YWwtYXAtbGJsXCI7XHJcbiAgICAgICAgYXBMYmwuaW5uZXJIVE1MID0gYCR7dGhpcy5jb250cm9sbGVyLm15UGxheWVyQVB9QVBgO1xyXG4gICAgICAgIGFwTGJsLnN0eWxlLmNvbG9yID0gXCIjMDAwMGIyXCI7XHJcbiAgICAgICAgYXBMYmwuc3R5bGUubWFyZ2luID0gXCIwLjI1ZW1cIjtcclxuXHJcbiAgICAgICAgdGhpcy5hY3Rpb25MaXN0LmFwcGVuZENoaWxkKHRpdGxlTGJsKTtcclxuICAgICAgICB0aGlzLmFjdGlvbkxpc3QuYXBwZW5kQ2hpbGQoYXBMYmwpO1xyXG4gICAgICAgIHRoaXMuYWN0aW9uTGlzdC5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XHJcbiAgICAgICAgdGhpcy5hY3Rpb25MaXN0LnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7Ki9cclxuICAgICAgICB0aGlzLmFjdGlvbkxpc3RDb250YWluZXIuc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoaWRlKCkge1xyXG4gICAgICAgIHRoaXMuYWN0aW9uTGlzdENvbnRhaW5lci5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIjtcclxuICAgIH1cclxufVxyXG4iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcIi4uL0NvbnRyb2xsZXJcIlxyXG5pbXBvcnQgeyBJdGVtLCBHdW4gfSBmcm9tIFwiLi4vZ2FtZS9JdGVtXCJcclxuXHJcbmV4cG9ydCBjbGFzcyBBdHRhY2tDaGFyRGl2IHtcclxuICAgIC8vIEhUTUwgRWxlbWVudHNcclxuICAgIHByaXZhdGUgYXR0YWNrQ2hhckRpdjogSFRNTERpdkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGF0dGFja0NoYXJMYmw6IEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGF0dGFja0NoYXJDYW5jZWxCdG46IEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBhdHRhY2tIZWFkQnRuOiBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrQ2hlc3RCdG46IEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBhdHRhY2tSQXJtQnRuOiBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrTEFybUJ0bjogSFRNTEJ1dHRvbkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGF0dGFja1JMZWdCdG46IEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBhdHRhY2tMTGVnQnRuOiBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrSGVhZEhQQmFyOiBIVE1MU3BhbkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGF0dGFja0NoZXN0SFBCYXI6IEhUTUxTcGFuRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrUkFybUhQQmFyOiBIVE1MU3BhbkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGF0dGFja0xBcm1IUEJhcjogSFRNTFNwYW5FbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBhdHRhY2tSTGVnSFBCYXI6IEhUTUxTcGFuRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrTExlZ0hQQmFyOiBIVE1MU3BhbkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGF0dGFja0hlYWRIUExibDogSFRNTExhYmVsRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrQ2hlc3RIUExibDogSFRNTExhYmVsRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrUkFybUhQTGJsOiBIVE1MTGFiZWxFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBhdHRhY2tMQXJtSFBMYmw6IEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGF0dGFja1JMZWdIUExibDogSFRNTExhYmVsRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrTExlZ0hQTGJsOiBIVE1MTGFiZWxFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBhdHRhY2tIZWFkSGl0Q2hhbmNlTGJsOiBIVE1MTGFiZWxFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBhdHRhY2tDaGVzdEhpdENoYW5jZUxibDogSFRNTExhYmVsRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrUkFybUhpdENoYW5jZUxibDogSFRNTExhYmVsRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrTEFybUhpdENoYW5jZUxibDogSFRNTExhYmVsRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrUkxlZ0hpdENoYW5jZUxibDogSFRNTExhYmVsRWxlbWVudDtcclxuICAgIHByaXZhdGUgYXR0YWNrTExlZ0hpdENoYW5jZUxibDogSFRNTExhYmVsRWxlbWVudDtcclxuXHJcbiAgICAvLyBJbnRlcm5hbCBTdGF0ZVxyXG4gICAgLy8gSUQgb2YgcGxheWVyIGJlaW5nIHRhcmdldGVkXHJcbiAgICBwcml2YXRlIHBsYXllcklEOiBzdHJpbmc7XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIHRoaXMuYXR0YWNrQ2hhckRpdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLWNoYXItZGl2XCIpIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrQ2hhckxibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLWNoYXItbGJsXCIpIGFzIEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tDaGFyQ2FuY2VsQnRuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stY2hhci1jYW5jZWwtYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG5cclxuICAgICAgICB0aGlzLmF0dGFja0hlYWRCdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImF0dGFjay1oZWFkLWJ0blwiKSBhcyBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgICAgICB0aGlzLmF0dGFja0NoZXN0QnRuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stY2hlc3QtYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrUkFybUJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLXJhcm0tYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrTEFybUJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLWxhcm0tYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrUkxlZ0J0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLXJsZWctYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrTExlZ0J0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLWxsZWctYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG5cclxuICAgICAgICB0aGlzLmF0dGFja0hlYWRIUEJhciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLWhlYWQtaHAtYmFyXCIpIGFzIEhUTUxTcGFuRWxlbWVudDtcclxuICAgICAgICB0aGlzLmF0dGFja0NoZXN0SFBCYXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImF0dGFjay1jaGVzdC1ocC1iYXJcIikgYXMgSFRNTFNwYW5FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrUkFybUhQQmFyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stcmFybS1ocC1iYXJcIikgYXMgSFRNTFNwYW5FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrTEFybUhQQmFyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stbGFybS1ocC1iYXJcIikgYXMgSFRNTFNwYW5FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrUkxlZ0hQQmFyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stcmxlZy1ocC1iYXJcIikgYXMgSFRNTFNwYW5FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrTExlZ0hQQmFyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stbGxlZy1ocC1iYXJcIikgYXMgSFRNTFNwYW5FbGVtZW50O1xyXG5cclxuICAgICAgICB0aGlzLmF0dGFja0hlYWRIUExibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLWhlYWQtaHAtbGJsXCIpIGFzIEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tDaGVzdEhQTGJsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stY2hlc3QtaHAtbGJsXCIpIGFzIEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tSQXJtSFBMYmwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImF0dGFjay1yYXJtLWhwLWxibFwiKSBhcyBIVE1MTGFiZWxFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrTEFybUhQTGJsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stbGFybS1ocC1sYmxcIikgYXMgSFRNTExhYmVsRWxlbWVudDtcclxuICAgICAgICB0aGlzLmF0dGFja1JMZWdIUExibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLXJsZWctaHAtbGJsXCIpIGFzIEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tMTGVnSFBMYmwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImF0dGFjay1sbGVnLWhwLWxibFwiKSBhcyBIVE1MTGFiZWxFbGVtZW50O1xyXG5cclxuICAgICAgICB0aGlzLmF0dGFja0hlYWRIaXRDaGFuY2VMYmwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImF0dGFjay1oZWFkLWhpdC1jaGFuY2UtbGJsXCIpIGFzIEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tDaGVzdEhpdENoYW5jZUxibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLWNoZXN0LWhpdC1jaGFuY2UtbGJsXCIpIGFzIEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tSQXJtSGl0Q2hhbmNlTGJsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stcmFybS1oaXQtY2hhbmNlLWxibFwiKSBhcyBIVE1MTGFiZWxFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYXR0YWNrTEFybUhpdENoYW5jZUxibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYXR0YWNrLWxhcm0taGl0LWNoYW5jZS1sYmxcIikgYXMgSFRNTExhYmVsRWxlbWVudDtcclxuICAgICAgICB0aGlzLmF0dGFja1JMZWdIaXRDaGFuY2VMYmwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImF0dGFjay1ybGVnLWhpdC1jaGFuY2UtbGJsXCIpIGFzIEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tMTGVnSGl0Q2hhbmNlTGJsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhdHRhY2stbGxlZy1oaXQtY2hhbmNlLWxibFwiKSBhcyBIVE1MTGFiZWxFbGVtZW50O1xyXG5cclxuICAgICAgICB0aGlzLmF0dGFja0NoYXJDYW5jZWxCdG4uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMub25DYW5jZWxCdG5DbGljayk7XHJcblxyXG4gICAgICAgIHRoaXMuYXR0YWNrSGVhZEJ0bi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5vbkF0dGFja0hlYWRCdG5DbGljayk7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tDaGVzdEJ0bi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5vbkF0dGFja0NoZXN0QnRuQ2xpY2spO1xyXG4gICAgICAgIHRoaXMuYXR0YWNrUkFybUJ0bi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5vbkF0dGFja1JBcm1CdG5DbGljayk7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tMQXJtQnRuLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCB0aGlzLm9uQXR0YWNrTEFybUJ0bkNsaWNrKTtcclxuICAgICAgICB0aGlzLmF0dGFja1JMZWdCdG4uYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHRoaXMub25BdHRhY2tSTGVnQnRuQ2xpY2spO1xyXG4gICAgICAgIHRoaXMuYXR0YWNrTExlZ0J0bi5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgdGhpcy5vbkF0dGFja0xMZWdCdG5DbGljayk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzaG93KCkge1xyXG4gICAgICAgIHRoaXMuYXR0YWNrQ2hhckRpdi5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGhpZGUoKSB7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tDaGFyRGl2LnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyByZWZyZXNoRGlzcGxheShwbGF5ZXJJRDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXJJRCA9IHBsYXllcklEO1xyXG4gICAgICAgIHRoaXMuY2FsY0hpdENoYW5jZXMoKTtcclxuICAgICAgICB0aGlzLnNob3coKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNhbGNIaXRDaGFuY2VzKCkge1xyXG4gICAgICAgIGNvbnN0IHAgPSB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJEZXRhaWxzO1xyXG4gICAgICAgIGNvbnN0IHRhcmdldCA9IHRoaXMuY29udHJvbGxlci5wbGF5ZXJEZXRhaWxzW3RoaXMucGxheWVySURdO1xyXG4gICAgICAgIC8vIFRPRE8gLSB0aGlzIHJhbmdlIGRvZXNuJ3QgcmVhbGx5IHdvcmsgd2l0aCB0aGUgaGV4Z3JpZCBzeXN0ZW1cclxuICAgICAgICBjb25zdCByYW5nZVggPSBNYXRoLmFicyhwLnggLSB0YXJnZXQueCk7XHJcbiAgICAgICAgY29uc3QgcmFuZ2VZID0gTWF0aC5hYnMocC55IC0gdGFyZ2V0LnkpO1xyXG4gICAgICAgIGNvbnN0IHJhbmdlRCA9IE1hdGguc3FydChyYW5nZVgqcmFuZ2VYICsgcmFuZ2VZKnJhbmdlWSkgKyAxO1xyXG5cclxuICAgICAgICBjb25zdCBlID0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyRXF1aXBwZWRJdGVtO1xyXG4gICAgICAgIGxldCBkbWdQZXJIaXQ7XHJcbiAgICAgICAgbGV0IHNob3RzUGVyQWN0aW9uO1xyXG4gICAgICAgIGxldCBhY2N1cmFjeTtcclxuXHJcbiAgICAgICAgbGV0IHdlYXBvbkVxdWlwcGVkID0gZmFsc2U7XHJcbiAgICAgICAgaWYoZSAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGd1biA9IHRoaXMuY29udHJvbGxlci5pdGVtc1tlLml0ZW1JRF0gYXMgR3VuO1xyXG4gICAgICAgICAgICBpZihndW4pIHtcclxuICAgICAgICAgICAgICAgIC8vIElmIGEgd2VhcG9uIGlzIGVxdWlwcGVkLCB1c2Ugd2VhcG9uIHN0YXRzXHJcbiAgICAgICAgICAgICAgICBkbWdQZXJIaXQgPSBndW4uZG1nUGVySGl0O1xyXG4gICAgICAgICAgICAgICAgc2hvdHNQZXJBY3Rpb24gPSBndW4uc2hvdHNQZXJBY3Rpb247XHJcbiAgICAgICAgICAgICAgICBhY2N1cmFjeSA9IGd1bi5hY2N1cmFjeTtcclxuICAgICAgICAgICAgICAgIHdlYXBvbkVxdWlwcGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYoIXdlYXBvbkVxdWlwcGVkKSB7XHJcbiAgICAgICAgICAgIC8vIElmIG5vIHdlYXBvbiBlcXVpcHBlZCwgc2ltdWxhdGUgcHVuY2hpbmdcclxuICAgICAgICAgICAgZG1nUGVySGl0ID0gNTtcclxuICAgICAgICAgICAgc2hvdHNQZXJBY3Rpb24gPSAxO1xyXG4gICAgICAgICAgICBhY2N1cmFjeSA9IDAuNjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIENhbGN1bGF0ZSB0aGUgaGl0IGNoYW5jZSBhcyBhIHdob2xlIG51bWJlcmVkIHBlcmNlbnRhZ2VcclxuICAgICAgICBjb25zdCBoaXRDaGFuY2UgPSBNYXRoLmZsb29yKDEwMCAvIHJhbmdlRCAqIGFjY3VyYWN5ICogKDEgKyB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJNb2RpZmllcnMuaGl0Q2hhbmNlLzEwMCkgKyAwLjUpO1xyXG5cclxuICAgICAgICAvLyBEaXNwbGF5IHRoZSBoaXQgY2hhbmNlIGJlbG93IHRoZSBoZWFsdGggYmFyc1xyXG4gICAgICAgIHRoaXMuYXR0YWNrSGVhZEhpdENoYW5jZUxibC5pbm5lckhUTUwgPSBcIlwiK2hpdENoYW5jZStcIiUgVG8gSGl0XCI7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tDaGVzdEhpdENoYW5jZUxibC5pbm5lckhUTUwgPSBcIlwiK2hpdENoYW5jZStcIiUgVG8gSGl0XCI7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tSQXJtSGl0Q2hhbmNlTGJsLmlubmVySFRNTCA9IFwiXCIraGl0Q2hhbmNlK1wiJSBUbyBIaXRcIjtcclxuICAgICAgICB0aGlzLmF0dGFja0xBcm1IaXRDaGFuY2VMYmwuaW5uZXJIVE1MID0gXCJcIitoaXRDaGFuY2UrXCIlIFRvIEhpdFwiO1xyXG4gICAgICAgIHRoaXMuYXR0YWNrUkxlZ0hpdENoYW5jZUxibC5pbm5lckhUTUwgPSBcIlwiK2hpdENoYW5jZStcIiUgVG8gSGl0XCI7XHJcbiAgICAgICAgdGhpcy5hdHRhY2tMTGVnSGl0Q2hhbmNlTGJsLmlubmVySFRNTCA9IFwiXCIraGl0Q2hhbmNlK1wiJSBUbyBIaXRcIjtcclxuXHJcbiAgICAgICAgLy8gRGlzcGxheSB0aGUgcGxheWVyJ3MgbmFtZSBpbiB0aGUgdGl0bGUgbGFiZWxcclxuICAgICAgICB0aGlzLmF0dGFja0NoYXJMYmwuaW5uZXJIVE1MID0gXCJBdHRhY2sgXCIgKyAodGhpcy5jb250cm9sbGVyLnBsYXllclN0dWJzW3RoaXMucGxheWVySURdID9cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIucGxheWVyU3R1YnNbdGhpcy5wbGF5ZXJJRF0uTmFtZSA6IFwiUGxheWVyXCIgKyB0aGlzLnBsYXllcklEKTtcclxuXHJcbiAgICAgICAgLy8gU2V0IHZhbHVlIG9mIEhQIGFzIHBlcmNlbnRhZ2Ugd2lkdGggb2Ygc3BhblxyXG4gICAgICAgIGNvbnN0IGhwID0gdGhpcy5jb250cm9sbGVyLnBsYXllckhQc1t0aGlzLnBsYXllcklEXVxyXG4gICAgICAgIGlmKGhwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXR0YWNrSGVhZEhQQmFyLnN0eWxlLndpZHRoID0gYCR7aHAuaGVhZCA8IDAgPyAwIDogaHAuaGVhZH0lYDtcclxuICAgICAgICAgICAgdGhpcy5hdHRhY2tDaGVzdEhQQmFyLnN0eWxlLndpZHRoID0gYCR7aHAuY2hlc3QgPCAwID8gMCA6IGhwLmNoZXN0fSVgO1xyXG4gICAgICAgICAgICB0aGlzLmF0dGFja1JBcm1IUEJhci5zdHlsZS53aWR0aCA9IGAke2hwLnJhcm0gPCAwID8gMCA6IGhwLnJhcm19JWA7XHJcbiAgICAgICAgICAgIHRoaXMuYXR0YWNrTEFybUhQQmFyLnN0eWxlLndpZHRoID0gYCR7aHAubGFybSA8IDAgPyAwIDogaHAubGFybX0lYDtcclxuICAgICAgICAgICAgdGhpcy5hdHRhY2tSTGVnSFBCYXIuc3R5bGUud2lkdGggPSBgJHtocC5ybGVnIDwgMCA/IDAgOiBocC5ybGVnfSVgO1xyXG4gICAgICAgICAgICB0aGlzLmF0dGFja0xMZWdIUEJhci5zdHlsZS53aWR0aCA9IGAke2hwLmxsZWcgPCAwID8gMCA6IGhwLmxsZWd9JWA7XHJcblxyXG4gICAgICAgICAgICAvLyBTZXQgbGFiZWwgb2YgaHAgYmFyIHRvIGJlIHRoZSB2YWx1ZSBvZiBocFxyXG4gICAgICAgICAgICB0aGlzLmF0dGFja0hlYWRIUExibC5pbm5lckhUTUwgPSBcIlwiICsgKGhwLmhlYWQgPCAwID8gMCA6IGhwLmhlYWQpO1xyXG4gICAgICAgICAgICB0aGlzLmF0dGFja0NoZXN0SFBMYmwuaW5uZXJIVE1MID0gXCJcIiArIChocC5jaGVzdCA8IDAgPyAwIDogaHAuY2hlc3QpO1xyXG4gICAgICAgICAgICB0aGlzLmF0dGFja1JBcm1IUExibC5pbm5lckhUTUwgPSBcIlwiICsgKGhwLnJhcm0gPCAwID8gMCA6IGhwLnJhcm0pO1xyXG4gICAgICAgICAgICB0aGlzLmF0dGFja0xBcm1IUExibC5pbm5lckhUTUwgPSBcIlwiICsgKGhwLmxhcm0gPCAwID8gMCA6IGhwLmxhcm0pO1xyXG4gICAgICAgICAgICB0aGlzLmF0dGFja1JMZWdIUExibC5pbm5lckhUTUwgPSBcIlwiICsgKGhwLnJsZWcgPCAwID8gMCA6IGhwLnJsZWcpO1xyXG4gICAgICAgICAgICB0aGlzLmF0dGFja0xMZWdIUExibC5pbm5lckhUTUwgPSBcIlwiICsgKGhwLmxsZWcgPCAwID8gMCA6IGhwLmxsZWcpO1xyXG5cclxuICAgICAgICAgICAgaWYoaHAuaGVhZCA8PSAzMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hdHRhY2tIZWFkSFBCYXIuY2xhc3NMaXN0LmFkZChcImxpbWItYnRuLWhwLWJhci1pbmp1cmVkXCIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hdHRhY2tIZWFkSFBCYXIuY2xhc3NMaXN0LnJlbW92ZShcImxpbWItYnRuLWhwLWJhci1pbmp1cmVkXCIpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZihocC5jaGVzdCA8PSAzMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hdHRhY2tDaGVzdEhQQmFyLmNsYXNzTGlzdC5hZGQoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXR0YWNrQ2hlc3RIUEJhci5jbGFzc0xpc3QucmVtb3ZlKFwibGltYi1idG4taHAtYmFyLWluanVyZWRcIik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmKGhwLnJhcm0gPD0gMzApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXR0YWNrUkFybUhQQmFyLmNsYXNzTGlzdC5hZGQoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXR0YWNrUkFybUhQQmFyLmNsYXNzTGlzdC5yZW1vdmUoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYoaHAubGFybSA8PSAzMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hdHRhY2tMQXJtSFBCYXIuY2xhc3NMaXN0LmFkZChcImxpbWItYnRuLWhwLWJhci1pbmp1cmVkXCIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hdHRhY2tMQXJtSFBCYXIuY2xhc3NMaXN0LnJlbW92ZShcImxpbWItYnRuLWhwLWJhci1pbmp1cmVkXCIpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZihocC5ybGVnIDw9IDMwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmF0dGFja1JMZWdIUEJhci5jbGFzc0xpc3QuYWRkKFwibGltYi1idG4taHAtYmFyLWluanVyZWRcIik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmF0dGFja1JMZWdIUEJhci5jbGFzc0xpc3QucmVtb3ZlKFwibGltYi1idG4taHAtYmFyLWluanVyZWRcIik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmKGhwLmxsZWcgPD0gMzApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXR0YWNrTExlZ0hQQmFyLmNsYXNzTGlzdC5hZGQoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYXR0YWNrTExlZ0hQQmFyLmNsYXNzTGlzdC5yZW1vdmUoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb25DYW5jZWxCdG5DbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uQXR0YWNrSGVhZEJ0bkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLmNvbnRyb2xsZXIuYXR0YWNrUGxheWVyKHRoaXMucGxheWVySUQsIFwiaGVhZFwiKTtcclxuICAgICAgICB0aGlzLmhpZGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb25BdHRhY2tDaGVzdEJ0bkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLmNvbnRyb2xsZXIuYXR0YWNrUGxheWVyKHRoaXMucGxheWVySUQsIFwiY2hlc3RcIik7XHJcbiAgICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uQXR0YWNrUkFybUJ0bkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLmNvbnRyb2xsZXIuYXR0YWNrUGxheWVyKHRoaXMucGxheWVySUQsIFwicmFybVwiKTtcclxuICAgICAgICB0aGlzLmhpZGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb25BdHRhY2tMQXJtQnRuQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIHRoaXMuY29udHJvbGxlci5hdHRhY2tQbGF5ZXIodGhpcy5wbGF5ZXJJRCwgXCJsYXJtXCIpO1xyXG4gICAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBvbkF0dGFja1JMZWdCdG5DbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLmF0dGFja1BsYXllcih0aGlzLnBsYXllcklELCBcInJsZWdcIik7XHJcbiAgICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uQXR0YWNrTExlZ0J0bkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLmNvbnRyb2xsZXIuYXR0YWNrUGxheWVyKHRoaXMucGxheWVySUQsIFwibGxlZ1wiKTtcclxuICAgICAgICB0aGlzLmhpZGUoKTtcclxuICAgIH1cclxufVxyXG4iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcIi4uL0NvbnRyb2xsZXJcIlxyXG5cclxuZXhwb3J0IGNsYXNzIENlbnRlckxibCB7XHJcbiAgICBwcml2YXRlIGNlbnRlckxibDogSFRNTExhYmVsRWxlbWVudDtcclxuXHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IocHJpdmF0ZSBjb250cm9sbGVyOiBDb250cm9sbGVyKSB7XHJcbiAgICAgICAgdGhpcy5jZW50ZXJMYmwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNlbnRlci1sYmxcIikgYXMgSFRNTExhYmVsRWxlbWVudDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2hvdyhtc2c6IHN0cmluZywgZmFkZUluQW5kT3V0OiBib29sZWFuPWZhbHNlKSB7XHJcbiAgICAgICAgdGhpcy5jZW50ZXJMYmwuaW5uZXJIVE1MID0gbXNnO1xyXG5cclxuICAgICAgICBpZihmYWRlSW5BbmRPdXQpIHtcclxuICAgICAgICAgICAgdGhpcy5jZW50ZXJMYmwuc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XHJcbiAgICAgICAgICAgIHRoaXMuY2VudGVyTGJsLmNsYXNzTGlzdC5yZW1vdmUoXCJmYWRlLWluXCIpO1xyXG4gICAgICAgICAgICB0aGlzLmNlbnRlckxibC5jbGFzc0xpc3QuYWRkKFwiZmFkZS1pbi1vdXRcIik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jZW50ZXJMYmwuc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xyXG4gICAgICAgICAgICB0aGlzLmNlbnRlckxibC5jbGFzc0xpc3QucmVtb3ZlKFwiZmFkZS1pbi1vdXRcIik7XHJcbiAgICAgICAgICAgIHRoaXMuY2VudGVyTGJsLmNsYXNzTGlzdC5hZGQoXCJmYWRlLWluXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaGlkZSgpIHtcclxuICAgICAgICB0aGlzLmNlbnRlckxibC5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIjtcclxuICAgIH1cclxufVxyXG4iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcIi4uL0NvbnRyb2xsZXJcIlxyXG5pbXBvcnQgeyBHYW1lUGhhc2UgfSBmcm9tIFwiLi4vZ2FtZS9HYW1lUGhhc2VcIlxyXG5cclxuZXhwb3J0IGNsYXNzIENoYXJhY3RlclBhbmVsIHtcclxuICAgIHByaXZhdGUgY2hhckRpdjogSFRNTERpdkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGNoYXJMYmw6IEhUTUxMYWJlbEVsZW1lbnQ7XHJcblxyXG4gICAgcHJpdmF0ZSBoZWFkQnRuOiBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgIHByaXZhdGUgY2hlc3RCdG46IEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgcHJpdmF0ZSByQXJtQnRuOiBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgIHByaXZhdGUgbEFybUJ0bjogSFRNTEJ1dHRvbkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIHJMZWdCdG46IEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBsTGVnQnRuOiBIVE1MQnV0dG9uRWxlbWVudDtcclxuXHJcbiAgICBwcml2YXRlIHRvdGFsSFBCYXI6IEhUTUxTcGFuRWxlbWVudDtcclxuICAgIHByaXZhdGUgaGVhZEhQQmFyOiBIVE1MU3BhbkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGNoZXN0SFBCYXI6IEhUTUxTcGFuRWxlbWVudDtcclxuICAgIHByaXZhdGUgcmFybUhQQmFyOiBIVE1MU3BhbkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGxhcm1IUEJhcjogSFRNTFNwYW5FbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBybGVnSFBCYXI6IEhUTUxTcGFuRWxlbWVudDtcclxuICAgIHByaXZhdGUgbGxlZ0hQQmFyOiBIVE1MU3BhbkVsZW1lbnQ7XHJcblxyXG4gICAgcHJpdmF0ZSBoZWFkSFBMYmw6IEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGNoZXN0SFBMYmw6IEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIHJhcm1IUExibDogSFRNTExhYmVsRWxlbWVudDtcclxuICAgIHByaXZhdGUgbGFybUhQTGJsOiBIVE1MTGFiZWxFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBybGVnSFBMYmw6IEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGxsZWdIUExibDogSFRNTExhYmVsRWxlbWVudDtcclxuXHJcbiAgICBwcml2YXRlIGhlYWRCbG9vZFNwbGF0SW1nOiBIVE1MSW1hZ2VFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBjaGVzdEJsb29kU3BsYXRJbWc6IEhUTUxJbWFnZUVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIHJhcm1CbG9vZFNwbGF0SW1nOiBIVE1MSW1hZ2VFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBsYXJtQmxvb2RTcGxhdEltZzogSFRNTEltYWdlRWxlbWVudDtcclxuICAgIHByaXZhdGUgcmxlZ0Jsb29kU3BsYXRJbWc6IEhUTUxJbWFnZUVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGxsZWdCbG9vZFNwbGF0SW1nOiBIVE1MSW1hZ2VFbGVtZW50O1xyXG5cclxuICAgIHByaXZhdGUgYmxvb2RTcGxhdEltZ3M6IEFycmF5PHN0cmluZz4gPSBbXCJhc3NldHMvQmxvb2QtU3BsYXQtMS5wbmdcIiwgXCJhc3NldHMvQmxvb2QtU3BsYXQtMi5wbmdcIiwgXCJhc3NldHMvQmxvb2QtU3BsYXQtMy5wbmdcIl07XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIHRoaXMuY2hhckRpdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2hhci1kaXZcIikgYXMgSFRNTERpdkVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5jaGFyTGJsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjaGFyLWxibFwiKSBhcyBIVE1MTGFiZWxFbGVtZW50O1xyXG5cclxuICAgICAgICB0aGlzLmhlYWRCdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImhlYWQtYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuY2hlc3RCdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNoZXN0LWJ0blwiKSBhcyBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgICAgICB0aGlzLnJBcm1CdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInJhcm0tYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgICAgIHRoaXMubEFybUJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibGFybS1idG5cIikgYXMgSFRNTEJ1dHRvbkVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5yTGVnQnRuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJybGVnLWJ0blwiKSBhcyBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgICAgICB0aGlzLmxMZWdCdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImxsZWctYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG5cclxuICAgICAgICB0aGlzLnRvdGFsSFBCYXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRvdGFsLWhwLWJhclwiKSBhcyBIVE1MU3BhbkVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5oZWFkSFBCYXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImhlYWQtaHAtYmFyXCIpIGFzIEhUTUxTcGFuRWxlbWVudDtcclxuICAgICAgICB0aGlzLmNoZXN0SFBCYXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNoZXN0LWhwLWJhclwiKSBhcyBIVE1MU3BhbkVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5yYXJtSFBCYXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInJhcm0taHAtYmFyXCIpIGFzIEhUTUxTcGFuRWxlbWVudDtcclxuICAgICAgICB0aGlzLmxhcm1IUEJhciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibGFybS1ocC1iYXJcIikgYXMgSFRNTFNwYW5FbGVtZW50O1xyXG4gICAgICAgIHRoaXMucmxlZ0hQQmFyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJybGVnLWhwLWJhclwiKSBhcyBIVE1MU3BhbkVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5sbGVnSFBCYXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImxsZWctaHAtYmFyXCIpIGFzIEhUTUxTcGFuRWxlbWVudDtcclxuXHJcbiAgICAgICAgdGhpcy5oZWFkSFBMYmwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImhlYWQtaHAtbGJsXCIpIGFzIEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5jaGVzdEhQTGJsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjaGVzdC1ocC1sYmxcIikgYXMgSFRNTExhYmVsRWxlbWVudDtcclxuICAgICAgICB0aGlzLnJhcm1IUExibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicmFybS1ocC1sYmxcIikgYXMgSFRNTExhYmVsRWxlbWVudDtcclxuICAgICAgICB0aGlzLmxhcm1IUExibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibGFybS1ocC1sYmxcIikgYXMgSFRNTExhYmVsRWxlbWVudDtcclxuICAgICAgICB0aGlzLnJsZWdIUExibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicmxlZy1ocC1sYmxcIikgYXMgSFRNTExhYmVsRWxlbWVudDtcclxuICAgICAgICB0aGlzLmxsZWdIUExibCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibGxlZy1ocC1sYmxcIikgYXMgSFRNTExhYmVsRWxlbWVudDtcclxuXHJcbiAgICAgICAgdGhpcy5oZWFkQmxvb2RTcGxhdEltZyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaGVhZC1ibG9vZC1zcGxhdC1pbWdcIikgYXMgSFRNTEltYWdlRWxlbWVudDtcclxuICAgICAgICB0aGlzLmNoZXN0Qmxvb2RTcGxhdEltZyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2hlc3QtYmxvb2Qtc3BsYXQtaW1nXCIpIGFzIEhUTUxJbWFnZUVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5yYXJtQmxvb2RTcGxhdEltZyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwicmFybS1ibG9vZC1zcGxhdC1pbWdcIikgYXMgSFRNTEltYWdlRWxlbWVudDtcclxuICAgICAgICB0aGlzLmxhcm1CbG9vZFNwbGF0SW1nID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJsYXJtLWJsb29kLXNwbGF0LWltZ1wiKSBhcyBIVE1MSW1hZ2VFbGVtZW50O1xyXG4gICAgICAgIHRoaXMucmxlZ0Jsb29kU3BsYXRJbWcgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInJsZWctYmxvb2Qtc3BsYXQtaW1nXCIpIGFzIEhUTUxJbWFnZUVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5sbGVnQmxvb2RTcGxhdEltZyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwibGxlZy1ibG9vZC1zcGxhdC1pbWdcIikgYXMgSFRNTEltYWdlRWxlbWVudDtcclxuXHJcbiAgICAgICAgdGhpcy5oZWFkQnRuLmFkZEV2ZW50TGlzdGVuZXIoQ29udHJvbGxlci5DTElDS19FVkVOVCwgdGhpcy5vbkhlYWRCdG5DbGljayk7XHJcbiAgICAgICAgdGhpcy5jaGVzdEJ0bi5hZGRFdmVudExpc3RlbmVyKENvbnRyb2xsZXIuQ0xJQ0tfRVZFTlQsIHRoaXMub25DaGVzdEJ0bkNsaWNrKTtcclxuICAgICAgICB0aGlzLnJBcm1CdG4uYWRkRXZlbnRMaXN0ZW5lcihDb250cm9sbGVyLkNMSUNLX0VWRU5ULCB0aGlzLm9uUkFybUJ0bkNsaWNrKTtcclxuICAgICAgICB0aGlzLmxBcm1CdG4uYWRkRXZlbnRMaXN0ZW5lcihDb250cm9sbGVyLkNMSUNLX0VWRU5ULCB0aGlzLm9uTEFybUJ0bkNsaWNrKTtcclxuICAgICAgICB0aGlzLnJMZWdCdG4uYWRkRXZlbnRMaXN0ZW5lcihDb250cm9sbGVyLkNMSUNLX0VWRU5ULCB0aGlzLm9uUkxlZ0J0bkNsaWNrKTtcclxuICAgICAgICB0aGlzLmxMZWdCdG4uYWRkRXZlbnRMaXN0ZW5lcihDb250cm9sbGVyLkNMSUNLX0VWRU5ULCB0aGlzLm9uTExlZ0J0bkNsaWNrKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcmVmcmVzaERpc3BsYXkoKSB7XHJcbiAgICAgICAgY29uc3QgbXlQbGF5ZXJIUCA9IHRoaXMuY29udHJvbGxlci5teVBsYXllckhQO1xyXG4gICAgICAgIGlmKG15UGxheWVySFApIHtcclxuICAgICAgICAgICAgdGhpcy5jaGFyTGJsLmlubmVySFRNTCA9IGBUb3RhbCBIUCA8c3BhbiBzdHlsZT1cImNvbG9yOiAke215UGxheWVySFAudG90YWwgPD0gMzAgPyBcIiMwMDQwZmZcIiA6IFwiIzAwNDBmZlwifVwiPlxyXG4gICAgICAgICAgICAgICAgICAgICR7bXlQbGF5ZXJIUC50b3RhbCA8IDAgPyAwIDogbXlQbGF5ZXJIUC50b3RhbH08L3NwYW4+YDtcclxuXHJcbiAgICAgICAgICAgIC8vIFNldCB2YWx1ZSBvZiB0b3RhbCBIUCBhcyBwZXJjZW50YWdlIG9mIGhlaWdodFxyXG4gICAgICAgICAgICB0aGlzLnRvdGFsSFBCYXIuc3R5bGUuaGVpZ2h0ID0gYCR7bXlQbGF5ZXJIUC50b3RhbCA8IDAgPyAwIDogbXlQbGF5ZXJIUC50b3RhbH0lYDtcclxuXHJcbiAgICAgICAgICAgIC8vIFNldCB2YWx1ZSBvZiBIUCBhcyBwZXJjZW50YWdlIHdpZHRoIG9mIHNwYW5cclxuICAgICAgICAgICAgdGhpcy5oZWFkSFBCYXIuc3R5bGUud2lkdGggPSBgJHtteVBsYXllckhQLmhlYWQgPCAwID8gMCA6IG15UGxheWVySFAuaGVhZH0lYDtcclxuICAgICAgICAgICAgdGhpcy5jaGVzdEhQQmFyLnN0eWxlLndpZHRoID0gYCR7bXlQbGF5ZXJIUC5jaGVzdCA8IDAgPyAwIDogbXlQbGF5ZXJIUC5jaGVzdH0lYDtcclxuICAgICAgICAgICAgdGhpcy5yYXJtSFBCYXIuc3R5bGUud2lkdGggPSBgJHtteVBsYXllckhQLnJhcm0gPCAwID8gMCA6IG15UGxheWVySFAucmFybX0lYDtcclxuICAgICAgICAgICAgdGhpcy5sYXJtSFBCYXIuc3R5bGUud2lkdGggPSBgJHtteVBsYXllckhQLmxhcm0gPCAwID8gMCA6IG15UGxheWVySFAubGFybX0lYDtcclxuICAgICAgICAgICAgdGhpcy5ybGVnSFBCYXIuc3R5bGUud2lkdGggPSBgJHtteVBsYXllckhQLnJsZWcgPCAwID8gMCA6IG15UGxheWVySFAucmxlZ30lYDtcclxuICAgICAgICAgICAgdGhpcy5sbGVnSFBCYXIuc3R5bGUud2lkdGggPSBgJHtteVBsYXllckhQLmxsZWcgPCAwID8gMCA6IG15UGxheWVySFAubGxlZ30lYDtcclxuXHJcbiAgICAgICAgICAgIC8vIFNldCBsYWJlbCBvZiBocCBiYXIgdG8gYmUgdGhlIHZhbHVlIG9mIGhwXHJcbiAgICAgICAgICAgIHRoaXMuaGVhZEhQTGJsLmlubmVySFRNTCA9IFwiXCIgKyAobXlQbGF5ZXJIUC5oZWFkIDwgMCA/IDAgOiBteVBsYXllckhQLmhlYWQpO1xyXG4gICAgICAgICAgICB0aGlzLmNoZXN0SFBMYmwuaW5uZXJIVE1MID0gXCJcIiArIChteVBsYXllckhQLmNoZXN0IDwgMCA/IDAgOiBteVBsYXllckhQLmNoZXN0KTtcclxuICAgICAgICAgICAgdGhpcy5yYXJtSFBMYmwuaW5uZXJIVE1MID0gXCJcIiArIChteVBsYXllckhQLnJhcm0gPCAwID8gMCA6IG15UGxheWVySFAucmFybSk7XHJcbiAgICAgICAgICAgIHRoaXMubGFybUhQTGJsLmlubmVySFRNTCA9IFwiXCIgKyAobXlQbGF5ZXJIUC5sYXJtIDwgMCA/IDAgOiBteVBsYXllckhQLmxhcm0pO1xyXG4gICAgICAgICAgICB0aGlzLnJsZWdIUExibC5pbm5lckhUTUwgPSBcIlwiICsgKG15UGxheWVySFAucmxlZyA8IDAgPyAwIDogbXlQbGF5ZXJIUC5ybGVnKTtcclxuICAgICAgICAgICAgdGhpcy5sbGVnSFBMYmwuaW5uZXJIVE1MID0gXCJcIiArIChteVBsYXllckhQLmxsZWcgPCAwID8gMCA6IG15UGxheWVySFAubGxlZyk7XHJcblxyXG4gICAgICAgICAgICAvLyBDaGFuZ2UgY29sb3Igb2YgaHAgYmFycyB0byByZWQgaWYgYmVsb3cgMzAlIGhwXHJcbiAgICAgICAgICAgIGlmKG15UGxheWVySFAudG90YWwgPD0gMzApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudG90YWxIUEJhci5jbGFzc0xpc3QuYWRkKFwibGltYi1idG4taHAtYmFyLWluanVyZWRcIik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRvdGFsSFBCYXIuY2xhc3NMaXN0LnJlbW92ZShcImxpbWItYnRuLWhwLWJhci1pbmp1cmVkXCIpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZihteVBsYXllckhQLmhlYWQgPD0gMzApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaGVhZEhQQmFyLmNsYXNzTGlzdC5hZGQoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaGVhZEhQQmFyLmNsYXNzTGlzdC5yZW1vdmUoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYobXlQbGF5ZXJIUC5jaGVzdCA8PSAzMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jaGVzdEhQQmFyLmNsYXNzTGlzdC5hZGQoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2hlc3RIUEJhci5jbGFzc0xpc3QucmVtb3ZlKFwibGltYi1idG4taHAtYmFyLWluanVyZWRcIik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmKG15UGxheWVySFAucmFybSA8PSAzMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yYXJtSFBCYXIuY2xhc3NMaXN0LmFkZChcImxpbWItYnRuLWhwLWJhci1pbmp1cmVkXCIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yYXJtSFBCYXIuY2xhc3NMaXN0LnJlbW92ZShcImxpbWItYnRuLWhwLWJhci1pbmp1cmVkXCIpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZihteVBsYXllckhQLmxhcm0gPD0gMzApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubGFybUhQQmFyLmNsYXNzTGlzdC5hZGQoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubGFybUhQQmFyLmNsYXNzTGlzdC5yZW1vdmUoXCJsaW1iLWJ0bi1ocC1iYXItaW5qdXJlZFwiKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYobXlQbGF5ZXJIUC5ybGVnIDw9IDMwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJsZWdIUEJhci5jbGFzc0xpc3QuYWRkKFwibGltYi1idG4taHAtYmFyLWluanVyZWRcIik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJsZWdIUEJhci5jbGFzc0xpc3QucmVtb3ZlKFwibGltYi1idG4taHAtYmFyLWluanVyZWRcIik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmKG15UGxheWVySFAubGxlZyA8PSAzMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sbGVnSFBCYXIuY2xhc3NMaXN0LmFkZChcImxpbWItYnRuLWhwLWJhci1pbmp1cmVkXCIpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sbGVnSFBCYXIuY2xhc3NMaXN0LnJlbW92ZShcImxpbWItYnRuLWhwLWJhci1pbmp1cmVkXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaG93Qmxvb2RTcGxhdEhlYWQoKSB7XHJcbiAgICAgICAgY29uc3Qgc3BsYXQgPSB0aGlzLmdldFJhbmRvbVNwbGF0KCk7XHJcbiAgICAgICAgdGhpcy5oZWFkQmxvb2RTcGxhdEltZy5zcmMgPSBzcGxhdDtcclxuICAgICAgICB0aGlzLmhlYWRCbG9vZFNwbGF0SW1nLmNsYXNzTGlzdC5yZW1vdmUoXCJmYWRlLW91dFwiKTtcclxuICAgICAgICB0aGlzLmhlYWRCbG9vZFNwbGF0SW1nLmNsYXNzTGlzdC5hZGQoXCJmYWRlLW91dFwiKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2hvd0Jsb29kU3BsYXRDaGVzdCgpIHtcclxuICAgICAgICBjb25zdCBzcGxhdCA9IHRoaXMuZ2V0UmFuZG9tU3BsYXQoKTtcclxuICAgICAgICB0aGlzLmNoZXN0Qmxvb2RTcGxhdEltZy5zcmMgPSBzcGxhdDtcclxuICAgICAgICB0aGlzLmNoZXN0Qmxvb2RTcGxhdEltZy5jbGFzc0xpc3QucmVtb3ZlKFwiZmFkZS1vdXRcIik7XHJcbiAgICAgICAgdGhpcy5jaGVzdEJsb29kU3BsYXRJbWcuY2xhc3NMaXN0LmFkZChcImZhZGUtb3V0XCIpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaG93Qmxvb2RTcGxhdFJBcm0oKSB7XHJcbiAgICAgICAgY29uc3Qgc3BsYXQgPSB0aGlzLmdldFJhbmRvbVNwbGF0KCk7XHJcbiAgICAgICAgdGhpcy5yYXJtQmxvb2RTcGxhdEltZy5zcmMgPSBzcGxhdDtcclxuICAgICAgICB0aGlzLnJhcm1CbG9vZFNwbGF0SW1nLmNsYXNzTGlzdC5yZW1vdmUoXCJmYWRlLW91dFwiKTtcclxuICAgICAgICB0aGlzLnJhcm1CbG9vZFNwbGF0SW1nLmNsYXNzTGlzdC5hZGQoXCJmYWRlLW91dFwiKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2hvd0Jsb29kU3BsYXRMQXJtKCkge1xyXG4gICAgICAgIGNvbnN0IHNwbGF0ID0gdGhpcy5nZXRSYW5kb21TcGxhdCgpO1xyXG4gICAgICAgIHRoaXMubGFybUJsb29kU3BsYXRJbWcuc3JjID0gc3BsYXQ7XHJcbiAgICAgICAgdGhpcy5sYXJtQmxvb2RTcGxhdEltZy5jbGFzc0xpc3QucmVtb3ZlKFwiZmFkZS1vdXRcIik7XHJcbiAgICAgICAgdGhpcy5sYXJtQmxvb2RTcGxhdEltZy5jbGFzc0xpc3QuYWRkKFwiZmFkZS1vdXRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNob3dCbG9vZFNwbGF0UkxlZygpIHtcclxuICAgICAgICBjb25zdCBzcGxhdCA9IHRoaXMuZ2V0UmFuZG9tU3BsYXQoKTtcclxuICAgICAgICB0aGlzLnJsZWdCbG9vZFNwbGF0SW1nLnNyYyA9IHNwbGF0O1xyXG4gICAgICAgIHRoaXMucmxlZ0Jsb29kU3BsYXRJbWcuY2xhc3NMaXN0LnJlbW92ZShcImZhZGUtb3V0XCIpO1xyXG4gICAgICAgIHRoaXMucmxlZ0Jsb29kU3BsYXRJbWcuY2xhc3NMaXN0LmFkZChcImZhZGUtb3V0XCIpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaG93Qmxvb2RTcGxhdExMZWcoKSB7XHJcbiAgICAgICAgY29uc3Qgc3BsYXQgPSB0aGlzLmdldFJhbmRvbVNwbGF0KCk7XHJcbiAgICAgICAgdGhpcy5sbGVnQmxvb2RTcGxhdEltZy5zcmMgPSBzcGxhdDtcclxuICAgICAgICB0aGlzLmxsZWdCbG9vZFNwbGF0SW1nLmNsYXNzTGlzdC5yZW1vdmUoXCJmYWRlLW91dFwiKTtcclxuICAgICAgICB0aGlzLmxsZWdCbG9vZFNwbGF0SW1nLmNsYXNzTGlzdC5hZGQoXCJmYWRlLW91dFwiKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldFJhbmRvbVNwbGF0KCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYmxvb2RTcGxhdEltZ3NbTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogdGhpcy5ibG9vZFNwbGF0SW1ncy5sZW5ndGgpXTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uSGVhZEJ0bkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICBpZih0aGlzLmNvbnRyb2xsZXIuZ2FtZVBoYXNlID09IEdhbWVQaGFzZS5BQ1RJT05fUEhBU0UgJiZcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5teVBsYXllckhQLmhlYWQgPCAxMDAgJiYgdGhpcy5jb250cm9sbGVyLm15UGxheWVySFAuaGVhZCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uSGVhbExpbWJDbGljayhcImhlYWRcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25DaGVzdEJ0bkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICBpZih0aGlzLmNvbnRyb2xsZXIuZ2FtZVBoYXNlID09IEdhbWVQaGFzZS5BQ1RJT05fUEhBU0UgJiZcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5teVBsYXllckhQLmNoZXN0IDwgMTAwICYmIHRoaXMuY29udHJvbGxlci5teVBsYXllckhQLmNoZXN0ID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25IZWFsTGltYkNsaWNrKFwiY2hlc3RcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25SQXJtQnRuQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIGlmKHRoaXMuY29udHJvbGxlci5nYW1lUGhhc2UgPT0gR2FtZVBoYXNlLkFDVElPTl9QSEFTRSAmJlxyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm15UGxheWVySFAucmFybSA8IDEwMCAmJiB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJIUC5yYXJtID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25IZWFsTGltYkNsaWNrKFwicmFybVwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkxBcm1CdG5DbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgaWYodGhpcy5jb250cm9sbGVyLmdhbWVQaGFzZSA9PSBHYW1lUGhhc2UuQUNUSU9OX1BIQVNFICYmXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJIUC5sYXJtIDwgMTAwICYmIHRoaXMuY29udHJvbGxlci5teVBsYXllckhQLmxhcm0gPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5vbkhlYWxMaW1iQ2xpY2soXCJsYXJtXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uUkxlZ0J0bkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICBpZih0aGlzLmNvbnRyb2xsZXIuZ2FtZVBoYXNlID09IEdhbWVQaGFzZS5BQ1RJT05fUEhBU0UgJiZcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5teVBsYXllckhQLnJsZWcgPCAxMDAgJiYgdGhpcy5jb250cm9sbGVyLm15UGxheWVySFAucmxlZyA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uSGVhbExpbWJDbGljayhcInJsZWdcIik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25MTGVnQnRuQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIGlmKHRoaXMuY29udHJvbGxlci5nYW1lUGhhc2UgPT0gR2FtZVBoYXNlLkFDVElPTl9QSEFTRSAmJlxyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm15UGxheWVySFAubGxlZyA8IDEwMCAmJiB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJIUC5sbGVnID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25IZWFsTGltYkNsaWNrKFwibGxlZ1wiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuXHJcbmV4cG9ydCBjbGFzcyBDaGF0V2luZG93IHtcclxuICAgIHByaXZhdGUgY2hhdFdpbmRvdzogSFRNTERpdkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGNoYXRMb2c6IEhUTUxEaXZFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBjaGF0SW5wdXQ6IEhUTUxJbnB1dEVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGNoYXRTZW5kQnRuOiBIVE1MQnV0dG9uRWxlbWVudDtcclxuXHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IocHJpdmF0ZSBjb250cm9sbGVyOiBDb250cm9sbGVyKSB7XHJcbiAgICAgICAgdGhpcy5jaGF0V2luZG93ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjaGF0LXdpbmRvd1wiKSBhcyBIVE1MRGl2RWxlbWVudDtcclxuICAgICAgICB0aGlzLmNoYXRMb2cgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImNoYXQtbG9nXCIpIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuY2hhdElucHV0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjaGF0LWlucHV0XCIpIGFzIEhUTUxJbnB1dEVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5jaGF0U2VuZEJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2hhdC1zZW5kLWJ0blwiKSBhcyBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgICAgICAvL3RoaXMuY2hhdFNlbmRCdG4uYWRkRXZlbnRMaXN0ZW5lcihDb250cm9sbGVyLkNMSUNLX0VWRU5ULCB0aGlzLm9uU2VuZEJ0bkNsaWNrKTtcclxuICAgICAgICB0aGlzLmNoYXRJbnB1dC5hZGRFdmVudExpc3RlbmVyKFwia2V5dXBcIiwgdGhpcy5vbklucHV0RW50ZXJlZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFwcGVuZE1zZyhtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGxldCBsYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwicFwiKTtcclxuICAgICAgICBsYmwuaW5uZXJIVE1MID0gbXNnO1xyXG4gICAgICAgIGxibC5jbGFzc0xpc3QuYWRkKFwiY2hhdC13aW5kb3ctaXRlbVwiKTtcclxuICAgICAgICB0aGlzLmNoYXRMb2cuYXBwZW5kQ2hpbGQobGJsKTtcclxuICAgICAgICAvL3RoaXMuY2hhdFdpbmRvdy5pbnNlcnRCZWZvcmUobGJsLCB0aGlzLmNoYXRXaW5kb3cubGFzdENoaWxkKTtcclxuICAgICAgICAvL3RoaXMuY2hhdFdpbmRvdy5hcHBlbmRDaGlsZChsYmwpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc2VuZE1zZyhtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGlmKG1zZykge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIuc2VuZE1zZyhtc2cpO1xyXG4gICAgICAgICAgICB0aGlzLmNoYXRJbnB1dC52YWx1ZSA9IFwiXCI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25TZW5kQnRuQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2VuZE1zZyh0aGlzLmNoYXRJbnB1dC52YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbklucHV0RW50ZXJlZCA9IChlKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJpbnB1dCBlbnRlcmVkXCIpXHJcbiAgICAgICAgaWYoZS5rZXlDb2RlID09IDEzKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZW50ZXIgZW50ZXJlZFwiKVxyXG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIHRoaXMuc2VuZE1zZyh0aGlzLmNoYXRJbnB1dC52YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCB7IENvbnRyb2xsZXIgfSBmcm9tIFwiLi4vQ29udHJvbGxlclwiXHJcblxyXG5leHBvcnQgY2xhc3MgRHJvcGRvd25EaXYge1xyXG4gICAgcHJpdmF0ZSBkcm9wZG93bkRpdjogSFRNTERpdkVsZW1lbnQ7XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIHRoaXMuZHJvcGRvd25EaXYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImRyb3Bkb3duLWRpdlwiKSBhcyBIVE1MRGl2RWxlbWVudDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaGlkZSgpIHtcclxuICAgICAgICB0aGlzLmRyb3Bkb3duRGl2LnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc2hvdyh4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIC8vIHNldCB0aGUgcG9zaXRpb24gb2YgdGhlIHRvb2x0aXBcclxuICAgICAgICB0aGlzLmRyb3Bkb3duRGl2LnN0eWxlLmxlZnQgPSBgY2FsYygke3h9cHggLSAke3RoaXMuZHJvcGRvd25EaXYuY2xpZW50V2lkdGgvMn1weClgO1xyXG4gICAgICAgIHRoaXMuZHJvcGRvd25EaXYuc3R5bGUudG9wID0gYCR7eX1weGA7XHJcblxyXG4gICAgICAgIC8vIG1ha2Ugc3VyZSB0aGUgdG9vbHRpcCBpcyB2aXNpYmxlXHJcbiAgICAgICAgdGhpcy5kcm9wZG93bkRpdi5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjbGVhcigpIHtcclxuICAgICAgICAvLyBjbGVhciBjb250ZW50cyBvZiBkaXZcclxuICAgICAgICB3aGlsZSh0aGlzLmRyb3Bkb3duRGl2LmZpcnN0Q2hpbGQpIHtcclxuICAgICAgICAgICAgdGhpcy5kcm9wZG93bkRpdi5yZW1vdmVDaGlsZCh0aGlzLmRyb3Bkb3duRGl2LmZpcnN0Q2hpbGQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2hvd1VzZUJhbmRhZ2VMaXN0KHg6IG51bWJlciwgeTogbnVtYmVyLCBlbnRpdHlJRDogbnVtYmVyKSB7XHJcbiAgICAgICAgY29uc3QgaHAgPSB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJIUDtcclxuICAgICAgICBpZih4ID49IDAgJiYgeSA+PSAwICYmIGhwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2xlYXIoKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0ICR0aGlzID0gdGhpcztcclxuICAgICAgICAgICAgZnVuY3Rpb24gb25DbGljayhsaW1iOiBzdHJpbmcpIHtcclxuICAgICAgICAgICAgICAgICR0aGlzLmNvbnRyb2xsZXIudXNlQmFuZGFnZShlbnRpdHlJRCwgbGltYik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHNldCB0aGUgY29udGVudHMgb2YgdGhlIGRyb3Bkb3duXHJcbiAgICAgICAgICAgIGlmKGhwLmhlYWQgPiAwICYmIGhwLmhlYWQgPCAxMDApIHtcclxuICAgICAgICAgICAgICAgIGxldCBidG4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIpO1xyXG4gICAgICAgICAgICAgICAgYnRuLmlubmVySFRNTCA9IFwiQmFuZGFnZSBIZWFkXCI7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRyb3Bkb3duRGl2LmFwcGVuZENoaWxkKGJ0bik7XHJcbiAgICAgICAgICAgICAgICBidG4uYWRkRXZlbnRMaXN0ZW5lcihDb250cm9sbGVyLkNMSUNLX0VWRU5ULCAoZSkgPT4geyBvbkNsaWNrKFwiaGVhZFwiKTsgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYoaHAuY2hlc3QgPiAwICYmIGhwLmNoZXN0IDwgMTAwKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgYnRuID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImJ1dHRvblwiKTtcclxuICAgICAgICAgICAgICAgIGJ0bi5pbm5lckhUTUwgPSBcIkJhbmRhZ2UgQ2hlc3RcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMuZHJvcGRvd25EaXYuYXBwZW5kQ2hpbGQoYnRuKTtcclxuICAgICAgICAgICAgICAgIGJ0bi5hZGRFdmVudExpc3RlbmVyKENvbnRyb2xsZXIuQ0xJQ0tfRVZFTlQsIChlKSA9PiB7IG9uQ2xpY2soXCJjaGVzdFwiKTsgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYoaHAucmFybSA+IDAgJiYgaHAucmFybSA8IDEwMCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IGJ0biA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJidXR0b25cIik7XHJcbiAgICAgICAgICAgICAgICBidG4uaW5uZXJIVE1MID0gXCJCYW5kYWdlIFJpZ2h0IEFybVwiO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kcm9wZG93bkRpdi5hcHBlbmRDaGlsZChidG4pO1xyXG4gICAgICAgICAgICAgICAgYnRuLmFkZEV2ZW50TGlzdGVuZXIoQ29udHJvbGxlci5DTElDS19FVkVOVCwgKGUpID0+IHsgb25DbGljayhcInJhcm1cIik7IH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmKGhwLmxhcm0gPiAwICYmIGhwLmxhcm0gPCAxMDApIHtcclxuICAgICAgICAgICAgICAgIGxldCBidG4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIpO1xyXG4gICAgICAgICAgICAgICAgYnRuLmlubmVySFRNTCA9IFwiQmFuZGFnZSBMZWZ0IEFybVwiO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kcm9wZG93bkRpdi5hcHBlbmRDaGlsZChidG4pO1xyXG4gICAgICAgICAgICAgICAgYnRuLmFkZEV2ZW50TGlzdGVuZXIoQ29udHJvbGxlci5DTElDS19FVkVOVCwgKGUpID0+IHsgb25DbGljayhcImxhcm1cIik7IH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmKGhwLnJsZWcgPiAwICYmIGhwLnJsZWcgPCAxMDApIHtcclxuICAgICAgICAgICAgICAgIGxldCBidG4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIpO1xyXG4gICAgICAgICAgICAgICAgYnRuLmlubmVySFRNTCA9IFwiQmFuZGFnZSBSaWdodCBMZWdcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMuZHJvcGRvd25EaXYuYXBwZW5kQ2hpbGQoYnRuKTtcclxuICAgICAgICAgICAgICAgIGJ0bi5hZGRFdmVudExpc3RlbmVyKENvbnRyb2xsZXIuQ0xJQ0tfRVZFTlQsIChlKSA9PiB7IG9uQ2xpY2soXCJybGVnXCIpOyB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZihocC5sbGVnID4gMCAmJiBocC5sbGVnIDwgMTAwKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgYnRuID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImJ1dHRvblwiKTtcclxuICAgICAgICAgICAgICAgIGJ0bi5pbm5lckhUTUwgPSBcIkJhbmRhZ2UgTGVmdCBMZWdcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMuZHJvcGRvd25EaXYuYXBwZW5kQ2hpbGQoYnRuKTtcclxuICAgICAgICAgICAgICAgIGJ0bi5hZGRFdmVudExpc3RlbmVyKENvbnRyb2xsZXIuQ0xJQ0tfRVZFTlQsIChlKSA9PiB7IG9uQ2xpY2soXCJsbGVnXCIpOyB9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5zaG93KHgsIHkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2hvd1RpbGVBY3Rpb25MaXN0KHg6IG51bWJlciwgeTogbnVtYmVyLCB0aWxlWDogbnVtYmVyLCB0aWxlWTogbnVtYmVyLFxyXG4gICAgICAgICAgICBjYW5TZWFyY2g6IGJvb2xlYW49ZmFsc2UsIGNhbkF0dGFjazogYm9vbGVhbj1mYWxzZSwgY2FuVGhyb3dHcmVuYWRlOiBib29sZWFuPWZhbHNlKSB7XHJcbiAgICAgICAgaWYoeCA+PSAwICYmIHkgPj0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmNsZWFyKCk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCAkdGhpcyA9IHRoaXM7XHJcbiAgICAgICAgICAgIGZ1bmN0aW9uIG9uQXR0YWNrQ2xpY2soKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwbGF5ZXJJRHMgPSAkdGhpcy5jb250cm9sbGVyLmdldFBsYXllcnNPblRpbGUodGlsZVgsIHRpbGVZKTtcclxuICAgICAgICAgICAgICAgIGlmKHBsYXllcklEcy5sZW5ndGggPT09IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBJZiB0aGVyZSBpcyBvbmx5IG9uZSBwbGF5ZXIgb24gdGhlIHRpbGUsIHRoZW4gYXR0YWNrIHRoZSBwbGF5ZXJcclxuICAgICAgICAgICAgICAgICAgICAkdGhpcy5jb250cm9sbGVyLm9uSW5pdGlhdGVBdHRhY2tQbGF5ZXIocGxheWVySURzWzBdKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZihwbGF5ZXJJRHMubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIElmIHRoZXJlIGFyZSBtb3JlIHRoYW4gcGxheWVycyBvbiB0aGUgdGlsZSwgYWxsb3cgbXkgcGxheWVyIHRvIGNob29zZVxyXG4gICAgICAgICAgICAgICAgICAgICR0aGlzLnNob3dBdHRhY2tQbGF5ZXJMaXN0KHgsIHksIHRpbGVYLCB0aWxlWSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHNldCB0aGUgY29udGVudHMgb2YgdGhlIGRyb3Bkb3duXHJcbiAgICAgICAgICAgIGlmKGNhbkF0dGFjaykge1xyXG4gICAgICAgICAgICAgICAgbGV0IGJ0biA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJidXR0b25cIik7XHJcbiAgICAgICAgICAgICAgICBsZXQgaW1nID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImltZ1wiKTtcclxuICAgICAgICAgICAgICAgIGxldCBzcGFuID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInNwYW5cIik7XHJcbiAgICAgICAgICAgICAgICBpbWcuc3JjID0gXCJhc3NldHMvdGFyZ2V0X2ljb24ucG5nXCI7XHJcbiAgICAgICAgICAgICAgICBpbWcuc3R5bGUuZGlzcGxheSA9IFwiaW5saW5lLWJsb2NrXCI7XHJcbiAgICAgICAgICAgICAgICBzcGFuLmlubmVySFRNTCA9IFwiQXR0YWNrXCI7XHJcbiAgICAgICAgICAgICAgICBzcGFuLnN0eWxlLmRpc3BsYXkgPSBcInRhYmxlLWNlbGxcIjtcclxuICAgICAgICAgICAgICAgIGJ0bi5hcHBlbmRDaGlsZChpbWcpO1xyXG4gICAgICAgICAgICAgICAgYnRuLmFwcGVuZENoaWxkKHNwYW4pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kcm9wZG93bkRpdi5hcHBlbmRDaGlsZChidG4pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBzZXQgdGhlIGNvbnRlbnRzIG9mIHRoZSBkcm9wZG93blxyXG4gICAgICAgICAgICBpZihjYW5TZWFyY2gpIHtcclxuICAgICAgICAgICAgICAgIGxldCBidG4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGltZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIik7XHJcbiAgICAgICAgICAgICAgICBsZXQgc3BhbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIpO1xyXG4gICAgICAgICAgICAgICAgaW1nLnNyYyA9IFwiYXNzZXRzL3NlYXJjaF9pY29uLnBuZ1wiO1xyXG4gICAgICAgICAgICAgICAgaW1nLnN0eWxlLmRpc3BsYXkgPSBcImlubGluZS1ibG9ja1wiO1xyXG4gICAgICAgICAgICAgICAgaW1nLnN0eWxlLmhlaWdodCA9IFwiMTAwJVwiO1xyXG4gICAgICAgICAgICAgICAgc3Bhbi5pbm5lckhUTUwgPSBcIlNlYXJjaFwiO1xyXG4gICAgICAgICAgICAgICAgc3Bhbi5zdHlsZS5kaXNwbGF5ID0gXCJpbmxpbmUtYmxvY2tcIjtcclxuICAgICAgICAgICAgICAgIHNwYW4uc3R5bGUuaGVpZ2h0ID0gXCIxMDAlXCI7XHJcbiAgICAgICAgICAgICAgICBzcGFuLnN0eWxlLnZlcnRpY2FsQWxpZ24gPSBcIm1pZGRsZVwiO1xyXG4gICAgICAgICAgICAgICAgYnRuLmFwcGVuZENoaWxkKGltZyk7XHJcbiAgICAgICAgICAgICAgICBidG4uYXBwZW5kQ2hpbGQoc3Bhbik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRyb3Bkb3duRGl2LmFwcGVuZENoaWxkKGJ0bik7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHNldCB0aGUgY29udGVudHMgb2YgdGhlIHRvb2x0aXBcclxuICAgICAgICAgICAgLyppZihjYW5UaHJvd0dyZW5hZGUpIHtcclxuICAgICAgICAgICAgICAgIGxldCBsYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIpO1xyXG4gICAgICAgICAgICAgICAgbGJsLmlubmVySFRNTCA9IFwiR3JlbmFkZVwiXHJcbiAgICAgICAgICAgICAgICB0aGlzLnRpbGVBY3Rpb25MaXN0LmFwcGVuZENoaWxkKGxibCk7XHJcbiAgICAgICAgICAgIH0qL1xyXG5cclxuICAgICAgICAgICAgdGhpcy5zaG93KHgsIHkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2hvd0F0dGFja1BsYXllckxpc3QoeDogbnVtYmVyLCB5OiBudW1iZXIsIHRpbGVYOiBudW1iZXIsIHRpbGVZOiBudW1iZXIpIHtcclxuICAgICAgICBpZih4ID49IDAgJiYgeSA+PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2xlYXIoKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0ICR0aGlzID0gdGhpcztcclxuICAgICAgICAgICAgZnVuY3Rpb24gb25DbGljayhwbGF5ZXJJRDogc3RyaW5nKSB7XHJcbiAgICAgICAgICAgICAgICAkdGhpcy5jb250cm9sbGVyLm9uSW5pdGlhdGVBdHRhY2tQbGF5ZXIocGxheWVySUQpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBwbGF5ZXJzID0gdGhpcy5jb250cm9sbGVyLmdldFBsYXllcnNPblRpbGUodGlsZVgsIHRpbGVZKTtcclxuICAgICAgICAgICAgZm9yKGNvbnN0IHBsYXllcklEIG9mIHBsYXllcnMpIHtcclxuICAgICAgICAgICAgICAgIGxldCBidG4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc3R1YiA9IHRoaXMuY29udHJvbGxlci5wbGF5ZXJTdHVic1twbGF5ZXJJRF07XHJcblxyXG4gICAgICAgICAgICAgICAgaWYoc3R1Yikge1xyXG4gICAgICAgICAgICAgICAgICAgIGJ0bi5pbm5lckhUTUwgPSBzdHViLk5hbWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGJ0bi5pbm5lckhUTUwgPSBcIlBsYXllclwiICsgcGxheWVySUQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5kcm9wZG93bkRpdi5hcHBlbmRDaGlsZChidG4pO1xyXG4gICAgICAgICAgICAgICAgYnRuLmFkZEV2ZW50TGlzdGVuZXIoQ29udHJvbGxlci5DTElDS19FVkVOVCwgKGUpID0+IHsgb25DbGljayhwbGF5ZXJJRCk7IH0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnNob3coeCwgeSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCB7IENvbnRyb2xsZXIgfSBmcm9tIFwiLi4vQ29udHJvbGxlclwiXHJcblxyXG5leHBvcnQgY2xhc3MgRW5kVHVybkJ0biB7XHJcbiAgICBwcml2YXRlIGVuZFR1cm5CdG46IEhUTUxCdXR0b25FbGVtZW50O1xyXG5cclxuICAgIHB1YmxpYyBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbnRyb2xsZXI6IENvbnRyb2xsZXIpIHtcclxuICAgICAgICB0aGlzLmVuZFR1cm5CdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImVuZC10dXJuLWJ0blwiKSBhcyBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgICAgICB0aGlzLmVuZFR1cm5CdG4uYWRkRXZlbnRMaXN0ZW5lcihDb250cm9sbGVyLkNMSUNLX0VWRU5ULCB0aGlzLm9uQ2xpY2spO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBlbmFibGUoKSB7XHJcbiAgICAgICAgdGhpcy5lbmRUdXJuQnRuLmRpc2FibGVkID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLmNvbnRyb2xsZXIuZW5kVHVybigpO1xyXG4gICAgICAgIHRoaXMuZW5kVHVybkJ0bi5kaXNhYmxlZCA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuXHJcbmV4cG9ydCBjbGFzcyBGdWxsT3ZlcmxheSB7XHJcbiAgICBwcml2YXRlIGZ1bGxPdmVybGF5OiBIVE1MRGl2RWxlbWVudDtcclxuXHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IocHJpdmF0ZSBjb250cm9sbGVyOiBDb250cm9sbGVyKSB7XHJcbiAgICAgICAgdGhpcy5mdWxsT3ZlcmxheSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZnVsbC1vdmVybGF5XCIpIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuZnVsbE92ZXJsYXkuYWRkRXZlbnRMaXN0ZW5lcihDb250cm9sbGVyLkNMSUNLX0VWRU5ULCB0aGlzLm9uQ2xpY2spO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoaWRlKCkge1xyXG4gICAgICAgIHRoaXMuZnVsbE92ZXJsYXkuc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNob3coKSB7XHJcbiAgICAgICAgdGhpcy5mdWxsT3ZlcmxheS5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICB0aGlzLmhpZGUoKTtcclxuICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25GdWxsT3ZlcmxheUNsaWNrKCk7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuaW1wb3J0IHsgR2FtZVBoYXNlIH0gZnJvbSBcIi4uL2dhbWUvR2FtZVBoYXNlXCJcclxuaW1wb3J0IHsgUGxheWVyRGV0YWlscyB9IGZyb20gXCIuLi9nYW1lL1BsYXllckRldGFpbHNcIlxyXG5pbXBvcnQgeyBBY3Rpb24sIEFjdGlvbkF0dGFjaywgQWN0aW9uRXF1aXBXZWFwb24sIEFjdGlvbkFwQ29zdHMgfSBmcm9tIFwiLi4vZ2FtZS9BY3Rpb25cIlxyXG5pbXBvcnQgeyBJdGVtRW50aXR5IH0gZnJvbSBcIi4uL2dhbWUvSXRlbVwiXHJcbmltcG9ydCB7IEl0ZW1NZW1lbnRvIH0gZnJvbSBcIi4uL2dhbWUvSXRlbU1lbWVudG9cIlxyXG5pbXBvcnQgeyBNYXBDYW52YXMgfSBmcm9tIFwiLi9NYXBDYW52YXNcIlxyXG5pbXBvcnQgeyBQbGF5ZXJMaXN0IH0gZnJvbSBcIi4vUGxheWVyTGlzdFwiXHJcbmltcG9ydCB7IFRvZ2dsZVJlYWR5QnRuIH0gZnJvbSBcIi4vVG9nZ2xlUmVhZHlCdG5cIlxyXG5pbXBvcnQgeyBUdXJuTGJsIH0gZnJvbSBcIi4vVHVybkxibFwiXHJcbmltcG9ydCB7IEl0ZW1zRm91bmREaXYgfSBmcm9tIFwiLi9JdGVtc0ZvdW5kRGl2XCJcclxuaW1wb3J0IHsgSW52ZW50b3J5UGFuZWwgfSBmcm9tIFwiLi9JbnZlbnRvcnlQYW5lbFwiXHJcbmltcG9ydCB7IE1vZGlmaWVyc0xpc3QgfSBmcm9tIFwiLi9Nb2RpZmllcnNMaXN0XCJcclxuaW1wb3J0IHsgVG9vbHRpcCB9IGZyb20gXCIuL1Rvb2x0aXBcIlxyXG5pbXBvcnQgeyBBY3Rpb25MaXN0IH0gZnJvbSBcIi4vQWN0aW9uTGlzdFwiXHJcbmltcG9ydCB7IENoYXRXaW5kb3cgfSBmcm9tIFwiLi9DaGF0V2luZG93XCJcclxuaW1wb3J0IHsgSXRlbUFjdGlvbkxpc3QgfSBmcm9tIFwiLi9JdGVtQWN0aW9uTGlzdFwiXHJcbmltcG9ydCB7IEF0dGFja0NoYXJEaXYgfSBmcm9tIFwiLi9BdHRhY2tDaGFyRGl2XCJcclxuaW1wb3J0IHsgQ2hhcmFjdGVyUGFuZWwgfSBmcm9tIFwiLi9DaGFyYWN0ZXJQYW5lbFwiXHJcbmltcG9ydCB7IENlbnRlckxibCB9IGZyb20gXCIuL0NlbnRlckxibFwiXHJcbmltcG9ydCB7IFZpZXdwb3J0RGFtYWdlSW5kaWNhdG9yIH0gZnJvbSBcIi4vVmlld3BvcnREYW1hZ2VJbmRpY2F0b3JcIlxyXG5pbXBvcnQgeyBFbmRUdXJuQnRuIH0gZnJvbSBcIi4vRW5kVHVybkJ0blwiXHJcbmltcG9ydCB7IERyb3Bkb3duRGl2fSBmcm9tIFwiLi9Ecm9wZG93bkRpdlwiXHJcbmltcG9ydCB7IEZ1bGxPdmVybGF5IH0gZnJvbSBcIi4vRnVsbE92ZXJsYXlcIlxyXG5pbXBvcnQgeyBSaWdodFBhbmVsTW9iaWxlRGl2IH0gZnJvbSBcIi4vUmlnaHRQYW5lbE1vYmlsZURpdlwiXHJcblxyXG5leHBvcnQgY2xhc3MgR1VJIHtcclxuICAgIHByaXZhdGUgX21hcENhbnZhczogTWFwQ2FudmFzO1xyXG4gICAgcHJpdmF0ZSBfcGxheWVyTGlzdDogUGxheWVyTGlzdDtcclxuICAgIHByaXZhdGUgX3RvZ2dsZVJlYWR5QnRuOiBUb2dnbGVSZWFkeUJ0bjtcclxuICAgIHByaXZhdGUgX3R1cm5MYmw6IFR1cm5MYmw7XHJcbiAgICBwcml2YXRlIF9pdGVtc0ZvdW5kRGl2OiBJdGVtc0ZvdW5kRGl2O1xyXG4gICAgcHJpdmF0ZSBfaW52ZW50UGFuZWw6IEludmVudG9yeVBhbmVsO1xyXG4gICAgcHJpdmF0ZSBfbW9kaWZpZXJzTGlzdDogTW9kaWZpZXJzTGlzdDtcclxuICAgIHByaXZhdGUgX3Rvb2x0aXA6IFRvb2x0aXA7XHJcbiAgICBwcml2YXRlIF9hY3Rpb25MaXN0OiBBY3Rpb25MaXN0O1xyXG4gICAgcHJpdmF0ZSBfY2hhdFdpbmRvdzogQ2hhdFdpbmRvdztcclxuICAgIHByaXZhdGUgX2l0ZW1BY3Rpb25MaXN0OiBJdGVtQWN0aW9uTGlzdDtcclxuICAgIHByaXZhdGUgX2F0dGFja0NoYXJEaXY6IEF0dGFja0NoYXJEaXY7XHJcbiAgICBwcml2YXRlIF9jaGFyYWN0ZXJQYW5lbDogQ2hhcmFjdGVyUGFuZWw7XHJcbiAgICBwcml2YXRlIF9jZW50ZXJMYmw6IENlbnRlckxibDtcclxuICAgIHByaXZhdGUgX3ZpZXdwb3J0RGFtYWdlSW5kaWNhdG9yOiBWaWV3cG9ydERhbWFnZUluZGljYXRvcjtcclxuICAgIHByaXZhdGUgX2VuZFR1cm5CdG46IEVuZFR1cm5CdG47XHJcbiAgICBwcml2YXRlIF9kcm9wZG93bkRpdjogRHJvcGRvd25EaXY7XHJcbiAgICBwcml2YXRlIF9mdWxsT3ZlcmxheTogRnVsbE92ZXJsYXk7XHJcbiAgICBwcml2YXRlIF9yaWdodFBhbmVsTW9iaWxlRGl2OiBSaWdodFBhbmVsTW9iaWxlRGl2O1xyXG5cclxuICAgIHByaXZhdGUgY29uc3RydWN0b3IocHJpdmF0ZSBjb250cm9sbGVyOiBDb250cm9sbGVyKSB7XHJcbiAgICAgICAgdGhpcy5fdG9vbHRpcCA9IG5ldyBUb29sdGlwKGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIHRoaXMuX21hcENhbnZhcyA9IG5ldyBNYXBDYW52YXMoY29udHJvbGxlcik7XHJcbiAgICAgICAgdGhpcy5fcGxheWVyTGlzdCA9IG5ldyBQbGF5ZXJMaXN0KGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIHRoaXMuX3RvZ2dsZVJlYWR5QnRuID0gbmV3IFRvZ2dsZVJlYWR5QnRuKGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIHRoaXMuX3R1cm5MYmwgPSBuZXcgVHVybkxibChjb250cm9sbGVyKTtcclxuICAgICAgICB0aGlzLl9pdGVtc0ZvdW5kRGl2ID0gbmV3IEl0ZW1zRm91bmREaXYoY29udHJvbGxlcik7XHJcbiAgICAgICAgdGhpcy5faW52ZW50UGFuZWwgPSBuZXcgSW52ZW50b3J5UGFuZWwoY29udHJvbGxlcik7XHJcbiAgICAgICAgdGhpcy5fbW9kaWZpZXJzTGlzdCA9IG5ldyBNb2RpZmllcnNMaXN0KGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIHRoaXMuX2FjdGlvbkxpc3QgPSBuZXcgQWN0aW9uTGlzdChjb250cm9sbGVyKTtcclxuICAgICAgICB0aGlzLl9jaGF0V2luZG93ID0gbmV3IENoYXRXaW5kb3coY29udHJvbGxlcik7XHJcbiAgICAgICAgdGhpcy5faXRlbUFjdGlvbkxpc3QgPSBuZXcgSXRlbUFjdGlvbkxpc3QoY29udHJvbGxlcik7XHJcbiAgICAgICAgdGhpcy5fYXR0YWNrQ2hhckRpdiA9IG5ldyBBdHRhY2tDaGFyRGl2KGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIHRoaXMuX2NoYXJhY3RlclBhbmVsID0gbmV3IENoYXJhY3RlclBhbmVsKGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIHRoaXMuX2NlbnRlckxibCA9IG5ldyBDZW50ZXJMYmwoY29udHJvbGxlcik7XHJcbiAgICAgICAgdGhpcy5fdmlld3BvcnREYW1hZ2VJbmRpY2F0b3IgPSBuZXcgVmlld3BvcnREYW1hZ2VJbmRpY2F0b3IoY29udHJvbGxlcik7XHJcbiAgICAgICAgdGhpcy5fZW5kVHVybkJ0biA9IG5ldyBFbmRUdXJuQnRuKGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIHRoaXMuX2Ryb3Bkb3duRGl2ID0gbmV3IERyb3Bkb3duRGl2KGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIHRoaXMuX2Z1bGxPdmVybGF5ID0gbmV3IEZ1bGxPdmVybGF5KGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIHRoaXMuX3JpZ2h0UGFuZWxNb2JpbGVEaXYgPSBuZXcgUmlnaHRQYW5lbE1vYmlsZURpdihjb250cm9sbGVyKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIG5ldyhjb250cm9sbGVyOiBDb250cm9sbGVyKTogR1VJIHtcclxuICAgICAgICBpZihjb250cm9sbGVyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgR1VJKGNvbnRyb2xsZXIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBSZXF1ZXN0IHRoYXQgdGhlIG1hcCBiZSByZWRyYXduXHJcbiAgICBwdWJsaWMgcmVxdWVzdERyYXdNYXAoKSB7XHJcbiAgICAgICAgdGhpcy5fbWFwQ2FudmFzLmRyYXdNYXAoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBSZXF1ZXN0IHRoYXQgdGhlIG1vdmVtZW50IG9mIHBsYXllcnMgYmUgYW5pbWF0ZWQgb24gdGhlIG1hcFxyXG4gICAgcHVibGljIHJlcXVlc3RBbmltYXRlTW92ZW1lbnQob2xkUGxheWVyRGV0YWlsczogeyBbaWQ6IG51bWJlcl06IFBsYXllckRldGFpbHMgfSwgY2FsbGJhY2s6ICgoKSA9PiB2b2lkKSkge1xyXG4gICAgICAgIHRoaXMuX21hcENhbnZhcy5hbmltYXRlTW92ZW1lbnQob2xkUGxheWVyRGV0YWlscywgY2FsbGJhY2spO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJlcXVlc3QgdGhhdCB0aGUgZnVsbCBvdmVybGF5IGJlIGNsb3NlZFxyXG4gICAgcHVibGljIHJlcXVlc3RDbG9zZUZ1bGxPdmVybGF5KCkge1xyXG4gICAgICAgIHRoaXMuX2Z1bGxPdmVybGF5LmhpZGUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBVcGRhdGUgdGltZXIgbGFiZWxcclxuICAgIHB1YmxpYyB1cGRhdGVUaW1lckxhYmVsKHRleHQ6IHN0cmluZywgdGltZUxlZnQ6IG51bWJlcikge1xyXG4gICAgICAgIGNvbnN0IHR4dCA9IHRleHQgKyAodGltZUxlZnQgPiAwID8gXCIgXCIgKyB0aW1lTGVmdCA6IFwiXCIpO1xyXG4gICAgICAgIHRoaXMuX3R1cm5MYmwuc2V0VGl0bGVUZXh0KHR4dCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIGRyb3AgcGhhc2UgaGFzIGJlZ3VuXHJcbiAgICBwdWJsaWMgb25Ecm9wUGhhc2VTdGFydCgpIHtcclxuICAgICAgICB0aGlzLl9lbmRUdXJuQnRuLmVuYWJsZSgpO1xyXG4gICAgICAgIHRoaXMuX2NlbnRlckxibC5zaG93KFwiRHJvcCBQaGFzZVwiLCB0cnVlKTtcclxuICAgICAgICB0aGlzLl90dXJuTGJsLnNldEluc3RydWN0aW9uc1RleHQoXCJDaG9vc2UgYSBEcm9wIExvY2F0aW9uXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGEgbW92ZSBwaGFzZSBoYXMgYmVndW5cclxuICAgIHB1YmxpYyBvbk1vdmVQaGFzZVN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuX2VuZFR1cm5CdG4uZW5hYmxlKCk7XHJcbiAgICAgICAgdGhpcy5fY2VudGVyTGJsLnNob3coXCJNb3ZlIFBoYXNlXCIsIHRydWUpO1xyXG4gICAgICAgIHRoaXMuX2FjdGlvbkxpc3QucmVzZXRBY3Rpb25MaXN0KCk7XHJcbiAgICAgICAgdGhpcy5fYXR0YWNrQ2hhckRpdi5oaWRlKCk7XHJcbiAgICAgICAgdGhpcy5fYWN0aW9uTGlzdC5hcHBlbmRBY3Rpb24oeyBuYW1lOiBcIkNyb3VjaFwiLCBhcENvc3Q6IEFjdGlvbkFwQ29zdHMuQ1JPVUNIIH0sIG51bGwsIG51bGwpO1xyXG4gICAgICAgIHRoaXMuX3R1cm5MYmwuc2V0SW5zdHJ1Y3Rpb25zVGV4dCh0aGlzLmNvbnRyb2xsZXIuc3BlY3RhdGluZyA/XHJcbiAgICAgICAgICAgIFwiU3BlY3RhdGluZyBcIit0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJTdHViLk5hbWUgOiBcIk1vdmUgb3IgQ3JvdWNoXCIpO1xyXG4gICAgICAgIHRoaXMuX2Ryb3Bkb3duRGl2LmhpZGUoKTtcclxuICAgICAgICB0aGlzLl9tYXBDYW52YXMub25Nb3ZlUGhhc2VTdGFydCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGFuIGFjdGlvbiBwaGFzZSBoYXMgYmVndW5cclxuICAgIHB1YmxpYyBvbkFjdGlvblBoYXNlU3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5fZW5kVHVybkJ0bi5lbmFibGUoKTtcclxuICAgICAgICB0aGlzLl9jZW50ZXJMYmwuc2hvdyhcIkFjdGlvbiBQaGFzZVwiLCB0cnVlKTtcclxuICAgICAgICB0aGlzLl9hY3Rpb25MaXN0LnJlc2V0QWN0aW9uTGlzdCgpO1xyXG4gICAgICAgIHRoaXMuX3R1cm5MYmwuc2V0SW5zdHJ1Y3Rpb25zVGV4dCh0aGlzLmNvbnRyb2xsZXIuc3BlY3RhdGluZyA/XHJcbiAgICAgICAgICAgIFwiU3BlY3RhdGluZyBcIit0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJTdHViLk5hbWUgOiBcIlBlcmZvcm0gdXAgdG8gNSBBY3Rpb25zXCIpO1xyXG4gICAgICAgIHRoaXMuX3Rvb2x0aXAuaGlkZVRvb2x0aXAoKTtcclxuICAgICAgICB0aGlzLl9tYXBDYW52YXMub25BY3Rpb25QaGFzZVN0YXJ0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU2V0IHRoZSBjdXJyZW50IGl0ZW0gZm91bmRcclxuICAgIHB1YmxpYyBzZXRJdGVtc0ZvdW5kKGl0ZW1zOiBBcnJheTx7IGVudGl0eUlEOiBzdHJpbmcsIGl0ZW1JRDogc3RyaW5nIH0+KSB7XHJcbiAgICAgICAgdGhpcy5faXRlbXNGb3VuZERpdi5zZXRJdGVtc0ZvdW5kKGl0ZW1zKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgZ2FtZSBpcyBwYXVzZWQgd2hpbHN0IHdhaXRpbmcgZm9yIHNlcnZlclxyXG4gICAgcHVibGljIG9uV2FpdGluZ0ZvclNlcnZlcigpIHtcclxuICAgICAgICB0aGlzLl9jZW50ZXJMYmwuc2hvdyhcIldhaXRpbmcgZm9yIFNlcnZlciA8ZGl2IGNsYXNzPSdzcGlubmVyJz48L2Rpdj5cIik7XHJcbiAgICAgICAgdGhpcy5fbWFwQ2FudmFzLm9uV2FpdGluZ0ZvclNlcnZlcigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBtYXAgaXMgZnVsbHkgbG9hZGVkIGFuZCBpbml0aWFsaXNlZFxyXG4gICAgcHVibGljIG9uTWFwTG9hZGVkKCkge1xyXG4gICAgICAgIHRoaXMuX21hcENhbnZhcy5vbk1hcExvYWRlZCgpO1xyXG4gICAgICAgIHRoaXMuX3RvZ2dsZVJlYWR5QnRuLmVuYWJsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBwbGF5ZXIgc3R1YnMgbWFwIGlzIHVwZGF0ZWRcclxuICAgIHB1YmxpYyBvblBsYXllclN0dWJzVXBkYXRlZCgpIHtcclxuICAgICAgICB0aGlzLl9wbGF5ZXJMaXN0LnJlZnJlc2hEaXNwbGF5KCk7XHJcbiAgICAgICAgLy90aGlzLl90b2dnbGVSZWFkeUJ0bi51cGRhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIGRldGFpbHMgbWFwIGlzIHVwZGF0ZWRcclxuICAgIHB1YmxpYyBvbk15UGxheWVyRGV0YWlsc1VwZGF0ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5fbWFwQ2FudmFzLm9uTXlQbGF5ZXJEZXRhaWxzVXBkYXRlZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE9uIHBsYXllciBtb2RpZmllcnMgdXBkYXRlXHJcbiAgICBwdWJsaWMgb25NeVBsYXllck1vZGlmaWVyc1VwZGF0ZWQoKSB7XHJcbiAgICAgICAgdGhpcy5fbW9kaWZpZXJzTGlzdC5yZWZyZXNoRGlzcGxheSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE9uIHBsYXllciBocCB1cGRhdGVcclxuICAgIHB1YmxpYyBvbk15UGxheWVySFBVcGRhdGVkKHRvdGFsSFBDaGFuZ2VkQnk6IG51bWJlciwgaGVhZEluanVyZWQ6IGJvb2xlYW4sIGNoZXN0SW5qdXJlZDogYm9vbGVhbixcclxuICAgICAgICAgICAgcmFybUluanVyZWQ6IGJvb2xlYW4sIGxhcm1Jbmp1cmVkOiBib29sZWFuLCBybGVnSW5qdXJlZDogYm9vbGVhbixcclxuICAgICAgICAgICAgbGxlZ0luanVyZWQ6IGJvb2xlYW4pIHtcclxuICAgICAgICB0aGlzLl9jaGFyYWN0ZXJQYW5lbC5yZWZyZXNoRGlzcGxheSgpO1xyXG4gICAgICAgIGlmKHRvdGFsSFBDaGFuZ2VkQnkgPCAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3ZpZXdwb3J0RGFtYWdlSW5kaWNhdG9yLnJlZnJlc2hEaXNwbGF5KHRvdGFsSFBDaGFuZ2VkQnkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZihoZWFkSW5qdXJlZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9jaGFyYWN0ZXJQYW5lbC5zaG93Qmxvb2RTcGxhdEhlYWQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYoY2hlc3RJbmp1cmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NoYXJhY3RlclBhbmVsLnNob3dCbG9vZFNwbGF0Q2hlc3QoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYocmFybUluanVyZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5fY2hhcmFjdGVyUGFuZWwuc2hvd0Jsb29kU3BsYXRSQXJtKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmKGxhcm1Jbmp1cmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NoYXJhY3RlclBhbmVsLnNob3dCbG9vZFNwbGF0TEFybSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZihybGVnSW5qdXJlZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9jaGFyYWN0ZXJQYW5lbC5zaG93Qmxvb2RTcGxhdFJMZWcoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYobGxlZ0luanVyZWQpIHtcclxuICAgICAgICAgICAgdGhpcy5fY2hhcmFjdGVyUGFuZWwuc2hvd0Jsb29kU3BsYXRMTGVnKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGEgbW92ZSBoYXMgYmVlbiBkZWVtZWQgdmFsaWRcclxuICAgIHB1YmxpYyBvbk1vdmVWYWxpZChuZXdYOiBudW1iZXIsIG5ld1k6IG51bWJlciwgY3JvdWNoOiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5fYWN0aW9uTGlzdC5yZXNldEFjdGlvbkxpc3QoKTtcclxuICAgICAgICBpZihjcm91Y2gpIHtcclxuICAgICAgICAgICAgdGhpcy5fYWN0aW9uTGlzdC5hcHBlbmRBY3Rpb24oeyBuYW1lOiBcIkNyb3VjaFwiLCBhcENvc3Q6IEFjdGlvbkFwQ29zdHMuQ1JPVUNIIH0sIHRoaXMuY29udHJvbGxlci5nbG9iYWxNb3VzZVgsIHRoaXMuY29udHJvbGxlci5nbG9iYWxNb3VzZVkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2FjdGlvbkxpc3QuYXBwZW5kQWN0aW9uKHsgbmFtZTogXCJNb3ZlXCIsIGFwQ29zdDogKEFjdGlvbkFwQ29zdHMuTU9WRSArXHJcbiAgICAgICAgICAgICAgICAodGhpcy5jb250cm9sbGVyLm15UGxheWVyTW9kaWZpZXJzLmFwTW92ZUNvc3QgIT0gbnVsbCA/IHRoaXMuY29udHJvbGxlci5teVBsYXllck1vZGlmaWVycy5hcE1vdmVDb3N0IDogMCkpIH0sIG51bGwsIG51bGwpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLl9tYXBDYW52YXMub25Nb3ZlVmFsaWQobmV3WCwgbmV3WSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gYW4gYXR0YWNrIGhhcyBiZWVuIGRlZW1lZCB2YWxpZFxyXG4gICAgcHVibGljIG9uQXR0YWNrVmFsaWQoYWN0aW9uOiBBY3Rpb25BdHRhY2spIHtcclxuICAgICAgICB0aGlzLl9hY3Rpb25MaXN0LmFwcGVuZEFjdGlvbihhY3Rpb24sIG51bGwsIG51bGwpO1xyXG4gICAgICAgIHRoaXMuX21hcENhbnZhcy5vbkF0dGFja1ZhbGlkKGFjdGlvbi5wbGF5ZXJJRCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gYSBzZWFyY2ggaGFzIGJlZW4gZGVlbWVkIHZhbGlkXHJcbiAgICBwdWJsaWMgb25TZWFyY2hWYWxpZChhY3Rpb246IEFjdGlvbikge1xyXG4gICAgICAgIHRoaXMuX2FjdGlvbkxpc3QuYXBwZW5kQWN0aW9uKGFjdGlvbiwgbnVsbCwgbnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHNlcnZlciBoYXMgdmFsaWRpZmllZCBhIHRha2UgaXRlbSByZXF1ZXN0XHJcbiAgICBwdWJsaWMgb25UYWtlSXRlbVZhbGlkKGVudGl0eUlEOiBudW1iZXIsIGl0ZW1JRDogbnVtYmVyLCBmaXJzdFNsb3RYOiBudW1iZXIsIGZpcnN0U2xvdFk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2ludmVudFBhbmVsLm9uSXRlbVRha2VuKGVudGl0eUlELCBpdGVtSUQsIGZpcnN0U2xvdFgsIGZpcnN0U2xvdFkpO1xyXG4gICAgICAgIHRoaXMuX2l0ZW1zRm91bmREaXYucmVtb3ZlSXRlbShlbnRpdHlJRCwgaXRlbUlEKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgc2VydmVyIGhhcyB2YWxpZGlmaWVkIGFuIGVxdWlwIGl0ZW0gcmVxdWVzdFxyXG4gICAgcHVibGljIG9uRXF1aXBJdGVtVmFsaWQoYWN0aW9uOiBBY3Rpb25FcXVpcFdlYXBvbikge1xyXG4gICAgICAgIHRoaXMuX2FjdGlvbkxpc3QuYXBwZW5kQWN0aW9uKGFjdGlvbiwgbnVsbCwgbnVsbCk7XHJcbiAgICAgICAgdGhpcy5faW52ZW50UGFuZWwucmVzZXRTbG90U3R5bGUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgc2VydmVyIGhhcyB2YWxpZGlmZWQgYSBkcm9wIGl0ZW0gcmVxdWVzdFxyXG4gICAgcHVibGljIG9uRHJvcEl0ZW1WYWxpZChlbnRpdHlJRDogbnVtYmVyLCBpdGVtSUQ6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2l0ZW1zRm91bmREaXYuYWRkSXRlbShlbnRpdHlJRCwgaXRlbUlEKTtcclxuICAgICAgICB0aGlzLl9pbnZlbnRQYW5lbC5yZW1vdmVGcm9tQmFja3BhY2soZW50aXR5SUQsIGl0ZW1JRCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHNlcnZlciBoYXMgdmFsaWRpZmllZCBhIHVzZSBiYW5kYWdlIHJlcXVlc3RcclxuICAgIHB1YmxpYyBvblVzZUJhbmRhZ2VWYWxpZChlbnRpdHlJRDogbnVtYmVyLCBpdGVtSUQ6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2ludmVudFBhbmVsLnJlbW92ZUZyb21CYWNrcGFjayhlbnRpdHlJRCwgaXRlbUlEKTtcclxuICAgICAgICB0aGlzLl9hY3Rpb25MaXN0LmFwcGVuZEFjdGlvbih7IG5hbWU6IFwiQmFuZGFnZVwiLCBhcENvc3Q6IEFjdGlvbkFwQ29zdHMuQkFOREFHRSB9LCBudWxsLCBudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiBhIHBsYXllcnMgcmVhZHkgc3RhdGUgaXMgY2hhbmdlZFxyXG4gICAgcHVibGljIG9uUmVhZHlTdGF0ZVVwZGF0ZShwbGF5ZXJJRDogc3RyaW5nLCByZWFkeVN0YXRlOiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5fcGxheWVyTGlzdC51cGRhdGVMaXN0SXRlbShwbGF5ZXJJRCwgcmVhZHlTdGF0ZSk7XHJcbiAgICAgICAgLy90aGlzLl90b2dnbGVSZWFkeUJ0bi51cGRhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgZ2FtZSBsZWF2ZXMgdGhlIGxvYmJ5IHBoYXNlIGFuZCBlbnRlcnMgdGhlIGRyb3AgcGhhc2VcclxuICAgIHB1YmxpYyBvbkdhbWVTdGFydGVkKCkge1xyXG4gICAgICAgIHRoaXMuX3RvZ2dsZVJlYWR5QnRuLmRpc2FibGUoKTtcclxuICAgICAgICB0aGlzLl90b2dnbGVSZWFkeUJ0bi5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIGF0IGVuZCBvZiB0dXJuIGlmIHBsYXllcnMgaGF2ZSBkaWVkXHJcbiAgICBwdWJsaWMgb25QbGF5ZXJEZWFkKHBsYXllck5hbWU6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX2NoYXRXaW5kb3cuYXBwZW5kTXNnKGA8Yj48c3BhbiBzdHlsZT1cImNvbG9yOiAjMDAwMGU1O1wiPiR7cGxheWVyTmFtZX08L3NwYW4+PC9iPiBoYXMgZGllZGApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCBhbG9uZyB3aXRoIG9uUGxheWVyRGVhZCBpZiB0aGUgcGxheWVyIGlzIG15UGxheWVyXHJcbiAgICBwdWJsaWMgb25NeVBsYXllckRlYWQoKSB7XHJcbiAgICAgICAgLy8gU2hvdyB5b3UgZGllZCBtZXNzYWdlIGFuZCBoaWRlIGFsbCBnYW1lcGxheSBvbmx5IGNvbnRlbnRcclxuICAgICAgICB0aGlzLl9jZW50ZXJMYmwuc2hvdyhcIllvdSBEaWVkXCIpO1xyXG4gICAgICAgIHRoaXMuX3JpZ2h0UGFuZWxNb2JpbGVEaXYuaGlkZSgpO1xyXG4gICAgICAgIHRoaXMuX21hcENhbnZhcy5kaXNhYmxlKCk7XHJcbiAgICAgICAgdGhpcy5fYWN0aW9uTGlzdC5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIGdhbWUgaXMgb3ZlclxyXG4gICAgcHVibGljIG9uR2FtZU92ZXIod2lubmVySURzOiBBcnJheTxudW1iZXI+LCBkaWRNeVBsYXllcldpbjogYm9vbGVhbiwgZGlkTXlQbGF5ZXJEcmF3OiBib29sZWFuKSB7XHJcbiAgICAgICAgLy8gRGlzcGxheSBtc2cgc3BlY2lmaWMgdG8gbXkgcGxheWVyXHJcbiAgICAgICAgaWYoZGlkTXlQbGF5ZXJEcmF3KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NlbnRlckxibC5zaG93KFwiRHJhd1wiKTtcclxuICAgICAgICB9IGVsc2UgaWYoZGlkTXlQbGF5ZXJXaW4pIHtcclxuICAgICAgICAgICAgdGhpcy5fY2VudGVyTGJsLnNob3coXCJWaWN0b3J5XCIpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2NlbnRlckxibC5zaG93KFwiRGVmZWF0XCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gRGlzcGxheSB3aG8gd29uIGluIHRoZSBraWxsIGZlZWRcclxuICAgICAgICBsZXQgd2luTXNnID0gXCJcIjtcclxuICAgICAgICBmb3IoY29uc3Qgd2lubmVySUQgb2Ygd2lubmVySURzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBsYXllck5hbWUgPSB0aGlzLmNvbnRyb2xsZXIucGxheWVyU3R1YnNbd2lubmVySURdID9cclxuICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5wbGF5ZXJTdHVic1t3aW5uZXJJRF0uTmFtZSA6IFwiUGxheWVyXCIgKyB3aW5uZXJJRDtcclxuICAgICAgICAgICAgd2luTXNnICs9IGA8Yj48c3BhbiBzdHlsZT1cImNvbG9yOiAjMDAwMGU1O1wiPiR7cGxheWVyTmFtZX08L3NwYW4+PC9iPiwgYDtcclxuICAgICAgICB9XHJcbiAgICAgICAgd2luTXNnID0gd2luTXNnLnNsaWNlKDAsIC0yKTtcclxuICAgICAgICB3aW5Nc2cgKz0gKHdpbm5lcklEcy5sZW5ndGggPT09IDEgPyBcIiBpc1wiIDogXCIgYXJlXCIpICsgXCIgdmljdG9yaW91c1wiO1xyXG4gICAgICAgIHRoaXMuX2NoYXRXaW5kb3cuYXBwZW5kTXNnKHdpbk1zZyk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gYSBzdG9ybSBhcHByb2FjaCB1cGRhdGUgaXMgcmVjZWl2ZWQgZnJvbSBzZXJ2ZXJcclxuICAgIHB1YmxpYyBvblN0b3JtQXBwcm9hY2hVcGRhdGUoKSB7XHJcbiAgICAgICAgdGhpcy5fY2VudGVyTGJsLnNob3coXCJOdWNsZWFyIE1lbHRkb3duIEltbWluZW50XCIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGEgc3Rvcm0gcmFkaXVzIHVwZGF0ZSBpcyByZWNlaXZlZCBmcm9tIHNlcnZlclxyXG4gICAgcHVibGljIG9uU3Rvcm1SYWRpdXNVcGRhdGUoKSB7XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLnJlcXVlc3REcmF3TWFwKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gYSBzdG9ybSBkbWcgdXBkYXRlIGlzIHJlY2VpdmVkIGZyb20gc2VydmVyXHJcbiAgICBwdWJsaWMgb25TdG9ybURtZ1VwZGF0ZSgpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBsYXllciBzdGFydHMgZHJhZ2dpbmcgYW4gaXRlbSBpbiB0aGVpciBpbnZlbnRvcnlcclxuICAgIHB1YmxpYyBvbkl0ZW1EcmFnU3RhcnQoKSB7XHJcbiAgICAgICAgdGhpcy5faXRlbXNGb3VuZERpdi5lbmFibGVEcm9wTW9kZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBwbGF5ZXIgc3RvcHMgZHJhZ2dpbmcgYW4gaXRlbSBpbiB0aGVpciBpbnZlbnRvcnlcclxuICAgIHB1YmxpYyBvbkl0ZW1EcmFnRW5kKCkge1xyXG4gICAgICAgIHRoaXMuX2l0ZW1zRm91bmREaXYuZGlzYWJsZURyb3BNb2RlKCk7XHJcbiAgICAgICAgdGhpcy5faW52ZW50UGFuZWwub25JdGVtRHJhZ0VuZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBwbGF5ZXIgYmVnaW5zIGhvdmVyaW5nIG92ZXIgYSBuZXcgdGlsZSBkdXJpbmcgdGhlIGRyb3AgcGhhc2VcclxuICAgIHB1YmxpYyBvblRpbGVIb3ZlckRyb3BQaGFzZShpOiBudW1iZXIsIGo6IG51bWJlciwgeDogbnVtYmVyLCB5OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl90b29sdGlwLnNob3dEcm9wVGlsZVRvb2x0aXAoaSwgaiwgeCwgeSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBsYXllciBiZWdpbnMgaG92ZXJpbmcgb3ZlciBhIG5ldyB0aWxlIGR1cmluZyB0aGUgbW92ZSBwaGFzZVxyXG4gICAgcHVibGljIG9uVGlsZUhvdmVyTW92ZVBoYXNlKGk6IG51bWJlciwgajogbnVtYmVyLCB4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX3Rvb2x0aXAuc2hvd1RpbGVUb29sdGlwKGksIGosIHgsIHkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBwbGF5ZXIgYmVnaW5zIGhvdmVyaW5nIG92ZXIgYSBuZXcgdGlsZSBkdXJpbmcgdGhlIGFjdGlvbiBwaGFzZVxyXG4gICAgcHVibGljIG9uVGlsZUhvdmVyQWN0aW9uUGhhc2UobmFtZTogc3RyaW5nLCBkZXNjOiBzdHJpbmcsIGFwQ29zdDogbnVtYmVyLCB4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX3Rvb2x0aXAuc2hvd0FjdGlvblRvb2x0aXAobmFtZSwgZGVzYywgYXBDb3N0LCB4LCB5KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIGNsaWNrcyBhIHRpbGUgZHVyaW5nIHRoZSBhY3Rpb24gcGhhc2VcclxuICAgIHB1YmxpYyBvblRpbGVDbGlja2VkQWN0aW9uUGhhc2UoeDogbnVtYmVyLCB5OiBudW1iZXIsIHRpbGVYOiBudW1iZXIsXHJcbiAgICAgICAgICAgIHRpbGVZOiBudW1iZXIsIGNhblNlYXJjaDogYm9vbGVhbiwgY2FuQXR0YWNrOiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5fZHJvcGRvd25EaXYuc2hvd1RpbGVBY3Rpb25MaXN0KHgsIHksIHRpbGVYLCB0aWxlWSwgY2FuU2VhcmNoLCBjYW5BdHRhY2ssIHRydWUpO1xyXG4gICAgICAgIHRoaXMuX3Rvb2x0aXAuaGlkZVRvb2x0aXAoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIGhvdmVycyBhbiBpdGVtIG1lbWVudG9cclxuICAgIHB1YmxpYyBvbkl0ZW1NZW1lbnRvSG92ZXIobWVtOiBJdGVtTWVtZW50bywgeDogbnVtYmVyLCB5OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl90b29sdGlwLnNob3dJdGVtTWVtZW50b1Rvb2x0aXAobWVtLCB4LCB5KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgcGxheWVyIHdhbnRzIHRvIGF0dGFjayBhIHBsYXllciBvbiBhIHRpbGVcclxuICAgIHB1YmxpYyBvbkF0dGFja1BsYXllck9uVGlsZSh4OiBudW1iZXIsIHk6IG51bWJlciwgdGlsZVg6IG51bWJlciwgdGlsZVk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX2Ryb3Bkb3duRGl2LnNob3dBdHRhY2tQbGF5ZXJMaXN0KHgsIHksIHRpbGVYLCB0aWxlWSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBsYXllciBiZWdpbnMgaG92ZXJpbmcgb3ZlciBhbiBpdGVtXHJcbiAgICBwdWJsaWMgb25JdGVtSG92ZXIoZW50aXR5OiBJdGVtRW50aXR5LCB4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuX3Rvb2x0aXAuc2hvd0l0ZW1Ub29sdGlwKGVudGl0eSwgeCwgeSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBsYXllciBsZWZ0IG9yIHJpZ2h0IGNsaWNrcyBhIGJhbmRhZ2UgaW4gdGhlaXIgaW52ZW50b3J5XHJcbiAgICBwdWJsaWMgb25CYW5kYWdlQ2xpY2soeDogbnVtYmVyLCB5OiBudW1iZXIsIGVudGl0eUlEOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLl9kcm9wZG93bkRpdi5zaG93VXNlQmFuZGFnZUxpc3QoeCwgeSwgZW50aXR5SUQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBwbGF5IGNsaWNrcyBhIGxpbWIgYnV0dG9uXHJcbiAgICBwdWJsaWMgb25IZWFsTGltYkNsaWNrKGxpbWI6IHN0cmluZykge1xyXG4gICAgICAgIC8vIEVuc3VyZSBwbGF5ZXIgaGFzIGJhbmRhZ2UgaW4gdGhlaXIgaW52ZW50b3J5XHJcbiAgICAgICAgaWYodGhpcy5faW52ZW50UGFuZWwuY29udGFpbnNCYW5kYWdlKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5fZnVsbE92ZXJsYXkuc2hvdygpO1xyXG4gICAgICAgICAgICB0aGlzLl9pbnZlbnRQYW5lbC5oaWdobGlnaHRCYW5kYWdlcyhsaW1iKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIHBsYXllciBjbGlja3MgdGhlIGZ1bGwgc2NyZWVuIG92ZXJsYXlcclxuICAgIHB1YmxpYyBvbkZ1bGxPdmVybGF5Q2xpY2soKSB7XHJcbiAgICAgICAgdGhpcy5faW52ZW50UGFuZWwudW5IaWdobGlnaHRJdGVtcygpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBwbGF5ZXIgaW5pdGlhdGVzIGFuIGF0dGFjayBvbiBhbm90aGVyIHBsYXllclxyXG4gICAgcHVibGljIG9uSW5pdGlhdGVBdHRhY2tQbGF5ZXIocGxheWVySUQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX2F0dGFja0NoYXJEaXYucmVmcmVzaERpc3BsYXkocGxheWVySUQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGEgcGxheWVyIGRpc2Nvbm5lY3RzIGZyb20gdGhlIGdhbWVcclxuICAgIHB1YmxpYyBvblBsYXllckRpc2Nvbm5lY3QocGxheWVyTmFtZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5fcGxheWVyTGlzdC5yZWZyZXNoRGlzcGxheSgpO1xyXG4gICAgICAgIHRoaXMuX2NoYXRXaW5kb3cuYXBwZW5kTXNnKGA8Yj48c3BhbiBzdHlsZT1cImNvbG9yOiAjMDAwMGU1O1wiPiR7cGxheWVyTmFtZX08L3NwYW4+PC9iPiBoYXMgZGlzY29ubmVjdGVkYCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHVwb24gcmVjZWl2aW5nIGEgbXNnIGZyb20gYW5vdGhlciBwbGF5ZXJcclxuICAgIHB1YmxpYyBvbk1zZ1JlY2VpdmVkKHBsYXllck5hbWU6IHN0cmluZywgbXNnOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLl9jaGF0V2luZG93LmFwcGVuZE1zZyhgPGI+PHNwYW4gc3R5bGU9XCJjb2xvcjogIzAwMDBlNTtcIj4ke3BsYXllck5hbWV9Ojwvc3Bhbj48L2I+ICR7bXNnfWApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBvbldpbmRvd1Jlc2l6ZSgpIHtcclxuICAgICAgICB0aGlzLl9tYXBDYW52YXMucmVzaXplKCk7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuaW1wb3J0IHsgR2FtZVBoYXNlIH0gZnJvbSBcIi4uL2dhbWUvR2FtZVBoYXNlXCJcclxuaW1wb3J0IHsgSXRlbSwgSXRlbUVudGl0eSwgaXNCYW5kYWdlLCBpc0d1biB9IGZyb20gXCIuLi9nYW1lL0l0ZW1cIlxyXG5cclxuZXhwb3J0IGNsYXNzIEludmVudG9yeVBhbmVsIHtcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgcm93TGVuZ3RoID0gMztcclxuICAgIHByaXZhdGUgcmVhZG9ubHkgY29sSGVpZ2h0ID0gNDtcclxuICAgIHByaXZhdGUgaXRlbVNsb3RzOiBBcnJheTxBcnJheTxIVE1MRGl2RWxlbWVudD4+O1xyXG5cclxuICAgIHByaXZhdGUgbGltYlNlbGVjdGVkOiBzdHJpbmc7XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIHRoaXMuaXRlbVNsb3RzID0gbmV3IEFycmF5PEFycmF5PEhUTUxEaXZFbGVtZW50Pj4oKTtcclxuICAgICAgICBmb3IobGV0IGogPSAwOyBqIDwgdGhpcy5jb2xIZWlnaHQ7IGorKykge1xyXG4gICAgICAgICAgICB0aGlzLml0ZW1TbG90cy5wdXNoKG5ldyBBcnJheTxIVE1MRGl2RWxlbWVudD4oKSk7XHJcbiAgICAgICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLnJvd0xlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBpdGVtU2xvdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGBpbnZlbnQtc2xvdC0ke2krKGoqMyl9YCkgYXMgSFRNTERpdkVsZW1lbnQ7XHJcbiAgICAgICAgICAgICAgICBpdGVtU2xvdC5hZGRFdmVudExpc3RlbmVyKFwiZHJhZ2VudGVyXCIsIHRoaXMub25EcmFnRW50ZXIpO1xyXG4gICAgICAgICAgICAgICAgaXRlbVNsb3QuYWRkRXZlbnRMaXN0ZW5lcihcImRyYWdsZWF2ZVwiLCB0aGlzLm9uRHJhZ0xlYXZlKTtcclxuICAgICAgICAgICAgICAgIGl0ZW1TbG90LmFkZEV2ZW50TGlzdGVuZXIoXCJkcmFnb3ZlclwiLCB0aGlzLm9uRHJhZ092ZXIpO1xyXG4gICAgICAgICAgICAgICAgaXRlbVNsb3QuYWRkRXZlbnRMaXN0ZW5lcihcImRyb3BcIiwgdGhpcy5vbkRyb3ApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pdGVtU2xvdHNbdGhpcy5pdGVtU2xvdHMubGVuZ3RoLTFdLnB1c2goaXRlbVNsb3QpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIFJldHVybnMgdHJ1ZSBpZmYgdGhlIHBsYXllcidzIGludmVudG9yeSBjb250YWlucyBhdCBsZWFzdCBvbmUgYmFuZGFnZSBpdGVtXHJcbiAgICBwdWJsaWMgY29udGFpbnNCYW5kYWdlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGZvcihjb25zdCBncm91cCBvZiB0aGlzLml0ZW1TbG90cykge1xyXG4gICAgICAgICAgICBmb3IoY29uc3Qgc2xvdCBvZiBncm91cCkge1xyXG4gICAgICAgICAgICAgICAgaWYoc2xvdC5maXJzdENoaWxkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3BsaXRzID0gc2xvdC5jaGlsZHJlblswXS5pZC5zcGxpdChcIl9cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoc3BsaXRzLmxlbmd0aCA+PSAzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGl0ZW1JbmRleCA9IHNwbGl0c1syXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaXRlbSA9IHRoaXMuY29udHJvbGxlci5pdGVtc1tpdGVtSW5kZXhdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihpc0JhbmRhZ2UoaXRlbSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBIaWdobGlnaHQgYmFuZGFnZXMgc28gcGxheWVyIGNhbiBlYXNpbHkgcGljayB3aGljaCB0byB1c2UgdG8gaGVhbCBsaW1iXHJcbiAgICBwdWJsaWMgaGlnaGxpZ2h0QmFuZGFnZXMobGltYjogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5saW1iU2VsZWN0ZWQgPSBsaW1iO1xyXG4gICAgICAgIGZvcihjb25zdCBncm91cCBvZiB0aGlzLml0ZW1TbG90cykge1xyXG4gICAgICAgICAgICBmb3IoY29uc3Qgc2xvdCBvZiBncm91cCkge1xyXG4gICAgICAgICAgICAgICAgaWYoc2xvdC5maXJzdENoaWxkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3BsaXRzID0gc2xvdC5jaGlsZHJlblswXS5pZC5zcGxpdChcIl9cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoc3BsaXRzLmxlbmd0aCA+PSAzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGl0ZW1JbmRleCA9IHNwbGl0c1syXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgaXRlbSA9IHRoaXMuY29udHJvbGxlci5pdGVtc1tpdGVtSW5kZXhdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihpc0JhbmRhZ2UoaXRlbSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNsb3Quc3R5bGUuekluZGV4ID0gXCI3NVwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB1bkhpZ2hsaWdodEl0ZW1zKCkge1xyXG4gICAgICAgIHRoaXMubGltYlNlbGVjdGVkID0gbnVsbDtcclxuICAgICAgICBmb3IoY29uc3QgZ3JvdXAgb2YgdGhpcy5pdGVtU2xvdHMpIHtcclxuICAgICAgICAgICAgZm9yKGNvbnN0IHNsb3Qgb2YgZ3JvdXApIHtcclxuICAgICAgICAgICAgICAgIHNsb3Quc3R5bGUuekluZGV4ID0gXCIwXCI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRyYWdFbnRlciA9IChlKSA9PiB7XHJcbiAgICAgICAgaWYoIWUudGFyZ2V0KSB7IHJldHVybjsgfVxyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBjb25zdCBzbG90SW5kZXggPSBwYXJzZUludChlLnRhcmdldC5pZC5zcGxpdChcImludmVudC1zbG90LVwiKVsxXSwgMTApO1xyXG4gICAgICAgIGNvbnN0IHNwbGl0cyA9IGUuZGF0YVRyYW5zZmVyLmdldERhdGEoXCJ0ZXh0XCIpLnNwbGl0KFwiX1wiKTtcclxuICAgICAgICBsZXQgaXRlbUluZGV4LCBlbnRpdHlJRDtcclxuXHJcbiAgICAgICAgaWYoc3BsaXRzLmxlbmd0aCA9PSAzKSB7XHJcbiAgICAgICAgICAgIC8vIERyb3BwZWQgaXRlbSBmcm9tIGdyb3VuZFxyXG4gICAgICAgICAgICBpdGVtSW5kZXggPSBwYXJzZUludChzcGxpdHNbMV0sIDEwKTtcclxuICAgICAgICAgICAgZW50aXR5SUQgPSBwYXJzZUludChzcGxpdHNbMl0sIDEwKTtcclxuICAgICAgICB9IGVsc2UgaWYoc3BsaXRzLmxlbmd0aCA9PSA2KSB7XHJcbiAgICAgICAgICAgIC8vIERyb3BwZWQgaXRlbSBhbHJlYWR5IGluIGludmVudG9yeVxyXG4gICAgICAgICAgICBpdGVtSW5kZXggPSBwYXJzZUludChzcGxpdHNbMl0sIDEwKTtcclxuICAgICAgICAgICAgZW50aXR5SUQgPSBwYXJzZUludChzcGxpdHNbM10sIDEwKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKGlzTmFOKHNsb3RJbmRleCkgfHwgaXNOYU4oaXRlbUluZGV4KSB8fCBpc05hTihlbnRpdHlJRCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQ2xlYXIgYWxsIGRyYWcgaG92ZXIgZWZmZWN0c1xyXG4gICAgICAgIGZvcihjb25zdCByb3cgb2YgdGhpcy5pdGVtU2xvdHMpIHtcclxuICAgICAgICAgICAgZm9yKGNvbnN0IGl0ZW1TbG90IG9mIHJvdykge1xyXG4gICAgICAgICAgICAgICAgaXRlbVNsb3Quc3R5bGUuY3NzVGV4dCA9IFwiXCI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIERldGVybWluZSB3aGV0aGVyIGEgZHJvcCBpcyBhbGxvd2VkIGJhc2VkIG9uIHNsb3QgZW1wdGluZXNzXHJcbiAgICAgICAgY29uc3QgaXRlbSA9IHRoaXMuY29udHJvbGxlci5pdGVtc1tpdGVtSW5kZXhdO1xyXG4gICAgICAgIHRoaXMudXBkYXRlU2xvdChpdGVtLCBzbG90SW5kZXgsIGVudGl0eUlEKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uRHJhZ0xlYXZlID0gKGUpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRyYWdPdmVyID0gKGUpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRyb3AgPSAoZSkgPT4ge1xyXG4gICAgICAgIGlmKCFlLnRhcmdldCkgeyByZXR1cm47IH1cclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3Qgc2xvdEluZGV4ID0gcGFyc2VJbnQoZS50YXJnZXQuaWQuc3BsaXQoXCJpbnZlbnQtc2xvdC1cIilbMV0sIDEwKTtcclxuICAgICAgICBjb25zdCBzcGxpdHMgPSBlLmRhdGFUcmFuc2Zlci5nZXREYXRhKFwidGV4dFwiKS5zcGxpdChcIl9cIik7XHJcbiAgICAgICAgbGV0IGl0ZW1JbmRleCwgZW50aXR5SUQ7XHJcblxyXG4gICAgICAgIGlmKHNwbGl0cy5sZW5ndGggPT0gMykge1xyXG4gICAgICAgICAgICAvLyBEcm9wcGVkIGl0ZW0gZnJvbSBncm91bmRcclxuICAgICAgICAgICAgaXRlbUluZGV4ID0gc3BsaXRzWzFdO1xyXG4gICAgICAgICAgICBlbnRpdHlJRCA9IHNwbGl0c1syXTtcclxuICAgICAgICB9IGVsc2UgaWYoc3BsaXRzLmxlbmd0aCA9PSA2KSB7XHJcbiAgICAgICAgICAgIC8vIERyb3BwZWQgaXRlbSBhbHJlYWR5IGluIGludmVudG9yeVxyXG4gICAgICAgICAgICBpdGVtSW5kZXggPSBzcGxpdHNbMl07XHJcbiAgICAgICAgICAgIGVudGl0eUlEID0gc3BsaXRzWzNdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYoaXNOYU4oc2xvdEluZGV4KSB8fCBpc05hTihpdGVtSW5kZXgpIHx8IGlzTmFOKGVudGl0eUlEKSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBDaGVjayB0aGUgd2V0aGVyIHRoZSBzbG90IGlzIGF2aWFsYWJsZSB0byBiZSBkcm9wcGVkIGluXHJcbiAgICAgICAgY29uc3QgaXRlbSA9IHRoaXMuY29udHJvbGxlci5pdGVtc1tpdGVtSW5kZXhdO1xyXG4gICAgICAgIHRoaXMudXBkYXRlU2xvdChpdGVtLCBzbG90SW5kZXgsIGVudGl0eUlELCBmYWxzZSwgdHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uSXRlbURyYWdFbmQoKSB7XHJcbiAgICAgICAgdGhpcy5yZXNldFNsb3RTdHlsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFzc3VtZXMgc2xvdCBoYXMgYWxyZWFkeSBiZWVuIHZhbGlkaWZpZWRcclxuICAgIHByaXZhdGUgZmlsbFNsb3QoZW50aXR5SUQ6IG51bWJlciwgZmlyc3RTbG90WDogbnVtYmVyLCBmaXJzdFNsb3RZOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLmNvbnRyb2xsZXIudGFrZUl0ZW0oZW50aXR5SUQsIGZpcnN0U2xvdFgsIGZpcnN0U2xvdFkpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdXBkYXRlU2xvdChpdGVtOiBJdGVtLCBzbG90SW5kZXg6IG51bWJlciwgZW50aXR5SUQ6IG51bWJlciwgY29sb3IgPSB0cnVlLCBkcm9wID0gZmFsc2UpIHtcclxuICAgICAgICBjb25zdCBmaXJzdFNsb3RYID0gKHNsb3RJbmRleCAlIHRoaXMucm93TGVuZ3RoKTtcclxuICAgICAgICBjb25zdCBmaXJzdFNsb3RZID0gTWF0aC5mbG9vcihzbG90SW5kZXggLyB0aGlzLnJvd0xlbmd0aCk7XHJcbiAgICAgICAgY29uc3Qgc2xvdFkgPSBmaXJzdFNsb3RZO1xyXG5cclxuICAgICAgICAvLyBjaGVjayBpZiB0aGUgaXRlbSBmaXRzIG9uIHRoaXMgcm93XHJcbiAgICAgICAgaWYoZmlyc3RTbG90WCArIGl0ZW0uZ3JpZFdpZHRoID4gdGhpcy5yb3dMZW5ndGgpIHtcclxuICAgICAgICAgICAgaWYoY29sb3IpIHtcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgeCA9IGZpcnN0U2xvdFg7IHggPCB0aGlzLnJvd0xlbmd0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pdGVtU2xvdHNbc2xvdFldW3hdLnN0eWxlLmJhY2tncm91bmRDb2xvciA9IFwiI2YwMFwiO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGNoZWNrIGlmIHRoZSBzbG90cyBhcmUgdGFrZW5cclxuICAgICAgICBmb3IobGV0IHggPSBmaXJzdFNsb3RYOyB4IDwgZmlyc3RTbG90WCtpdGVtLmdyaWRXaWR0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgIGlmKHRoaXMuaXRlbVNsb3RzW3Nsb3RZXVt4XS5pbm5lckhUTUwgIT0gXCJcIikge1xyXG4gICAgICAgICAgICAgICAgaWYoY29sb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IobGV0IHggPSBmaXJzdFNsb3RYOyB4IDwgZmlyc3RTbG90WCtpdGVtLmdyaWRXaWR0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXRlbVNsb3RzW3Nsb3RZXVt4XS5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSBcIiNmMDBcIjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGVsc2UsIHRoZSBzbG90cyBtdXN0IGJlIGZyZWVcclxuICAgICAgICBmb3IobGV0IHggPSBmaXJzdFNsb3RYOyB4IDwgZmlyc3RTbG90WCtpdGVtLmdyaWRXaWR0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgIGlmKGNvbG9yKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLml0ZW1TbG90c1tzbG90WV1beF0uc3R5bGUuYmFja2dyb3VuZENvbG9yID0gXCIjMGYwXCI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKGRyb3ApIHtcclxuICAgICAgICAgICAgLy8gb25seSBuZWVkIHRvIHRlbGwgc2VydmVyIGFib3V0IGZpcnN0IHNsb3QgaW5kZXhlc1xyXG4gICAgICAgICAgICB0aGlzLmZpbGxTbG90KGVudGl0eUlELCBmaXJzdFNsb3RYLCBmaXJzdFNsb3RZKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlbW92ZUZyb21CYWNrcGFjayhlbnRpdHlJRDogbnVtYmVyLCBpdGVtSUQ6IG51bWJlcikge1xyXG4gICAgICAgIGZvcihsZXQgc2xvdFkgPSAwOyBzbG90WSA8IHRoaXMuY29sSGVpZ2h0OyBzbG90WSsrKSB7XHJcbiAgICAgICAgICAgIGZvcihsZXQgc2xvdFggPSAwOyBzbG90WCA8IHRoaXMucm93TGVuZ3RoOyBzbG90WCsrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBlbnRpdHkgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgYmFja3BhY2tfaXRlbV8ke2l0ZW1JRH1fJHtlbnRpdHlJRH1fJHtzbG90WH1fJHtzbG90WX1gKTtcclxuICAgICAgICAgICAgICAgIGlmKGVudGl0eSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGVudGl0eS5yZW1vdmUoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLml0ZW1TbG90c1tzbG90WV1bc2xvdFhdLmNsYXNzTGlzdC5yZW1vdmUoXCJpbnZlbnQtc2xvdC1qb2luZWQtcmlnaHRcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pdGVtU2xvdHNbc2xvdFldW3Nsb3RYXS5jbGFzc0xpc3QucmVtb3ZlKFwiaW52ZW50LXNsb3Qtam9pbmVkLWxlZnRcIik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIGZyb20gY29udHJvbGxlciBvbmNlIHNlcnZlciBoYXMgdmFsaWRpZmllZCBpdGVtIHRha2VuXHJcbiAgICBwdWJsaWMgb25JdGVtVGFrZW4oZW50aXR5SUQ6IG51bWJlciwgaXRlbUlEOiBudW1iZXIsIGZpcnN0U2xvdFg6IG51bWJlciwgZmlyc3RTbG90WTogbnVtYmVyKSB7XHJcbiAgICAgICAgaWYoaXRlbUlEIDwgMCB8fCBpdGVtSUQgPj0gdGhpcy5jb250cm9sbGVyLml0ZW1zLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGl0ZW0gPSB0aGlzLmNvbnRyb2xsZXIuaXRlbXNbaXRlbUlEXTtcclxuXHJcbiAgICAgICAgLy8gZmlyc3QsIGVuc3VyZSB0aGUgaXRlbSBmaXRzIGludG8gdGhlIHNwYWNlXHJcbiAgICAgICAgaWYoaXRlbS5ncmlkV2lkdGggPiBmaXJzdFNsb3RYICsgaXRlbS5ncmlkV2lkdGgpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gcmVtb3ZlIHRoZSBpdGVtIGZyb20gdGhlIGN1cnJlbnQgc3BvdCBpbiBiYWNrcGFjayBpZiBpdCBpcyBpbiBiYWNrcGFja1xyXG4gICAgICAgIHRoaXMucmVtb3ZlRnJvbUJhY2twYWNrKGVudGl0eUlELCBpdGVtSUQpO1xyXG5cclxuICAgICAgICBjb25zdCBzbG90WSA9IGZpcnN0U2xvdFk7ICAgLy8gVE9ETyAtIGNoYW5nZSB3aGVuIGl0ZW0gcm90YXRpb24gaW1wbGVtZW50ZWRcclxuICAgICAgICBsZXQgaSA9IDA7XHJcbiAgICAgICAgZm9yKGxldCBzbG90WCA9IGZpcnN0U2xvdFg7IHNsb3RYIDwgZmlyc3RTbG90WCtpdGVtLmdyaWRXaWR0aDsgc2xvdFgrKykge1xyXG4gICAgICAgICAgICBjb25zdCBzbG90ID0gdGhpcy5pdGVtU2xvdHNbc2xvdFldW3Nsb3RYXTtcclxuICAgICAgICAgICAgLy8gc2V0IHRoZSBkcmFnLWltZyB0byB0aGUgaXRlbSBpbWcsIHdpdGggc2l6ZSByZWxhdGl2ZSB0byBpdGVtIHNsb3Qgc2l6ZVxyXG4gICAgICAgICAgICAvLyBtZXRob2QgZm9yIHNldHRpbmcgZHJhZy1pbWcgc2l6ZSBzb3VyY2VkIGZyb20gaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvMzE5OTQ1NzIvY2hhbmdlLWRyYWctZ2hvc3QtaW1hZ2Utc2l6ZVxyXG4gICAgICAgICAgICBsZXQgaW1nID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImltZ1wiKTtcclxuICAgICAgICAgICAgaW1nLmlkID0gYGJhY2twYWNrX2l0ZW1fJHtpdGVtSUR9XyR7ZW50aXR5SUR9XyR7c2xvdFh9XyR7c2xvdFl9YDtcclxuICAgICAgICAgICAgaW1nLnNyYyA9IGl0ZW0uaW1hZ2U7XHJcbiAgICAgICAgICAgIGltZy5zdHlsZS53aWR0aCA9IGAkeyhzbG90LmNsaWVudFdpZHRoLTApKml0ZW0uZ3JpZFdpZHRofXB4YDtcclxuICAgICAgICAgICAgaW1nLnN0eWxlLmhlaWdodCA9IGAkeyhzbG90LmNsaWVudEhlaWdodC0wKSppdGVtLmdyaWRIZWlnaHR9cHhgO1xyXG4gICAgICAgICAgICBpbWcuY2xhc3NMaXN0LmFkZChcImludmVudC1pdGVtLWltZ1wiKTtcclxuICAgICAgICAgICAgaW1nLnN0eWxlLmxlZnQgPSBgJHstc2xvdC5jbGllbnRXaWR0aCppfXB4YDtcclxuICAgICAgICAgICAgaW1nLmFkZEV2ZW50TGlzdGVuZXIoXCJkcmFnc3RhcnRcIiwgdGhpcy5vbkRyYWdTdGFydCk7XHJcbiAgICAgICAgICAgIGltZy5hZGRFdmVudExpc3RlbmVyKFwiZHJhZ2VuZFwiLCB0aGlzLm9uRHJhZ0VuZCk7XHJcbiAgICAgICAgICAgIGltZy5hZGRFdmVudExpc3RlbmVyKENvbnRyb2xsZXIuQ0xJQ0tfRVZFTlQsIHRoaXMub25DbGljayk7XHJcbiAgICAgICAgICAgIGltZy5hZGRFdmVudExpc3RlbmVyKFwibW91c2Vtb3ZlXCIsIHRoaXMub25Ib3Zlcik7XHJcbiAgICAgICAgICAgIHNsb3QuYXBwZW5kQ2hpbGQoaW1nKTtcclxuICAgICAgICAgICAgaSsrO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gcmVtb3ZlIHJpZ2h0IGJvcmRlcnMgZnJvbSBhbGwgYnV0IGxhc3Qgc2xvdFxyXG4gICAgICAgIGZvcihsZXQgc2xvdFggPSBmaXJzdFNsb3RYOyBzbG90WCA8IGZpcnN0U2xvdFgraXRlbS5ncmlkV2lkdGgtMTsgc2xvdFgrKykge1xyXG4gICAgICAgICAgICAvLy9jb25zb2xlLmxvZyhcInJlbW92aW5nIHJpZ2h0IGJvcmRlclwiKTtcclxuICAgICAgICAgICAgY29uc3Qgc2xvdCA9IHRoaXMuaXRlbVNsb3RzW3Nsb3RZXVtzbG90WF07XHJcbiAgICAgICAgICAgIHNsb3Quc3R5bGUuYm9yZGVyUmlnaHQgPSBcIm5vbmVcIjtcclxuICAgICAgICAgICAgc2xvdC5jbGFzc0xpc3QuYWRkKFwiaW52ZW50LXNsb3Qtam9pbmVkLXJpZ2h0XCIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gcmVtb3ZlIGxlZnQgYm9yZGVycyBmcm9tIGFsbCBidXQgZmlyc3Qgc2xvdFxyXG4gICAgICAgIGZvcihsZXQgc2xvdFggPSBmaXJzdFNsb3RYKzE7IHNsb3RYIDwgZmlyc3RTbG90WCtpdGVtLmdyaWRXaWR0aDsgc2xvdFgrKykge1xyXG4gICAgICAgICAgICAvLy9jb25zb2xlLmxvZyhcInJlbW92aW5nIGxlZnQgYm9yZGVyXCIpO1xyXG4gICAgICAgICAgICBjb25zdCBzbG90ID0gdGhpcy5pdGVtU2xvdHNbc2xvdFldW3Nsb3RYXTtcclxuICAgICAgICAgICAgc2xvdC5jbGFzc0xpc3QuYWRkKFwiaW52ZW50LXNsb3Qtam9pbmVkLWxlZnRcIik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnJlc2V0U2xvdFN0eWxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRyYWdTdGFydCA9IChlKSA9PiB7XHJcbiAgICAgICAgY29uc3QgaXRlbU9iaiA9IHRoaXMuY29udHJvbGxlci5pdGVtc1twYXJzZUludChlLnRhcmdldC5pZC5zcGxpdChcIl9cIilbMl0sIDEwKV07XHJcbiAgICAgICAgY29uc3QgZW50aXR5SUQgPSBwYXJzZUludChlLnRhcmdldC5pZC5zcGxpdChcIl9cIilbM10sIDEwKTtcclxuXHJcbiAgICAgICAgLy8gc2V0IHRoZSBlbnRpdHkgdG8gYmUgaGlkZGVuIHNvIGl0IGxvb2tzIGxpa2UgaXQgaXMgYmVpbmcgcGlja2VkIHVwXHJcbiAgICAgICAgZm9yKGNvbnN0IHJvdyBvZiB0aGlzLml0ZW1TbG90cykge1xyXG4gICAgICAgICAgICBmb3IoY29uc3Qgc2xvdCBvZiByb3cpIHtcclxuICAgICAgICAgICAgICAgIGlmKHNsb3QuY2hpbGRyZW4ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGVsZW0gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChzbG90LmNoaWxkcmVuWzBdLmlkKTtcclxuICAgICAgICAgICAgICAgICAgICBpZihlbGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGVJRCA9IHBhcnNlSW50KGVsZW0uaWQuc3BsaXQoXCJfXCIpWzNdLCAxMCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKGVJRCA9PSBlbnRpdHlJRCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZWxlbS5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gc2V0IHRoZSBkcmFnLWltZyB0byB0aGUgaXRlbSBpbWcsIHdpdGggc2l6ZSByZWxhdGl2ZSB0byBpdGVtIHNsb3Qgc2l6ZVxyXG4gICAgICAgIC8vIG1ldGhvZCBmb3Igc2V0dGluZyBkcmFnLWltZyBzaXplIHNvdXJjZWQgZnJvbSBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy8zMTk5NDU3Mi9jaGFuZ2UtZHJhZy1naG9zdC1pbWFnZS1zaXplXHJcbiAgICAgICAgbGV0IGRyYWdJY29uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImltZ1wiKTtcclxuICAgICAgICBkcmFnSWNvbi5zcmMgPSBlLnRhcmdldC5zcmM7XHJcbiAgICAgICAgZHJhZ0ljb24uc3R5bGUud2lkdGggPSBgJHt0aGlzLml0ZW1TbG90c1swXVswXS5jbGllbnRXaWR0aCppdGVtT2JqLmdyaWRXaWR0aH1weGA7XHJcbiAgICAgICAgZHJhZ0ljb24uc3R5bGUuaGVpZ2h0ID0gYCR7dGhpcy5pdGVtU2xvdHNbMF1bMF0uY2xpZW50SGVpZ2h0Kml0ZW1PYmouZ3JpZEhlaWdodH1weGA7XHJcblxyXG4gICAgICAgIGxldCBkaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xyXG4gICAgICAgIGRpdi5pZCA9IGBkcmFnXyR7ZS50YXJnZXQuaWR9YDtcclxuICAgICAgICBkaXYuYXBwZW5kQ2hpbGQoZHJhZ0ljb24pO1xyXG4gICAgICAgIGRpdi5zdHlsZS5wb3NpdGlvbiA9IFwiYWJzb2x1dGVcIjtcclxuICAgICAgICBkaXYuc3R5bGUudG9wID0gXCIwcHhcIjtcclxuICAgICAgICBkaXYuc3R5bGUubGVmdD0gYCR7LXRoaXMuaXRlbVNsb3RzWzBdWzBdLmNsaWVudFdpZHRoKml0ZW1PYmouZ3JpZFdpZHRofXB4YDtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiYm9keVwiKS5hcHBlbmRDaGlsZChkaXYpO1xyXG5cclxuICAgICAgICBlLmRhdGFUcmFuc2Zlci5zZXREYXRhKFwidGV4dC9wbGFpblwiLCBlLnRhcmdldC5pZCk7XHJcbiAgICAgICAgZS5kYXRhVHJhbnNmZXIuc2V0RHJhZ0ltYWdlKGRpdixcclxuICAgICAgICAgICAgdGhpcy5pdGVtU2xvdHNbMF1bMF0uY2xpZW50V2lkdGgvMixcclxuICAgICAgICAgICAgdGhpcy5pdGVtU2xvdHNbMF1bMF0uY2xpZW50SGVpZ2h0LzIpO1xyXG5cclxuICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25JdGVtRHJhZ1N0YXJ0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRyYWdFbmQgPSAoZSkgPT4ge1xyXG4gICAgICAgIC8vIHdoZW4gdGhlIGl0ZW0gaGFzIGZpbmlzaGVkIGJlaW5nIGRyYWdnZWQsIHJlbW92ZSB0aGUgaW1hZ2UgaW4gdGhlIGRvbVxyXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGBkcmFnXyR7ZS50YXJnZXQuaWR9YCkucmVtb3ZlKCk7XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uSXRlbURyYWdFbmQoKTtcclxuICAgICAgICBjb25zdCBlbnRpdHlJRCA9IGUudGFyZ2V0LmlkLnNwbGl0KFwiX1wiKVszXTtcclxuXHJcbiAgICAgICAgLy8gc2V0IHRoZSBlbnRpdHkgaW4gYmFjayB0byBiZWluZyB2aXNpYmxlXHJcbiAgICAgICAgdGhpcy5yZXNldFNsb3RTdHlsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25DbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGlmKHRoaXMuY29udHJvbGxlci5nYW1lUGhhc2UgPT0gR2FtZVBoYXNlLkFDVElPTl9QSEFTRSkge1xyXG4gICAgICAgICAgICBjb25zdCBlbnRpdHlJRCA9IGUudGFyZ2V0LmlkLnNwbGl0KFwiX1wiKVszXTtcclxuICAgICAgICAgICAgY29uc3QgaXRlbUlEID0gZS50YXJnZXQuaWQuc3BsaXQoXCJfXCIpWzJdO1xyXG4gICAgICAgICAgICBjb25zdCBpdGVtID0gdGhpcy5jb250cm9sbGVyLml0ZW1zW2l0ZW1JRF07XHJcbiAgICAgICAgICAgIGlmKGlzQmFuZGFnZShpdGVtKSkge1xyXG4gICAgICAgICAgICAgICAgaWYodGhpcy5saW1iU2VsZWN0ZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIudXNlQmFuZGFnZShlbnRpdHlJRCwgdGhpcy5saW1iU2VsZWN0ZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGltYlNlbGVjdGVkID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uQmFuZGFnZUNsaWNrKGUuY2xpZW50WCwgZS5jbGllbnRZLCBlbnRpdHlJRCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZihpc0d1bihpdGVtKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLmVxdWlwSXRlbShlbnRpdHlJRCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkhvdmVyID0gKGUpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3QgaXRlbUlEID0gcGFyc2VJbnQoZS50YXJnZXQuaWQuc3BsaXQoXCJfXCIpWzJdLCAxMCk7XHJcbiAgICAgICAgY29uc3QgZW50aXR5SUQgPSBwYXJzZUludChlLnRhcmdldC5pZC5zcGxpdChcIl9cIilbM10sIDEwKTtcclxuICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25JdGVtSG92ZXIobmV3IEl0ZW1FbnRpdHkoZW50aXR5SUQsIGl0ZW1JRCksIGUuY2xpZW50WCwgZS5jbGllbnRZKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiB0aGUgYWN0aW9uIHBoYXNlIGlzIHBlcmZvcm1lZCBhbmQgYSBuZXcgd2VhcG9uIGlzIGVxdWlwcGVkXHJcbiAgICBwdWJsaWMgb25JdGVtRXF1aXBwZWQoKSB7XHJcbiAgICAgICAgdGhpcy5yZXNldFNsb3RTdHlsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyByZXNldFNsb3RTdHlsZSgpIHtcclxuICAgICAgICBmb3IoY29uc3Qgcm93IG9mIHRoaXMuaXRlbVNsb3RzKSB7XHJcbiAgICAgICAgICAgIGZvcihjb25zdCBzbG90IG9mIHJvdykge1xyXG4gICAgICAgICAgICAgICAgbGV0IGVxdWlwcGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAvLyBzZXQgdGhlIGVudGl0eSBpbiBiYWNrIHRvIGJlaW5nIHZpc2libGVcclxuICAgICAgICAgICAgICAgIGlmKHNsb3QuY2hpbGRyZW4ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGVsZW0gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChzbG90LmNoaWxkcmVuWzBdLmlkKTtcclxuICAgICAgICAgICAgICAgICAgICBpZihlbGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW0uc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBlSUQgPSBwYXJzZUludChlbGVtLmlkLnNwbGl0KFwiX1wiKVszXSwgMTApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZih0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJFcXVpcHBlZEl0ZW0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlSUQgPT0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyRXF1aXBwZWRJdGVtLmVudGl0eUlEKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzbG90LnN0eWxlLmJhY2tncm91bmRDb2xvciA9IFwiIzBFQkZFOVwiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXF1aXBwZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gc2V0IGFueSBzbG90cyBub3QgY29udGFpbmluZyBlcXVpcHBlZCB3ZWFwb25zIHRvIGRlZmF1bHRcclxuICAgICAgICAgICAgICAgIGlmKCFlcXVpcHBlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHNsb3Quc3R5bGUuY3NzVGV4dCA9IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuXHJcbmV4cG9ydCBjbGFzcyBJdGVtQWN0aW9uTGlzdCB7XHJcbiAgICBwcml2YXRlIGl0ZW1BY3Rpb25MaXN0OiBIVE1MRGl2RWxlbWVudDtcclxuXHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IocHJpdmF0ZSBjb250cm9sbGVyOiBDb250cm9sbGVyKSB7XHJcbiAgICAgICAgdGhpcy5pdGVtQWN0aW9uTGlzdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaXRlbS1hY3Rpb24tbGlzdFwiKSBhcyBIVE1MRGl2RWxlbWVudDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2hvd0FjdGlvbkxpc3QoeDogbnVtYmVyLCB5OiBudW1iZXIsIGNhbkVxdWlwPWZhbHNlKSB7XHJcbiAgICAgICAgaWYoeCA+PSAwICYmIHkgPj0gMCkge1xyXG4gICAgICAgICAgICAvLyBjbGVhciBjb250ZW50cyBvZiBkaXZcclxuICAgICAgICAgICAgd2hpbGUodGhpcy5pdGVtQWN0aW9uTGlzdC5maXJzdENoaWxkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLml0ZW1BY3Rpb25MaXN0LnJlbW92ZUNoaWxkKHRoaXMuaXRlbUFjdGlvbkxpc3QuZmlyc3RDaGlsZCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHNldCB0aGUgY29udGVudHMgb2YgdGhlIHRvb2x0aXBcclxuICAgICAgICAgICAgaWYoY2FuRXF1aXApIHtcclxuICAgICAgICAgICAgICAgIGxldCBidG4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiYnV0dG9uXCIpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGltZyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbWdcIik7XHJcbiAgICAgICAgICAgICAgICBsZXQgc3BhbiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJzcGFuXCIpO1xyXG4gICAgICAgICAgICAgICAgaW1nLnNyYyA9IFwiYXNzZXRzL3VzZV9pY29uLnBuZ1wiO1xyXG4gICAgICAgICAgICAgICAgaW1nLnN0eWxlLmRpc3BsYXkgPSBcImlubGluZS1ibG9ja1wiO1xyXG4gICAgICAgICAgICAgICAgc3Bhbi5pbm5lckhUTUwgPSBcIkVxdWlwXCI7XHJcbiAgICAgICAgICAgICAgICBzcGFuLnN0eWxlLmRpc3BsYXkgPSBcInRhYmxlLWNlbGxcIjtcclxuICAgICAgICAgICAgICAgIGJ0bi5hcHBlbmRDaGlsZChpbWcpO1xyXG4gICAgICAgICAgICAgICAgYnRuLmFwcGVuZENoaWxkKHNwYW4pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pdGVtQWN0aW9uTGlzdC5hcHBlbmRDaGlsZChidG4pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBzZXQgdGhlIHBvc2l0aW9uIG9mIHRoZSB0b29sdGlwXHJcbiAgICAgICAgICAgIHRoaXMuaXRlbUFjdGlvbkxpc3Quc3R5bGUubGVmdCA9IGBjYWxjKCR7eH1weCAtICR7dGhpcy5pdGVtQWN0aW9uTGlzdC5jbGllbnRXaWR0aC8yfXB4KWA7XHJcbiAgICAgICAgICAgIHRoaXMuaXRlbUFjdGlvbkxpc3Quc3R5bGUudG9wID0gYCR7eX1weGA7XHJcblxyXG4gICAgICAgICAgICAvLyBtYWtlIHN1cmUgdGhlIHRvb2x0aXAgaXMgdmlzaWJsZVxyXG4gICAgICAgICAgICB0aGlzLml0ZW1BY3Rpb25MaXN0LnN0eWxlLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLml0ZW1BY3Rpb25MaXN0LnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcIi4uL0NvbnRyb2xsZXJcIlxyXG5cclxuZXhwb3J0IGNsYXNzIEl0ZW1zRm91bmREaXYge1xyXG4gICAgcHJpdmF0ZSBpdGVtc0ZvdW5kRGl2OiBIVE1MRGl2RWxlbWVudDtcclxuICAgIHByaXZhdGUgaXRlbXNGb3VuZExpc3Q6IEhUTUxEaXZFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBpdGVtU2xvdDE6IEhUTUxEaXZFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBkcm9waXRlbURpdjogSFRNTERpdkVsZW1lbnQ7XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIHRoaXMuaXRlbXNGb3VuZERpdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiaXRlbXMtZm91bmQtZGl2XCIpIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuaXRlbXNGb3VuZExpc3QgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIml0ZW1zLWZvdW5kLWxpc3RcIikgYXMgSFRNTERpdkVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5pdGVtU2xvdDEgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImludmVudC1zbG90LTFcIikgYXMgSFRNTERpdkVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5kcm9waXRlbURpdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZHJvcC1pdGVtLWRpdlwiKSBhcyBIVE1MRGl2RWxlbWVudDtcclxuICAgICAgICB0aGlzLmRyb3BpdGVtRGl2LmFkZEV2ZW50TGlzdGVuZXIoXCJkcmFnZW50ZXJcIiwgdGhpcy5vbkRyYWdFbnRlcik7XHJcbiAgICAgICAgdGhpcy5kcm9waXRlbURpdi5hZGRFdmVudExpc3RlbmVyKFwiZHJhZ292ZXJcIiwgdGhpcy5vbkRyYWdPdmVyKTtcclxuICAgICAgICB0aGlzLmRyb3BpdGVtRGl2LmFkZEV2ZW50TGlzdGVuZXIoXCJkcm9wXCIsIHRoaXMub25Ecm9wKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNsZWFyKCkge1xyXG4gICAgICAgIHdoaWxlKHRoaXMuaXRlbXNGb3VuZExpc3QuZmlyc3RDaGlsZCkge1xyXG4gICAgICAgICAgICB0aGlzLml0ZW1zRm91bmRMaXN0LnJlbW92ZUNoaWxkKHRoaXMuaXRlbXNGb3VuZExpc3QuZmlyc3RDaGlsZCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRJdGVtc0ZvdW5kKGl0ZW1zOiBBcnJheTx7IGVudGl0eUlEOiBzdHJpbmcsIGl0ZW1JRDogc3RyaW5nIH0+KSB7XHJcbiAgICAgICAgdGhpcy5jbGVhcigpO1xyXG4gICAgICAgIGZvcihjb25zdCBpdGVtIG9mIGl0ZW1zKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGl0ZW1PYmogPSB0aGlzLmNvbnRyb2xsZXIuaXRlbXNbaXRlbS5pdGVtSURdO1xyXG4gICAgICAgICAgICBsZXQgaW1nID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImltZ1wiKTtcclxuICAgICAgICAgICAgaW1nLmlkID0gYGl0ZW1fJHtpdGVtLml0ZW1JRH1fJHtpdGVtLmVudGl0eUlEfWA7XHJcbiAgICAgICAgICAgIGltZy5jbGFzc0xpc3QuYWRkKFwiaXRlbS1mb3VuZC1pbWdcIik7XHJcbiAgICAgICAgICAgIGltZy5zcmMgPSBpdGVtT2JqLmltYWdlO1xyXG4gICAgICAgICAgICBpbWcudGl0bGUgPSBpdGVtT2JqLm5hbWU7XHJcbiAgICAgICAgICAgIGltZy5zdHlsZS53aWR0aCA9IGAke3RoaXMuaXRlbVNsb3QxLmNsaWVudFdpZHRoKml0ZW1PYmouZ3JpZFdpZHRofXB4YDtcclxuICAgICAgICAgICAgaW1nLnN0eWxlLmhlaWdodCA9IGAke3RoaXMuaXRlbVNsb3QxLmNsaWVudEhlaWdodCppdGVtT2JqLmdyaWRIZWlnaHR9cHhgO1xyXG4gICAgICAgICAgICB0aGlzLml0ZW1zRm91bmRMaXN0LmFwcGVuZENoaWxkKGltZyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLml0ZW1zRm91bmREaXYuc3R5bGUudmlzaWJpbGl0eSA9IGl0ZW1zLmxlbmd0aCA+IDAgPyBcInZpc2libGVcIiA6IFwiaGlkZGVuXCI7XHJcblxyXG4gICAgICAgIGZvcihjb25zdCBpdGVtIG9mIGl0ZW1zKSB7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGBpdGVtXyR7aXRlbS5pdGVtSUR9XyR7aXRlbS5lbnRpdHlJRH1gKS5hZGRFdmVudExpc3RlbmVyKFwiZHJhZ3N0YXJ0XCIsIHRoaXMub25EcmFnU3RhcnQpO1xyXG4gICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgaXRlbV8ke2l0ZW0uaXRlbUlEfV8ke2l0ZW0uZW50aXR5SUR9YCkuYWRkRXZlbnRMaXN0ZW5lcihcImRyYWdlbmRcIiwgdGhpcy5vbkRyYWdFbmQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYWRkSXRlbShlbnRpdHlJRDogbnVtYmVyLCBpdGVtSUQ6IG51bWJlcikge1xyXG4gICAgICAgIGNvbnN0IGl0ZW1PYmogPSB0aGlzLmNvbnRyb2xsZXIuaXRlbXNbaXRlbUlEXTtcclxuICAgICAgICBjb25zdCBpbWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaW1nXCIpO1xyXG4gICAgICAgIGltZy5pZCA9IGBpdGVtXyR7aXRlbUlEfV8ke2VudGl0eUlEfWA7XHJcbiAgICAgICAgaW1nLmNsYXNzTGlzdC5hZGQoXCJpdGVtLWZvdW5kLWltZ1wiKTtcclxuICAgICAgICBpbWcuc3JjID0gaXRlbU9iai5pbWFnZTtcclxuICAgICAgICBpbWcudGl0bGUgPSBpdGVtT2JqLm5hbWU7XHJcbiAgICAgICAgaW1nLnN0eWxlLndpZHRoID0gYCR7dGhpcy5pdGVtU2xvdDEuY2xpZW50V2lkdGgqaXRlbU9iai5ncmlkV2lkdGh9cHhgO1xyXG4gICAgICAgIGltZy5zdHlsZS5oZWlnaHQgPSBgJHt0aGlzLml0ZW1TbG90MS5jbGllbnRIZWlnaHQqaXRlbU9iai5ncmlkSGVpZ2h0fXB4YDtcclxuICAgICAgICBpbWcuYWRkRXZlbnRMaXN0ZW5lcihcImRyYWdzdGFydFwiLCB0aGlzLm9uRHJhZ1N0YXJ0KTtcclxuICAgICAgICBpbWcuYWRkRXZlbnRMaXN0ZW5lcihcImRyYWdlbmRcIiwgdGhpcy5vbkRyYWdFbmQpO1xyXG4gICAgICAgIHRoaXMuaXRlbXNGb3VuZERpdi5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XHJcbiAgICAgICAgdGhpcy5pdGVtc0ZvdW5kTGlzdC5hcHBlbmRDaGlsZChpbWcpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyByZW1vdmVJdGVtKGVudGl0eUlEOiBudW1iZXIsIGl0ZW1JRDogbnVtYmVyKSB7XHJcbiAgICAgICAgY29uc3QgZW50aXR5ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoYGl0ZW1fJHtpdGVtSUR9XyR7ZW50aXR5SUR9YCk7XHJcbiAgICAgICAgaWYoZW50aXR5KSB7XHJcbiAgICAgICAgICAgIGVudGl0eS5yZW1vdmUoKTtcclxuICAgICAgICAgICAgaWYodGhpcy5pdGVtc0ZvdW5kTGlzdC5jaGlsZHJlbi5sZW5ndGggPD0gMSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pdGVtc0ZvdW5kRGl2LnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25EcmFnU3RhcnQgPSAoZSkgPT4ge1xyXG4gICAgICAgIGNvbnN0IGl0ZW1PYmogPSB0aGlzLmNvbnRyb2xsZXIuaXRlbXNbcGFyc2VJbnQoZS50YXJnZXQuaWQuc3BsaXQoXCJfXCIpWzFdLCAxMCldO1xyXG5cclxuICAgICAgICAvLyBzZXQgdGhlIGRyYWctaW1nIHRvIHRoZSBpdGVtIGltZywgd2l0aCBzaXplIHJlbGF0aXZlIHRvIGl0ZW0gc2xvdCBzaXplXHJcbiAgICAgICAgLy8gbWV0aG9kIGZvciBzZXR0aW5nIGRyYWctaW1nIHNpemUgc291cmNlZCBmcm9tIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzMxOTk0NTcyL2NoYW5nZS1kcmFnLWdob3N0LWltYWdlLXNpemVcclxuICAgICAgICBsZXQgZHJhZ0ljb24gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaW1nXCIpO1xyXG4gICAgICAgIGRyYWdJY29uLnNyYyA9IGUudGFyZ2V0LnNyYztcclxuICAgICAgICBkcmFnSWNvbi5zdHlsZS53aWR0aCA9IGAke3RoaXMuaXRlbVNsb3QxLmNsaWVudFdpZHRoKml0ZW1PYmouZ3JpZFdpZHRofXB4YDtcclxuICAgICAgICBkcmFnSWNvbi5zdHlsZS5oZWlnaHQgPSBgJHt0aGlzLml0ZW1TbG90MS5jbGllbnRIZWlnaHQqaXRlbU9iai5ncmlkSGVpZ2h0fXB4YDtcclxuXHJcbiAgICAgICAgbGV0IGRpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XHJcbiAgICAgICAgZGl2LmlkID0gYGRyYWdfJHtlLnRhcmdldC5pZH1gO1xyXG4gICAgICAgIGRpdi5hcHBlbmRDaGlsZChkcmFnSWNvbik7XHJcbiAgICAgICAgZGl2LnN0eWxlLnBvc2l0aW9uID0gXCJhYnNvbHV0ZVwiO1xyXG4gICAgICAgIGRpdi5zdHlsZS50b3AgPSBcIjBweFwiO1xyXG4gICAgICAgIGRpdi5zdHlsZS5sZWZ0PSBgJHstdGhpcy5pdGVtU2xvdDEuY2xpZW50V2lkdGgqaXRlbU9iai5ncmlkV2lkdGh9cHhgO1xyXG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJib2R5XCIpLmFwcGVuZENoaWxkKGRpdik7XHJcblxyXG4gICAgICAgIGUuZGF0YVRyYW5zZmVyLnNldERhdGEoXCJ0ZXh0L3BsYWluXCIsIGUudGFyZ2V0LmlkKTtcclxuICAgICAgICBlLmRhdGFUcmFuc2Zlci5zZXREcmFnSW1hZ2UoZGl2LFxyXG4gICAgICAgICAgICB0aGlzLml0ZW1TbG90MS5jbGllbnRXaWR0aC8yLFxyXG4gICAgICAgICAgICB0aGlzLml0ZW1TbG90MS5jbGllbnRIZWlnaHQvMik7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRyYWdFbmQgPSAoZSkgPT4ge1xyXG4gICAgICAgIC8vIHdoZW4gdGhlIGl0ZW0gaGFzIGZpbmlzaGVkIGJlaW5nIGRyYWdnZWQsIHJlbW92ZSB0aGUgaW1hZ2UgaW4gdGhlIGRvbVxyXG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGBkcmFnXyR7ZS50YXJnZXQuaWR9YCkucmVtb3ZlKCk7XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uSXRlbURyYWdFbmQoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZW5hYmxlRHJvcE1vZGUoKSB7XHJcbiAgICAgICAgdGhpcy5kcm9waXRlbURpdi5zdHlsZS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XHJcbiAgICAgICAgdGhpcy5pdGVtc0ZvdW5kRGl2LnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBkaXNhYmxlRHJvcE1vZGUoKSB7XHJcbiAgICAgICAgdGhpcy5kcm9waXRlbURpdi5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIjtcclxuICAgICAgICBpZih0aGlzLml0ZW1zRm91bmRMaXN0LmNoaWxkcmVuLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgdGhpcy5pdGVtc0ZvdW5kRGl2LnN0eWxlLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRyYWdFbnRlciA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25EcmFnT3ZlciA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25Ecm9wID0gKGUpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3QgZW50aXR5SUQgPSBwYXJzZUludChlLmRhdGFUcmFuc2Zlci5nZXREYXRhKFwidGV4dC9odG1sXCIpLnNwbGl0KFwiX1wiKVszXSwgMTApO1xyXG4gICAgICAgIHRoaXMuY29udHJvbGxlci5kcm9wSXRlbShlbnRpdHlJRCk7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuaW1wb3J0IHsgR2FtZVBoYXNlIH0gZnJvbSBcIi4uL2dhbWUvR2FtZVBoYXNlXCJcclxuaW1wb3J0IHsgVGlsZVR5cGUgfSBmcm9tIFwiLi4vZ2FtZS9UaWxlVHlwZVwiXHJcbmltcG9ydCB7IEl0ZW0sIEd1biB9IGZyb20gXCIuLi9nYW1lL0l0ZW1cIlxyXG5pbXBvcnQgeyBJdGVtTWVtZW50byB9IGZyb20gXCIuLi9nYW1lL0l0ZW1NZW1lbnRvXCJcclxuaW1wb3J0IHsgQWN0aW9uQXBDb3N0cyB9IGZyb20gXCIuLi9nYW1lL0FjdGlvblwiXHJcbmltcG9ydCB7IFBsYXllckRldGFpbHMgfSBmcm9tIFwiLi4vZ2FtZS9QbGF5ZXJEZXRhaWxzXCJcclxuaW1wb3J0IHsgTWFwQ2FudmFzQXJyb3cgfSBmcm9tIFwiLi9NYXBDYW52YXNBcnJvd1wiXHJcblxyXG5leHBvcnQgY2xhc3MgTWFwQ2FudmFzIHtcclxuICAgIC8vIEhUTUwgZWxlbWVudHNcclxuICAgIHByaXZhdGUgY2FudmFzOiBIVE1MQ2FudmFzRWxlbWVudDtcclxuICAgIHByaXZhdGUgY3R4OiBDYW52YXNSZW5kZXJpbmdDb250ZXh0MkQ7XHJcbiAgICBwcml2YXRlIGhpdENhbnZhczogSFRNTENhbnZhc0VsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGhpdEN0eDogQ2FudmFzUmVuZGVyaW5nQ29udGV4dDJEO1xyXG5cclxuICAgIC8vIERldGVybWluZXMgd2hlcmUgdGhlIGZvY3VzIG9mIHRoZSB6b29tIGFuZCBob3cgZmFyIHpvb21lZCBpbiB0aGUgbWFwIGlzXHJcbiAgICBwcml2YXRlIHpvb21UYXJnZXRYOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIHpvb21UYXJnZXRZOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIHpvb21MZXZlbDogbnVtYmVyID0gMS4wO1xyXG5cclxuICAgIC8vIFRoZSB0b3RhbCB3aWR0aCBhbmQgaGVpZ2h0IG9mIHRoZSBoZXhhZ29uIGdyaWQgKGluIHBpeGVscylcclxuICAgIHByaXZhdGUgdG90YWxNYXBXaWR0aDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSB0b3RhbE1hcEhlaWdodDogbnVtYmVyO1xyXG5cclxuICAgIC8vIFRoZSBtb3VzZSBwb3NpdGlvbiByZWxhdGl2ZSB0byBjYW52YXMgc2V0IG9uIGxhc3QgbW91c2VNb3ZlIG9yIG9uQ2xpY2sgY2FsbFxyXG4gICAgcHJpdmF0ZSBsYXN0TW91c2VYOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIGxhc3RNb3VzZVk6IG51bWJlcjtcclxuICAgIC8vIFRoZSBtb3VzZSBwb3NpdGlvbiByZWxhdGl2ZSB0byB0aGUgZG9tIG9yaWdpblxyXG4gICAgcHJpdmF0ZSBsYXN0R2xvYmFsTW91c2VYOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIGxhc3RHbG9iYWxNb3VzZVk6IG51bWJlcjtcclxuXHJcbiAgICAvLyBUaGUgaGV4YWdvbiB3aGljaCBoYXMgYmVlbiBjbGlja2VkIGJ5IHRoZSB1c2VyJ3MgbW91c2VcclxuICAgIHByaXZhdGUgaGV4U2VsZWN0ZWRYOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIGhleFNlbGVjdGVkWTogbnVtYmVyO1xyXG5cclxuICAgIC8vIFRoZSBoZXhhZ29uIHdoaWNoIGlzIGJlaW5nIGhvdmVyZWQgb3ZlciBieSB0aGUgdXNlcidzIG1vdXNlXHJcbiAgICBwcml2YXRlIGhleEhvdmVyWDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBoZXhIb3Zlclk6IG51bWJlcjtcclxuICAgIHByaXZhdGUgaGV4SG92ZXJWYWxpZDogYm9vbGVhbjtcclxuXHJcbiAgICAvLyBBbGwgaW5mb3JtYXRpb24gbmVlZGVkIHRvIGRyYXcgYSBoZXhhZ29uXHJcbiAgICAvLyBTb3VyY2VkIGZyb20gaHR0cHM6Ly9naXN0LmdpdGh1Yi5jb20vY0RlY2tlcjMyLzI1MDA4NjhcclxuICAgIHByaXZhdGUgaGV4YWdvbkFuZ2xlID0gMC41MjM1OTg3NzY7IC8vIDMwIGRlZ3JlZXMgaW4gcmFkaWFuc1xyXG4gICAgcHJpdmF0ZSBzaWRlTGVuZ3RoID0gNzI7XHJcbiAgICBwcml2YXRlIGhleEhlaWdodDogbnVtYmVyID0gTWF0aC5zaW4odGhpcy5oZXhhZ29uQW5nbGUpICogdGhpcy5zaWRlTGVuZ3RoO1xyXG4gICAgcHJpdmF0ZSBoZXhSYWRpdXM6IG51bWJlciA9IE1hdGguY29zKHRoaXMuaGV4YWdvbkFuZ2xlKSAqIHRoaXMuc2lkZUxlbmd0aDtcclxuICAgIHByaXZhdGUgaGV4UmVjdGFuZ2xlSGVpZ2h0OiBudW1iZXIgID0gdGhpcy5zaWRlTGVuZ3RoICsgMiAqIHRoaXMuaGV4SGVpZ2h0O1xyXG4gICAgcHJpdmF0ZSBoZXhSZWN0YW5nbGVXaWR0aDogbnVtYmVyICA9IDIgKiB0aGlzLmhleFJhZGl1cztcclxuXHJcbiAgICAvLyBUaGUgcGxheWVyIGltYWdlIHRvIGJlIGRyYXduIHdoZXJldmVyIHRoZXJlJ3MgYSBwbGF5ZXJcclxuICAgIC8vIFRPRE8gLSBpbmNsdWRlIGEgcGxheWVyIGltYWdlIGluIHRoZSBwbGF5ZXIncyBzdHViXHJcbiAgICBwcml2YXRlIHBsYXllckltYWdlID0gbmV3IEltYWdlKCk7XHJcblxyXG4gICAgLy8gTGlzdCBvZiBhcnJvd3MgdG8gZHJhdyBvbiB0aGUgR1VJXHJcbiAgICBwcml2YXRlIGRpc3BsYXlBcnJvd3M6IEFycmF5PE1hcENhbnZhc0Fycm93PjtcclxuXHJcbiAgICAvLyBJbml0aWFsaXNlIHRoZSBNYXBDYW52YXMgb2JqZWN0XHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IocHJpdmF0ZSBjb250cm9sbGVyOiBDb250cm9sbGVyKSB7XHJcbiAgICAgICAgdGhpcy5jYW52YXMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1hcC1jYW52YXNcIikgYXMgSFRNTENhbnZhc0VsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5oaXRDYW52YXMgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImhpdC1jYW52YXNcIikgYXMgSFRNTENhbnZhc0VsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5jdHggPSB0aGlzLmNhbnZhcy5nZXRDb250ZXh0KFwiMmRcIik7XHJcbiAgICAgICAgdGhpcy5oaXRDdHggPSB0aGlzLmhpdENhbnZhcy5nZXRDb250ZXh0KFwiMmRcIik7XHJcbiAgICAgICAgdGhpcy5yZXNpemUoKTtcclxuXHJcbiAgICAgICAgLy8gTG9hZCB0aGUgcGxheWVyIGltYWdlXHJcbiAgICAgICAgdGhpcy5wbGF5ZXJJbWFnZS5zcmMgPSBcImFzc2V0cy9TdGlja21hbiAxLnBuZ1wiO1xyXG5cclxuICAgICAgICAvLyBJbml0aWFsaXNlIHRoZSBsaXN0IG9mIGFycm93c1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUFycm93cyA9IG5ldyBBcnJheTxNYXBDYW52YXNBcnJvdz4oKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBEaXNhYmxlIGFmdGVyIG15IHBsYXllciBkaWVzIHRvIGRpc2FsbG93IG1vdXNlIGlucHV0XHJcbiAgICBwdWJsaWMgZGlzYWJsZSgpIHtcclxuICAgICAgICB0aGlzLmNhbnZhcy5zdHlsZS5wb2ludGVyRXZlbnRzID0gXCJub25lXCI7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFuaW1hdGVNb3ZlbWVudChvbGRQbGF5ZXJEZXRhaWxzOiB7IFtpZDogbnVtYmVyXTogUGxheWVyRGV0YWlscyB9LCBjYWxsYmFjazogKCgpID0+IHZvaWQpKSB7XHJcbiAgICAgICAgY29uc3QgbmV3UGxheWVyRGV0YWlscyA9IHRoaXMuY29udHJvbGxlci5wbGF5ZXJEZXRhaWxzO1xyXG4gICAgICAgIGxldCBpbnRlcnBvbGF0ZWREZXRhaWxzID0ge307XHJcbiAgICAgICAgZm9yKGNvbnN0IHBsYXllcklEIGluIG5ld1BsYXllckRldGFpbHMpIHtcclxuICAgICAgICAgICAgaW50ZXJwb2xhdGVkRGV0YWlsc1twbGF5ZXJJRF0gPSBPYmplY3QuYXNzaWduKHt9LCBuZXdQbGF5ZXJEZXRhaWxzW3BsYXllcklEXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGFuaW1EdXJhdGlvbiA9IDE0MDA7XHJcblxyXG4gICAgICAgIHRoaXMuY29udHJvbGxlci5zdGFydFRpbWVyKGFuaW1EdXJhdGlvbi8xMDAwLCAodGltZUxlZnQsIHRpbWVvdXRJRCkgPT4ge1xyXG4gICAgICAgICAgICAvLyBFdmVyeSBpbnRlcnZhbCBzZWNvbmRzLCByZWRyYXcgbWFwIHdpdGggaW50ZXJwb2xhdGVkIHBsYXllciBjby1vcmRpbmF0ZXNcclxuICAgICAgICAgICAgZm9yKGNvbnN0IHBsYXllcklEIGluIG5ld1BsYXllckRldGFpbHMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG5ld0RldGFpbHMgPSBuZXdQbGF5ZXJEZXRhaWxzW3BsYXllcklEXTtcclxuICAgICAgICAgICAgICAgIGxldCBzbGVycFg6IG51bWJlciA9IG5ld0RldGFpbHMueDtcclxuICAgICAgICAgICAgICAgIGxldCBzbGVycFk6IG51bWJlciA9IG5ld0RldGFpbHMueTtcclxuICAgICAgICAgICAgICAgIGlmKG9sZFBsYXllckRldGFpbHNbcGxheWVySURdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2xlcnBGYWN0b3IgPSAxIC0gKHRpbWVMZWZ0L2FuaW1EdXJhdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgb2xkRGV0YWlscyA9IG9sZFBsYXllckRldGFpbHNbcGxheWVySURdO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGR5ID0gb2xkRGV0YWlscy55IC0gbmV3RGV0YWlscy55O1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBvbGRYID0gb2xkRGV0YWlscy54O1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBuZXdYID0gbmV3RGV0YWlscy54O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZihvbGREZXRhaWxzLnkgJSAyID09PSAwICYmIGR5ICE9IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2xkWCAtPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYoZHkgPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKG9sZERldGFpbHMueSAlIDIgPT09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9sZFggKz0gMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5ld1ggLT0gMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2xlcnBYID0gKG9sZFgpICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgKHNsZXJwRmFjdG9yKihuZXdYLW9sZFgpKTtcclxuICAgICAgICAgICAgICAgICAgICBzbGVycFkgPSAob2xkRGV0YWlscy55KSArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChzbGVycEZhY3RvcioobmV3RGV0YWlscy55LW9sZERldGFpbHMueSkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaW50ZXJwb2xhdGVkRGV0YWlsc1twbGF5ZXJJRF0ueCA9IHNsZXJwWDtcclxuICAgICAgICAgICAgICAgIGludGVycG9sYXRlZERldGFpbHNbcGxheWVySURdLnkgPSBzbGVycFk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5kcmF3TWFwKGludGVycG9sYXRlZERldGFpbHMpO1xyXG4gICAgICAgIH0sIGNhbGxiYWNrLCBmYWxzZSwgNDApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBkcmF3TWFwKHBsYXllckRldGFpbHM6IHsgW2lkOiBudW1iZXJdOiBQbGF5ZXJEZXRhaWxzIH09dGhpcy5jb250cm9sbGVyLnBsYXllckRldGFpbHMpIHtcclxuICAgICAgICBpZighdGhpcy5jb250cm9sbGVyLm1hcCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGhleEdyaWQgPSB0aGlzLmNvbnRyb2xsZXIubWFwLmhleEdyaWQ7XHJcbiAgICAgICAgaWYoIWhleEdyaWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5jdHguZmlsbFN0eWxlID0gVGlsZVR5cGUudHlwZXNbMF0uY29sb3I7XHJcbiAgICAgICAgLy9jdHguY2xlYXJSZWN0KDAsIDAsIGNhbnZhcy53aWR0aCwgY2FudmFzLmhlaWdodCk7XHJcbiAgICAgICAgdGhpcy5jdHguZmlsbFJlY3QoMCwgMCwgdGhpcy5jYW52YXMud2lkdGgsIHRoaXMuY2FudmFzLmhlaWdodCk7XHJcbiAgICAgICAgdGhpcy5oaXRDdHguY2xlYXJSZWN0KDAsIDAsIHRoaXMuaGl0Q2FudmFzLndpZHRoLCB0aGlzLmhpdENhbnZhcy5oZWlnaHQpO1xyXG5cclxuICAgICAgICB0aGlzLmN0eC5zYXZlKCk7XHJcbiAgICAgICAgdGhpcy5jdHgudHJhbnNsYXRlKHRoaXMuem9vbVRhcmdldFgsIHRoaXMuem9vbVRhcmdldFkpO1xyXG4gICAgICAgIHRoaXMuY3R4LnNjYWxlKHRoaXMuem9vbUxldmVsLCB0aGlzLnpvb21MZXZlbCk7XHJcbiAgICAgICAgdGhpcy5oaXRDdHguc2F2ZSgpO1xyXG4gICAgICAgIHRoaXMuaGl0Q3R4LnRyYW5zbGF0ZSh0aGlzLnpvb21UYXJnZXRYLCB0aGlzLnpvb21UYXJnZXRZKTtcclxuICAgICAgICB0aGlzLmhpdEN0eC5zY2FsZSh0aGlzLnpvb21MZXZlbCwgdGhpcy56b29tTGV2ZWwpO1xyXG5cclxuICAgICAgICAvLyBnZXQgYSByZWZlcmVuY2UgdG8gbXkgcGxheWVyXHJcbiAgICAgICAgY29uc3QgbXlEZXRhaWxzID0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyRGV0YWlscztcclxuXHJcbiAgICAgICAgLy8gZHJhdyB0aGUgaGV4IGdyaWRcclxuICAgICAgICBmb3IodmFyIGogPSAwOyBqIDwgaGV4R3JpZC5sZW5ndGg7IGorKykge1xyXG4gICAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgaGV4R3JpZFtqXS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kcmF3VGlsZShpLCBqLCBoZXhHcmlkLCB0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYobXlEZXRhaWxzKSB7XHJcbiAgICAgICAgICAgIC8vIGRyYXcgYSBzaGFkb3cgb3ZlciB0aGUgZW50aXJlIG1hcFxyXG4gICAgICAgICAgICB0aGlzLmN0eC5nbG9iYWxBbHBoYSA9IDAuNjtcclxuICAgICAgICAgICAgdGhpcy5jdHguZmlsbFN0eWxlID0gXCIjMDAwXCI7XHJcbiAgICAgICAgICAgIHRoaXMuY3R4LmZpbGxSZWN0KC10aGlzLnpvb21UYXJnZXRYL3RoaXMuem9vbUxldmVsLCAtdGhpcy56b29tVGFyZ2V0WS90aGlzLnpvb21MZXZlbCwgdGhpcy5jYW52YXMud2lkdGgvdGhpcy56b29tTGV2ZWwsIHRoaXMuY2FudmFzLmhlaWdodC90aGlzLnpvb21MZXZlbCk7XHJcbiAgICAgICAgICAgIHRoaXMuY3R4Lmdsb2JhbEFscGhhID0gMS4wO1xyXG4gICAgICAgICAgICAvLyBkZXRlcm1pbmUgdGhlIHdoaWNoIHRpbGVzIHRvIGRyYXcgdGhlIG91dGxpbmUgYXJvdW5kXHJcbiAgICAgICAgICAgIGxldCByYW5nZSA9IDE7ICAvLyBtb3ZlIHJhbmdlXHJcbiAgICAgICAgICAgIGlmKHRoaXMuY29udHJvbGxlci5nYW1lUGhhc2UgPT09IEdhbWVQaGFzZS5BQ1RJT05fUEhBU0UpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGl0ZW0gPSB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJFcXVpcHBlZEl0ZW0gP1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5pdGVtc1t0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJFcXVpcHBlZEl0ZW0uaXRlbUlEXSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWFyY2hSYW5nZSA9IDE7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBhdHRhY2tSYW5nZSA9IGl0ZW0gPyAoaXRlbSBhcyBHdW4pLm1heFJhbmdlIDogMDtcclxuICAgICAgICAgICAgICAgIHJhbmdlID0gTWF0aC5tYXgoc2VhcmNoUmFuZ2UsIGF0dGFja1JhbmdlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBkcmF3IGFuIG91dGxpbmUgYXJvdW5kIHRoZSBtb3ZhYmxlIHRpbGVzXHJcbiAgICAgICAgICAgIGNvbnN0IHZpZXdSYW5nZSA9IE1hdGgubWF4KDIgKyB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJNb2RpZmllcnMudmlld1JhbmdlLCAwKTtcclxuICAgICAgICAgICAgLy8gYWN0aW9uYWJsZSByYW5nZSBtdXN0IGJlIDw9IHZpZXcgcmFuZ2VcclxuICAgICAgICAgICAgcmFuZ2UgPSBNYXRoLm1pbihyYW5nZSwgdmlld1JhbmdlKTtcclxuICAgICAgICAgICAgY29uc3QgZ2FwID0gdmlld1JhbmdlLXJhbmdlO1xyXG4gICAgICAgICAgICB0aGlzLmRyYXdPdXRlclRpbGVzSW5WaWV3UmFuZ2UobXlEZXRhaWxzLCBoZXhHcmlkLCBnYXApO1xyXG4gICAgICAgICAgICB0aGlzLmRyYXdUaWxlT3V0bGluZXMobXlEZXRhaWxzLCByYW5nZSwgaGV4R3JpZCk7XHJcbiAgICAgICAgICAgIC8vIHJlZHJhdyB0aGUgcGFydHMgb2YgdGhlIG1hcCBpbiB0aGUgcGxheWVycyB2aXNpb25cclxuICAgICAgICAgICAgdGhpcy5kcmF3SW5uZXJUaWxlc0luVmlld1JhbmdlKG15RGV0YWlscywgaGV4R3JpZCwgZ2FwKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGRyYXcgdGhlIHRpbGUgc2VsZWN0ZWQgY2lyY2xlXHJcbiAgICAgICAgaWYodGhpcy5oZXhTZWxlY3RlZFggPj0gMCAmJiB0aGlzLmhleFNlbGVjdGVkWSA+PSAwKSB7XHJcbiAgICAgICAgICAgIHZhciB4ID0gdGhpcy5oZXhTZWxlY3RlZFggKiB0aGlzLmhleFJlY3RhbmdsZVdpZHRoICsgKHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGgvMiAqICh0aGlzLmhleFNlbGVjdGVkWSAlIDIpKSArIHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGgvMjtcclxuICAgICAgICAgICAgdmFyIHkgPSB0aGlzLmhleFNlbGVjdGVkWSAqIHRoaXMuaGV4UmVjdGFuZ2xlSGVpZ2h0IC0gKHRoaXMuc2lkZUxlbmd0aC8yICogdGhpcy5oZXhTZWxlY3RlZFkpICsgdGhpcy5oZXhSZWN0YW5nbGVIZWlnaHQvMjtcclxuICAgICAgICAgICAgdGhpcy5jdHguc2F2ZSgpO1xyXG4gICAgICAgICAgICB0aGlzLmN0eC5zdHJva2VTdHlsZSA9IFwiI0FERkYyRlwiO1xyXG4gICAgICAgICAgICB0aGlzLmN0eC5zaGFkb3dDb2xvciA9IFwiYmxhY2tcIjtcclxuICAgICAgICAgICAgdGhpcy5jdHguc2hhZG93Qmx1ciA9IDE1O1xyXG4gICAgICAgICAgICB0aGlzLmN0eC5zaGFkb3dPZmZzZXRYID0gMDtcclxuICAgICAgICAgICAgdGhpcy5jdHguc2hhZG93T2Zmc2V0WSA9IDA7XHJcbiAgICAgICAgICAgIHRoaXMuY3R4LmxpbmVXaWR0aCA9IDIgLyB0aGlzLnpvb21MZXZlbDtcclxuICAgICAgICAgICAgdGhpcy5kcmF3Q2lyY2xlKHRoaXMuY3R4LCB4LCB5LCB0aGlzLmhleFJhZGl1cyk7XHJcbiAgICAgICAgICAgIHRoaXMuY3R4LnN0cm9rZSgpO1xyXG4gICAgICAgICAgICB0aGlzLmN0eC5yZXN0b3JlKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBkcmF3IHRoZSBtb3VzZSBob3ZlciBjaXJjbGUgJiB0aGUgaG92ZXIgdGlsZSB0b29sdGlwXHJcbiAgICAgICAgaWYodGhpcy5oZXhIb3ZlclggPj0gMCAmJiB0aGlzLmhleEhvdmVyWSA+PSAwKSB7XHJcbiAgICAgICAgICAgIHZhciB4ID0gdGhpcy5oZXhIb3ZlclggKiB0aGlzLmhleFJlY3RhbmdsZVdpZHRoICsgKHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGgvMiAqICh0aGlzLmhleEhvdmVyWSAlIDIpKSArIHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGgvMjtcclxuICAgICAgICAgICAgdmFyIHkgPSB0aGlzLmhleEhvdmVyWSAqIHRoaXMuaGV4UmVjdGFuZ2xlSGVpZ2h0IC0gKHRoaXMuc2lkZUxlbmd0aC8yICogdGhpcy5oZXhIb3ZlclkpICsgdGhpcy5oZXhSZWN0YW5nbGVIZWlnaHQvMjtcclxuICAgICAgICAgICAgdGhpcy5jdHguc2F2ZSgpO1xyXG4gICAgICAgICAgICB0aGlzLmN0eC5zdHJva2VTdHlsZSA9IHRoaXMuaGV4SG92ZXJWYWxpZCA/IFwiIzM0OThkYlwiIDogXCIjZTc0YzNjXCI7XHJcbiAgICAgICAgICAgIHRoaXMuY3R4LnNoYWRvd0NvbG9yID0gXCJibGFja1wiO1xyXG4gICAgICAgICAgICB0aGlzLmN0eC5zaGFkb3dCbHVyID0gMTU7XHJcbiAgICAgICAgICAgIHRoaXMuY3R4LnNoYWRvd09mZnNldFggPSAwO1xyXG4gICAgICAgICAgICB0aGlzLmN0eC5zaGFkb3dPZmZzZXRZID0gMDtcclxuICAgICAgICAgICAgdGhpcy5jdHgubGluZVdpZHRoID0gMiAvIHRoaXMuem9vbUxldmVsO1xyXG4gICAgICAgICAgICB0aGlzLmRyYXdDaXJjbGUodGhpcy5jdHgsIHgsIHksIHRoaXMuaGV4UmFkaXVzKTtcclxuICAgICAgICAgICAgdGhpcy5jdHguc3Ryb2tlKCk7XHJcbiAgICAgICAgICAgIHRoaXMuY3R4LnJlc3RvcmUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGRyYXcgYWxsIHRoZSBwbGF5ZXJzIG15IHBsYXllciBrbm93cyB0aGUgbG9jYXRpb24gb2ZcclxuICAgICAgICBmb3IoY29uc3QgcGxheWVySUQgaW4gcGxheWVyRGV0YWlscykge1xyXG4gICAgICAgICAgICBjb25zdCBkZXRhaWxzID0gcGxheWVyRGV0YWlsc1twbGF5ZXJJRF07XHJcbiAgICAgICAgICAgIGlmKGRldGFpbHMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1vZHVsb011bHRpcGxpZXIgPSBkZXRhaWxzLnkgJSAyO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgeCA9IGRldGFpbHMueCAqIHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGggKyAodGhpcy5oZXhSZWN0YW5nbGVXaWR0aC8yICogbW9kdWxvTXVsdGlwbGllcikgKyB0aGlzLmhleFJlY3RhbmdsZVdpZHRoLzI7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB5ID0gZGV0YWlscy55ICogdGhpcy5oZXhSZWN0YW5nbGVIZWlnaHQgLSAodGhpcy5zaWRlTGVuZ3RoLzIgKiBkZXRhaWxzLnkpICsgdGhpcy5oZXhSZWN0YW5nbGVIZWlnaHQvMjtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGltZ1dpZHRoID0gdGhpcy5oZXhSZWN0YW5nbGVIZWlnaHQgKiAwLjc1ICogMTA4MCAvIDE5MjA7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBpbWdIZWlnaHQgPSB0aGlzLmhleFJlY3RhbmdsZUhlaWdodCAqIDAuNzU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN0eC5kcmF3SW1hZ2UodGhpcy5wbGF5ZXJJbWFnZSwgeC1pbWdXaWR0aC8yLCB5LWltZ0hlaWdodC8yLCBpbWdXaWR0aCwgaW1nSGVpZ2h0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gZHJhdyBhbGwgdGhlIGRpc3BsYXkgYXJyb3dzXHJcbiAgICAgICAgZm9yKGNvbnN0IGFycm93IG9mIHRoaXMuZGlzcGxheUFycm93cykge1xyXG4gICAgICAgICAgICBjb25zdCB4MSA9IGFycm93LmZyb21YICogdGhpcy5oZXhSZWN0YW5nbGVXaWR0aCArICh0aGlzLmhleFJlY3RhbmdsZVdpZHRoLzIgKiAoYXJyb3cuZnJvbVkgJSAyKSkgKyB0aGlzLmhleFJlY3RhbmdsZVdpZHRoLzI7XHJcbiAgICAgICAgICAgIGNvbnN0IHkxID0gYXJyb3cuZnJvbVkgKiB0aGlzLmhleFJlY3RhbmdsZUhlaWdodCAtICh0aGlzLnNpZGVMZW5ndGgvMiAqIGFycm93LmZyb21ZKSArIHRoaXMuaGV4UmVjdGFuZ2xlSGVpZ2h0LzI7XHJcbiAgICAgICAgICAgIGNvbnN0IHg0ID0gYXJyb3cudG9YICogdGhpcy5oZXhSZWN0YW5nbGVXaWR0aCArICh0aGlzLmhleFJlY3RhbmdsZVdpZHRoLzIgKiAoYXJyb3cudG9ZICUgMikpICsgdGhpcy5oZXhSZWN0YW5nbGVXaWR0aC8yO1xyXG4gICAgICAgICAgICBjb25zdCB5NCA9IGFycm93LnRvWSAqIHRoaXMuaGV4UmVjdGFuZ2xlSGVpZ2h0IC0gKHRoaXMuc2lkZUxlbmd0aC8yICogYXJyb3cudG9ZKSArIHRoaXMuaGV4UmVjdGFuZ2xlSGVpZ2h0LzI7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmN0eC5zdHJva2VTdHlsZSA9IFwiIzAwMFwiO1xyXG4gICAgICAgICAgICB0aGlzLmN0eC5iZWdpblBhdGgoKTtcclxuICAgICAgICAgICAgdGhpcy5jdHgubW92ZVRvKHgxLCB5MSk7XHJcbiAgICAgICAgICAgIHRoaXMuY3R4LmxpbmVUbyh4NCwgeTQpO1xyXG4gICAgICAgICAgICB0aGlzLmN0eC5zdHJva2UoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKG15RGV0YWlscykge1xyXG4gICAgICAgICAgICAvLyBkcmF3IGFsbCBpdGVtIG1lbWVudG8gYnViYmxlc1xyXG4gICAgICAgICAgICBmb3IoY29uc3QgbSBvZiB0aGlzLmNvbnRyb2xsZXIuaXRlbU1lbWVudG9zKSB7XHJcbiAgICAgICAgICAgICAgICBpZighKG0ubWFwWCA9PT0gbXlEZXRhaWxzLnggJiYgbS5tYXBZID09PSBteURldGFpbHMueSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBkcmF3IHRoZSBpdGVtIGluZm8gYnViYmxlXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5kcmF3SXRlbUJ1YmJsZShtKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5jdHgucmVzdG9yZSgpO1xyXG4gICAgICAgIHRoaXMuaGl0Q3R4LnJlc3RvcmUoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRyYXdUaWxlKGksIGosIGhleEdyaWQsIGRyYXdUb0hpdD1mYWxzZSkge1xyXG4gICAgICAgIGlmKGogPCAwIHx8IGogPj0gaGV4R3JpZC5sZW5ndGggfHwgaSA8IDAgfHwgaSA+PSBoZXhHcmlkW2pdLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB0aWxlVHlwZSA9IFRpbGVUeXBlLnR5cGVzW2hleEdyaWRbal1baV0udHlwZV07XHJcblxyXG4gICAgICAgIHZhciB4ID0gaSAqIHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGggKyAodGhpcy5oZXhSZWN0YW5nbGVXaWR0aC8yICogKGogJSAyKSk7XHJcbiAgICAgICAgdmFyIHkgPSBqICogdGhpcy5oZXhSZWN0YW5nbGVIZWlnaHQgLSAodGhpcy5zaWRlTGVuZ3RoLzIgKiBqKTtcclxuXHJcbiAgICAgICAgLy8gZHJhdyB2aXNpYmxlIGhleGFnb25cclxuICAgICAgICB0aGlzLmN0eC5maWxsU3R5bGUgPSB0aWxlVHlwZS5jb2xvcjtcclxuICAgICAgICB0aGlzLmN0eC5zdHJva2VTdHlsZSA9IFwiIzk5OVwiO1xyXG4gICAgICAgIHRoaXMuZHJhd0hleGFnb24odGhpcy5jdHgsIHgsIHkpO1xyXG4gICAgICAgIHRoaXMuY3R4LmZpbGwoKTtcclxuICAgICAgICB0aGlzLmN0eC5zdHJva2UoKTtcclxuXHJcbiAgICAgICAgLy8gZHJhdyB0aWxlIGltYWdlIG92ZXIgdG9wIG9mIGhleGFnb25cclxuICAgICAgICBpZih0aWxlVHlwZS5pbWcpIHtcclxuICAgICAgICAgICAgdmFyIGltZ1dpZHRoID0gdGhpcy5oZXhSZWN0YW5nbGVIZWlnaHQ7XHJcbiAgICAgICAgICAgIHZhciBpbWdIZWlnaHQgPSB0aGlzLmhleFJlY3RhbmdsZUhlaWdodDtcclxuICAgICAgICAgICAgaWYoaW1nV2lkdGggPiB0aGlzLmhleFJlY3RhbmdsZVdpZHRoKSB7XHJcbiAgICAgICAgICAgICAgICBpbWdXaWR0aCA9IHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGg7XHJcbiAgICAgICAgICAgICAgICBpbWdIZWlnaHQgPSB0aGlzLmhleFJlY3RhbmdsZVdpZHRoO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuY3R4LmRyYXdJbWFnZSh0aWxlVHlwZS5pbWcsIHgsIHksIGltZ1dpZHRoLCBpbWdIZWlnaHQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gZHJhdyBzdG9ybSBvdmVyIHRpbGUgaWYgdGlsZSBpcyBpbiB0aGUgc3Rvcm1cclxuICAgICAgICBjb25zdCBzdG9ybVJhZGl1cyA9IHRoaXMuY29udHJvbGxlci5zdG9ybVJhZGl1cztcclxuICAgICAgICBpZihzdG9ybVJhZGl1cyA+PSAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0b3JtWCA9IE1hdGguZmxvb3IoaGV4R3JpZC5sZW5ndGgvMik7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0b3JtWSA9IE1hdGguZmxvb3IoaGV4R3JpZFswXS5sZW5ndGgvMik7XHJcbiAgICAgICAgLy8gICAgY29uc3QgaU9mZnNldCA9IChzdG9ybVgrc3Rvcm1ZJTIpIC0gKGkraiUyKTtcclxuICAgICAgICAvLyAgICBjb25zdCBqT2Zmc2V0ID0gc3Rvcm1ZIC0gajtcclxuICAgICAgICAgICAgaWYoIXRoaXMuaXNQb2ludFdpdGhpbkhleGFnb24oaSwgaiwgc3Rvcm1SYWRpdXMsIHN0b3JtWCwgc3Rvcm1ZKSkge1xyXG4gICAgICAgICAgICAvL2lmKGlPZmZzZXQqaU9mZnNldCArIGpPZmZzZXQqak9mZnNldCA+IHN0b3JtUmFkaXVzKnN0b3JtUmFkaXVzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN0eC5maWxsU3R5bGUgPSBcIiNmZmE1MDBcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3R4Lmdsb2JhbEFscGhhID0gMC42O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kcmF3SGV4YWdvbih0aGlzLmN0eCwgeCwgeSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN0eC5maWxsKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN0eC5nbG9iYWxBbHBoYSA9IDEuMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYoZHJhd1RvSGl0KSB7XHJcbiAgICAgICAgICAgIC8vIGRyYXcgaGl0Ym94IGhleGFnb25cclxuICAgICAgICAgICAgdGhpcy5oaXRDdHguZmlsbFN0eWxlID0gaGV4R3JpZFtqXVtpXS5jb2xvcktleTtcclxuICAgICAgICAgICAgdGhpcy5kcmF3SGV4YWdvbih0aGlzLmhpdEN0eCwgeCwgeSk7XHJcbiAgICAgICAgICAgIHRoaXMuaGl0Q3R4LmZpbGwoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBkcmF3SGV4YWdvbihjb250ZXh0LCB4LCB5KSB7XHJcbiAgICAgICAgY29udGV4dC5iZWdpblBhdGgoKTtcclxuICAgICAgICBjb250ZXh0Lm1vdmVUbyh4ICsgdGhpcy5oZXhSYWRpdXMsIHkpO1xyXG4gICAgICAgIGNvbnRleHQubGluZVRvKHggKyB0aGlzLmhleFJlY3RhbmdsZVdpZHRoLCB5ICsgdGhpcy5oZXhIZWlnaHQpO1xyXG4gICAgICAgIGNvbnRleHQubGluZVRvKHggKyB0aGlzLmhleFJlY3RhbmdsZVdpZHRoLCB5ICsgdGhpcy5oZXhIZWlnaHQgKyB0aGlzLnNpZGVMZW5ndGgpO1xyXG4gICAgICAgIGNvbnRleHQubGluZVRvKHggKyB0aGlzLmhleFJhZGl1cywgeSArIHRoaXMuaGV4UmVjdGFuZ2xlSGVpZ2h0KTtcclxuICAgICAgICBjb250ZXh0LmxpbmVUbyh4LCB5ICsgdGhpcy5zaWRlTGVuZ3RoICsgdGhpcy5oZXhIZWlnaHQpO1xyXG4gICAgICAgIGNvbnRleHQubGluZVRvKHgsIHkgKyB0aGlzLmhleEhlaWdodCk7XHJcbiAgICAgICAgY29udGV4dC5jbG9zZVBhdGgoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRyYXdDaXJjbGUoY29udGV4dCwgeCwgeSwgcikge1xyXG4gICAgICAgIGNvbnRleHQuYmVnaW5QYXRoKCk7XHJcbiAgICAgICAgY29udGV4dC5hcmMoeCwgeSwgciwgMCwgMipNYXRoLlBJKTtcclxuICAgICAgICBjb250ZXh0LmNsb3NlUGF0aCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZHJhd0l0ZW1CdWJibGUobTogSXRlbU1lbWVudG8pIHtcclxuICAgICAgICBjb25zdCBpID0gbS5tYXBYO1xyXG4gICAgICAgIGNvbnN0IGogPSBtLm1hcFk7XHJcbiAgICAgICAgY29uc3QgeCA9IGkgKiB0aGlzLmhleFJlY3RhbmdsZVdpZHRoICsgKHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGgvMiAqIChqJTIpKSArIHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGgvMjtcclxuICAgICAgICBjb25zdCB5ID0gaiAqIHRoaXMuaGV4UmVjdGFuZ2xlSGVpZ2h0IC0gKHRoaXMuc2lkZUxlbmd0aC8yICogaikgKyAodGhpcy5oZXhSYWRpdXMvNCk7XHJcbiAgICAgICAgY29uc3QgYm90WSA9IHkgKyB0aGlzLmhleFJhZGl1cztcclxuICAgICAgICBjb25zdCByYWRpdXMgPSB0aGlzLmhleFJhZGl1cy8yO1xyXG4gICAgICAgIC8vIG1hdGhzIGNsYXNzIHBhaWQgb2ZmXHJcbiAgICAgICAgY29uc3QgeU9mZnNldCA9IHJhZGl1cyAqIE1hdGguc2luKE1hdGguUEkvNCk7XHJcbiAgICAgICAgY29uc3QgeE9mZnNldCA9IE1hdGguc3FydChyYWRpdXMqcmFkaXVzIC0geU9mZnNldCp5T2Zmc2V0KTtcclxuXHJcbiAgICAgICAgLy8gZHJhdyB0aGUgaWNvblxyXG4gICAgICAgIHRoaXMuY3R4LnNhdmUoKTtcclxuICAgICAgICB0aGlzLmN0eC5maWxsU3R5bGUgPSBcIiM1NTVcIjtcclxuICAgICAgICB0aGlzLmN0eC5zdHJva2VTdHlsZSA9IFwiI2ZmZlwiO1xyXG4gICAgICAgIHRoaXMuY3R4Lmdsb2JhbEFscGhhID0gMC45O1xyXG4gICAgICAgIHRoaXMuY3R4LmJlZ2luUGF0aCgpO1xyXG4gICAgICAgIHRoaXMuY3R4LmFyYyh4LCB5LCByYWRpdXMsIE1hdGguUEkvNCwgLTEuMjUqTWF0aC5QSSwgdHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5jdHguYmV6aWVyQ3VydmVUbyh4LXhPZmZzZXQvMiwgKHkrYm90WSkvMiwgeCwgYm90WSwgeCwgYm90WSk7XHJcbiAgICAgICAgdGhpcy5jdHguYmV6aWVyQ3VydmVUbyh4LCBib3RZLCB4K3hPZmZzZXQvMiwgKHkrYm90WSkvMiwgeCt4T2Zmc2V0LCB5K3lPZmZzZXQpO1xyXG4gICAgICAgIHRoaXMuY3R4LmZpbGwoKTtcclxuICAgICAgICB0aGlzLmN0eC5nbG9iYWxBbHBoYSA9IDEuMDtcclxuICAgICAgICB0aGlzLmN0eC5zdHJva2UoKTtcclxuICAgICAgICB0aGlzLmN0eC5yZXN0b3JlKCk7XHJcblxyXG4gICAgICAgIC8vIGRyYXcgdGhlIHRleHRcclxuICAgICAgICB0aGlzLmN0eC5maWxsU3R5bGUgPSBcIiMwMGZmMDBcIjtcclxuICAgICAgICB0aGlzLmN0eC50ZXh0QWxpZ24gPSBcImNlbnRlclwiO1xyXG4gICAgICAgIHRoaXMuY3R4LnRleHRCYXNlbGluZSA9IFwibWlkZGxlXCI7XHJcbiAgICAgICAgdGhpcy5jdHguZm9udCA9IGAke3JhZGl1cyoxLjV9cHggQ2FsaWJyaWA7XHJcbiAgICAgICAgdGhpcy5jdHguZmlsbFRleHQoXCJcIittLnNpbmNlLCB4LCB5KTtcclxuXHJcbiAgICAgICAgLy8gZHJhdyBhIGNpcmNsZSB0byB0aGUgaGl0IGNvbnRleHRcclxuICAgICAgICB0aGlzLmhpdEN0eC5maWxsU3R5bGUgPSBtLmNvbG9yS2V5O1xyXG4gICAgICAgIHRoaXMuZHJhd0NpcmNsZSh0aGlzLmhpdEN0eCwgeCwgeSwgcmFkaXVzKTtcclxuICAgICAgICB0aGlzLmhpdEN0eC5maWxsKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBkcmF3T3V0ZXJUaWxlc0luVmlld1JhbmdlKG15RGV0YWlscywgaGV4R3JpZCwgZ2FwOiBudW1iZXIpIHtcclxuICAgICAgICBjb25zdCB2aWV3UmFuZ2UgPSBNYXRoLm1heCgyICsgdGhpcy5jb250cm9sbGVyLm15UGxheWVyTW9kaWZpZXJzLnZpZXdSYW5nZSwgMCk7XHJcbiAgICAgICAgY29uc3QgZGlhbWV0ZXIgPSB2aWV3UmFuZ2UgKiAyICsgMTtcclxuICAgICAgICBjb25zdCB4T2Zmc2V0ID0gbXlEZXRhaWxzLnkgJSAyO1xyXG4gICAgICAgIGNvbnN0IHN0YXJ0WSA9IG15RGV0YWlscy55LXZpZXdSYW5nZTtcclxuICAgICAgICBjb25zdCBlbmRZID0gbXlEZXRhaWxzLnkrdmlld1JhbmdlO1xyXG4gICAgICAgIGZvcihsZXQgaiA9IHN0YXJ0WTsgaiA8PSBlbmRZOyBqKyspIHtcclxuICAgICAgICAgICAgLy8gY2FsY3VsYXRlIHRoZSBsZW5ndGggb2YgdGhpcyByb3cgYmFzZWQgb24gdGhlIHZlcnRpY2FsIGRpc3RhbmNlIGZyb20gdGhlIGNlbnRlciB0aWxlXHJcbiAgICAgICAgICAgIGNvbnN0IHZEaXN0VG9DZW50ZXIgPSBNYXRoLmFicyhqIC0gbXlEZXRhaWxzLnkpO1xyXG4gICAgICAgICAgICBjb25zdCByb3dMZW5ndGggPSBkaWFtZXRlciAtIHZEaXN0VG9DZW50ZXI7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvd1hPZmZzZXQgPSAodkRpc3RUb0NlbnRlciAlIDIpICogeE9mZnNldDtcclxuICAgICAgICAgICAgY29uc3Qgc3RhcnRYID0gbXlEZXRhaWxzLnggLSBNYXRoLmZsb29yKHJvd0xlbmd0aC8yKTtcclxuICAgICAgICAgICAgLy8gaWYgdGhlIHJvdyBpcyBpbiB0aGUgb3V0ZXIgcmFuZ2UsIGRyYXcgd2hvbGUgcm93LCBlbHNlLCBkcmF3IGxlZnQgYW5kIHJpZ2h0IHNpZGUgb25seVxyXG4gICAgICAgICAgICBpZihqIDwgc3RhcnRZICsgZ2FwIHx8IGogPiBlbmRZLWdhcCkge1xyXG4gICAgICAgICAgICAgICAgLy8gZHJhdyB3aG9sZSByb3dcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgaSA9IHN0YXJ0WDsgaSA8IHN0YXJ0WCtyb3dMZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZHJhd1RpbGUoaStyb3dYT2Zmc2V0LCBqLCBoZXhHcmlkKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIGRyYXcgdGhlIGxlZnQgc2lkZSBvZiB0aGUgY3VycmVudCByb3dcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgaSA9IHN0YXJ0WDsgaSA8IHN0YXJ0WCtnYXA7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZHJhd1RpbGUoaStyb3dYT2Zmc2V0LCBqLCBoZXhHcmlkKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIGRyYXcgdGhlIHJpZ2h0IHNpZGUgb2YgdGhlIGN1cnJlbnQgcm93XHJcbiAgICAgICAgICAgICAgICBmb3IobGV0IGkgPSBzdGFydFgrcm93TGVuZ3RoLWdhcDsgaSA8IHN0YXJ0WCtyb3dMZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZHJhd1RpbGUoaStyb3dYT2Zmc2V0LCBqLCBoZXhHcmlkKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRyYXdJbm5lclRpbGVzSW5WaWV3UmFuZ2UobXlEZXRhaWxzLCBoZXhHcmlkLCBnYXA6IG51bWJlcikge1xyXG4gICAgICAgIC8vIGRyYXcgdGhlIHRpbGVzXHJcbiAgICAgICAgLy8gd293LCBpIGNhbid0IGJlbGlldmUgdGhpcyB3b3JrZWQgZmlyc3QgdHJ5Li4uXHJcbiAgICAgICAgLy8gLi4udGhpcyBtYXkgYmUgdGhlIGdyZWF0ZXN0IGFjaGlldmVtZW50IGluIHRoZSBoaXN0b3J5IG9mIG1hdGhlbWF0aWNzXHJcbiAgICAgICAgY29uc3Qgdmlld1JhbmdlID0gTWF0aC5tYXgoMiArIHRoaXMuY29udHJvbGxlci5teVBsYXllck1vZGlmaWVycy52aWV3UmFuZ2UsIDApO1xyXG4gICAgICAgIGNvbnN0IGRpYW1ldGVyID0gdmlld1JhbmdlICogMiArIDE7XHJcbiAgICAgICAgY29uc3QgeE9mZnNldCA9IG15RGV0YWlscy55ICUgMjtcclxuICAgICAgICBmb3IobGV0IGogPSBteURldGFpbHMueS12aWV3UmFuZ2UrZ2FwOyBqIDw9IG15RGV0YWlscy55K3ZpZXdSYW5nZS1nYXA7IGorKykge1xyXG4gICAgICAgICAgICAvLyBjYWxjdWxhdGUgdGhlIGxlbmd0aCBvZiB0aGlzIHJvdyBiYXNlZCBvbiB0aGUgdmVydGljYWwgZGlzdGFuY2UgZnJvbSB0aGUgY2VudGVyIHRpbGVcclxuICAgICAgICAgICAgY29uc3QgdkRpc3RUb0NlbnRlciA9IE1hdGguYWJzKGogLSBteURldGFpbHMueSk7XHJcbiAgICAgICAgICAgIGNvbnN0IHJvd0xlbmd0aCA9IGRpYW1ldGVyIC0gdkRpc3RUb0NlbnRlcjtcclxuICAgICAgICAgICAgY29uc3Qgcm93WE9mZnNldCA9ICh2RGlzdFRvQ2VudGVyICUgMikgKiB4T2Zmc2V0O1xyXG4gICAgICAgICAgICBjb25zdCBzdGFydFggPSBteURldGFpbHMueCAtIE1hdGguZmxvb3Iocm93TGVuZ3RoLzIpO1xyXG5cclxuICAgICAgICAgICAgLy8gZHJhdyB0aGUgY3VycmVudCByb3dcclxuICAgICAgICAgICAgZm9yKGxldCBpID0gc3RhcnRYK2dhcDsgaSA8IHN0YXJ0WCtyb3dMZW5ndGgtZ2FwOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZHJhd1RpbGUoaStyb3dYT2Zmc2V0LCBqLCBoZXhHcmlkKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRyYXdUaWxlT3V0bGluZXMobXlEZXRhaWxzLCByYW5nZSwgaGV4R3JpZCkge1xyXG4gICAgICAgIGNvbnN0IGNlbnRlclggPSBteURldGFpbHMueDtcclxuICAgICAgICBjb25zdCBjZW50ZXJZID0gbXlEZXRhaWxzLnk7XHJcbiAgICAgICAgY29uc3QgZGlhbWV0ZXIgPSByYW5nZSAqIDIgKyAxO1xyXG4gICAgICAgIGNvbnN0IHhPZmZzZXQgPSBjZW50ZXJZICUgMjtcclxuXHJcbiAgICAgICAgdGhpcy5jdHguc3Ryb2tlU3R5bGUgPSBcIiNmZmZcIjtcclxuICAgICAgICB0aGlzLmN0eC5nbG9iYWxBbHBoYSA9IDE7XHJcbiAgICAgICAgdGhpcy5jdHgubGluZVdpZHRoID0gNy41O1xyXG5cclxuICAgICAgICBmb3IobGV0IGogPSBjZW50ZXJZLXJhbmdlOyBqIDw9IGNlbnRlclkrcmFuZ2U7IGorKykge1xyXG4gICAgICAgICAgICAvLyBjYWxjdWxhdGUgdGhlIGxlbmd0aCBvZiB0aGlzIHJvdyBiYXNlZCBvbiB0aGUgdmVydGljYWwgZGlzdGFuY2UgZnJvbSB0aGUgY2VudGVyIHRpbGVcclxuICAgICAgICAgICAgY29uc3QgdkRpc3RUb0NlbnRlciA9IE1hdGguYWJzKGogLSBjZW50ZXJZKTtcclxuICAgICAgICAgICAgY29uc3Qgcm93TGVuZ3RoID0gZGlhbWV0ZXIgLSB2RGlzdFRvQ2VudGVyO1xyXG4gICAgICAgICAgICBjb25zdCByb3dYT2Zmc2V0ID0gKHZEaXN0VG9DZW50ZXIgJSAyKSAqIHhPZmZzZXQ7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXJ0WCA9IGNlbnRlclggLSBNYXRoLmZsb29yKHJvd0xlbmd0aC8yKTtcclxuXHJcbiAgICAgICAgICAgIGZvcihsZXQgaSA9IHN0YXJ0WDsgaSA8IHN0YXJ0WCtyb3dMZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgdmFyIHggPSAoaStyb3dYT2Zmc2V0KSAqIHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGggKyAodGhpcy5oZXhSZWN0YW5nbGVXaWR0aC8yICogKGogJSAyKSk7XHJcbiAgICAgICAgICAgICAgICB2YXIgeSA9IGogKiB0aGlzLmhleFJlY3RhbmdsZUhlaWdodCAtICh0aGlzLnNpZGVMZW5ndGgvMiAqIGopO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kcmF3SGV4YWdvbih0aGlzLmN0eCwgeCwgeSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN0eC5zdHJva2UoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5jdHgubGluZVdpZHRoID0gMTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzUG9pbnRXaXRoaW5IZXhhZ29uKHBvaW50WDogbnVtYmVyLCBwb2ludFk6IG51bWJlciwgb3V0ZXJSYWRpdXM6IG51bWJlciwgY2lyY2xlWDogbnVtYmVyLCBjaXJjbGVZOiBudW1iZXIpOiBib29sZWFuIHtcclxuICAgICAgICBjb25zdCBkaWFtZXRlciA9IG91dGVyUmFkaXVzICogMiArIDE7XHJcbiAgICAgICAgY29uc3QgeE9mZnNldCA9IGNpcmNsZVkgJSAyO1xyXG5cclxuICAgICAgICBmb3IobGV0IGogPSBjaXJjbGVZLW91dGVyUmFkaXVzOyBqIDw9IGNpcmNsZVkrb3V0ZXJSYWRpdXM7IGorKykge1xyXG4gICAgICAgICAgICAvLyBjYWxjdWxhdGUgdGhlIGxlbmd0aCBvZiB0aGlzIHJvdyBiYXNlZCBvbiB0aGUgdmVydGljYWwgZGlzdGFuY2UgZnJvbSB0aGUgY2VudGVyIHRpbGVcclxuICAgICAgICAgICAgY29uc3QgdkRpc3RUb0NlbnRlciA9IE1hdGguYWJzKGogLSBjaXJjbGVZKTtcclxuICAgICAgICAgICAgY29uc3Qgcm93TGVuZ3RoID0gZGlhbWV0ZXIgLSB2RGlzdFRvQ2VudGVyO1xyXG4gICAgICAgICAgICBjb25zdCByb3dYT2Zmc2V0ID0gKHZEaXN0VG9DZW50ZXIgJSAyKSAqIHhPZmZzZXQ7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXJ0WCA9IGNpcmNsZVggLSBNYXRoLmZsb29yKHJvd0xlbmd0aC8yKTtcclxuXHJcbiAgICAgICAgICAgIC8vIGNoZWNrIHdoZXRoZXIgaGV4YWdvbiB4ICYgeSBhcmUgd2l0aGluIHNlYXJjaCByYW5nZVxyXG4gICAgICAgICAgICBmb3IobGV0IGkgPSBzdGFydFg7IGkgPCBzdGFydFgrcm93TGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlmKHBvaW50WCA9PT0gaStyb3dYT2Zmc2V0ICYmIHBvaW50WSA9PT0gaikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkNsaWNrID0gKGU6IE1vdXNlRXZlbnQpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgY29uc3QgbXlEZXRhaWxzID0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyRGV0YWlscztcclxuICAgICAgICBjb25zdCBvYmpVbmRlck1vdXNlID0gdGhpcy5nZXRPYmplY3RVbmRlck1vdXNlKGUpO1xyXG4gICAgICAgIGNvbnN0IGhleEdyaWQgPSB0aGlzLmNvbnRyb2xsZXIubWFwLmhleEdyaWQ7XHJcblxyXG4gICAgICAgIGlmKGhleEdyaWQgJiYgb2JqVW5kZXJNb3VzZSAmJiAoXCJ4XCIgaW4gb2JqVW5kZXJNb3VzZSkpIHtcclxuICAgICAgICAgICAgY29uc3QgaGV4YWdvbiA9IG9ialVuZGVyTW91c2UgYXMgeyB4OiBudW1iZXIsIHk6IG51bWJlciB9O1xyXG4gICAgICAgICAgICAvLyBnZXQgdGhlIHRpbGUgdHlwZSBpZCBhbmQgdGlsZSB0eXBlXHJcbiAgICAgICAgICAgIGNvbnN0IHRpbGVUeXBlSUQgPSBoZXhHcmlkW2hleGFnb24ueV1baGV4YWdvbi54XS50eXBlO1xyXG4gICAgICAgICAgICBjb25zdCB0aWxlVHlwZSA9IFRpbGVUeXBlLnR5cGVzW3RpbGVUeXBlSURdO1xyXG5cclxuICAgICAgICAgICAgaWYodGhpcy5jb250cm9sbGVyLmdhbWVQaGFzZSA9PSBHYW1lUGhhc2UuRFJPUF9QSEFTRSkge1xyXG4gICAgICAgICAgICAgICAgLy8gRW5zdXJlIHRoZSB0aWxlIGNhbiBiZSBsYW5kZWQgb25cclxuICAgICAgICAgICAgICAgIGlmKCF0aWxlVHlwZS5pbXBhc3NhYmxlICYmICF0aWxlVHlwZS51bmRyb3BwYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGV4U2VsZWN0ZWRYID0gaGV4YWdvbi54O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGV4U2VsZWN0ZWRZID0gaGV4YWdvbi55O1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5zZXRNb3ZlVHVybih0aGlzLmhleFNlbGVjdGVkWCwgdGhpcy5oZXhTZWxlY3RlZFkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYodGhpcy5jb250cm9sbGVyLmdhbWVQaGFzZSA9PSBHYW1lUGhhc2UuTU9WRV9QSEFTRSkge1xyXG4gICAgICAgICAgICAgICAgaWYobXlEZXRhaWxzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZHggPSBteURldGFpbHMueCAtIGhleGFnb24ueDtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkeSA9IG15RGV0YWlscy55IC0gaGV4YWdvbi55O1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKCgoKGR5ID09IC0xIHx8IGR5ID09IDEpICYmIChteURldGFpbHMueSAlIDIgPT0gMSkgJiYgZHggPj0gLTEgJiYgZHggPD0gMCkgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAoKGR5ID09IC0xIHx8IGR5ID09IDEpICYmIChteURldGFpbHMueSAlIDIgPT0gMCkgJiYgZHggPj0gMCAmJiBkeCA8PSAxKSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgIChkeSA9PSAwICYmIGR4ID49IC0xICYmIGR4IDw9IDEpKSAmJiAhdGlsZVR5cGUuaW1wYXNzYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhleFNlbGVjdGVkWCA9IGhleGFnb24ueDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oZXhTZWxlY3RlZFkgPSBoZXhhZ29uLnk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5zZXRNb3ZlVHVybih0aGlzLmhleFNlbGVjdGVkWCwgdGhpcy5oZXhTZWxlY3RlZFkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25Nb3ZlVmFsaWQodGhpcy5oZXhTZWxlY3RlZFgsIHRoaXMuaGV4U2VsZWN0ZWRZLCBkeCA9PSBkeSAmJiBkeCA9PSAwKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZih0aGlzLmNvbnRyb2xsZXIuZ2FtZVBoYXNlID09IEdhbWVQaGFzZS5BQ1RJT05fUEhBU0UpIHtcclxuICAgICAgICAgICAgICAgIGlmKG15RGV0YWlscykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGl0ZW0gPSB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJFcXVpcHBlZEl0ZW0gP1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIuaXRlbXNbdGhpcy5jb250cm9sbGVyLm15UGxheWVyRXF1aXBwZWRJdGVtLml0ZW1JRF0gOiBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHNlYXJjaFJhbmdlID0gTWF0aC5tYXgoMSwgMCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYXR0YWNrUmFuZ2UgPSBNYXRoLm1heChpdGVtID8gKGl0ZW0gYXMgR3VuKS5tYXhSYW5nZSA6IDAsIDApO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNhblNlYXJjaCA9IHRoaXMuaXNQb2ludFdpdGhpbkhleGFnb24oaGV4YWdvbi54LCBoZXhhZ29uLnksIHNlYXJjaFJhbmdlLCBteURldGFpbHMueCwgbXlEZXRhaWxzLnkpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNhbkF0dGFjayA9IHRoaXMuY29udHJvbGxlci5pc1BsYXllck9uVGlsZShoZXhhZ29uLngsIGhleGFnb24ueSkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNQb2ludFdpdGhpbkhleGFnb24oaGV4YWdvbi54LCBoZXhhZ29uLnksIGF0dGFja1JhbmdlLCBteURldGFpbHMueCwgbXlEZXRhaWxzLnkpICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJBUCA+PSBBY3Rpb25BcENvc3RzLkFUVEFDSztcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYoZS5idXR0b24gPT0gMikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBpZiByaWdodCBjbGljaywgc2hvdyB0aGUgY3VzdG9tIGNvbnRleHQgbWVudVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25UaWxlUmlnaHRDbGlja2VkQWN0aW9uUGhhc2UodGhpcy5sYXN0R2xvYmFsTW91c2VYLCB0aGlzLmxhc3RHbG9iYWxNb3VzZVksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZXhhZ29uLngsIGhleGFnb24ueSwgY2FuU2VhcmNoLCBjYW5BdHRhY2spO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZihlLmJ1dHRvbiA9PSAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGlmIGxlZnQgY2xpY2sgYW5kIGNhbiBhdHRhY2ssIGRldGVybWluZSB3aGljaCBwbGF5ZXIgaXMgb24gdGlsZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwbGF5ZXJJRHMgPSB0aGlzLmNvbnRyb2xsZXIuZ2V0UGxheWVyc09uVGlsZShoZXhhZ29uLngsIGhleGFnb24ueSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKGNhbkF0dGFjayAmJiBwbGF5ZXJJRHMubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25Jbml0aWF0ZUF0dGFja1BsYXllcihwbGF5ZXJJRHNbMF0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYoY2FuQXR0YWNrICYmIHBsYXllcklEcy5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25BdHRhY2tQbGF5ZXJPblRpbGUodGhpcy5sYXN0R2xvYmFsTW91c2VYLCB0aGlzLmxhc3RHbG9iYWxNb3VzZVksIGhleGFnb24ueCwgaGV4YWdvbi55KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmKGNhblNlYXJjaCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLnNlYXJjaFRpbGUoaGV4YWdvbi54LCBoZXhhZ29uLnkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIHJlcXVlc3QgdGhlIG1hcCBiZSByZWRyYXduIHdpdGggbmV3IHNlbGVjdGlvbiBpbmZvXHJcbiAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5yZXF1ZXN0RHJhd01hcCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbk1vdXNlTW92ZSA9IChlOiBNb3VzZUV2ZW50KSA9PiB7XHJcbiAgICAgICAgY29uc3QgbXlEZXRhaWxzID0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyRGV0YWlscztcclxuICAgICAgICBjb25zdCBvYmpVbmRlck1vdXNlID0gdGhpcy5nZXRPYmplY3RVbmRlck1vdXNlKGUpO1xyXG4gICAgICAgIGNvbnN0IGhleEdyaWQgPSB0aGlzLmNvbnRyb2xsZXIubWFwLmhleEdyaWQ7XHJcblxyXG4gICAgICAgIHRoaXMuY29udHJvbGxlci5nbG9iYWxNb3VzZVggPSB0aGlzLmxhc3RHbG9iYWxNb3VzZVggPSBlLmNsaWVudFg7XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLmdsb2JhbE1vdXNlWSA9IHRoaXMubGFzdEdsb2JhbE1vdXNlWSA9IGUuY2xpZW50WTtcclxuICAgICAgICB0aGlzLmxhc3RNb3VzZVggPSB0aGlzLmxhc3RHbG9iYWxNb3VzZVggLSB0aGlzLmNhbnZhcy5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5sZWZ0O1xyXG4gICAgICAgIHRoaXMubGFzdE1vdXNlWSA9IHRoaXMubGFzdEdsb2JhbE1vdXNlWSAtIHRoaXMuY2FudmFzLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLnRvcDtcclxuXHJcbiAgICAgICAgaWYoaGV4R3JpZCAmJiBvYmpVbmRlck1vdXNlICYmIChcInhcIiBpbiBvYmpVbmRlck1vdXNlKSkge1xyXG4gICAgICAgICAgICBjb25zdCBoZXhhZ29uID0gb2JqVW5kZXJNb3VzZSBhcyB7IHg6IG51bWJlciwgeTogbnVtYmVyIH07XHJcbiAgICAgICAgICAgIC8vIGdldCB0aGUgdGlsZSB0eXBlIGlkIGFuZCB0aWxlIHR5cGVcclxuICAgICAgICAgICAgY29uc3QgdGlsZVR5cGVJRCA9IGhleEdyaWRbaGV4YWdvbi55XVtoZXhhZ29uLnhdLnR5cGU7XHJcbiAgICAgICAgICAgIGNvbnN0IHRpbGVUeXBlID0gVGlsZVR5cGUudHlwZXNbdGlsZVR5cGVJRF07XHJcblxyXG4gICAgICAgICAgICB0aGlzLmhleEhvdmVyWCA9IGhleGFnb24ueDtcclxuICAgICAgICAgICAgdGhpcy5oZXhIb3ZlclkgPSBoZXhhZ29uLnk7XHJcblxyXG4gICAgICAgICAgICBpZih0aGlzLmNvbnRyb2xsZXIuZ2FtZVBoYXNlID09IEdhbWVQaGFzZS5EUk9QX1BIQVNFKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBub3RpZnkgdGhlIGNvbnRyb2xsZXIgYWJvdXQgdGlsZSBob3ZlciBzbyBhIHRvb2x0aXAgY2FuIGJlIGNyZWF0ZWRcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5vblRpbGVIb3ZlckRyb3BQaGFzZShoZXhhZ29uLngsIGhleGFnb24ueSwgdGhpcy5sYXN0R2xvYmFsTW91c2VYLCB0aGlzLmxhc3RHbG9iYWxNb3VzZVkpO1xyXG4gICAgICAgICAgICAgICAgLy8gRW5zdXJlIHRoZSB0aWxlIGNhbiBiZSBsYW5kZWQgb25cclxuICAgICAgICAgICAgICAgIGlmKCF0aWxlVHlwZS5pbXBhc3NhYmxlICYmICF0aWxlVHlwZS51bmRyb3BwYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGV4SG92ZXJWYWxpZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jYW52YXMuc3R5bGUuY3Vyc29yID0gXCJ1cmwoJ2Fzc2V0cy9Cb290c19JY29uLnBuZycpIDE2IDE2LCBhdXRvXCI7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGV4SG92ZXJWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2FudmFzLnN0eWxlLmN1cnNvciA9IFwidXJsKCdhc3NldHMvQ3Jvc3NfSWNvbi5wbmcnKSAxNiAxNiwgYXV0b1wiO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYodGhpcy5jb250cm9sbGVyLmdhbWVQaGFzZSA9PSBHYW1lUGhhc2UuTU9WRV9QSEFTRSkge1xyXG4gICAgICAgICAgICAgICAgLy8gbm90aWZ5IHRoZSBjb250cm9sbGVyIGFib3V0IHRoZSB0aWxlIGhvdmVyIHNvIGEgdG9vbHRpcCBjYW4gYmUgY3JlYXRlZFxyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uVGlsZUhvdmVyTW92ZVBoYXNlKGhleGFnb24ueCwgaGV4YWdvbi55LCB0aGlzLmxhc3RHbG9iYWxNb3VzZVgsIHRoaXMubGFzdEdsb2JhbE1vdXNlWSk7XHJcbiAgICAgICAgICAgICAgICBpZihteURldGFpbHMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkeCA9IG15RGV0YWlscy54IC0gaGV4YWdvbi54O1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGR5ID0gbXlEZXRhaWxzLnkgLSBoZXhhZ29uLnk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoKCgoZHkgPT0gLTEgfHwgZHkgPT0gMSkgJiYgKG15RGV0YWlscy55ICUgMiA9PSAxKSAmJiBkeCA+PSAtMSAmJiBkeCA8PSAwKSB8fFxyXG4gICAgICAgICAgICAgICAgICAgICAgICgoZHkgPT0gLTEgfHwgZHkgPT0gMSkgJiYgKG15RGV0YWlscy55ICUgMiA9PSAwKSAmJiBkeCA+PSAwICYmIGR4IDw9IDEpIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgICAgKGR5ID09IDAgJiYgZHggPj0gLTEgJiYgZHggPD0gMSkpICYmICF0aWxlVHlwZS5pbXBhc3NhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGV4SG92ZXJWYWxpZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FudmFzLnN0eWxlLmN1cnNvciA9IFwidXJsKCdhc3NldHMvQm9vdHNfSWNvbi5wbmcnKSAxNiAxNiwgYXV0b1wiO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGV4SG92ZXJWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNhbnZhcy5zdHlsZS5jdXJzb3IgPSBcInVybCgnYXNzZXRzL0Nyb3NzX0ljb24ucG5nJykgMTYgMTYsIGF1dG9cIjtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZih0aGlzLmNvbnRyb2xsZXIuZ2FtZVBoYXNlID09IEdhbWVQaGFzZS5BQ1RJT05fUEhBU0UpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaGV4SG92ZXJWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgaWYobXlEZXRhaWxzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaXRlbSA9IHRoaXMuY29udHJvbGxlci5teVBsYXllckVxdWlwcGVkSXRlbSA/XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5pdGVtc1t0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJFcXVpcHBlZEl0ZW0uaXRlbUlEXSA6IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc2VhcmNoUmFuZ2UgPSBNYXRoLm1heCgxLCAwKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBhdHRhY2tSYW5nZSA9IE1hdGgubWF4KGl0ZW0gPyAoaXRlbSBhcyBHdW4pLm1heFJhbmdlIDogMCwgMCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY2FuU2VhcmNoID0gdGhpcy5pc1BvaW50V2l0aGluSGV4YWdvbihoZXhhZ29uLngsIGhleGFnb24ueSwgc2VhcmNoUmFuZ2UsIG15RGV0YWlscy54LCBteURldGFpbHMueSk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY2FuQXR0YWNrID0gdGhpcy5jb250cm9sbGVyLmlzUGxheWVyT25UaWxlKGhleGFnb24ueCwgaGV4YWdvbi55KSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc1BvaW50V2l0aGluSGV4YWdvbihoZXhhZ29uLngsIGhleGFnb24ueSwgYXR0YWNrUmFuZ2UsIG15RGV0YWlscy54LCBteURldGFpbHMueSkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5teVBsYXllckFQID49IEFjdGlvbkFwQ29zdHMuQVRUQUNLO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBzZXQgdGhlIG1vdXNlIGN1cnNvciBpY29uIGJhc2VkIG9uIHdoYXQgaXMgYmVpbmcgaG92ZXJlZFxyXG4gICAgICAgICAgICAgICAgLy8gICAgY29uc29sZS5sb2coXCJDYW4gQXR0YWNrL1NlYXJjaFwiLCBjYW5BdHRhY2ssIGNhblNlYXJjaCwgdGhpcy5jb250cm9sbGVyLm15UGxheWVyQVApO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGNhblNlYXJjaCAmJiAhY2FuQXR0YWNrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY2FudmFzLnN0eWxlLmN1cnNvciA9IFwidXJsKCdhc3NldHMvc2VhcmNoX2ljb24ucG5nJykgMTYgMTYsIGF1dG9cIjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5oZXhIb3ZlclZhbGlkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gVE9ETyAtIGdldCBwcm9wZXIgYXAgY29zdHMgZnJvbSBzb21ld2hlcmVcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uVGlsZUhvdmVyQWN0aW9uUGhhc2UoXCJTZWFyY2hcIiwgXCJTZWFyY2ggY3VycmVudC9hZGphY2VudCB0aWxlIGZvciBtaW5lc1wiLCAxMCwgdGhpcy5sYXN0R2xvYmFsTW91c2VYLCB0aGlzLmxhc3RHbG9iYWxNb3VzZVkpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZihjYW5BdHRhY2spIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jYW52YXMuc3R5bGUuY3Vyc29yID0gXCJ1cmwoJ2Fzc2V0cy90YXJnZXRfaWNvbi5wbmcnKSAyMCAyMCwgYXV0b1wiO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhleEhvdmVyVmFsaWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBUT0RPIC0gZ2V0IHByb3BlciBhcCBjb3N0cyBmcm9tIHNvbWV3aGVyZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25UaWxlSG92ZXJBY3Rpb25QaGFzZShcIkF0dGFja1wiLCBcIkF0dGFjayBwbGF5ZXIgd2l0aCBjdXJyZW50bHkgZXF1aXBwZWQgd2VhcG9uXCIsIDYwLCB0aGlzLmxhc3RHbG9iYWxNb3VzZVgsIHRoaXMubGFzdEdsb2JhbE1vdXNlWSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jYW52YXMuc3R5bGUuY3Vyc29yID0gXCJ1cmwoJ2Fzc2V0cy9Dcm9zc19JY29uLnBuZycpIDE2IDE2LCBhdXRvXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5vblRpbGVIb3ZlckFjdGlvblBoYXNlKG51bGwsIG51bGwsIDAsIDAsIDApO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyByZXF1ZXN0IHRoZSBtYXAgYmUgcmVkcmF3biB3aXRoIG5ldyBob3ZlciBpbmZvXHJcbiAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5yZXF1ZXN0RHJhd01hcCgpO1xyXG4gICAgICAgIH0gZWxzZSBpZihvYmpVbmRlck1vdXNlICYmIChcIm1lbVwiIGluIG9ialVuZGVyTW91c2UpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG1lbWVudG8gPSAob2JqVW5kZXJNb3VzZSBhcyB7IG1lbTogSXRlbU1lbWVudG8gfSkubWVtO1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25JdGVtTWVtZW50b0hvdmVyKG1lbWVudG8sIHRoaXMubGFzdEdsb2JhbE1vdXNlWCwgdGhpcy5sYXN0R2xvYmFsTW91c2VZKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbk1vdXNlV2hlZWwgPSAoZTogV2hlZWxFdmVudCkgPT4ge1xyXG4gICAgICAgIC8vIENhbGN1bGF0ZSB0aGUgbWluLiB6b29tIGxldmVsIHMudC4gdGhlIGVudGlyZSBtYXAgaXMgdmlzaWJsZVxyXG4gICAgICAgIGNvbnN0IHdoZWVsRGVsdGEgPSBlLndoZWVsRGVsdGEgPyBlLndoZWVsRGVsdGEgOiBlLmRlbHRhWSA/IC1lLmRlbHRhWSA6IDA7XHJcbiAgICAgICAgY29uc3QgY2FudmFzVG9NYXBXaWR0aFJhdGlvID0gdGhpcy5jYW52YXMud2lkdGggLyB0aGlzLnRvdGFsTWFwV2lkdGg7XHJcbiAgICAgICAgY29uc3QgY2FudmFzVG9NYXBIZWlnaHRSYXRpbyA9IHRoaXMuY2FudmFzLmhlaWdodCAvIHRoaXMudG90YWxNYXBIZWlnaHQ7XHJcbiAgICAgICAgbGV0IG1pblpvb21MZXZlbCA9IGNhbnZhc1RvTWFwV2lkdGhSYXRpbyA8IGNhbnZhc1RvTWFwSGVpZ2h0UmF0aW8gPyBjYW52YXNUb01hcFdpZHRoUmF0aW8gOiBjYW52YXNUb01hcEhlaWdodFJhdGlvO1xyXG5cclxuICAgICAgICAvLyBDYWxjdWxhdGUgdGhlIG5ldyB6b29tIGxldmVsIGJhc2VkIG9uIHRoZSBkaXJlY3Rpb24gdGhlIHdoZWVsIHdhcyBzcHVuXHJcbiAgICAgICAgY29uc3Qgd2hlZWxEaXJlY3Rpb24gPSB3aGVlbERlbHRhIDwgMCA/IC0xIDogd2hlZWxEZWx0YSA+IDAgPyAxIDogMDtcclxuICAgICAgICB0aGlzLnpvb21MZXZlbCArPSB3aGVlbERpcmVjdGlvbiAqIDAuMDU7XHJcbiAgICAgICAgdGhpcy56b29tTGV2ZWwgPSB0aGlzLnpvb21MZXZlbCA8IG1pblpvb21MZXZlbCA/IG1pblpvb21MZXZlbCA6IHRoaXMuem9vbUxldmVsID4gMS4wID8gMS4wIDogdGhpcy56b29tTGV2ZWw7XHJcblxyXG4gICAgICAgIC8vIERldGVybWluZSB0aGUgem9vbSB0YXJnZXQgYmFzZWQgb24gdGhlIG1vdXNlIGxvY2F0aW9uXHJcbiAgICAgICAgLy8gTk9URSAtIHdoZW4gem9vbWluZyBvdXQsIHRoZSB0YXJnZXQgc2hvdWxkIGJlIGNlbnRlcmVkIG9uIG1hcCBjZW50ZXJcclxuICAgICAgICBpZighdGhpcy5jb250cm9sbGVyLm15UGxheWVyRGV0YWlscykge1xyXG4gICAgICAgICAgICBpZih3aGVlbERpcmVjdGlvbiA8IDApIHtcclxuICAgICAgICAgICAgICAgIC8vIHpvb21pbmcgb3V0XHJcbiAgICAgICAgICAgICAgICB0aGlzLnpvb21UYXJnZXRYID0gdGhpcy5jYW52YXMud2lkdGgvMiAtIHRoaXMudG90YWxNYXBXaWR0aC8yKnRoaXMuem9vbUxldmVsO1xyXG4gICAgICAgICAgICAgICAgdGhpcy56b29tVGFyZ2V0WSA9IHRoaXMuY2FudmFzLmhlaWdodC8yIC0gdGhpcy50b3RhbE1hcEhlaWdodC8yKnRoaXMuem9vbUxldmVsO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgLy8gem9vbWluZyBpblxyXG4gICAgICAgICAgICAgICAgdGhpcy56b29tVGFyZ2V0WCA9IHRoaXMuY2FudmFzLndpZHRoLzIgLSB0aGlzLnRvdGFsTWFwV2lkdGgvMip0aGlzLnpvb21MZXZlbDtcclxuICAgICAgICAgICAgICAgIHRoaXMuem9vbVRhcmdldFkgPSB0aGlzLmNhbnZhcy5oZWlnaHQvMiAtIHRoaXMudG90YWxNYXBIZWlnaHQvMip0aGlzLnpvb21MZXZlbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIGNlbnRlciB0aGUgcGxheWVyXHJcbiAgICAgICAgICAgIGNvbnN0IGkgPSB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJEZXRhaWxzLng7XHJcbiAgICAgICAgICAgIGNvbnN0IGogPSB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJEZXRhaWxzLnk7XHJcbiAgICAgICAgICAgIHZhciB4ID0gaSAqIHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGggKyAodGhpcy5oZXhSZWN0YW5nbGVXaWR0aC8yICogKGogJSAyKSkgKyB0aGlzLmhleFJlY3RhbmdsZVdpZHRoLzI7XHJcbiAgICAgICAgICAgIHZhciB5ID0gaiAqIHRoaXMuaGV4UmVjdGFuZ2xlSGVpZ2h0IC0gKHRoaXMuc2lkZUxlbmd0aC8yICogaikgKyB0aGlzLmhleFJlY3RhbmdsZUhlaWdodC8yO1xyXG4gICAgICAgICAgICB0aGlzLnpvb21UYXJnZXRYID0gdGhpcy5jYW52YXMud2lkdGgvMiAtIHgqdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgICAgIHRoaXMuem9vbVRhcmdldFkgPSB0aGlzLmNhbnZhcy5oZWlnaHQvMiAtIHkqdGhpcy56b29tTGV2ZWw7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBSZWRyYXcgdGhlIG1hcCBhdCB0aGUgbmV3IHpvb20gbGV2ZWxcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLnJlcXVlc3REcmF3TWFwKCk7XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBvbk15UGxheWVyRGV0YWlsc1VwZGF0ZWQoKSB7XHJcbiAgICAgICAgLy8gY2VudGVyIHRoZSBwbGF5ZXJcclxuICAgICAgICBjb25zdCBpID0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyRGV0YWlscy54O1xyXG4gICAgICAgIGNvbnN0IGogPSB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJEZXRhaWxzLnk7XHJcbiAgICAgICAgdmFyIHggPSBpICogdGhpcy5oZXhSZWN0YW5nbGVXaWR0aCArICh0aGlzLmhleFJlY3RhbmdsZVdpZHRoLzIgKiAoaiAlIDIpKSArIHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGgvMjtcclxuICAgICAgICB2YXIgeSA9IGogKiB0aGlzLmhleFJlY3RhbmdsZUhlaWdodCAtICh0aGlzLnNpZGVMZW5ndGgvMiAqIGopICsgdGhpcy5oZXhSZWN0YW5nbGVIZWlnaHQvMjtcclxuICAgICAgICB0aGlzLnpvb21UYXJnZXRYID0gdGhpcy5jYW52YXMud2lkdGgvMi14KnRoaXMuem9vbUxldmVsO1xyXG4gICAgICAgIHRoaXMuem9vbVRhcmdldFkgPSB0aGlzLmNhbnZhcy5oZWlnaHQvMi15KnRoaXMuem9vbUxldmVsO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBvbk1vdmVWYWxpZChuZXdYOiBudW1iZXIsIG5ld1k6IG51bWJlcikge1xyXG4gICAgICAgIGNvbnN0IG15RGV0YWlscyA9IHRoaXMuY29udHJvbGxlci5teVBsYXllckRldGFpbHM7XHJcbiAgICAgICAgaWYobXlEZXRhaWxzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUFycm93cy5sZW5ndGggPSAwO1xyXG4gICAgICAgICAgICBpZihteURldGFpbHMueCAhPT0gbmV3WCAmJiBteURldGFpbHMueSAhPT0gbmV3WSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kaXNwbGF5QXJyb3dzLnB1c2gobmV3IE1hcENhbnZhc0Fycm93KG15RGV0YWlscy54LCBteURldGFpbHMueSwgbmV3WCwgbmV3WSwgXCJNT1ZFXCIpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb25BdHRhY2tWYWxpZCh0YXJnZXRJRDogbnVtYmVyKSB7XHJcbiAgICAgICAgY29uc3QgbXlEZXRhaWxzID0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyRGV0YWlscztcclxuICAgICAgICBjb25zdCB0YXJnZXREZXRhaWxzID0gdGhpcy5jb250cm9sbGVyLnBsYXllckRldGFpbHNbdGFyZ2V0SURdO1xyXG5cclxuICAgICAgICAvLyBBZGQgYW4gXCJBdHRhY2tcIiBhcnJvdyBmcm9tIG15IHBsYXllciB0byB0aGUgdGFyZ2V0IHBsYXllclxyXG4gICAgICAgIGlmKG15RGV0YWlscyAmJiB0YXJnZXREZXRhaWxzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheUFycm93cy5wdXNoKG5ldyBNYXBDYW52YXNBcnJvdyhteURldGFpbHMueCwgbXlEZXRhaWxzLnksIHRhcmdldERldGFpbHMueCwgdGFyZ2V0RGV0YWlscy55LCBcIkFUVEFDS1wiKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyByZXNpemUoKSB7XHJcbiAgICAgICAgLy8gU2V0IHRoZSBpbnRlcm5hbCB3aWR0aCAmIGhlaWdodCBvZiB0aGUgY2FudmFzZXMgZXF1YWwgdG8gdGhlIGNhbnZhcyBlbGVtZW50J3Mgd2lkdGggJiBoZWlnaHRcclxuICAgICAgICB0aGlzLmNhbnZhcy5zZXRBdHRyaWJ1dGUoXCJ3aWR0aFwiLCBTdHJpbmcodGhpcy5jYW52YXMuY2xpZW50V2lkdGgpKTtcclxuICAgICAgICB0aGlzLmNhbnZhcy5zZXRBdHRyaWJ1dGUoXCJoZWlnaHRcIiwgU3RyaW5nKHRoaXMuY2FudmFzLmNsaWVudEhlaWdodCkpO1xyXG4gICAgICAgIHRoaXMuaGl0Q2FudmFzLnNldEF0dHJpYnV0ZShcIndpZHRoXCIsIFN0cmluZyh0aGlzLmhpdENhbnZhcy5jbGllbnRXaWR0aCkpO1xyXG4gICAgICAgIHRoaXMuaGl0Q2FudmFzLnNldEF0dHJpYnV0ZShcImhlaWdodFwiLCBTdHJpbmcodGhpcy5oaXRDYW52YXMuY2xpZW50SGVpZ2h0KSk7XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLnJlcXVlc3REcmF3TWFwKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uTWFwTG9hZGVkKCkge1xyXG4gICAgICAgIHRoaXMudG90YWxNYXBXaWR0aCA9IHRoaXMuaGV4UmVjdGFuZ2xlV2lkdGggKiAodGhpcy5jb250cm9sbGVyLm1hcC5oZXhHcmlkLmxlbmd0aCArIDAuNSk7XHJcbiAgICAgICAgdGhpcy50b3RhbE1hcEhlaWdodCA9IHRoaXMuaGV4UmVjdGFuZ2xlSGVpZ2h0ICogKHRoaXMuY29udHJvbGxlci5tYXAuaGV4R3JpZFswXS5sZW5ndGggKyAwLjUpICogMy80O1xyXG5cclxuICAgICAgICB0aGlzLmNhbnZhcy5hZGRFdmVudExpc3RlbmVyKENvbnRyb2xsZXIuQ0xJQ0tfRVZFTlQsIHRoaXMub25DbGljayk7XHJcbiAgICAgICAgdGhpcy5jYW52YXMuYWRkRXZlbnRMaXN0ZW5lcihcImNvbnRleHRtZW51XCIsIHRoaXMub25DbGljayk7XHJcbiAgICAgICAgdGhpcy5jYW52YXMuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbW92ZVwiLCB0aGlzLm9uTW91c2VNb3ZlKTtcclxuICAgICAgICB0aGlzLmNhbnZhcy5hZGRFdmVudExpc3RlbmVyKFwib253aGVlbFwiIGluIHdpbmRvdyA/IFwid2hlZWxcIiA6IFwiRE9NTW91c2VTY3JvbGxcIiwgdGhpcy5vbk1vdXNlV2hlZWwsIHRydWUpO1xyXG5cclxuICAgICAgICAvLyBzZXQgZGVmYXVsdCB6b29tIGxldmVsIHRvIGJlIGZ1bGx5IHpvb21lZCBvdXRcclxuICAgICAgICBjb25zdCBjYW52YXNUb01hcFdpZHRoUmF0aW8gPSB0aGlzLmNhbnZhcy53aWR0aCAvIHRoaXMudG90YWxNYXBXaWR0aDtcclxuICAgICAgICBjb25zdCBjYW52YXNUb01hcEhlaWdodFJhdGlvID0gdGhpcy5jYW52YXMuaGVpZ2h0IC8gdGhpcy50b3RhbE1hcEhlaWdodDtcclxuICAgICAgICBsZXQgbWluWm9vbUxldmVsID0gY2FudmFzVG9NYXBXaWR0aFJhdGlvIDwgY2FudmFzVG9NYXBIZWlnaHRSYXRpbyA/IGNhbnZhc1RvTWFwV2lkdGhSYXRpbyA6IGNhbnZhc1RvTWFwSGVpZ2h0UmF0aW87XHJcbiAgICAgICAgdGhpcy56b29tTGV2ZWwgPSBtaW5ab29tTGV2ZWw7XHJcblxyXG4gICAgICAgIC8vIHNldCBkZWZhdWx0IHpvb20gdG8gcHJvdmlkZSBjb21wbGV0ZSBvdmVydmlldyBvZiBtYXBcclxuICAgICAgICB0aGlzLnpvb21UYXJnZXRYID0gdGhpcy5jYW52YXMud2lkdGgvMiAtIHRoaXMudG90YWxNYXBXaWR0aC8yKnRoaXMuem9vbUxldmVsO1xyXG4gICAgICAgIHRoaXMuem9vbVRhcmdldFkgPSB0aGlzLmNhbnZhcy5oZWlnaHQvMiAtIHRoaXMudG90YWxNYXBIZWlnaHQvMip0aGlzLnpvb21MZXZlbDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiBhIG1vdmUgcGhhc2UgaGFzIGJlZ3VuXHJcbiAgICBwdWJsaWMgb25Nb3ZlUGhhc2VTdGFydCgpIHtcclxuICAgICAgICB0aGlzLmRpc3BsYXlBcnJvd3MubGVuZ3RoID0gMDtcclxuXHJcbiAgICAgICAgaWYodGhpcy5jb250cm9sbGVyLm15UGxheWVyRGV0YWlscykge1xyXG4gICAgICAgICAgICB0aGlzLmhleFNlbGVjdGVkWCA9IHRoaXMuY29udHJvbGxlci5teVBsYXllckRldGFpbHMueDtcclxuICAgICAgICAgICAgdGhpcy5oZXhTZWxlY3RlZFkgPSB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJEZXRhaWxzLnk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIFRyaWdnZXIgZmFsc2Ugb25Nb3VzZU1vdmUgZXZlbnQgdG8gcmVzZXQgbW91c2UgY3Vyc29yIGljb25cclxuICAgICAgICB0aGlzLm9uTW91c2VNb3ZlKHtcclxuICAgICAgICAgICAgY2xpZW50WDogdGhpcy5sYXN0TW91c2VYICsgdGhpcy5jYW52YXMuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdCxcclxuICAgICAgICAgICAgY2xpZW50WTogdGhpcy5sYXN0TW91c2VZICsgdGhpcy5jYW52YXMuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wXHJcbiAgICAgICAgfSBhcyBNb3VzZUV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgd2hlbiBhbiBhY3Rpb24gcGhhc2UgaGFzIGJlZ3VuXHJcbiAgICBwdWJsaWMgb25BY3Rpb25QaGFzZVN0YXJ0KCkge1xyXG4gICAgICAgIHRoaXMuZGlzcGxheUFycm93cy5sZW5ndGggPSAwO1xyXG5cclxuICAgICAgICBpZih0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJEZXRhaWxzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaGV4U2VsZWN0ZWRYID0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyRGV0YWlscy54O1xyXG4gICAgICAgICAgICB0aGlzLmhleFNlbGVjdGVkWSA9IHRoaXMuY29udHJvbGxlci5teVBsYXllckRldGFpbHMueTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gVHJpZ2dlciBmYWxzZSBvbk1vdXNlTW92ZSBldmVudCB0byByZXNldCBtb3VzZSBjdXJzb3IgaWNvblxyXG4gICAgICAgIHRoaXMub25Nb3VzZU1vdmUoe1xyXG4gICAgICAgICAgICBjbGllbnRYOiB0aGlzLmxhc3RNb3VzZVggKyB0aGlzLmNhbnZhcy5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5sZWZ0LFxyXG4gICAgICAgICAgICBjbGllbnRZOiB0aGlzLmxhc3RNb3VzZVkgKyB0aGlzLmNhbnZhcy5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS50b3BcclxuICAgICAgICB9IGFzIE1vdXNlRXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGEgcGhhc2UgaGFzIGZpbmlzaGVkIGFuZCB0aGUgZ2FtZSBpcyBub3cgd2FpdGluZyBmb3IgdGhlIHNlcnZlclxyXG4gICAgcHVibGljIG9uV2FpdGluZ0ZvclNlcnZlcigpIHtcclxuICAgICAgICB0aGlzLmNhbnZhcy5zdHlsZS5jdXJzb3IgPSBcInVybCgnYXNzZXRzL0Nyb3NzX0ljb24ucG5nJykgMTYgMTYsIGF1dG9cIjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBHZXQgdGhlIGhleGFnb24gb2YgdGhlIGhpdCBjYW52YXMgdGhlIG1vdXNlIGlzIHBvc2l0aW9uZWQgb3ZlclxyXG4gICAgcHJpdmF0ZSBnZXRPYmplY3RVbmRlck1vdXNlKGUpIHtcclxuICAgICAgICB2YXIgbW91c2V4ID0gZS5jbGllbnRYIC0gdGhpcy5oaXRDYW52YXMuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkubGVmdDtcclxuICAgICAgICB2YXIgbW91c2V5ID0gZS5jbGllbnRZIC0gdGhpcy5oaXRDYW52YXMuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkudG9wO1xyXG5cclxuICAgICAgICB2YXIgcGl4ZWwgPSB0aGlzLmhpdEN0eC5nZXRJbWFnZURhdGEobW91c2V4LCBtb3VzZXksIDEsIDEpLmRhdGE7XHJcbiAgICAgICAgdmFyIGNvbG9yID0gYHJnYigke3BpeGVsWzBdfSwke3BpeGVsWzFdfSwke3BpeGVsWzJdfSlgO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNvbnRyb2xsZXIuaGl0TWFwLmdldChjb2xvcik7XHJcbiAgICB9XHJcbn1cclxuIiwiZXhwb3J0IGNsYXNzIE1hcENhbnZhc0Fycm93IHtcclxuICAgIHB1YmxpYyByZWFkb25seSBmcm9tWDogbnVtYmVyO1xyXG4gICAgcHVibGljIHJlYWRvbmx5IGZyb21ZOiBudW1iZXI7XHJcbiAgICBwdWJsaWMgcmVhZG9ubHkgdG9YOiBudW1iZXI7XHJcbiAgICBwdWJsaWMgcmVhZG9ubHkgdG9ZOiBudW1iZXI7XHJcbiAgICBwdWJsaWMgcmVhZG9ubHkgdGV4dDogc3RyaW5nO1xyXG5cclxuICAgIHB1YmxpYyBjb25zdHJ1Y3Rvcihmcm9tWCwgZnJvbVksIHRvWCwgdG9ZLCB0ZXh0KSB7XHJcbiAgICAgICAgdGhpcy5mcm9tWCA9IGZyb21YO1xyXG4gICAgICAgIHRoaXMuZnJvbVkgPSBmcm9tWTtcclxuICAgICAgICB0aGlzLnRvWCA9IHRvWDtcclxuICAgICAgICB0aGlzLnRvWSA9IHRvWTtcclxuICAgICAgICB0aGlzLnRleHQgPSB0ZXh0O1xyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCB7IENvbnRyb2xsZXIgfSBmcm9tIFwiLi4vQ29udHJvbGxlclwiXHJcblxyXG5leHBvcnQgY2xhc3MgTW9kaWZpZXJzTGlzdCB7XHJcbiAgICBwcml2YXRlIG1vZGlmaWVyc0xpc3Q6IEhUTUxEaXZFbGVtZW50O1xyXG5cclxuICAgIHB1YmxpYyBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbnRyb2xsZXI6IENvbnRyb2xsZXIpIHtcclxuICAgICAgICB0aGlzLm1vZGlmaWVyc0xpc3QgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1vZGlmaWVycy1saXN0XCIpIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyByZWZyZXNoRGlzcGxheSgpIHtcclxuICAgICAgICBsZXQgaHRtbCA9IFwiXCI7XHJcbiAgICAgICAgY29uc3QgbW9kaWZpZXJzID0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyTW9kaWZpZXJzO1xyXG5cclxuICAgICAgICBpZihtb2RpZmllcnMudmlld1JhbmdlICE9IDAgfHwgKG1vZGlmaWVycy52aWV3UmFuZ2VEZXNjICYmIG1vZGlmaWVycy52aWV3UmFuZ2VEZXNjLmxlbmd0aCA+IDApKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNpZ24gPSBtb2RpZmllcnMudmlld1JhbmdlID4gMCA/IFwiK1wiIDogXCJcIjtcclxuICAgICAgICAgICAgY29uc3QgY29sb3IgPSBzaWduID09IFwiK1wiID8gXCIjMDAwMGIyXCIgOiBcIiNiMjAwMDBcIiA7XHJcblxyXG4gICAgICAgICAgICBodG1sICs9IGBcclxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJ2aWV3LXJhbmdlLW1vZGlmaWVyLWRpdlwiIGNsYXNzPVwibW9kaWZpZXJzLWxpc3QtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cIm1vZGlmaWVycy1uYW1lLWxibFwiPlZpZXcgUmFuZ2U8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cIm1vZGlmaWVycy12YWx1ZS1sYmxcIiBzdHlsZT1cImNvbG9yOiR7Y29sb3J9XCI+JHtzaWdufSR7bW9kaWZpZXJzLnZpZXdSYW5nZX08L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+YDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKG1vZGlmaWVycy5oaXRDaGFuY2UgIT0gMCB8fCAobW9kaWZpZXJzLmhpdENoYW5jZURlc2MgJiYgbW9kaWZpZXJzLmhpdENoYW5jZURlc2MubGVuZ3RoID4gMCkpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2lnbiA9IG1vZGlmaWVycy5oaXRDaGFuY2UgPiAwID8gXCIrXCIgOiBcIlwiO1xyXG4gICAgICAgICAgICBjb25zdCBjb2xvciA9IHNpZ24gPT0gXCIrXCIgPyBcIiMwMDAwYjJcIiA6IFwiI2IyMDAwMFwiIDtcclxuXHJcbiAgICAgICAgICAgIGh0bWwgKz0gYFxyXG4gICAgICAgICAgICAgICAgPGRpdiBpZD1cImhpdC1jaGFuY2UtbW9kaWZpZXItZGl2XCIgY2xhc3M9XCJtb2RpZmllcnMtbGlzdC1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwibW9kaWZpZXJzLW5hbWUtbGJsXCI+SGl0IENoYW5jZTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwibW9kaWZpZXJzLXZhbHVlLWxibFwiIHN0eWxlPVwiY29sb3I6JHtjb2xvcn1cIj4ke3NpZ259JHttb2RpZmllcnMuaGl0Q2hhbmNlfSU8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+YDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKG1vZGlmaWVycy50b3RhbEFQICE9IDAgfHwgKG1vZGlmaWVycy50b3RhbEFQRGVzYyAmJiBtb2RpZmllcnMudG90YWxBUERlc2MubGVuZ3RoID4gMCkpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2lnbiA9IG1vZGlmaWVycy50b3RhbEFQID4gMCA/IFwiK1wiIDogXCJcIjtcclxuICAgICAgICAgICAgY29uc3QgY29sb3IgPSBzaWduID09IFwiK1wiID8gXCIjMDAwMGIyXCIgOiBcIiNiMjAwMDBcIiA7XHJcblxyXG4gICAgICAgICAgICBodG1sICs9IGBcclxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJ0b3RhbC1hcC1tb2RpZmllci1kaXZcIiBjbGFzcz1cIm1vZGlmaWVycy1saXN0LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJtb2RpZmllcnMtbmFtZS1sYmxcIj5Ub3RhbCBBUDwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwibW9kaWZpZXJzLXZhbHVlLWxibFwiIHN0eWxlPVwiY29sb3I6JHtjb2xvcn1cIj4ke3NpZ259JHttb2RpZmllcnMudG90YWxBUH08L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+YDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmKG1vZGlmaWVycy5hcE1vdmVDb3N0ICE9IDAgfHwgKG1vZGlmaWVycy5hcE1vdmVDb3N0RGVzYyAmJiBtb2RpZmllcnMuYXBNb3ZlQ29zdERlc2MubGVuZ3RoID4gMCkpIHtcclxuICAgICAgICAgICAgY29uc3Qgc2lnbiA9IG1vZGlmaWVycy5hcE1vdmVDb3N0ID4gMCA/IFwiK1wiIDogXCJcIjtcclxuICAgICAgICAgICAgY29uc3QgY29sb3IgPSBzaWduID09IFwiXCIgPyBcIiMwMDAwYjJcIiA6IFwiI2IyMDAwMFwiIDtcclxuXHJcbiAgICAgICAgICAgIGh0bWwgKz0gYFxyXG4gICAgICAgICAgICAgICAgPGRpdiBpZD1cImFwLW1vdmUtY29zdC1tb2RpZmllci1kaXZcIiBjbGFzcz1cIm1vZGlmaWVycy1saXN0LWl0ZW1cIj5cclxuICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJtb2RpZmllcnMtbmFtZS1sYmxcIj5Nb3ZlIEFQIENvc3Q8L2xhYmVsPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cIm1vZGlmaWVycy12YWx1ZS1sYmxcIiBzdHlsZT1cImNvbG9yOiR7Y29sb3J9XCI+JHtzaWdufSR7bW9kaWZpZXJzLmFwTW92ZUNvc3R9PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PmA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZihtb2RpZmllcnMuZG1nUGVyTW92ZSAhPSAwIHx8IChtb2RpZmllcnMuZG1nUGVyTW92ZURlc2MgJiYgbW9kaWZpZXJzLmRtZ1Blck1vdmVEZXNjLmxlbmd0aCA+IDApKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNpZ24gPSBtb2RpZmllcnMuZG1nUGVyTW92ZSA+IDAgPyBcIitcIiA6IFwiXCI7XHJcbiAgICAgICAgICAgIGNvbnN0IGNvbG9yID0gc2lnbiA9PSBcIlwiID8gXCIjMDAwMGIyXCIgOiBcIiNiMjAwMDBcIiA7XHJcblxyXG4gICAgICAgICAgICBodG1sICs9IGBcclxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XCJkbWctcGVyLW1vdmUtbW9kaWZpZXItZGl2XCIgY2xhc3M9XCJtb2RpZmllcnMtbGlzdC1pdGVtXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwibW9kaWZpZXJzLW5hbWUtbGJsXCI+RG1nIFBlciBNb3ZlPC9sYWJlbD5cclxuICAgICAgICAgICAgICAgICAgICA8bGFiZWwgY2xhc3M9XCJtb2RpZmllcnMtdmFsdWUtbGJsXCIgc3R5bGU9XCJjb2xvcjoke2NvbG9yfVwiPiR7c2lnbn0ke21vZGlmaWVycy5kbWdQZXJNb3ZlfTwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5gO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYobW9kaWZpZXJzLnNpY2tEbWdQZXJUdXJuICE9IDAgfHwgKG1vZGlmaWVycy5zaWNrRG1nUGVyVHVybkRlc2MgJiYgbW9kaWZpZXJzLnNpY2tEbWdQZXJUdXJuRGVzYy5sZW5ndGggPiAwKSkge1xyXG4gICAgICAgICAgICBjb25zdCBzaWduID0gbW9kaWZpZXJzLnNpY2tEbWdQZXJUdXJuID4gMCA/IFwiK1wiIDogXCJcIjtcclxuICAgICAgICAgICAgY29uc3QgY29sb3IgPSBzaWduID09IFwiXCIgPyBcIiMwMDAwYjJcIiA6IFwiI2IyMDAwMFwiIDtcclxuXHJcbiAgICAgICAgICAgIGh0bWwgKz0gYFxyXG4gICAgICAgICAgICAgICAgPGRpdiBpZD1cInNpY2stZG1nLXBlci10dXJuLW1vZGlmaWVyLWRpdlwiIGNsYXNzPVwibW9kaWZpZXJzLWxpc3QtaXRlbVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzcz1cIm1vZGlmaWVycy1uYW1lLWxibFwiPkRtZyBQZXIgVHVybjwvbGFiZWw+XHJcbiAgICAgICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzPVwibW9kaWZpZXJzLXZhbHVlLWxibFwiIHN0eWxlPVwiY29sb3I6JHtjb2xvcn1cIj4ke3NpZ259JHttb2RpZmllcnMuc2lja0RtZ1BlclR1cm59PC9sYWJlbD5cclxuICAgICAgICAgICAgICAgIDwvZGl2PmA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm1vZGlmaWVyc0xpc3QuaW5uZXJIVE1MID0gaHRtbDtcclxuXHJcbiAgICAgICAgY29uc3Qgdmlld1JhbmdlTW9kaWZpZXJEaXYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInZpZXctcmFuZ2UtbW9kaWZpZXItZGl2XCIpO1xyXG4gICAgICAgIGlmKHZpZXdSYW5nZU1vZGlmaWVyRGl2KSB7XHJcbiAgICAgICAgICAgIHZpZXdSYW5nZU1vZGlmaWVyRGl2LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWxlYXZlXCIsIHRoaXMub25Nb3VzZUxlYXZlKTtcclxuICAgICAgICAgICAgdmlld1JhbmdlTW9kaWZpZXJEaXYuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZW50ZXJcIiwgdGhpcy5vbk1vdXNlRW50ZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgaGl0Q2hhbmNlTW9kaWZpZXJEaXYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImhpdC1jaGFuY2UtbW9kaWZpZXItZGl2XCIpO1xyXG4gICAgICAgIGlmKGhpdENoYW5jZU1vZGlmaWVyRGl2KSB7XHJcbiAgICAgICAgICAgIGhpdENoYW5jZU1vZGlmaWVyRGl2LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWxlYXZlXCIsIHRoaXMub25Nb3VzZUxlYXZlKTtcclxuICAgICAgICAgICAgaGl0Q2hhbmNlTW9kaWZpZXJEaXYuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZW50ZXJcIiwgdGhpcy5vbk1vdXNlRW50ZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgdG90YWxBUE1vZGlmaWVyRGl2ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0b3RhbC1hcC1tb2RpZmllci1kaXZcIik7XHJcbiAgICAgICAgaWYodG90YWxBUE1vZGlmaWVyRGl2KSB7XHJcbiAgICAgICAgICAgIHRvdGFsQVBNb2RpZmllckRpdi5hZGRFdmVudExpc3RlbmVyKFwibW91c2VsZWF2ZVwiLCB0aGlzLm9uTW91c2VMZWF2ZSk7XHJcbiAgICAgICAgICAgIHRvdGFsQVBNb2RpZmllckRpdi5hZGRFdmVudExpc3RlbmVyKFwibW91c2VlbnRlclwiLCB0aGlzLm9uTW91c2VFbnRlcik7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBhcE1vdmVDb3N0TW9kaWZpZXJzRGl2ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJhcC1tb3ZlLWNvc3QtbW9kaWZpZXItZGl2XCIpO1xyXG4gICAgICAgIGlmKGFwTW92ZUNvc3RNb2RpZmllcnNEaXYpIHtcclxuICAgICAgICAgICAgYXBNb3ZlQ29zdE1vZGlmaWVyc0Rpdi5hZGRFdmVudExpc3RlbmVyKFwibW91c2VsZWF2ZVwiLCB0aGlzLm9uTW91c2VMZWF2ZSk7XHJcbiAgICAgICAgICAgIGFwTW92ZUNvc3RNb2RpZmllcnNEaXYuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZW50ZXJcIiwgdGhpcy5vbk1vdXNlRW50ZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZG1nUGVyTW92ZU1vZGlmaWVyc0RpdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZG1nLXBlci1tb3ZlLW1vZGlmaWVyLWRpdlwiKTtcclxuICAgICAgICBpZihkbWdQZXJNb3ZlTW9kaWZpZXJzRGl2KSB7XHJcbiAgICAgICAgICAgIGRtZ1Blck1vdmVNb2RpZmllcnNEaXYuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbGVhdmVcIiwgdGhpcy5vbk1vdXNlTGVhdmUpO1xyXG4gICAgICAgICAgICBkbWdQZXJNb3ZlTW9kaWZpZXJzRGl2LmFkZEV2ZW50TGlzdGVuZXIoXCJtb3VzZWVudGVyXCIsIHRoaXMub25Nb3VzZUVudGVyKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHNpY2tEbWdQZXJUdXJuTW9kaWZpZXJzRGl2ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzaWNrLWRtZy1wZXItdHVybi1tb2RpZmllci1kaXZcIik7XHJcbiAgICAgICAgaWYoc2lja0RtZ1BlclR1cm5Nb2RpZmllcnNEaXYpIHtcclxuICAgICAgICAgICAgc2lja0RtZ1BlclR1cm5Nb2RpZmllcnNEaXYuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlbGVhdmVcIiwgdGhpcy5vbk1vdXNlTGVhdmUpO1xyXG4gICAgICAgICAgICBzaWNrRG1nUGVyVHVybk1vZGlmaWVyc0Rpdi5hZGRFdmVudExpc3RlbmVyKFwibW91c2VlbnRlclwiLCB0aGlzLm9uTW91c2VFbnRlcik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25Nb3VzZUVudGVyID0gKGUpID0+IHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgaWYoZS50YXJnZXQuaWQgPT0gXCJ2aWV3LXJhbmdlLW1vZGlmaWVyLWRpdlwiKSB7XHJcbiAgICAgICAgICAgIGZvcihjb25zdCBkZXNjIG9mIHRoaXMuY29udHJvbGxlci5teVBsYXllck1vZGlmaWVycy52aWV3UmFuZ2VEZXNjKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcclxuICAgICAgICAgICAgICAgIGRpdi5pZCA9IFwidmlldy1yYW5nZS1tb2RpZmllci1kZXNjXCI7XHJcbiAgICAgICAgICAgICAgICBkaXYuY2xhc3NMaXN0LmFkZChcIm1vZGlmaWVycy1saXN0LWl0ZW1cIik7XHJcbiAgICAgICAgICAgICAgICBsZXQgbGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICAgICAgbGJsLmNsYXNzTGlzdC5hZGQoXCJtb2RpZmllcnMtZGVzYy1sYmxcIik7XHJcbiAgICAgICAgICAgICAgICBsYmwuaW5uZXJIVE1MID0gZGVzYztcclxuICAgICAgICAgICAgICAgIGRpdi5hcHBlbmRDaGlsZChsYmwpO1xyXG4gICAgICAgICAgICAgICAgZS50YXJnZXQuYXBwZW5kQ2hpbGQoZGl2KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZihlLnRhcmdldC5pZCA9PSBcImhpdC1jaGFuY2UtbW9kaWZpZXItZGl2XCIpIHtcclxuICAgICAgICAgICAgZm9yKGNvbnN0IGRlc2Mgb2YgdGhpcy5jb250cm9sbGVyLm15UGxheWVyTW9kaWZpZXJzLmhpdENoYW5jZURlc2MpIHtcclxuICAgICAgICAgICAgICAgIGxldCBkaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xyXG4gICAgICAgICAgICAgICAgZGl2LmlkID0gXCJoaXQtY2hhbmNlLW1vZGlmaWVyLWRlc2NcIjtcclxuICAgICAgICAgICAgICAgIGRpdi5jbGFzc0xpc3QuYWRkKFwibW9kaWZpZXJzLWxpc3QtaXRlbVwiKTtcclxuICAgICAgICAgICAgICAgIGxldCBsYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIik7XHJcbiAgICAgICAgICAgICAgICBsYmwuY2xhc3NMaXN0LmFkZChcIm1vZGlmaWVycy1kZXNjLWxibFwiKTtcclxuICAgICAgICAgICAgICAgIGxibC5pbm5lckhUTUwgPSBkZXNjO1xyXG4gICAgICAgICAgICAgICAgZGl2LmFwcGVuZENoaWxkKGxibCk7XHJcbiAgICAgICAgICAgICAgICBlLnRhcmdldC5hcHBlbmRDaGlsZChkaXYpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmKGUudGFyZ2V0LmlkID09IFwidG90YWwtYXAtbW9kaWZpZXItZGl2XCIpIHtcclxuICAgICAgICAgICAgZm9yKGNvbnN0IGRlc2Mgb2YgdGhpcy5jb250cm9sbGVyLm15UGxheWVyTW9kaWZpZXJzLnRvdGFsQVBEZXNjKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcclxuICAgICAgICAgICAgICAgIGRpdi5pZCA9IFwidG90YWwtYXAtbW9kaWZpZXItZGVzY1wiO1xyXG4gICAgICAgICAgICAgICAgZGl2LmNsYXNzTGlzdC5hZGQoXCJtb2RpZmllcnMtbGlzdC1pdGVtXCIpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGxibCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsYWJlbFwiKTtcclxuICAgICAgICAgICAgICAgIGxibC5jbGFzc0xpc3QuYWRkKFwibW9kaWZpZXJzLWRlc2MtbGJsXCIpO1xyXG4gICAgICAgICAgICAgICAgbGJsLmlubmVySFRNTCA9IGRlc2M7XHJcbiAgICAgICAgICAgICAgICBkaXYuYXBwZW5kQ2hpbGQobGJsKTtcclxuICAgICAgICAgICAgICAgIGUudGFyZ2V0LmFwcGVuZENoaWxkKGRpdik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYoZS50YXJnZXQuaWQgPT0gXCJhcC1tb3ZlLWNvc3QtbW9kaWZpZXItZGl2XCIpIHtcclxuICAgICAgICAgICAgZm9yKGNvbnN0IGRlc2Mgb2YgdGhpcy5jb250cm9sbGVyLm15UGxheWVyTW9kaWZpZXJzLmFwTW92ZUNvc3REZXNjKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcclxuICAgICAgICAgICAgICAgIGRpdi5pZCA9IFwiYXAtbW92ZS1jb3N0LW1vZGlmaWVyLWRlc2NcIjtcclxuICAgICAgICAgICAgICAgIGRpdi5jbGFzc0xpc3QuYWRkKFwibW9kaWZpZXJzLWxpc3QtaXRlbVwiKTtcclxuICAgICAgICAgICAgICAgIGxldCBsYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIik7XHJcbiAgICAgICAgICAgICAgICBsYmwuY2xhc3NMaXN0LmFkZChcIm1vZGlmaWVycy1kZXNjLWxibFwiKTtcclxuICAgICAgICAgICAgICAgIGxibC5pbm5lckhUTUwgPSBkZXNjO1xyXG4gICAgICAgICAgICAgICAgZGl2LmFwcGVuZENoaWxkKGxibCk7XHJcbiAgICAgICAgICAgICAgICBlLnRhcmdldC5hcHBlbmRDaGlsZChkaXYpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmKGUudGFyZ2V0LmlkID09IFwiZG1nLXBlci1tb3ZlLW1vZGlmaWVyLWRpdlwiKSB7XHJcbiAgICAgICAgICAgIGZvcihjb25zdCBkZXNjIG9mIHRoaXMuY29udHJvbGxlci5teVBsYXllck1vZGlmaWVycy5kbWdQZXJNb3ZlRGVzYykge1xyXG4gICAgICAgICAgICAgICAgbGV0IGRpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XHJcbiAgICAgICAgICAgICAgICBkaXYuaWQgPSBcImRtZy1wZXItbW92ZS1tb2RpZmllci1kZXNjXCI7XHJcbiAgICAgICAgICAgICAgICBkaXYuY2xhc3NMaXN0LmFkZChcIm1vZGlmaWVycy1saXN0LWl0ZW1cIik7XHJcbiAgICAgICAgICAgICAgICBsZXQgbGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICAgICAgbGJsLmNsYXNzTGlzdC5hZGQoXCJtb2RpZmllcnMtZGVzYy1sYmxcIik7XHJcbiAgICAgICAgICAgICAgICBsYmwuaW5uZXJIVE1MID0gZGVzYztcclxuICAgICAgICAgICAgICAgIGRpdi5hcHBlbmRDaGlsZChsYmwpO1xyXG4gICAgICAgICAgICAgICAgZS50YXJnZXQuYXBwZW5kQ2hpbGQoZGl2KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZihlLnRhcmdldC5pZCA9PSBcInNpY2stZG1nLXBlci10dXJuLW1vZGlmaWVyLWRpdlwiKSB7XHJcbiAgICAgICAgICAgIGZvcihjb25zdCBkZXNjIG9mIHRoaXMuY29udHJvbGxlci5teVBsYXllck1vZGlmaWVycy5zaWNrRG1nUGVyVHVybkRlc2MpIHtcclxuICAgICAgICAgICAgICAgIGxldCBkaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xyXG4gICAgICAgICAgICAgICAgZGl2LmlkID0gXCJzaWNrLWRtZy1wZXItdHVybi1tb2RpZmllci1kZXNjXCI7XHJcbiAgICAgICAgICAgICAgICBkaXYuY2xhc3NMaXN0LmFkZChcIm1vZGlmaWVycy1saXN0LWl0ZW1cIik7XHJcbiAgICAgICAgICAgICAgICBsZXQgbGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICAgICAgbGJsLmNsYXNzTGlzdC5hZGQoXCJtb2RpZmllcnMtZGVzYy1sYmxcIik7XHJcbiAgICAgICAgICAgICAgICBsYmwuaW5uZXJIVE1MID0gZGVzYztcclxuICAgICAgICAgICAgICAgIGRpdi5hcHBlbmRDaGlsZChsYmwpO1xyXG4gICAgICAgICAgICAgICAgZS50YXJnZXQuYXBwZW5kQ2hpbGQoZGl2KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uTW91c2VMZWF2ZSA9IChlKSA9PiB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIHRoaXMucmVmcmVzaERpc3BsYXkoKTtcclxuICAgIH1cclxufVxyXG4iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcIi4uL0NvbnRyb2xsZXJcIlxyXG5cclxuZXhwb3J0IGNsYXNzIFBsYXllckxpc3Qge1xyXG4gICAgcHJpdmF0ZSBwbGF5ZXJMaXN0OiBIVE1MRGl2RWxlbWVudDtcclxuXHJcbiAgICBwdWJsaWMgY29uc3RydWN0b3IocHJpdmF0ZSBjb250cm9sbGVyOiBDb250cm9sbGVyKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXJMaXN0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJwbGF5ZXItbGlzdFwiKSBhcyBIVE1MRGl2RWxlbWVudDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBVcGRhdGUgdGhlIEhUTUwgdG8gcmVmbGVjdCBjaGFuZ2VzIHRvIHBsYXllciBsaXN0XHJcbiAgICBwdWJsaWMgcmVmcmVzaERpc3BsYXkoKSB7XHJcbiAgICAgICAgbGV0IGh0bWwgPSBcIlwiO1xyXG4gICAgICAgIGZvcihjb25zdCBpZCBpbiB0aGlzLmNvbnRyb2xsZXIucGxheWVyU3R1YnMpIHtcclxuICAgICAgICAgICAgY29uc3QgcGxheWVyID0gdGhpcy5jb250cm9sbGVyLnBsYXllclN0dWJzW2lkXTtcclxuICAgICAgICAgICAgaHRtbCArPSBgPGJ1dHRvbiBpZD1cInBsYXllci1saXN0LWl0ZW0tJHtpZH1cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzcz1cInBsYXllci1saXN0LWl0ZW1cIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cImJvcmRlci1jb2xvcjoke3BsYXllci5SZWFkeSA/ICcjNTVjYzU1JyA6ICcjY2M1NTU1J307XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICR7cGxheWVyLk5hbWV9PC9idXR0b24+YDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wbGF5ZXJMaXN0LmlubmVySFRNTCA9IGh0bWw7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHVwZGF0ZUxpc3RJdGVtKHBsYXllcklEOiBzdHJpbmcsIHJlYWR5U3RhdGU6IGJvb2xlYW4pIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcInVwZGF0ZSBwbGF5ZXIgXCIgKyBwbGF5ZXJJRCArIFwiLCBcIiArIHJlYWR5U3RhdGUpO1xyXG4gICAgICAgIGNvbnN0IGxpc3RJdGVtID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoYHBsYXllci1saXN0LWl0ZW0tJHtwbGF5ZXJJRH1gKTtcclxuICAgICAgICBpZihsaXN0SXRlbSkge1xyXG4gICAgICAgICAgICAvLyB1cGRhdGUgdGhlIGxpc3QgaXRlbSB0byByZWZsZWN0IHRoZSB1cGRhdGVcclxuICAgICAgICAgICAgaWYocmVhZHlTdGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgbGlzdEl0ZW0uc3R5bGUuYm9yZGVyQ29sb3IgPSBcIiM1NWNjNTVcIjtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxpc3RJdGVtLnN0eWxlLmJvcmRlckNvbG9yID0gXCIjY2M1NTU1XCI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuXHJcbmV4cG9ydCBjbGFzcyBSaWdodFBhbmVsTW9iaWxlRGl2IHtcclxuICAgIHByaXZhdGUgc2hvd0NoYXJCdG46IEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBzaG93SW52ZW50QnRuOiBIVE1MQnV0dG9uRWxlbWVudDtcclxuICAgIHByaXZhdGUgY2hhckRpdjogSFRNTERpdkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGludmVudERpdjogSFRNTERpdkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIHJpZ2h0UGFuZWw6IEhUTUxEaXZFbGVtZW50O1xyXG5cclxuICAgIHB1YmxpYyBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbnRyb2xsZXI6IENvbnRyb2xsZXIpIHtcclxuICAgICAgICB0aGlzLnNob3dDaGFyQnRuID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzaG93LWNoYXItYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuc2hvd0ludmVudEJ0biA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwic2hvdy1pbnZlbnQtYnRuXCIpIGFzIEhUTUxCdXR0b25FbGVtZW50O1xyXG4gICAgICAgIHRoaXMuY2hhckRpdiA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiY2hhci1kaXZcIikgYXMgSFRNTERpdkVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5pbnZlbnREaXYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImludmVudC1kaXZcIikgYXMgSFRNTERpdkVsZW1lbnQ7XHJcbiAgICAgICAgdGhpcy5yaWdodFBhbmVsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJyaWdodC1wYW5lbFwiKSBhcyBIVE1MRGl2RWxlbWVudDtcclxuICAgICAgICB0aGlzLnNob3dDaGFyQnRuLmFkZEV2ZW50TGlzdGVuZXIoQ29udHJvbGxlci5DTElDS19FVkVOVCwgdGhpcy5vblNob3dDaGFyQnRuQ2xpY2spO1xyXG4gICAgICAgIHRoaXMuc2hvd0ludmVudEJ0bi5hZGRFdmVudExpc3RlbmVyKENvbnRyb2xsZXIuQ0xJQ0tfRVZFTlQsIHRoaXMub25TaG93SW52ZW50QnRuQ2xpY2spO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoaWRlKCkge1xyXG4gICAgICAgIHRoaXMucmlnaHRQYW5lbC5zdHlsZS52aXNpYmlsaXR5ID0gXCJoaWRkZW5cIjtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uU2hvd0NoYXJCdG5DbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zaG93SW52ZW50QnRuLnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XHJcbiAgICAgICAgdGhpcy5zaG93Q2hhckJ0bi5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICAgICAgdGhpcy5pbnZlbnREaXYuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xyXG4gICAgICAgIHRoaXMuY2hhckRpdi5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25TaG93SW52ZW50QnRuQ2xpY2sgPSAoZSkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2hvd0ludmVudEJ0bi5zdHlsZS5kaXNwbGF5ID0gXCJub25lXCI7XHJcbiAgICAgICAgdGhpcy5zaG93Q2hhckJ0bi5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xyXG4gICAgICAgIHRoaXMuaW52ZW50RGl2LnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XHJcbiAgICAgICAgdGhpcy5jaGFyRGl2LnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcclxuICAgIH1cclxufVxyXG4iLCJpbXBvcnQgeyBDb250cm9sbGVyIH0gZnJvbSBcIi4uL0NvbnRyb2xsZXJcIlxyXG5cclxuZXhwb3J0IGNsYXNzIFRvZ2dsZVJlYWR5QnRuIHtcclxuICAgIHByaXZhdGUgdG9nZ2xlUmVhZHlCdG47XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIHRoaXMudG9nZ2xlUmVhZHlCdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRvZ2dsZS1yZWFkeS1idG5cIik7XHJcbiAgICAgICAgdGhpcy50b2dnbGVSZWFkeUJ0bi5hZGRFdmVudExpc3RlbmVyKENvbnRyb2xsZXIuQ0xJQ0tfRVZFTlQsIHRoaXMub25DbGljayk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHVwZGF0ZSgpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcInVwZGF0ZSB0b2dnbGUgcmVhZHkgYnRuXCIsIHRoaXMuY29udHJvbGxlci5teVBsYXllclN0dWIpXHJcbiAgICAgICAgaWYoIXRoaXMuY29udHJvbGxlci5teVBsYXllclN0dWIuUmVhZHkpIHtcclxuICAgICAgICAgICAgdGhpcy5zaG93KCk7XHJcbiAgICAgICAgICAgIHRoaXMuZW5hYmxlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlKCk7XHJcbiAgICAgICAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaGlkZSgpIHtcclxuICAgICAgICB0aGlzLnRvZ2dsZVJlYWR5QnRuLnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaG93KCkge1xyXG4gICAgICAgIHRoaXMudG9nZ2xlUmVhZHlCdG4uc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBkaXNhYmxlKCkge1xyXG4gICAgICAgIHRoaXMudG9nZ2xlUmVhZHlCdG4uZGlzYWJsZWQgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBlbmFibGUoKSB7XHJcbiAgICAgICAgdGhpcy50b2dnbGVSZWFkeUJ0bi5kaXNhYmxlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBpc1Zpc2libGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudG9nZ2xlUmVhZHlCdG4uc3R5bGUudmlzaWJpbGl0eSA9PSBcInZpc2libGVcIjtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb25DbGljayA9IChlKSA9PiB7XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLnRvZ2dsZVJlYWR5U3RhdGUoKTtcclxuICAgIH07XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuaW1wb3J0IHsgVGlsZVR5cGUgfSBmcm9tIFwiLi4vZ2FtZS9UaWxlVHlwZVwiXHJcbmltcG9ydCB7IEdhbWVQaGFzZSB9IGZyb20gXCIuLi9nYW1lL0dhbWVQaGFzZVwiXHJcbmltcG9ydCB7IEFjdGlvbkFwQ29zdHMgfSBmcm9tIFwiLi4vZ2FtZS9BY3Rpb25cIlxyXG5pbXBvcnQgeyBJdGVtTWVtZW50byB9IGZyb20gXCIuLi9nYW1lL0l0ZW1NZW1lbnRvXCJcclxuaW1wb3J0IHsgSXRlbSwgSXRlbUVudGl0eSwgR3VuLCBCYW5kYWdlLCBpc0d1biwgaXNCYW5kYWdlIH0gZnJvbSBcIi4uL2dhbWUvSXRlbVwiXHJcblxyXG5leHBvcnQgY2xhc3MgVG9vbHRpcCB7XHJcbiAgICBwcml2YXRlIHRvb2x0aXBEaXY6IEhUTUxEaXZFbGVtZW50O1xyXG5cclxuICAgIHB1YmxpYyBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbnRyb2xsZXI6IENvbnRyb2xsZXIpIHtcclxuICAgICAgICB0aGlzLnRvb2x0aXBEaXYgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRvb2x0aXAtZGl2XCIpIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoaWRlVG9vbHRpcCgpIHtcclxuICAgICAgICB0aGlzLnRvb2x0aXBEaXYuc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzaG93KHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgLy8gc2V0IHRoZSBwb3NpdGlvbiBvZiB0aGUgdG9vbHRpcFxyXG4gICAgICAgIHRoaXMudG9vbHRpcERpdi5zdHlsZS5sZWZ0ID0gYGNhbGMoJHt4fXB4IC0gJHt0aGlzLnRvb2x0aXBEaXYuY2xpZW50V2lkdGgvMn1weClgO1xyXG4gICAgICAgIHRoaXMudG9vbHRpcERpdi5zdHlsZS50b3AgPSBgJHt5KzI1fXB4YDtcclxuXHJcbiAgICAgICAgLy8gbWFrZSBzdXJlIHRoZSB0b29sdGlwIGlzIHZpc2libGVcclxuICAgICAgICB0aGlzLnRvb2x0aXBEaXYuc3R5bGUudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY2xlYXIoKSB7XHJcbiAgICAgICAgLy8gY2xlYXIgY29udGVudHMgb2YgZGl2XHJcbiAgICAgICAgd2hpbGUodGhpcy50b29sdGlwRGl2LmZpcnN0Q2hpbGQpIHtcclxuICAgICAgICAgICAgdGhpcy50b29sdGlwRGl2LnJlbW92ZUNoaWxkKHRoaXMudG9vbHRpcERpdi5maXJzdENoaWxkKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNob3dEcm9wVGlsZVRvb2x0aXAoaTogbnVtYmVyLCBqOiBudW1iZXIsIHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgaWYoaSA+PSAwICYmIGogPj0gMCkge1xyXG4gICAgICAgICAgICAvLyBnZXQgdGhlIHRpbGUgdHlwZSBvZiB0aGUgdGlsZSBob3ZlcmVkXHJcbiAgICAgICAgICAgIGNvbnN0IHRpbGVUeXBlID0gVGlsZVR5cGUudHlwZXNbdGhpcy5jb250cm9sbGVyLm1hcC5oZXhHcmlkW2pdW2ldLnR5cGVdO1xyXG4gICAgICAgICAgICB0aGlzLmNsZWFyKCk7XHJcblxyXG4gICAgICAgICAgICAvLyBzZXQgdGhlIGNvbnRlbnRzIG9mIHRoZSB0b29sdGlwXHJcbiAgICAgICAgICAgIGxldCBuYW1lTGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICBuYW1lTGJsLmlubmVySFRNTCA9IHRpbGVUeXBlLm5hbWU7XHJcbiAgICAgICAgICAgIHRoaXMudG9vbHRpcERpdi5hcHBlbmRDaGlsZChuYW1lTGJsKTtcclxuXHJcbiAgICAgICAgICAgIGlmKHRpbGVUeXBlLmltcGFzc2FibGUgfHwgdGlsZVR5cGUudW5kcm9wcGFibGUpIHtcclxuICAgICAgICAgICAgICAgIGxldCBsYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIik7XHJcbiAgICAgICAgICAgICAgICBsYmwuaW5uZXJIVE1MID0gXCJZb3UgY2FuJ3QgbGFuZCBoZXJlXCI7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRvb2x0aXBEaXYuYXBwZW5kQ2hpbGQobGJsKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGxldCBsYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIik7XHJcbiAgICAgICAgICAgICAgICBsYmwuaW5uZXJIVE1MID0gXCJDbGljayB0byBzZXQgRHJvcCBMb2NhdGlvblwiO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b29sdGlwRGl2LmFwcGVuZENoaWxkKGxibCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2hvdyh4LCB5KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmhpZGVUb29sdGlwKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaG93VGlsZVRvb2x0aXAoaTogbnVtYmVyLCBqOiBudW1iZXIsIHg6IG51bWJlciwgeTogbnVtYmVyKSB7XHJcbiAgICAgICAgaWYoaSA+PSAwICYmIGogPj0gMCkge1xyXG4gICAgICAgICAgICAvLyBnZXQgdGhlIHRpbGUgdHlwZSBvZiB0aGUgdGlsZSBob3ZlcmVkXHJcbiAgICAgICAgICAgIGNvbnN0IHRpbGVUeXBlID0gVGlsZVR5cGUudHlwZXNbdGhpcy5jb250cm9sbGVyLm1hcC5oZXhHcmlkW2pdW2ldLnR5cGVdO1xyXG4gICAgICAgICAgICB0aGlzLmNsZWFyKCk7XHJcblxyXG4gICAgICAgICAgICAvLyBzZXQgdGhlIGNvbnRlbnRzIG9mIHRoZSB0b29sdGlwXHJcbiAgICAgICAgICAgIGxldCBuYW1lTGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICBuYW1lTGJsLmlubmVySFRNTCA9IHRpbGVUeXBlLm5hbWU7XHJcbiAgICAgICAgICAgIHRoaXMudG9vbHRpcERpdi5hcHBlbmRDaGlsZChuYW1lTGJsKTtcclxuXHJcbiAgICAgICAgICAgIGZvcihjb25zdCBkZXNjIG9mIHRpbGVUeXBlLmRlc2MpIHtcclxuICAgICAgICAgICAgICAgIGxldCBkZXNjTGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICAgICAgZGVzY0xibC5pbm5lckhUTUwgPSBkZXNjO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b29sdGlwRGl2LmFwcGVuZENoaWxkKGRlc2NMYmwpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnNob3coeCwgeSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5oaWRlVG9vbHRpcCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2hvd0FjdGlvblRvb2x0aXAobmFtZTogc3RyaW5nLCBkZXNjOiBzdHJpbmcsIGFwQ29zdDogbnVtYmVyLCB4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIGlmKG5hbWUgIT0gbnVsbCAmJiBkZXNjICE9IG51bGwpIHtcclxuICAgICAgICAgICAgLy8gY2xlYXIgY29udGVudHMgb2YgZGl2XHJcbiAgICAgICAgICAgIHRoaXMuY2xlYXIoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIHNldCB0aGUgY29udGVudHMgb2YgdGhlIHRvb2x0aXBcclxuICAgICAgICAgICAgbGV0IG5hbWVMYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIik7XHJcbiAgICAgICAgICAgIG5hbWVMYmwuaW5uZXJIVE1MID0gbmFtZTtcclxuICAgICAgICAgICAgbmFtZUxibC5zdHlsZS5kaXNwbGF5ID0gXCJpbmxpbmVcIjtcclxuICAgICAgICAgICAgdGhpcy50b29sdGlwRGl2LmFwcGVuZENoaWxkKG5hbWVMYmwpO1xyXG5cclxuICAgICAgICAgICAgbGV0IGFwTGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICBhcExibC5pbm5lckhUTUwgPSBgJHthcENvc3R9QVBgO1xyXG4gICAgICAgICAgICBhcExibC5zdHlsZS5kaXNwbGF5ID0gXCJpbmxpbmVcIjtcclxuICAgICAgICAgICAgYXBMYmwuc3R5bGUuY29sb3IgPSBcIiMwMDAwZTVcIjtcclxuICAgICAgICAgICAgdGhpcy50b29sdGlwRGl2LmFwcGVuZENoaWxkKGFwTGJsKTtcclxuXHJcbiAgICAgICAgICAgIGxldCBkZXNjTGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICBkZXNjTGJsLmlubmVySFRNTCA9IGRlc2M7XHJcbiAgICAgICAgICAgIHRoaXMudG9vbHRpcERpdi5hcHBlbmRDaGlsZChkZXNjTGJsKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2hvdyh4LCB5KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmhpZGVUb29sdGlwKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzaG93SXRlbVRvb2x0aXAoZW50aXR5OiBJdGVtRW50aXR5LCB4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIGlmKCFlbnRpdHkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBpdGVtID0gdGhpcy5jb250cm9sbGVyLml0ZW1zW2VudGl0eS5pdGVtSURdO1xyXG4gICAgICAgIGlmKGl0ZW0pIHtcclxuICAgICAgICAgICAgLy8gY2xlYXIgY29udGVudHMgb2YgZGl2XHJcbiAgICAgICAgICAgIHRoaXMuY2xlYXIoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIHNldCB0aGUgY29udGVudHMgb2YgdGhlIHRvb2x0aXBcclxuICAgICAgICAgICAgbGV0IG5hbWVMYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIik7XHJcbiAgICAgICAgICAgIG5hbWVMYmwuaW5uZXJIVE1MID0gaXRlbS5uYW1lO1xyXG4gICAgICAgICAgICBuYW1lTGJsLnN0eWxlLmRpc3BsYXkgPSBcImlubGluZVwiO1xyXG4gICAgICAgICAgICB0aGlzLnRvb2x0aXBEaXYuYXBwZW5kQ2hpbGQobmFtZUxibCk7XHJcblxyXG4gICAgICAgICAgICAvLyBpZiB0aGUgaXRlbSBpcyBhIGd1biwgaW5jbHVkZSBndW4gc3BlY2lmaWMgaW5mb1xyXG4gICAgICAgICAgICBpZihpc0d1bihpdGVtKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZ3VuID0gaXRlbSBhcyBHdW47XHJcblxyXG4gICAgICAgICAgICAgICAgaWYoKHRoaXMuY29udHJvbGxlci5teVBsYXllckVxdWlwcGVkSXRlbSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbnRpdHkuZW50aXR5SUQgPT09IHRoaXMuY29udHJvbGxlci5teVBsYXllckVxdWlwcGVkSXRlbS5lbnRpdHlJRCkgfHxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLmdhbWVQaGFzZSA9PT0gR2FtZVBoYXNlLkFDVElPTl9QSEFTRSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBlcXVpcExibCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsYWJlbFwiKTtcclxuICAgICAgICAgICAgICAgICAgICBlcXVpcExibC5pbm5lckhUTUwgPSB0aGlzLmNvbnRyb2xsZXIubXlQbGF5ZXJFcXVpcHBlZEl0ZW0gJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVudGl0eS5lbnRpdHlJRCA9PT0gdGhpcy5jb250cm9sbGVyLm15UGxheWVyRXF1aXBwZWRJdGVtLmVudGl0eUlEID9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFwiRXF1aXBwZWRcIiA6IFwiQ2xpY2sgVG8gRXF1aXAgXCIrQWN0aW9uQXBDb3N0cy5FUVVJUF9XRUFQT04rXCJBUFwiO1xyXG4gICAgICAgICAgICAgICAgICAgIGVxdWlwTGJsLnN0eWxlLmRpc3BsYXkgPSBcImlubGluZVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIGVxdWlwTGJsLnN0eWxlLmNvbG9yID0gXCIjMDAwMGU1XCI7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50b29sdGlwRGl2LmFwcGVuZENoaWxkKGVxdWlwTGJsKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgbWF4UmFuZ2VMYmwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwibGFiZWxcIik7XHJcbiAgICAgICAgICAgICAgICBtYXhSYW5nZUxibC5pbm5lckhUTUwgPSBcIk1heCBSYW5nZSA8c3BhbiBzdHlsZT0nY29sb3I6IzAwMDBlNSc+XCIrZ3VuLm1heFJhbmdlK1wiPC9zcGFuPlwiO1xyXG4gICAgICAgICAgICAgICAgbWF4UmFuZ2VMYmwuc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMudG9vbHRpcERpdi5hcHBlbmRDaGlsZChtYXhSYW5nZUxibCk7XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IGFjY3VyYWN5TGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICAgICAgYWNjdXJhY3lMYmwuaW5uZXJIVE1MID0gXCJBY2N1cmFjeSA8c3BhbiBzdHlsZT0nY29sb3I6IzAwMDBlNSc+XCIrTWF0aC5mbG9vcihndW4uYWNjdXJhY3kqMTAwKzAuNSkrXCIlPC9zcGFuPlwiO1xyXG4gICAgICAgICAgICAgICAgYWNjdXJhY3lMYmwuc3R5bGUuZGlzcGxheSA9IFwiYmxvY2tcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMudG9vbHRpcERpdi5hcHBlbmRDaGlsZChhY2N1cmFjeUxibCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gaWYgdGhlIGl0ZW0gaXMgYSBiYW5kYWdlLCBpbmNsdWRlIGJhbmRhZ2Ugc3BlY2lmaWMgaW5mb1xyXG4gICAgICAgICAgICBlbHNlIGlmKGlzQmFuZGFnZShpdGVtKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYmFuZGFnZSA9IGl0ZW0gYXMgQmFuZGFnZTtcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgaGVhbEFtb3VudExibCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsYWJlbFwiKTtcclxuICAgICAgICAgICAgICAgIGhlYWxBbW91bnRMYmwuaW5uZXJIVE1MID0gXCJIZWFsIEFtb3VudCA8c3BhbiBzdHlsZT0nY29sb3I6IzAwMDBlNSc+XCIrYmFuZGFnZS5oZWFsQW1vdW50K1wiPC9zcGFuPlwiO1xyXG4gICAgICAgICAgICAgICAgaGVhbEFtb3VudExibC5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b29sdGlwRGl2LmFwcGVuZENoaWxkKGhlYWxBbW91bnRMYmwpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnNob3coeCwgeSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5oaWRlVG9vbHRpcCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2hvd0l0ZW1NZW1lbnRvVG9vbHRpcChtZW06IEl0ZW1NZW1lbnRvLCB4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIGlmKG1lbSkge1xyXG4gICAgICAgICAgICB0aGlzLmNsZWFyKCk7XHJcblxyXG4gICAgICAgICAgICAvLyBzZXQgdGhlIGNvbnRlbnRzIG9mIHRoZSB0b29sdGlwXHJcbiAgICAgICAgICAgIGxldCBuYW1lTGJsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxhYmVsXCIpO1xyXG4gICAgICAgICAgICBuYW1lTGJsLmlubmVySFRNTCA9IFwiTGFzdCBWaXNpdGVkIFwiICsgbWVtLnNpbmNlICsgXCIgUm91bmQocykgQWdvXCI7XHJcbiAgICAgICAgICAgIG5hbWVMYmwuc3R5bGUuZGlzcGxheSA9IFwiaW5saW5lXCI7XHJcbiAgICAgICAgICAgIHRoaXMudG9vbHRpcERpdi5hcHBlbmRDaGlsZChuYW1lTGJsKTtcclxuXHJcbiAgICAgICAgICAgIC8vIGFkZCBhbiBpY29uIGZvciBlYWNoIGl0ZW1cclxuICAgICAgICAgICAgZm9yKGNvbnN0IGl0ZW0gb2YgbWVtLml0ZW1zKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgaW1nID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImltZ1wiKTtcclxuICAgICAgICAgICAgICAgIGltZy5jbGFzc0xpc3QuYWRkKFwiaXRlbS1mb3VuZC1pbWdcIik7XHJcbiAgICAgICAgICAgICAgICBpbWcuc3JjID0gaXRlbS5pbWFnZTtcclxuICAgICAgICAgICAgICAgIGltZy50aXRsZSA9IGl0ZW0ubmFtZTtcclxuICAgICAgICAgICAgICAgIGltZy5zdHlsZS53aWR0aCA9IGAke3RoaXMudG9vbHRpcERpdi5jbGllbnRXaWR0aC8zKml0ZW0uZ3JpZFdpZHRofXB4YDtcclxuICAgICAgICAgICAgICAgIGltZy5zdHlsZS5oZWlnaHQgPSBgJHt0aGlzLnRvb2x0aXBEaXYuY2xpZW50V2lkdGgvMyppdGVtLmdyaWRIZWlnaHR9cHhgO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b29sdGlwRGl2LmFwcGVuZENoaWxkKGltZyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2hvdyh4LCB5KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmhpZGVUb29sdGlwKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCB7IENvbnRyb2xsZXIgfSBmcm9tIFwiLi4vQ29udHJvbGxlclwiXHJcblxyXG5leHBvcnQgY2xhc3MgVHVybkxibCB7XHJcbiAgICBwcml2YXRlIHRpdGxlTGJsOiBIVE1MTGFiZWxFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBpbnN0cnVjdGlvbnNMYmw6IEhUTUxMYWJlbEVsZW1lbnQ7XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIHRoaXMudGl0bGVMYmwgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInRpdGxlLWxibFwiKSBhcyBIVE1MTGFiZWxFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuaW5zdHJ1Y3Rpb25zTGJsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJpbnN0cnVjdGlvbnMtbGJsXCIpIGFzIEhUTUxMYWJlbEVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNldFRpdGxlVGV4dCh0ZXh0OiBzdHJpbmcpIHtcclxuICAgICAgICBpZih0ZXh0ICE9IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy50aXRsZUxibC5pbm5lckhUTUwgPSB0ZXh0O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2V0SW5zdHJ1Y3Rpb25zVGV4dCh0ZXh0OiBzdHJpbmcpIHtcclxuICAgICAgICBpZih0ZXh0ICE9IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5pbnN0cnVjdGlvbnNMYmwuaW5uZXJIVE1MID0gdGV4dDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuXHJcbmV4cG9ydCBjbGFzcyBWaWV3cG9ydERhbWFnZUluZGljYXRvciB7XHJcbiAgICBwcml2YXRlIGJsb29kVHJpYW5nbGVUb3BMZWZ0OiBIVE1MRGl2RWxlbWVudDtcclxuICAgIHByaXZhdGUgYmxvb2RUcmlhbmdsZVRvcFJpZ2h0OiBIVE1MRGl2RWxlbWVudDtcclxuICAgIHByaXZhdGUgYmxvb2RUcmlhbmdsZUJvdHRvbUxlZnQ6IEhUTUxEaXZFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBibG9vZFRyaWFuZ2xlQm90dG9tUmlnaHQ6IEhUTUxEaXZFbGVtZW50O1xyXG5cclxuICAgIHB1YmxpYyBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbnRyb2xsZXI6IENvbnRyb2xsZXIpIHtcclxuICAgICAgICB0aGlzLmJsb29kVHJpYW5nbGVUb3BMZWZ0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJibG9vZC10cmlhbmdsZS10b3AtbGVmdFwiKSBhcyBIVE1MRGl2RWxlbWVudDtcclxuICAgICAgICB0aGlzLmJsb29kVHJpYW5nbGVUb3BSaWdodCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYmxvb2QtdHJpYW5nbGUtdG9wLXJpZ2h0XCIpIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZUJvdHRvbUxlZnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImJsb29kLXRyaWFuZ2xlLWJvdHRvbS1sZWZ0XCIpIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZUJvdHRvbVJpZ2h0ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJibG9vZC10cmlhbmdsZS1ib3R0b20tcmlnaHRcIikgYXMgSFRNTERpdkVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlZnJlc2hEaXNwbGF5KHRvdGFsSFBDaGFuZ2VkQnk6IG51bWJlcikge1xyXG4gICAgICAgIGNvbnN0IHBlcmNlbnRhZ2UgPSAtdG90YWxIUENoYW5nZWRCeSoxLjUgPiA1MCA/IDUwIDogLXRvdGFsSFBDaGFuZ2VkQnkqMS41O1xyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZVRvcExlZnQuc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XHJcbiAgICAgICAgdGhpcy5ibG9vZFRyaWFuZ2xlVG9wTGVmdC5zdHlsZS53aWR0aCA9IGAke3BlcmNlbnRhZ2V9JWA7XHJcbiAgICAgICAgdGhpcy5ibG9vZFRyaWFuZ2xlVG9wTGVmdC5zdHlsZS5oZWlnaHQgPSBgJHtwZXJjZW50YWdlfSVgO1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZVRvcExlZnQuY2xhc3NMaXN0LnJlbW92ZShcImJsb29kLXNwbGF0LWRpc3NhcHBlYXJcIik7XHJcbiAgICAgICAgdm9pZCB0aGlzLmJsb29kVHJpYW5nbGVUb3BMZWZ0Lm9mZnNldFdpZHRoO1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZVRvcExlZnQuY2xhc3NMaXN0LmFkZChcImJsb29kLXNwbGF0LWRpc3NhcHBlYXJcIik7XHJcblxyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZVRvcFJpZ2h0LnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZVRvcFJpZ2h0LnN0eWxlLndpZHRoID0gYCR7cGVyY2VudGFnZX0lYDtcclxuICAgICAgICB0aGlzLmJsb29kVHJpYW5nbGVUb3BSaWdodC5zdHlsZS5oZWlnaHQgPSBgJHtwZXJjZW50YWdlfSVgO1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZVRvcFJpZ2h0LmNsYXNzTGlzdC5yZW1vdmUoXCJibG9vZC1zcGxhdC1kaXNzYXBwZWFyXCIpO1xyXG4gICAgICAgIHZvaWQgdGhpcy5ibG9vZFRyaWFuZ2xlVG9wUmlnaHQub2Zmc2V0V2lkdGg7XHJcbiAgICAgICAgdGhpcy5ibG9vZFRyaWFuZ2xlVG9wUmlnaHQuY2xhc3NMaXN0LmFkZChcImJsb29kLXNwbGF0LWRpc3NhcHBlYXJcIik7XHJcblxyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZUJvdHRvbUxlZnQuc3R5bGUudmlzaWJpbGl0eSA9IFwiaGlkZGVuXCI7XHJcbiAgICAgICAgdGhpcy5ibG9vZFRyaWFuZ2xlQm90dG9tTGVmdC5zdHlsZS53aWR0aCA9IGAke3BlcmNlbnRhZ2V9JWA7XHJcbiAgICAgICAgdGhpcy5ibG9vZFRyaWFuZ2xlQm90dG9tTGVmdC5zdHlsZS5oZWlnaHQgPSBgJHtwZXJjZW50YWdlfSVgO1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZUJvdHRvbUxlZnQuY2xhc3NMaXN0LnJlbW92ZShcImJsb29kLXNwbGF0LWRpc3NhcHBlYXJcIik7XHJcbiAgICAgICAgdm9pZCB0aGlzLmJsb29kVHJpYW5nbGVCb3R0b21MZWZ0Lm9mZnNldFdpZHRoO1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZUJvdHRvbUxlZnQuY2xhc3NMaXN0LmFkZChcImJsb29kLXNwbGF0LWRpc3NhcHBlYXJcIik7XHJcblxyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZUJvdHRvbVJpZ2h0LnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZUJvdHRvbVJpZ2h0LnN0eWxlLndpZHRoID0gYCR7cGVyY2VudGFnZX0lYDtcclxuICAgICAgICB0aGlzLmJsb29kVHJpYW5nbGVCb3R0b21SaWdodC5zdHlsZS5oZWlnaHQgPSBgJHtwZXJjZW50YWdlfSVgO1xyXG4gICAgICAgIHRoaXMuYmxvb2RUcmlhbmdsZUJvdHRvbVJpZ2h0LmNsYXNzTGlzdC5yZW1vdmUoXCJibG9vZC1zcGxhdC1kaXNzYXBwZWFyXCIpO1xyXG4gICAgICAgIHZvaWQgdGhpcy5ibG9vZFRyaWFuZ2xlQm90dG9tUmlnaHQub2Zmc2V0V2lkdGg7XHJcbiAgICAgICAgdGhpcy5ibG9vZFRyaWFuZ2xlQm90dG9tUmlnaHQuY2xhc3NMaXN0LmFkZChcImJsb29kLXNwbGF0LWRpc3NhcHBlYXJcIik7XHJcbiAgICB9XHJcbn1cclxuIiwiaW1wb3J0IHsgQ29udHJvbGxlciB9IGZyb20gXCIuLi9Db250cm9sbGVyXCJcclxuaW1wb3J0IHsgZmV0Y2hNYXAsIGZldGNoUGxheWVyTGlzdCB9IGZyb20gXCIuL0hUVFBIYW5kbGVyXCJcclxuaW1wb3J0IHsgTWFwIH0gZnJvbSBcIi4uL2dhbWUvTWFwXCJcclxuaW1wb3J0IHsgSXRlbSwgR3VuLCBQcm9qZWN0aWxlLCBCYW5kYWdlIH0gZnJvbSBcIi4uL2dhbWUvSXRlbVwiXHJcbmltcG9ydCB7IFBsYXllckRldGFpbHMgfSBmcm9tIFwiLi4vZ2FtZS9QbGF5ZXJEZXRhaWxzXCJcclxuaW1wb3J0IHsgUGxheWVySGVhbHRoIH0gZnJvbSBcIi4uL2dhbWUvUGxheWVySGVhbHRoXCJcclxuXHJcbi8vIEFsbG93cyB1c2Ugb2YganMtY29va2llXHJcbmRlY2xhcmUgdmFyIENvb2tpZXM7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2xpZW50IHtcclxuICAgIHByaXZhdGUgc29ja2V0OiBXZWJTb2NrZXQ7XHJcblxyXG4gICAgcHVibGljIGNvbnN0cnVjdG9yKHByaXZhdGUgY29udHJvbGxlcjogQ29udHJvbGxlcikge1xyXG4gICAgICAgIHRoaXMuY29ubmVjdFRvV2ViU29ja2V0U2VydmVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjb25uZWN0VG9XZWJTb2NrZXRTZXJ2ZXIoKSB7XHJcbiAgICAgICAgdGhpcy5zb2NrZXQgPSBuZXcgV2ViU29ja2V0KGB3czovLyR7bG9jYXRpb24uaG9zdG5hbWV9L29wZW5fd3NfY29ubmVjdGlvbmApO1xyXG5cclxuICAgICAgICB0aGlzLnNvY2tldC5vbm9wZW4gPSAoZSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNvY2tldC5zZW5kKGBBbmFtZToke0Nvb2tpZXMuZ2V0KFwicGxheWVyX25hbWVcIil9O3Rva2VuOiR7Q29va2llcy5nZXQoXCJwbGF5ZXJfdG9rZW5cIil9O2xvYmJ5OiR7Q29va2llcy5nZXQoXCJsb2JieV9uYW1lXCIpfWApO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMuc29ja2V0Lm9ubWVzc2FnZSA9IChlKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub25NZXNzYWdlKGUuZGF0YSk7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc2VuZFRvZ2dsZVJlYWR5U3RhdGVNc2coKSB7XHJcbiAgICAgICAgdGhpcy5zb2NrZXQuc2VuZChgQmApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZW5kU2V0UmVhZHlTdGF0ZVRvVHJ1ZU1zZygpIHtcclxuICAgICAgICB0aGlzLnNvY2tldC5zZW5kKGBDYCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNlbmRTZXRNb3ZlVHVybk1zZyh4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuc29ja2V0LnNlbmQoYEdjcm91Y2g6ZmFsc2U7eDoke3h9O3k6JHt5fWApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZW5kU2VhcmNoVGlsZU1zZyh4OiBudW1iZXIsIHk6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuc29ja2V0LnNlbmQoYE14OiR7eH07eToke3l9YCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNlbmRUYWtlSXRlbU1zZyhlbnRpdHlJRDogbnVtYmVyLCBiYWNrcGFja1g6IG51bWJlciwgYmFja3BhY2tZOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnNvY2tldC5zZW5kKGBIZW50aXR5SUQ6JHtlbnRpdHlJRH07YmFja3BhY2tYOiR7YmFja3BhY2tYfTtiYWNrcGFja1k6JHtiYWNrcGFja1l9YCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNlbmRFcXVpcEl0ZW1Nc2coZW50aXR5SUQ6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuc29ja2V0LnNlbmQoYEllbnRpdHlJRDoke2VudGl0eUlEfWApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZW5kRHJvcEl0ZW1Nc2coZW50aXR5SUQ6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMuc29ja2V0LnNlbmQoYEtlbnRpdHlJRDoke2VudGl0eUlEfWApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZW5kVXNlQmFuZGFnZU1zZyhlbnRpdHlJRDogbnVtYmVyLCBsaW1iOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLnNvY2tldC5zZW5kKGBMZW50aXR5SUQ6JHtlbnRpdHlJRH07bGltYjoke2xpbWJ9YCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNlbmRBdHRhY2tQbGF5ZXJNc2cocGxheWVySUQ6IHN0cmluZywgbGltYjogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5zb2NrZXQuc2VuZChgSnBsYXllcklEOiR7cGxheWVySUR9O2xpbWI6JHtsaW1ifWApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZW5kRW5kVHVybk1zZygpIHtcclxuICAgICAgICB0aGlzLnNvY2tldC5zZW5kKGBEYCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHNlbmRTZW5kTXNnTXNnKG1zZzogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5zb2NrZXQuc2VuZChgRW1zZzoke21zZ31gKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDYWxsZWQgdXBvbiByZWNlaXZpbmcgYSBtZXNzYWdlIGZyb20gdGhlIHNlcnZlclxyXG4gICAgcHJpdmF0ZSBvbk1lc3NhZ2UobXNnOiBzdHJpbmcpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcInJlY2VpdmVkOiBcIiArIG1zZyk7XHJcbiAgICAgICAgaWYobXNnID09IFwidXBkYXRlOnBsYXllcnNcIikge1xyXG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NQbGF5ZXJzVXBkYXRlKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZy5zdGFydHNXaXRoKFwidXBkYXRlOnJlYWR5O1wiKSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NSZWFkeVN0YXRlVXBkYXRlKG1zZyk7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZy5zdGFydHNXaXRoKFwidXBkYXRlOmdhbWU7XCIpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc0dhbWVVcGRhdGUobXNnKTtcclxuICAgICAgICB9IGVsc2UgaWYobXNnLnN0YXJ0c1dpdGgoXCJ1cGRhdGU6ZGVhZDtcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzRGVhZFBsYXllcnNVcGRhdGUobXNnKTtcclxuICAgICAgICB9IGVsc2UgaWYobXNnLnN0YXJ0c1dpdGgoXCJ1cGRhdGU6d2lubmVycztcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzV2lubmVyc1VwZGF0ZShtc2cpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cuc3RhcnRzV2l0aChcInVwZGF0ZTpzdG9ybTtcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzU3Rvcm1VcGRhdGUobXNnKTtcclxuICAgICAgICB9IGVsc2UgaWYobXNnLnN0YXJ0c1dpdGgoXCJ5b3VyaWQ7XCIpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc015SURVcGRhdGUobXNnKTtcclxuICAgICAgICB9IGVsc2UgaWYobXNnLnN0YXJ0c1dpdGgoXCJwaGFzZXRpbWU7XCIpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc1BoYXNlVGltZVVwZGF0ZShtc2cpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cuc3RhcnRzV2l0aChcIm1hcDtcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzTWFwVXBkYXRlKG1zZyk7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZy5zdGFydHNXaXRoKFwiaXRlbXM7XCIpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc0l0ZW1zTGlzdChtc2cpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cgPT0gXCJyZXF1ZXN0OmRyb3BcIikge1xyXG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NEcm9wUGhhc2UoKTtcclxuICAgICAgICB9IGVsc2UgaWYobXNnID09IFwicmVxdWVzdDptb3ZlXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzTW92ZVBoYXNlKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZyA9PSBcInJlcXVlc3Q6YWN0aW9uXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzQWN0aW9uUGhhc2UoKTtcclxuICAgICAgICB9IGVsc2UgaWYobXNnLnN0YXJ0c1dpdGgoXCJ0YWtlaXRlbTp2YWxpZDtcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzVGFrZUl0ZW1WYWxpZChtc2cpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cgPT0gXCJ0YWtlaXRlbTppbnZhbGlkO1wiKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidGFrZSBpdGVtIGludmFsaWRcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZy5zdGFydHNXaXRoKFwiZXF1aXBpdGVtOnZhbGlkO1wiKSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NFcXVpcEl0ZW1WYWxpZChtc2cpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cgPT0gXCJlcXVpcGl0ZW06aW52YWxpZDtcIikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImVxdWlwIGl0ZW0gaW52YWxpZFwiKTtcclxuICAgICAgICB9IGVsc2UgaWYobXNnLnN0YXJ0c1dpdGgoXCJkcm9waXRlbTp2YWxpZDtcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzRHJvcEl0ZW1WYWxpZChtc2cpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cgPT0gXCJkcm9waXRlbTppbnZhbGlkO1wiKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZHJvcCBpdGVtIGludmFsaWRcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZyA9PSBcIm1vdmU6dmFsaWRcIikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIm1vdmUgdmFsaWRcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZyA9PSBcIm1vdmU6aW52YWxpZFwiKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwibW92ZSBpbnZhbGlkXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cgPT0gXCJhdHRhY2s6dmFsaWRcIikge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25BdHRhY2tWYWxpZCgpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cgPT0gXCJhdHRhY2s6aW52YWxpZFwiKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiYXR0YWNrIGludmFsaWRcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZy5zdGFydHNXaXRoKFwidXNlYmFuZGFnZTp2YWxpZDtcIikpIHtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzVXNlQmFuZGFnZVZhbGlkKG1zZyk7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZyA9PSBcInVzZWJhbmRhZ2U6aW52YWxpZDtcIikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInVzZSBiYW5kYWdlIGludmFsaWRcIik7XHJcbiAgICAgICAgfSBlbHNlIGlmKG1zZyA9PSBcInNlYXJjaDp2YWxpZDtcIikge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25TZWFyY2hWYWxpZCgpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cgPT0gXCJzZWFyY2g6aW52YWxpZDtcIikge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInNlYXJjaCBpbnZhbGlkXCIpO1xyXG4gICAgICAgIH0gZWxzZSBpZihtc2cuc3RhcnRzV2l0aChcImRpc2Nvbm5lY3Q7XCIpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc0Rpc2Nvbm5lY3QobXNnKTtcclxuICAgICAgICB9IGVsc2UgaWYobXNnLnN0YXJ0c1dpdGgoXCJtZXNzYWdlO1wiKSkge1xyXG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NQbGF5ZXJNc2cobXNnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzRHJvcFBoYXNlKCkge1xyXG4gICAgICAgIHRoaXMuY29udHJvbGxlci5vbkRyb3BQaGFzZVN0YXJ0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzTW92ZVBoYXNlKCkge1xyXG4gICAgICAgIHRoaXMuY29udHJvbGxlci5vbk1vdmVQaGFzZVN0YXJ0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzQWN0aW9uUGhhc2UoKSB7XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uQWN0aW9uUGhhc2VTdGFydCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcHJvY2Vzc0dhbWVVcGRhdGUobXNnOiBzdHJpbmcpIHtcclxuICAgICAgICB2YXIgZGF0YSA9IG1zZy5zcGxpdChcInVwZGF0ZTpnYW1lO1wiKS5sZW5ndGggPiAxID8gbXNnLnNwbGl0KFwidXBkYXRlOmdhbWU7XCIpWzFdIDogbnVsbDtcclxuICAgICAgICBpZihkYXRhKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNwbGl0cyA9IGRhdGEuc3BsaXQoXCI7XCIpO1xyXG4gICAgICAgICAgICBsZXQgaXRlbXMgPSBuZXcgQXJyYXk8eyBlbnRpdHlJRDogc3RyaW5nLCBpdGVtSUQ6IHN0cmluZyB9PigpO1xyXG4gICAgICAgICAgICBsZXQgbmV3UGxheWVyRGV0YWlsczogeyBbaWQ6IG51bWJlcl06IFBsYXllckRldGFpbHMgfSA9IHt9O1xyXG4gICAgICAgICAgICBsZXQgbmV3UGxheWVySFA6IHsgW2lkOiBudW1iZXJdOiBQbGF5ZXJIZWFsdGggfSA9IHt9O1xyXG5cclxuICAgICAgICAgICAgLy8gZXh0cmFjdCByZWxldmFudCBpbmZvcm1hdGlvbiBmcm9tIGRhdGEgc3RyaW5nXHJcbiAgICAgICAgICAgIGZvcihjb25zdCBlbGVtIG9mIHNwbGl0cykge1xyXG4gICAgICAgICAgICAgICAgaWYoZWxlbS5zdGFydHNXaXRoKFwiZGV0YWlsczpwbGF5ZXIsXCIpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZGV0YWlscyA9IGVsZW0uc3BsaXQoXCJkZXRhaWxzOnBsYXllcixcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoZGV0YWlscy5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHAgPSB0aGlzLnByb2Nlc3NQbGF5ZXJEZXRhaWxzVXBkYXRlKGRldGFpbHNbMV0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihwICYmIHAuaWQgJiYgcC5kZXRhaWxzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXdQbGF5ZXJEZXRhaWxzW3AuaWRdID0gcC5kZXRhaWxzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmKGVsZW0uc3RhcnRzV2l0aChcImRldGFpbHM6aXRlbSxcIikpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkZXRhaWxzID0gZWxlbS5zcGxpdChcImRldGFpbHM6aXRlbSxcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoZGV0YWlscy5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGl0ZW0gPSB0aGlzLnByb2Nlc3NJdGVtc0ZvdW5kKGRldGFpbHNbMV0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZihpdGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtcy5wdXNoKGl0ZW0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmKGVsZW0uc3RhcnRzV2l0aChcImRldGFpbHM6bW9kaWZpZXJzLFwiKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRldGFpbHMgPSBlbGVtLnNwbGl0KFwiZGV0YWlsczptb2RpZmllcnMsXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGRldGFpbHMubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2Nlc3NNeVBsYXllck1vZGlmaWVyc1VwZGF0ZShkZXRhaWxzWzFdKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYoZWxlbS5zdGFydHNXaXRoKFwiZGV0YWlsczpocCxcIikpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBkZXRhaWxzID0gZWxlbS5zcGxpdChcImRldGFpbHM6aHAsXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGRldGFpbHMubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwID0gdGhpcy5wcm9jZXNzUGxheWVySFBVcGRhdGUoZGV0YWlsc1sxXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHAgJiYgcC5pZCAmJiBwLmhwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXdQbGF5ZXJIUFtwLmlkXSA9IHAuaHA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5wbGF5ZXJEZXRhaWxzID0gbmV3UGxheWVyRGV0YWlscztcclxuICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLnBsYXllckhQcyA9IG5ld1BsYXllckhQO1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIuc2V0SXRlbXNGb3VuZChpdGVtcyk7XHJcbiAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5yZXF1ZXN0RHJhd01hcCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBQcm9jZXNzIGxpc3Qgb2YgZGVhZCBwbGF5ZXJzXHJcbiAgICBwcml2YXRlIHByb2Nlc3NEZWFkUGxheWVyc1VwZGF0ZShtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSBtc2cuc3BsaXQoXCI7XCIpO1xyXG4gICAgICAgIGZvcihsZXQgaW5kZXggPSAxOyBpbmRleCA8IGRhdGEubGVuZ3RoOyBpbmRleCsrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBsYXllcklEID0gcGFyc2VJbnQoZGF0YVtpbmRleF0sIDEwKTtcclxuICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uUGxheWVyRGVhZChwbGF5ZXJJRCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIFByb2Nlc3MgbGlzdCBvZiB3aW5uZXJzXHJcbiAgICBwcml2YXRlIHByb2Nlc3NXaW5uZXJzVXBkYXRlKG1zZzogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IG1zZy5zcGxpdChcIjtcIik7XHJcbiAgICAgICAgbGV0IHdpbm5lcklEcyA9IG5ldyBBcnJheTxudW1iZXI+KCk7XHJcbiAgICAgICAgZm9yKGxldCBpbmRleCA9IDE7IGluZGV4IDwgZGF0YS5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICAgICAgY29uc3QgcGxheWVySUQgPSBwYXJzZUludChkYXRhW2luZGV4XSwgMTApO1xyXG4gICAgICAgICAgICB3aW5uZXJJRHMucHVzaChwbGF5ZXJJRClcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uR2FtZU92ZXIod2lubmVySURzKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBQcm9jZXNzIHVwZGF0ZSB0byB0aGUgbnVjbGVhciBzdG9ybVxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzU3Rvcm1VcGRhdGUobXNnOiBzdHJpbmcpIHtcclxuICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5kaXNzZWN0TXVsdGlQYXJ0TXNnKG1zZywgXCJ1cGRhdGU6c3Rvcm07XCIpO1xyXG4gICAgICAgIGlmKGRhdGEpIHtcclxuICAgICAgICAgICAgaWYoZGF0YS5hcHByb2FjaCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uU3Rvcm1BcHByb2FjaFVwZGF0ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmKGRhdGEucmFkaXVzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByYWRpdXMgPSBwYXJzZUludChkYXRhLnJhZGl1cywgMTApO1xyXG4gICAgICAgICAgICAgICAgaWYocmFkaXVzICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25TdG9ybVJhZGl1c1VwZGF0ZShyYWRpdXMpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmKGRhdGEuZG1nICYmIGRhdGEuc2lja0RtZykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZG1nUGVyVHVybiA9IHBhcnNlSW50KGRhdGEuZG1nLCAxMCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzaWNrRG1nUGVyVHVybiA9IHBhcnNlSW50KGRhdGEuc2lja2RtZywgMTApO1xyXG4gICAgICAgICAgICAgICAgaWYoZG1nUGVyVHVybiAhPSBudWxsICYmIHNpY2tEbWdQZXJUdXJuICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25TdG9ybURtZ1VwZGF0ZShkbWdQZXJUdXJuLCBzaWNrRG1nUGVyVHVybik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUHJvY2VzcyBhbiBpbmRpdmlkdWFsIHBsYXllcidzIGRldGFpbHMgdXBkYXRlXHJcbiAgICBwcml2YXRlIHByb2Nlc3NQbGF5ZXJEZXRhaWxzVXBkYXRlKGVsZW06IHN0cmluZyk6IHsgaWQ6IHN0cmluZywgZGV0YWlsczogUGxheWVyRGV0YWlscyB9IHtcclxuICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5kaXNzZWN0TXVsdGlQYXJ0TXNnKGVsZW0sIG51bGwsIFwiLFwiKTtcclxuICAgICAgICBpZihkYXRhKSB7XHJcbiAgICAgICAgICAgIGlmKGRhdGEuaWQgJiYgZGF0YS54ICYmIGRhdGEueSAmJiBkYXRhLmMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHggPSBwYXJzZUludChkYXRhLngsIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHkgPSBwYXJzZUludChkYXRhLnksIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNyb3VjaCA9IGRhdGEuY3JvdWNoID09IFwidHJ1ZVwiO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHsgaWQ6IGRhdGEuaWQsIGRldGFpbHM6IHsgeDogeCwgeTogeSwgY3JvdWNoOiBjcm91Y2ggfSB9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFByb2Nlc3MgYW4gdXBkYXRlIHRvIHRoZSBpdGVtcyBvbiB0aGUgY3VycmVudCB0aWxlXHJcbiAgICBwcml2YXRlIHByb2Nlc3NJdGVtc0ZvdW5kKGVsZW06IHN0cmluZyk6IHsgZW50aXR5SUQ6IHN0cmluZywgaXRlbUlEOiBzdHJpbmcgfSB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuZGlzc2VjdE11bHRpUGFydE1zZyhlbGVtLCBudWxsLCBcIixcIik7XHJcbiAgICAgICAgaWYoZGF0YSkge1xyXG4gICAgICAgICAgICBpZihkYXRhLmVudGl0eUlEICYmIGRhdGEuaXRlbUlEKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImZvdW5kIGl0ZW1cIiwgZGF0YS5lbnRpdHlJRCwgZGF0YS5pdGVtSUQpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHsgZW50aXR5SUQ6IGRhdGEuZW50aXR5SUQsIGl0ZW1JRDogZGF0YS5pdGVtSUQgfTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBQcm9jZXNzIGFuIHVwZGF0ZSB0byBteSBwbGF5ZXIncyBtb2RpZmllcnMgbGlzdFxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzTXlQbGF5ZXJNb2RpZmllcnNVcGRhdGUoZWxlbTogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuZGlzc2VjdE11bHRpUGFydE1zZyhlbGVtLCBudWxsLCBcIixcIik7XHJcbiAgICAgICAgaWYoZGF0YSkge1xyXG4gICAgICAgICAgICBpZihkYXRhLnZpZXdSYW5nZSAmJiBkYXRhLmhpdENoYW5jZSAmJiBkYXRhLnZpZXdSYW5nZURlc2MhPW51bGwgJiYgZGF0YS5oaXRDaGFuY2VEZXNjIT1udWxsKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB2aWV3UmFuZ2UgPSBwYXJzZUludChkYXRhLnZpZXdSYW5nZSwgMTApO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaGl0Q2hhbmNlID0gcGFyc2VJbnQoZGF0YS5oaXRDaGFuY2UsIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHRvdGFsQVAgPSBwYXJzZUludChkYXRhLnRvdGFsQVAsIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGFwTW92ZUNvc3QgPSBwYXJzZUludChkYXRhLmFwTW92ZUNvc3QsIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRtZ1Blck1vdmUgPSBwYXJzZUludChkYXRhLmRtZ1Blck1vdmUsIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNpY2tEbWdQZXJUdXJuID0gcGFyc2VJbnQoZGF0YS5zaWNrRG1nUGVyVHVybiwgMTApO1xyXG4gICAgICAgICAgICAgICAgbGV0IHZpZXdSYW5nZURlc2MgPSBuZXcgQXJyYXk8c3RyaW5nPigpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGhpdENoYW5jZURlc2MgPSBuZXcgQXJyYXk8c3RyaW5nPigpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHRvdGFsQVBEZXNjID0gbmV3IEFycmF5PHN0cmluZz4oKTtcclxuICAgICAgICAgICAgICAgIGxldCBhcE1vdmVDb3N0RGVzYyA9IG5ldyBBcnJheTxzdHJpbmc+KCk7XHJcbiAgICAgICAgICAgICAgICBsZXQgZG1nUGVyTW92ZURlc2MgPSBuZXcgQXJyYXk8c3RyaW5nPigpO1xyXG4gICAgICAgICAgICAgICAgbGV0IHNpY2tEbWdQZXJUdXJuRGVzYyA9IG5ldyBBcnJheTxzdHJpbmc+KCk7XHJcbiAgICAgICAgICAgICAgICAvLyBnZXQgdGhlIHJlYXNvbnMgZm9yIHRoZSB2aWV3IHJhbmdlIG1vZGlmaWVyXHJcbiAgICAgICAgICAgICAgICBmb3IobGV0IGRlc2Mgb2YgZGF0YS52aWV3UmFuZ2VEZXNjLnNwbGl0KFwiflwiKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGRlc2MpICAgIHZpZXdSYW5nZURlc2MucHVzaChkZXNjKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIGdldCB0aGUgcmVhc29ucyBmb3IgdGhlIGhpdCBjaGFuY2UgbW9kaWZpZXJcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgZGVzYyBvZiBkYXRhLmhpdENoYW5jZURlc2Muc3BsaXQoXCJ+XCIpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYoZGVzYykgICAgaGl0Q2hhbmNlRGVzYy5wdXNoKGRlc2MpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gZ2V0IHRoZSByZWFzb25zIGZvciB0aGUgdG90YWwgYXAgbW9kaWZpZXJcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgZGVzYyBvZiBkYXRhLnRvdGFsQVBEZXNjLnNwbGl0KFwiflwiKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGRlc2MpICAgIHRvdGFsQVBEZXNjLnB1c2goZGVzYyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvLyBnZXQgdGhlIHJlYXNvbnMgZm9yIHRoZSBhcCBtb3ZlIGNvc3QgbW9kaWZpZXJcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgZGVzYyBvZiBkYXRhLmFwTW92ZUNvc3REZXNjLnNwbGl0KFwiflwiKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGRlc2MpICAgIGFwTW92ZUNvc3REZXNjLnB1c2goZGVzYyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvLyBnZXQgdGhlIHJlYXNvbnMgZm9yIHRoZSBkbWcgcGVyIG1vdmUgbW9kaWZpZXJcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgZGVzYyBvZiBkYXRhLmRtZ1Blck1vdmVEZXNjLnNwbGl0KFwiflwiKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmKGRlc2MpICAgIGRtZ1Blck1vdmVEZXNjLnB1c2goZGVzYyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvLyBnZXQgdGhlIHJlYXNvbnMgZm9yIHRoZSBzaWNrbmVzcyBkbWcgcGVyIHR1cm4gbW9kaWZpZXJcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgZGVzYyBvZiBkYXRhLnNpY2tEbWdQZXJUdXJuRGVzYy5zcGxpdChcIn5cIikpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZihkZXNjKSAgICBzaWNrRG1nUGVyVHVybkRlc2MucHVzaChkZXNjKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIHVwZGF0ZSBteSBwbGF5ZXJzIG1vZGlmaWVycyB3aXRoIHRoZSBuZXcgaW5mb3JtYXRpb25cclxuICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5teVBsYXllck1vZGlmaWVycyA9IHtcclxuICAgICAgICAgICAgICAgICAgICB2aWV3UmFuZ2U6IHZpZXdSYW5nZSxcclxuICAgICAgICAgICAgICAgICAgICBoaXRDaGFuY2U6IGhpdENoYW5jZSxcclxuICAgICAgICAgICAgICAgICAgICB0b3RhbEFQOiB0b3RhbEFQLFxyXG4gICAgICAgICAgICAgICAgICAgIGFwTW92ZUNvc3Q6IGFwTW92ZUNvc3QsXHJcbiAgICAgICAgICAgICAgICAgICAgZG1nUGVyTW92ZTogZG1nUGVyTW92ZSxcclxuICAgICAgICAgICAgICAgICAgICBzaWNrRG1nUGVyVHVybjogc2lja0RtZ1BlclR1cm4sXHJcbiAgICAgICAgICAgICAgICAgICAgdmlld1JhbmdlRGVzYzogdmlld1JhbmdlRGVzYyxcclxuICAgICAgICAgICAgICAgICAgICBoaXRDaGFuY2VEZXNjOiBoaXRDaGFuY2VEZXNjLFxyXG4gICAgICAgICAgICAgICAgICAgIHRvdGFsQVBEZXNjOiB0b3RhbEFQRGVzYyxcclxuICAgICAgICAgICAgICAgICAgICBhcE1vdmVDb3N0RGVzYzogYXBNb3ZlQ29zdERlc2MsXHJcbiAgICAgICAgICAgICAgICAgICAgZG1nUGVyTW92ZURlc2M6IGRtZ1Blck1vdmVEZXNjLFxyXG4gICAgICAgICAgICAgICAgICAgIHNpY2tEbWdQZXJUdXJuRGVzYzogc2lja0RtZ1BlclR1cm5EZXNjXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIFByb2Nlc3MgYW4gdXBkYXRlIHRvIG15IHBsYXllcidzIGhlYWx0aFxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzUGxheWVySFBVcGRhdGUoZWxlbTogc3RyaW5nKTogeyBpZDogc3RyaW5nLCBocDogUGxheWVySGVhbHRoIH0ge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmRpc3NlY3RNdWx0aVBhcnRNc2coZWxlbSwgbnVsbCwgXCIsXCIpO1xyXG4gICAgICAgIGlmKGRhdGEpIHtcclxuICAgICAgICAgICAgaWYoZGF0YS5pZCAmJiBkYXRhLnRvdGFsICYmIGRhdGEuaGVhZCAmJiBkYXRhLmNoZXN0ICYmIGRhdGEucmFybSAmJiBkYXRhLmxhcm0gJiYgZGF0YS5ybGVnICYmIGRhdGEubGxlZykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGxheWVySUQgPSBkYXRhLmlkO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdG90YWwgPSBwYXJzZUludChkYXRhLnRvdGFsLCAxMCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBoZWFkID0gcGFyc2VJbnQoZGF0YS5oZWFkLCAxMCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjaGVzdCA9IHBhcnNlSW50KGRhdGEuY2hlc3QsIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJhcm0gPSBwYXJzZUludChkYXRhLnJhcm0sIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGxhcm0gPSBwYXJzZUludChkYXRhLmxhcm0sIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJsZWcgPSBwYXJzZUludChkYXRhLnJsZWcsIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGxsZWcgPSBwYXJzZUludChkYXRhLmxsZWcsIDEwKTtcclxuICAgICAgICAgICAgICAgIC8vIHVwZGF0ZSB0aGUgcGxheWVycyBocCB3aXRoIHRoZSBuZXcgaW5mb3JtYXRpb25cclxuICAgICAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6IHBsYXllcklELFxyXG4gICAgICAgICAgICAgICAgICAgIGhwOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsOiB0b3RhbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGVhZDogaGVhZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2hlc3Q6IGNoZXN0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByYXJtOiByYXJtLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXJtOiBsYXJtLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBybGVnOiBybGVnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsbGVnOiBsbGVnXHJcbiAgICAgICAgICAgICAgICAgICAgfSBhcyBQbGF5ZXJIZWFsdGhcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUHJvY2VzcyBhbiB1cGRhdGUgdG8gdGhlIHBsYXllciBsaXN0XHJcbiAgICBwcml2YXRlIHByb2Nlc3NQbGF5ZXJzVXBkYXRlKCkge1xyXG4gICAgICAgIGZldGNoUGxheWVyTGlzdChDb29raWVzLmdldChcImxvYmJ5X25hbWVcIiksICh4bWxIdHRwKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIG9uIHN1Y2Nlc3NcclxuICAgICAgICAgICAgY29uc3QgcCA9IEpTT04ucGFyc2UoeG1sSHR0cC5yZXNwb25zZVRleHQpO1xyXG4gICAgICAgICAgICBpZihwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIucGxheWVyU3R1YnMgPSBwO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgKHhtbEh0dHApID0+IHtcclxuICAgICAgICAgICAgLy8gb24gZXJyb3JcclxuICAgICAgICAgICAgY29uc29sZS5sb2coeG1sSHR0cC5yZXNwb25zZVRleHQpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFByb2Nlc3MgYSB2YWxpZCB0YWtlIGl0ZW0gcmVxdWVzdFxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzVGFrZUl0ZW1WYWxpZChtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmRpc3NlY3RNdWx0aVBhcnRNc2cobXNnLCBcInRha2VpdGVtOnZhbGlkO1wiKTtcclxuICAgICAgICBpZihkYXRhKSB7XHJcbiAgICAgICAgICAgIC8vIG1lc3NhZ2UgbXVzdCBjb250YWluIGVudGl0eSBpZCBhbmQgaXRlbSBpZCB0byBiZSB1c2FibGUgLi4uXHJcbiAgICAgICAgICAgIGlmKGRhdGEuZW50aXR5SUQgJiYgZGF0YS5pdGVtSUQgJiYgZGF0YS5maXJzdFNsb3RYICYmIGRhdGEuZmlyc3RTbG90WSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZW50aXR5SUQgPSBwYXJzZUludChkYXRhLmVudGl0eUlELCAxMCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBpdGVtSUQgPSBwYXJzZUludChkYXRhLml0ZW1JRCwgMTApO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZmlyc3RTbG90WCA9IHBhcnNlSW50KGRhdGEuZmlyc3RTbG90WCwgMTApO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZmlyc3RTbG90WSA9IHBhcnNlSW50KGRhdGEuZmlyc3RTbG90WSwgMTApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uVGFrZUl0ZW1WYWxpZChlbnRpdHlJRCwgaXRlbUlELCBmaXJzdFNsb3RYLCBmaXJzdFNsb3RZKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBQcm9jZXNzIGEgdmFsaWQgZXF1aXAgaXRlbSByZXF1ZXN0XHJcbiAgICBwcml2YXRlIHByb2Nlc3NFcXVpcEl0ZW1WYWxpZChtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmRpc3NlY3RNdWx0aVBhcnRNc2cobXNnLCBcImVxdWlwaXRlbTp2YWxpZDtcIik7XHJcbiAgICAgICAgaWYoZGF0YSkge1xyXG4gICAgICAgICAgICAvLyBtZXNzYWdlIG11c3QgY29udGFpbiBlbnRpdHkgaWQgYW5kIGl0ZW0gaWQgdG8gYmUgdXNhYmxlXHJcbiAgICAgICAgICAgIGlmKGRhdGEuZW50aXR5SUQgJiYgZGF0YS5pdGVtSUQpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGVudGl0eUlEID0gcGFyc2VJbnQoZGF0YS5lbnRpdHlJRCwgMTApO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaXRlbUlEID0gcGFyc2VJbnQoZGF0YS5pdGVtSUQsIDEwKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5vbkVxdWlwSXRlbVZhbGlkKGVudGl0eUlELCBpdGVtSUQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIFByb2Nlc3MgYSB2YWxpZCBkcm9wIGl0ZW0gcmVxdWVzdFxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzRHJvcEl0ZW1WYWxpZChtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmRpc3NlY3RNdWx0aVBhcnRNc2cobXNnLCBcImRyb3BpdGVtOnZhbGlkO1wiKTtcclxuICAgICAgICBpZihkYXRhKSB7XHJcbiAgICAgICAgICAgIC8vIG1lc3NhZ2UgbXVzdCBjb250YWluIGVudGl0eSBpZCBhbmQgaXRlbSBpZCB0byBiZSB1c2FibGVcclxuICAgICAgICAgICAgaWYoZGF0YS5lbnRpdHlJRCAmJiBkYXRhLml0ZW1JRCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZW50aXR5SUQgPSBwYXJzZUludChkYXRhLmVudGl0eUlELCAxMCk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBpdGVtSUQgPSBwYXJzZUludChkYXRhLml0ZW1JRCwgMTApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uRHJvcEl0ZW1WYWxpZChlbnRpdHlJRCwgaXRlbUlEKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvLyBQcm9jZXNzIGEgdmFsaWQgdXNlIGJhbmRhZ2UgcmVxdWVzdFxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzVXNlQmFuZGFnZVZhbGlkKG1zZzogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuZGlzc2VjdE11bHRpUGFydE1zZyhtc2csIFwidXNlYmFuZGFnZTp2YWxpZDtcIik7XHJcbiAgICAgICAgaWYoZGF0YSkge1xyXG4gICAgICAgICAgICBpZihkYXRhLmVudGl0eUlEICYmIGRhdGEuaXRlbUlEKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBlbnRpdHlJRCA9IHBhcnNlSW50KGRhdGEuZW50aXR5SUQsIDEwKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGl0ZW1JRCA9IHBhcnNlSW50KGRhdGEuaXRlbUlELCAxMCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25Vc2VCYW5kYWdlVmFsaWQoZW50aXR5SUQsIGl0ZW1JRCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gRGlzc2VjdCBhIHJlYWR5IHN0YXRlIHVwZGF0ZSBtZXNzYWdlIGFuZCBtYWtlIHJlbGV2YW50IEdVSSBjaGFuZ2VzXHJcbiAgICBwcml2YXRlIHByb2Nlc3NSZWFkeVN0YXRlVXBkYXRlKG1zZzogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHRoaXMuZGlzc2VjdE11bHRpUGFydE1zZyhtc2csIFwidXBkYXRlOnJlYWR5O1wiKTtcclxuICAgICAgICBpZihkYXRhKSB7XHJcbiAgICAgICAgICAgIC8vIG1lc3NhZ2UgbXVzdCBjb250YWluIHBsYXllciBhbmQgc3RhdGUgdG8gYmUgdXNhYmxlXHJcbiAgICAgICAgICAgIGlmKGRhdGEucGxheWVyICYmIGRhdGEucmVhZHkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udHJvbGxlci5vblJlYWR5U3RhdGVVcGRhdGUoZGF0YS5wbGF5ZXIsIGRhdGEucmVhZHkgPT0gXCJ0cnVlXCIgPyB0cnVlIDogZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGEgcGhhc2UgdGltZSB1cGRhdGUgaXMgcmVjZWl2ZWQgZnJvbSB0aGUgc2VydmVyXHJcbiAgICBwcml2YXRlIHByb2Nlc3NQaGFzZVRpbWVVcGRhdGUobXNnOiBzdHJpbmcpIHtcclxuICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5kaXNzZWN0TXVsdGlQYXJ0TXNnKG1zZywgXCJwaGFzZXRpbWU7XCIpO1xyXG4gICAgICAgIGlmKGRhdGEpIHtcclxuICAgICAgICAgICAgLy8gbWVzc2FnZSBtdXN0IGNvbnRhaW4gZHJvcHRpbWUsIG1vdmV0aW1lIGFuZCBhY3Rpb250aW1lIHRvIGJlIHVzYWJsZVxyXG4gICAgICAgICAgICBpZihkYXRhLmRyb3B0aW1lICYmIGRhdGEubW92ZXRpbWUgJiYgZGF0YS5hY3Rpb250aW1lKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25QaGFzZVRpbWVzVXBkYXRlKGRhdGEuZHJvcHRpbWUsIGRhdGEubW92ZXRpbWUsIGRhdGEuYWN0aW9udGltZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIG5hbWUgb2YgdGhlIG5leHQgbWFwIHRvIGJlIHBsYXllZCBpcyByZWNlaXZlZCBmcm9tIHRoZSBzZXJ2ZXJcclxuICAgIHByaXZhdGUgcHJvY2Vzc01hcFVwZGF0ZShtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmRpc3NlY3RNdWx0aVBhcnRNc2cobXNnLCBcIm1hcDtcIik7XHJcblxyXG4gICAgICAgIC8vIG1lc3NhZ2UgbXVzdCBjb250YWluIG1hcE5hbWUgdG8gYmUgdXNhYmxlXHJcbiAgICAgICAgaWYoZGF0YS5uYW1lKSB7XHJcbiAgICAgICAgICAgIC8vIGZldGNoIHRoZSBtYXAgZnJvbSB0aGUgc2VydmVyXHJcbiAgICAgICAgICAgIGZldGNoTWFwKGRhdGEubmFtZSwgKHhtbEh0dHApID0+IHtcclxuICAgICAgICAgICAgICAgIC8vIG9uIHN1Y2Nlc3NcclxuICAgICAgICAgICAgICAgIGNvbnN0IG0gPSBuZXcgTWFwKHRoaXMuY29udHJvbGxlciwgeG1sSHR0cC5yZXNwb25zZVRleHQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm1hcCA9IG07XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25NYXBMb2FkZWQoKTtcclxuICAgICAgICAgICAgfSwgKHhtbEh0dHApID0+IHtcclxuICAgICAgICAgICAgICAgIC8vIG9uIGVycm9yXHJcbiAgICAgICAgICAgICAgICBhbGVydChcIkZhdGFsIEVycm9yOiBGYWlsZWQgdG8gbG9hZCBtYXAgXCIgKyBkYXRhLm1hcE5hbWUpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHdoZW4gdGhlIGl0ZW0gbGlzdCBmb3IgdGhpcyBnYW1lIGlzIHJlY2VpdmVkIGZyb20gdGhlIHNlcnZlclxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzSXRlbXNMaXN0KG1zZzogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IG1zZy5zcGxpdChcIml0ZW1zO1wiKS5sZW5ndGggPiAxID8gbXNnLnNwbGl0KFwiaXRlbXM7XCIpWzFdIDogbnVsbDtcclxuICAgICAgICBpZihkYXRhKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNwbGl0cyA9IGRhdGEuc3BsaXQoXCI7XCIpO1xyXG4gICAgICAgICAgICBsZXQgaXRlbXMgPSBuZXcgQXJyYXk8SXRlbT4oKTtcclxuXHJcbiAgICAgICAgICAgIC8vIGV4dHJhY3QgcmVsZXZhbnQgaW5mb3JtYXRpb24gZnJvbSBkYXRhIHN0cmluZ1xyXG4gICAgICAgICAgICBmb3IoY29uc3QgZWxlbSBvZiBzcGxpdHMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGl0ZW1PYmogPSB0aGlzLmRpc3NlY3RNdWx0aVBhcnRNc2coZWxlbSwgbnVsbCwgXCIsXCIpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGl0ZW07XHJcbiAgICAgICAgICAgICAgICBpZihpdGVtT2JqLnNob3RzUGVyQWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogaXRlbU9iai5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbWFnZTogaXRlbU9iai5pbWFnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZ3JpZFdpZHRoOiBwYXJzZUludChpdGVtT2JqLmdyaWR3LCAxMCkgfHwgMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZ3JpZEhlaWdodDogcGFyc2VJbnQoaXRlbU9iai5ncmlkaCwgMTApIHx8IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNob3RzUGVyQWN0aW9uOiBwYXJzZUZsb2F0KGl0ZW1PYmouc2hvdHNQZXJBY3Rpb24pIHx8IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRtZ1BlckhpdDogcGFyc2VGbG9hdChpdGVtT2JqLnNob3RzUGVyQWN0aW9uKSB8fCAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhSYW5nZTogcGFyc2VGbG9hdChpdGVtT2JqLm1heFJhbmdlKSB8fCAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY2N1cmFjeTogcGFyc2VGbG9hdChpdGVtT2JqLmFjY3VyYWN5KSB8fCAwXHJcbiAgICAgICAgICAgICAgICAgICAgfSBhcyBHdW47XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYoaXRlbU9iai50aW1lVG9UaHJvdykge1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IGl0ZW1PYmoubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2U6IGl0ZW1PYmouaW1hZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdyaWRXaWR0aDogcGFyc2VJbnQoaXRlbU9iai5ncmlkdywgMTApIHx8IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdyaWRIZWlnaHQ6IHBhcnNlSW50KGl0ZW1PYmouZ3JpZGgsIDEwKSB8fCAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aW1lVG9UaHJvdzogcGFyc2VGbG9hdChpdGVtT2JqLnRpbWVUb1Rocm93KSB8fCAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkbWdQZXJIaXQ6IHBhcnNlRmxvYXQoaXRlbU9iai5kbWdQZXJIaXQpIHx8IDBcclxuICAgICAgICAgICAgICAgICAgICB9IGFzIFByb2plY3RpbGU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYoaXRlbU9iai5oZWFsQW1vdW50KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogaXRlbU9iai5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbWFnZTogaXRlbU9iai5pbWFnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZ3JpZFdpZHRoOiBwYXJzZUludChpdGVtT2JqLmdyaWR3LCAxMCkgfHwgMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZ3JpZEhlaWdodDogcGFyc2VJbnQoaXRlbU9iai5ncmlkaCwgMTApIHx8IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlYWxBbW91bnQ6IHBhcnNlSW50KGl0ZW1PYmouaGVhbEFtb3VudCwgMTApIHx8IDBcclxuICAgICAgICAgICAgICAgICAgICB9IGFzIEJhbmRhZ2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpdGVtcy5wdXNoKGl0ZW0pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25JdGVtc0xvYWRlZChpdGVtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIHRoZSBJRCBvZiB0aGUgcGxheWVyIHVzaW5nIHRoaXMgY2xpZW50IGlzIHJlY2VpdmVkIGZyb20gdGhlIHNlcnZlclxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzTXlJRFVwZGF0ZShtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmRpc3NlY3RNdWx0aVBhcnRNc2cobXNnLCBcInlvdXJpZDtcIik7XHJcbiAgICAgICAgaWYoZGF0YSkge1xyXG4gICAgICAgICAgICBpZihkYXRhLmlkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25NeUlEVXBkYXRlKGRhdGEuaWQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIENhbGxlZCB3aGVuIGEgcGxheWVyIGRpc2Nvbm5lY3RzIGZyb20gdGhlIGdhbWVcclxuICAgIHByaXZhdGUgcHJvY2Vzc0Rpc2Nvbm5lY3QobXNnOiBzdHJpbmcpIHtcclxuICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5kaXNzZWN0TXVsdGlQYXJ0TXNnKG1zZywgXCJkaXNjb25uZWN0O1wiKTtcclxuICAgICAgICBpZihkYXRhICYmIGRhdGEuaWQpIHtcclxuICAgICAgICAgICAgdGhpcy5jb250cm9sbGVyLm9uUGxheWVyRGlzY29ubmVjdChkYXRhLmlkKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2FsbGVkIHVwb24gcmVjZWl2aW5nIGEgbXNnIGZyb20gYW5vdGhlciBwbGF5ZXJcclxuICAgIHByaXZhdGUgcHJvY2Vzc1BsYXllck1zZyhtc2c6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLmRpc3NlY3RNdWx0aVBhcnRNc2cobXNnLCBcIm1lc3NhZ2U7XCIpO1xyXG4gICAgICAgIGlmKGRhdGEgJiYgZGF0YS5pZCAmJiBkYXRhLm1zZykge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRyb2xsZXIub25Nc2dSZWNlaXZlZChkYXRhLmlkLCBkYXRhLm1zZyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZGlzc2VjdE11bHRpUGFydE1zZyhtc2c6IHN0cmluZywgY3V0QXQ6IHN0cmluZywgc2VwOiBzdHJpbmc9XCI7XCIpOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9IHtcclxuICAgICAgICBjb25zdCBkYXRhID0gbXNnLnNwbGl0KGN1dEF0KS5sZW5ndGggPiAxID8gbXNnLnNwbGl0KGN1dEF0KVsxXSA6IG1zZztcclxuICAgICAgICBsZXQgbWFwOiB7IFtrZXk6IHN0cmluZ106IHN0cmluZyB9ID0ge307XHJcblxyXG4gICAgICAgIGlmKGRhdGEpIHtcclxuICAgICAgICAgICAgY29uc3Qgc3BsaXRzID0gZGF0YS5zcGxpdChzZXApO1xyXG5cclxuICAgICAgICAgICAgLy8gZXh0cmFjdCByZWxldmFudCBpbmZvcm1hdGlvbiBmcm9tIGRhdGEgc3RyaW5nXHJcbiAgICAgICAgICAgIGZvcihjb25zdCBlbGVtIG9mIHNwbGl0cykge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGFydHMgPSBlbGVtLnNwbGl0KFwiOlwiKTtcclxuICAgICAgICAgICAgICAgIGlmKHBhcnRzLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBrZXkgPSBwYXJ0c1swXTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHBhcnRzWzFdO1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcFtrZXldID0gdmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBtYXA7XHJcbiAgICB9XHJcblxyXG59XHJcbiIsImZ1bmN0aW9uIHNlbmRHZXRSZXF1ZXN0KFVSTCwgc3VjY2VzcywgZXJyb3IpIHtcclxuICAgIHZhciB4bWxIdHRwID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XHJcbiAgICB4bWxIdHRwLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIGlmKHhtbEh0dHAucmVhZHlTdGF0ZSA9PSA0KSB7XHJcbiAgICAgICAgICAgIGlmKHhtbEh0dHAuc3RhdHVzID49IDIwMCAmJiB4bWxIdHRwLnN0YXR1cyA8IDMwMCkge1xyXG4gICAgICAgICAgICAgICAgaWYodHlwZW9mKHN1Y2Nlc3MpID09PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzKHhtbEh0dHApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaWYodHlwZW9mKGVycm9yKSA9PT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXJyb3IoeG1sSHR0cCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy8gc2VuZCB0aGUgYWpheCByZXF1ZXN0XHJcbiAgICB4bWxIdHRwLm9wZW4oXCJHRVRcIiwgVVJMLCB0cnVlKTsgICAvLyB0cnVlIGZvciBhc3luY2hyb25vdXNcclxuICAgIHhtbEh0dHAuc2VuZChudWxsKTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGZldGNoTWFwKG1hcE5hbWU6IHN0cmluZywgc3VjY2VzczogKCh4OiBYTUxIdHRwUmVxdWVzdCkgPT4gdm9pZCksIGVycm9yOiAoKHg6IFhNTEh0dHBSZXF1ZXN0KSA9PiB2b2lkKSkge1xyXG4gICAgc2VuZEdldFJlcXVlc3QoYC9tYXBzLyR7bWFwTmFtZX0udHh0YCwgKHhtbEh0dHApID0+IHtcclxuICAgICAgICAvLyBvbiBzdWNjZXNzXHJcbiAgICAgICAgaWYoc3VjY2Vzcykge1xyXG4gICAgICAgICAgICBzdWNjZXNzKHhtbEh0dHApO1xyXG4gICAgICAgIH1cclxuICAgIH0sICh4bWxIdHRwKSA9PiB7XHJcbiAgICAgICAgLy8gb24gZXJyb3JcclxuICAgICAgICBpZihlcnJvcikge1xyXG4gICAgICAgICAgICBlcnJvcih4bWxIdHRwKTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGZldGNoUGxheWVyTGlzdChsb2JieU5hbWU6IHN0cmluZywgc3VjY2VzczogKCh4OiBYTUxIdHRwUmVxdWVzdCkgPT4gdm9pZCksIGVycm9yOiAoKHg6IFhNTEh0dHBSZXF1ZXN0KSA9PiB2b2lkKSkge1xyXG4gICAgc2VuZEdldFJlcXVlc3QoYC9wbGF5ZXJzP2xvYmJ5X25hbWU9JHtsb2JieU5hbWV9YCwgKHhtbEh0dHApID0+IHtcclxuICAgICAgICAvLyBvbiBzdWNjZXNzXHJcbiAgICAgICAgaWYoc3VjY2Vzcykge1xyXG4gICAgICAgICAgICBzdWNjZXNzKHhtbEh0dHApO1xyXG4gICAgICAgIH1cclxuICAgIH0sICh4bWxIdHRwKSA9PiB7XHJcbiAgICAgICAgLy8gb24gZXJyb3JcclxuICAgICAgICBpZihlcnJvcikge1xyXG4gICAgICAgICAgICBlcnJvcih4bWxIdHRwKTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9