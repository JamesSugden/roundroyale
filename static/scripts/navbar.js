function main() {
    document.getElementById("dropdown-navbar-btn").addEventListener("click", onDropdownNavbarBtnClick);
}

function onDropdownNavbarBtnClick(e) {
    if(document.getElementById("dropdown-navbar-btn").classList.contains("active")) {
        document.getElementById("dropdown-navbar-btn").classList.remove("active");
    } else {
        document.getElementById("dropdown-navbar-btn").classList.add("active");
    }
}

window.addEventListener("load", main);
