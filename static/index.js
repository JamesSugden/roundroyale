(function() {
    // send an ajax request to the server to create a new lobby
    // lobby will be public iff !password
    function sendCreateLobbyRequest(name, password) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4) {
                if(xmlHttp.status >= 200 && xmlHttp.status < 300) {
                    // status ok
                    Cookies.set("player_token", xmlHttp.responseText);
                    Cookies.set("lobby_name", name);
                    Cookies.set("player_name", name);
                    document.getElementById("ajax-error-lbl").style.display = "none";
                    window.location.href = "game.html";
                } else {
                    console.log("error", xmlHttp.responseText);
                    // error occured
                    document.getElementById("ajax-error-lbl").innerHTML = xmlHttp.responseText;
                    document.getElementById("ajax-error-lbl").style.display = "block";
                }
            }
        }

        // send the ajax request
        if(password) {
            xmlHttp.open("GET", `/games/create?player_name=${name}&password=${password}`, true);   // true for asynchronous
        } else {
            xmlHttp.open("GET", `/games/create?player_name=${name}`, true);   // true for asynchronous
        }
        xmlHttp.send(null);
    }

    // send an ajax request to the server to join a lobby
    function sendJoinLobbyRequest(lobbyName, playerName, password) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4) {
                if(xmlHttp.status >= 200 && xmlHttp.status < 300) {
                    // status ok
                    Cookies.set("player_token", xmlHttp.responseText);
                    Cookies.set("lobby_name", lobbyName);
                    Cookies.set("player_name", playerName);
                    document.getElementById("ajax-error-lbl").style.display = "none";
                    window.location.href = "game.html";
                } else {
                    console.log("error", xmlHttp.responseText)
                    // show error by password field if it is empty & if unauthorized
                    if(!password && xmlHttp.status === 401) {
                        document.getElementById("password-error-lbl").innerHTML = "Required Field";
                        document.getElementById("password-error-lbl").style.display = "block";
                    } else {
                        // error occured
                        document.getElementById("ajax-error-lbl").innerHTML = xmlHttp.responseText;
                        document.getElementById("ajax-error-lbl").style.display = "block";
                    }
                }
            }
        }

//        document.getElementById("password-error-lbl").style.display = "none";

        // send the ajax request
        if(password) {
            xmlHttp.open("GET", `/games/join?lobby_name=${lobbyName}&player_name=${playerName}&password=${password}`, true);   // true for asynchronous
        } else {
            xmlHttp.open("GET", `/games/join?lobby_name=${lobbyName}&player_name=${playerName}`, true);   // true for asynchronous
        }
        xmlHttp.send(null);
    }

    // verify inputs before submitting ajax request
    function createLobbyBtnOnClick() {
        var name = document.getElementById("name-input").value;// name_input.value;
        var error = false;

        if(!name) {
            document.getElementById("name-error-lbl").innerHTML = "Required Field";
            document.getElementById("name-error-lbl").style.display = "block";
            error = true;
        } else {
            document.getElementById("name-error-lbl").style.display = "none";
        }

        /*var isPasswordProtected = document.getElementById("is-password-protected-input").checked;// is_password_protected_input.checked;
        var password = document.getElementById("password-input").value;// password_input.value;

        if(isPasswordProtected && !password) {
            document.getElementById("password-error-lbl").innerHTML = "Required Field";
            document.getElementById("password-error-lbl").style.display = "block";
            error = true;
        } else {
            document.getElementById("password-error-lbl").style.display = "none";
        }*/
        var password;

        // if there were no errors, make the ajax request
        if(!error) {
            console.log("sending create lobby request");
            sendCreateLobbyRequest(name, password);
        }
    };

    function fetchLobbyList() {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4) {
                if(xmlHttp.status >= 200 && xmlHttp.status < 300) {
                    // status ok
                    onLobbyListUpdated(JSON.parse(xmlHttp.responseText));
                }
            }
        }

        xmlHttp.open("GET", "/games", true);   // true for asynchronous
        xmlHttp.send(null);
    }

    function onLobbyListUpdated(lobbyList) {
        console.log(lobbyList);
        // Clear each list
        clearElement(document.getElementById("public-lobby-list"));
        clearElement(document.getElementById("private-lobby-list"));
        clearElement(document.getElementById("inprogress-lobby-list"));

        // Add new lobbies to list
        for(var lobbyName in lobbyList) {
            var lobby = lobbyList[lobbyName];

            if(lobby.Started) {
                // Add to in progress list
                addToLobbiesList("inprogress", lobbyName, lobby);
            } else if(lobby.Private) {
                // Add to private lobbies list
                addToLobbiesList("private", lobbyName, lobby);
            } else {
                // Add to public lobbies list
                addToLobbiesList("public", lobbyName, lobby);
            }
        }

        /* Only display the lists if they contain lobbies */
        document.getElementById("public-lobby-container").style.display =
            document.getElementById("public-lobby-list").firstChild ? "block" : "none";
        document.getElementById("private-lobby-container").style.display =
            document.getElementById("private-lobby-list").firstChild ? "block" : "none";
        document.getElementById("inprogress-lobby-container").style.display =
            document.getElementById("inprogress-lobby-list").firstChild ? "block" : "none";

        if(document.getElementById("public-lobby-container").style.display === "none" &&
            document.getElementById("private-lobby-container").style.display === "none" &&
            document.getElementById("inprogress-lobby-container").style.display === "none") {
            // No lobbies available therefore display no lobbies div
            document.getElementById("no-lobbies-div").style.display = "block";
        } else {
            // Lobbies are available therefore hide no lobbies div
            document.getElementById("no-lobbies-div").style.display = "none";
        }
    }

    function addToLobbiesList(whichList, lobbyName, lobby) {
        var list = document.getElementById(whichList + "-lobby-list");
        if(list) {
            var btn = document.createElement("a");
            var div = document.createElement("div");
            btn.classList.add("lobby-btn");
            div.classList.add("lobby-element");

            var nameLbl = document.createElement("label");
            nameLbl.innerHTML = lobbyName;
            nameLbl.classList.add("noselect");

            var numPlayersLbl = document.createElement("label");
            numPlayersLbl.innerHTML = ""+lobby.NumPlayers;
            numPlayersLbl.classList.add("noselect");

            btn.addEventListener("click", function(e) {
                onJoinLobbyBtnClick(lobbyName, lobby, div);
            });

            div.appendChild(nameLbl);
            div.appendChild(numPlayersLbl);
            btn.appendChild(div);
            list.appendChild(btn);
        }
    }

    function clearElement(element) {
        while(element.firstChild) {
            element.removeChild(element.firstChild);
        }
    }

    function onJoinLobbyBtnClick(lobbyName, lobby, div) {
        if(!lobby.Started) {
            if(!lobby.Private) {
                var playerName = document.getElementById("guest-name-input").value;
                console.log("Using Player Name", playerName);
                if(playerName) {
                    console.log("sending join lobby request")
                    sendJoinLobbyRequest(lobbyName, playerName, null);
                } else {
                    console.log("Error: No player name entered");
                    // add an error lbl below the lobby button
                    if(document.getElementById("lobby-error-lbl")) {
                        document.getElementById("lobby-error-lbl").remove();
                    }
                    var errorLbl = document.createElement("label");
                    errorLbl.innerHTML = "No player name entered";
                    errorLbl.classList.add("error-lbl");
                    errorLbl.id = "lobby-error-lbl";
                    errorLbl.style.display = "block";
                    div.parentNode.insertBefore(errorLbl, div.nextSibling);
                }
            }
        }
    }

    function genRandString(length) {
        // Method described here: https://stackoverflow.com/questions/1349404/generate-random-string-characters-in-javascript
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for(var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }

    window.onload = () => {
        fetchLobbyList();

        // Intialise player with a guest name
        // TODO - Only do this if not logged in
        document.getElementById("guest-name-input").value = "GUEST" + genRandString(5);

        document.getElementById("create-lobby-a").onclick = (e) => {
            if(document.getElementById("create-lobby-a").classList.contains("active")) {
                document.getElementById("create-lobby-a").classList.remove("active");
            } else {
                document.getElementById("create-lobby-a").classList.add("active");
            }
        };

        document.getElementById("create-lobby-btn").onclick = createLobbyBtnOnClick;
    };
})();
