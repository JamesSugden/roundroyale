(function() {
    var player_list;
    var toggle_ready_btn;
    var char_lbl;
    var title_lbl;
    var ctx;
    var canvas;
    var hitCanvas;
    var hitCtx;

    // server properties
    var socket;

    // game properties
    var dropPhaseTime = 15;             // default is 15
    var movePhaseTime = 10;             // default is 10
    var actionPhaseTime = 30;           // default is 30
    var started = false;
    var hexGrid;
    var hexSelectedX, hexSelectedY;
    var hexHoverX, hexHoverY;
    var hexHoverValid;
    var players = [];        // list of player stubs
    var playerDetails = {};  // map from player id to players game details
    var myID;

    // gui properties
    var zoomTargetX, zoomTargetY;
    var zoomLevel = 1.0;
    var totalMapWidth;
    var totalMapHeight;
    var lastMouseX, lastMouseY;

    var playerImage = new Image();
    playerImage.src = "assets/Stickman 1.png";

    var GamePhase = Object.freeze({
        DROP_PHASE: 0,
        MOVE_PHASE: 1,
        ACTION_PHASE: 2
    });

    var phase = GamePhase.DROP_PHASE;

    var TileType = Object.freeze({
        WATER: 0,
        DIRT: 1,
        GRASS: 2,
        FOREST: 3,
        HILL: 4,
        MOUNTAIN: 5,
        HOUSE: 6,
        HIGHRISE: 7,
        BRIDGE: 8
    });

    var TileColors = Object.freeze([
        "#40a4df",
        "#b69f66",
        "#63c900",
        "#63c900",
        "#63c900",
        "#808080",
        "#808080",
        "#808080",
        "#40a4df"
    ]);

    var TileImages = Object.freeze([
        null,
        null,
        null,
        new Image(),
        new Image(),
        new Image(),
        new Image(),
        new Image(),
        new Image()
    ]);

    // yuck
    TileImages[3].src = "assets/Tree 1.svg";
    TileImages[4].src = "assets/Hill 1.svg";
    TileImages[5].src = "assets/Mountain 1.svg";
    TileImages[6].src = "assets/House 1.svg";
    TileImages[7].src = "assets/Skyscraper 1.svg";
    TileImages[8].src = "assets/Bridge 1.svg";

    // map from color to tile (x, y)
    var tileColorMap = {};

    var hexHeight,
        hexRadius,
        hexRectangleHeight,
        hexRectangleWidth,
        hexagonAngle = 0.523598776, // 30 degrees in radians
        sideLength = 72;

    hexHeight = Math.sin(hexagonAngle) * sideLength;
    hexRadius = Math.cos(hexagonAngle) * sideLength;
    hexRectangleHeight = sideLength + 2 * hexHeight;
    hexRectangleWidth = 2 * hexRadius;

    function on_canvas_mouse_wheel(e) {
        var wheelDelta = e.wheelDelta ? e.wheelDelta : e.deltaY ? -e.deltaY : 0;
        var minZoomLevel = canvas.width / totalMapWidth;
        minZoomLevel = minZoomLevel >  canvas.height/totalMapHeight ? canvas.height/totalMapHeight : minZoomLevel;
        //console.log(JSON.stringify(e));
        var wheelDirection = wheelDelta < 0 ? -1 : wheelDelta > 0 ? 1 : 0;
        zoomLevel += wheelDirection * 0.05;
        zoomLevel = zoomLevel < minZoomLevel ? minZoomLevel : zoomLevel > 1.0 ? 1.0 : zoomLevel;
        //console.log(zoomLevel);
        if(wheelDirection < 0) {
            zoomTargetX = 0;
            zoomTargetY = 0;
        } else {
            zoomTargetX = 0;
            zoomTargetY = 0;
        }
        e.preventDefault();
        requestAnimationFrame(drawMap);
        return false;
    }

    function on_canvas_mouse_move(e) {
        var player = myPlayer();
        var hexagon = getHexagonUnderMouse(e);

        lastMouseX = e.clientX - canvas.getBoundingClientRect().left;
        lastMouseY = e.clientY - canvas.getBoundingClientRect().top;

        if(hexagon) {
            if(phase == GamePhase.DROP_PHASE) {
                hexHoverX = hexagon.x;
                hexHoverY = hexagon.y;
                var tileType = hexGrid[hexHoverY][hexHoverX].type;
                // Ensure the tile can be landed on
                if(!isTileImpassable(tileType) && !isTileUndroppable(tileType)) {
                    hexHoverValid = true;
                } else {
                    hexHoverValid = false;
                }
            } else if(phase == GamePhase.MOVE_PHASE && player) {
                var details = playerDetails[player.ID];
                if(details) {
                    var dx = details.x - hexagon.x;
                    var dy = details.y - hexagon.y;
                    hexHoverX = hexagon.x;
                    hexHoverY = hexagon.y;
                    var tileType = hexGrid[hexHoverY][hexHoverX].type;
                    if((((dy == -1 || dy == 1) && (details.y % 2 == 1) && dx >= -1 && dx <= 0) ||
                       ((dy == -1 || dy == 1) && (details.y % 2 == 0) && dx >= 0 && dx <= 1) ||
                       (dy == 0 && dx >= -1 && dx <= 1)) && !isTileImpassable(tileType)) {
                        hexHoverValid = true;
                    } else {
                        hexHoverValid = false;
                    }
                }
            } else if(phase == GamePhase.ACTION_PHASE && player) {
                // TODO
                hexHoverX = null;
                hexHoverY = null;
            }
            requestAnimationFrame(drawMap);
        }
    }

    function on_canvas_click(e) {
        var player = myPlayer();
        var hexagon = getHexagonUnderMouse(e);

        if(hexagon) {
            if(phase == GamePhase.DROP_PHASE) {
                // Ensure the tile can be landed on
                var tileType = hexGrid[hexagon.y][hexagon.x].type;
                if(!isTileImpassable(tileType) && !isTileUndroppable(tileType)) {
                    hexSelectedX = hexagon.x;
                    hexSelectedY = hexagon.y;
                    sendSetMoveTurnMessage();
                }
            } else if(phase == GamePhase.MOVE_PHASE && player) {
                var details = playerDetails[player.ID];
                if(details) {
                    var dx = details.x - hexagon.x;
                    var dy = details.y - hexagon.y;
                    var tileType = hexGrid[hexagon.y][hexagon.x].type;
                    if((((dy == -1 || dy == 1) && (details.y % 2 == 1) && dx >= -1 && dx <= 0) ||
                       ((dy == -1 || dy == 1) && (details.y % 2 == 0) && dx >= 0 && dx <= 1) ||
                       (dy == 0 && dx >= -1 && dx <= 1)) && !isTileImpassable(tileType)) {
                        hexSelectedX = hexagon.x;
                        hexSelectedY = hexagon.y;
                        sendSetMoveTurnMessage();
                    }
                }
            } else if(phase == GamePhase.ACTION_PHASE && player) {
                // TODO
            }
            requestAnimationFrame(drawMap);
        }
    }

    function getHexagonUnderMouse(e) {
        var mousex = e.clientX - hitCanvas.getBoundingClientRect().left;
        var mousey = e.clientY - hitCanvas.getBoundingClientRect().top;

        var pixel = hitCtx.getImageData(mousex, mousey, 1, 1).data;
        var color = `rgb(${pixel[0]},${pixel[1]},${pixel[2]})`;
        return tileColorMap[color];
    }

    function drawMap() {
        if(!hexGrid) { return; }

        ctx.fillStyle = TileColors[0];
        //ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        hitCtx.clearRect(0, 0, hitCanvas.width, hitCanvas.height);

        ctx.save();
        ctx.translate(zoomTargetX, zoomTargetY);
        ctx.scale(zoomLevel, zoomLevel);
        ctx.translate(-zoomTargetX, -zoomTargetY);
        //ctx.translate(0.5*canvas.width, 0.5*canvas.height);
        hitCtx.save();
        hitCtx.scale(zoomLevel, zoomLevel);
        //hitCtx.translate(0.5*hitCanvas.width, 0.5*hitCanvas.height);

        // get a reference to my player
        var player = myPlayer();
        var myDetails = playerDetails[player ? player.ID : -1];

        // draw the hex grid
        for(var j = 0; j < hexGrid.length; j++) {
            for(var i = 0; i < hexGrid[j].length; i++) {
                drawTile(i, j, true);
            }
        }

        // draw a shadow over the entire map
        ctx.globalAlpha = 0.6;
        ctx.fillStyle = "#000";
        ctx.fillRect(0, 0, canvas.width / zoomLevel, canvas.height / zoomLevel);

        // redraw the parts of the map in the players vision
        if(myDetails) {
            // draw tiles on same y value
            drawTile(myDetails.x, myDetails.y);
            drawTile(myDetails.x-1, myDetails.y);
            drawTile(myDetails.x+1, myDetails.y);
            drawTile(myDetails.x-2, myDetails.y);
            drawTile(myDetails.x+2, myDetails.y);

            if(myDetails.y % 2 == 0) {
                // draw tiles above
                drawTile(myDetails.x, myDetails.y-1);
                drawTile(myDetails.x+1, myDetails.y-1);
                drawTile(myDetails.x-1, myDetails.y-1);
                drawTile(myDetails.x-2, myDetails.y-1);

                // draw tiles below
                drawTile(myDetails.x, myDetails.y+1);
                drawTile(myDetails.x+1, myDetails.y+1);
                drawTile(myDetails.x-1, myDetails.y+1);
                drawTile(myDetails.x-2, myDetails.y+1);
            } else {
                // draw tiles above
                drawTile(myDetails.x, myDetails.y-1);
                drawTile(myDetails.x-1, myDetails.y-1);
                drawTile(myDetails.x+1, myDetails.y-1);
                drawTile(myDetails.x+2, myDetails.y-1);

                // draw tiles below
                drawTile(myDetails.x, myDetails.y+1);
                drawTile(myDetails.x-1, myDetails.y+1);
                drawTile(myDetails.x+1, myDetails.y+1);
                drawTile(myDetails.x+2, myDetails.y+1);
            }
            // top row
            drawTile(myDetails.x, myDetails.y-2);
            drawTile(myDetails.x-1, myDetails.y-2);
            drawTile(myDetails.x+1, myDetails.y-2);
            // bottom row
            drawTile(myDetails.x, myDetails.y+2);
            drawTile(myDetails.x-1, myDetails.y+2);
            drawTile(myDetails.x+1, myDetails.y+2);
        }

        // draw the tile selected circle
        if(typeof(hexSelectedX) == "number" && typeof(hexSelectedY) == "number") {
            var x = hexSelectedX * hexRectangleWidth + (hexRectangleWidth/2 * (hexSelectedY % 2)) + hexRectangleWidth/2;
            var y = hexSelectedY * hexRectangleHeight - (sideLength/2 * hexSelectedY) + hexRectangleHeight/2;
            ctx.save();
            ctx.strokeStyle = "#ADFF2F";
            ctx.shadowColor = "black";
            ctx.shadowBlur = 15;
            ctx.shadowOffsetX = 0;
            ctx.shadowOffsetY = 0;
            ctx.lineWidth = 1 / zoomLevel;
            drawCircle(ctx, x, y, hexRadius);
            ctx.stroke();
            ctx.restore();
        }

        // draw the mouse hover circle
        if(typeof(hexHoverX) == "number" && typeof(hexHoverY) == "number") {
            var x = hexHoverX * hexRectangleWidth + (hexRectangleWidth/2 * (hexHoverY % 2)) + hexRectangleWidth/2;
            var y = hexHoverY * hexRectangleHeight - (sideLength/2 * hexHoverY) + hexRectangleHeight/2;
            ctx.save();
            ctx.strokeStyle = hexHoverValid ? "#3498db" : "#e74c3c";
            ctx.shadowColor = "black";
            ctx.shadowBlur = 15;
            ctx.shadowOffsetX = 0;
            ctx.shadowOffsetY = 0;
            ctx.lineWidth = 1 / zoomLevel;
            drawCircle(ctx, x, y, hexRadius);
            ctx.stroke();
            ctx.restore();
        }

        // draw all the players my player knows the location of
        for(var player of players) {
            var details = playerDetails[player.ID];
            if(details) {
                var x = details.x * hexRectangleWidth + (hexRectangleWidth/2 * (details.y % 2)) + hexRectangleWidth/2;
                var y = details.y * hexRectangleHeight - (sideLength/2 * details.y) + hexRectangleHeight/2;
                var imgWidth = hexRectangleHeight * 0.75 * 1080 / 1920;
                var imgHeight = hexRectangleHeight * 0.75;
                ctx.drawImage(playerImage, x-imgWidth/2, y-imgHeight/2, imgWidth, imgHeight);
            }
        }

        ctx.restore();
        hitCtx.restore();

        //requestAnimationFrame(drawMap);
    }

    function drawTile(i, j, drawToHit) {
        if(j < 0 || j >= hexGrid.length || i < 0 || i >= hexGrid[j].length) {
            console.log(i, j)
            return;
        }

        var x = i * hexRectangleWidth + (hexRectangleWidth/2 * (j % 2));
        var y = j * hexRectangleHeight - (sideLength/2 * j);

        // draw visible hexagon
        ctx.fillStyle = TileColors[hexGrid[j][i].type];
        ctx.strokeStyle = "#aaa";
        drawHexagon(ctx, x, y);
        ctx.fill();
        ctx.stroke();

        // draw tile image over top of hexagon
        if(TileImages[hexGrid[j][i].type]) {
            var imgWidth = hexRectangleHeight * 1920 / 1080;
            var imgHeight = hexRectangleHeight;
            if(imgWidth > hexRectangleWidth) {
                imgWidth = hexRectangleWidth;
                imgHeight = hexRectangleWidth * 1080 / 1920;
            }
            ctx.drawImage(TileImages[hexGrid[j][i].type], x, y+imgHeight/2, imgWidth, imgHeight);
        }

        if(drawToHit) {
            // draw hitbox hexagon
            hitCtx.fillStyle = hexGrid[j][i].colorKey;
            drawHexagon(hitCtx, x, y);
            hitCtx.fill();
        }
    }

    function drawHexagon(context, x, y) {
        context.beginPath();
        context.moveTo(x + hexRadius, y);
        context.lineTo(x + hexRectangleWidth, y + hexHeight);
        context.lineTo(x + hexRectangleWidth, y + hexHeight + sideLength);
        context.lineTo(x + hexRadius, y + hexRectangleHeight);
        context.lineTo(x, y + sideLength + hexHeight);
        context.lineTo(x, y + hexHeight);
        context.closePath();
    }

    function drawCircle(context, x, y, r) {
        context.beginPath();
        context.arc(x, y, r, 0, 2*Math.PI);
        context.closePath();
    }

    function displayPlayerList(players) {
        var html = "";
        for(var player of players) {
            html += `<button id="player-list-item-${player.ID}" class="player-list-item">${player.Name}</button>`;
        }
        player_list.innerHTML = html;
    }

    function fetchPlayerList(lobbyName) {
        sendGetRequest(`/players?lobby_name=${lobbyName}`, (xmlHttp) => {
            // on success
            var p = JSON.parse(xmlHttp.responseText);
            if(p) { players = p; }
            console.log(players);
            displayPlayerList(players);
        }, (xmlHttp) => {
            // on error
            console.log(XMLHttpRequest.responseText);
        });
    }

    function fetchMap(mapName) {
        sendGetRequest(`/maps/${mapName}.txt`, (xmlHttp) => {
            // on success
            // initialise the map array
            hexGrid = [];
            // split into lines (rows)
            var lines = xmlHttp.responseText.split("\n");
            var y = 0;
            for(var line of lines) {
                hexGrid.push([]);
                // split into tiles
                var tiles = line.split(" ");
                var x = 0;
                for(var tile of tiles) {
                    var colorKey = genRandomColor();
                    // key generating random colors until unique color is found
                    while(tileColorMap[colorKey]) {
                        colorKey = genRandomColor();
                    }
                    // set the tile object
                    hexGrid[hexGrid.length-1].push({
                        type: parseInt(tile) || 0,
                        colorKey: colorKey
                    });
                    // add tile to color map
                    tileColorMap[colorKey] = {
                        x: x,
                        y: y
                    };
                    x++;
                }
                totalMapWidth = x * hexRectangleWidth;
                y++;
            }
            totalMapHeight = y * hexRectangleHeight;

            var minZoomLevel = canvas.width / totalMapWidth;
            minZoomLevel = minZoomLevel >  canvas.height/totalMapHeight ? canvas.height/totalMapHeight : minZoomLevel;
            zoomLevel = minZoomLevel;

            toggle_ready_btn.disabled = false;  // allow the player to ready up
        }, (xmlHttp) => {
            // on error
            alert("Fatal Error: Failed to retrieve map " + mapName);
        });
    }

    function toggleReadyState(e) {
        socket.send(`B`);
    }

    function connectToWebSocketServer(socketOpenedCallback) {
        socket = new WebSocket(`ws://${location.hostname}/open_ws_connection`);

        socket.onopen = (e) => {
            socket.send(`Aname:${Cookies.get("player_name")};token:${Cookies.get("player_token")};lobby:${Cookies.get("lobby_name")}`);
            char_lbl.innerHTML = Cookies.get("player_name");
            if(typeof(socketOpenedCallback) === "function") {
                socketOpenedCallback();
            }
        };

        socket.onmessage = (e) => {
            onMessage(e.data);
        };
    }

    function sendSetMoveTurnMessage() {
        socket.send(`Gcrouch:false;x:${hexSelectedX};y:${hexSelectedY}`);
    }

    // Called upon receiving a message from the server
    function onMessage(msg) {
        console.log("received: " + msg);
        if(msg == "update:players") {
            fetchPlayerList(Cookies.get("lobby_name"));
        } else if(msg.startsWith("update:ready;")) {
            processReadyStateUpdate(msg);
        } else if(msg.startsWith("update:game;")) {
            processGameUpdate(msg);
        } else if(msg.startsWith("yourid;")) {
            processYourID(msg);
        } else if(msg.startsWith("phasetime;")) {
            processPhaseTimeUpdate(msg);
        } else if(msg.startsWith("map;")) {
            processMapUpdate(msg);
        } else if(msg == "request:drop") {
            processDropPhase();
        } else if(msg == "request:move") {
            processMovePhase();
        } else if(msg == "request:action") {
            processActionPhase();
        } else if(msg == "turn:valid") {
            console.log("turn valid");
        } else if(msg == "turn:invalid") {
            console.log("turn invalid");
        }
    }

    function processDropPhase() {
        console.log("choose a drop tile");
        phase = GamePhase.DROP_PHASE;

        if(!started) {
            started = true;
            toggle_ready_btn.style.visibility = "hidden";
            requestAnimationFrame(drawMap);
        }

        var startTime = Date.now();

        var id = setInterval(() => {
            var timeElapsed = Date.now() - startTime;
            var timeLeftSecs = Math.floor((dropPhaseTime - timeElapsed / 1000));
            title_lbl.innerHTML = `Drop Phase ${timeLeftSecs}`;

            if(timeLeftSecs <= 1) {
                clearInterval(id);
            }
        }, 1000);
    }

    function processMovePhase() {
        console.log("enter a move");
        phase = GamePhase.MOVE_PHASE;

        var startTime = Date.now();

        var id = setInterval(() => {
            var timeElapsed = Date.now() - startTime;
            var timeLeftSecs = Math.floor((movePhaseTime - timeElapsed / 1000));
            title_lbl.innerHTML = `Move Phase ${timeLeftSecs}`;

            if(timeLeftSecs <= 1) {
                clearInterval(id);
            }
        }, 1000);
    }

    function processActionPhase() {
        console.log("enter a list of actions");
        phase = GamePhase.ACTION_PHASE;

        var startTime = Date.now();

        var id = setInterval(() => {
            var timeElapsed = Date.now() - startTime;
            var timeLeftSecs = Math.floor((actionPhaseTime - timeElapsed / 1000));
            title_lbl.innerHTML = `Action Phase ${timeLeftSecs}`;

            if(timeLeftSecs <= 1) {
                clearInterval(id);
            }
        }, 1000);
    }

    // Dissect a ready state update message and make relevant GUI changes
    function processReadyStateUpdate(msg) {
        var data = msg.split("update:ready;").length > 1 ? msg.split("update:ready;")[1] : null;
        if(data) {
            var splits = data.split(";");
            var player, state;

            // extract relevant information from data string
            for(var elem of splits) {
                var parts = elem.split(":");
                if(parts.length > 1) {
                    var key = parts[0];
                    var value = parts[1];

                    if(key == "player") {
                        player = value;
                    } else if(key == "ready") {
                        state = value;
                    }
                }
            }

            console.log(player, state)

            // message must contain player and state to be usable
            if(player && state) {
                var listItem = document.getElementById(`player-list-item-${player}`);
                if(listItem) {
                    // update the list item to reflect the update
                    if(state == "true") {
                        listItem.style.backgroundColor = "#55ff55";
                    } else {
                        listItem.style.backgroundColor = "#ff5555";
                    }
                }
            }
        }
    }

    // Dissect a game update message and make relevant changes
    function processGameUpdate(msg) {
        var data = msg.split("update:game;").length > 1 ? msg.split("update:game;")[1] : null;
        if(data) {
            var splits = data.split(";");

            // extract relevant information from data string
            for(var elem of splits) {
                var id, x, y, crouch;
                if(elem.startsWith("details:player,")) {
                    var details = elem.split("details:player,");
                    if(details.length > 1) {
                        processPlayerUpdate(details[1]);
                    }
                }
            }

            requestAnimationFrame(drawMap);
        }
    }

    // Process a player segment of a game update
    function processPlayerUpdate(elem) {
        var segments = elem.split(",");
        for(var seg of segments) {
            var parts = seg.split(":");
            if(parts.length > 1) {
                var key = parts[0];
                var value = parts[1];

                if(key == "id") {
                    id = value;
                } else if(key == "x") {
                    x = value;
                } else if(key == "y") {
                    y = value;
                } else if(key == "c") {
                    crouch = value;
                }
            }
        }

        // update the player details map with the new information
        playerDetails[id] = {
            x: parseInt(x, 10),
            y: parseInt(y, 10),
            crouch: crouch == "true"
        }
    }

    // Obtain this players ID from the message
    function processYourID(msg) {
        var data = msg.split("yourid;").length > 1 ? msg.split("yourid;")[1] : null;
        if(data) {
            var splits = data.split(";");
            var id;

            // extract relevant information from data string
            for(var elem of splits) {
                var parts = elem.split(":");
                if(parts.length > 1) {
                    var key = parts[0];
                    var value = parts[1];

                    if(key == "id") {
                        id = value;
                    }
                }
            }

            // message must contain id to be usable
            if(id) {
                myID = id;
            }
        }
    }

    // Dissect a phase time message and set the phase time variables
    function processPhaseTimeUpdate(msg) {
        var data = msg.split("phasetime;").length > 1 ? msg.split("phasetime;")[1] : null;
        if(data) {
            var splits = data.split(";");
            var movetime, actiontime, droptime;

            // extract relevant information from data string
            for(var elem of splits) {
                var parts = elem.split(":");
                if(parts.length > 1) {
                    var key = parts[0];
                    var value = parts[1];

                    if(key == "movetime") {
                        movetime = value;
                    } else if(key == "actiontime") {
                        actiontime = value;
                    } else if(key == "droptime") {
                        droptime = value;
                    }
                }
            }

            console.log(droptime, movetime, actiontime);

            // message must contain movetime and actiontime to be usable
            if(droptime && movetime && actiontime) {
                dropPhaseTime = parseInt(droptime, 10) || dropPhaseTime;
                movePhaseTime = parseInt(movetime, 10) || movePhaseTime;
                actionPhaseTime = parseInt(actiontime, 10) || actionPhaseTime;
            }
        }
    }

    // Dissect a map update and then load the map from the server
    function processMapUpdate(msg) {
        var data = msg.split("map;").length > 1 ? msg.split("map;")[1] : null;
        if(data) {
            var splits = data.split(";");
            var mapName;

            // extract relevant information from data string
            for(var elem of splits) {
                var parts = elem.split(":");
                if(parts.length > 1) {
                    var key = parts[0];
                    var value = parts[1];

                    if(key == "name") {
                        mapName = value;
                    }
                }
            }

            console.log("loading map " + mapName);

            // message must contain mapName to be usable
            if(mapName) {
                // fetch the map from the server
                fetchMap(mapName);
            }
        }
    }

    function sendGetRequest(URL, success, error) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4) {
                if(xmlHttp.status >= 200 && xmlHttp.status < 300) {
                    if(typeof(success) === "function") {
                        success(xmlHttp);
                    }
                } else {
                    if(typeof(error) === "function") {
                        error(xmlHttp);
                    }
                }
            }
        }

        // send the ajax request
        xmlHttp.open("GET", URL, true);   // true for asynchronous
        xmlHttp.send(null);
    }

    // get the object representing my player
    function myPlayer() {
        for(var player of players) {
            if(player.ID == myID) {
                return player;
            }
        }
        return null;
    }

    // Returns true iff the tile type is impassable.
    function isTileImpassable(t) {
        if(t == TileType.WATER || t == TileType.MOUNTAIN) {
            return true;
        } else {
            return false;
        }
    }

    // NOTE - Does not include impassable tiles.
    // Returns true iff the tile cannot be landed on in the drop phase.
    function isTileUndroppable(t) {
        if(t == TileType.HOUSE || t == TileType.HIGHRISE) {
            return true;
        } else {
            return false;
        }
    }

    // create arrays of any no. of dimensions
    // sourced from https://stackoverflow.com/questions/966225/how-can-i-create-a-two-dimensional-array-in-javascript/966938#966938
    function createArray(length) {
        var arr = new Array(length || 0);
        var i = length;

        if(arguments.length > 1) {
            var args = Array.prototype.slice.call(arguments, 1);
            while(i--) arr[length-1 - i] = createArray.apply(this, args);
        }

        return arr;
    }

    // generate a random color
    // sourced from https://blog.lavrton.com/hit-region-detection-for-html5-canvas-and-how-to-listen-to-click-events-on-canvas-shapes-815034d7e9f8
    function genRandomColor() {
        const r = Math.round(Math.random() * 255);
        const g = Math.round(Math.random() * 255);
        const b = Math.round(Math.random() * 255);
        return `rgb(${r},${g},${b})`;
    }

    window.onresize = () => {
        canvas.setAttribute("width", canvas.clientWidth);
        canvas.setAttribute("height", canvas.clientHeight);
        hitCanvas.setAttribute("width", hitCanvas.clientWidth);
        hitCanvas.setAttribute("height", hitCanvas.clientHeight);
        requestAnimationFrame(drawMap);
    };

    window.onload = () => {
        player_list = document.getElementById("player-list");
        toggle_ready_btn = document.getElementById("toggle-ready-btn");
        char_lbl = document.getElementById("char-lbl");
        title_lbl = document.getElementById("title-lbl");
        canvas = document.getElementById("map-canvas");
        ctx = canvas.getContext("2d");
        hitCanvas = document.getElementById("hit-canvas");
        hitCtx = hitCanvas.getContext("2d");

        canvas.setAttribute("width", canvas.clientWidth);
        canvas.setAttribute("height", canvas.clientHeight);
        canvas.addEventListener("click", on_canvas_click);
        canvas.addEventListener("mousemove", on_canvas_mouse_move);
        //canvas.addEventListener("DOMMouseScroll", (e) => { on_canvas_mouse_wheel(); }, false); // For Firefox
        canvas.addEventListener("wheel", on_canvas_mouse_wheel, true);     // For everyone else

        hitCanvas.setAttribute("width", hitCanvas.clientWidth);
        hitCanvas.setAttribute("height", hitCanvas.clientHeight);

        zoomTargetX = 0;
        zoomTargetY = 0;

        connectToWebSocketServer(() => {
            // socket open
            toggle_ready_btn.onclick = toggleReadyState;
        });
    };
})();
