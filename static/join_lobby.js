(function() {
    var lobby_name_input;
    var player_name_input;
    var password_input;
    var lobby_name_error_lbl;
    var player_name_error_lbl;
    var password_error_lbl;
    var ajax_error_lbl;

    function join_lobby_btn_on_click() {
        var lobbyName = lobby_name_input.value;
        var playerName = player_name_input.value;
        var password = password_input.value;
        var error = false;

        if(!lobbyName) {
            lobby_name_error_lbl.innerHTML = "*";
            lobby_name_error_lbl.style.visibility = "visible";
            error = true;
        } else {
            lobby_name_error_lbl.style.visibility = "hidden";
        }

        if(!playerName) {
            player_name_error_lbl.innerHTML = "*";
            player_name_error_lbl.style.visibility = "visible";
            error = true;
        } else {
            player_name_error_lbl.style.visibility = "hidden";
        }

        // if there were no errors, make the ajax request
        if(!error) {
            sendJoinLobbyRequest(lobbyName, playerName, password)
        }
    }

    // send an ajax request to the server to join a lobby
    function sendJoinLobbyRequest(lobbyName, playerName, password) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4) {
                if(xmlHttp.status >= 200 && xmlHttp.status < 300) {
                    // status ok
                    Cookies.set("player_token", xmlHttp.responseText);
                    Cookies.set("lobby_name", lobbyName);
                    Cookies.set("player_name", playerName);
                    ajax_error_lbl.style.visibility = "hidden";
                    window.location.href = "game.html";
                } else {
                    // show error by password field if it is empty & if unauthorized
                    if(!password && xmlHttp.status === 401) {
                        password_error_lbl.innerHTML = "*";
                        password_error_lbl.style.visibility = "visible";
                    } else {
                        // error occured
                        ajax_error_lbl.innerHTML = xmlHttp.responseText;
                        ajax_error_lbl.style.visibility = "visible";
                    }
                }
            }
        }

        password_error_lbl.style.visibility = "hidden";

        // send the ajax request
        if(password) {
            xmlHttp.open("GET", `/games/join?lobby_name=${lobbyName}&player_name=${playerName}&password=${password}`, true);   // true for asynchronous
        } else {
            xmlHttp.open("GET", `/games/join?lobby_name=${lobbyName}&player_name=${playerName}`, true);   // true for asynchronous
        }
        xmlHttp.send(null);
    }

    // wait for document to fully load before getting element and setting event callbacks
    window.onload = () => {
        lobby_name_input = document.getElementById("lobby-name-input");
        player_name_input = document.getElementById("player-name-input");
        password_input = document.getElementById("password-input");
        lobby_name_error_lbl = document.getElementById("lobby-name-error-lbl");
        player_name_error_lbl = document.getElementById("player-name-error-lbl");
        password_error_lbl = document.getElementById("password-error-lbl");
        ajax_error_lbl = document.getElementById("ajax-error-lbl");
        document.getElementById("join-lobby-btn").onclick = join_lobby_btn_on_click;
    };
})();
