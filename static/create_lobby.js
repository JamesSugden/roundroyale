(function() {
    var name_input;
    var password_input;
    var is_password_protected_input;
    var name_error_lbl;
    var password_error_lbl;
    var ajax_error_lbl;

    // enabled or disable password protection
    function is_password_protected_input_on_click() {
        if(is_password_protected_input.checked) {
            password_input.disabled = false;
            password_input.focus();
        } else {
            password_input.value = "";
            password_input.disabled = true;
            password_error_lbl.style.visibility = "hidden";
        }
    }

    // verify inputs before submitting ajax request
    function create_lobby_btn_on_click() {
        var name = name_input.value;
        var error = false;

        if(!name) {
            name_error_lbl.innerHTML = "*";
            name_error_lbl.style.visibility = "visible";
            error = true;
        } else {
            name_error_lbl.style.visibility = "hidden";
        }

        var isPasswordProtected = is_password_protected_input.checked;
        var password = password_input.value;

        if(isPasswordProtected && !password) {
            password_error_lbl.innerHTML = "*";
            password_error_lbl.style.visibility = "visible";
            error = true;
        } else {
            password_error_lbl.style.visibility = "hidden";
        }

        // if there were no errors, make the ajax request
        if(!error) {
            sendCreateLobbyRequest(name, password)
        }
    };

    // send an ajax request to the server to create a new lobby
    // lobby will be public iff !password
    function sendCreateLobbyRequest(name, password) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == 4) {
                if(xmlHttp.status >= 200 && xmlHttp.status < 300) {
                    // status ok
                    Cookies.set("player_token", xmlHttp.responseText);
                    Cookies.set("lobby_name", name);
                    Cookies.set("player_name", name);
                    ajax_error_lbl.style.visibility = "hidden";
                    window.location.href = "game.html";
                } else {
                    // error occured
                    ajax_error_lbl.innerHTML = xmlHttp.responseText;
                    ajax_error_lbl.style.visibility = "visible";
                }
            }
        }

        // send the ajax request
        if(password) {
            xmlHttp.open("GET", `/games/create?player_name=${name}&password=${password}`, true);   // true for asynchronous
        } else {
            xmlHttp.open("GET", `/games/create?player_name=${name}`, true);   // true for asynchronous
        }
        xmlHttp.send(null);
    }

    // wait for document to fully load before getting element and setting event callbacks
    window.onload = () => {
        name_input = document.getElementById("name-input");
        password_input = document.getElementById("password-input");
        is_password_protected_input = document.getElementById("is-password-protected-input");
        name_error_lbl = document.getElementById("name-error-lbl");
        password_error_lbl = document.getElementById("password-error-lbl");
        ajax_error_lbl = document.getElementById("ajax-error-lbl");
        document.getElementById("create-lobby-btn").onclick = create_lobby_btn_on_click;
        is_password_protected_input.onclick = is_password_protected_input_on_click;
    };
})();
