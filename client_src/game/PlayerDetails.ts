export interface PlayerDetails {
    x: number;
    y: number;
    crouch: boolean;
}
