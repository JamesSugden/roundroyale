export const ActionApCosts = {
    CROUCH: 0,
    MOVE: 40,
    ATTACK: 60,
    EQUIP_WEAPON: 5,
    BANDAGE: 30,
    SEARCH: 20
};

export interface Action {
    name: string,
    apCost: number
}

export interface ActionAttack extends Action {
    playerID: number
}

export interface ActionEquipWeapon extends Action {
    entityID: number,
    itemID: number
}
