export enum GamePhase {
    LOBBY,
    DROP_PHASE,
    MOVE_PHASE,
    ACTION_PHASE,
    WAITING_FOR_SERVER
}
