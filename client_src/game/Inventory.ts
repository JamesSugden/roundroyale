import { Controller } from "../Controller"
import { ItemEntity } from "./Item"

export class Inventory {
    public static readonly ROW_LENGTH = 3;
    public static readonly COL_HEIGHT = 4;

    // Grid of item entities
    private _itemSlots: Array<Array<ItemEntity>>;

    public constructor(private controller: Controller) {
        // Initialise item slots as a grid full of null item entities
        this._itemSlots = new Array<Array<ItemEntity>>();
        for(let j = 0; j < Inventory.COL_HEIGHT; j++) {
            this._itemSlots.push(new Array<ItemEntity>());
            for(let i = 0; i < Inventory.ROW_LENGTH; i++) {
                this._itemSlots[j].push(null);
            }
        }
    }

    public get itemSlots(): Array<Array<ItemEntity>> {
        return this._itemSlots;
    }

    // Add an item entity to the players inventory
    // Returns true iff the item is added successfully
    public addItemEntity(itemEntity: ItemEntity, firstSlotX: number, firstSlotY: number): boolean {
        if(!itemEntity) {
            return false;
        }

        const item = this.controller.items[itemEntity.itemID];

        // Check item exists
        if(!item) {
            return false;
        }

        // Bounds check
        if(firstSlotX < 0 || firstSlotX+item.gridWidth > Inventory.ROW_LENGTH ||
            firstSlotY < 0 || firstSlotY+item.gridHeight > Inventory.COL_HEIGHT) {
            return false;
        }

        // If all slots required by the item are free, add the entity to the inventory
        // TODO - add support for rotated item entities
        for(let slotX = firstSlotX; slotX < firstSlotX+item.gridWidth; slotX++) {
            for(let slotY = firstSlotY; slotY < firstSlotY+item.gridHeight; slotY++) {
                this._itemSlots[slotY][slotX] = itemEntity;
            }
        }

        // Return true as item entity successfully added
        return true;
    }

    public removeItemEntity(itemEntity: ItemEntity) {

    }
}
