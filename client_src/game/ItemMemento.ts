import { Item } from "./Item"

export class ItemMemento {
    private _since: number;  // counts up from 0, once 5, memento will be removed
    private _items: Array<Item>;
    private _colorKey: string;

    public constructor(private _mapX: number, private _mapY: number) {
        this._items = new Array<Item>();
        this._since = 0;
    }

    get mapX(): number {
        return this._mapX
    }

    get mapY(): number {
        return this._mapY;
    }

    get since(): number {
        return this._since;
    }

    public incSince() {
        this._since ++;
    }

    public reset() {
        this._since = 0;
    }

    get items(): Array<Item> {
        return this._items;
    }

    set items(i: Array<Item>) {
        if(i) {
            this._items = i;
        }
    }

    get colorKey(): string {
        return this._colorKey;
    }

    set colorKey(s: string) {
        if(s) {
            this._colorKey = s;
        }
    }
}
