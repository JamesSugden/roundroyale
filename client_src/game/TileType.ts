export class TileType {
    private static _types: Array<TileType>;

    private constructor(private _name: string, private _color: string, private _desc: Array<string>, private _img: HTMLImageElement=null,
                    private _impassable: boolean=false, private _undroppable: boolean=false,
                    private _blocksVision: boolean=false) {}

    // TODO - make tileset map specific using map.meta file
    static init() {
        this._types = [
            new TileType("Water", "#40a4df", ["Requires boat to travel"], null, true, true),
            new TileType("Bog", "#395D33", ["Vehicles get stuck"]),
            new TileType("Grassland", "#308014", ["May contain mines"]),
            new TileType("Forest", "#308014", ["Provides cover from enemies", "Greater chance to find items"], new Image(), false, false, true),
            new TileType("Hills", "#308014", ["Up High (+1 View Range)"], new Image()),
            new TileType("Moutains", "#808080", ["Will probably block vision when I figure out how to get that to work"], new Image(), true, true, true),
            new TileType("House", "#7f0000", ["Provides cover from enemies", "Greater chance to find items"], new Image(), false, true),
            new TileType("City", "#7f0000", ["Provides cover from enemies", "Greater chance to find items", "Up High (+1 View Range)"], new Image(), false, true, true),
            new TileType("Bridge", "#40a4df", ["Allows land-based travel over water"], new Image())
        ];

        // yuck
        this._types[3].img.src = "assets/Tree (512x512).png";
        this._types[4].img.src = "assets/Hill (512x512).png";
        this._types[5].img.src = "assets/Mountain (512x512).png";
        this._types[6].img.src = "assets/House (512x512).png";
        this._types[7].img.src = "assets/Skyscraper (512x512).png";
        this._types[8].img.src = "assets/Bridge (512x512).png";
    }

    get name(): string {
        return this._name;
    }

    get color(): string {
        return this._color;
    }

    get desc(): Array<string> {
        return this._desc;
    }

    get img(): HTMLImageElement {
        return this._img;
    }

    get impassable(): boolean {
        return this._impassable;
    }

    get undroppable(): boolean {
        return this._undroppable;
    }

    get blocksVision(): boolean {
        return this._blocksVision;
    }

    static get types(): Array<TileType> {
        return TileType._types;
    }
}

TileType.init();
