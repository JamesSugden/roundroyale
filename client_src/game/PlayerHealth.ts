export interface PlayerHealth {
    total: number,
    head: number,
    chest: number,
    rarm: number,
    larm: number,
    rleg: number,
    lleg: number
}
