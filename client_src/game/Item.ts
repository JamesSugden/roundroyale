export interface Item {
    name: string,
    image: string,
    gridWidth: number,
    gridHeight: number
}

export interface Gun extends Item {
    shotsPerAction: number,
    dmgPerHit: number,
    maxRange: number,
    accuracy: number,
}

export interface Projectile extends Item {
    timeToThrow: number,
    dmgPerHit: number
}

export interface Bandage extends Item {
    healAmount: number
}

export function isGun(item: Item): boolean {
    return "shotsPerAction" in item;
}

export function isProjectile(item: Item): boolean {
    return "timeToThrow" in item;
}

export function isBandage(item: Item): boolean {
    return "healAmount" in item;
}

export class ItemEntity {
    public constructor(public entityID: number, public itemID: number) {}
}
