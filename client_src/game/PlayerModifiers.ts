// Provides modifier values to exisiting attributes
// Does NOT give the value of the attributes
export interface PlayerModifiers {
    viewRange: number,
    hitChance: number,
    totalAP: number,
    apMoveCost: number,
    dmgPerMove: number,
    sickDmgPerTurn: number,
    //moveRange: number,
    viewRangeDesc: Array<string>,
    hitChanceDesc: Array<string>,
    //moveRangeDesc: Array<string>
    totalAPDesc: Array<string>,
    apMoveCostDesc: Array<string>,
    dmgPerMoveDesc: Array<string>,
    sickDmgPerTurnDesc: Array<string>
}
