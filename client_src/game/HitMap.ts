import { ItemMemento } from "./ItemMemento"

export class HitMap {
    // NOTE - use 2 separate maps for faster memento lookup
    // Map from color to tile (x, y)
    private _tileColorMap = {};
    // Map from color to item memento (mem)
    private _mementoColorMap = {};

    // add an item memento to the color map
    // returns the color key as a string
    public addItemMemento(m): string {
        if(m) {
            const colorKey = this.genUniqueRandomColor();
            this._mementoColorMap[colorKey] = { mem: m };
            return colorKey;
        }
        return null;
    }

    // remove an item memento from the color map
    public removeItemMemento(m) {
        for(const color in this._mementoColorMap) {
            const mem = this._mementoColorMap[color].mem;
            if(mem === m) {
                delete this._mementoColorMap[color];
                break;
            }
        }
    }

    // add a tile to the color map
    // returns the color key as a string
    public addTile(x: number, y: number): string {
        const colorKey = this.genUniqueRandomColor();
        this._tileColorMap[colorKey] = {
            x: x,
            y: y
        };
        return colorKey;
    }

    // get the tile or item memento associated with the given color
    public get(colorKey: string): { x: number, y: number } | { mem: ItemMemento } {
        if(this._tileColorMap[colorKey]) {
            return this._tileColorMap[colorKey];
        }
        return this._mementoColorMap[colorKey];
    }

    // generate a unique random color
    // sourced from https://blog.lavrton.com/hit-region-detection-for-html5-canvas-and-how-to-listen-to-click-events-on-canvas-shapes-815034d7e9f8
    private genUniqueRandomColor(): string {
        // keep generating random colors until unique color is found
        var colorKey;
        do {
            const r = Math.round(Math.random() * 255);
            const g = Math.round(Math.random() * 255);
            const b = Math.round(Math.random() * 255);
            colorKey = `rgb(${r},${g},${b})`;
        } while(this._tileColorMap[colorKey] || this._mementoColorMap[colorKey]);

        return colorKey;
    }
}
