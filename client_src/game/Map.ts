import { Controller } from "../Controller"

export class Map {
    // multi-dimensional array giving the map tiles
    private _hexGrid: any;

    // map from color to tile (x, y)
    //private _tileColorMap = {};

    public constructor(private controller: Controller, text: string) {
        this.initMap(text);
    }

    private initMap(text: string) {
        // initialise the map array
        this._hexGrid = [];
        // split into lines (rows)
        const lines = text.split("\n");
        let y = 0;
        for(const line of lines) {
            this._hexGrid.push([]);
            // split into tiles
            const tiles = line.split(" ");
            let x = 0;
            for(const tile of tiles) {
                /*var colorKey = this.genRandomColor();
                // key generating random colors until unique color is found
                while(this._tileColorMap[colorKey]) {
                    colorKey = this.genRandomColor();
                }*/
                // set the tile object & add tile to hit map
                this._hexGrid[this._hexGrid.length-1].push({
                    type: parseInt(tile) || 0,
                    colorKey: this.controller.hitMap.addTile(x, y)
                });
                // add tile to color map
                /*this._tileColorMap[colorKey] = {
                    x: x,
                    y: y
                };*/
                x++;
            }
            y++;
        }
    }

    // generate a random color
    // sourced from https://blog.lavrton.com/hit-region-detection-for-html5-canvas-and-how-to-listen-to-click-events-on-canvas-shapes-815034d7e9f8
    /*private genRandomColor(): string {
        const r = Math.round(Math.random() * 255);
        const g = Math.round(Math.random() * 255);
        const b = Math.round(Math.random() * 255);
        return `rgb(${r},${g},${b})`;
    }*/

    get hexGrid() {
        return this._hexGrid;
    }

/*    get tileColorMap() {
        return this._tileColorMap;
    }*/
}
