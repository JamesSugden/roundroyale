import { GUI } from "./gui/GUI"
import { Client } from "./networking/Client"
import { PlayerStub } from "./game/PlayerStub"
import { PlayerDetails } from "./game/PlayerDetails"
import { PlayerModifiers } from "./game/PlayerModifiers"
import { PlayerHealth } from "./game/PlayerHealth"
import { Map } from "./game/Map"
import { HitMap } from "./game/HitMap"
import { GamePhase } from "./game/GamePhase"
import { Item, ItemEntity } from "./game/Item"
import { Inventory } from "./game/Inventory"
import { ItemMemento } from "./game/ItemMemento"
import { Action, ActionApCosts, ActionEquipWeapon } from "./game/Action"

export class Controller {
    // Uses touchstart on mobile devices and click on desktop
    public static readonly CLICK_EVENT: string = "ontouchend" in window ? "touchend" : "click";

    // Reference to the GUI
    private _gui: GUI;

    // Network WebSocket client
    private _client: Client;

    // Global mousex and mousey from the last mouse event
    private _globalMouseX: number;
    private _globalMouseY: number;

    // Map object loaded from the server
    private _map: Map;

    // HitMap object, provide method of mouse -> map hit detection
    private _hitMap: HitMap;

    // List of available items
    private _items: Array<Item>

    // ID of current Timeout used by this object
    private _timeoutID: number;

    // True iff the GUI is animating changes
    private _animating: boolean = false;

    // List of item mementos, each dissapears after 5 turns
    private _itemMementos: Array<ItemMemento>;

    // Storm variables
    private _stormRadius: number = -1;
    private _stormDmgPerTurn: number = 0;
    private _stormSickDmgPerTurn: number = 0;

    // Game details obtained from server
    // The current phase of the game (defaults to LOBBY i.e. not started)
    private _gamePhase: GamePhase = GamePhase.LOBBY;
    // Time taken for each phase (in seconds)
    private _dropPhaseTime: number = 10;
    private _movePhaseTime: number = 10;
    private _actionPhaseTime: number = 30;
    // Map from player ID to stub and details objects
    private _playerStubs: { [id: number]: PlayerStub };
    private _playerDetails: { [id: number]: PlayerDetails };
    // Health values for all visible players
    private _playerHPs: { [id: number]: PlayerHealth };
    // Modifier values for my player
    private _myPlayerModifiers: PlayerModifiers;
    // AP value for my player (as a percentage)
    private _myPlayerAP: number = 100;
    // List of actions to be performed by my player at end of action phase
    private _myPlayerActions: Array<Action>;
    // The inventory of my player
    private _myPlayerInventory: Inventory;
    // The item currently equipped by my player
    private _myPlayerEquippedItem: ItemEntity;
    // The item effectively equipped for future actions
    private _myPlayerEffectivelyEquippedItem: ItemEntity;
    // The player ID of the the player using this client
    private _myID: number = -1;
    // If true, _myID gives the ID of spectated player and no actions can be made
    private _spectating: boolean = false;

    public constructor() {
        this._gui = GUI.new(this);
        this._client = new Client(this);
        this._playerStubs = {};
        this._playerDetails = {};
        this._playerHPs = {};
        this._myPlayerModifiers = {
            viewRange: 0,
            hitChance: 0,
            totalAP: 0,
            apMoveCost: 0,
            dmgPerMove: 0,
            sickDmgPerTurn: 0,
            viewRangeDesc: null,
            hitChanceDesc: null,
            totalAPDesc: null,
            apMoveCostDesc: null,
            dmgPerMoveDesc: null,
            sickDmgPerTurnDesc: null
        };
        this._hitMap = new HitMap();
        this._items = new Array<Item>();
        this._itemMementos = new Array<ItemMemento>();
        this._myPlayerActions = new Array<Action>();
        this._myPlayerInventory = new Inventory(this);
    }

    // Start a timer
    public startTimer(time: number, intervalCallback: ((timeLeft: number, timeoutID: number) => void),
            endedCallback: (() => void), setTimeoutID: boolean=false, interval: number=200) {
        const startTime = Date.now();
        const $this = this;

        function timerLoop() {
            const timeElapsed = Date.now() - startTime;
            const timeLeft = time*1000 - timeElapsed;

            if(timeLeft > 0) {
                // check again in 200 ms
                const id = setTimeout(timerLoop, interval);
                if(setTimeoutID) {
                    $this._timeoutID = id;
                }

                if(intervalCallback) {
                    intervalCallback(timeLeft, id);
                }
            } else if(endedCallback) {
                // Reached end of time, therefore, call callback function
                endedCallback();
            }
        }

        // start the first execution of timer loop immediately
        timerLoop();
    }

    // Send a message to the other players in the lobby
    public sendMsg(msg: string) {
        this._client.sendSendMsgMsg(msg);
    }

    // Toggle the ready state of the player
    // Sends ready state toggle command to server
    public toggleReadyState() {
        this._client.sendToggleReadyStateMsg();
    }

    // Set the ready state of player to true
    public setReadyStateToTrue() {
        this._client.sendSetReadyStateToTrueMsg();
    }

    // Notify the server of the player's move
    public setMoveTurn(x: number, y: number) {
        this._client.sendSetMoveTurnMsg(x, y);
    }

    // Notify the server that the player wishes to search a tile
    public searchTile(x: number, y: number) {
        this._client.sendSearchTileMsg(x, y);
    }

    // Notify the server that the player has taken an item
    public takeItem(entityID: number, backpackX: number, backpackY: number) {
        this._client.sendTakeItemMsg(entityID, backpackX, backpackY);
    }

    // Notify the server that the player wishes to equip an item from their backpack
    public equipItem(entityID: number) {
        this._client.sendEquipItemMsg(entityID);
    }

    // Notify the server that the player wishes to drop an item from their backpack
    public dropItem(entityID: number) {
        this._client.sendDropItemMsg(entityID);
    }

    // Notify the server that the player wishes to use a bandage from their backpack
    public useBandage(entityID: number, limb: string) {
        this._client.sendUseBandageMsg(entityID, limb);
        this._gui.requestCloseFullOverlay();
    }

    // Notify the server that the player wishses to attack another player
    public attackPlayer(playerID: string, limb: string) {
        this._client.sendAttackPlayerMsg(playerID, limb);
    }

    // Notify the server that the player has finished their turn
    public endTurn() {
        this._client.sendEndTurnMsg();
    }

    // Request the map to be redrawn
    public requestDrawMap() {
        if(this._gui && !this._animating) {
            this._gui.requestDrawMap();
        }
    }

    // Request that the movement of players be animated on the map
    public requestAnimateMovement(oldPlayerDetails: { [id: number]: PlayerDetails }, callback: (() => void)) {
        if(!this._animating) {
            this._animating = true;
            this._gui.requestAnimateMovement(oldPlayerDetails, () => {
                this._animating = false;
                callback();
            });
        }
    }

    // Returns true iff there is a player on the specified tile
    // IMPORTANT - Excludes my player
    public isPlayerOnTile(x: number, y: number) {
        for(let playerID in this._playerDetails) {
            const player = this._playerDetails[playerID];
            if(player.x == x && player.y == y && player != this.myPlayerDetails) {
                return true;
            }
        }
        return false;
    }

    // Returns list of player ids on tile
    // IMPORTANT - Excludes my player
    public getPlayersOnTile(x: number, y: number): Array<string> {
        let ls = new Array<string>();
        for(let playerID in this._playerDetails) {
            const player = this._playerDetails[playerID];
            if(player.x == x && player.y == y && player != this.myPlayerDetails) {
                ls.push(playerID);
            }
        }
        return ls;
    }

    get myPlayerStub(): PlayerStub {
        return this._playerStubs[this._myID];
    }

    get myPlayerDetails(): PlayerDetails {
        return this._playerDetails[this._myID];
    }

    get myPlayerModifiers(): PlayerModifiers {
        return this._myPlayerModifiers;
    }

    set myPlayerModifiers(p: PlayerModifiers) {
        if(p) {
            this._myPlayerModifiers = p;
            this._gui.onMyPlayerModifiersUpdated();
        }
    }

    get myPlayerHP(): PlayerHealth {
        return this._playerHPs[this._myID];
    }

    get playerHPs(): { [id: number]: PlayerHealth } {
        return this._playerHPs;
    }

    set playerHPs(hp: { [id: number]: PlayerHealth }) {
        if(hp) {
            if(hp[this._myID]) {
                const totalHPChangedBy = this.myPlayerHP ? hp[this._myID].total - this.myPlayerHP.total : 0;
                const headInjured = this.myPlayerHP ? this.myPlayerHP.head > hp[this._myID].head : false;
                const chestInjured = this.myPlayerHP ? this.myPlayerHP.chest > hp[this._myID].chest : false;
                const rarmInjured = this.myPlayerHP ? this.myPlayerHP.rarm > hp[this._myID].rarm : false;
                const larmInjured = this.myPlayerHP ? this.myPlayerHP.larm > hp[this._myID].larm : false;
                const rlegInjured = this.myPlayerHP ? this.myPlayerHP.rleg > hp[this._myID].rleg : false;
                const llegInjured = this.myPlayerHP ? this.myPlayerHP.lleg > hp[this._myID].lleg : false;
                this._playerHPs = hp;
                this._gui.onMyPlayerHPUpdated(totalHPChangedBy, headInjured, chestInjured, rarmInjured, larmInjured, rlegInjured, llegInjured);
            } else {
                this._playerHPs = hp;
            }
        }
    }

    get myPlayerAP(): number {
        return this._myPlayerAP;
    }

    /*set myPlayerAP(ap: number) {
        this._myPlayerAP = ap;
    }*/

    get myPlayerInventory(): Inventory {
        return this._myPlayerInventory;
    }

    get myPlayerEquippedItem(): ItemEntity {
        //console.log("Equipped", this._myPlayerEffectivelyEquippedItem, this._myPlayerEquippedItem)
        if(this._myPlayerEffectivelyEquippedItem != null) {
            return this._myPlayerEffectivelyEquippedItem;
        }
        return this._myPlayerEquippedItem;
    }

    set myPlayerEquippedItem(entity: ItemEntity) {
        if(entity) {
            this._myPlayerEquippedItem = entity;
        }
    }

    get spectating(): boolean {
        return this._spectating;
    }

    get gamePhase(): GamePhase {
        return this._gamePhase;
    }

    get globalMouseX(): number {
        return this._globalMouseX;
    }

    get globalMouseY(): number {
        return this._globalMouseY;
    }

    set globalMouseX(x: number) {
        this._globalMouseX = x;
    }

    set globalMouseY(y: number) {
        this._globalMouseY = y;
    }

    // Get a reference to the loaded map
    get map(): Map {
        return this._map;
    }

    // Set the map being played
    set map(m: Map) {
        if(m) {
            this._map = m;
        }
    }

    get hitMap(): HitMap {
        return this._hitMap;
    }

    // Get a reference to the list of all available items
    get items(): Array<Item> {
        return this._items;
    }

    get stormRadius(): number {
        return this._stormRadius;
    }

    get stormDmgPerTurn(): number {
        return this._stormDmgPerTurn;
    }

    get stormSickDmgPerTurn(): number {
        return this._stormSickDmgPerTurn;
    }

    // Get all player stubs
    get playerStubs(): { [id: number]: PlayerStub } {
        return this._playerStubs;
    }

    // Get all player details
    get playerDetails(): { [id: number]: PlayerDetails } {
        return this._playerDetails;
    }

    // Set all the player stubs
    set playerStubs(stubs: { [id: number]: PlayerStub }) {
        if(stubs) {
            this._playerStubs = stubs;
            this._gui.onPlayerStubsUpdated();   // notify GUI of player stubs update
        }
    }

    // Set all the player details
    set playerDetails(details: { [id: number]: PlayerDetails }) {
        if(details) {
            // Keep track of previous details and animate between them
            clearTimeout(this._timeoutID);
            this._timeoutID = null;
            const oldDetails = this._playerDetails;
            this._playerDetails = details;

            console.log("got details", this.gamePhase)

            // If in the move phase, animate the movement of players
            if(this.gamePhase == GamePhase.MOVE_PHASE) {
                console.log("animating");
                this.requestAnimateMovement(oldDetails, () => {
                    this.setReadyStateToTrue();
                    this._gui.onWaitingForServer();
                    console.log("done animating");
                    this.requestDrawMap();
                });
            } else {
                this.setReadyStateToTrue();
                this._gui.onWaitingForServer();
            }

            if(this._playerDetails[this._myID]) {
                this.onMyPlayerDetailsUpdated();
            }
        }
    }

    // Set a single player stub
    public setPlayerStub(id: string, stub: PlayerStub) {
        if(stub) {
            this._playerStubs[id] = stub;
            this._gui.onPlayerStubsUpdated();   // notify GUI of player stubs update
        }
    }

    // Set a single player's details
    public setPlayerDetails(id: string, details: PlayerDetails) {
        if(details) {
            this._playerDetails[id] = details;
            if(id == ""+this._myID) {
                this.onMyPlayerDetailsUpdated();
            }
        }
    }

    // Called whenever myPlayerDetails are updated by the server
    private onMyPlayerDetailsUpdated() {
        // Notify GUI of my player details update
        this._gui.onMyPlayerDetailsUpdated();
    }

    // Get a reference to the list of item mementos
    get itemMementos(): Array<ItemMemento> {
        return this._itemMementos;
    }

    // Add a new item memento to the list of item mementos
    public addItemMemento(i: ItemMemento) {
        if(i) {
            let exists = false;
            for(const m of this._itemMementos) {
                if(m.mapX === i.mapX && m.mapY === i.mapY) {
                    // Reset this memento rather than adding a new one
                    m.items = i.items;
                    m.reset();
                    exists = true;
                    break;
                }
            }
            // Only add new memento if there is no momento on same tile
            if(!exists) {
                i.colorKey = this._hitMap.addItemMemento(i);
                this._itemMementos.push(i);
            }
        }
    }

    // Set the current item found
    // TODO - add support for multiple items found on one tile
    public setItemsFound(items: Array<{ entityID: string, itemID: string }>) {
        // Add a new memento for the current tile
        let m = new ItemMemento(this.myPlayerDetails.x, this.myPlayerDetails.y);
        for(const obj of items) {
            const itemID = parseInt(obj.itemID, 10);
            const item = itemID >= 0 && itemID < this._items.length
                        ? this._items[itemID] : null;
            if(item) {
                m.items.push(item);
            }
        }
        this.addItemMemento(m);
        this._gui.setItemsFound(items);
    }

    // Get the time given for the drop phase (in seconds)
    get dropPhaseTime(): number {
        return this._dropPhaseTime;
    }

    // Get the time given for the move phase (in seconds)
    get movePhaseTime(): number {
        return this._movePhaseTime;
    }

    // Get the time given for the action phase (in seconds)
    get actionPhaseTime(): number {
        return this._actionPhaseTime;
    }

    // Called when the server initiates the drop phase
    public onDropPhaseStart() {
        this._gamePhase = GamePhase.DROP_PHASE;
        this._gui.onGameStarted();
        this._gui.onDropPhaseStart();
        this.requestDrawMap();
        this.startTimer(this._dropPhaseTime, (timeLeft) => {
            this._gui.updateTimerLabel("Drop Phase", Math.floor(timeLeft/1000));
        }, () => {
            if(this._gamePhase === GamePhase.DROP_PHASE) {
                this._gamePhase = GamePhase.WAITING_FOR_SERVER;
                // Send ready message
                this.setReadyStateToTrue();
                this._gui.onWaitingForServer();
            }
        }, true);
    }

    // Called when the server initiates a move phase
    public onMovePhaseStart() {
        // Full overlay should only ever be open during action phase
        this._gui.requestCloseFullOverlay();

        // Determine the current effectively equipped item
        this._myPlayerEquippedItem = this._myPlayerEffectivelyEquippedItem ?
            this._myPlayerEffectivelyEquippedItem : this._myPlayerEquippedItem;
        this._myPlayerEffectivelyEquippedItem = null;

        // Increment all item momentos
        for(const m of this._itemMementos) {
            m.incSince();
        }

        // Remove any expired item mementos
        this._itemMementos = this._itemMementos.filter((e) => {
            return e.since < 6;
        });

        // Reset turn stats
        this._myPlayerAP = 100 +
            (this.myPlayerModifiers.totalAP != null ? this.myPlayerModifiers.totalAP : 0);

        this._gamePhase = GamePhase.MOVE_PHASE;
        this._gui.onMovePhaseStart();
        this.startTimer(this._movePhaseTime, (timeLeft) => {
            this._gui.updateTimerLabel("Move Phase", Math.floor(timeLeft/1000));
        }, () => {
            if(this._gamePhase === GamePhase.MOVE_PHASE) {
                // Send ready message
                this.waitForPlayerDetailsUpdate(5);
            //    this._gui.onWaitingForServer();
            }
        }, true);
    }

    // Called when the server initiates an action phase
    public onActionPhaseStart() {
        this._myPlayerActions.length = 0;
        this._gamePhase = GamePhase.ACTION_PHASE;
        this._gui.onActionPhaseStart();
        this.startTimer(this.actionPhaseTime, (timeLeft) => {
            this._gui.updateTimerLabel("Action Phase", Math.floor(timeLeft/1000));
        }, () => {
            if(this._gamePhase === GamePhase.ACTION_PHASE) {
                this._gamePhase = GamePhase.WAITING_FOR_SERVER;

                // Send ready message
                this.setReadyStateToTrue();
                this._gui.onWaitingForServer();
            }
        }, true);
    }

    // Wait for timeout seconds for the client to receive a player stubs update
    // Allows client to animate player movement & gunfire
    private waitForPlayerDetailsUpdate(timeout: number) {
        this.startTimer(timeout, (timeLeft, timeoutID) => {
            /*if(!this._animating) {
                clearTimeout(timeoutID);
                timeoutID = null;
                this.toggleReadyState();
                this._gui.onWaitingForServer();
            }*/
        }, () => {
            this._gamePhase = GamePhase.WAITING_FOR_SERVER;
            this.requestDrawMap();
            this.setReadyStateToTrue();
            this._gui.onWaitingForServer();
        });
    }

    // Called when the phase time settings have been received from the server
    public onPhaseTimesUpdate(dropTime: string, moveTime: string, actionTime: string) {
        this._dropPhaseTime = parseInt(dropTime, 10) || this._dropPhaseTime;
        this._movePhaseTime = parseInt(moveTime, 10) || this._movePhaseTime;
        this._actionPhaseTime = parseInt(actionTime, 10) || this._actionPhaseTime;
    }

    // Called when a move has been deemed valid
    public onMoveValid(newX: number, newY: number, crouch: boolean) {
        if(this.gamePhase == GamePhase.MOVE_PHASE) {
            if(crouch) {
                // NOTE - Reset AP rather than subtract since can clicking resets move turn
                this._myPlayerAP = 100 + this._myPlayerModifiers.totalAP - ActionApCosts.CROUCH;
            } else {
                // NOTE - Reset AP rather than subtract since can clicking resets move turn
                this._myPlayerAP = 100 + this._myPlayerModifiers.totalAP - ActionApCosts.MOVE -
                    (this.myPlayerModifiers.apMoveCost != null ? this.myPlayerModifiers.apMoveCost : 0);
            }
            this._gui.onMoveValid(newX, newY, crouch);
        }
    }

    // Called when an attack has been deemed valid
    public onAttackValid() {
        if(this.gamePhase == GamePhase.ACTION_PHASE) {
            this._myPlayerAP -= ActionApCosts.ATTACK;
            this._gui.onAttackValid({ name: "Attack", apCost: ActionApCosts.ATTACK, playerID: -1 });
        }
    }

    // Called when a search has been deemed valid
    public onSearchValid() {
        if(this.gamePhase == GamePhase.ACTION_PHASE) {
            this._myPlayerAP -= ActionApCosts.SEARCH;
            this._gui.onSearchValid({ name: "Search Tile", apCost: ActionApCosts.SEARCH });
        }
    }

    // Called when the server has validified a take item request
    public onTakeItemValid(entityID: number, itemID: number, firstSlotX: number, firstSlotY: number) {
        this._myPlayerInventory.addItemEntity(new ItemEntity(entityID, itemID), firstSlotX, firstSlotY);
        this._gui.onTakeItemValid(entityID, itemID, firstSlotX, firstSlotY);
    }

    // Called when the server has validified an equip item request
    public onEquipItemValid(entityID: number, itemID: number) {
        if(itemID < 0 || itemID >= this._items.length ||
            this._myPlayerActions.length >= 5 ||
            this._myPlayerAP - ActionApCosts.EQUIP_WEAPON + this._myPlayerModifiers.totalAP < 0) {
            return;
        }
        const action = {
            name: "Equip "+this._items[itemID].name,
            apCost: ActionApCosts.EQUIP_WEAPON,
            entityID: entityID,
            itemID: itemID
        } as ActionEquipWeapon;
        this._myPlayerEffectivelyEquippedItem = new ItemEntity(entityID, itemID);
        this._myPlayerAP -= ActionApCosts.EQUIP_WEAPON;
        this._myPlayerActions.push(action);
        this._gui.onEquipItemValid(action);
    }

    // Called when the server has validifed a drop item request
    public onDropItemValid(entityID: number, itemID: number) {
        if(itemID < 0 || itemID >= this._items.length) {
            return;
        }
        this._gui.onDropItemValid(entityID, itemID);
    }

    // Called when the server has validified a use bandage request
    public onUseBandageValid(entityID: number, itemID: number) {
        if(itemID < 0 || itemID >= this._items.length) {
            return;
        }
        this._gui.onUseBandageValid(entityID, itemID);
    }

    // Called when the player initiates an attack on another player
    public onInitiateAttackPlayer(playerID: string) {
        this._gui.onInitiateAttackPlayer(playerID);
    }

    // Called when the player wants to attack a player on a tile
    public onAttackPlayerOnTile(x: number, y: number, tileX: number, tileY: number) {
        this._gui.onAttackPlayerOnTile(x, y, tileX, tileY);
    }

    // Called at end of turn if players have died
    public onPlayerDead(playerID: number) {
        const playerName = this._playerStubs[playerID] ? this._playerStubs[playerID].Name : "Player"+playerID;
        this._gui.onPlayerDead(playerName);
        // If my player died, display You Died message
        if(playerID == this._myID) {
            this._gui.onMyPlayerDead();
            // Spectate the player who killed you
        //    this._myID = playerID;
            this._spectating = true;
        }
    }

    // Called at the end of the game
    public onGameOver(winnerIDs: Array<number>) {
        if(winnerIDs.length === 1) {
            if(winnerIDs[0] === this._myID) {
                // My player is the sole winner
                this._gui.onGameOver(winnerIDs, true, false);
            } else {
                // My player lost
                this._gui.onGameOver(winnerIDs, false, false);
            }
        } else {
            let didMyPlayerDraw = false;
            for(const winnerID of winnerIDs) {
                if(winnerID === this._myID) {
                    didMyPlayerDraw = true;
                }
            }
            if(didMyPlayerDraw) {
                // My player tied with other players
                this._gui.onGameOver(winnerIDs, true, true);
            } else {
                // My player lost
                this._gui.onGameOver(winnerIDs, false, false);
            }
        }
    }

    // Called when a storm approach update is received from server
    public onStormApproachUpdate() {
        this._gui.onStormApproachUpdate();
    }

    // Called when a storm radius update is received from server
    public onStormRadiusUpdate(radius: number) {
        this._stormRadius = radius;
        this._gui.onStormRadiusUpdate();
    }

    // Called when a storm dmg update is received from server
    public onStormDmgUpdate(dmgPerTurn: number, sickDmgPerTurn: number) {
        this._stormDmgPerTurn = dmgPerTurn;
        this._stormSickDmgPerTurn = sickDmgPerTurn;
        this._gui.onStormDmgUpdate();
    }

    // Called when the map is finished loading and being initialised
    public onMapLoaded() {
        this._gui.onMapLoaded();
    }

    // Called when the item list is received from the server
    public onItemsLoaded(items: Array<Item>) {
        if(items) {
            this._items = items;
        }
    }

    // Called when a players ready state is changed
    public onReadyStateUpdate(playerID: string, readyState: boolean) {
        if(this._playerStubs[playerID]) {
            this._playerStubs[playerID].Ready = readyState;
        }
        this._gui.onReadyStateUpdate(playerID, readyState);
    }

    // Called when this player's ID is received from the server
    public onMyIDUpdate(myID: string) {
        this._myID = !isNaN(parseInt(myID, 10)) ? parseInt(myID, 10) : this._myID;
    }

    // Called when a player disconnects from the game
    public onPlayerDisconnect(playerID: string) {
        const playerName = this.playerStubs[playerID] ?
                this.playerStubs[playerID].Name : `Player ${playerID}`;
        if(this._playerStubs[playerID]) {
            delete this._playerStubs[playerID];
        }
        if(this._playerDetails[playerID]) {
            delete this._playerDetails[playerID];
        }
        this._gui.onPlayerDisconnect(playerName);
    }

    // Called upon receiving a msg from another player
    public onMsgReceived(playerID: string, msg: string) {
        const playerName = this.playerStubs[playerID] ?
                this.playerStubs[playerID].Name : `Player ${playerID}`;
        this._gui.onMsgReceived(playerName, msg);
    }

    // Called when the player starts dragging an item in their inventory
    public onItemDragStart() {
        this._gui.onItemDragStart();
    }

    // Called when the player stops dragging an item in their inventory
    public onItemDragEnd() {
        this._gui.onItemDragEnd();
    }

    // Called when the player left or right clicks a bandage in their inventory
    public onBandageClick(x: number, y: number, entityID: number) {
        this._gui.onBandageClick(x, y, entityID);
    }

    // Called when the player clicks a limb button
    public onHealLimbClick(limb: string) {
        this._gui.onHealLimbClick(limb);
    }

    // Called when the player clicks the full screen overlay
    public onFullOverlayClick() {
        this._gui.onFullOverlayClick();
    }

    // Called when the player begins hovering over a new tile during the drop phase
    public onTileHoverDropPhase(i: number, j: number, x: number, y: number) {
        this._gui.onTileHoverDropPhase(i, j, x, y);
    }

    // Called when the player begins hovering over a new tile during the move phase
    public onTileHoverMovePhase(i: number, j: number, x: number, y: number) {
        this._gui.onTileHoverMovePhase(i, j, x, y);
    }

    // Called when the player begins hovering over a new tile during the action phase
    public onTileHoverActionPhase(name: string, desc: string, apCost: number, x: number, y: number) {
        this._gui.onTileHoverActionPhase(name, desc, apCost, x, y);
    }

    // Called when the player clicks a tile during the action phase
    public onTileRightClickedActionPhase(x: number, y: number, tileX: number,
            tileY: number, canSearch: boolean, canAttack: boolean) {
        this._gui.onTileClickedActionPhase(x, y, tileX, tileY, canSearch, canAttack);
    }

    // Called when the player begins hovering over an item
    public onItemHover(entity: ItemEntity, x: number, y: number) {
        this._gui.onItemHover(entity, x, y);
    }

    // Called when the player hovers an item memento
    public onItemMementoHover(mem: ItemMemento, x: number, y: number) {
        this._gui.onItemMementoHover(mem, x, y);
    }

    // Resize parts of the GUI when the window resizes
    public onWindowResize() {
        this._gui.onWindowResize();
    }
}
