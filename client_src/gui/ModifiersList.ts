import { Controller } from "../Controller"

export class ModifiersList {
    private modifiersList: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.modifiersList = document.getElementById("modifiers-list") as HTMLDivElement;
    }

    public refreshDisplay() {
        let html = "";
        const modifiers = this.controller.myPlayerModifiers;

        if(modifiers.viewRange != 0 || (modifiers.viewRangeDesc && modifiers.viewRangeDesc.length > 0)) {
            const sign = modifiers.viewRange > 0 ? "+" : "";
            const color = sign == "+" ? "#0000b2" : "#b20000" ;

            html += `
                <div id="view-range-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">View Range</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.viewRange}</label>
                </div>`;
        }

        if(modifiers.hitChance != 0 || (modifiers.hitChanceDesc && modifiers.hitChanceDesc.length > 0)) {
            const sign = modifiers.hitChance > 0 ? "+" : "";
            const color = sign == "+" ? "#0000b2" : "#b20000" ;

            html += `
                <div id="hit-chance-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Hit Chance</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.hitChance}%</label>
                </div>`;
        }

        if(modifiers.totalAP != 0 || (modifiers.totalAPDesc && modifiers.totalAPDesc.length > 0)) {
            const sign = modifiers.totalAP > 0 ? "+" : "";
            const color = sign == "+" ? "#0000b2" : "#b20000" ;

            html += `
                <div id="total-ap-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Total AP</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.totalAP}</label>
                </div>`;
        }

        if(modifiers.apMoveCost != 0 || (modifiers.apMoveCostDesc && modifiers.apMoveCostDesc.length > 0)) {
            const sign = modifiers.apMoveCost > 0 ? "+" : "";
            const color = sign == "" ? "#0000b2" : "#b20000" ;

            html += `
                <div id="ap-move-cost-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Move AP Cost</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.apMoveCost}</label>
                </div>`;
        }

        if(modifiers.dmgPerMove != 0 || (modifiers.dmgPerMoveDesc && modifiers.dmgPerMoveDesc.length > 0)) {
            const sign = modifiers.dmgPerMove > 0 ? "+" : "";
            const color = sign == "" ? "#0000b2" : "#b20000" ;

            html += `
                <div id="dmg-per-move-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Dmg Per Move</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.dmgPerMove}</label>
                </div>`;
        }

        if(modifiers.sickDmgPerTurn != 0 || (modifiers.sickDmgPerTurnDesc && modifiers.sickDmgPerTurnDesc.length > 0)) {
            const sign = modifiers.sickDmgPerTurn > 0 ? "+" : "";
            const color = sign == "" ? "#0000b2" : "#b20000" ;

            html += `
                <div id="sick-dmg-per-turn-modifier-div" class="modifiers-list-item">
                    <label class="modifiers-name-lbl">Dmg Per Turn</label>
                    <label class="modifiers-value-lbl" style="color:${color}">${sign}${modifiers.sickDmgPerTurn}</label>
                </div>`;
        }

        this.modifiersList.innerHTML = html;

        const viewRangeModifierDiv = document.getElementById("view-range-modifier-div");
        if(viewRangeModifierDiv) {
            viewRangeModifierDiv.addEventListener("mouseleave", this.onMouseLeave);
            viewRangeModifierDiv.addEventListener("mouseenter", this.onMouseEnter);
        }

        const hitChanceModifierDiv = document.getElementById("hit-chance-modifier-div");
        if(hitChanceModifierDiv) {
            hitChanceModifierDiv.addEventListener("mouseleave", this.onMouseLeave);
            hitChanceModifierDiv.addEventListener("mouseenter", this.onMouseEnter);
        }

        const totalAPModifierDiv = document.getElementById("total-ap-modifier-div");
        if(totalAPModifierDiv) {
            totalAPModifierDiv.addEventListener("mouseleave", this.onMouseLeave);
            totalAPModifierDiv.addEventListener("mouseenter", this.onMouseEnter);
        }

        const apMoveCostModifiersDiv = document.getElementById("ap-move-cost-modifier-div");
        if(apMoveCostModifiersDiv) {
            apMoveCostModifiersDiv.addEventListener("mouseleave", this.onMouseLeave);
            apMoveCostModifiersDiv.addEventListener("mouseenter", this.onMouseEnter);
        }

        const dmgPerMoveModifiersDiv = document.getElementById("dmg-per-move-modifier-div");
        if(dmgPerMoveModifiersDiv) {
            dmgPerMoveModifiersDiv.addEventListener("mouseleave", this.onMouseLeave);
            dmgPerMoveModifiersDiv.addEventListener("mouseenter", this.onMouseEnter);
        }

        const sickDmgPerTurnModifiersDiv = document.getElementById("sick-dmg-per-turn-modifier-div");
        if(sickDmgPerTurnModifiersDiv) {
            sickDmgPerTurnModifiersDiv.addEventListener("mouseleave", this.onMouseLeave);
            sickDmgPerTurnModifiersDiv.addEventListener("mouseenter", this.onMouseEnter);
        }
    }

    private onMouseEnter = (e) => {
        e.preventDefault();
        if(e.target.id == "view-range-modifier-div") {
            for(const desc of this.controller.myPlayerModifiers.viewRangeDesc) {
                let div = document.createElement("div");
                div.id = "view-range-modifier-desc";
                div.classList.add("modifiers-list-item");
                let lbl = document.createElement("label");
                lbl.classList.add("modifiers-desc-lbl");
                lbl.innerHTML = desc;
                div.appendChild(lbl);
                e.target.appendChild(div);
            }
        } else if(e.target.id == "hit-chance-modifier-div") {
            for(const desc of this.controller.myPlayerModifiers.hitChanceDesc) {
                let div = document.createElement("div");
                div.id = "hit-chance-modifier-desc";
                div.classList.add("modifiers-list-item");
                let lbl = document.createElement("label");
                lbl.classList.add("modifiers-desc-lbl");
                lbl.innerHTML = desc;
                div.appendChild(lbl);
                e.target.appendChild(div);
            }
        } else if(e.target.id == "total-ap-modifier-div") {
            for(const desc of this.controller.myPlayerModifiers.totalAPDesc) {
                let div = document.createElement("div");
                div.id = "total-ap-modifier-desc";
                div.classList.add("modifiers-list-item");
                let lbl = document.createElement("label");
                lbl.classList.add("modifiers-desc-lbl");
                lbl.innerHTML = desc;
                div.appendChild(lbl);
                e.target.appendChild(div);
            }
        } else if(e.target.id == "ap-move-cost-modifier-div") {
            for(const desc of this.controller.myPlayerModifiers.apMoveCostDesc) {
                let div = document.createElement("div");
                div.id = "ap-move-cost-modifier-desc";
                div.classList.add("modifiers-list-item");
                let lbl = document.createElement("label");
                lbl.classList.add("modifiers-desc-lbl");
                lbl.innerHTML = desc;
                div.appendChild(lbl);
                e.target.appendChild(div);
            }
        } else if(e.target.id == "dmg-per-move-modifier-div") {
            for(const desc of this.controller.myPlayerModifiers.dmgPerMoveDesc) {
                let div = document.createElement("div");
                div.id = "dmg-per-move-modifier-desc";
                div.classList.add("modifiers-list-item");
                let lbl = document.createElement("label");
                lbl.classList.add("modifiers-desc-lbl");
                lbl.innerHTML = desc;
                div.appendChild(lbl);
                e.target.appendChild(div);
            }
        } else if(e.target.id == "sick-dmg-per-turn-modifier-div") {
            for(const desc of this.controller.myPlayerModifiers.sickDmgPerTurnDesc) {
                let div = document.createElement("div");
                div.id = "sick-dmg-per-turn-modifier-desc";
                div.classList.add("modifiers-list-item");
                let lbl = document.createElement("label");
                lbl.classList.add("modifiers-desc-lbl");
                lbl.innerHTML = desc;
                div.appendChild(lbl);
                e.target.appendChild(div);
            }
        }
    }

    private onMouseLeave = (e) => {
        e.preventDefault();
        this.refreshDisplay();
    }
}
