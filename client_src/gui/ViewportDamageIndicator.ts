import { Controller } from "../Controller"

export class ViewportDamageIndicator {
    private bloodTriangleTopLeft: HTMLDivElement;
    private bloodTriangleTopRight: HTMLDivElement;
    private bloodTriangleBottomLeft: HTMLDivElement;
    private bloodTriangleBottomRight: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.bloodTriangleTopLeft = document.getElementById("blood-triangle-top-left") as HTMLDivElement;
        this.bloodTriangleTopRight = document.getElementById("blood-triangle-top-right") as HTMLDivElement;
        this.bloodTriangleBottomLeft = document.getElementById("blood-triangle-bottom-left") as HTMLDivElement;
        this.bloodTriangleBottomRight = document.getElementById("blood-triangle-bottom-right") as HTMLDivElement;
    }

    public refreshDisplay(totalHPChangedBy: number) {
        const percentage = -totalHPChangedBy*1.5 > 50 ? 50 : -totalHPChangedBy*1.5;
        
        this.bloodTriangleTopLeft.style.visibility = "hidden";
        this.bloodTriangleTopLeft.style.width = `${percentage}%`;
        this.bloodTriangleTopLeft.style.height = `${percentage}%`;
        this.bloodTriangleTopLeft.classList.remove("blood-splat-dissappear");
        void this.bloodTriangleTopLeft.offsetWidth;
        this.bloodTriangleTopLeft.classList.add("blood-splat-dissappear");

        this.bloodTriangleTopRight.style.visibility = "hidden";
        this.bloodTriangleTopRight.style.width = `${percentage}%`;
        this.bloodTriangleTopRight.style.height = `${percentage}%`;
        this.bloodTriangleTopRight.classList.remove("blood-splat-dissappear");
        void this.bloodTriangleTopRight.offsetWidth;
        this.bloodTriangleTopRight.classList.add("blood-splat-dissappear");

        this.bloodTriangleBottomLeft.style.visibility = "hidden";
        this.bloodTriangleBottomLeft.style.width = `${percentage}%`;
        this.bloodTriangleBottomLeft.style.height = `${percentage}%`;
        this.bloodTriangleBottomLeft.classList.remove("blood-splat-dissappear");
        void this.bloodTriangleBottomLeft.offsetWidth;
        this.bloodTriangleBottomLeft.classList.add("blood-splat-dissappear");

        this.bloodTriangleBottomRight.style.visibility = "hidden";
        this.bloodTriangleBottomRight.style.width = `${percentage}%`;
        this.bloodTriangleBottomRight.style.height = `${percentage}%`;
        this.bloodTriangleBottomRight.classList.remove("blood-splat-dissappear");
        void this.bloodTriangleBottomRight.offsetWidth;
        this.bloodTriangleBottomRight.classList.add("blood-splat-dissappear");
    }
}
