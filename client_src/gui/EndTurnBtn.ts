import { Controller } from "../Controller"

export class EndTurnBtn {
    private endTurnBtn: HTMLButtonElement;

    public constructor(private controller: Controller) {
        this.endTurnBtn = document.getElementById("end-turn-btn") as HTMLButtonElement;
        this.endTurnBtn.addEventListener(Controller.CLICK_EVENT, this.onClick);
    }

    public enable() {
        this.endTurnBtn.disabled = false;
    }

    private onClick = (e) => {
        this.controller.endTurn();
        this.endTurnBtn.disabled = true;
    }
}
