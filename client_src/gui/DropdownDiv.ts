import { Controller } from "../Controller"

export class DropdownDiv {
    private dropdownDiv: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.dropdownDiv = document.getElementById("dropdown-div") as HTMLDivElement;
    }

    public hide() {
        this.dropdownDiv.style.visibility = "hidden";
    }

    private show(x: number, y: number) {
        // set the position of the tooltip
        this.dropdownDiv.style.left = `calc(${x}px - ${this.dropdownDiv.clientWidth/2}px)`;
        this.dropdownDiv.style.top = `${y}px`;

        // make sure the tooltip is visible
        this.dropdownDiv.style.visibility = "visible";
    }

    private clear() {
        // clear contents of div
        while(this.dropdownDiv.firstChild) {
            this.dropdownDiv.removeChild(this.dropdownDiv.firstChild);
        }
    }

    public showUseBandageList(x: number, y: number, entityID: number) {
        const hp = this.controller.myPlayerHP;
        if(x >= 0 && y >= 0 && hp) {
            this.clear();

            const $this = this;
            function onClick(limb: string) {
                $this.controller.useBandage(entityID, limb);
            }

            // set the contents of the dropdown
            if(hp.head > 0 && hp.head < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Head";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller.CLICK_EVENT, (e) => { onClick("head"); });
            }
            if(hp.chest > 0 && hp.chest < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Chest";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller.CLICK_EVENT, (e) => { onClick("chest"); });
            }
            if(hp.rarm > 0 && hp.rarm < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Right Arm";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller.CLICK_EVENT, (e) => { onClick("rarm"); });
            }
            if(hp.larm > 0 && hp.larm < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Left Arm";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller.CLICK_EVENT, (e) => { onClick("larm"); });
            }
            if(hp.rleg > 0 && hp.rleg < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Right Leg";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller.CLICK_EVENT, (e) => { onClick("rleg"); });
            }
            if(hp.lleg > 0 && hp.lleg < 100) {
                let btn = document.createElement("button");
                btn.innerHTML = "Bandage Left Leg";
                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller.CLICK_EVENT, (e) => { onClick("lleg"); });
            }

            this.show(x, y);
        } else {
            this.hide();
        }
    }

    public showTileActionList(x: number, y: number, tileX: number, tileY: number,
            canSearch: boolean=false, canAttack: boolean=false, canThrowGrenade: boolean=false) {
        if(x >= 0 && y >= 0) {
            this.clear();

            const $this = this;
            function onAttackClick() {
                const playerIDs = $this.controller.getPlayersOnTile(tileX, tileY);
                if(playerIDs.length === 1) {
                    // If there is only one player on the tile, then attack the player
                    $this.controller.onInitiateAttackPlayer(playerIDs[0]);
                } else if(playerIDs.length > 1) {
                    // If there are more than players on the tile, allow my player to choose
                    $this.showAttackPlayerList(x, y, tileX, tileY);
                }
            }

            // set the contents of the dropdown
            if(canAttack) {
                let btn = document.createElement("button");
                let img = document.createElement("img");
                let span = document.createElement("span");
                img.src = "assets/target_icon.png";
                img.style.display = "inline-block";
                span.innerHTML = "Attack";
                span.style.display = "table-cell";
                btn.appendChild(img);
                btn.appendChild(span);
                this.dropdownDiv.appendChild(btn);
            }

            // set the contents of the dropdown
            if(canSearch) {
                let btn = document.createElement("button");
                let img = document.createElement("img");
                let span = document.createElement("span");
                img.src = "assets/search_icon.png";
                img.style.display = "inline-block";
                img.style.height = "100%";
                span.innerHTML = "Search";
                span.style.display = "inline-block";
                span.style.height = "100%";
                span.style.verticalAlign = "middle";
                btn.appendChild(img);
                btn.appendChild(span);
                this.dropdownDiv.appendChild(btn);
            }

            // set the contents of the tooltip
            /*if(canThrowGrenade) {
                let lbl = document.createElement("button");
                lbl.innerHTML = "Grenade"
                this.tileActionList.appendChild(lbl);
            }*/

            this.show(x, y);
        } else {
            this.hide();
        }
    }

    public showAttackPlayerList(x: number, y: number, tileX: number, tileY: number) {
        if(x >= 0 && y >= 0) {
            this.clear();

            const $this = this;
            function onClick(playerID: string) {
                $this.controller.onInitiateAttackPlayer(playerID);
            }

            const players = this.controller.getPlayersOnTile(tileX, tileY);
            for(const playerID of players) {
                let btn = document.createElement("button");
                const stub = this.controller.playerStubs[playerID];

                if(stub) {
                    btn.innerHTML = stub.Name;
                } else {
                    btn.innerHTML = "Player" + playerID;
                }

                this.dropdownDiv.appendChild(btn);
                btn.addEventListener(Controller.CLICK_EVENT, (e) => { onClick(playerID); });
            }

            this.show(x, y);
        } else {
            this.hide();
        }
    }
}
