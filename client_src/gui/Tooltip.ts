import { Controller } from "../Controller"
import { TileType } from "../game/TileType"
import { GamePhase } from "../game/GamePhase"
import { ActionApCosts } from "../game/Action"
import { ItemMemento } from "../game/ItemMemento"
import { Item, ItemEntity, Gun, Bandage, isGun, isBandage } from "../game/Item"

export class Tooltip {
    private tooltipDiv: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.tooltipDiv = document.getElementById("tooltip-div") as HTMLDivElement;
    }

    public hideTooltip() {
        this.tooltipDiv.style.visibility = "hidden";
    }

    private show(x: number, y: number) {
        // set the position of the tooltip
        this.tooltipDiv.style.left = `calc(${x}px - ${this.tooltipDiv.clientWidth/2}px)`;
        this.tooltipDiv.style.top = `${y+25}px`;

        // make sure the tooltip is visible
        this.tooltipDiv.style.visibility = "visible";
    }

    private clear() {
        // clear contents of div
        while(this.tooltipDiv.firstChild) {
            this.tooltipDiv.removeChild(this.tooltipDiv.firstChild);
        }
    }

    public showDropTileTooltip(i: number, j: number, x: number, y: number) {
        if(i >= 0 && j >= 0) {
            // get the tile type of the tile hovered
            const tileType = TileType.types[this.controller.map.hexGrid[j][i].type];
            this.clear();

            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = tileType.name;
            this.tooltipDiv.appendChild(nameLbl);

            if(tileType.impassable || tileType.undroppable) {
                let lbl = document.createElement("label");
                lbl.innerHTML = "You can't land here";
                this.tooltipDiv.appendChild(lbl);
            } else {
                let lbl = document.createElement("label");
                lbl.innerHTML = "Click to set Drop Location";
                this.tooltipDiv.appendChild(lbl);
            }

            this.show(x, y);
        } else {
            this.hideTooltip();
        }
    }

    public showTileTooltip(i: number, j: number, x: number, y: number) {
        if(i >= 0 && j >= 0) {
            // get the tile type of the tile hovered
            const tileType = TileType.types[this.controller.map.hexGrid[j][i].type];
            this.clear();

            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = tileType.name;
            this.tooltipDiv.appendChild(nameLbl);

            for(const desc of tileType.desc) {
                let descLbl = document.createElement("label");
                descLbl.innerHTML = desc;
                this.tooltipDiv.appendChild(descLbl);
            }

            this.show(x, y);
        } else {
            this.hideTooltip();
        }
    }

    public showActionTooltip(name: string, desc: string, apCost: number, x: number, y: number) {
        if(name != null && desc != null) {
            // clear contents of div
            this.clear();

            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = name;
            nameLbl.style.display = "inline";
            this.tooltipDiv.appendChild(nameLbl);

            let apLbl = document.createElement("label");
            apLbl.innerHTML = `${apCost}AP`;
            apLbl.style.display = "inline";
            apLbl.style.color = "#0000e5";
            this.tooltipDiv.appendChild(apLbl);

            let descLbl = document.createElement("label");
            descLbl.innerHTML = desc;
            this.tooltipDiv.appendChild(descLbl);

            this.show(x, y);
        } else {
            this.hideTooltip();
        }
    }

    public showItemTooltip(entity: ItemEntity, x: number, y: number) {
        if(!entity) {
            return;
        }
        const item = this.controller.items[entity.itemID];
        if(item) {
            // clear contents of div
            this.clear();

            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = item.name;
            nameLbl.style.display = "inline";
            this.tooltipDiv.appendChild(nameLbl);

            // if the item is a gun, include gun specific info
            if(isGun(item)) {
                const gun = item as Gun;

                if((this.controller.myPlayerEquippedItem &&
                        entity.entityID === this.controller.myPlayerEquippedItem.entityID) ||
                        this.controller.gamePhase === GamePhase.ACTION_PHASE) {
                    let equipLbl = document.createElement("label");
                    equipLbl.innerHTML = this.controller.myPlayerEquippedItem &&
                            entity.entityID === this.controller.myPlayerEquippedItem.entityID ?
                            "Equipped" : "Click To Equip "+ActionApCosts.EQUIP_WEAPON+"AP";
                    equipLbl.style.display = "inline";
                    equipLbl.style.color = "#0000e5";
                    this.tooltipDiv.appendChild(equipLbl);
                }

                let maxRangeLbl = document.createElement("label");
                maxRangeLbl.innerHTML = "Max Range <span style='color:#0000e5'>"+gun.maxRange+"</span>";
                maxRangeLbl.style.display = "block";
                this.tooltipDiv.appendChild(maxRangeLbl);

                let accuracyLbl = document.createElement("label");
                accuracyLbl.innerHTML = "Accuracy <span style='color:#0000e5'>"+Math.floor(gun.accuracy*100+0.5)+"%</span>";
                accuracyLbl.style.display = "block";
                this.tooltipDiv.appendChild(accuracyLbl);
            }
            // if the item is a bandage, include bandage specific info
            else if(isBandage(item)) {
                const bandage = item as Bandage;

                let healAmountLbl = document.createElement("label");
                healAmountLbl.innerHTML = "Heal Amount <span style='color:#0000e5'>"+bandage.healAmount+"</span>";
                healAmountLbl.style.display = "block";
                this.tooltipDiv.appendChild(healAmountLbl);
            }

            this.show(x, y);
        } else {
            this.hideTooltip();
        }
    }

    public showItemMementoTooltip(mem: ItemMemento, x: number, y: number) {
        if(mem) {
            this.clear();

            // set the contents of the tooltip
            let nameLbl = document.createElement("label");
            nameLbl.innerHTML = "Last Visited " + mem.since + " Round(s) Ago";
            nameLbl.style.display = "inline";
            this.tooltipDiv.appendChild(nameLbl);

            // add an icon for each item
            for(const item of mem.items) {
                let img = document.createElement("img");
                img.classList.add("item-found-img");
                img.src = item.image;
                img.title = item.name;
                img.style.width = `${this.tooltipDiv.clientWidth/3*item.gridWidth}px`;
                img.style.height = `${this.tooltipDiv.clientWidth/3*item.gridHeight}px`;
                this.tooltipDiv.appendChild(img);
            }

            this.show(x, y);
        } else {
            this.hideTooltip();
        }
    }
}
