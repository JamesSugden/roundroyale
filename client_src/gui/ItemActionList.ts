import { Controller } from "../Controller"

export class ItemActionList {
    private itemActionList: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.itemActionList = document.getElementById("item-action-list") as HTMLDivElement;
    }

    public showActionList(x: number, y: number, canEquip=false) {
        if(x >= 0 && y >= 0) {
            // clear contents of div
            while(this.itemActionList.firstChild) {
                this.itemActionList.removeChild(this.itemActionList.firstChild);
            }

            // set the contents of the tooltip
            if(canEquip) {
                let btn = document.createElement("button");
                let img = document.createElement("img");
                let span = document.createElement("span");
                img.src = "assets/use_icon.png";
                img.style.display = "inline-block";
                span.innerHTML = "Equip";
                span.style.display = "table-cell";
                btn.appendChild(img);
                btn.appendChild(span);
                this.itemActionList.appendChild(btn);
            }

            // set the position of the tooltip
            this.itemActionList.style.left = `calc(${x}px - ${this.itemActionList.clientWidth/2}px)`;
            this.itemActionList.style.top = `${y}px`;

            // make sure the tooltip is visible
            this.itemActionList.style.visibility = "visible";
        } else {
            this.itemActionList.style.visibility = "hidden";
        }
    }
}
