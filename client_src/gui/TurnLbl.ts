import { Controller } from "../Controller"

export class TurnLbl {
    private titleLbl: HTMLLabelElement;
    private instructionsLbl: HTMLLabelElement;

    public constructor(private controller: Controller) {
        this.titleLbl = document.getElementById("title-lbl") as HTMLLabelElement;
        this.instructionsLbl = document.getElementById("instructions-lbl") as HTMLLabelElement;
    }

    public setTitleText(text: string) {
        if(text != null) {
            this.titleLbl.innerHTML = text;
        }
    }

    public setInstructionsText(text: string) {
        if(text != null) {
            this.instructionsLbl.innerHTML = text;
        }
    }
}
