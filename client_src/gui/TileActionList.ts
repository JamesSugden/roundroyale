import { Controller } from "../Controller"

export class TileActionList {
    private tileActionList: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.tileActionList = document.getElementById("tile-action-list") as HTMLDivElement;
    }

    public showActionList(x: number, y: number, canSearch: boolean=false, canAttack: boolean=false, canThrowGrenade: boolean=false) {
        if(x >= 0 && y >= 0) {
            // clear contents of div
            while(this.tileActionList.firstChild) {
                this.tileActionList.removeChild(this.tileActionList.firstChild);
            }

            // set the contents of the tooltip
            if(canAttack) {
                let btn = document.createElement("button");
                let img = document.createElement("img");
                let span = document.createElement("span");
                img.src = "assets/target_icon.png";
                img.style.display = "inline-block";
                span.innerHTML = "Attack";
                span.style.display = "table-cell";
                btn.appendChild(img);
                btn.appendChild(span);
                this.tileActionList.appendChild(btn);
            }

            // set the contents of the tooltip
            if(canSearch) {
                let btn = document.createElement("button");
                let img = document.createElement("img");
                let span = document.createElement("span");
                img.src = "assets/search_icon.png";
                img.style.display = "inline-block";
                img.style.height = "100%";
                span.innerHTML = "Search";
                span.style.display = "inline-block";
                span.style.height = "100%";
                span.style.verticalAlign = "middle";
                btn.appendChild(img);
                btn.appendChild(span);
                this.tileActionList.appendChild(btn);
            }

            // set the contents of the tooltip
            /*if(canThrowGrenade) {
                let lbl = document.createElement("button");
                lbl.innerHTML = "Grenade"
                this.tileActionList.appendChild(lbl);
            }*/

            // set the position of the tooltip
            this.tileActionList.style.left = `calc(${x}px - ${this.tileActionList.clientWidth/2}px)`;
            this.tileActionList.style.top = `${y}px`;

            // make sure the tooltip is visible
            this.tileActionList.style.visibility = "visible";
        } else {
            this.tileActionList.style.visibility = "hidden";
        }
    }
}
