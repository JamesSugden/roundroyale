import { Controller } from "../Controller"

export class ToggleReadyBtn {
    private toggleReadyBtn;

    public constructor(private controller: Controller) {
        this.toggleReadyBtn = document.getElementById("toggle-ready-btn");
        this.toggleReadyBtn.addEventListener(Controller.CLICK_EVENT, this.onClick);
    }

    public update() {
        console.log("update toggle ready btn", this.controller.myPlayerStub)
        if(!this.controller.myPlayerStub.Ready) {
            this.show();
            this.enable();
        } else {
            this.disable();
            this.hide();
        }
    }

    public hide() {
        this.toggleReadyBtn.style.visibility = "hidden";
    }

    public show() {
        this.toggleReadyBtn.style.visibility = "visible";
    }

    public disable() {
        this.toggleReadyBtn.disabled = true;
    }

    public enable() {
        this.toggleReadyBtn.disabled = false;
    }

    public isVisible(): boolean {
        return this.toggleReadyBtn.style.visibility == "visible";
    }

    public onClick = (e) => {
        this.controller.toggleReadyState();
    };
}
