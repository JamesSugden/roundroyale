import { Controller } from "../Controller"
import { GamePhase } from "../game/GamePhase"
import { PlayerDetails } from "../game/PlayerDetails"
import { Action, ActionAttack, ActionEquipWeapon, ActionApCosts } from "../game/Action"
import { ItemEntity } from "../game/Item"
import { ItemMemento } from "../game/ItemMemento"
import { MapCanvas } from "./MapCanvas"
import { PlayerList } from "./PlayerList"
import { ToggleReadyBtn } from "./ToggleReadyBtn"
import { TurnLbl } from "./TurnLbl"
import { ItemsFoundDiv } from "./ItemsFoundDiv"
import { InventoryPanel } from "./InventoryPanel"
import { ModifiersList } from "./ModifiersList"
import { Tooltip } from "./Tooltip"
import { ActionList } from "./ActionList"
import { ChatWindow } from "./ChatWindow"
import { ItemActionList } from "./ItemActionList"
import { AttackCharDiv } from "./AttackCharDiv"
import { CharacterPanel } from "./CharacterPanel"
import { CenterLbl } from "./CenterLbl"
import { ViewportDamageIndicator } from "./ViewportDamageIndicator"
import { EndTurnBtn } from "./EndTurnBtn"
import { DropdownDiv} from "./DropdownDiv"
import { FullOverlay } from "./FullOverlay"
import { RightPanelMobileDiv } from "./RightPanelMobileDiv"

export class GUI {
    private _mapCanvas: MapCanvas;
    private _playerList: PlayerList;
    private _toggleReadyBtn: ToggleReadyBtn;
    private _turnLbl: TurnLbl;
    private _itemsFoundDiv: ItemsFoundDiv;
    private _inventPanel: InventoryPanel;
    private _modifiersList: ModifiersList;
    private _tooltip: Tooltip;
    private _actionList: ActionList;
    private _chatWindow: ChatWindow;
    private _itemActionList: ItemActionList;
    private _attackCharDiv: AttackCharDiv;
    private _characterPanel: CharacterPanel;
    private _centerLbl: CenterLbl;
    private _viewportDamageIndicator: ViewportDamageIndicator;
    private _endTurnBtn: EndTurnBtn;
    private _dropdownDiv: DropdownDiv;
    private _fullOverlay: FullOverlay;
    private _rightPanelMobileDiv: RightPanelMobileDiv;

    private constructor(private controller: Controller) {
        this._tooltip = new Tooltip(controller);
        this._mapCanvas = new MapCanvas(controller);
        this._playerList = new PlayerList(controller);
        this._toggleReadyBtn = new ToggleReadyBtn(controller);
        this._turnLbl = new TurnLbl(controller);
        this._itemsFoundDiv = new ItemsFoundDiv(controller);
        this._inventPanel = new InventoryPanel(controller);
        this._modifiersList = new ModifiersList(controller);
        this._actionList = new ActionList(controller);
        this._chatWindow = new ChatWindow(controller);
        this._itemActionList = new ItemActionList(controller);
        this._attackCharDiv = new AttackCharDiv(controller);
        this._characterPanel = new CharacterPanel(controller);
        this._centerLbl = new CenterLbl(controller);
        this._viewportDamageIndicator = new ViewportDamageIndicator(controller);
        this._endTurnBtn = new EndTurnBtn(controller);
        this._dropdownDiv = new DropdownDiv(controller);
        this._fullOverlay = new FullOverlay(controller);
        this._rightPanelMobileDiv = new RightPanelMobileDiv(controller);
    }

    public static new(controller: Controller): GUI {
        if(controller) {
            return new GUI(controller);
        }
        return null;
    }

    // Request that the map be redrawn
    public requestDrawMap() {
        this._mapCanvas.drawMap();
    }

    // Request that the movement of players be animated on the map
    public requestAnimateMovement(oldPlayerDetails: { [id: number]: PlayerDetails }, callback: (() => void)) {
        this._mapCanvas.animateMovement(oldPlayerDetails, callback);
    }

    // Request that the full overlay be closed
    public requestCloseFullOverlay() {
        this._fullOverlay.hide();
    }

    // Update timer label
    public updateTimerLabel(text: string, timeLeft: number) {
        const txt = text + (timeLeft > 0 ? " " + timeLeft : "");
        this._turnLbl.setTitleText(txt);
    }

    // Called when the drop phase has begun
    public onDropPhaseStart() {
        this._endTurnBtn.enable();
        this._centerLbl.show("Drop Phase", true);
        this._turnLbl.setInstructionsText("Choose a Drop Location");
    }

    // Called when a move phase has begun
    public onMovePhaseStart() {
        this._endTurnBtn.enable();
        this._centerLbl.show("Move Phase", true);
        this._actionList.resetActionList();
        this._attackCharDiv.hide();
        this._actionList.appendAction({ name: "Crouch", apCost: ActionApCosts.CROUCH }, null, null);
        this._turnLbl.setInstructionsText(this.controller.spectating ?
            "Spectating "+this.controller.myPlayerStub.Name : "Move or Crouch");
        this._dropdownDiv.hide();
        this._mapCanvas.onMovePhaseStart();
    }

    // Called when an action phase has begun
    public onActionPhaseStart() {
        this._endTurnBtn.enable();
        this._centerLbl.show("Action Phase", true);
        this._actionList.resetActionList();
        this._turnLbl.setInstructionsText(this.controller.spectating ?
            "Spectating "+this.controller.myPlayerStub.Name : "Perform up to 5 Actions");
        this._tooltip.hideTooltip();
        this._mapCanvas.onActionPhaseStart();
    }

    // Set the current item found
    public setItemsFound(items: Array<{ entityID: string, itemID: string }>) {
        this._itemsFoundDiv.setItemsFound(items);
    }

    // Called when the game is paused whilst waiting for server
    public onWaitingForServer() {
        this._centerLbl.show("Waiting for Server <div class='spinner'></div>");
        this._mapCanvas.onWaitingForServer();
    }

    // Called when the map is fully loaded and initialised
    public onMapLoaded() {
        this._mapCanvas.onMapLoaded();
        this._toggleReadyBtn.enable();
    }

    // Called when the player stubs map is updated
    public onPlayerStubsUpdated() {
        this._playerList.refreshDisplay();
        //this._toggleReadyBtn.update();
    }

    // Called when the player details map is updated
    public onMyPlayerDetailsUpdated() {
        this._mapCanvas.onMyPlayerDetailsUpdated();
    }

    // On player modifiers update
    public onMyPlayerModifiersUpdated() {
        this._modifiersList.refreshDisplay();
    }

    // On player hp update
    public onMyPlayerHPUpdated(totalHPChangedBy: number, headInjured: boolean, chestInjured: boolean,
            rarmInjured: boolean, larmInjured: boolean, rlegInjured: boolean,
            llegInjured: boolean) {
        this._characterPanel.refreshDisplay();
        if(totalHPChangedBy < 0) {
            this._viewportDamageIndicator.refreshDisplay(totalHPChangedBy);
        }
        if(headInjured) {
            this._characterPanel.showBloodSplatHead();
        }
        if(chestInjured) {
            this._characterPanel.showBloodSplatChest();
        }
        if(rarmInjured) {
            this._characterPanel.showBloodSplatRArm();
        }
        if(larmInjured) {
            this._characterPanel.showBloodSplatLArm();
        }
        if(rlegInjured) {
            this._characterPanel.showBloodSplatRLeg();
        }
        if(llegInjured) {
            this._characterPanel.showBloodSplatLLeg();
        }
    }

    // Called when a move has been deemed valid
    public onMoveValid(newX: number, newY: number, crouch: boolean) {
        this._actionList.resetActionList();
        if(crouch) {
            this._actionList.appendAction({ name: "Crouch", apCost: ActionApCosts.CROUCH }, this.controller.globalMouseX, this.controller.globalMouseY);
        } else {
            this._actionList.appendAction({ name: "Move", apCost: (ActionApCosts.MOVE +
                (this.controller.myPlayerModifiers.apMoveCost != null ? this.controller.myPlayerModifiers.apMoveCost : 0)) }, null, null);
        }
        this._mapCanvas.onMoveValid(newX, newY);
    }

    // Called when an attack has been deemed valid
    public onAttackValid(action: ActionAttack) {
        this._actionList.appendAction(action, null, null);
        this._mapCanvas.onAttackValid(action.playerID);
    }

    // Called when a search has been deemed valid
    public onSearchValid(action: Action) {
        this._actionList.appendAction(action, null, null);
    }

    // Called when the server has validified a take item request
    public onTakeItemValid(entityID: number, itemID: number, firstSlotX: number, firstSlotY: number) {
        this._inventPanel.onItemTaken(entityID, itemID, firstSlotX, firstSlotY);
        this._itemsFoundDiv.removeItem(entityID, itemID);
    }

    // Called when the server has validified an equip item request
    public onEquipItemValid(action: ActionEquipWeapon) {
        this._actionList.appendAction(action, null, null);
        this._inventPanel.resetSlotStyle();
    }

    // Called when the server has validifed a drop item request
    public onDropItemValid(entityID: number, itemID: number) {
        this._itemsFoundDiv.addItem(entityID, itemID);
        this._inventPanel.removeFromBackpack(entityID, itemID);
    }

    // Called when the server has validified a use bandage request
    public onUseBandageValid(entityID: number, itemID: number) {
        this._inventPanel.removeFromBackpack(entityID, itemID);
        this._actionList.appendAction({ name: "Bandage", apCost: ActionApCosts.BANDAGE }, null, null);
    }

    // Called when a players ready state is changed
    public onReadyStateUpdate(playerID: string, readyState: boolean) {
        this._playerList.updateListItem(playerID, readyState);
        //this._toggleReadyBtn.update();
    }

    // Called when the game leaves the lobby phase and enters the drop phase
    public onGameStarted() {
        this._toggleReadyBtn.disable();
        this._toggleReadyBtn.hide();
    }

    // Called at end of turn if players have died
    public onPlayerDead(playerName: string) {
        this._chatWindow.appendMsg(`<b><span style="color: #0000e5;">${playerName}</span></b> has died`);
    }

    // Called along with onPlayerDead if the player is myPlayer
    public onMyPlayerDead() {
        // Show you died message and hide all gameplay only content
        this._centerLbl.show("You Died");
        this._rightPanelMobileDiv.hide();
        this._mapCanvas.disable();
        this._actionList.hide();
    }

    // Called when the game is over
    public onGameOver(winnerIDs: Array<number>, didMyPlayerWin: boolean, didMyPlayerDraw: boolean) {
        // Display msg specific to my player
        if(didMyPlayerDraw) {
            this._centerLbl.show("Draw");
        } else if(didMyPlayerWin) {
            this._centerLbl.show("Victory");
        } else {
            this._centerLbl.show("Defeat");
        }

        // Display who won in the kill feed
        let winMsg = "";
        for(const winnerID of winnerIDs) {
            const playerName = this.controller.playerStubs[winnerID] ?
                this.controller.playerStubs[winnerID].Name : "Player" + winnerID;
            winMsg += `<b><span style="color: #0000e5;">${playerName}</span></b>, `;
        }
        winMsg = winMsg.slice(0, -2);
        winMsg += (winnerIDs.length === 1 ? " is" : " are") + " victorious";
        this._chatWindow.appendMsg(winMsg);
    }

    // Called when a storm approach update is received from server
    public onStormApproachUpdate() {
        this._centerLbl.show("Nuclear Meltdown Imminent");
    }

    // Called when a storm radius update is received from server
    public onStormRadiusUpdate() {
        this.controller.requestDrawMap();
    }

    // Called when a storm dmg update is received from server
    public onStormDmgUpdate() {

    }

    // Called when the player starts dragging an item in their inventory
    public onItemDragStart() {
        this._itemsFoundDiv.enableDropMode();
    }

    // Called when the player stops dragging an item in their inventory
    public onItemDragEnd() {
        this._itemsFoundDiv.disableDropMode();
        this._inventPanel.onItemDragEnd();
    }

    // Called when the player begins hovering over a new tile during the drop phase
    public onTileHoverDropPhase(i: number, j: number, x: number, y: number) {
        this._tooltip.showDropTileTooltip(i, j, x, y);
    }

    // Called when the player begins hovering over a new tile during the move phase
    public onTileHoverMovePhase(i: number, j: number, x: number, y: number) {
        this._tooltip.showTileTooltip(i, j, x, y);
    }

    // Called when the player begins hovering over a new tile during the action phase
    public onTileHoverActionPhase(name: string, desc: string, apCost: number, x: number, y: number) {
        this._tooltip.showActionTooltip(name, desc, apCost, x, y);
    }

    // Called when the player clicks a tile during the action phase
    public onTileClickedActionPhase(x: number, y: number, tileX: number,
            tileY: number, canSearch: boolean, canAttack: boolean) {
        this._dropdownDiv.showTileActionList(x, y, tileX, tileY, canSearch, canAttack, true);
        this._tooltip.hideTooltip();
    }

    // Called when the player hovers an item memento
    public onItemMementoHover(mem: ItemMemento, x: number, y: number) {
        this._tooltip.showItemMementoTooltip(mem, x, y);
    }

    // Called when the player wants to attack a player on a tile
    public onAttackPlayerOnTile(x: number, y: number, tileX: number, tileY: number) {
        this._dropdownDiv.showAttackPlayerList(x, y, tileX, tileY);
    }

    // Called when the player begins hovering over an item
    public onItemHover(entity: ItemEntity, x: number, y: number) {
        this._tooltip.showItemTooltip(entity, x, y);
    }

    // Called when the player left or right clicks a bandage in their inventory
    public onBandageClick(x: number, y: number, entityID: number) {
        this._dropdownDiv.showUseBandageList(x, y, entityID);
    }

    // Called when the play clicks a limb button
    public onHealLimbClick(limb: string) {
        // Ensure player has bandage in their inventory
        if(this._inventPanel.containsBandage()) {
            this._fullOverlay.show();
            this._inventPanel.highlightBandages(limb);
        }
    }

    // Called when the player clicks the full screen overlay
    public onFullOverlayClick() {
        this._inventPanel.unHighlightItems();
    }

    // Called when the player initiates an attack on another player
    public onInitiateAttackPlayer(playerID: string) {
        this._attackCharDiv.refreshDisplay(playerID);
    }

    // Called when a player disconnects from the game
    public onPlayerDisconnect(playerName: string) {
        this._playerList.refreshDisplay();
        this._chatWindow.appendMsg(`<b><span style="color: #0000e5;">${playerName}</span></b> has disconnected`);
    }

    // Called upon receiving a msg from another player
    public onMsgReceived(playerName: string, msg: string) {
        this._chatWindow.appendMsg(`<b><span style="color: #0000e5;">${playerName}:</span></b> ${msg}`);
    }

    public onWindowResize() {
        this._mapCanvas.resize();
    }
}
