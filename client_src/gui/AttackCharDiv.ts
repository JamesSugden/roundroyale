import { Controller } from "../Controller"
import { Item, Gun } from "../game/Item"

export class AttackCharDiv {
    // HTML Elements
    private attackCharDiv: HTMLDivElement;
    private attackCharLbl: HTMLLabelElement;
    private attackCharCancelBtn: HTMLButtonElement;
    private attackHeadBtn: HTMLButtonElement;
    private attackChestBtn: HTMLButtonElement;
    private attackRArmBtn: HTMLButtonElement;
    private attackLArmBtn: HTMLButtonElement;
    private attackRLegBtn: HTMLButtonElement;
    private attackLLegBtn: HTMLButtonElement;
    private attackHeadHPBar: HTMLSpanElement;
    private attackChestHPBar: HTMLSpanElement;
    private attackRArmHPBar: HTMLSpanElement;
    private attackLArmHPBar: HTMLSpanElement;
    private attackRLegHPBar: HTMLSpanElement;
    private attackLLegHPBar: HTMLSpanElement;
    private attackHeadHPLbl: HTMLLabelElement;
    private attackChestHPLbl: HTMLLabelElement;
    private attackRArmHPLbl: HTMLLabelElement;
    private attackLArmHPLbl: HTMLLabelElement;
    private attackRLegHPLbl: HTMLLabelElement;
    private attackLLegHPLbl: HTMLLabelElement;
    private attackHeadHitChanceLbl: HTMLLabelElement;
    private attackChestHitChanceLbl: HTMLLabelElement;
    private attackRArmHitChanceLbl: HTMLLabelElement;
    private attackLArmHitChanceLbl: HTMLLabelElement;
    private attackRLegHitChanceLbl: HTMLLabelElement;
    private attackLLegHitChanceLbl: HTMLLabelElement;

    // Internal State
    // ID of player being targeted
    private playerID: string;

    public constructor(private controller: Controller) {
        this.attackCharDiv = document.getElementById("attack-char-div") as HTMLDivElement;
        this.attackCharLbl = document.getElementById("attack-char-lbl") as HTMLLabelElement;
        this.attackCharCancelBtn = document.getElementById("attack-char-cancel-btn") as HTMLButtonElement;

        this.attackHeadBtn = document.getElementById("attack-head-btn") as HTMLButtonElement;
        this.attackChestBtn = document.getElementById("attack-chest-btn") as HTMLButtonElement;
        this.attackRArmBtn = document.getElementById("attack-rarm-btn") as HTMLButtonElement;
        this.attackLArmBtn = document.getElementById("attack-larm-btn") as HTMLButtonElement;
        this.attackRLegBtn = document.getElementById("attack-rleg-btn") as HTMLButtonElement;
        this.attackLLegBtn = document.getElementById("attack-lleg-btn") as HTMLButtonElement;

        this.attackHeadHPBar = document.getElementById("attack-head-hp-bar") as HTMLSpanElement;
        this.attackChestHPBar = document.getElementById("attack-chest-hp-bar") as HTMLSpanElement;
        this.attackRArmHPBar = document.getElementById("attack-rarm-hp-bar") as HTMLSpanElement;
        this.attackLArmHPBar = document.getElementById("attack-larm-hp-bar") as HTMLSpanElement;
        this.attackRLegHPBar = document.getElementById("attack-rleg-hp-bar") as HTMLSpanElement;
        this.attackLLegHPBar = document.getElementById("attack-lleg-hp-bar") as HTMLSpanElement;

        this.attackHeadHPLbl = document.getElementById("attack-head-hp-lbl") as HTMLLabelElement;
        this.attackChestHPLbl = document.getElementById("attack-chest-hp-lbl") as HTMLLabelElement;
        this.attackRArmHPLbl = document.getElementById("attack-rarm-hp-lbl") as HTMLLabelElement;
        this.attackLArmHPLbl = document.getElementById("attack-larm-hp-lbl") as HTMLLabelElement;
        this.attackRLegHPLbl = document.getElementById("attack-rleg-hp-lbl") as HTMLLabelElement;
        this.attackLLegHPLbl = document.getElementById("attack-lleg-hp-lbl") as HTMLLabelElement;

        this.attackHeadHitChanceLbl = document.getElementById("attack-head-hit-chance-lbl") as HTMLLabelElement;
        this.attackChestHitChanceLbl = document.getElementById("attack-chest-hit-chance-lbl") as HTMLLabelElement;
        this.attackRArmHitChanceLbl = document.getElementById("attack-rarm-hit-chance-lbl") as HTMLLabelElement;
        this.attackLArmHitChanceLbl = document.getElementById("attack-larm-hit-chance-lbl") as HTMLLabelElement;
        this.attackRLegHitChanceLbl = document.getElementById("attack-rleg-hit-chance-lbl") as HTMLLabelElement;
        this.attackLLegHitChanceLbl = document.getElementById("attack-lleg-hit-chance-lbl") as HTMLLabelElement;

        this.attackCharCancelBtn.addEventListener("click", this.onCancelBtnClick);

        this.attackHeadBtn.addEventListener("click", this.onAttackHeadBtnClick);
        this.attackChestBtn.addEventListener("click", this.onAttackChestBtnClick);
        this.attackRArmBtn.addEventListener("click", this.onAttackRArmBtnClick);
        this.attackLArmBtn.addEventListener("click", this.onAttackLArmBtnClick);
        this.attackRLegBtn.addEventListener("click", this.onAttackRLegBtnClick);
        this.attackLLegBtn.addEventListener("click", this.onAttackLLegBtnClick);
    }

    private show() {
        this.attackCharDiv.style.visibility = "visible";
    }

    public hide() {
        this.attackCharDiv.style.visibility = "hidden";
    }

    public refreshDisplay(playerID: string) {
        this.playerID = playerID;
        this.calcHitChances();
        this.show();
    }

    private calcHitChances() {
        const p = this.controller.myPlayerDetails;
        const target = this.controller.playerDetails[this.playerID];
        // TODO - this range doesn't really work with the hexgrid system
        const rangeX = Math.abs(p.x - target.x);
        const rangeY = Math.abs(p.y - target.y);
        const rangeD = Math.sqrt(rangeX*rangeX + rangeY*rangeY) + 1;

        const e = this.controller.myPlayerEquippedItem;
        let dmgPerHit;
        let shotsPerAction;
        let accuracy;

        let weaponEquipped = false;
        if(e != null) {
            const gun = this.controller.items[e.itemID] as Gun;
            if(gun) {
                // If a weapon is equipped, use weapon stats
                dmgPerHit = gun.dmgPerHit;
                shotsPerAction = gun.shotsPerAction;
                accuracy = gun.accuracy;
                weaponEquipped = true;
            }
        }

        if(!weaponEquipped) {
            // If no weapon equipped, simulate punching
            dmgPerHit = 5;
            shotsPerAction = 1;
            accuracy = 0.6;
        }

        // Calculate the hit chance as a whole numbered percentage
        const hitChance = Math.floor(100 / rangeD * accuracy * (1 + this.controller.myPlayerModifiers.hitChance/100) + 0.5);

        // Display the hit chance below the health bars
        this.attackHeadHitChanceLbl.innerHTML = ""+hitChance+"% To Hit";
        this.attackChestHitChanceLbl.innerHTML = ""+hitChance+"% To Hit";
        this.attackRArmHitChanceLbl.innerHTML = ""+hitChance+"% To Hit";
        this.attackLArmHitChanceLbl.innerHTML = ""+hitChance+"% To Hit";
        this.attackRLegHitChanceLbl.innerHTML = ""+hitChance+"% To Hit";
        this.attackLLegHitChanceLbl.innerHTML = ""+hitChance+"% To Hit";

        // Display the player's name in the title label
        this.attackCharLbl.innerHTML = "Attack " + (this.controller.playerStubs[this.playerID] ?
                    this.controller.playerStubs[this.playerID].Name : "Player" + this.playerID);

        // Set value of HP as percentage width of span
        const hp = this.controller.playerHPs[this.playerID]
        if(hp) {
            this.attackHeadHPBar.style.width = `${hp.head < 0 ? 0 : hp.head}%`;
            this.attackChestHPBar.style.width = `${hp.chest < 0 ? 0 : hp.chest}%`;
            this.attackRArmHPBar.style.width = `${hp.rarm < 0 ? 0 : hp.rarm}%`;
            this.attackLArmHPBar.style.width = `${hp.larm < 0 ? 0 : hp.larm}%`;
            this.attackRLegHPBar.style.width = `${hp.rleg < 0 ? 0 : hp.rleg}%`;
            this.attackLLegHPBar.style.width = `${hp.lleg < 0 ? 0 : hp.lleg}%`;

            // Set label of hp bar to be the value of hp
            this.attackHeadHPLbl.innerHTML = "" + (hp.head < 0 ? 0 : hp.head);
            this.attackChestHPLbl.innerHTML = "" + (hp.chest < 0 ? 0 : hp.chest);
            this.attackRArmHPLbl.innerHTML = "" + (hp.rarm < 0 ? 0 : hp.rarm);
            this.attackLArmHPLbl.innerHTML = "" + (hp.larm < 0 ? 0 : hp.larm);
            this.attackRLegHPLbl.innerHTML = "" + (hp.rleg < 0 ? 0 : hp.rleg);
            this.attackLLegHPLbl.innerHTML = "" + (hp.lleg < 0 ? 0 : hp.lleg);

            if(hp.head <= 30) {
                this.attackHeadHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.attackHeadHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(hp.chest <= 30) {
                this.attackChestHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.attackChestHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(hp.rarm <= 30) {
                this.attackRArmHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.attackRArmHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(hp.larm <= 30) {
                this.attackLArmHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.attackLArmHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(hp.rleg <= 30) {
                this.attackRLegHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.attackRLegHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(hp.lleg <= 30) {
                this.attackLLegHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.attackLLegHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
        }
    }

    public onCancelBtnClick = (e) => {
        this.hide();
    }

    public onAttackHeadBtnClick = (e) => {
        this.controller.attackPlayer(this.playerID, "head");
        this.hide();
    }

    public onAttackChestBtnClick = (e) => {
        this.controller.attackPlayer(this.playerID, "chest");
        this.hide();
    }

    public onAttackRArmBtnClick = (e) => {
        this.controller.attackPlayer(this.playerID, "rarm");
        this.hide();
    }

    public onAttackLArmBtnClick = (e) => {
        this.controller.attackPlayer(this.playerID, "larm");
        this.hide();
    }

    public onAttackRLegBtnClick = (e) => {
        this.controller.attackPlayer(this.playerID, "rleg");
        this.hide();
    }

    public onAttackLLegBtnClick = (e) => {
        this.controller.attackPlayer(this.playerID, "lleg");
        this.hide();
    }
}
