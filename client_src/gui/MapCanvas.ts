import { Controller } from "../Controller"
import { GamePhase } from "../game/GamePhase"
import { TileType } from "../game/TileType"
import { Item, Gun } from "../game/Item"
import { ItemMemento } from "../game/ItemMemento"
import { ActionApCosts } from "../game/Action"
import { PlayerDetails } from "../game/PlayerDetails"
import { MapCanvasArrow } from "./MapCanvasArrow"

export class MapCanvas {
    // HTML elements
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;
    private hitCanvas: HTMLCanvasElement;
    private hitCtx: CanvasRenderingContext2D;

    // Determines where the focus of the zoom and how far zoomed in the map is
    private zoomTargetX: number;
    private zoomTargetY: number;
    private zoomLevel: number = 1.0;

    // The total width and height of the hexagon grid (in pixels)
    private totalMapWidth: number;
    private totalMapHeight: number;

    // The mouse position relative to canvas set on last mouseMove or onClick call
    private lastMouseX: number;
    private lastMouseY: number;
    // The mouse position relative to the dom origin
    private lastGlobalMouseX: number;
    private lastGlobalMouseY: number;

    // The hexagon which has been clicked by the user's mouse
    private hexSelectedX: number;
    private hexSelectedY: number;

    // The hexagon which is being hovered over by the user's mouse
    private hexHoverX: number;
    private hexHoverY: number;
    private hexHoverValid: boolean;

    // All information needed to draw a hexagon
    // Sourced from https://gist.github.com/cDecker32/2500868
    private hexagonAngle = 0.523598776; // 30 degrees in radians
    private sideLength = 72;
    private hexHeight: number = Math.sin(this.hexagonAngle) * this.sideLength;
    private hexRadius: number = Math.cos(this.hexagonAngle) * this.sideLength;
    private hexRectangleHeight: number  = this.sideLength + 2 * this.hexHeight;
    private hexRectangleWidth: number  = 2 * this.hexRadius;

    // The player image to be drawn wherever there's a player
    // TODO - include a player image in the player's stub
    private playerImage = new Image();

    // List of arrows to draw on the GUI
    private displayArrows: Array<MapCanvasArrow>;

    // Initialise the MapCanvas object
    public constructor(private controller: Controller) {
        this.canvas = document.getElementById("map-canvas") as HTMLCanvasElement;
        this.hitCanvas = document.getElementById("hit-canvas") as HTMLCanvasElement;
        this.ctx = this.canvas.getContext("2d");
        this.hitCtx = this.hitCanvas.getContext("2d");
        this.resize();

        // Load the player image
        this.playerImage.src = "assets/Stickman 1.png";

        // Initialise the list of arrows
        this.displayArrows = new Array<MapCanvasArrow>();
    }

    // Disable after my player dies to disallow mouse input
    public disable() {
        this.canvas.style.pointerEvents = "none";
    }

    public animateMovement(oldPlayerDetails: { [id: number]: PlayerDetails }, callback: (() => void)) {
        const newPlayerDetails = this.controller.playerDetails;
        let interpolatedDetails = {};
        for(const playerID in newPlayerDetails) {
            interpolatedDetails[playerID] = Object.assign({}, newPlayerDetails[playerID]);
        }
        const animDuration = 1400;

        this.controller.startTimer(animDuration/1000, (timeLeft, timeoutID) => {
            // Every interval seconds, redraw map with interpolated player co-ordinates
            for(const playerID in newPlayerDetails) {
                const newDetails = newPlayerDetails[playerID];
                let slerpX: number = newDetails.x;
                let slerpY: number = newDetails.y;
                if(oldPlayerDetails[playerID]) {
                    const slerpFactor = 1 - (timeLeft/animDuration);
                    const oldDetails = oldPlayerDetails[playerID];
                    const dy = oldDetails.y - newDetails.y;
                    let oldX = oldDetails.x;
                    let newX = newDetails.x;

                    if(oldDetails.y % 2 === 0 && dy != 0) {
                        oldX -= 1;
                    }

                    if(dy === -1) {
                        if(oldDetails.y % 2 === 0) {
                            oldX += 1;
                        } else {
                            newX -= 1;
                        }
                    }

                    slerpX = (oldX) +
                        (slerpFactor*(newX-oldX));
                    slerpY = (oldDetails.y) +
                        (slerpFactor*(newDetails.y-oldDetails.y));
                }
                interpolatedDetails[playerID].x = slerpX;
                interpolatedDetails[playerID].y = slerpY;
            }
            this.drawMap(interpolatedDetails);
        }, callback, false, 40);
    }

    public drawMap(playerDetails: { [id: number]: PlayerDetails }=this.controller.playerDetails) {
        if(!this.controller.map) {
            return;
        }
        const hexGrid = this.controller.map.hexGrid;
        if(!hexGrid) {
            return;
        }

        this.ctx.fillStyle = TileType.types[0].color;
        //ctx.clearRect(0, 0, canvas.width, canvas.height);
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.hitCtx.clearRect(0, 0, this.hitCanvas.width, this.hitCanvas.height);

        this.ctx.save();
        this.ctx.translate(this.zoomTargetX, this.zoomTargetY);
        this.ctx.scale(this.zoomLevel, this.zoomLevel);
        this.hitCtx.save();
        this.hitCtx.translate(this.zoomTargetX, this.zoomTargetY);
        this.hitCtx.scale(this.zoomLevel, this.zoomLevel);

        // get a reference to my player
        const myDetails = this.controller.myPlayerDetails;

        // draw the hex grid
        for(var j = 0; j < hexGrid.length; j++) {
            for(var i = 0; i < hexGrid[j].length; i++) {
                this.drawTile(i, j, hexGrid, true);
            }
        }

        if(myDetails) {
            // draw a shadow over the entire map
            this.ctx.globalAlpha = 0.6;
            this.ctx.fillStyle = "#000";
            this.ctx.fillRect(-this.zoomTargetX/this.zoomLevel, -this.zoomTargetY/this.zoomLevel, this.canvas.width/this.zoomLevel, this.canvas.height/this.zoomLevel);
            this.ctx.globalAlpha = 1.0;
            // determine the which tiles to draw the outline around
            let range = 1;  // move range
            if(this.controller.gamePhase === GamePhase.ACTION_PHASE) {
                const item = this.controller.myPlayerEquippedItem ?
                    this.controller.items[this.controller.myPlayerEquippedItem.itemID] : null;
                const searchRange = 1;
                const attackRange = item ? (item as Gun).maxRange : 0;
                range = Math.max(searchRange, attackRange);
            }
            // draw an outline around the movable tiles
            const viewRange = Math.max(2 + this.controller.myPlayerModifiers.viewRange, 0);
            // actionable range must be <= view range
            range = Math.min(range, viewRange);
            const gap = viewRange-range;
            this.drawOuterTilesInViewRange(myDetails, hexGrid, gap);
            this.drawTileOutlines(myDetails, range, hexGrid);
            // redraw the parts of the map in the players vision
            this.drawInnerTilesInViewRange(myDetails, hexGrid, gap);
        }

        // draw the tile selected circle
        if(this.hexSelectedX >= 0 && this.hexSelectedY >= 0) {
            var x = this.hexSelectedX * this.hexRectangleWidth + (this.hexRectangleWidth/2 * (this.hexSelectedY % 2)) + this.hexRectangleWidth/2;
            var y = this.hexSelectedY * this.hexRectangleHeight - (this.sideLength/2 * this.hexSelectedY) + this.hexRectangleHeight/2;
            this.ctx.save();
            this.ctx.strokeStyle = "#ADFF2F";
            this.ctx.shadowColor = "black";
            this.ctx.shadowBlur = 15;
            this.ctx.shadowOffsetX = 0;
            this.ctx.shadowOffsetY = 0;
            this.ctx.lineWidth = 2 / this.zoomLevel;
            this.drawCircle(this.ctx, x, y, this.hexRadius);
            this.ctx.stroke();
            this.ctx.restore();
        }

        // draw the mouse hover circle & the hover tile tooltip
        if(this.hexHoverX >= 0 && this.hexHoverY >= 0) {
            var x = this.hexHoverX * this.hexRectangleWidth + (this.hexRectangleWidth/2 * (this.hexHoverY % 2)) + this.hexRectangleWidth/2;
            var y = this.hexHoverY * this.hexRectangleHeight - (this.sideLength/2 * this.hexHoverY) + this.hexRectangleHeight/2;
            this.ctx.save();
            this.ctx.strokeStyle = this.hexHoverValid ? "#3498db" : "#e74c3c";
            this.ctx.shadowColor = "black";
            this.ctx.shadowBlur = 15;
            this.ctx.shadowOffsetX = 0;
            this.ctx.shadowOffsetY = 0;
            this.ctx.lineWidth = 2 / this.zoomLevel;
            this.drawCircle(this.ctx, x, y, this.hexRadius);
            this.ctx.stroke();
            this.ctx.restore();
        }

        // draw all the players my player knows the location of
        for(const playerID in playerDetails) {
            const details = playerDetails[playerID];
            if(details) {
                const moduloMultiplier = details.y % 2;
                const x = details.x * this.hexRectangleWidth + (this.hexRectangleWidth/2 * moduloMultiplier) + this.hexRectangleWidth/2;
                const y = details.y * this.hexRectangleHeight - (this.sideLength/2 * details.y) + this.hexRectangleHeight/2;
                const imgWidth = this.hexRectangleHeight * 0.75 * 1080 / 1920;
                const imgHeight = this.hexRectangleHeight * 0.75;
                this.ctx.drawImage(this.playerImage, x-imgWidth/2, y-imgHeight/2, imgWidth, imgHeight);
            }
        }

        // draw all the display arrows
        for(const arrow of this.displayArrows) {
            const x1 = arrow.fromX * this.hexRectangleWidth + (this.hexRectangleWidth/2 * (arrow.fromY % 2)) + this.hexRectangleWidth/2;
            const y1 = arrow.fromY * this.hexRectangleHeight - (this.sideLength/2 * arrow.fromY) + this.hexRectangleHeight/2;
            const x4 = arrow.toX * this.hexRectangleWidth + (this.hexRectangleWidth/2 * (arrow.toY % 2)) + this.hexRectangleWidth/2;
            const y4 = arrow.toY * this.hexRectangleHeight - (this.sideLength/2 * arrow.toY) + this.hexRectangleHeight/2;

            this.ctx.strokeStyle = "#000";
            this.ctx.beginPath();
            this.ctx.moveTo(x1, y1);
            this.ctx.lineTo(x4, y4);
            this.ctx.stroke();
        }

        if(myDetails) {
            // draw all item memento bubbles
            for(const m of this.controller.itemMementos) {
                if(!(m.mapX === myDetails.x && m.mapY === myDetails.y)) {
                    // draw the item info bubble
                    this.drawItemBubble(m);
                }
            }
        }

        this.ctx.restore();
        this.hitCtx.restore();
    }

    private drawTile(i, j, hexGrid, drawToHit=false) {
        if(j < 0 || j >= hexGrid.length || i < 0 || i >= hexGrid[j].length) {
            return;
        }

        const tileType = TileType.types[hexGrid[j][i].type];

        var x = i * this.hexRectangleWidth + (this.hexRectangleWidth/2 * (j % 2));
        var y = j * this.hexRectangleHeight - (this.sideLength/2 * j);

        // draw visible hexagon
        this.ctx.fillStyle = tileType.color;
        this.ctx.strokeStyle = "#999";
        this.drawHexagon(this.ctx, x, y);
        this.ctx.fill();
        this.ctx.stroke();

        // draw tile image over top of hexagon
        if(tileType.img) {
            var imgWidth = this.hexRectangleHeight;
            var imgHeight = this.hexRectangleHeight;
            if(imgWidth > this.hexRectangleWidth) {
                imgWidth = this.hexRectangleWidth;
                imgHeight = this.hexRectangleWidth;
            }
            this.ctx.drawImage(tileType.img, x, y, imgWidth, imgHeight);
        }

        // draw storm over tile if tile is in the storm
        const stormRadius = this.controller.stormRadius;
        if(stormRadius >= 0) {
            const stormX = Math.floor(hexGrid.length/2);
            const stormY = Math.floor(hexGrid[0].length/2);
        //    const iOffset = (stormX+stormY%2) - (i+j%2);
        //    const jOffset = stormY - j;
            if(!this.isPointWithinHexagon(i, j, stormRadius, stormX, stormY)) {
            //if(iOffset*iOffset + jOffset*jOffset > stormRadius*stormRadius) {
                this.ctx.fillStyle = "#ffa500";
                this.ctx.globalAlpha = 0.6;
                this.drawHexagon(this.ctx, x, y);
                this.ctx.fill();
                this.ctx.globalAlpha = 1.0;
            }
        }

        if(drawToHit) {
            // draw hitbox hexagon
            this.hitCtx.fillStyle = hexGrid[j][i].colorKey;
            this.drawHexagon(this.hitCtx, x, y);
            this.hitCtx.fill();
        }
    }

    private drawHexagon(context, x, y) {
        context.beginPath();
        context.moveTo(x + this.hexRadius, y);
        context.lineTo(x + this.hexRectangleWidth, y + this.hexHeight);
        context.lineTo(x + this.hexRectangleWidth, y + this.hexHeight + this.sideLength);
        context.lineTo(x + this.hexRadius, y + this.hexRectangleHeight);
        context.lineTo(x, y + this.sideLength + this.hexHeight);
        context.lineTo(x, y + this.hexHeight);
        context.closePath();
    }

    private drawCircle(context, x, y, r) {
        context.beginPath();
        context.arc(x, y, r, 0, 2*Math.PI);
        context.closePath();
    }

    private drawItemBubble(m: ItemMemento) {
        const i = m.mapX;
        const j = m.mapY;
        const x = i * this.hexRectangleWidth + (this.hexRectangleWidth/2 * (j%2)) + this.hexRectangleWidth/2;
        const y = j * this.hexRectangleHeight - (this.sideLength/2 * j) + (this.hexRadius/4);
        const botY = y + this.hexRadius;
        const radius = this.hexRadius/2;
        // maths class paid off
        const yOffset = radius * Math.sin(Math.PI/4);
        const xOffset = Math.sqrt(radius*radius - yOffset*yOffset);

        // draw the icon
        this.ctx.save();
        this.ctx.fillStyle = "#555";
        this.ctx.strokeStyle = "#fff";
        this.ctx.globalAlpha = 0.9;
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius, Math.PI/4, -1.25*Math.PI, true);
        this.ctx.bezierCurveTo(x-xOffset/2, (y+botY)/2, x, botY, x, botY);
        this.ctx.bezierCurveTo(x, botY, x+xOffset/2, (y+botY)/2, x+xOffset, y+yOffset);
        this.ctx.fill();
        this.ctx.globalAlpha = 1.0;
        this.ctx.stroke();
        this.ctx.restore();

        // draw the text
        this.ctx.fillStyle = "#00ff00";
        this.ctx.textAlign = "center";
        this.ctx.textBaseline = "middle";
        this.ctx.font = `${radius*1.5}px Calibri`;
        this.ctx.fillText(""+m.since, x, y);

        // draw a circle to the hit context
        this.hitCtx.fillStyle = m.colorKey;
        this.drawCircle(this.hitCtx, x, y, radius);
        this.hitCtx.fill();
    }

    private drawOuterTilesInViewRange(myDetails, hexGrid, gap: number) {
        const viewRange = Math.max(2 + this.controller.myPlayerModifiers.viewRange, 0);
        const diameter = viewRange * 2 + 1;
        const xOffset = myDetails.y % 2;
        const startY = myDetails.y-viewRange;
        const endY = myDetails.y+viewRange;
        for(let j = startY; j <= endY; j++) {
            // calculate the length of this row based on the vertical distance from the center tile
            const vDistToCenter = Math.abs(j - myDetails.y);
            const rowLength = diameter - vDistToCenter;
            const rowXOffset = (vDistToCenter % 2) * xOffset;
            const startX = myDetails.x - Math.floor(rowLength/2);
            // if the row is in the outer range, draw whole row, else, draw left and right side only
            if(j < startY + gap || j > endY-gap) {
                // draw whole row
                for(let i = startX; i < startX+rowLength; i++) {
                    this.drawTile(i+rowXOffset, j, hexGrid);
                }
            } else {
                // draw the left side of the current row
                for(let i = startX; i < startX+gap; i++) {
                    this.drawTile(i+rowXOffset, j, hexGrid);
                }
                // draw the right side of the current row
                for(let i = startX+rowLength-gap; i < startX+rowLength; i++) {
                    this.drawTile(i+rowXOffset, j, hexGrid);
                }
            }
        }
    }

    private drawInnerTilesInViewRange(myDetails, hexGrid, gap: number) {
        // draw the tiles
        // wow, i can't believe this worked first try...
        // ...this may be the greatest achievement in the history of mathematics
        const viewRange = Math.max(2 + this.controller.myPlayerModifiers.viewRange, 0);
        const diameter = viewRange * 2 + 1;
        const xOffset = myDetails.y % 2;
        for(let j = myDetails.y-viewRange+gap; j <= myDetails.y+viewRange-gap; j++) {
            // calculate the length of this row based on the vertical distance from the center tile
            const vDistToCenter = Math.abs(j - myDetails.y);
            const rowLength = diameter - vDistToCenter;
            const rowXOffset = (vDistToCenter % 2) * xOffset;
            const startX = myDetails.x - Math.floor(rowLength/2);

            // draw the current row
            for(let i = startX+gap; i < startX+rowLength-gap; i++) {
                this.drawTile(i+rowXOffset, j, hexGrid);
            }
        }
    }

    private drawTileOutlines(myDetails, range, hexGrid) {
        const centerX = myDetails.x;
        const centerY = myDetails.y;
        const diameter = range * 2 + 1;
        const xOffset = centerY % 2;

        this.ctx.strokeStyle = "#fff";
        this.ctx.globalAlpha = 1;
        this.ctx.lineWidth = 7.5;

        for(let j = centerY-range; j <= centerY+range; j++) {
            // calculate the length of this row based on the vertical distance from the center tile
            const vDistToCenter = Math.abs(j - centerY);
            const rowLength = diameter - vDistToCenter;
            const rowXOffset = (vDistToCenter % 2) * xOffset;
            const startX = centerX - Math.floor(rowLength/2);

            for(let i = startX; i < startX+rowLength; i++) {
                var x = (i+rowXOffset) * this.hexRectangleWidth + (this.hexRectangleWidth/2 * (j % 2));
                var y = j * this.hexRectangleHeight - (this.sideLength/2 * j);
                this.drawHexagon(this.ctx, x, y);
                this.ctx.stroke();
            }
        }

        this.ctx.lineWidth = 1;
    }

    private isPointWithinHexagon(pointX: number, pointY: number, outerRadius: number, circleX: number, circleY: number): boolean {
        const diameter = outerRadius * 2 + 1;
        const xOffset = circleY % 2;

        for(let j = circleY-outerRadius; j <= circleY+outerRadius; j++) {
            // calculate the length of this row based on the vertical distance from the center tile
            const vDistToCenter = Math.abs(j - circleY);
            const rowLength = diameter - vDistToCenter;
            const rowXOffset = (vDistToCenter % 2) * xOffset;
            const startX = circleX - Math.floor(rowLength/2);

            // check whether hexagon x & y are within search range
            for(let i = startX; i < startX+rowLength; i++) {
                if(pointX === i+rowXOffset && pointY === j) {
                    return true;
                }
            }
        }

        return false;
    }

    private onClick = (e: MouseEvent) => {
        e.preventDefault();
        const myDetails = this.controller.myPlayerDetails;
        const objUnderMouse = this.getObjectUnderMouse(e);
        const hexGrid = this.controller.map.hexGrid;

        if(hexGrid && objUnderMouse && ("x" in objUnderMouse)) {
            const hexagon = objUnderMouse as { x: number, y: number };
            // get the tile type id and tile type
            const tileTypeID = hexGrid[hexagon.y][hexagon.x].type;
            const tileType = TileType.types[tileTypeID];

            if(this.controller.gamePhase == GamePhase.DROP_PHASE) {
                // Ensure the tile can be landed on
                if(!tileType.impassable && !tileType.undroppable) {
                    this.hexSelectedX = hexagon.x;
                    this.hexSelectedY = hexagon.y;
                    this.controller.setMoveTurn(this.hexSelectedX, this.hexSelectedY);
                }
            } else if(this.controller.gamePhase == GamePhase.MOVE_PHASE) {
                if(myDetails) {
                    const dx = myDetails.x - hexagon.x;
                    const dy = myDetails.y - hexagon.y;
                    if((((dy == -1 || dy == 1) && (myDetails.y % 2 == 1) && dx >= -1 && dx <= 0) ||
                       ((dy == -1 || dy == 1) && (myDetails.y % 2 == 0) && dx >= 0 && dx <= 1) ||
                       (dy == 0 && dx >= -1 && dx <= 1)) && !tileType.impassable) {
                        this.hexSelectedX = hexagon.x;
                        this.hexSelectedY = hexagon.y;
                        this.controller.setMoveTurn(this.hexSelectedX, this.hexSelectedY);
                        this.controller.onMoveValid(this.hexSelectedX, this.hexSelectedY, dx == dy && dx == 0);
                    }
                }
            } else if(this.controller.gamePhase == GamePhase.ACTION_PHASE) {
                if(myDetails) {
                    const item = this.controller.myPlayerEquippedItem ?
                        this.controller.items[this.controller.myPlayerEquippedItem.itemID] : null;
                    const searchRange = Math.max(1, 0);
                    const attackRange = Math.max(item ? (item as Gun).maxRange : 0, 0);
                    const canSearch = this.isPointWithinHexagon(hexagon.x, hexagon.y, searchRange, myDetails.x, myDetails.y);
                    const canAttack = this.controller.isPlayerOnTile(hexagon.x, hexagon.y) &&
                            this.isPointWithinHexagon(hexagon.x, hexagon.y, attackRange, myDetails.x, myDetails.y) &&
                            this.controller.myPlayerAP >= ActionApCosts.ATTACK;

                    if(e.button == 2) {
                        // if right click, show the custom context menu
                        this.controller.onTileRightClickedActionPhase(this.lastGlobalMouseX, this.lastGlobalMouseY,
                            hexagon.x, hexagon.y, canSearch, canAttack);
                    } else if(e.button == 0) {
                        // if left click and can attack, determine which player is on tile
                        const playerIDs = this.controller.getPlayersOnTile(hexagon.x, hexagon.y);
                        if(canAttack && playerIDs.length === 1) {
                            this.controller.onInitiateAttackPlayer(playerIDs[0]);
                        } else if(canAttack && playerIDs.length > 1) {
                            this.controller.onAttackPlayerOnTile(this.lastGlobalMouseX, this.lastGlobalMouseY, hexagon.x, hexagon.y);
                        } else if(canSearch) {
                            this.controller.searchTile(hexagon.x, hexagon.y);
                        }
                    }
                }
            }
            // request the map be redrawn with new selection info
            this.controller.requestDrawMap();
        }
        return false;
    }

    private onMouseMove = (e: MouseEvent) => {
        const myDetails = this.controller.myPlayerDetails;
        const objUnderMouse = this.getObjectUnderMouse(e);
        const hexGrid = this.controller.map.hexGrid;

        this.controller.globalMouseX = this.lastGlobalMouseX = e.clientX;
        this.controller.globalMouseY = this.lastGlobalMouseY = e.clientY;
        this.lastMouseX = this.lastGlobalMouseX - this.canvas.getBoundingClientRect().left;
        this.lastMouseY = this.lastGlobalMouseY - this.canvas.getBoundingClientRect().top;

        if(hexGrid && objUnderMouse && ("x" in objUnderMouse)) {
            const hexagon = objUnderMouse as { x: number, y: number };
            // get the tile type id and tile type
            const tileTypeID = hexGrid[hexagon.y][hexagon.x].type;
            const tileType = TileType.types[tileTypeID];

            this.hexHoverX = hexagon.x;
            this.hexHoverY = hexagon.y;

            if(this.controller.gamePhase == GamePhase.DROP_PHASE) {
                // notify the controller about tile hover so a tooltip can be created
                this.controller.onTileHoverDropPhase(hexagon.x, hexagon.y, this.lastGlobalMouseX, this.lastGlobalMouseY);
                // Ensure the tile can be landed on
                if(!tileType.impassable && !tileType.undroppable) {
                    this.hexHoverValid = true;
                    this.canvas.style.cursor = "url('assets/Boots_Icon.png') 16 16, auto";
                } else {
                    this.hexHoverValid = false;
                    this.canvas.style.cursor = "url('assets/Cross_Icon.png') 16 16, auto";
                }
            } else if(this.controller.gamePhase == GamePhase.MOVE_PHASE) {
                // notify the controller about the tile hover so a tooltip can be created
                this.controller.onTileHoverMovePhase(hexagon.x, hexagon.y, this.lastGlobalMouseX, this.lastGlobalMouseY);
                if(myDetails) {
                    const dx = myDetails.x - hexagon.x;
                    const dy = myDetails.y - hexagon.y;
                    if((((dy == -1 || dy == 1) && (myDetails.y % 2 == 1) && dx >= -1 && dx <= 0) ||
                       ((dy == -1 || dy == 1) && (myDetails.y % 2 == 0) && dx >= 0 && dx <= 1) ||
                       (dy == 0 && dx >= -1 && dx <= 1)) && !tileType.impassable) {
                        this.hexHoverValid = true;
                        this.canvas.style.cursor = "url('assets/Boots_Icon.png') 16 16, auto";
                    } else {
                        this.hexHoverValid = false;
                        this.canvas.style.cursor = "url('assets/Cross_Icon.png') 16 16, auto";
                    }
                }
            } else if(this.controller.gamePhase == GamePhase.ACTION_PHASE) {
                this.hexHoverValid = false;
                if(myDetails) {
                    const item = this.controller.myPlayerEquippedItem ?
                        this.controller.items[this.controller.myPlayerEquippedItem.itemID] : null;
                    const searchRange = Math.max(1, 0);
                    const attackRange = Math.max(item ? (item as Gun).maxRange : 0, 0);
                    const canSearch = this.isPointWithinHexagon(hexagon.x, hexagon.y, searchRange, myDetails.x, myDetails.y);
                    const canAttack = this.controller.isPlayerOnTile(hexagon.x, hexagon.y) &&
                            this.isPointWithinHexagon(hexagon.x, hexagon.y, attackRange, myDetails.x, myDetails.y) &&
                            this.controller.myPlayerAP >= ActionApCosts.ATTACK;

                    // set the mouse cursor icon based on what is being hovered
                //    console.log("Can Attack/Search", canAttack, canSearch, this.controller.myPlayerAP);
                    if(canSearch && !canAttack) {
                        this.canvas.style.cursor = "url('assets/search_icon.png') 16 16, auto";
                        this.hexHoverValid = true;
                        // TODO - get proper ap costs from somewhere
                        this.controller.onTileHoverActionPhase("Search", "Search current/adjacent tile for mines", 10, this.lastGlobalMouseX, this.lastGlobalMouseY);
                    } else if(canAttack) {
                        this.canvas.style.cursor = "url('assets/target_icon.png') 20 20, auto";
                        this.hexHoverValid = true;
                        // TODO - get proper ap costs from somewhere
                        this.controller.onTileHoverActionPhase("Attack", "Attack player with currently equipped weapon", 60, this.lastGlobalMouseX, this.lastGlobalMouseY);
                    } else {
                        this.canvas.style.cursor = "url('assets/Cross_Icon.png') 16 16, auto";
                        this.controller.onTileHoverActionPhase(null, null, 0, 0, 0);
                    }
                }
            }
            // request the map be redrawn with new hover info
            this.controller.requestDrawMap();
        } else if(objUnderMouse && ("mem" in objUnderMouse)) {
            const memento = (objUnderMouse as { mem: ItemMemento }).mem;
            this.controller.onItemMementoHover(memento, this.lastGlobalMouseX, this.lastGlobalMouseY);
        }
    }

    private onMouseWheel = (e: WheelEvent) => {
        // Calculate the min. zoom level s.t. the entire map is visible
        const wheelDelta = e.wheelDelta ? e.wheelDelta : e.deltaY ? -e.deltaY : 0;
        const canvasToMapWidthRatio = this.canvas.width / this.totalMapWidth;
        const canvasToMapHeightRatio = this.canvas.height / this.totalMapHeight;
        let minZoomLevel = canvasToMapWidthRatio < canvasToMapHeightRatio ? canvasToMapWidthRatio : canvasToMapHeightRatio;

        // Calculate the new zoom level based on the direction the wheel was spun
        const wheelDirection = wheelDelta < 0 ? -1 : wheelDelta > 0 ? 1 : 0;
        this.zoomLevel += wheelDirection * 0.05;
        this.zoomLevel = this.zoomLevel < minZoomLevel ? minZoomLevel : this.zoomLevel > 1.0 ? 1.0 : this.zoomLevel;

        // Determine the zoom target based on the mouse location
        // NOTE - when zooming out, the target should be centered on map center
        if(!this.controller.myPlayerDetails) {
            if(wheelDirection < 0) {
                // zooming out
                this.zoomTargetX = this.canvas.width/2 - this.totalMapWidth/2*this.zoomLevel;
                this.zoomTargetY = this.canvas.height/2 - this.totalMapHeight/2*this.zoomLevel;
            } else {
                // zooming in
                this.zoomTargetX = this.canvas.width/2 - this.totalMapWidth/2*this.zoomLevel;
                this.zoomTargetY = this.canvas.height/2 - this.totalMapHeight/2*this.zoomLevel;
            }
        } else {
            // center the player
            const i = this.controller.myPlayerDetails.x;
            const j = this.controller.myPlayerDetails.y;
            var x = i * this.hexRectangleWidth + (this.hexRectangleWidth/2 * (j % 2)) + this.hexRectangleWidth/2;
            var y = j * this.hexRectangleHeight - (this.sideLength/2 * j) + this.hexRectangleHeight/2;
            this.zoomTargetX = this.canvas.width/2 - x*this.zoomLevel;
            this.zoomTargetY = this.canvas.height/2 - y*this.zoomLevel;
        }

        // Redraw the map at the new zoom level
        e.preventDefault();
        this.controller.requestDrawMap();
        return false;
    }

    public onMyPlayerDetailsUpdated() {
        // center the player
        const i = this.controller.myPlayerDetails.x;
        const j = this.controller.myPlayerDetails.y;
        var x = i * this.hexRectangleWidth + (this.hexRectangleWidth/2 * (j % 2)) + this.hexRectangleWidth/2;
        var y = j * this.hexRectangleHeight - (this.sideLength/2 * j) + this.hexRectangleHeight/2;
        this.zoomTargetX = this.canvas.width/2-x*this.zoomLevel;
        this.zoomTargetY = this.canvas.height/2-y*this.zoomLevel;
    }

    public onMoveValid(newX: number, newY: number) {
        const myDetails = this.controller.myPlayerDetails;
        if(myDetails) {
            this.displayArrows.length = 0;
            if(myDetails.x !== newX && myDetails.y !== newY) {
                this.displayArrows.push(new MapCanvasArrow(myDetails.x, myDetails.y, newX, newY, "MOVE"));
            }
        }
    }

    public onAttackValid(targetID: number) {
        const myDetails = this.controller.myPlayerDetails;
        const targetDetails = this.controller.playerDetails[targetID];

        // Add an "Attack" arrow from my player to the target player
        if(myDetails && targetDetails) {
            this.displayArrows.push(new MapCanvasArrow(myDetails.x, myDetails.y, targetDetails.x, targetDetails.y, "ATTACK"));
        }
    }

    public resize() {
        // Set the internal width & height of the canvases equal to the canvas element's width & height
        this.canvas.setAttribute("width", String(this.canvas.clientWidth));
        this.canvas.setAttribute("height", String(this.canvas.clientHeight));
        this.hitCanvas.setAttribute("width", String(this.hitCanvas.clientWidth));
        this.hitCanvas.setAttribute("height", String(this.hitCanvas.clientHeight));
        this.controller.requestDrawMap();
    }

    public onMapLoaded() {
        this.totalMapWidth = this.hexRectangleWidth * (this.controller.map.hexGrid.length + 0.5);
        this.totalMapHeight = this.hexRectangleHeight * (this.controller.map.hexGrid[0].length + 0.5) * 3/4;

        this.canvas.addEventListener(Controller.CLICK_EVENT, this.onClick);
        this.canvas.addEventListener("contextmenu", this.onClick);
        this.canvas.addEventListener("mousemove", this.onMouseMove);
        this.canvas.addEventListener("onwheel" in window ? "wheel" : "DOMMouseScroll", this.onMouseWheel, true);

        // set default zoom level to be fully zoomed out
        const canvasToMapWidthRatio = this.canvas.width / this.totalMapWidth;
        const canvasToMapHeightRatio = this.canvas.height / this.totalMapHeight;
        let minZoomLevel = canvasToMapWidthRatio < canvasToMapHeightRatio ? canvasToMapWidthRatio : canvasToMapHeightRatio;
        this.zoomLevel = minZoomLevel;

        // set default zoom to provide complete overview of map
        this.zoomTargetX = this.canvas.width/2 - this.totalMapWidth/2*this.zoomLevel;
        this.zoomTargetY = this.canvas.height/2 - this.totalMapHeight/2*this.zoomLevel;
    }

    // Called when a move phase has begun
    public onMovePhaseStart() {
        this.displayArrows.length = 0;

        if(this.controller.myPlayerDetails) {
            this.hexSelectedX = this.controller.myPlayerDetails.x;
            this.hexSelectedY = this.controller.myPlayerDetails.y;
        }
        // Trigger false onMouseMove event to reset mouse cursor icon
        this.onMouseMove({
            clientX: this.lastMouseX + this.canvas.getBoundingClientRect().left,
            clientY: this.lastMouseY + this.canvas.getBoundingClientRect().top
        } as MouseEvent);
    }

    // Called when an action phase has begun
    public onActionPhaseStart() {
        this.displayArrows.length = 0;

        if(this.controller.myPlayerDetails) {
            this.hexSelectedX = this.controller.myPlayerDetails.x;
            this.hexSelectedY = this.controller.myPlayerDetails.y;
        }
        // Trigger false onMouseMove event to reset mouse cursor icon
        this.onMouseMove({
            clientX: this.lastMouseX + this.canvas.getBoundingClientRect().left,
            clientY: this.lastMouseY + this.canvas.getBoundingClientRect().top
        } as MouseEvent);
    }

    // Called when a phase has finished and the game is now waiting for the server
    public onWaitingForServer() {
        this.canvas.style.cursor = "url('assets/Cross_Icon.png') 16 16, auto";
    }

    // Get the hexagon of the hit canvas the mouse is positioned over
    private getObjectUnderMouse(e) {
        var mousex = e.clientX - this.hitCanvas.getBoundingClientRect().left;
        var mousey = e.clientY - this.hitCanvas.getBoundingClientRect().top;

        var pixel = this.hitCtx.getImageData(mousex, mousey, 1, 1).data;
        var color = `rgb(${pixel[0]},${pixel[1]},${pixel[2]})`;
        return this.controller.hitMap.get(color);
    }
}
