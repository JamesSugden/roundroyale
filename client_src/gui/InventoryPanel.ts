import { Controller } from "../Controller"
import { GamePhase } from "../game/GamePhase"
import { Item, ItemEntity, isBandage, isGun } from "../game/Item"

export class InventoryPanel {
    private readonly rowLength = 3;
    private readonly colHeight = 4;
    private itemSlots: Array<Array<HTMLDivElement>>;

    private limbSelected: string;

    public constructor(private controller: Controller) {
        this.itemSlots = new Array<Array<HTMLDivElement>>();
        for(let j = 0; j < this.colHeight; j++) {
            this.itemSlots.push(new Array<HTMLDivElement>());
            for(let i = 0; i < this.rowLength; i++) {
                const itemSlot = document.getElementById(`invent-slot-${i+(j*3)}`) as HTMLDivElement;
                itemSlot.addEventListener("dragenter", this.onDragEnter);
                itemSlot.addEventListener("dragleave", this.onDragLeave);
                itemSlot.addEventListener("dragover", this.onDragOver);
                itemSlot.addEventListener("drop", this.onDrop);
                this.itemSlots[this.itemSlots.length-1].push(itemSlot);
            }
        }
    }

    // Returns true iff the player's inventory contains at least one bandage item
    public containsBandage(): boolean {
        for(const group of this.itemSlots) {
            for(const slot of group) {
                if(slot.firstChild) {
                    const splits = slot.children[0].id.split("_");
                    if(splits.length >= 3) {
                        const itemIndex = splits[2];
                        const item = this.controller.items[itemIndex];
                        if(isBandage(item)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    // Highlight bandages so player can easily pick which to use to heal limb
    public highlightBandages(limb: string) {
        this.limbSelected = limb;
        for(const group of this.itemSlots) {
            for(const slot of group) {
                if(slot.firstChild) {
                    const splits = slot.children[0].id.split("_");
                    if(splits.length >= 3) {
                        const itemIndex = splits[2];
                        const item = this.controller.items[itemIndex];
                        if(isBandage(item)) {
                            slot.style.zIndex = "75";
                        }
                    }
                }
            }
        }
    }

    public unHighlightItems() {
        this.limbSelected = null;
        for(const group of this.itemSlots) {
            for(const slot of group) {
                slot.style.zIndex = "0";
            }
        }
    }

    private onDragEnter = (e) => {
        if(!e.target) { return; }
        e.preventDefault();
        const slotIndex = parseInt(e.target.id.split("invent-slot-")[1], 10);
        const splits = e.dataTransfer.getData("text").split("_");
        let itemIndex, entityID;

        if(splits.length == 3) {
            // Dropped item from ground
            itemIndex = parseInt(splits[1], 10);
            entityID = parseInt(splits[2], 10);
        } else if(splits.length == 6) {
            // Dropped item already in inventory
            itemIndex = parseInt(splits[2], 10);
            entityID = parseInt(splits[3], 10);
        }

        if(isNaN(slotIndex) || isNaN(itemIndex) || isNaN(entityID)) {
            return;
        }

        // Clear all drag hover effects
        for(const row of this.itemSlots) {
            for(const itemSlot of row) {
                itemSlot.style.cssText = "";
            }
        }

        // Determine whether a drop is allowed based on slot emptiness
        const item = this.controller.items[itemIndex];
        this.updateSlot(item, slotIndex, entityID);
    }

    private onDragLeave = (e) => {
        e.preventDefault();
    }

    private onDragOver = (e) => {
        e.preventDefault();
    }

    private onDrop = (e) => {
        if(!e.target) { return; }
        e.preventDefault();
        const slotIndex = parseInt(e.target.id.split("invent-slot-")[1], 10);
        const splits = e.dataTransfer.getData("text").split("_");
        let itemIndex, entityID;

        if(splits.length == 3) {
            // Dropped item from ground
            itemIndex = splits[1];
            entityID = splits[2];
        } else if(splits.length == 6) {
            // Dropped item already in inventory
            itemIndex = splits[2];
            entityID = splits[3];
        }

        if(isNaN(slotIndex) || isNaN(itemIndex) || isNaN(entityID)) {
            return;
        }

        // Check the wether the slot is avialable to be dropped in
        const item = this.controller.items[itemIndex];
        this.updateSlot(item, slotIndex, entityID, false, true);
    }

    public onItemDragEnd() {
        this.resetSlotStyle();
    }

    // Assumes slot has already been validified
    private fillSlot(entityID: number, firstSlotX: number, firstSlotY: number) {
        this.controller.takeItem(entityID, firstSlotX, firstSlotY);
    }

    private updateSlot(item: Item, slotIndex: number, entityID: number, color = true, drop = false) {
        const firstSlotX = (slotIndex % this.rowLength);
        const firstSlotY = Math.floor(slotIndex / this.rowLength);
        const slotY = firstSlotY;

        // check if the item fits on this row
        if(firstSlotX + item.gridWidth > this.rowLength) {
            if(color) {
                for(let x = firstSlotX; x < this.rowLength; x++) {
                    this.itemSlots[slotY][x].style.backgroundColor = "#f00";
                }
            }
            return;
        }

        // check if the slots are taken
        for(let x = firstSlotX; x < firstSlotX+item.gridWidth; x++) {
            if(this.itemSlots[slotY][x].innerHTML != "") {
                if(color) {
                    for(let x = firstSlotX; x < firstSlotX+item.gridWidth; x++) {
                        this.itemSlots[slotY][x].style.backgroundColor = "#f00";
                    }
                }
                return;
            }
        }

        // else, the slots must be free
        for(let x = firstSlotX; x < firstSlotX+item.gridWidth; x++) {
            if(color) {
                this.itemSlots[slotY][x].style.backgroundColor = "#0f0";
            }
        }

        if(drop) {
            // only need to tell server about first slot indexes
            this.fillSlot(entityID, firstSlotX, firstSlotY);
        }
    }

    public removeFromBackpack(entityID: number, itemID: number) {
        for(let slotY = 0; slotY < this.colHeight; slotY++) {
            for(let slotX = 0; slotX < this.rowLength; slotX++) {
                const entity = document.getElementById(`backpack_item_${itemID}_${entityID}_${slotX}_${slotY}`);
                if(entity) {
                    entity.remove();
                    this.itemSlots[slotY][slotX].classList.remove("invent-slot-joined-right");
                    this.itemSlots[slotY][slotX].classList.remove("invent-slot-joined-left");
                }
            }
        }
    }

    // Called from controller once server has validified item taken
    public onItemTaken(entityID: number, itemID: number, firstSlotX: number, firstSlotY: number) {
        if(itemID < 0 || itemID >= this.controller.items.length) {
            return;
        }
        const item = this.controller.items[itemID];

        // first, ensure the item fits into the space
        if(item.gridWidth > firstSlotX + item.gridWidth) {
            return;
        }

        // remove the item from the current spot in backpack if it is in backpack
        this.removeFromBackpack(entityID, itemID);

        const slotY = firstSlotY;   // TODO - change when item rotation implemented
        let i = 0;
        for(let slotX = firstSlotX; slotX < firstSlotX+item.gridWidth; slotX++) {
            const slot = this.itemSlots[slotY][slotX];
            // set the drag-img to the item img, with size relative to item slot size
            // method for setting drag-img size sourced from https://stackoverflow.com/questions/31994572/change-drag-ghost-image-size
            let img = document.createElement("img");
            img.id = `backpack_item_${itemID}_${entityID}_${slotX}_${slotY}`;
            img.src = item.image;
            img.style.width = `${(slot.clientWidth-0)*item.gridWidth}px`;
            img.style.height = `${(slot.clientHeight-0)*item.gridHeight}px`;
            img.classList.add("invent-item-img");
            img.style.left = `${-slot.clientWidth*i}px`;
            img.addEventListener("dragstart", this.onDragStart);
            img.addEventListener("dragend", this.onDragEnd);
            img.addEventListener(Controller.CLICK_EVENT, this.onClick);
            img.addEventListener("mousemove", this.onHover);
            slot.appendChild(img);
            i++;
        }

        // remove right borders from all but last slot
        for(let slotX = firstSlotX; slotX < firstSlotX+item.gridWidth-1; slotX++) {
            ///console.log("removing right border");
            const slot = this.itemSlots[slotY][slotX];
            slot.style.borderRight = "none";
            slot.classList.add("invent-slot-joined-right");
        }

        // remove left borders from all but first slot
        for(let slotX = firstSlotX+1; slotX < firstSlotX+item.gridWidth; slotX++) {
            ///console.log("removing left border");
            const slot = this.itemSlots[slotY][slotX];
            slot.classList.add("invent-slot-joined-left");
        }

        this.resetSlotStyle();
    }

    private onDragStart = (e) => {
        const itemObj = this.controller.items[parseInt(e.target.id.split("_")[2], 10)];
        const entityID = parseInt(e.target.id.split("_")[3], 10);

        // set the entity to be hidden so it looks like it is being picked up
        for(const row of this.itemSlots) {
            for(const slot of row) {
                if(slot.children.length > 0) {
                    const elem = document.getElementById(slot.children[0].id);
                    if(elem) {
                        const eID = parseInt(elem.id.split("_")[3], 10);
                        if(eID == entityID) {
                            elem.style.visibility = "hidden";
                        }
                    }
                }
            }
        }

        // set the drag-img to the item img, with size relative to item slot size
        // method for setting drag-img size sourced from https://stackoverflow.com/questions/31994572/change-drag-ghost-image-size
        let dragIcon = document.createElement("img");
        dragIcon.src = e.target.src;
        dragIcon.style.width = `${this.itemSlots[0][0].clientWidth*itemObj.gridWidth}px`;
        dragIcon.style.height = `${this.itemSlots[0][0].clientHeight*itemObj.gridHeight}px`;

        let div = document.createElement("div");
        div.id = `drag_${e.target.id}`;
        div.appendChild(dragIcon);
        div.style.position = "absolute";
        div.style.top = "0px";
        div.style.left= `${-this.itemSlots[0][0].clientWidth*itemObj.gridWidth}px`;
        document.querySelector("body").appendChild(div);

        e.dataTransfer.setData("text/plain", e.target.id);
        e.dataTransfer.setDragImage(div,
            this.itemSlots[0][0].clientWidth/2,
            this.itemSlots[0][0].clientHeight/2);

        this.controller.onItemDragStart();
    }

    private onDragEnd = (e) => {
        // when the item has finished being dragged, remove the image in the dom
        document.getElementById(`drag_${e.target.id}`).remove();
        this.controller.onItemDragEnd();
        const entityID = e.target.id.split("_")[3];

        // set the entity in back to being visible
        this.resetSlotStyle();
    }

    private onClick = (e) => {
        e.preventDefault();
        if(this.controller.gamePhase == GamePhase.ACTION_PHASE) {
            const entityID = e.target.id.split("_")[3];
            const itemID = e.target.id.split("_")[2];
            const item = this.controller.items[itemID];
            if(isBandage(item)) {
                if(this.limbSelected) {
                    this.controller.useBandage(entityID, this.limbSelected);
                    this.limbSelected = null;
                } else {
                    this.controller.onBandageClick(e.clientX, e.clientY, entityID);
                }
            } else if(isGun(item)) {
                this.controller.equipItem(entityID);
            }
        }
    }

    private onHover = (e) => {
        e.preventDefault();
        const itemID = parseInt(e.target.id.split("_")[2], 10);
        const entityID = parseInt(e.target.id.split("_")[3], 10);
        this.controller.onItemHover(new ItemEntity(entityID, itemID), e.clientX, e.clientY);
    }

    // Called when the action phase is performed and a new weapon is equipped
    public onItemEquipped() {
        this.resetSlotStyle();
    }

    public resetSlotStyle() {
        for(const row of this.itemSlots) {
            for(const slot of row) {
                let equipped = false;
                // set the entity in back to being visible
                if(slot.children.length > 0) {
                    const elem = document.getElementById(slot.children[0].id);
                    if(elem) {
                        elem.style.visibility = "visible";
                        const eID = parseInt(elem.id.split("_")[3], 10);
                        if(this.controller.myPlayerEquippedItem &&
                                eID == this.controller.myPlayerEquippedItem.entityID) {
                            slot.style.backgroundColor = "#0EBFE9";
                            equipped = true;
                        }
                    }
                }
                // set any slots not containing equipped weapons to default
                if(!equipped) {
                    slot.style.cssText = "";
                }
            }
        }
    }
}
