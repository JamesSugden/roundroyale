import { Controller } from "../Controller"
import { GamePhase } from "../game/GamePhase"

export class CharacterPanel {
    private charDiv: HTMLDivElement;
    private charLbl: HTMLLabelElement;

    private headBtn: HTMLButtonElement;
    private chestBtn: HTMLButtonElement;
    private rArmBtn: HTMLButtonElement;
    private lArmBtn: HTMLButtonElement;
    private rLegBtn: HTMLButtonElement;
    private lLegBtn: HTMLButtonElement;

    private totalHPBar: HTMLSpanElement;
    private headHPBar: HTMLSpanElement;
    private chestHPBar: HTMLSpanElement;
    private rarmHPBar: HTMLSpanElement;
    private larmHPBar: HTMLSpanElement;
    private rlegHPBar: HTMLSpanElement;
    private llegHPBar: HTMLSpanElement;

    private headHPLbl: HTMLLabelElement;
    private chestHPLbl: HTMLLabelElement;
    private rarmHPLbl: HTMLLabelElement;
    private larmHPLbl: HTMLLabelElement;
    private rlegHPLbl: HTMLLabelElement;
    private llegHPLbl: HTMLLabelElement;

    private headBloodSplatImg: HTMLImageElement;
    private chestBloodSplatImg: HTMLImageElement;
    private rarmBloodSplatImg: HTMLImageElement;
    private larmBloodSplatImg: HTMLImageElement;
    private rlegBloodSplatImg: HTMLImageElement;
    private llegBloodSplatImg: HTMLImageElement;

    private bloodSplatImgs: Array<string> = ["assets/Blood-Splat-1.png", "assets/Blood-Splat-2.png", "assets/Blood-Splat-3.png"];

    public constructor(private controller: Controller) {
        this.charDiv = document.getElementById("char-div") as HTMLDivElement;
        this.charLbl = document.getElementById("char-lbl") as HTMLLabelElement;

        this.headBtn = document.getElementById("head-btn") as HTMLButtonElement;
        this.chestBtn = document.getElementById("chest-btn") as HTMLButtonElement;
        this.rArmBtn = document.getElementById("rarm-btn") as HTMLButtonElement;
        this.lArmBtn = document.getElementById("larm-btn") as HTMLButtonElement;
        this.rLegBtn = document.getElementById("rleg-btn") as HTMLButtonElement;
        this.lLegBtn = document.getElementById("lleg-btn") as HTMLButtonElement;

        this.totalHPBar = document.getElementById("total-hp-bar") as HTMLSpanElement;
        this.headHPBar = document.getElementById("head-hp-bar") as HTMLSpanElement;
        this.chestHPBar = document.getElementById("chest-hp-bar") as HTMLSpanElement;
        this.rarmHPBar = document.getElementById("rarm-hp-bar") as HTMLSpanElement;
        this.larmHPBar = document.getElementById("larm-hp-bar") as HTMLSpanElement;
        this.rlegHPBar = document.getElementById("rleg-hp-bar") as HTMLSpanElement;
        this.llegHPBar = document.getElementById("lleg-hp-bar") as HTMLSpanElement;

        this.headHPLbl = document.getElementById("head-hp-lbl") as HTMLLabelElement;
        this.chestHPLbl = document.getElementById("chest-hp-lbl") as HTMLLabelElement;
        this.rarmHPLbl = document.getElementById("rarm-hp-lbl") as HTMLLabelElement;
        this.larmHPLbl = document.getElementById("larm-hp-lbl") as HTMLLabelElement;
        this.rlegHPLbl = document.getElementById("rleg-hp-lbl") as HTMLLabelElement;
        this.llegHPLbl = document.getElementById("lleg-hp-lbl") as HTMLLabelElement;

        this.headBloodSplatImg = document.getElementById("head-blood-splat-img") as HTMLImageElement;
        this.chestBloodSplatImg = document.getElementById("chest-blood-splat-img") as HTMLImageElement;
        this.rarmBloodSplatImg = document.getElementById("rarm-blood-splat-img") as HTMLImageElement;
        this.larmBloodSplatImg = document.getElementById("larm-blood-splat-img") as HTMLImageElement;
        this.rlegBloodSplatImg = document.getElementById("rleg-blood-splat-img") as HTMLImageElement;
        this.llegBloodSplatImg = document.getElementById("lleg-blood-splat-img") as HTMLImageElement;

        this.headBtn.addEventListener(Controller.CLICK_EVENT, this.onHeadBtnClick);
        this.chestBtn.addEventListener(Controller.CLICK_EVENT, this.onChestBtnClick);
        this.rArmBtn.addEventListener(Controller.CLICK_EVENT, this.onRArmBtnClick);
        this.lArmBtn.addEventListener(Controller.CLICK_EVENT, this.onLArmBtnClick);
        this.rLegBtn.addEventListener(Controller.CLICK_EVENT, this.onRLegBtnClick);
        this.lLegBtn.addEventListener(Controller.CLICK_EVENT, this.onLLegBtnClick);
    }

    public refreshDisplay() {
        const myPlayerHP = this.controller.myPlayerHP;
        if(myPlayerHP) {
            this.charLbl.innerHTML = `Total HP <span style="color: ${myPlayerHP.total <= 30 ? "#0040ff" : "#0040ff"}">
                    ${myPlayerHP.total < 0 ? 0 : myPlayerHP.total}</span>`;

            // Set value of total HP as percentage of height
            this.totalHPBar.style.height = `${myPlayerHP.total < 0 ? 0 : myPlayerHP.total}%`;

            // Set value of HP as percentage width of span
            this.headHPBar.style.width = `${myPlayerHP.head < 0 ? 0 : myPlayerHP.head}%`;
            this.chestHPBar.style.width = `${myPlayerHP.chest < 0 ? 0 : myPlayerHP.chest}%`;
            this.rarmHPBar.style.width = `${myPlayerHP.rarm < 0 ? 0 : myPlayerHP.rarm}%`;
            this.larmHPBar.style.width = `${myPlayerHP.larm < 0 ? 0 : myPlayerHP.larm}%`;
            this.rlegHPBar.style.width = `${myPlayerHP.rleg < 0 ? 0 : myPlayerHP.rleg}%`;
            this.llegHPBar.style.width = `${myPlayerHP.lleg < 0 ? 0 : myPlayerHP.lleg}%`;

            // Set label of hp bar to be the value of hp
            this.headHPLbl.innerHTML = "" + (myPlayerHP.head < 0 ? 0 : myPlayerHP.head);
            this.chestHPLbl.innerHTML = "" + (myPlayerHP.chest < 0 ? 0 : myPlayerHP.chest);
            this.rarmHPLbl.innerHTML = "" + (myPlayerHP.rarm < 0 ? 0 : myPlayerHP.rarm);
            this.larmHPLbl.innerHTML = "" + (myPlayerHP.larm < 0 ? 0 : myPlayerHP.larm);
            this.rlegHPLbl.innerHTML = "" + (myPlayerHP.rleg < 0 ? 0 : myPlayerHP.rleg);
            this.llegHPLbl.innerHTML = "" + (myPlayerHP.lleg < 0 ? 0 : myPlayerHP.lleg);

            // Change color of hp bars to red if below 30% hp
            if(myPlayerHP.total <= 30) {
                this.totalHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.totalHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(myPlayerHP.head <= 30) {
                this.headHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.headHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(myPlayerHP.chest <= 30) {
                this.chestHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.chestHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(myPlayerHP.rarm <= 30) {
                this.rarmHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.rarmHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(myPlayerHP.larm <= 30) {
                this.larmHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.larmHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(myPlayerHP.rleg <= 30) {
                this.rlegHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.rlegHPBar.classList.remove("limb-btn-hp-bar-injured");
            }

            if(myPlayerHP.lleg <= 30) {
                this.llegHPBar.classList.add("limb-btn-hp-bar-injured");
            } else {
                this.llegHPBar.classList.remove("limb-btn-hp-bar-injured");
            }
        }
    }

    public showBloodSplatHead() {
        const splat = this.getRandomSplat();
        this.headBloodSplatImg.src = splat;
        this.headBloodSplatImg.classList.remove("fade-out");
        this.headBloodSplatImg.classList.add("fade-out");
    }

    public showBloodSplatChest() {
        const splat = this.getRandomSplat();
        this.chestBloodSplatImg.src = splat;
        this.chestBloodSplatImg.classList.remove("fade-out");
        this.chestBloodSplatImg.classList.add("fade-out");
    }

    public showBloodSplatRArm() {
        const splat = this.getRandomSplat();
        this.rarmBloodSplatImg.src = splat;
        this.rarmBloodSplatImg.classList.remove("fade-out");
        this.rarmBloodSplatImg.classList.add("fade-out");
    }

    public showBloodSplatLArm() {
        const splat = this.getRandomSplat();
        this.larmBloodSplatImg.src = splat;
        this.larmBloodSplatImg.classList.remove("fade-out");
        this.larmBloodSplatImg.classList.add("fade-out");
    }

    public showBloodSplatRLeg() {
        const splat = this.getRandomSplat();
        this.rlegBloodSplatImg.src = splat;
        this.rlegBloodSplatImg.classList.remove("fade-out");
        this.rlegBloodSplatImg.classList.add("fade-out");
    }

    public showBloodSplatLLeg() {
        const splat = this.getRandomSplat();
        this.llegBloodSplatImg.src = splat;
        this.llegBloodSplatImg.classList.remove("fade-out");
        this.llegBloodSplatImg.classList.add("fade-out");
    }

    private getRandomSplat(): string {
        return this.bloodSplatImgs[Math.floor(Math.random() * this.bloodSplatImgs.length)];
    }

    private onHeadBtnClick = (e) => {
        if(this.controller.gamePhase == GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.head < 100 && this.controller.myPlayerHP.head > 0) {
            this.controller.onHealLimbClick("head");
        }
    }

    private onChestBtnClick = (e) => {
        if(this.controller.gamePhase == GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.chest < 100 && this.controller.myPlayerHP.chest > 0) {
            this.controller.onHealLimbClick("chest");
        }
    }

    private onRArmBtnClick = (e) => {
        if(this.controller.gamePhase == GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.rarm < 100 && this.controller.myPlayerHP.rarm > 0) {
            this.controller.onHealLimbClick("rarm");
        }
    }

    private onLArmBtnClick = (e) => {
        if(this.controller.gamePhase == GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.larm < 100 && this.controller.myPlayerHP.larm > 0) {
            this.controller.onHealLimbClick("larm");
        }
    }

    private onRLegBtnClick = (e) => {
        if(this.controller.gamePhase == GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.rleg < 100 && this.controller.myPlayerHP.rleg > 0) {
            this.controller.onHealLimbClick("rleg");
        }
    }

    private onLLegBtnClick = (e) => {
        if(this.controller.gamePhase == GamePhase.ACTION_PHASE &&
                this.controller.myPlayerHP.lleg < 100 && this.controller.myPlayerHP.lleg > 0) {
            this.controller.onHealLimbClick("lleg");
        }
    }
}
