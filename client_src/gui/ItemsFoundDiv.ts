import { Controller } from "../Controller"

export class ItemsFoundDiv {
    private itemsFoundDiv: HTMLDivElement;
    private itemsFoundList: HTMLDivElement;
    private itemSlot1: HTMLDivElement;
    private dropitemDiv: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.itemsFoundDiv = document.getElementById("items-found-div") as HTMLDivElement;
        this.itemsFoundList = document.getElementById("items-found-list") as HTMLDivElement;
        this.itemSlot1 = document.getElementById("invent-slot-1") as HTMLDivElement;
        this.dropitemDiv = document.getElementById("drop-item-div") as HTMLDivElement;
        this.dropitemDiv.addEventListener("dragenter", this.onDragEnter);
        this.dropitemDiv.addEventListener("dragover", this.onDragOver);
        this.dropitemDiv.addEventListener("drop", this.onDrop);
    }

    private clear() {
        while(this.itemsFoundList.firstChild) {
            this.itemsFoundList.removeChild(this.itemsFoundList.firstChild);
        }
    }

    public setItemsFound(items: Array<{ entityID: string, itemID: string }>) {
        this.clear();
        for(const item of items) {
            const itemObj = this.controller.items[item.itemID];
            let img = document.createElement("img");
            img.id = `item_${item.itemID}_${item.entityID}`;
            img.classList.add("item-found-img");
            img.src = itemObj.image;
            img.title = itemObj.name;
            img.style.width = `${this.itemSlot1.clientWidth*itemObj.gridWidth}px`;
            img.style.height = `${this.itemSlot1.clientHeight*itemObj.gridHeight}px`;
            this.itemsFoundList.appendChild(img);
        }

        this.itemsFoundDiv.style.visibility = items.length > 0 ? "visible" : "hidden";

        for(const item of items) {
            document.getElementById(`item_${item.itemID}_${item.entityID}`).addEventListener("dragstart", this.onDragStart);
            document.getElementById(`item_${item.itemID}_${item.entityID}`).addEventListener("dragend", this.onDragEnd);
        }
    }

    public addItem(entityID: number, itemID: number) {
        const itemObj = this.controller.items[itemID];
        const img = document.createElement("img");
        img.id = `item_${itemID}_${entityID}`;
        img.classList.add("item-found-img");
        img.src = itemObj.image;
        img.title = itemObj.name;
        img.style.width = `${this.itemSlot1.clientWidth*itemObj.gridWidth}px`;
        img.style.height = `${this.itemSlot1.clientHeight*itemObj.gridHeight}px`;
        img.addEventListener("dragstart", this.onDragStart);
        img.addEventListener("dragend", this.onDragEnd);
        this.itemsFoundDiv.style.visibility = "visible";
        this.itemsFoundList.appendChild(img);
    }

    public removeItem(entityID: number, itemID: number) {
        const entity = document.getElementById(`item_${itemID}_${entityID}`);
        if(entity) {
            entity.remove();
            if(this.itemsFoundList.children.length <= 1) {
                this.itemsFoundDiv.style.visibility = "hidden";
            }
        }
    }

    private onDragStart = (e) => {
        const itemObj = this.controller.items[parseInt(e.target.id.split("_")[1], 10)];

        // set the drag-img to the item img, with size relative to item slot size
        // method for setting drag-img size sourced from https://stackoverflow.com/questions/31994572/change-drag-ghost-image-size
        let dragIcon = document.createElement("img");
        dragIcon.src = e.target.src;
        dragIcon.style.width = `${this.itemSlot1.clientWidth*itemObj.gridWidth}px`;
        dragIcon.style.height = `${this.itemSlot1.clientHeight*itemObj.gridHeight}px`;

        let div = document.createElement("div");
        div.id = `drag_${e.target.id}`;
        div.appendChild(dragIcon);
        div.style.position = "absolute";
        div.style.top = "0px";
        div.style.left= `${-this.itemSlot1.clientWidth*itemObj.gridWidth}px`;
        document.querySelector("body").appendChild(div);

        e.dataTransfer.setData("text/plain", e.target.id);
        e.dataTransfer.setDragImage(div,
            this.itemSlot1.clientWidth/2,
            this.itemSlot1.clientHeight/2);
    }

    private onDragEnd = (e) => {
        // when the item has finished being dragged, remove the image in the dom
        document.getElementById(`drag_${e.target.id}`).remove();
        this.controller.onItemDragEnd();
    }

    public enableDropMode() {
        this.dropitemDiv.style.visibility = "visible";
        this.itemsFoundDiv.style.visibility = "hidden";
    }

    public disableDropMode() {
        this.dropitemDiv.style.visibility = "hidden";
        if(this.itemsFoundList.children.length > 1) {
            this.itemsFoundDiv.style.visibility = "visible";
        }
    }

    private onDragEnter = (e) => {
        e.preventDefault();
    }

    private onDragOver = (e) => {
        e.preventDefault();
    }

    private onDrop = (e) => {
        e.preventDefault();
        const entityID = parseInt(e.dataTransfer.getData("text/html").split("_")[3], 10);
        this.controller.dropItem(entityID);
    }
}
