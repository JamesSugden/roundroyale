export class MapCanvasArrow {
    public readonly fromX: number;
    public readonly fromY: number;
    public readonly toX: number;
    public readonly toY: number;
    public readonly text: string;

    public constructor(fromX, fromY, toX, toY, text) {
        this.fromX = fromX;
        this.fromY = fromY;
        this.toX = toX;
        this.toY = toY;
        this.text = text;
    }
}
