import { Controller } from "../Controller"

export class FullOverlay {
    private fullOverlay: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.fullOverlay = document.getElementById("full-overlay") as HTMLDivElement;
        this.fullOverlay.addEventListener(Controller.CLICK_EVENT, this.onClick);
    }

    public hide() {
        this.fullOverlay.style.visibility = "hidden";
    }

    public show() {
        this.fullOverlay.style.visibility = "visible";
    }

    private onClick = (e) => {
        this.hide();
        this.controller.onFullOverlayClick();
    }
}
