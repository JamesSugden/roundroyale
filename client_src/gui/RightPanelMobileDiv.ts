import { Controller } from "../Controller"

export class RightPanelMobileDiv {
    private showCharBtn: HTMLButtonElement;
    private showInventBtn: HTMLButtonElement;
    private charDiv: HTMLDivElement;
    private inventDiv: HTMLDivElement;
    private rightPanel: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.showCharBtn = document.getElementById("show-char-btn") as HTMLButtonElement;
        this.showInventBtn = document.getElementById("show-invent-btn") as HTMLButtonElement;
        this.charDiv = document.getElementById("char-div") as HTMLDivElement;
        this.inventDiv = document.getElementById("invent-div") as HTMLDivElement;
        this.rightPanel = document.getElementById("right-panel") as HTMLDivElement;
        this.showCharBtn.addEventListener(Controller.CLICK_EVENT, this.onShowCharBtnClick);
        this.showInventBtn.addEventListener(Controller.CLICK_EVENT, this.onShowInventBtnClick);
    }

    public hide() {
        this.rightPanel.style.visibility = "hidden";
    }

    private onShowCharBtnClick = (e) => {
        this.showInventBtn.style.display = "block";
        this.showCharBtn.style.display = "none";
        this.inventDiv.style.display = "none";
        this.charDiv.style.display = "block";
    }

    private onShowInventBtnClick = (e) => {
        this.showInventBtn.style.display = "none";
        this.showCharBtn.style.display = "block";
        this.inventDiv.style.display = "block";
        this.charDiv.style.display = "none";
    }
}
