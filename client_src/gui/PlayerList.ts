import { Controller } from "../Controller"

export class PlayerList {
    private playerList: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.playerList = document.getElementById("player-list") as HTMLDivElement;
    }

    // Update the HTML to reflect changes to player list
    public refreshDisplay() {
        let html = "";
        for(const id in this.controller.playerStubs) {
            const player = this.controller.playerStubs[id];
            html += `<button id="player-list-item-${id}"
                        class="player-list-item"
                        style="border-color:${player.Ready ? '#55cc55' : '#cc5555'};">
                        ${player.Name}</button>`;
        }
        this.playerList.innerHTML = html;
    }

    public updateListItem(playerID: string, readyState: boolean) {
        console.log("update player " + playerID + ", " + readyState);
        const listItem = document.getElementById(`player-list-item-${playerID}`);
        if(listItem) {
            // update the list item to reflect the update
            if(readyState) {
                listItem.style.borderColor = "#55cc55";
            } else {
                listItem.style.borderColor = "#cc5555";
            }
        }
    }
}
