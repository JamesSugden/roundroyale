import { Controller } from "../Controller"
import { Action } from "../game/Action"

export class ActionList {
    private actionListContainer: HTMLDivElement;
    private actionList: HTMLDivElement;

    public constructor(private controller: Controller) {
        this.actionListContainer = document.getElementById("action-list-container") as HTMLDivElement;
        this.actionList = document.getElementById("action-list") as HTMLDivElement;
    }

    public appendAction(action: Action, mousex: number, mousey: number) {
        let titleLbl = document.createElement("label");
        titleLbl.innerHTML = action.name;
        titleLbl.style.margin = "5px";
        titleLbl.style.fontSize = "2vw";

        let apLbl = document.createElement("label");
        apLbl.innerHTML = `${action.apCost}AP`;
        apLbl.style.color = "#0000b2";
        apLbl.style.margin = "5px";
        apLbl.style.fontSize = "2vw";

        const totalAPLbl = document.getElementById("action-list-ap-lbl");
        totalAPLbl.innerHTML = `${this.controller.myPlayerAP}AP`;

        this.actionList.appendChild(document.createElement("br"));
        this.actionList.appendChild(titleLbl);
        this.actionList.appendChild(apLbl);
    }

    public resetActionList() {
        // Remove all elements from the div
        while(this.actionList.firstChild) {
            this.actionList.removeChild(this.actionList.firstChild);
        }

        // Re-add the title and AP label
        /*let titleLbl = document.createElement("label");
        titleLbl.innerHTML = "Actions";
        titleLbl.style.margin = "0.25em";

        let apLbl = document.createElement("label");
        apLbl.id = "total-ap-lbl";
        apLbl.innerHTML = `${this.controller.myPlayerAP}AP`;
        apLbl.style.color = "#0000b2";
        apLbl.style.margin = "0.25em";

        this.actionList.appendChild(titleLbl);
        this.actionList.appendChild(apLbl);
        this.actionList.style.visibility = "visible";
        this.actionList.style.display = "block";*/
        this.actionListContainer.style.visibility = "visible";
    }

    public hide() {
        this.actionListContainer.style.visibility = "hidden";
    }
}
