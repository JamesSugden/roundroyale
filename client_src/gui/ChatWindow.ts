import { Controller } from "../Controller"

export class ChatWindow {
    private chatWindow: HTMLDivElement;
    private chatLog: HTMLDivElement;
    private chatInput: HTMLInputElement;
    private chatSendBtn: HTMLButtonElement;

    public constructor(private controller: Controller) {
        this.chatWindow = document.getElementById("chat-window") as HTMLDivElement;
        this.chatLog = document.getElementById("chat-log") as HTMLDivElement;
        this.chatInput = document.getElementById("chat-input") as HTMLInputElement;
        this.chatSendBtn = document.getElementById("chat-send-btn") as HTMLButtonElement;
        //this.chatSendBtn.addEventListener(Controller.CLICK_EVENT, this.onSendBtnClick);
        this.chatInput.addEventListener("keyup", this.onInputEntered);
    }

    public appendMsg(msg: string) {
        let lbl = document.createElement("p");
        lbl.innerHTML = msg;
        lbl.classList.add("chat-window-item");
        this.chatLog.appendChild(lbl);
        //this.chatWindow.insertBefore(lbl, this.chatWindow.lastChild);
        //this.chatWindow.appendChild(lbl);
    }

    private sendMsg(msg: string) {
        if(msg) {
            this.controller.sendMsg(msg);
            this.chatInput.value = "";
        }
    }

    private onSendBtnClick = (e) => {
        this.sendMsg(this.chatInput.value);
    }

    private onInputEntered = (e) => {
        console.log("input entered")
        if(e.keyCode == 13) {
            console.log("enter entered")
            e.preventDefault();
            this.sendMsg(this.chatInput.value);
        }
    }
}
