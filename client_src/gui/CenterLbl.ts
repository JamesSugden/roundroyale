import { Controller } from "../Controller"

export class CenterLbl {
    private centerLbl: HTMLLabelElement;

    public constructor(private controller: Controller) {
        this.centerLbl = document.getElementById("center-lbl") as HTMLLabelElement;
    }

    public show(msg: string, fadeInAndOut: boolean=false) {
        this.centerLbl.innerHTML = msg;

        if(fadeInAndOut) {
            this.centerLbl.style.visibility = "hidden";
            this.centerLbl.classList.remove("fade-in");
            this.centerLbl.classList.add("fade-in-out");
        } else {
            this.centerLbl.style.visibility = "visible";
            this.centerLbl.classList.remove("fade-in-out");
            this.centerLbl.classList.add("fade-in");
        }
    }

    public hide() {
        this.centerLbl.style.visibility = "hidden";
    }
}
