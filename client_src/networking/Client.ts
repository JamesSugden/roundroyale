import { Controller } from "../Controller"
import { fetchMap, fetchPlayerList } from "./HTTPHandler"
import { Map } from "../game/Map"
import { Item, Gun, Projectile, Bandage } from "../game/Item"
import { PlayerDetails } from "../game/PlayerDetails"
import { PlayerHealth } from "../game/PlayerHealth"

// Allows use of js-cookie
declare var Cookies;

export class Client {
    private socket: WebSocket;

    public constructor(private controller: Controller) {
        this.connectToWebSocketServer();
    }

    private connectToWebSocketServer() {
        this.socket = new WebSocket(`ws://${location.hostname}/open_ws_connection`);

        this.socket.onopen = (e) => {
            this.socket.send(`Aname:${Cookies.get("player_name")};token:${Cookies.get("player_token")};lobby:${Cookies.get("lobby_name")}`);
        };

        this.socket.onmessage = (e) => {
            this.onMessage(e.data);
        };
    }

    public sendToggleReadyStateMsg() {
        this.socket.send(`B`);
    }

    public sendSetReadyStateToTrueMsg() {
        this.socket.send(`C`);
    }

    public sendSetMoveTurnMsg(x: number, y: number) {
        this.socket.send(`Gcrouch:false;x:${x};y:${y}`);
    }

    public sendSearchTileMsg(x: number, y: number) {
        this.socket.send(`Mx:${x};y:${y}`);
    }

    public sendTakeItemMsg(entityID: number, backpackX: number, backpackY: number) {
        this.socket.send(`HentityID:${entityID};backpackX:${backpackX};backpackY:${backpackY}`);
    }

    public sendEquipItemMsg(entityID: number) {
        this.socket.send(`IentityID:${entityID}`);
    }

    public sendDropItemMsg(entityID: number) {
        this.socket.send(`KentityID:${entityID}`);
    }

    public sendUseBandageMsg(entityID: number, limb: string) {
        this.socket.send(`LentityID:${entityID};limb:${limb}`);
    }

    public sendAttackPlayerMsg(playerID: string, limb: string) {
        this.socket.send(`JplayerID:${playerID};limb:${limb}`);
    }

    public sendEndTurnMsg() {
        this.socket.send(`D`);
    }

    public sendSendMsgMsg(msg: string) {
        this.socket.send(`Emsg:${msg}`);
    }

    // Called upon receiving a message from the server
    private onMessage(msg: string) {
        console.log("received: " + msg);
        if(msg == "update:players") {
            this.processPlayersUpdate();
        } else if(msg.startsWith("update:ready;")) {
            this.processReadyStateUpdate(msg);
        } else if(msg.startsWith("update:game;")) {
            this.processGameUpdate(msg);
        } else if(msg.startsWith("update:dead;")) {
            this.processDeadPlayersUpdate(msg);
        } else if(msg.startsWith("update:winners;")) {
            this.processWinnersUpdate(msg);
        } else if(msg.startsWith("update:storm;")) {
            this.processStormUpdate(msg);
        } else if(msg.startsWith("yourid;")) {
            this.processMyIDUpdate(msg);
        } else if(msg.startsWith("phasetime;")) {
            this.processPhaseTimeUpdate(msg);
        } else if(msg.startsWith("map;")) {
            this.processMapUpdate(msg);
        } else if(msg.startsWith("items;")) {
            this.processItemsList(msg);
        } else if(msg == "request:drop") {
            this.processDropPhase();
        } else if(msg == "request:move") {
            this.processMovePhase();
        } else if(msg == "request:action") {
            this.processActionPhase();
        } else if(msg.startsWith("takeitem:valid;")) {
            this.processTakeItemValid(msg);
        } else if(msg == "takeitem:invalid;") {
            console.log("take item invalid");
        } else if(msg.startsWith("equipitem:valid;")) {
            this.processEquipItemValid(msg);
        } else if(msg == "equipitem:invalid;") {
            console.log("equip item invalid");
        } else if(msg.startsWith("dropitem:valid;")) {
            this.processDropItemValid(msg);
        } else if(msg == "dropitem:invalid;") {
            console.log("drop item invalid");
        } else if(msg == "move:valid") {
            console.log("move valid");
        } else if(msg == "move:invalid") {
            console.log("move invalid");
        } else if(msg == "attack:valid") {
            this.controller.onAttackValid();
        } else if(msg == "attack:invalid") {
            console.log("attack invalid");
        } else if(msg.startsWith("usebandage:valid;")) {
            this.processUseBandageValid(msg);
        } else if(msg == "usebandage:invalid;") {
            console.log("use bandage invalid");
        } else if(msg == "search:valid;") {
            this.controller.onSearchValid();
        } else if(msg == "search:invalid;") {
            console.log("search invalid");
        } else if(msg.startsWith("disconnect;")) {
            this.processDisconnect(msg);
        } else if(msg.startsWith("message;")) {
            this.processPlayerMsg(msg);
        }
    }

    private processDropPhase() {
        this.controller.onDropPhaseStart();
    }

    private processMovePhase() {
        this.controller.onMovePhaseStart();
    }

    private processActionPhase() {
        this.controller.onActionPhaseStart();
    }

    private processGameUpdate(msg: string) {
        var data = msg.split("update:game;").length > 1 ? msg.split("update:game;")[1] : null;
        if(data) {
            const splits = data.split(";");
            let items = new Array<{ entityID: string, itemID: string }>();
            let newPlayerDetails: { [id: number]: PlayerDetails } = {};
            let newPlayerHP: { [id: number]: PlayerHealth } = {};

            // extract relevant information from data string
            for(const elem of splits) {
                if(elem.startsWith("details:player,")) {
                    const details = elem.split("details:player,");
                    if(details.length > 1) {
                        const p = this.processPlayerDetailsUpdate(details[1]);
                        if(p && p.id && p.details) {
                            newPlayerDetails[p.id] = p.details;
                        }
                    }
                } else if(elem.startsWith("details:item,")) {
                    const details = elem.split("details:item,");
                    if(details.length > 1) {
                        const item = this.processItemsFound(details[1]);
                        if(item) {
                            items.push(item);
                        }
                    }
                } else if(elem.startsWith("details:modifiers,")) {
                    const details = elem.split("details:modifiers,");
                    if(details.length > 1) {
                        this.processMyPlayerModifiersUpdate(details[1]);
                    }
                } else if(elem.startsWith("details:hp,")) {
                    const details = elem.split("details:hp,");
                    if(details.length > 1) {
                        const p = this.processPlayerHPUpdate(details[1]);
                        if(p && p.id && p.hp) {
                            newPlayerHP[p.id] = p.hp;
                        }
                    }
                }
            }

            this.controller.playerDetails = newPlayerDetails;
            this.controller.playerHPs = newPlayerHP;
            this.controller.setItemsFound(items);
            this.controller.requestDrawMap();
        }
    }

    // Process list of dead players
    private processDeadPlayersUpdate(msg: string) {
        const data = msg.split(";");
        for(let index = 1; index < data.length; index++) {
            const playerID = parseInt(data[index], 10);
            this.controller.onPlayerDead(playerID);
        }
    }

    // Process list of winners
    private processWinnersUpdate(msg: string) {
        const data = msg.split(";");
        let winnerIDs = new Array<number>();
        for(let index = 1; index < data.length; index++) {
            const playerID = parseInt(data[index], 10);
            winnerIDs.push(playerID)
        }
        this.controller.onGameOver(winnerIDs);
    }

    // Process update to the nuclear storm
    private processStormUpdate(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "update:storm;");
        if(data) {
            if(data.approach) {
                this.controller.onStormApproachUpdate();
            }
            if(data.radius) {
                const radius = parseInt(data.radius, 10);
                if(radius != null) {
                    this.controller.onStormRadiusUpdate(radius);
                }
            }
            if(data.dmg && data.sickDmg) {
                const dmgPerTurn = parseInt(data.dmg, 10);
                const sickDmgPerTurn = parseInt(data.sickdmg, 10);
                if(dmgPerTurn != null && sickDmgPerTurn != null) {
                    this.controller.onStormDmgUpdate(dmgPerTurn, sickDmgPerTurn);
                }
            }
        }
    }

    // Process an individual player's details update
    private processPlayerDetailsUpdate(elem: string): { id: string, details: PlayerDetails } {
        const data = this.dissectMultiPartMsg(elem, null, ",");
        if(data) {
            if(data.id && data.x && data.y && data.c) {
                const x = parseInt(data.x, 10);
                const y = parseInt(data.y, 10);
                const crouch = data.crouch == "true";
                return { id: data.id, details: { x: x, y: y, crouch: crouch } };
            }
        }
        return null;
    }

    // Process an update to the items on the current tile
    private processItemsFound(elem: string): { entityID: string, itemID: string } {
        const data = this.dissectMultiPartMsg(elem, null, ",");
        if(data) {
            if(data.entityID && data.itemID) {
                console.log("found item", data.entityID, data.itemID);
                return { entityID: data.entityID, itemID: data.itemID };
            }
        }
        return null;
    }

    // Process an update to my player's modifiers list
    private processMyPlayerModifiersUpdate(elem: string) {
        const data = this.dissectMultiPartMsg(elem, null, ",");
        if(data) {
            if(data.viewRange && data.hitChance && data.viewRangeDesc!=null && data.hitChanceDesc!=null) {
                const viewRange = parseInt(data.viewRange, 10);
                const hitChance = parseInt(data.hitChance, 10);
                const totalAP = parseInt(data.totalAP, 10);
                const apMoveCost = parseInt(data.apMoveCost, 10);
                const dmgPerMove = parseInt(data.dmgPerMove, 10);
                const sickDmgPerTurn = parseInt(data.sickDmgPerTurn, 10);
                let viewRangeDesc = new Array<string>();
                let hitChanceDesc = new Array<string>();
                let totalAPDesc = new Array<string>();
                let apMoveCostDesc = new Array<string>();
                let dmgPerMoveDesc = new Array<string>();
                let sickDmgPerTurnDesc = new Array<string>();
                // get the reasons for the view range modifier
                for(let desc of data.viewRangeDesc.split("~")) {
                    if(desc)    viewRangeDesc.push(desc);
                }
                // get the reasons for the hit chance modifier
                for(let desc of data.hitChanceDesc.split("~")) {
                    if(desc)    hitChanceDesc.push(desc);
                }
                // get the reasons for the total ap modifier
                for(let desc of data.totalAPDesc.split("~")) {
                    if(desc)    totalAPDesc.push(desc);
                }
                // get the reasons for the ap move cost modifier
                for(let desc of data.apMoveCostDesc.split("~")) {
                    if(desc)    apMoveCostDesc.push(desc);
                }
                // get the reasons for the dmg per move modifier
                for(let desc of data.dmgPerMoveDesc.split("~")) {
                    if(desc)    dmgPerMoveDesc.push(desc);
                }
                // get the reasons for the sickness dmg per turn modifier
                for(let desc of data.sickDmgPerTurnDesc.split("~")) {
                    if(desc)    sickDmgPerTurnDesc.push(desc);
                }
                // update my players modifiers with the new information
                this.controller.myPlayerModifiers = {
                    viewRange: viewRange,
                    hitChance: hitChance,
                    totalAP: totalAP,
                    apMoveCost: apMoveCost,
                    dmgPerMove: dmgPerMove,
                    sickDmgPerTurn: sickDmgPerTurn,
                    viewRangeDesc: viewRangeDesc,
                    hitChanceDesc: hitChanceDesc,
                    totalAPDesc: totalAPDesc,
                    apMoveCostDesc: apMoveCostDesc,
                    dmgPerMoveDesc: dmgPerMoveDesc,
                    sickDmgPerTurnDesc: sickDmgPerTurnDesc
                };
            }
        }
    }

    // Process an update to my player's health
    private processPlayerHPUpdate(elem: string): { id: string, hp: PlayerHealth } {
        const data = this.dissectMultiPartMsg(elem, null, ",");
        if(data) {
            if(data.id && data.total && data.head && data.chest && data.rarm && data.larm && data.rleg && data.lleg) {
                const playerID = data.id;
                const total = parseInt(data.total, 10);
                const head = parseInt(data.head, 10);
                const chest = parseInt(data.chest, 10);
                const rarm = parseInt(data.rarm, 10);
                const larm = parseInt(data.larm, 10);
                const rleg = parseInt(data.rleg, 10);
                const lleg = parseInt(data.lleg, 10);
                // update the players hp with the new information
                return {
                    id: playerID,
                    hp: {
                        total: total,
                        head: head,
                        chest: chest,
                        rarm: rarm,
                        larm: larm,
                        rleg: rleg,
                        lleg: lleg
                    } as PlayerHealth
                };
            }
        }
        return null;
    }

    // Process an update to the player list
    private processPlayersUpdate() {
        fetchPlayerList(Cookies.get("lobby_name"), (xmlHttp) => {
            // on success
            const p = JSON.parse(xmlHttp.responseText);
            if(p) {
                this.controller.playerStubs = p;
            }
        }, (xmlHttp) => {
            // on error
            console.log(xmlHttp.responseText);
        });
    }

    // Process a valid take item request
    private processTakeItemValid(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "takeitem:valid;");
        if(data) {
            // message must contain entity id and item id to be usable ...
            if(data.entityID && data.itemID && data.firstSlotX && data.firstSlotY) {
                const entityID = parseInt(data.entityID, 10);
                const itemID = parseInt(data.itemID, 10);
                const firstSlotX = parseInt(data.firstSlotX, 10);
                const firstSlotY = parseInt(data.firstSlotY, 10);
                this.controller.onTakeItemValid(entityID, itemID, firstSlotX, firstSlotY);
            }
        }
    }

    // Process a valid equip item request
    private processEquipItemValid(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "equipitem:valid;");
        if(data) {
            // message must contain entity id and item id to be usable
            if(data.entityID && data.itemID) {
                const entityID = parseInt(data.entityID, 10);
                const itemID = parseInt(data.itemID, 10);
                this.controller.onEquipItemValid(entityID, itemID);
            }
        }
    }

    // Process a valid drop item request
    private processDropItemValid(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "dropitem:valid;");
        if(data) {
            // message must contain entity id and item id to be usable
            if(data.entityID && data.itemID) {
                const entityID = parseInt(data.entityID, 10);
                const itemID = parseInt(data.itemID, 10);
                this.controller.onDropItemValid(entityID, itemID);
            }
        }
    }

    // Process a valid use bandage request
    private processUseBandageValid(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "usebandage:valid;");
        if(data) {
            if(data.entityID && data.itemID) {
                const entityID = parseInt(data.entityID, 10);
                const itemID = parseInt(data.itemID, 10);
                this.controller.onUseBandageValid(entityID, itemID);
            }
        }
    }

    // Dissect a ready state update message and make relevant GUI changes
    private processReadyStateUpdate(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "update:ready;");
        if(data) {
            // message must contain player and state to be usable
            if(data.player && data.ready) {
                this.controller.onReadyStateUpdate(data.player, data.ready == "true" ? true : false);
            }
        }
    }

    // Called when a phase time update is received from the server
    private processPhaseTimeUpdate(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "phasetime;");
        if(data) {
            // message must contain droptime, movetime and actiontime to be usable
            if(data.droptime && data.movetime && data.actiontime) {
                this.controller.onPhaseTimesUpdate(data.droptime, data.movetime, data.actiontime);
            }
        }
    }

    // Called when the name of the next map to be played is received from the server
    private processMapUpdate(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "map;");

        // message must contain mapName to be usable
        if(data.name) {
            // fetch the map from the server
            fetchMap(data.name, (xmlHttp) => {
                // on success
                const m = new Map(this.controller, xmlHttp.responseText);
                this.controller.map = m;
                this.controller.onMapLoaded();
            }, (xmlHttp) => {
                // on error
                alert("Fatal Error: Failed to load map " + data.mapName);
            });
        }
    }

    // Called when the item list for this game is received from the server
    private processItemsList(msg: string) {
        const data = msg.split("items;").length > 1 ? msg.split("items;")[1] : null;
        if(data) {
            const splits = data.split(";");
            let items = new Array<Item>();

            // extract relevant information from data string
            for(const elem of splits) {
                const itemObj = this.dissectMultiPartMsg(elem, null, ",");
                let item;
                if(itemObj.shotsPerAction) {
                    item = {
                        name: itemObj.name,
                        image: itemObj.image,
                        gridWidth: parseInt(itemObj.gridw, 10) || 1,
                        gridHeight: parseInt(itemObj.gridh, 10) || 1,
                        shotsPerAction: parseFloat(itemObj.shotsPerAction) || 0,
                        dmgPerHit: parseFloat(itemObj.shotsPerAction) || 0,
                        maxRange: parseFloat(itemObj.maxRange) || 0,
                        accuracy: parseFloat(itemObj.accuracy) || 0
                    } as Gun;
                } else if(itemObj.timeToThrow) {
                    item = {
                        name: itemObj.name,
                        image: itemObj.image,
                        gridWidth: parseInt(itemObj.gridw, 10) || 1,
                        gridHeight: parseInt(itemObj.gridh, 10) || 1,
                        timeToThrow: parseFloat(itemObj.timeToThrow) || 0,
                        dmgPerHit: parseFloat(itemObj.dmgPerHit) || 0
                    } as Projectile;
                } else if(itemObj.healAmount) {
                    item = {
                        name: itemObj.name,
                        image: itemObj.image,
                        gridWidth: parseInt(itemObj.gridw, 10) || 1,
                        gridHeight: parseInt(itemObj.gridh, 10) || 1,
                        healAmount: parseInt(itemObj.healAmount, 10) || 0
                    } as Bandage;
                }
                items.push(item);
            }

            this.controller.onItemsLoaded(items);
        }
    }

    // Called when the ID of the player using this client is received from the server
    private processMyIDUpdate(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "yourid;");
        if(data) {
            if(data.id) {
                this.controller.onMyIDUpdate(data.id);
            }
        }
    }

    // Called when a player disconnects from the game
    private processDisconnect(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "disconnect;");
        if(data && data.id) {
            this.controller.onPlayerDisconnect(data.id);
        }
    }

    // Called upon receiving a msg from another player
    private processPlayerMsg(msg: string) {
        const data = this.dissectMultiPartMsg(msg, "message;");
        if(data && data.id && data.msg) {
            this.controller.onMsgReceived(data.id, data.msg);
        }
    }

    private dissectMultiPartMsg(msg: string, cutAt: string, sep: string=";"): { [key: string]: string } {
        const data = msg.split(cutAt).length > 1 ? msg.split(cutAt)[1] : msg;
        let map: { [key: string]: string } = {};

        if(data) {
            const splits = data.split(sep);

            // extract relevant information from data string
            for(const elem of splits) {
                const parts = elem.split(":");
                if(parts.length > 1) {
                    const key = parts[0];
                    const value = parts[1];
                    map[key] = value;
                }
            }
        }

        return map;
    }

}
