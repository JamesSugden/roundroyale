function sendGetRequest(URL, success, error) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if(xmlHttp.readyState == 4) {
            if(xmlHttp.status >= 200 && xmlHttp.status < 300) {
                if(typeof(success) === "function") {
                    success(xmlHttp);
                }
            } else {
                if(typeof(error) === "function") {
                    error(xmlHttp);
                }
            }
        }
    }

    // send the ajax request
    xmlHttp.open("GET", URL, true);   // true for asynchronous
    xmlHttp.send(null);
}

export function fetchMap(mapName: string, success: ((x: XMLHttpRequest) => void), error: ((x: XMLHttpRequest) => void)) {
    sendGetRequest(`/maps/${mapName}.txt`, (xmlHttp) => {
        // on success
        if(success) {
            success(xmlHttp);
        }
    }, (xmlHttp) => {
        // on error
        if(error) {
            error(xmlHttp);
        }
    });
}

export function fetchPlayerList(lobbyName: string, success: ((x: XMLHttpRequest) => void), error: ((x: XMLHttpRequest) => void)) {
    sendGetRequest(`/players?lobby_name=${lobbyName}`, (xmlHttp) => {
        // on success
        if(success) {
            success(xmlHttp);
        }
    }, (xmlHttp) => {
        // on error
        if(error) {
            error(xmlHttp);
        }
    });
}
