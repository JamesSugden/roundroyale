package main

import (
    "log"
    "net/http"
    "routes"
    "networking"
)

// Initialise network routes.
func main() {
    // Allow static content to be served, i.e. webpages
    fs := http.FileServer(http.Dir("static"))
    http.Handle("/", fs)

    fs2 := http.FileServer(http.Dir("dist/static"))
    http.Handle("/experimental/", http.StripPrefix("/experimental/", fs2))

    // Add routes
    http.HandleFunc("/games", routes.GetLobbyList)
    http.HandleFunc("/games/create", routes.CreateLobby)
    http.HandleFunc("/games/join", routes.JoinLobby)
    http.HandleFunc("/players", routes.GetPlayerList)
    http.HandleFunc("/open_ws_connection", networking.OpenWebSocketConnection)

    log.Println("Server started")

    // Listen on port 80
    if err := http.ListenAndServe(":80", nil); err != nil {
        panic(err)
    }
}
