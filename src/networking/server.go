package networking

import (
    "log"
    "net"
    "time"
    "state"
    "game"
    "game/item"
    "game/turn"
    "strings"
    "net/http"
    "strconv"
    "github.com/gobwas/ws"
    "github.com/gobwas/ws/wsutil"
)

const (
    DropPhaseTime = 8
    MovePhaseTime = 5
    ActionPhaseTime = 8
)

// Open a web socket connection.
// With help from https://github.com/gobwas/ws/blob/master/example/autobahn/autobahn.go
func OpenWebSocketConnection(w http.ResponseWriter, r *http.Request) {
    conn, _, _, err := ws.UpgradeHTTP(r, w, nil)
    if err != nil {
        log.Printf("upgrade error: %s", err)
        return
    }
    defer conn.Close()

    // The player the connection belongs to
    // Starts out as nil and is only set once the connection is validated
    var player *game.Player

    // The game lobby the connection belongs to
    // Starts out as nil and is only set once the connection is validated
    var game *game.Game

    for {
        // Read message from the client
        bts, _, err := wsutil.ReadClientData(conn)
        if err != nil {
            log.Printf("read message error: %v", err)
            closePlayerConnection(game, player)
            return
        }

        if p, g := processMessage(bts, conn, player, game); p != nil && g != nil {
            player = p
            game = g
        }
    }
}

// Server responsible for the game loop.
// Server waits 30 seconds between turn.
func gameLoop(g *game.Game) {
    // Accquire exclusive access to Game object
    g.Access.Lock()

    // Before the standard turns can start, each player must choose a drop location
    sendGameMessage(g, "request:drop")

    // Release access to Game object
    g.Access.Unlock()

    // Wait for players to submit their drop commands
    // Returns true iff the lobby should be closed
    if allowPlayerCommands(g, DropPhaseTime) {
        return
    }

    // Accquire exclusive access to Game object
    g.Access.Lock()

    // Tell the game to process drop phase
    g.DoDropPhase()

    // Notify players of changes
    sendPlayerUpdateMessages(g)

    // Release access to Game object
    g.Access.Unlock()

    // Wait for all players to send ready signal
    waitForPlayers(g, 1000, 40)

    // Accquire exclusive access to Game object
    g.Access.Lock()

    for {
        //log.Println("Game Tick", g.GetName())

        // Update the storm and send all players an update msg
        updateStorm(g)

        // Reset all players move turns before asking for new ones
        g.ResetMoveTurns()

        // Set the game phase to the move phase
        g.Phase = game.MOVE_PHASE

        // Do the movement phase
        // First, request the actions from each client
        sendGameMessage(g, "request:move")

        // Release access to Game object
        g.Access.Unlock()

        // Wait for players to submit their movement commands
        // Returns true iff the lobby should be closed
        if allowPlayerCommands(g, MovePhaseTime) {
            return
        }

        // Accquire exclusive access to Game object
        log.Println("Accquiring Move Phase Lock")
        g.Access.Lock()
        log.Println("Lock Accquired")

        // Tell the game to process the movement phase
        g.DoMovePhase()

        // Do intermediary storm phase
        deadPlayers, winners := g.DoStormPhase()

        // Notify all players of those who have died
        sendDeadPlayersUpdateMessage(g, deadPlayers)

        // Notify players of changes
        sendPlayerUpdateMessages(g)

        // Release access to Game object
        g.Access.Unlock()

        // Wait for all players to send ready signal
        waitForPlayers(g, 1000, 40)

        // Accquire exclusive access to Game object
        g.Access.Lock()

        // Reset all players action turns before asking for new ones
        g.ResetActionTurns()

        // Set the game phase to the action phase
        g.Phase = game.ACTION_PHASE

        // Do the action phase
        // First, request the actions from each client
        sendGameMessage(g, "request:action")

        // Release access to Game object
        g.Access.Unlock()

        // Wait for players to submit their action commands
        // Returns true iff the lobby should be closed
        if allowPlayerCommands(g, ActionPhaseTime) {
            return
        }

        // Accquire exclusive access to Game object
        log.Println("Accquiring Action Phase Lock")
        g.Access.Lock()
        log.Println("Lock Accquired")

        // Tell the game to process the action phase
        deadPlayers, winners = g.DoActionPhase()

        // Notify all players of those who have died
        sendDeadPlayersUpdateMessage(g, deadPlayers)

        // Notify players of changes
        sendPlayerUpdateMessages(g)

        // If there are winners, notify players of the winners
        // Returns true iff the game is over
        gameOver := sendWinnersMessage(g, winners)

        // If the game is over, close the lobby and break from the game loop
        if gameOver {
            log.Println("Game Finished, Closing Lobby", g.GetName())
            state.RemoveGame(g.GetName())
            g.Access.Unlock()
            break
        }

        // Release access to Game object
        g.Access.Unlock()

        // Wait for all players to send ready signal
        waitForPlayers(g, 1000, 40)

        // Accquire exclusive access to Game object
        g.Access.Lock()
    }
}

// Idle the game thread and allow player's to submit commands
func allowPlayerCommands(g *game.Game, phaseTime time.Duration) bool {
    // Clear the channel, i.e. ignore un-processed end turn requests
    /*for len(g.InterruptChan) > 0 {
        <-g.InterruptChan
    }*/
    // BUG - If interrupt is sent to InterruptChan as time.After is called, program can deadlock since Access is never unlocked
    select {
    case closeLobby := <-g.InterruptChan:
        return closeLobby
    case <-time.After(phaseTime * time.Second):
        return false
    }
}

// Run a single phase of the game
// Process input from users and returns when time is up or every player has ended turn
/*func gamePhase(g *game.Game, phaseTime time.Duration) {
    //startTime := time.Now()
    // Continue to accept player instructions until time is up or all players are ready
    for {
        elapsedTime := time.Since(startTime)
        select {
        case f := <-g.ExecuteChan:
            // Execute a function from a player
            f()
        case <-g.InterruptChan:
            return
        case <-time.After(phaseTime * time.Second - elapsedTime):
            return
        }
    }
}*/

// Wait for all players to be ready before returning
// Returns once or players are ready or once timeout is reached
// TODO - might slow down under heavy server load - change to real time ???
func waitForPlayers(g *game.Game, checkInterval time.Duration, maxNumChecks int) {
    // Wait for all players to be ready
    // Max wait time = maxNumChecks * checkInterval ms
    for numChecks := 0; numChecks < maxNumChecks; numChecks++ {
        // Check every half a second if all players are ready
        if areAllPlayersReady(g) {
            return
        }
        //log.Println(checkInterval * time.Millisecond)
        time.Sleep(checkInterval * time.Millisecond)
    }
}

// Update the storm then send updates to players
func updateStorm(g *game.Game) {
    approachUpdate, radiusUpdate, dmgUpdate := g.UpdateStorm()

    if approachUpdate || radiusUpdate || dmgUpdate {
        msg := "update:storm"
        if approachUpdate {
            msg += ";approach:true"
        }
        if radiusUpdate {
            msg += ";radius:"+strconv.FormatInt(int64(g.Storm.Radius), 10)
        }
        if dmgUpdate {
            msg += ";dmg"+strconv.FormatInt(int64(g.Storm.DmgPerTurn), 10)+
                   ";sickdmg"+strconv.FormatInt(int64(g.Storm.SicknessDmgPerTurn), 10)
        }
        sendGameMessage(g, msg)
    }
}

// Send a personalised update message to each player in the game
func sendPlayerUpdateMessages(g *game.Game) {
    // Construct a unique message for each player
    // Prevents players knowing information they shouldn't
    for _, p := range g.GetPlayers() {
        // Build a string containing the message
        msg := "update:game"
        // Determine which players info should be included
        for _, player := range g.GetPlayers() {
            if p.CanSeePlayer2(player) && !player.IsDead() {
                // Include player details, e.g. location
                msg += ";details:player,id:"+strconv.FormatInt(player.ID, 10)+",x:"+strconv.FormatInt(player.GetX(), 10)+
                        ",y:"+strconv.FormatInt(player.GetY(), 10)+",c:"+strconv.FormatBool(player.IsCrouching())
                // Include player health stats
                msg += ";details:hp,id:"+strconv.FormatInt(player.ID, 10)+
                        ",total:"+strconv.FormatInt(player.GetHealth().TotalHP, 10)+
                        ",head:"+strconv.FormatInt(player.GetHealth().HeadHP, 10)+
                        ",chest:"+strconv.FormatInt(player.GetHealth().ChestHP, 10)+
                        ",rarm:"+strconv.FormatInt(player.GetHealth().RArmHP, 10)+
                        ",larm:"+strconv.FormatInt(player.GetHealth().LArmHP, 10)+
                        ",rleg:"+strconv.FormatInt(player.GetHealth().RLegHP, 10)+
                        ",lleg:"+strconv.FormatInt(player.GetHealth().LLegHP, 10)
            }
        }

        // Get the items from the tile the player p is currently stood on
        itemsOnGround := g.ItemsOnGround[p.GetX() + p.GetY()*g.MapWidth]

        // Tell the player about the items, if there is any
        for _, itemEntity := range itemsOnGround {
            // TODO - send all items at start of game then refer to them by id here
            msg += ";details:item,itemID:"+strconv.FormatInt(int64(itemEntity.ItemID), 10)+
                    ",entityID:"+strconv.FormatInt(int64(itemEntity.EntityID), 10)
        }

        // Add the list of reasons for view range modifier to the msg
        viewRangeDesc := ",viewRangeDesc:"
        for _, desc := range p.GetModifiers().ViewRangeDesc {
            viewRangeDesc += "~"+desc
        }

        // Add the list of reasons for hit chance modifier to the msg
        hitChanceDesc := ",hitChanceDesc:"
        for _, desc := range p.GetModifiers().HitChanceDesc {
            hitChanceDesc += "~"+desc
        }

        // Add the list of reasons for total ap modifier to the msg
        totalAPDesc := ",totalAPDesc:"
        for _, desc := range p.GetModifiers().TotalAPDesc {
            totalAPDesc += "~"+desc
        }

        // Add the list of reasons for hit chance modifier to the msg
        apMoveCostDesc := ",apMoveCostDesc:"
        for _, desc := range p.GetModifiers().APMoveCostDesc {
            apMoveCostDesc += "~"+desc
        }

        // Add the list of reasons for dmg per move modifier to the msg
        dmgPerMoveDesc := ",dmgPerMoveDesc:"
        for _, desc := range p.GetModifiers().DmgPerMoveDesc {
            dmgPerMoveDesc += "~"+desc
        }

        // Add the list of reasons for dmg per turn modifier to the msg
        sickDmgPerTurnDesc := ",sickDmgPerTurnDesc:"
        for _, desc := range p.GetModifiers().SickDmgPerTurnDesc {
            sickDmgPerTurnDesc += "~"+desc
        }

        // Send the player their modifier information
        msg += ";details:modifiers,viewRange:"+strconv.FormatInt(p.GetModifiers().ViewRange, 10) +
                ",hitChance:"+strconv.FormatInt(p.GetModifiers().HitChance, 10) +
                ",totalAP:"+strconv.FormatInt(p.GetModifiers().TotalAP, 10) +
                ",apMoveCost:"+strconv.FormatInt(p.GetModifiers().APMoveCost, 10) +
                ",dmgPerMove:"+strconv.FormatInt(p.GetModifiers().DmgPerMove, 10) +
                ",sickDmgPerTurn:"+strconv.FormatInt(p.GetModifiers().SickDmgPerTurn, 10) +
                viewRangeDesc + hitChanceDesc + totalAPDesc +
                apMoveCostDesc + dmgPerMoveDesc + sickDmgPerTurnDesc

        //log.Println("Sending Update Message")
        sendPlayerMessage(p, g, msg)
    }
}

// Send a message to every player notifying them of who has died this turn
func sendDeadPlayersUpdateMessage(g *game.Game, deadPlayers []*game.Player) {
    if len(deadPlayers) > 0 {
        msg := "update:dead"
        for _, d := range deadPlayers {
            msg += ";" + strconv.FormatInt(d.ID, 10)
        }
        sendGameMessage(g, msg)
    }
}

// Send a message to every player containing a list of winners
// Returns true iff there are winners and the game is over
func sendWinnersMessage(g *game.Game, winners []*game.Player) bool {
    // Only send winners message if there are winners
    if len(winners) > 0 {
        msg := "update:winners"
        for _, w := range winners {
            msg += ";" + strconv.FormatInt(w.ID, 10)
        }
        sendGameMessage(g, msg)
        return true
    }
    return false
}

// Send a message to the player saying whether their item picking up was valid
func sendItemTakenResponseMsg(p *game.Player, g *game.Game, itemEntity item.ItemEntity, firstSlotX int, firstSlotY int, valid bool) {
    if valid {
        sendPlayerMessage(p, g, "takeitem:valid;entityID:"+strconv.FormatInt(int64(itemEntity.EntityID), 10)+
                                ";itemID:"+strconv.FormatInt(int64(itemEntity.ItemID), 10)+
                                ";firstSlotX:"+strconv.FormatInt(int64(firstSlotX), 10)+
                                ";firstSlotY:"+strconv.FormatInt(int64(firstSlotY), 10))
    } else {
        sendPlayerMessage(p, g, "takeitem:invalid;")
    }
}

// Send a message to the player saying whether their item dropping up was valid
func sendItemDroppedResponseMsg(p *game.Player, g *game.Game, itemEntity item.ItemEntity, valid bool) {
    if valid {
        sendPlayerMessage(p, g, "dropitem:valid;entityID:"+strconv.FormatInt(int64(itemEntity.EntityID), 10)+
                                ";itemID:"+strconv.FormatInt(int64(itemEntity.ItemID), 10))
    } else {
        sendPlayerMessage(p, g, "dropitem:invalid;")
    }
}

// Send a message to every player in a game with validated TCP connection.
func sendGameMessage(game *game.Game, msg string) {
    if game != nil {
        for _, player := range game.GetPlayers() {
            if player.Conn != nil {
                // Send a message to the client
                err := wsutil.WriteServerMessage(player.Conn, ws.OpText, []byte(msg))
                if err != nil {
                    log.Printf("write message error: %v", err)
                    closePlayerConnection(game, player)
                    return
                }
            }
        }
    }
}

// Send a message to a single player in a game with validated TCP connection.
func sendPlayerMessage(player *game.Player, game *game.Game, msg string) {
    if player != nil {
        if player.Conn != nil {
            // Send a message to the client
            err := wsutil.WriteServerMessage(player.Conn, ws.OpText, []byte(msg))
            if err != nil {
                log.Printf("write message error: %v", err)
                closePlayerConnection(game, player)
                return
            }
        }
    }
}

// Closes the websocket connection for a player and removes them from the game
// If all players sockets are closed, the game lobby will be destroyed
func closePlayerConnection(g *game.Game, player *game.Player) {
    if g == nil {
        return
    }
    g.Access.Lock()
    defer g.Access.Unlock()
    if player != nil && player.Conn != nil {
        err := player.Conn.Close()
        if err == nil {
            g.RemovePlayer(player)
            log.Println("Removed player", player.Name)
        }
        if len(g.GetPlayers()) == 0 {
            // No players left, therefore, close lobby
            log.Println("Closing lobby", g.GetName())
            state.RemoveGame(g.GetName())
            g.InterruptChan <-true
        } else {
            // Send all players in the game an update players message
            sendGameMessage(g, "disconnect;id:"+strconv.FormatInt(player.ID, 10))
        }
    }
}

// Checks whether every player is ready for the next turn
// If all players are ready, their ready states are set to false
// Returns true iff all players have their Ready flag set to true
func areAllPlayersReady(g *game.Game) bool {
    for _, player := range g.GetPlayers() {
        if !player.Ready && !player.IsDead() {
            return false
        }
    }
    // All ready flags set to true
    // Set all ready flags to false for next time
    for _, player := range g.GetPlayers() {
        player.Ready = false
    }
    return true
}

func processMessage(bts []byte, conn net.Conn, player *game.Player, game *game.Game) (*game.Player, *game.Game) {
    // Convert the byte array to a string
    msg := string(bts[:len(bts)])

    log.Println(msg)

    // Every msg needs an opcode
    if len(msg) == 0 {
        return nil, nil
    }

    // Get the Op-Code from the string (as a byte)
    opCode := msg[0]

    if opCode == 65 {
        // OpCode "A" means new connection
        return processNewConnectionMessage(msg, conn)
    }

    // Following messages are only valid from validated players
    if player != nil && game != nil && !player.IsDead() {
        if opCode == 66 {
            // OpCode "B" means toggle ready state
            processReadyStateUpdate(player, game)
        } else if opCode == 67 {
            // OpCode "C" means set ready state to true
            processSetReadyStateTrue(player, game)
        } else if opCode == 68 {
            // OpCode "D" means end turn early
            processEndTurn(player, game)
        } else if opCode == 69 {
            // OpCode "E" means send msg
            processPlayerMsg(msg, player, game)
        } else if opCode == 71 {
            // OpCode "G" means set move turn
            processSetMoveTurn(msg, player, game.Phase, game)
        } else if opCode == 72 {
            // OpCode "H" means take item from ground
            processItemTaken(msg, player, game)
        } else if opCode == 73 {
            // OpCode "I" means equip item in backpack
            processEquipItem(msg, player, game)
        } else if opCode == 74 {
            // OpCode "J" means attack player
            processAttackPlayer(msg, player, game)
        } else if opCode == 75 {
            // OpCode "K" means drop item on ground
            processItemDropped(msg, player, game)
        } else if opCode == 76 {
            // OpCode "L" means use bandage on limb
            processUseBandage(msg, player, game)
        } else if opCode == 77 {
            // OpCode "M" means search tile
            processSearchTile(msg, player, game)
        }
    }

    return nil, nil
}

func processReadyStateUpdate(player *game.Player, game *game.Game) {
    // If the game hasn't started, use the ready update to signal they are ready for match to start
    game.Access.Lock()
    defer game.Access.Unlock()
    if !game.HasStarted() {
        player.Ready = !player.Ready
        // Send all players in the game ready state of player
        sendGameMessage(game, "update:ready;player:"+strconv.FormatInt(player.ID, 10)+";ready:"+strconv.FormatBool(player.Ready))
        // Check whether the game can start
        if areAllPlayersReady(game) {
            game.StartGame()
            go gameLoop(game)
        }
    }
}

// NOTE - not used in lobby phase
func processSetReadyStateTrue(player *game.Player, g *game.Game) {
    if g != nil && player != nil {
        g.Access.Lock()
        player.Ready = true
        g.Access.Unlock()
    }
}

func processEndTurn(player *game.Player, g *game.Game) {
    // TODO - disallow any more moves to be accepted by server
    g.Access.Lock()
    player.Ready = true
    if areAllPlayersReady(g) {
        g.Access.Unlock()
        // If all players are ready, move onto next turn
    //    g.InterruptChan <-false
    } else {
        g.Access.Unlock()
    }
}

func processPlayerMsg(msg string, player *game.Player, g *game.Game) {
    // Retrieve the data in string form from the message
    data := dissectMultiPartMsg(msg)

    var playerMsg string
    if playerMsgStr, ok := data["msg"]; ok {
        playerMsg = playerMsgStr
    }

    if playerMsg == "" {
        return
    }

    // Cleanse of any nasty html code
    playerMsg = strings.Replace(playerMsg, "&", "&amp", -1)
    playerMsg = strings.Replace(playerMsg, "<", "&lt", -1)

    g.Access.Lock()
    defer g.Access.Unlock()
    sendGameMessage(g, "message;id:"+strconv.FormatInt(player.ID, 10)+";msg:"+playerMsg)
}

func processSetMoveTurn(msg string, player *game.Player, phase game.GamePhase, g *game.Game) {
    // Retrieve the data in string form from the message
    data := dissectMultiPartMsg(msg)

    // Process the message and create a MoveTurn object
    var crouch bool
    var x, y int64

    if crouchStr, ok := data["crouch"]; ok {
        crouch_, err := strconv.ParseBool(crouchStr)
        if err != nil { return }
        crouch = crouch_
    }

    if xStr, ok := data["x"]; ok {
        x_, err := strconv.ParseInt(xStr, 10, 64)
        if err != nil { return }
        x = x_
    }

    if yStr, ok := data["y"]; ok {
        y_, err := strconv.ParseInt(yStr, 10, 64)
        if err != nil { return }
        y = y_
    }

    m := new(turn.MoveTurn)

    // Set the fields of the MoveTurn object
    if crouch {
        m.Crouch = true
    } else {
        m.NewX = x
        m.NewY = y
        m.Crouch = false
    }

    // Set the players NextMoveTurn if the turn is valid
    // If the turn is valid, send a turn valid message to player
    // If the turn is invalid, send a turn invalid message to player
    g.Access.Lock()
    defer g.Access.Unlock()
    if player.SetMoveIfValid(m, phase, g) {
        sendPlayerMessage(player, g, "move:valid")
    } else {
        sendPlayerMessage(player, g, "move:invalid")
    }
}

func processItemTaken(msg string, p *game.Player, g *game.Game) {
    // Retrieve the data in string form from the message
    data := dissectMultiPartMsg(msg)

    var entityID, backpackX, backpackY int

    if entityIDStr, ok := data["entityID"]; ok {
        entityID_, err := strconv.ParseInt(entityIDStr, 10, 32)
        if err != nil { return }
        entityID = int(entityID_)
    }

    if backpackXStr, ok := data["backpackX"]; ok {
        backpackX_, err := strconv.ParseInt(backpackXStr, 10, 32)
        if err != nil { return }
        backpackX = int(backpackX_)
    }

    if backpackYStr, ok := data["backpackY"]; ok {
        backpackY_, err := strconv.ParseInt(backpackYStr, 10, 32)
        if err != nil { return }
        backpackY = int(backpackY_)
    }

    g.Access.Lock()
    defer g.Access.Unlock()
    // Find the entity on the ground
    itemsOnGround := g.ItemsOnGround[p.GetX() + p.GetY()*g.MapWidth]
    for i := len(itemsOnGround)-1; i >= 0; i--  {
        entity := itemsOnGround[i]
        if entity.EntityID == entityID {
            // Move the entity to the player's backpack
            valid := p.AddToBackpack(g, &entity, backpackX, backpackY)
            // If valid, remove the entity from the ground
            if valid {
                // Delete element from slice
                g.ItemsOnGround[p.GetX() + p.GetY()*g.MapWidth] =
                    append(itemsOnGround[:i], itemsOnGround[i+1:]...)
            }
            // Notify player of validity/invalidity
            sendItemTakenResponseMsg(p, g, entity, backpackX, backpackY, valid)
            return
        }
    }

    // If not on the ground, look for the item in the player's backpack
    entity := p.GetItemInBackpack(entityID)
    if entity != nil {
        // Try to add the item to the backpack at the new location
        valid := p.MoveItemInBackpack(g, entity, backpackX, backpackY)
        sendItemTakenResponseMsg(p, g, *entity, backpackX, backpackY, valid)
        return
    }

    // If neither method is valid, send an invalid response
    sendPlayerMessage(p, g, "takeitem:invalid;")
}

func processItemDropped(msg string, p *game.Player, g *game.Game) {
    // Retrieve the data in string form from the message
    data := dissectMultiPartMsg(msg)

    var entityID int

    if entityIDStr, ok := data["entityID"]; ok {
        entityID_, err := strconv.ParseInt(entityIDStr, 10, 32)
        if err != nil { return }
        entityID = int(entityID_)
    }

    // Look for the item in the player's backpack
    g.Access.Lock()
    defer g.Access.Unlock()
    entity := p.GetItemInBackpack(entityID)
    if entity != nil {
        // Remove the item entity from the backpack and add to the ground
        p.RemoveFromBackpack(entity)
        g.ItemsOnGround[p.GetX()+p.GetY()*g.MapWidth] = append(g.ItemsOnGround[p.GetX()+p.GetY()*g.MapWidth], *entity)
        sendItemDroppedResponseMsg(p, g, *entity, true)
    } else {
        // If player does not have the item, send invalid response
        sendPlayerMessage(p, g, "dropitem:invalid;")
    }
}

func processEquipItem(msg string, player *game.Player, g *game.Game) {
    // Retrieve the data in string form from the message
    data := dissectMultiPartMsg(msg)

    // Expecting entity ID
    var entityID int

    if entityIDStr, ok := data["entityID"]; ok {
        entityID_, err := strconv.ParseInt(entityIDStr, 10, 32)
        if err != nil { return }
        entityID = int(entityID_)
    }

    g.Access.Lock()
    defer g.Access.Unlock()
    // If the item was equipped successfully, return success msg
    success, itemID := player.EquipItem(entityID, g)
    if success {
        sendPlayerMessage(player, g, "equipitem:valid;entityID:"+strconv.FormatInt(int64(entityID), 10)+
                                    ";itemID:"+strconv.FormatInt(int64(itemID), 10))
    } else {
        sendPlayerMessage(player, g, "equipitem:invalid")
    }
}

func processUseBandage(msg string, player *game.Player, g *game.Game) {
    // Retrieve the data in string form from the message
    data := dissectMultiPartMsg(msg)

    // Expecting entity ID & limb
    var entityID int
    var limb string

    if entityIDStr, ok := data["entityID"]; ok {
        entityID_, err := strconv.ParseInt(entityIDStr, 10, 32)
        if err != nil { return }
        entityID = int(entityID_)
    }

    if limbStr, ok := data["limb"]; ok {
        if !(limbStr == "head" || limbStr == "chest" || limbStr == "rarm" ||
            limbStr == "larm" || limbStr == "rleg" || limbStr == "lleg") {
            return
        }
        limb = limbStr
    }

    g.Access.Lock()
    defer g.Access.Unlock()
    // If the item was used successfully, return success msg
    success, itemID := player.UseBandage(entityID, limb, g)
    if success {
        sendPlayerMessage(player, g, "usebandage:valid;entityID:"+strconv.FormatInt(int64(entityID), 10)+
                                    ";itemID:"+strconv.FormatInt(int64(itemID), 10))
    } else {
        sendPlayerMessage(player, g, "usebandage:invalid")
    }
}

func processAttackPlayer(msg string, player *game.Player, g *game.Game) {
    // Retrieve the data in string form from the message
    data := dissectMultiPartMsg(msg)

    // Expecting player ID and limb
    var playerID int64
    var limb string

    if playerIDStr, ok := data["playerID"]; ok {
        playerID_, err := strconv.ParseInt(playerIDStr, 10, 32)
        if err != nil { return }
        playerID = playerID_
    }

    if limbStr, ok := data["limb"]; ok {
        limb = limbStr
    }

    g.Access.Lock()
    defer g.Access.Unlock()
    // Get a reference to ther player to target
    target := g.GetPlayerByID(playerID)
    if target != nil {
        // Add the attack action to the player's action list if valid
        success := player.AttackPlayer(target, limb, g)
        if success {
            // Return valid attack msg to player
            sendPlayerMessage(player, g, "attack:valid")
        } else {
            // Return invalid attack msg to player
            sendPlayerMessage(player, g, "attack:invalid")
        }
    }
}

func processSearchTile(msg string, player *game.Player, g *game.Game) {
    // Retrieve the data in string form from the message
    data := dissectMultiPartMsg(msg)

    // Expecting tileX and tileY
    var tileX int64
    var tileY int64

    if tileXStr, ok := data["x"]; ok {
        tileX_, err := strconv.ParseInt(tileXStr, 10, 32)
        if err != nil { return }
        tileX = tileX_
    }

    if tileYStr, ok := data["y"]; ok {
        tileY_, err := strconv.ParseInt(tileYStr, 10, 32)
        if err != nil { return }
        tileY = tileY_
    }

    g.Access.Lock()
    defer g.Access.Unlock()
    // Attempt to queue up a search command
    if player.SearchTile(tileX, tileY, g) {
        // Send success response
        sendPlayerMessage(player, g, "search:valid;")
    } else {
        // Send failure response
        sendPlayerMessage(player, g, "search:invalid;")
    }
}

// Process a new connection request.
// Sets the player's net.Conn if connection is valid.
// Returns a pointer to the player & game if the connection is validated as the player.
func processNewConnectionMessage(msg string, conn net.Conn) (*game.Player, *game.Game) {
    // Retrieve the data in string form from the message
    data := dissectMultiPartMsg(msg)

    // Expecting 3 pieces of data, name, token and lobby
    var name, token, lobby string

    if nameStr, ok := data["name"]; ok {
        name = nameStr
    }

    if tokenStr, ok := data["token"]; ok {
        token = tokenStr
    }

    if lobbyStr, ok := data["lobby"]; ok {
        lobby = lobbyStr
    }

    // Ensure both name and token have been provided
    if name == "" || token == "" || lobby == "" {
        return nil, nil
    }

    // Check game exists
    game := state.GetGame(lobby)
    if game == nil {
        return nil, nil
    }

    game.Access.Lock()
    defer game.Access.Unlock()

    // If game has already started, player can't join
    if game.HasStarted() {
        return nil, nil
    }

    // Check player exists
    player := game.GetPlayer(name)
    if player == nil {
        return nil, nil
    }

    // Validate player token
    if player.IsTokenValid(token) {
        player.Conn = conn
        // Send all players in the game an update players message
        sendGameMessage(game, "update:players")
        // Send the player their player id
        sendPlayerMessage(player, game, "yourid;id:"+strconv.FormatInt(player.ID, 10))
        // Send the player that joined the phase times for this game
        sendPlayerMessage(player, game, "phasetime;droptime:"+strconv.FormatInt(DropPhaseTime, 10)+";movetime:"+strconv.FormatInt(MovePhaseTime, 10)+";actiontime:"+strconv.FormatInt(ActionPhaseTime, 10))
        // Send the player that joined the name of the map being used
        sendPlayerMessage(player, game, "map;name:isla_tyco")
        // Send the player that joined the list of items
        itemsMsg := "items"
        for _, item := range game.Items {
            itemsMsg += item.GetItemMsg()
        }
        sendPlayerMessage(player, game, itemsMsg)
        return player, game
    }

    return nil, nil
}

func dissectMultiPartMsg(msg string) map[string]string {
    m := make(map[string]string)

    // If msg only contains opcode, return empty map
    if len(msg) < 1 {
        return m
    }

    // Get the part of the msg not containing the OpCode
    data := msg[1:]

    // Split data into parts
    s := strings.Split(data, ";")

    // Loop through every piece of info and add to map
    for _, elem := range s {
        // Split the element into a key and a value
        s2 := strings.Split(elem, ":")

        if len(s2) < 2 {
            continue
        }

        key := s2[0]
        value := s2[1]
        m[key] = value
    }

    return m
}
