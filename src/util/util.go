package util

// sourced from https://mrekucci.blogspot.com/2015/07/dont-abuse-mathmax-mathmin.html
func Min(x, y int64) int64 {
    if x < y {
        return x
    }
    return y
}

// sourced from https://mrekucci.blogspot.com/2015/07/dont-abuse-mathmax-mathmin.html
func Max(x, y int64) int64 {
    if x > y {
        return x
    }
    return y
}

func Abs(x int64) int64 {
    if x < 0 {
        return -x
    }
    return x
}
