package state

import (
    "fmt"
    "game"
    "math/rand"
)

// games is a map from name to a pointer to a game
var games map[string]*game.Game = make(map[string]*game.Game)

// Add a new game to the list of active games.
// Param: name(string) - The name of the game as it will appear in searches
// Param: game(*game.Game) - Pointer to the game object to add
// Returns true iff the game is added, returns false if the name is already in use
func AddGame(name string, game *game.Game) bool {
    _, exists := games[name]

    // Add the new game iff the name isn't already taken
    if !exists {
        games[name] = game
    }

    return exists
}

// Get the game with the name given
// Param: name(string) - The unique name of the game lobby
// Returns pointer to game or nil if game does not exist
func GetGame(name string) *game.Game {
    _, exists := games[name]

    // Return the game iff it exists
    if exists {
        return games[name]
    }

    return nil
}

// Get a list of all games.
// Returns pointer to the lobby map.
func GetGames() *map[string]*game.Game {
    return &games
}

// Remove the given game from the list of games
// Param: name(string) - The name of the game as it will appear in searches
func RemoveGame(name string) {
    _, exists := games[name]

    // Add the new game iff the name isn't already taken
    if exists {
        delete(games, name)
    }
}

// Generate a random token used to identify games.
// Returns the token as a string
// IMPORTANT NOTE - Do not use this for anything security related
// With help from https://stackoverflow.com/questions/25431658/how-to-generate-a-random-token-with-md5
func GenRandomToken() string {
    b := make([]byte, 8)
    rand.Read(b)
    return fmt.Sprintf("%x", b)
}
