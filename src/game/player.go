package game

import (
    "net"
    "util"
    "log"
    "math"
    "game/turn"
    "game/stats"
    "game/item"
)

type Player struct {
    Name string
    token string
    ID int64
    Conn net.Conn
    Ready bool
    x int64
    y int64
    crouching bool
    ap int64
    dead bool
    modifiers stats.Modifiers
    health stats.Health
    backpack [4][3]*item.ItemEntity  // items currently in backpack
    equipped *item.ItemEntity        // weapon the player is holding (must be in backpack)
    nextMoveTurn *turn.MoveTurn
    nextActionTurn *turn.ActionTurn
}

func NewPlayer(name string, token string) *Player {
    p := new(Player)
    p.Name = name
    p.token = token
    p.crouching = false
    p.dead = false
    p.modifiers = stats.Modifiers{ViewRange:0, HitChance:0}
    p.health = stats.Health{TotalHP:100, HeadHP:100, ChestHP:100, RArmHP:100, LArmHP:100, RLegHP:100, LLegHP:100}
    return p
}

// Test whether the given move is valid given the players current properties.
// If the move is valid, set the players NextMoveTurn field.
// Returns true iff the MoveTurn given is valid for this player.
// TODO - check whether location is impassable
func (p *Player) SetMoveIfValid(m *turn.MoveTurn, gp GamePhase, g *Game) bool {
    // Check if the move is valid
    valid := false
    impassable := false

    // If turn is nil, set crouch as true
    if m == nil {
        m = new(turn.MoveTurn)
        m.Crouch = true
    }

    // Register not moving as crouching
    if m.Crouch || (p.x == m.NewX && p.y == m.NewY) {
        // If the player wishes to crouch, the turn is valid
        valid = true
        m.Crouch = true
    } else {
        // Calculate the difference in x and y of old and new position
        dx := p.x - m.NewX
        dy := p.y - m.NewY

        // Else, the player can move to an adjacent tile
        if ((dy == -1 || dy == 1) && (p.y % 2 == 1) && dx >= -1 && dx <= 0) ||
           ((dy == -1 || dy == 1) && (p.y % 2 == 0) && dx >= 0 && dx <= 1) ||
           (dy == 0 && dx >= -1 && dx <= 1) {
            valid = true
        }
    }

    // If the destination tile is impassable, the move is always invalid
    if IsTileImpassable(g.Map[int(m.NewX + m.NewY*g.MapWidth)]) {
        impassable = true
    }

    // If the turn is the drop phase ignore distance between player and tile
    if gp == DROP_PHASE {
        valid = true

        // Pretend tile is impassable if it is undroppable and in the drop phase
        if IsTileUndroppable(g.Map[int(m.NewX + m.NewY*g.MapWidth)]) {
            impassable = true
        }
    }

    // No matter the phase, impassable tile moves are never valid
    if impassable == true {
        valid = false
    }

    // If the turn is valid, set the players NextMoveTurn field
    if valid {
        p.nextMoveTurn = m
    }

    return valid
}

// Test whether the search is valid given the player's current position.
// If the search is valid, add a search commands to player's next action turn.
// Returns true iff the search is valid.
func (p *Player) SearchTile(tileX int64, tileY int64, g *Game) bool {
    // Calculate the difference in x and y of player and tile position
    dx := p.x - tileX
    dy := p.y - tileY

    // Else, the player can move to an adjacent tile
    if (((dy == -1 || dy == 1) && (p.y % 2 == 1) && dx >= -1 && dx <= 0) ||
           ((dy == -1 || dy == 1) && (p.y % 2 == 0) && dx >= 0 && dx <= 1) ||
           (dy == 0 && dx >= -1 && dx <= 1)) &&
           (!IsTileImpassable(g.Map[int(tileX + tileY*g.MapWidth)])) {
        if p.nextActionTurn == nil {
            p.nextActionTurn = new(turn.ActionTurn)
        }
        newAction := turn.NewCommandSearch(tileX, tileY)

        // Ensure the player has enough AP to make the action
        if p.ap - newAction.GetAPCost() >= 0 {
            p.ap -= newAction.GetAPCost()
            p.nextActionTurn.Commands = append(p.nextActionTurn.Commands, newAction)
            return true
        }
        log.Println("Search Failed: Not enough AP")
    }
    log.Println("Search Failed: Not within range")
    return false
}

// Return true iff player p2 is within view range of player p
func (p *Player) CanSeePlayer2(p2 *Player) bool {
    viewRange := util.Max(2 + p.modifiers.ViewRange, 0)
    diameter := viewRange * 2 + 1
    xOffset := p.y % 2

    // TODO - redo this by working whether p2.x and p2.y meet requirements

    for j := p.y-viewRange; j <= p.y+viewRange; j++ {
        // calculate the length of this row based on the vertical distance from the center tile
        vDistToCenter := util.Abs(j - p.y)
        rowLength := diameter - vDistToCenter
        rowXOffset := (vDistToCenter % 2) * xOffset
        startX := p.x - rowLength/2    // floor automatically since integer types used

        // test whether player 2 is on this row
        for i := startX; i < startX+rowLength; i++ {
            if p2.x == i+rowXOffset && p2.y == j {
                return true
            }
        }
    }

    return false
}

// Return true iff the player is within the bounds of the hexagon
func (p *Player) IsPlayerInHexagon(hexRadius int64, hexX int64, hexY int64) bool {
    diameter := hexRadius * 2 + 1
    xOffset := hexY % 2

    for j := int64(hexY-hexRadius); j <= hexY+hexRadius; j++ {
        // calculate the length of this row based on the vertical distance from the center tile
        vDistToCenter := util.Abs(j - hexY)
        rowLength := diameter - vDistToCenter
        rowXOffset := (vDistToCenter % 2) * xOffset
        startX := hexX - int64(math.Floor(float64(rowLength)/2))

        // check whether hexagon x & y are within search range
        for i := startX; i < startX+rowLength; i++ {
            if p.x == i+rowXOffset && p.y == j {
                return true
            }
        }
    }

    return false
}

// Adds item to the players backpack at the given location
// Returns true iff the item is added successfully
// Returns false if the item could not be added at the location given
func (p *Player) AddToBackpack(g *Game, itemEntity *item.ItemEntity, firstSlotX int, firstSlotY int) bool {
    slotY := firstSlotY;
    item := g.Items[itemEntity.ItemID]
    rowLength := len(p.backpack[0])
    colHeight := len(p.backpack)

    // check if the item fits on this row
    if firstSlotX + item.GetGridWidth() > rowLength || slotY >= colHeight {
        return false
    }

    // check if the slots are taken
    for x := firstSlotX; x < firstSlotX+item.GetGridWidth(); x++ {
        if p.backpack[slotY][x] != nil {
            return false
        }
    }

    // else, the slots must be free
    for x := firstSlotX; x < firstSlotX+item.GetGridWidth(); x++ {
        p.backpack[slotY][x] = itemEntity
    }

    return true
}

// Move from one backpack location to another
// X & Y gives the new location of the item
func (p *Player) MoveItemInBackpack(g *Game, itemEntity *item.ItemEntity, firstSlotX int, firstSlotY int) bool {
    slotY := firstSlotY;
    item := g.Items[itemEntity.ItemID]
    rowLength := len(p.backpack[0])
    colHeight := len(p.backpack)

    // check if the item fits on new row row
    if firstSlotX + item.GetGridWidth() > rowLength || slotY >= colHeight {
        return false
    }

    // check if the slots are taken
    for x := firstSlotX; x < firstSlotX+item.GetGridWidth(); x++ {
        if p.backpack[slotY][x] != nil {
            return false
        }
    }

    // else, clear the entity from all backpack slots
    p.RemoveFromBackpack(itemEntity)

    // then, add the item at the new location
    for x := firstSlotX; x < firstSlotX+item.GetGridWidth(); x++ {
        p.backpack[slotY][x] = itemEntity
    }

    return true
}

// Remove the item entity from the player's backpack at given location
func (p *Player) RemoveFromBackpack(itemEntity *item.ItemEntity) {
    // Clear the entity from all backpack slots
    for yIndex, row := range p.backpack {
        for xIndex, e := range row {
            if e != nil && e.EntityID == itemEntity.EntityID {
                p.backpack[yIndex][xIndex] = nil
            }
        }
    }
}

// Returns a pointer to the ItemEntity with given entityID if it is in player's backpack
func (p *Player) GetItemInBackpack(entityID int) *item.ItemEntity {
    for _, row := range p.backpack {
        for _, e := range row {
            if e != nil && e.EntityID == entityID {
                return e
            }
        }
    }
    return nil
}

// Equip an item from the player's backpack
// Returns true along with item ID iff the item has been equipped
func (p *Player) EquipItem(entityID int, g *Game) (bool, int) {
    entity := p.GetItemInBackpack(entityID)
    log.Println("Equipping item", entityID)
    if entity != nil {
        log.Println("Entity confirmed in backpack", entityID)
        itm := g.Items[entity.ItemID]
        // Ensure the item is equipable
        _, ok := itm.(item.Gun)
        log.Println(itm.GetItemMsg())
        if ok {
            log.Println("Item is equipable", entityID)

            if p.nextActionTurn == nil {
                p.nextActionTurn = new(turn.ActionTurn)
            }
            newAction := turn.NewCommandEquipWeapon(entity)

            // Ensure the player has enough AP to make the action
            if p.ap - newAction.GetAPCost() >= 0 {
                p.ap -= newAction.GetAPCost()
                p.nextActionTurn.Commands = append(p.nextActionTurn.Commands, newAction)
                return true, entity.ItemID
            }
        }
    }
    return false, -1
}

// Use a bandage in the player's inventory to heal a limb
// Returns true along with the item ID iff the bandage is can be used
func (p *Player) UseBandage(entityID int, limb string, g *Game) (bool, int) {
    entity := p.GetItemInBackpack(entityID)
    if entity != nil {
        itm := g.Items[entity.ItemID]
        // Ensure the item is a bandage
        if _, ok := itm.(item.Bandage); ok {
            if p.nextActionTurn == nil {
                p.nextActionTurn = new(turn.ActionTurn)
            }
            newAction := turn.NewCommandBandage(limb, entity)

            // Ensure the player has enough AP to make the action
            if p.ap - newAction.GetAPCost() >= 0 {
                p.ap -= newAction.GetAPCost()
                p.nextActionTurn.Commands = append(p.nextActionTurn.Commands, newAction)
                return true, entity.ItemID
            }
        }
    }
    return false, -1
}

// Add action to attack another player if valid.
// Returns true iff the attack request is valid.
func (p *Player) AttackPlayer(p2 *Player, limb string, g *Game) bool {
    // Look backwards through action list to see if a new weapon should be equipped
    equipped := p.equipped
    if p.nextActionTurn != nil {
        for index := len(p.nextActionTurn.Commands)-1; index >= 0; index-- {
            action := p.nextActionTurn.Commands[index]
            e, ok := action.(turn.CommandEquipWeapon)
            // If the action is to equip a weapon, that is the weapon to use for this attack action
            if ok {
                equipped = e.GetWeapon()
            }
        }
    }
    if equipped == nil {
        log.Println("Attack Failed: No weapon equipped")
        return false
    }

    // Get a reference to the gun object from the item entity
    weapon, ok := g.Items[equipped.ItemID].(item.Gun)
    if !ok {
        log.Println("Attack Failed: Equipped item is not a weapon")
        return false
    }

    maxRange := weapon.MaxRange
    diameter := maxRange * 2 + 1
    xOffset := p.y % 2

    // TODO - redo this by working whether p2.x and p2.y meet requirements

    for j := p.y-maxRange; j <= p.y+maxRange; j++ {
        // calculate the length of this row based on the vertical distance from the center tile
        var vDistToCenter int64 = util.Abs(j - p.y)
        var rowLength int64 = diameter - vDistToCenter
        var rowXOffset int64 = (vDistToCenter % 2) * xOffset
        var startX int64 = p.x - rowLength/2    // floor automatically since integer types used

        // test whether player 2 is on this row
        for i := startX; i < startX+rowLength; i++ {
            if p2.x == i+rowXOffset && p2.y == j {
                // Queue up attack action and subtract from ap
                action := turn.NewCommandAttack(p2, limb)
                if p.ap - action.GetAPCost() >= 0 {
                    p.ap -= action.GetAPCost()
                    if p.nextActionTurn == nil {
                        p.nextActionTurn = new(turn.ActionTurn)
                    }
                    p.nextActionTurn.Commands = append(p.nextActionTurn.Commands, action)
                    return true
                }
                log.Println("Attack Failed: Not enough AP")
                return false
            }
        }
    }
    log.Println("Attack Failed: Not within range")
    return false
}

func (p *Player) GetX() int64 {
    return p.x
}

func (p *Player) GetY() int64 {
    return p.y
}

func (p *Player) IsCrouching() bool {
    return p.crouching
}

func (p *Player) SetX(x int64) {
    p.x = x
}

func (p *Player) SetY(y int64) {
    p.y = y
}

func (p *Player) SetCrouching(c bool) {
    p.crouching = c
}

func (p *Player) GetEquipped() *item.ItemEntity {
    return p.equipped
}

func (p *Player) SetEquipped(entity *item.ItemEntity) {
    p.equipped = entity
}

func (p *Player) GetHealth() *stats.Health {
    return &p.health
}

func (p *Player) GetModifiers() *stats.Modifiers {
    return &p.modifiers;
}

func (p *Player) GetNextMoveTurn() *turn.MoveTurn {
    return p.nextMoveTurn
}

func (p *Player) GetNextActionTurn() *turn.ActionTurn {
    return p.nextActionTurn
}

func (p *Player) SetNextMoveTurn(t *turn.MoveTurn) {
    p.nextMoveTurn = t
}

func (p *Player) SetNextActionTurn(t *turn.ActionTurn) {
    p.nextActionTurn = t
}

func (p *Player) IsDead() bool {
    return p.dead
}

func (p *Player) SetDead(dead bool) {
    p.dead = dead
}

// Called at the start of each turn to reset each player's ap to 100%
func (p *Player) ResetAP() {
    p.ap = 100 + p.modifiers.TotalAP
}

func (p *Player) SubAP(val int64) {
    p.ap -= val
}

func (p *Player) IsTokenValid(token string) bool {
    return p.token == token
}
