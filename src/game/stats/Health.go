package stats

type Health struct {
    TotalHP int64
    HeadHP int64
    ChestHP int64
    RArmHP int64
    LArmHP int64
    RLegHP int64
    LLegHP int64
}
