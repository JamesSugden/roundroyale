package stats

type Modifiers struct {
    ViewRange int64             // increase in no. of tile that can be seen
    HitChance int64             // increase in hit chance (as a percentage)
    TotalAP int64               // increase in total ap per turn
    APMoveCost int64            // increase in ap cost of movement
    DmgPerMove int64            // amount of damage taken on move
    SickDmgPerTurn int64        // amount of damage taken every turn from radiation sickness
    ViewRangeDesc []string      // description of each modifier to view range
    HitChanceDesc []string      // description of each modifier to hist chance
    TotalAPDesc []string        // description of each modifier to total ap
    APMoveCostDesc []string     // description of each modifier to ap move cost
    DmgPerMoveDesc []string     // description of each modifier to dmg per move
    SickDmgPerTurnDesc []string // description of each modifier to to sickness dmg per turn
}
