package turn

import (
    "game/item"
    "game/stats"
)

type PlayerStub interface {
    GetX() int64
    GetY() int64
    GetEquipped() *item.ItemEntity
    GetHealth() *stats.Health
    GetModifiers() *stats.Modifiers
    SetEquipped(entity *item.ItemEntity)
    RemoveFromBackpack(itemEntity *item.ItemEntity)
}
