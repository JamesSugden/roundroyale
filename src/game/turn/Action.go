package turn

type Action interface {
    Apply()
    GetSubject() PlayerStub
    GetObject() PlayerStub
}
