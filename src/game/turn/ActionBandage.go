package turn

import (
    "game/item"
    "log"
)

type ActionBandage struct {
    Subject PlayerStub
    Object PlayerStub
    Limb string
    LimbHealAmount int64
    TotalHealAmount int64
    Entity *item.ItemEntity
}

func (a *ActionBandage) Apply() {
    // Heal the total HP
    a.Object.GetHealth().TotalHP += a.TotalHealAmount
    if a.Object.GetHealth().TotalHP > 100 {
        a.Object.GetHealth().TotalHP = 100
    }

    log.Println("Applying Bandage Heal", a.Limb, a.LimbHealAmount)

    // Apply damage to the limb health of the target
    switch a.Limb {
    case "head":
        a.Object.GetHealth().HeadHP += a.LimbHealAmount
        if a.Object.GetHealth().HeadHP > 100 {
            a.Object.GetHealth().HeadHP = 100
        }
    case "chest":
        a.Object.GetHealth().ChestHP += a.LimbHealAmount
        if a.Object.GetHealth().ChestHP > 100 {
            a.Object.GetHealth().ChestHP = 100
        }
    case "rarm":
        a.Object.GetHealth().RArmHP += a.LimbHealAmount
        if a.Object.GetHealth().RArmHP > 100 {
            a.Object.GetHealth().RArmHP = 100
        }
    case "larm":
        a.Object.GetHealth().LArmHP += a.LimbHealAmount
        if a.Object.GetHealth().LArmHP > 100 {
            a.Object.GetHealth().LArmHP = 100
        }
    case "rleg":
        a.Object.GetHealth().RLegHP += a.LimbHealAmount
        if a.Object.GetHealth().RLegHP > 100 {
            a.Object.GetHealth().RLegHP = 100
        }
    case "lleg":
        a.Object.GetHealth().LLegHP += a.LimbHealAmount
        if a.Object.GetHealth().LLegHP > 100 {
            a.Object.GetHealth().LLegHP = 100
        }
    }

    // Remove the bandage from the player's inventory
    a.Subject.RemoveFromBackpack(a.Entity)
}

func (a *ActionBandage) GetSubject() PlayerStub {
    return a.Subject
}

func (a *ActionBandage) GetObject() PlayerStub {
    return a.Object
}
