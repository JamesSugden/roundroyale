package turn

import (
    "game/item"
)

type CommandEquipWeapon struct {
    weapon *item.ItemEntity
}

func NewCommandEquipWeapon(weapon *item.ItemEntity) *CommandEquipWeapon {
    a := new(CommandEquipWeapon)
    a.weapon = weapon
    return a
}

func (a CommandEquipWeapon) GetWeapon() *item.ItemEntity {
    return a.weapon
}

func (a CommandEquipWeapon) GenAction(p PlayerStub, items []item.Item) Action {
    p.SetEquipped(a.weapon)
    return nil
}

func (a CommandEquipWeapon) GetAPCost() int64 {
    return 5
}
