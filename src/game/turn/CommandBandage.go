package turn

import (
    "game/item"
)

type CommandBandage struct {
    // Bandages can be used on a limb
    limb string
    bandage *item.ItemEntity
}

func NewCommandBandage(limb string, bandage *item.ItemEntity) *CommandBandage {
    a := new(CommandBandage)
    a.limb = limb
    a.bandage = bandage
    return a
}

func (a CommandBandage) GetAPCost() int64 {
    return 30
}

func (a CommandBandage) GenAction(p PlayerStub, items []item.Item) Action {
    // Determine which limb to heal
    /*var limbPtr *int64
    if a.limb == "head" {
        limbPtr = &p.GetHealth().HeadHP
    } else if a.limb == "chest" {
        limbPtr = &p.GetHealth().ChestHP
    } else if a.limb == "rarm" {
        limbPtr = &p.GetHealth().RArmHP
    } else if a.limb == "larm" {
        limbPtr = &p.GetHealth().LArmHP
    } else if a.limb == "rleg" {
        limbPtr = &p.GetHealth().RLegHP
    } else if a.limb == "lleg" {
        limbPtr = &p.GetHealth().LLegHP
    }*/

    // Get a pointer to the player's total HP
    //var totalPtr *int64
    //totalPtr = &p.GetHealth().TotalHP

    // Heal the total HP of the player by a third of the bandage healing amount
    var healLimbVal int64
    if itm, ok := items[a.bandage.ItemID].(item.Bandage); ok {
        healLimbVal = int64(itm.HealAmount)
    } else {
        return nil
    }
    healTotalVal := healLimbVal / 3

    // Cap the limb health at 100
    /*if healLimbVal + *limbPtr > 100 {
        healLimbVal = 100 - *limbPtr
    }

    // Cap the total health at 100
    if healTotalVal + *totalPtr > 100 {
        healTotalVal = 100 - *totalPtr
    }*/

    // Return modifiers to limb hp and total hp
    return &ActionBandage{ Subject: p, Object: p, Limb: a.limb, TotalHealAmount: healTotalVal, LimbHealAmount: healLimbVal, Entity: a.bandage }
    //return []IntModifier{ IntModifier{Ptr: limbPtr, Val: healLimbVal}, IntModifier{Ptr: totalPtr, Val: healTotalVal} }
}
