package turn

type ActionAttack struct {
    Subject PlayerStub
    Object PlayerStub
    Limb string
    TotalDmg int64
    LimbDmg int64
}

func (a *ActionAttack) Apply() {
    // Apply damage to the total health of the target
    a.Object.GetHealth().TotalHP -= a.TotalDmg
    if a.Object.GetHealth().TotalHP < 0 {
        a.Object.GetHealth().TotalHP = 0
    }

    // Apply damage to the limb health of the target
    switch a.Limb {
    case "head":
        a.Object.GetHealth().HeadHP -= a.LimbDmg
        if a.Object.GetHealth().HeadHP < 0 {
            a.Object.GetHealth().HeadHP = 0
        }
    case "chest":
        a.Object.GetHealth().ChestHP -= a.LimbDmg
        if a.Object.GetHealth().ChestHP < 0 {
            a.Object.GetHealth().ChestHP = 0
        }
    case "rarm":
        a.Object.GetHealth().RArmHP -= a.LimbDmg
        if a.Object.GetHealth().RArmHP < 0 {
            a.Object.GetHealth().RArmHP = 0
        }
    case "larm":
        a.Object.GetHealth().LArmHP -= a.LimbDmg
        if a.Object.GetHealth().LArmHP < 0 {
            a.Object.GetHealth().LArmHP = 0
        }
    case "rleg":
        a.Object.GetHealth().RLegHP -= a.LimbDmg
        if a.Object.GetHealth().RLegHP < 0 {
            a.Object.GetHealth().RLegHP = 0
        }
    case "lleg":
        a.Object.GetHealth().LLegHP -= a.LimbDmg
        if a.Object.GetHealth().LLegHP < 0 {
            a.Object.GetHealth().LLegHP = 0
        }
    }
}

func (a *ActionAttack) GetSubject() PlayerStub {
    return a.Subject
}

func (a *ActionAttack) GetObject() PlayerStub {
    return a.Object
}
