package turn

import (
    "game/item"
)

type CommandSearch struct {
    // Co-ordinates of tile to search
    tileX int64
    tileY int64
}

func NewCommandSearch(tileX int64, tileY int64) *CommandSearch {
    a := new(CommandSearch)
    a.tileX = tileX
    a.tileY = tileY
    return a
}

func (a CommandSearch) GenAction(p PlayerStub, items []item.Item) Action {
    return nil
}

func (a CommandSearch) GetAPCost() int64 {
    return 20
}
