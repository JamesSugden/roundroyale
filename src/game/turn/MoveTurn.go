package turn

type MoveTurn struct {
    // Either move somewhere or crouch
    Crouch bool
    // If move is true, move to this location
    NewX int64
    NewY int64
}
