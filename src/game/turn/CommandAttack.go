package turn

import (
    "util"
    "game/item"
    "math"
    "math/rand"
)

type CommandAttack struct {
    target PlayerStub
    limb string
}

func NewCommandAttack(target PlayerStub, limb string) *CommandAttack {
    a := new(CommandAttack)
    a.target = target
    a.limb = limb
    return a
}

func (a CommandAttack) GetAPCost() int64 {
    return 60
}

func (a CommandAttack) GenAction(p PlayerStub, items []item.Item) Action {
    // TODO - this range doesn't really work with the hexgrid system
    rangeX := util.Abs(p.GetX() - a.target.GetX())
    rangeY := util.Abs(p.GetY() - a.target.GetY())
    rangeD := math.Sqrt(float64(rangeX*rangeX + rangeY*rangeY)) + 1

    e := p.GetEquipped()
    var dmgPerHit int64
    var shotsPerAction int64
    var accuracy float64

    weaponEquipped := false
    if e != nil {
        gun, ok := items[e.ItemID].(item.Gun)
        if ok {
            // If a weapon is equipped, use weapon stats
            dmgPerHit = gun.DmgPerHit
            shotsPerAction = gun.ShotsPerAction
            accuracy = gun.Accuracy
            weaponEquipped = true
        }
    }

    if !weaponEquipped {
        // If no weapon equipped, simulate punching
        dmgPerHit = 5
        shotsPerAction = 1
        accuracy = 0.6
    }

    // Calculate the damage done to the target limb
    hitChance := 1 / rangeD * accuracy * (1 + float64(p.GetModifiers().HitChance)/100)
    var dmgDone int64 = 0
    var i int64
    for i = 0; i < shotsPerAction; i++ {
        x := rand.Float64()
        // Randomly determine for each shot whether it hits target
        if x > 0 && x <= hitChance {
            dmgDone += dmgPerHit
        }
    }

    /*log.Println("Dmg Per Hit", dmgPerHit)
    log.Println("Shots Per Action", shotsPerAction)
    log.Println("Accuracy", accuracy)
    log.Println("Hit Chance", hitChance)
    log.Println("Dmg Done", dmgDone)*/

    // Determine which limb to apply the damage to
    /*var limbPtr *int64
    if a.limb == "head" {
        limbPtr = &a.target.GetHealth().HeadHP
    } else if a.limb == "chest" {
        limbPtr = &a.target.GetHealth().ChestHP
    } else if a.limb == "rarm" {
        limbPtr = &a.target.GetHealth().RArmHP
    } else if a.limb == "larm" {
        limbPtr = &a.target.GetHealth().LArmHP
    } else if a.limb == "rleg" {
        limbPtr = &a.target.GetHealth().RLegHP
    } else if a.limb == "lleg" {
        limbPtr = &a.target.GetHealth().LLegHP
    }

    // Total hp modified by half of dmgDone value
    var totalPtr *int64
    totalPtr = &a.target.GetHealth().TotalHP*/
    totalDmgDone := int64(float64(dmgDone) * 0.5)

    // Return modifiers to limb hp and total hp
    return &ActionAttack{ Subject: p, Object: a.target, Limb: a.limb, TotalDmg: totalDmgDone, LimbDmg: dmgDone }
    //return []IntModifier{ IntModifier{Ptr: limbPtr, Val: -dmgDone}, IntModifier{Ptr: totalPtr, Val: -totalDmgDone} }
}
