package turn

type ActionTurn struct {
    // Action turn is composed of a list of action commands
    Commands []Command
}
