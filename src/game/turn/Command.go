package turn

import (
    "game/item"
)

type Command interface {
    GetAPCost() int64   // Max AP = 100
    // TODO - GenMemento
    GenAction(p PlayerStub, items []item.Item) Action
}
