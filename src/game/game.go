package game

import (
    "math/rand"
    "io/ioutil"
    "math"
    "log"
    "path/filepath"
    "strings"
    "strconv"
    "sync"
    "time"
    "util"
    "game/item"
    "game/turn"
)

const (
    LOW_DROP_CHANCE = 5
    MED_DROP_CHANCE = 30
    HIGH_DROP_CHANCE = 60
)

type Game struct {
    // Game fields can only be accessed by 1 goroutine at a time
    // Nothing should be accessed via player command without being granted lock
    Access sync.Mutex

    started bool
    name string
    password string
    players []*Player
    Phase GamePhase
    nextPlayerID int64

    // Map
    Map []TileType
    MapWidth int64
    MapHeight int64

    // Nuclear Storm
    turnCounter int64
    Storm NuclearStorm

    // Available items
    Items []item.Item
    // Items on the map
    ItemsOnGround [][]item.ItemEntity

    // Channels allowing communication through client web sockets
    InterruptChan chan bool
    ExecuteChan chan func()
}

// Create a new public game.
// Param: name(string) - The name of the lobby's creator
// Returns a pointer to game object.
func NewPublicGame(name string) *Game {
    g := new(Game)
    g.name = name
    g.init()
    return g
}

// Create a new private game.
// Param: name(string) - The name of the lobby's creator
// Param: password(string) - The password required to join the lobby
// Returns a pointer to game object.
func NewPrivateGame(name string, password string) *Game {
    g := new(Game)
    g.name = name
    g.password = password
    g.init()
    return g
}

func (g *Game) init() {
    rand.Seed(time.Now().Unix())
    g.started = false
    g.nextPlayerID = 0
    g.initItemList()
    g.loadMap("isla_tyco")
    g.genItemSpawns()
    g.Storm = NuclearStorm{ Radius: -1, DmgPerTurn: 0, SicknessDmgPerTurn: 0 }
    g.InterruptChan = make(chan bool)
    g.Access = sync.Mutex{}
    //g.AccessChan = make(chan struct{}, 1)
}

// Initialise the list of items
func (g *Game) initItemList() {
    g.Items = append(g.Items, item.Gun{ Name:"Pistol", ShotsPerAction:8, DmgPerHit:15, MaxRange: 2, Accuracy: 0.6, Image:"assets/Pistol (512x512).png", GridWidth:1, GridHeight:1 })
    g.Items = append(g.Items, item.Gun{ Name:"SMG", ShotsPerAction:15, DmgPerHit:12, MaxRange: 2, Accuracy: 0.5, Image:"assets/SMG (1024x512).png", GridWidth:2, GridHeight:1 })
    g.Items = append(g.Items, item.Gun{ Name:"AR", ShotsPerAction:10, DmgPerHit:20, MaxRange: 2, Accuracy: 0.75, Image:"assets/Assault Rifle (1024x512).png", GridWidth:2, GridHeight:1 })
    //g.Items = append(g.Items, item.Projectile{ Name:"Grenade", TimeToThrow: 5, DmgPerHit:75, Image:"assets/Grenade (512x512).png", GridWidth:1, GridHeight:1 })
    g.Items = append(g.Items, item.Bandage{ Name:"Cloth Scraps", HealAmount:35, Image:"assets/Blood-Splat-1.png", GridWidth:1, GridHeight:1 })
    g.Items = append(g.Items, item.Bandage{ Name:"Bandage", HealAmount:65, Image:"assets/Blood-Splat-2.png", GridWidth:1, GridHeight:1 })
}

// Load a map
func (g *Game) loadMap(mapName string) {
    path, _ := filepath.Abs("static/maps/" + mapName + ".txt")
    b, err := ioutil.ReadFile(path)
    if err != nil {
        log.Println(err)
    }

    // Convert content to a string
    str := string(b)

    // Clear the map slice if there is already a map loaded
    g.Map = nil

    // Create a replacer to remove and carriage returns (\r) from the text
    replacer := strings.NewReplacer("\r", "")
    str = replacer.Replace(str)
    // Parse the string and create the map array
    // Note - map is stored as a flat slice since 2D slices are yucky
    lines := strings.Split(str, "\n")
    g.MapHeight = int64(len(lines))
    for _, line := range lines {
        elems := strings.Split(line, " ")
        g.MapWidth = int64(len(elems))
        for _, elem := range elems {
            tileType, err := strconv.ParseInt(elem, 10, 64)
            if err == nil {
                g.Map = append(g.Map, TileType(tileType))
            } else {
                log.Println(err, elem)
                g.Map = append(g.Map, TileType(0))
            }
        }
    }

    //log.Printf("Map (%d, %d), %d", g.MapWidth, g.MapHeight, len(g.Map))
    //log.Println("\n", g.Map)
}

// Generate the item spawns for this game
func (g *Game) genItemSpawns() {
    nextItemEntityID := 0
    for _, tile := range g.Map {
        dropChance := GetTileDropChance(tile)
        x := rand.Intn(100)
        if x > 0 && x <= dropChance {
            i := rand.Intn(len(g.Items))    // Get a random item
            g.ItemsOnGround = append(g.ItemsOnGround, []item.ItemEntity{item.ItemEntity{EntityID: nextItemEntityID, ItemID: i}})
            nextItemEntityID ++
        } else {
            g.ItemsOnGround = append(g.ItemsOnGround, []item.ItemEntity{})
        }
    }
}

// Start the game
// Once started, no more players can join
func (g *Game) StartGame() {
    g.started = true
    g.Phase = DROP_PHASE
    g.turnCounter = 0
    // Allow every player to be able to make request at once
    g.ExecuteChan = make(chan func())//, len(g.players))
}

// Returns true iff the game has started and is in progress
func (g *Game) HasStarted() bool {
    return g.started
}

// Reset every players move turn
func (g *Game) ResetMoveTurns() {
    for _, player := range g.players {
        player.SetNextMoveTurn(nil)
    }
}

// Reset every players action turn
func (g *Game) ResetActionTurns() {
    for _, player := range g.players {
        player.SetNextActionTurn(nil)
    }
}

// Process the drop phase.
// Use players NextMoveTurn objects to choose drop location.
func (g *Game) DoDropPhase() {
    for _, player := range g.players {
        // Turns have already been tested for validity server side so don't need to retest
        if player.GetNextMoveTurn() != nil {
            // Drop where player selected if player selected a location
            player.SetX(player.GetNextMoveTurn().NewX)
            player.SetY(player.GetNextMoveTurn().NewY)
        } else {
            for {
                x := int64(rand.Intn(int(g.MapWidth)))
                y := int64(rand.Intn(int(g.MapHeight)))

                log.Printf("generated (%d, %d)", x, y)
                log.Printf("tile type %d", g.Map[int(x + y*g.MapWidth)])

                if !IsTileImpassable(g.Map[int(x + y*g.MapWidth)]) &&
                   !IsTileUndroppable(g.Map[int(x + y*g.MapWidth)]) {
                    player.SetX(x)
                    player.SetY(y)
                    break
                }
            }
        }

        // Update view range incase player landed on elevated tile
        g.updateViewRangeModifier(player)
    }
}

// Process the movement phase.
// Player's turns are stored in their player object.
func (g *Game) DoMovePhase() {
    for _, player := range g.players {
        // Reset each player's ap here since it is before any actions have been taken
        player.ResetAP()

        // Turns have already been tested for validity server side so don't need to retest
        if player.GetNextMoveTurn() != nil {
            player.SetCrouching(player.GetNextMoveTurn().Crouch)

            // Only update players position if player is not crouching
            if !player.IsCrouching() {
                player.SetX(player.GetNextMoveTurn().NewX)
                player.SetY(player.GetNextMoveTurn().NewY)
                g.updateViewRangeModifier(player)
                player.SubAP(40 + player.GetModifiers().APMoveCost)   // Subtract 40 ap for moving

                if player.GetModifiers().DmgPerMove > 0 {
                    player.GetHealth().TotalHP -= player.GetModifiers().DmgPerMove
                }
            }
        } else {
            // If the turn is nil, set crouching as true
            player.SetCrouching(true)
        }

        // Update the hit chance modifier incase the player is crouching
        g.updateHitChanceModifier(player)
    }
}

// Process the action phase.
// Player's actions are computed in parallel.
// Player's turns are stored in their player object.
// Returns array of players who died this turn followed by array of winners.
// Array of winners is empty/nil if the game isn't over.
func (g *Game) DoActionPhase() ([]*Player, []*Player) {
    //log.Println("Doing Action Phase")
    deadPlayers := []*Player{}
    var winners []*Player  // If all players die on last round, all get the win
    for {
        done := true
        //allMods := []turn.IntModifier{}
        actions := []turn.Action{}
        // Take i'th action from each player and calculate the changes without making them
        for _, player := range g.players {
            if !player.IsDead() && player.GetNextActionTurn() != nil {
                // Toke note of changes to make due to this action
                coms := player.GetNextActionTurn().Commands
                //log.Println("player", player.ID, len(coms))
                if len(coms) > 0 {
                    done = false
                    var com turn.Command
                    com, player.GetNextActionTurn().Commands = coms[0], coms[1:]
                    action := com.GenAction(player, g.Items)
                    if action != nil {
                        actions = append(actions, action)
                    }
                }
            }
        }
        // Execute all actions effectively simulatenously
        for _, action := range actions {
            action.Apply()
        }
        // Check if someone is dead & re-calculate modifiers
        // Determine winners if there are any
        var deadPlayersThisRound []*Player
        deadPlayersThisRound, winners = g.assessPlayers()
        deadPlayers = append(deadPlayers, deadPlayersThisRound...)
        // Once every action has been processed, exit loop
        if done || len(winners) > 0 {
        //    log.Println("Done All Actions")
            break
        }
    }
    return deadPlayers, winners
}

// Determine whether any players have died or won.
// Recalculate modifiers for all living players.
// Returns array of players who died this turn followed by array of winners.
// Array of winners is empty/nil if the game isn't over.
func (g *Game) assessPlayers() ([]*Player, []*Player) {
    deadPlayers := []*Player{}
    var winners []*Player  // If all players die on last round, all get the win
    // Check if someone is dead & re-calculate modifiers
    for _, player := range g.players {
        hp := player.GetHealth()
        // Player dead if the player's total hp is 0 or all limbs are perma-crippled
        if (hp.TotalHP <= 0 || (hp.HeadHP <= 0 && hp.ChestHP <= 0 ||
                hp.RArmHP <= 0 && hp.LArmHP <= 0 &&
                hp.RLegHP <= 0 && hp.LLegHP <= 0)) && !player.IsDead() {
            player.SetDead(true)
            deadPlayers = append(deadPlayers, player)
        } else {
            // Reasses modifiers to players
            g.updateViewRangeModifier(player)
            g.updateHitChanceModifier(player)
            g.updateTotalAPModifier(player)
            g.updateAPMoveCostModifier(player)
            g.updateDmgPerMoveModifier(player)
        }
    }
    // Check how many players are still alive
    allLivingPlayers := []*Player{}
    for _, player := range g.players {
        if !player.IsDead() {
            allLivingPlayers = append(allLivingPlayers, player)
        }
    }
    // Determine if anyone won
    if len(allLivingPlayers) == 1 {
        // If only one player is still alive, they are the winner
        winners = allLivingPlayers
    } else if len(allLivingPlayers) == 0 {
        // If everyone is dead, the players who died this turn are the winners
        winners = deadPlayers
    }
    return deadPlayers, winners
}

// Update the storm radius and damage
// Returns following bools (sendStormApproachingUpdate, sendStormRadiusUpdate, sendStormDamageUpdate)
func (g *Game) UpdateStorm() (bool, bool, bool) {
    // Keep track of which turn the game is on
    g.turnCounter ++

    sendStormApproachingUpdate := g.turnCounter == 4
    sendStormRadiusUpdate := false
    sendStormDamageUpdate := false

    // If on turn 5, start the storm
    if g.turnCounter == 5 {
        x := util.Max(g.MapWidth/2, g.MapHeight/2)
        g.Storm.Radius = int64(math.Sqrt(float64(x*x*2)))
        g.Storm.CenterX = int64(g.MapWidth/2)
        g.Storm.CenterY = int64(g.MapHeight/2)
        g.Storm.DmgPerTurn = 5
        g.Storm.SicknessDmgPerTurn = 2
        sendStormRadiusUpdate = true
        sendStormDamageUpdate = true
        log.Println("Nuclear Storm Approaching, Radius", g.Storm.Radius)
    }

    // If on turn greater than 5, update the storm
    if g.turnCounter > 5 {
        // Decrease the radius of the storm
        g.Storm.Radius -= g.turnCounter % 2
        sendStormRadiusUpdate = true
        // Every 5 turns, increase the storm's damage
        if g.turnCounter % 5 == 0 {
            g.Storm.DmgPerTurn += 10
            g.Storm.SicknessDmgPerTurn += 2
            sendStormDamageUpdate = true
        }
    }

    return sendStormApproachingUpdate, sendStormRadiusUpdate, sendStormDamageUpdate
}

// Update the player's HP and radiation sickness modifier
// Returns (HasTakenDmg, HasDied) booleans
func (g *Game) DoStormPhase() ([]*Player, []*Player) {
    if g.Storm.Radius >= 0 {
        for _, player := range g.players {
            if !player.IsPlayerInHexagon(g.Storm.Radius, g.Storm.CenterX, g.Storm.CenterY) {
                log.Println("Player In Hexagon")
                player.GetHealth().TotalHP -= g.Storm.DmgPerTurn
                player.GetModifiers().SickDmgPerTurn = g.Storm.SicknessDmgPerTurn
                player.GetModifiers().SickDmgPerTurnDesc = []string{"Radiation Sickness"}
            }
            player.GetHealth().TotalHP -= player.GetModifiers().SickDmgPerTurn
            player.GetHealth().HeadHP -= player.GetModifiers().SickDmgPerTurn
            player.GetHealth().ChestHP -= player.GetModifiers().SickDmgPerTurn
            player.GetHealth().RArmHP -= player.GetModifiers().SickDmgPerTurn
            player.GetHealth().LArmHP -= player.GetModifiers().SickDmgPerTurn
            player.GetHealth().RLegHP -= player.GetModifiers().SickDmgPerTurn
            player.GetHealth().LLegHP -= player.GetModifiers().SickDmgPerTurn
        }
    }
    // Still need to do this for dmg per move update
    return g.assessPlayers()
}

func (g *Game) updateViewRangeModifier(player *Player) {
    // Reset the current modifier
    player.GetModifiers().ViewRange = 0
    player.GetModifiers().ViewRangeDesc = []string{}
    // If the tile is high up then give the player a view range bonus
    if IsTileHigh(g.Map[player.GetX() + player.GetY()*g.MapWidth]) {
        player.GetModifiers().ViewRange += 1
        player.GetModifiers().ViewRangeDesc = append(player.GetModifiers().ViewRangeDesc, "+1 Up High")
    }
    // If the player's head is crippled, view range is decrease by 1
    if player.GetHealth().HeadHP <= 30 {
        player.GetModifiers().ViewRange -= 1
        player.GetModifiers().ViewRangeDesc = append(player.GetModifiers().ViewRangeDesc, "-1 Head Crippled")
    }
}

func (g *Game) updateHitChanceModifier(player *Player) {
    // Reset the current modifier
    player.GetModifiers().HitChance = 0
    player.GetModifiers().HitChanceDesc = []string{}
    // Add to modifier if the player is crouching
    if player.IsCrouching() {
        // Update the hit chance modifier if the player is crouching
        player.GetModifiers().HitChance += 20
        player.GetModifiers().HitChanceDesc = append(player.GetModifiers().HitChanceDesc, "+20% Crouching")
    }
    // For each of the player's arms which are crippled, decrease hit chance
    if player.GetHealth().RArmHP <= 30 {
        player.GetModifiers().HitChance -= 15
        player.GetModifiers().HitChanceDesc = append(player.GetModifiers().HitChanceDesc, "-15% Right Arm Crippled")
    }
    // For each of the player's arms which are crippled, decrease hit chance
    if player.GetHealth().LArmHP <= 30 {
        player.GetModifiers().HitChance -= 15
        player.GetModifiers().HitChanceDesc = append(player.GetModifiers().HitChanceDesc, "-15% Left Arm Crippled")
    }
}

func (g *Game) updateTotalAPModifier(player *Player) {
    // Reset the current modifier
    player.GetModifiers().TotalAP = 0
    player.GetModifiers().TotalAPDesc = []string{}
    // If the player's chest is crippled, decrease total ap
    if player.GetHealth().ChestHP <= 30 {
        player.GetModifiers().TotalAP -= 10
        player.GetModifiers().TotalAPDesc = append(player.GetModifiers().TotalAPDesc, "-10 Chest Crippled")
    }
}

func (g *Game) updateAPMoveCostModifier(player *Player) {
    // Reset the current modifier
    player.GetModifiers().APMoveCost = 0
    player.GetModifiers().APMoveCostDesc = []string{}
    // If one of the player's legs is crippled, increase movement cost
    if player.GetHealth().RLegHP <= 30 || player.GetHealth().LLegHP <= 30 {
        player.GetModifiers().APMoveCost += 10
        player.GetModifiers().APMoveCostDesc = append(player.GetModifiers().APMoveCostDesc, "+10 Leg Crippled")
    }
}

func (g *Game) updateDmgPerMoveModifier(player *Player) {
    // Reset the current modifier
    player.GetModifiers().DmgPerMove = 0
    player.GetModifiers().DmgPerMoveDesc = []string{}
    // If both the player's legs are cripppled, increase damage per move
    if player.GetHealth().RLegHP <= 30 && player.GetHealth().LLegHP <= 30 {
        player.GetModifiers().DmgPerMove += 5
        player.GetModifiers().DmgPerMoveDesc = append(player.GetModifiers().DmgPerMoveDesc, "+5 Both Legs Crippled")
    }
}

// Add a player to a game.
// Can only be done before the game is started (i.e. in lobby state).
func (g *Game) AddPlayer(p *Player) {
    if p != nil && !g.started {
        p.ID = g.nextPlayerID
        g.players = append(g.players, p)
        g.nextPlayerID ++
    }
}

// Remove a player from the game.
// Returns true iff the player was removed.
func (g *Game) RemovePlayer(p *Player) bool {
    for index, player := range g.players {
        if player.ID == p.ID {
            g.players = append(g.players[:index], g.players[index+1:]...)
            return true;
        }
    }
    return false
}

// Get a pointer to the player with the given name.
// Returns nil if player doesn't exist.
func (g *Game) GetPlayer(name string) *Player {
    for _, p := range g.players {
        if p.Name == name {
            return p
        }
    }

    return nil
}

func (g *Game) GetPlayerByID(id int64) *Player {
    for _, p := range g.players {
        if p.ID == id {
            return p
        }
    }

    return nil
}

// Get a list of players.
func (g *Game) GetPlayers() []*Player {
    return g.players
}

// Get the name of the game
func (g *Game) GetName() string {
    return g.name
}

// Test whether the password is correct.
// Param: password(string) - The password input by a player attempting to join
// Returns true iff the password is correct
func (g *Game) IsPasswordCorrect(pword string) bool {
    return g.password == pword
}

// Returns true if the lobby requires a password to join
func (g *Game) IsLobbyPrivate() bool {
    return g.password != ""
}
