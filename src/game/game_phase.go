package game

type GamePhase int

const (
    DROP_PHASE GamePhase = 0
    MOVE_PHASE GamePhase = 1
    ACTION_PHASE GamePhase = 2
)
