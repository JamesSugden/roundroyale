package game

type NuclearStorm struct {
    Radius int64
    CenterX int64
    CenterY int64
    DmgPerTurn int64
    SicknessDmgPerTurn int64
}
