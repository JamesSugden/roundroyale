package game

type TileType int

const (
    WATER TileType = 0
    DIRT TileType = 1
    GRASS TileType = 2
    FOREST TileType = 3
    HILL TileType = 4
    MOUNTAIN TileType = 5
    HOUSE TileType = 6
    HIGHRISE TileType = 7
    BRIDGE TileType = 8
)

// Returns true iff the tile type is impassable.
func IsTileImpassable(t TileType) bool {
    if t == WATER || t == MOUNTAIN {
        return true
    } else {
        return false
    }
}

// NOTE - Does not include impassable tiles.
// Returns true iff the tile cannot be landed on in the drop phase.
func IsTileUndroppable(t TileType) bool {
    if t == HOUSE || t == HIGHRISE {
        return true
    } else {
        return false
    }
}

// Returns true iff the tile is high up, therefore granting a view range bonus
func IsTileHigh(t TileType) bool {
    if t == HIGHRISE || t == HILL {
        return true
    } else {
        return false
    }
}

func GetTileDropChance(t TileType) int {
    if t == WATER || t == MOUNTAIN || t == BRIDGE {
        return 0
    } else if t == DIRT || t == GRASS || t == HILL {
        return 5
    } else if t == FOREST {
        return 25
    } else if t == HOUSE {
        return 80
    } else if t == HIGHRISE {
        return 100
    } else {
        return 0
    }
}
