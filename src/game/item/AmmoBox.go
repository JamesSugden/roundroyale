package item

import (
    "strconv"
)

type AmmoType int
type AmmoModifier int

const (
    // Ammo Types
    PISTOL AmmoType = 0
    SHOTGUN AmmoType = 1
    AR AmmoType = 2
    SMG AmmoType = 3

    // Ammo Modifiers
    REGULAR AmmoModifier = 0
    INCENDIARY AmmoModifier = 1
    ARMOR_PIERCING AmmoModifier = 2
)

type AmmoBox struct {
    // Required item fields
    Name string
    Image string
    GridWidth int
    GridHeight int

    // AmmoBox specific fields
    Type AmmoType
    Modifier AmmoModifier
    Amount int
}

func (p AmmoBox) GetName() string {
    return p.Name
}

func (p AmmoBox) GetImage() string {
    return p.Image
}

func (p AmmoBox) GetGridWidth() int {
    return p.GridWidth
}

func (p AmmoBox) GetGridHeight() int {
    return p.GridHeight
}

func (p AmmoBox) GetItemMsg() string {
    return ";name:"+p.Name+",type:"+strconv.FormatInt(int64(p.Type), 10)+
            ",mod:"+strconv.FormatInt(int64(p.Modifier), 10)+
            ",amount:"+strconv.FormatInt(int64(p.Amount), 10)+
            ",image:"+p.Image+",gridw:"+strconv.FormatInt(int64(p.GridWidth), 10)+
            ",gridh:"+strconv.FormatInt(int64(p.GridHeight), 10)
}
