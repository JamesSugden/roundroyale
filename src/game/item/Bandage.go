package item

import (
    "strconv"
)

type Bandage struct {
    Name string
    Image string
    GridWidth int
    GridHeight int
    HealAmount int
}

func (p Bandage) GetName() string {
    return p.Name
}

func (p Bandage) GetImage() string {
    return p.Image
}

func (p Bandage) GetGridWidth() int {
    return p.GridWidth
}

func (p Bandage) GetGridHeight() int {
    return p.GridHeight
}

func (p Bandage) GetItemMsg() string {
    return ";name:"+p.Name+",healAmount:"+strconv.FormatInt(int64(p.HealAmount), 10)+
            ",image:"+p.Image+",gridw:"+strconv.FormatInt(int64(p.GridWidth), 10)+
            ",gridh:"+strconv.FormatInt(int64(p.GridHeight), 10)
}
