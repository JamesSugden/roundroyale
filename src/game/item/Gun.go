package item

import (
    "strconv"
)

type Gun struct {
    Name string
    Image string
    GridWidth int
    GridHeight int
    ShotsPerAction int64
    DmgPerHit int64
    MaxRange int64
    Accuracy float64
}

func (g Gun) GetName() string {
    return g.Name
}

func (g Gun) GetImage() string {
    return g.Image
}

func (g Gun) GetGridWidth() int {
    return g.GridWidth
}

func (g Gun) GetGridHeight() int {
    return g.GridHeight
}

func (g Gun) GetItemMsg() string {
    return ";name:"+g.Name+
            ",shotsPerAction:"+strconv.FormatInt(g.ShotsPerAction, 10)+
            ",dmgPerHit:"+strconv.FormatInt(g.DmgPerHit, 10)+
            ",maxRange:"+strconv.FormatInt(g.MaxRange, 10)+
            ",accuracy:"+strconv.FormatFloat(g.Accuracy, 'f', -1, 32)+
            ",image:"+g.Image+",gridw:"+strconv.FormatInt(int64(g.GridWidth), 10)+
            ",gridh:"+strconv.FormatInt(int64(g.GridHeight), 10)
}
