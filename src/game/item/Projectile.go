package item

import (
    "strconv"
)

type Projectile struct {
    Name string
    Image string
    GridWidth int
    GridHeight int
    TimeToThrow float32
    DmgPerHit float32
}

func (p Projectile) GetName() string {
    return p.Name
}

func (p Projectile) GetImage() string {
    return p.Image
}

func (p Projectile) GetGridWidth() int {
    return p.GridWidth
}

func (p Projectile) GetGridHeight() int {
    return p.GridHeight
}

func (p Projectile) GetItemMsg() string {
    return ";name:"+p.Name+",timeToThrow:"+strconv.FormatFloat(float64(p.TimeToThrow), 'f', -1, 32)+
            ",dmgPerHit:"+strconv.FormatFloat(float64(p.DmgPerHit), 'f', -1, 32)+
            ",image:"+p.Image+",gridw:"+strconv.FormatInt(int64(p.GridWidth), 10)+
            ",gridh:"+strconv.FormatInt(int64(p.GridHeight), 10)
}
