package item

type Item interface {
    GetName() string
    GetImage() string
    GetGridWidth() int
    GetGridHeight() int
    GetItemMsg() string
}
