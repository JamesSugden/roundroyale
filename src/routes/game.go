package routes

import (
    "log"
    "net/http"
    "state"
    "encoding/json"
)

// Returns the list of players in a game/lobby.
// Authorization token must be validated before list is returned.
func GetPlayerList(w http.ResponseWriter, r *http.Request) {
    lobbyNames, okLobbyName := r.URL.Query()["lobby_name"]

    // Ensure lobby_name is given
    if !okLobbyName || len(lobbyNames) < 1 {
        log.Println("Error: URL param lobby_name missing")
        http.Error(w, "Error: URL param lobby_name missing", http.StatusBadRequest)
        return
    }

    // Get lobby_name
    lobbyName := lobbyNames[0]

    // Get the game
    game := state.GetGame(lobbyName)

    if game == nil {
        // If the game doesn't exist, return an error
        log.Println("Error: game not found")
        http.Error(w, "Error: game not found", http.StatusNotFound)
        return
    }

    // Set content type to JSON so client knows what to expect
    w.Header().Set("Content-Type", "application/json")

    // Send a list of JSON encoded players
    json.NewEncoder(w).Encode(game.GetPlayers())
}
