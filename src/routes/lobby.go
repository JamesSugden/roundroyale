package routes

import (
    "log"
    "net/http"
    "game"
    "state"
    "encoding/json"
)

// Creates a new lobby which other players can join.
// Lobby can be public or password-protected.
func CreateLobby(w http.ResponseWriter, r *http.Request) {
    playerNames, okPlayerName := r.URL.Query()["player_name"]
    pwords, okPword := r.URL.Query()["password"]

    // Ensure player_name is given
    if !okPlayerName || len(playerNames) < 1 {
        log.Println("Error: URL param player_name missing")
        http.Error(w, "Error: URL param player_name missing", http.StatusBadRequest)
        return
    }

    // Get player_name
    playerName := playerNames[0]

    var g *game.Game
    if !okPword || len(pwords) < 1 {
        // Create a new public game lobby
        g = game.NewPublicGame(playerName)
    } else {
        // Get password
        pword := pwords[0]
        // Create a new private game lobby
        g = game.NewPrivateGame(playerName, pword)
    }

    // Add the game to the list of active games.
    // Then, add a player and return it's validation token.
    if state.AddGame(playerName, g) {
        // Error: Game with name already exists
        http.Error(w, "Error: lobby name taken", http.StatusBadRequest)
    } else {
        log.Println("Created Lobby")
        log.Println("Lobby Name: " + playerName)

        // Add the player who created the lobby automatically
        token := state.GenRandomToken() // token used to validate input is from the correct user
        g.AddPlayer(game.NewPlayer(playerName, token))
        w.Write([]byte(token))  // send the token to the player
    }
}

// Join an existing game lobby.
// Can only join if the game has not yet started.
// If the lobby is private, a password is required
func JoinLobby(w http.ResponseWriter, r *http.Request) {
    playerNames, okPlayerName := r.URL.Query()["player_name"]
    lobbyNames, okLobbyName := r.URL.Query()["lobby_name"]
    pwords, okPword := r.URL.Query()["password"]

    // Ensure lobby_name is given
    if !okPlayerName || len(playerNames) < 1 {
        log.Println("Error: URL param player_name missing")
        return
    }

    // Get player_name
    playerName := playerNames[0]

    // Ensure lobby_name is given
    if !okLobbyName || len(lobbyNames) < 1 {
        log.Println("Error: URL param lobby_name missing")
        http.Error(w, "Error: URL param lobby_name missing", http.StatusBadRequest)
        return
    }

    // Get lobby_name
    lobbyName := lobbyNames[0]

    // Get a pointer to the game
    g := state.GetGame(lobbyName)

    if g == nil {
        // Error: Lobby does not exist
        log.Println("Error: lobby not found")
        http.Error(w, "Error: lobby not found", http.StatusNotFound)
        return
    }

    var pword string = ""
    if !(!okPword || len(pwords) < 1) {
        // Get password
        pword = pwords[0]
    }

    // TODO - check player name is unique
    player := g.GetPlayer(playerName)
    if player != nil {
        http.Error(w, "Error: player name taken", http.StatusBadRequest)
        return
    }

    // Add the player iff the password is correct
    if g.IsPasswordCorrect(pword) {
        token := state.GenRandomToken() // token used to validate input is from the correct user
        g.AddPlayer(game.NewPlayer(playerName, token))
        w.Write([]byte(token))  // send the token to the player
    } else {
        // Error: Password wrong
        http.Error(w, "Error: password incorrect", http.StatusUnauthorized)
    }
}

// Get a list of existing game lobbys.
func GetLobbyList(w http.ResponseWriter, r *http.Request) {
    // Get list of lobbys
    games := state.GetGames()

    // For each lobby, add an object to the map
    lobbyInfo := make(map[string]interface{})
    for name, game := range *games {
        lobbyInfo[name] = struct{
            Started bool
            NumPlayers int
            Private bool
        }{ game.HasStarted(), len(game.GetPlayers()), game.IsLobbyPrivate() }
    }

    // Set content type to JSON so client knows what to expect
    w.Header().Set("Content-Type", "application/json")

    // Send a list of JSON encoded lobbys
    json.NewEncoder(w).Encode(lobbyInfo)
}
